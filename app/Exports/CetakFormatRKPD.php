<?php

namespace App\Exports;

use App\Models\Urusan_90;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class CetakFormatRKPD implements FromView
{
    public function view(): View
    {
        /*
        $data = Urusan_90::with([
            'bidangurusan.program.kegiatan.subkegiatan.indikatorsubkegiatan' => function ($query) {
                $query->whereHas('renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->with('realisasiindikatorsubkeg');
            },
            'bidangurusan.program.kegiatan.subkegiatan.apbd_kota' => function ($query) {
                $query->selectRaw('id_sub_kegiatan,id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->where('isPerubahan', 0)->groupBy('id_sub_kegiatan','id_renja');
            },
            'bidangurusan.program.kegiatan.subkegiatan.realisasi_anggaran' => function ($query) {
                $query->selectRaw('id_sub_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->where('isPerubahan', 0)->groupBy('id_sub_kegiatan','id_renja');
            },
            'bidangurusan.program.kegiatan.indikatorkegiatan' => function ($query) {
                $query->whereHas('renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->with(['realisasiindikatorkeg','renja.skpd']);
            },
            'bidangurusan.program.kegiatan.apbd_kota' => function ($query) {
                $query->selectRaw('id_kegiatan,id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->where('isPerubahan', 0)->groupBy('id_kegiatan','id_renja');
            },
            'bidangurusan.program.kegiatan.realisasi_anggaran' => function ($query) {
                $query->selectRaw('id_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022);
                })->where('isPerubahan', 0)->groupBy('id_kegiatan','id_renja');
            },
            'bidangurusan.program.kegiatan.subkegiatan.logrenja90.renja.skpd' => function ($query) {
                $query->whereHas('renja', function ($qr) {
                    return $qr->where('periode_usulan', 2022)
                    ->whereHas('logrenja', function ($ql) {
                        return $ql->where('isPerubahan', 0);
                    });
                });
            }
            ])->get();
            

        return view('monev.renja.cetakrkpd', [
            'data' => $data
        ]);

        */

        ini_set('max_execution_time', 3000); 

        $data = DB::select("SELECT cok.*,
        (SELECT ISNULL(SUM(l.apbd_kota),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2021 AND l.isPerubahan = CASE WHEN cok.perubahan = 0 THEN 0 ELSE 1 END
        ) AS anggaran_21,
        (SELECT ISNULL(SUM(l.apbd_kota),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2022 AND l.isPerubahan = CASE WHEN cok.tahun_22 = 1 THEN 0 ELSE -1 END
        ) AS anggaran_22,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_1),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON CASE WHEN cok.perubahan = 1 THEN R_LogRenja_90.id_log_renja_perubahan ELSE R_LogRenja_90.id_log_renja_perubahan END = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2021 AND l.isPerubahan = CASE WHEN cok.perubahan = 0 THEN 0 ELSE 1 END
        ) AS r_anggaran_21_tw_1,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_2),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON CASE WHEN cok.perubahan = 1 THEN R_LogRenja_90.id_log_renja_perubahan ELSE R_LogRenja_90.id_log_renja_perubahan END = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2021 AND l.isPerubahan = CASE WHEN cok.perubahan = 0 THEN 0 ELSE 1 END
        ) AS r_anggaran_21_tw_2,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_3),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON CASE WHEN cok.perubahan = 1 THEN R_LogRenja_90.id_log_renja_perubahan ELSE R_LogRenja_90.id_log_renja_perubahan END = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2021 AND l.isPerubahan = CASE WHEN cok.perubahan = 0 THEN 0 ELSE 1 END
        ) AS r_anggaran_21_tw_3,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_4),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON CASE WHEN cok.perubahan = 1 THEN R_LogRenja_90.id_log_renja_perubahan ELSE R_LogRenja_90.id_log_renja_perubahan END = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2021 AND l.isPerubahan = CASE WHEN cok.perubahan = 0 THEN 0 ELSE 1 END
        ) AS r_anggaran_21_tw_4,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_1),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2022 AND l.isPerubahan = CASE WHEN cok.tahun_22 = 1 THEN 0 ELSE -1 END
        ) AS r_anggaran_22_tw_1,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_2),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2022 AND l.isPerubahan = CASE WHEN cok.tahun_22 = 1 THEN 0 ELSE -1 END
        ) AS r_anggaran_22_tw_2,
        (SELECT ISNULL(SUM(R_LogRenja_90.r_3),0) FROM LogRenja_90 AS l
        JOIN Renja_90 ON Renja_90.id_renja = l.id_renja
        JOIN SKPD_90 s ON s.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 sub ON sub.id_sub_kegiatan = l.id_sub_kegiatan
        JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = l.id_log_renja
        WHERE sub.nama_sub_kegiatan = cok.nama_sub_kegiatan AND s.nama_skpd = cok.nama_skpd AND Renja_90.periode_usulan = 2022 AND l.isPerubahan = CASE WHEN cok.tahun_22 = 1 THEN 0 ELSE -1 END
        ) AS r_anggaran_22_tw_3,
        (SELECT DISTINCT 
            (SELECT data.d21+'#'+data.d22+'#'+data.indikator_program+'#'+data.target+'#'+data.satuan+'#'+data.realisasi+';' AS [text()] FROM 
            (SELECT ISNULL(indikator_program,'na') AS indikator_program,ISNULL(target,0) AS target,ISNULL(satuan,'na') AS satuan,ISNULL(realisasi,0) AS realisasi,'2021' AS d21,'0' AS d22
            FROM TargetRealisasiIKP t2
            JOIN IndikatorProgramRPJMD t1 ON t1.id_indikator_program = t2.id_indikator_program
            JOIN SatuanRPJMD ON SatuanRPJMD.id_satuan = t1.id_satuan
            JOIN ProgramRPJMD t6 ON t6.id_program = t1.id_program
            JOIN SKPD_90 t3 ON t3.id_skpd = t1.id_skpd
            WHERE t3.nama_skpd = CASE WHEN cok.nama_skpd like '%BAGIAN%' THEN 'Sekretariat Daerah' ELSE cok.nama_skpd END AND t6.program = cok.nama_program AND t2.id_tahun = CASE WHEN (cok.tahun_21) = 1 THEN 7 ELSE -1 END
            GROUP BY indikator_program,target,satuan,realisasi
            UNION ALL
            SELECT ISNULL(indikator_program,'na') AS indikator_program,ISNULL(target,0) AS target,ISNULL(satuan,'na') AS satuan,ISNULL(realisasi,0) AS realisasi,'0' AS d21,'2022' AS d22
            FROM TargetRealisasiIKP t2
            JOIN IndikatorProgramRPJMD t1 ON t1.id_indikator_program = t2.id_indikator_program
            JOIN SatuanRPJMD ON SatuanRPJMD.id_satuan = t1.id_satuan
            JOIN ProgramRPJMD t6 ON t6.id_program = t1.id_program
            JOIN SKPD_90 t3 ON t3.id_skpd = t1.id_skpd
            WHERE t3.nama_skpd = CASE WHEN cok.nama_skpd like '%BAGIAN%' THEN 'Sekretariat Daerah' ELSE cok.nama_skpd END AND t6.program = cok.nama_program AND t2.id_tahun = CASE WHEN (cok.tahun_22) = 1 THEN 8 ELSE -1 END
            GROUP BY indikator_program,target,satuan,realisasi) data
            FOR XML PATH ('')
            )) AS indi_prog,
        
        (SELECT DISTINCT 
                (SELECT data.m1+'#'+data.p1+'#'+data.m2+'#'+data.indikator_kegiatan+'#'+data.target+'#'+data.target_perubahan+'#'+data.satuan+'#'+data.t_1+'#'+data.t_2+'#'+data.t_3+'#'+data.t_4+';' AS [text()] FROM 
                (SELECT indikator_kegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'121' AS m1,'0' AS p1,'0' AS m2
                FROM IndikatorKegiatan_90 t2
                LEFT JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = t2.id_indikator_kegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN Kegiatan_90 t5 ON t5.id_kegiatan = t2.id_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_kegiatan = cok.id_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_21) = 1 THEN 2021 ELSE -1 END AND t4.isPerubahan = 0
                GROUP BY indikator_kegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4
                UNION ALL
                SELECT indikator_kegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'0' AS m1,'221' AS p1,'0' AS m2
                FROM IndikatorKegiatan_90 t2
                LEFT JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = t2.id_indikator_kegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN Kegiatan_90 t5 ON t5.id_kegiatan = t2.id_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_kegiatan = cok.id_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_21) = 1 THEN 2021 ELSE -1 END AND t4.isPerubahan = 1
                GROUP BY indikator_kegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4
                UNION ALL
                SELECT indikator_kegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'0' AS m1,'0' AS p1,'122' AS m2
                FROM IndikatorKegiatan_90 t2
                LEFT JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = t2.id_indikator_kegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN Kegiatan_90 t5 ON t5.id_kegiatan = t2.id_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_kegiatan = cok.id_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_22) = 1 THEN 2022 ELSE -1 END AND t4.isPerubahan = 0
                GROUP BY indikator_kegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4) data
                FOR XML PATH ('')
                )) AS indi_keg,
            
        (SELECT DISTINCT 
                (SELECT data.m1+'#'+data.p1+'#'+data.m2+'#'+data.indikator_subkegiatan+'#'+data.target+'#'+data.target_perubahan+'#'+data.satuan+'#'+data.t_1+'#'+data.t_2+'#'+data.t_3+'#'+data.t_4+';' AS [text()] FROM 
                (SELECT indikator_subkegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'121' AS m1,'0' AS p1,'0' AS m2
                FROM IndikatorSubKegiatan_90 t2
                LEFT JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = t2.id_indikator_subkegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN SubKegiatan_90 t5 ON t5.id_sub_kegiatan = t2.id_sub_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_sub_kegiatan = cok.id_sub_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_21) = 1 THEN 2021 ELSE -1 END AND t4.isPerubahan = 0
                GROUP BY indikator_subkegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4
                UNION ALL
                SELECT indikator_subkegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'0' AS m1,'221' AS p1,'0' AS m2
                FROM IndikatorSubKegiatan_90 t2
                LEFT JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = t2.id_indikator_subkegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN SubKegiatan_90 t5 ON t5.id_sub_kegiatan = t2.id_sub_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_sub_kegiatan = cok.id_sub_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_21) = 1 THEN 2021 ELSE -1 END AND t4.isPerubahan = 1
                GROUP BY indikator_subkegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4
                UNION ALL
                SELECT indikator_subkegiatan,ISNULL(target,0) AS target,ISNULL(target_perubahan,0) AS target_perubahan,satuan,ISNULL(t_1,0) AS t_1,ISNULL(t_2,0) AS t_2,ISNULL(t_3,0) AS t_3,ISNULL(t_4,0) AS t_4,'0' AS m1,'0' AS p1,'122' AS m2
                FROM IndikatorSubKegiatan_90 t2
                LEFT JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = t2.id_indikator_subkegiatan
                JOIN Renja_90 t6 ON t6.id_renja = t2.id_renja
                JOIN SKPD_90 t3 ON t3.id_skpd = t6.id_skpd
                JOIN SubKegiatan_90 t5 ON t5.id_sub_kegiatan = t2.id_sub_kegiatan
                JOIN LogRenja_90 t4 ON t4.id_renja = t6.id_renja
                WHERE t3.id_skpd = cok.id_skpd AND t5.id_sub_kegiatan = cok.id_sub_kegiatan AND t6.periode_usulan= CASE WHEN (cok.tahun_22) = 1 THEN 2022 ELSE -1 END AND t4.isPerubahan = 0
                GROUP BY indikator_subkegiatan,target,target_perubahan,satuan,t_1,t_2,t_3,t_4) data
                FOR XML PATH ('')
                )) AS indi_sub_keg
        FROM
        (SELECT dataall.kode_urusan,dataall.nama_urusan,dataall.kode_bidang_urusan,dataall.nama_bidang_urusan,dataall.kode_program,dataall.nama_program,dataall.kode_kegiatan,dataall.nama_kegiatan,dataall.id_kegiatan,dataall.kode_sub_kegiatan,dataall.nama_sub_kegiatan,dataall.id_sub_kegiatan,dataall.id_skpd,dataall.nama_skpd,SUM(dataall.murni) AS murni,SUM(dataall.perubahan) AS perubahan,SUM(dataall.t_21) AS tahun_21,SUM(dataall.t_22) AS tahun_22 FROM 
        (SELECT data.kode_urusan,data.nama_urusan,data.kode_bidang_urusan,data.nama_bidang_urusan,data.kode_program,data.nama_program,data.kode_kegiatan,data.nama_kegiatan,data.id_kegiatan,data.kode_sub_kegiatan,data.nama_sub_kegiatan,data.id_sub_kegiatan,data.id_skpd,data.nama_skpd,SUM(data.murni) AS murni,SUM(data.perubahan) AS perubahan,1 AS t_21,0 AS t_22 FROM
        (SELECT Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,Kegiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.id_skpd,SKPD_90.nama_skpd,1 AS murni,0 AS perubahan FROM LogRenja_90
        JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
        JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
        JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
        JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
        JOIN BidangUrusan_90 ON BidangUrusan_90.id_bidang_urusan = Program_90.id_bidang_urusan
        JOIN Urusan_90 ON Urusan_90.id_urusan = BidangUrusan_90.id_urusan
        WHERE Renja_90.periode_usulan = 2021 AND LogRenja_90.isPerubahan = 0
        GROUP BY Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,KEgiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.nama_skpd,SKPD_90.id_skpd
        UNION ALL
        SELECT Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,Kegiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.id_skpd,SKPD_90.nama_skpd,0 AS murni,1 AS perubahan FROM LogRenja_90
        JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
        JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
        JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
        JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
        JOIN BidangUrusan_90 ON BidangUrusan_90.id_bidang_urusan = Program_90.id_bidang_urusan
        JOIN Urusan_90 ON Urusan_90.id_urusan = BidangUrusan_90.id_urusan
        WHERE Renja_90.periode_usulan = 2021 AND LogRenja_90.isPerubahan = 1
        GROUP BY Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,KEgiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.nama_skpd,SKPD_90.id_skpd) AS data
        GROUP BY data.kode_urusan,data.nama_urusan,data.kode_bidang_urusan,data.nama_bidang_urusan,data.kode_program,data.nama_program,data.kode_kegiatan,data.nama_kegiatan,data.id_kegiatan,data.kode_sub_kegiatan,data.nama_sub_kegiatan,data.id_sub_kegiatan,data.id_skpd,data.nama_skpd
        
        UNION ALL
        
        SELECT Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,Kegiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.id_skpd,SKPD_90.nama_skpd,1 AS murni,0 AS perubahan,0 AS t_21, 1 AS t_22 FROM LogRenja_90
        JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
        JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
        JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
        JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
        JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
        JOIN BidangUrusan_90 ON BidangUrusan_90.id_bidang_urusan = Program_90.id_bidang_urusan
        JOIN Urusan_90 ON Urusan_90.id_urusan = BidangUrusan_90.id_urusan
        WHERE Renja_90.periode_usulan = 2022 AND LogRenja_90.isPerubahan = 0
        GROUP BY Urusan_90.kode_urusan,Urusan_90.nama_urusan,BidangUrusan_90.kode_bidang_urusan,BidangUrusan_90.nama_bidang_urusan,Program_90.kode_program,Program_90.nama_program,Kegiatan_90.kode_kegiatan,Kegiatan_90.nama_kegiatan,KEgiatan_90.id_kegiatan,SubKegiatan_90.kode_sub_kegiatan,SubKegiatan_90.nama_sub_kegiatan,SubKegiatan_90.id_sub_kegiatan,SKPD_90.nama_skpd,SKPD_90.id_skpd) AS dataall
        GROUP BY dataall.kode_urusan,dataall.nama_urusan,dataall.kode_bidang_urusan,dataall.nama_bidang_urusan,dataall.kode_program,dataall.nama_program,dataall.kode_kegiatan,dataall.nama_kegiatan,dataall.id_kegiatan,dataall.kode_sub_kegiatan,dataall.nama_sub_kegiatan,dataall.id_sub_kegiatan,dataall.id_skpd,dataall.nama_skpd) AS cok
        ORDER BY cok.kode_urusan,cok.kode_bidang_urusan,cok.kode_program,cok.kode_kegiatan,cok.kode_sub_kegiatan"
        );
        return view('monev.renja.cetakrkpdnew', [
            'data' => $data
        ]);
    }
}
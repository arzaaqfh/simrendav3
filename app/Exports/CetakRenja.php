<?php

namespace App\Exports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Session;

use App\Models\SKPD_90;
class CetakRenja implements WithStyles,FromView
{
    public function __construct(Request $request)
    {
        $this->newRequest = $request;
    }

    public function styles(Worksheet $sheet)
    {
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = Session::get('id_skpd')[0];
        }else{
            $id_skpd = Session::get('pilih_id_skpd');
        }        
       
        // A13:AB285
        $all_row = 0;
        if($this->newRequest['all'] == 'true')
        {
            /*--------------------------------------------------------------
                MENGAMBIL DATA SKPD
            --------------------------------------------------------------*/
            $daftar_skpd = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->getdata($this->newRequest)
                ->toJson()
            );
            /*--------------------------------------------------------------*/
            // Ambil data program
            foreach($daftar_skpd as $skpd)
            {
                $newRequestAll = new Request();
                $newRequestAll->merge([
                    'token'     => Session::get('token'),
                    'triwulan'  => $this->newRequest['triwulan'],
                    'tahun'     => Session::get('tahun_monev'),
                    'opd'       => $skpd->id_skpd,
                    'subkeg'    => ''
                ]); 
                $data[] = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getCetakRKPD($newRequestAll)
                    ->toJson()
                );
            }
            $dataCetak = $data;
            $totalRow = 13;
            foreach($dataCetak as $dataAll)
            {
                $baris = 0;
                $barisP = 0;
                if(!empty($dataAll))
                {
                    $jml_urusan = count($dataAll);
                    foreach($dataAll as $urusan)
                    {
                        $baris++;
                        foreach($urusan->viewbidangurusan as $bu)
                        {
                            $baris++;
                            foreach($bu->viewprogram as $prg)
                            {
                                $baris++;
                                $baris = $baris+count($prg->indikator);
                                foreach($prg->viewkegiatan as $keg)
                                {
                                    $baris++;
                                    $baris = $baris+count($keg->indikator);
                                    foreach($keg->viewsubkegiatan as $subkeg)
                                    {
                                        $baris++;
                                        $baris = $baris+count($subkeg->indikator);
                                    }
                                }
                            }
                            $baris = $baris+6;
                        }
                    }
                    $totalRow = $totalRow + $baris;
                }
            }
            $all_row = $totalRow;
        }else{ 
            /*--------------------------------------------------------------
                MENGAMBIL DATA CETAK
            --------------------------------------------------------------*/
            // Ambil data program
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->getCetakRKPD($this->newRequest)
                ->toJson()
            );
            $dataCetak = $data;
            /*--------------------------------------------------------------*/
            $jml_total_row = 0;
            foreach($dataCetak as $urusan)
            {
                $jml_total_row = $jml_total_row + 1;
                foreach($urusan->viewbidangurusan as $bu)
                {
                    $jml_total_row = $jml_total_row + 1;
                    foreach($bu->viewprogram as $prg)
                    {
                        $jml_total_row = $jml_total_row + 1 + count($prg->indikator);
                        foreach($prg->viewkegiatan as $keg)
                        {
                            $jml_total_row = $jml_total_row + 1 + count($keg->indikator);
                            foreach($keg->viewsubkegiatan as $subkeg)
                            {
                                $jml_total_row = $jml_total_row + 1 + count($subkeg->indikator);
                            }
                        }
                    }
                }
            }
            $all_row = $jml_total_row+14+5;
        }
        // die();
        // echo 'A13:AB'.$all_row;
        // dd($dataCetak);
        return [
            // Style the first row as bold text.
            1 => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ],
            2 => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ],
            3 => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ],
            4 => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ],
            'A1:AB4' => [
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ]
            ],

            // Styling a specific cell by coordinate.
            'A10:AB13' => [
                'font' => ['bold' => true],
                'alignment' => [
                    'wrapText' => true,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ]
                ]
            ],
            'A14:AB'.$all_row => [
                'alignment' => [
                    'wrapText' => true
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ]                  
            ]
            // Styling an entire column.
            // 'C'  => ['font' => ['size' => 16]],
        ];
    }

    public function view(): View
    {
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = Session::get('id_skpd')[0];
        }else{
            $id_skpd = Session::get('pilih_id_skpd');
        }
        /*--------------------------------------------------------------
            MENGAMBIL DATA SKPD
        --------------------------------------------------------------*/
        $daftar_skpd = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getdata($this->newRequest)
            ->toJson()
        );
        /*--------------------------------------------------------------*/
        /*--------------------------------------------------------------
            MENGAMBIL DATA CETAK
        --------------------------------------------------------------*/
        $daftar_skpd_2 = null;
        $ipd = 0;
        if($this->newRequest['all'] == 'true')
        {
            // Ambil data program
            foreach($daftar_skpd as $skpd)
            {
                $newRequestAll = new Request();
                $newRequestAll->merge([
                    'token'     => Session::get('token'),
                    'triwulan'  => $this->newRequest['triwulan'],
                    'tahun'     => Session::get('tahun_monev'),
                    'opd'       => $skpd->id_skpd,
                    'subkeg'    => ''
                ]); 
                $data[] = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getCetakRKPD($newRequestAll)
                    ->toJson()
                );
                $skpd = '';
                if(!empty($data[$ipd]))
                {
                    $skpd = $data[$ipd][0]->viewbidangurusan[0]->viewprogram[0]->nama_skpd;
                }
                $daftar_skpd_2[] = $skpd;
                $ipd++;
            }
        }else{
            // Ambil data program
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->getCetakRKPD($this->newRequest)
                ->toJson()
            );
        }
        $dataCetak = $data;
        /*--------------------------------------------------------------*/
        $view = 'monev.renja.download.excel_rekapRENJA';
        $nama_skpd = SKPD_90::select('nama_skpd')->where('id_skpd', $id_skpd)->first()->nama_skpd;
        $triwulan = $this->newRequest['triwulan'];
        if($this->newRequest['all'] == 'true')
        {
            $view = 'monev.renja.download.excel_rekapRENJAAll';
            $nama_skpd = $daftar_skpd;
        }
        return view($view, [
            'dataCetak' => $dataCetak,
            'nama_skpd' => $nama_skpd,
            'triwulan'  => $triwulan
        ]);
    }
}

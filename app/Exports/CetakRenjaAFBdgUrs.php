<?php

namespace App\Exports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Session;

use App\Models\SKPD_90;
class CetakRenjaAFBdgUrs implements FromView
{
    public function __construct(Request $request)
    {
        $this->newRequest = $request;
    }

    public function view(): View
    {
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = '';
        }else{
            $id_skpd = Session::get('pilih_id_skpd');
        }
        
        /*--------------------------------------------------------------
            MENGAMBIL DATA CETAK
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getCetakRKPD($this->newRequest)
            ->toJson()
        );
        $dataCetak = $data;
        /*--------------------------------------------------------------*/
        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER SUB KEGIATAN
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($this->newRequest)
            ->toJson()
        );
        $anlSKG = null;
        foreach($data as $afSKG)
        {
            $anlSKG[$afSKG->id_sub_kegiatan] = $afSKG;
        }
        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER BIDANG URUSAN
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisFaktor($this->newRequest)
            ->toJson()
        );
        $anlFkt = $data;
        /*--------------------------------------------------------------*/
        $nama_skpd = SKPD_90::select('nama_skpd')->where('id_skpd', $id_skpd)->first()->nama_skpd;

        return view('monev.renja.download.excel_rekapAnalisisFaktor_BidangUrusan_RENJA', [
            'dataCetak' => $dataCetak,
            'nama_skpd' => $nama_skpd,
            'anlFkt'    => $anlFkt,
            'triwulan'  => $this->newRequest->triwulan
        ]);
    }
}

<?php

namespace App\Exports;

use App\Models\TempExportMonev;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportPreview implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return TempExportMonev::all();
    }

    public function headings(): array
    {
        return [
            'nomor', 
            'urusan',
            'bidang_urusan',
            'program',
            'indikator_program',
            'target_ip',
            'satuan_ip',
            'kegiatan',
            'indikator_kegiatan',
            'target_ik',
            'satuan_ik',
            'sub_kegiatan',
            'indikator_subkegiatan',
            'target_isk',
            'satuan_isk',
            'anggaran',
            'perangkat_daerah'
        ];
    }
}

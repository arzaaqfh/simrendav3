<?php

namespace App\Exports;

use App\Models\Sdgs_Target_Realisasi_Kota_90;
use App\Models\SKPD_90;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;
use Session;

class SdgsExport implements WithStyles,FromView
{
    public function __construct(Request $request)
    {
        $this->newRequest = $request;
    }
    public function styles(Worksheet $sheet)
    {        
        $tahun = $this->newRequest->tahun;
        if($this->newRequest->all == 1)
        {
            $dataCetak = DB::select("
                SELECT SDGS_Tujuan.nama_tujuan,SDGS_Global.target_global,SDGS_Nasional.target_nasional,SDGS_IndikatorKota_90.nama_indikator_kota,SDGS_IndikatorKota_90.satuan,
                SDGS_TargetRealisasiKota_90.*,SubKegiatan_90.nama_sub_kegiatan,Kegiatan_90.nama_kegiatan,Program_90.nama_program,SKPD_90.nama_skpd FROM SDGS_TargetRealisasiKota_90
                JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
                JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
                JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
                JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
                JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
                JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
                JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
                LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
                LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
                LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                WHERE SKPD_90.id_skpd NOT IN (14,15,16)
                AND SDGS_TargetRealisasiKota_90.tahun = ?
                ORDER BY SKPD_90.nama_skpd 
            ", array($this->newRequest->tahun));
        }else{
            $dataCetak = DB::select("
                SELECT SDGS_Tujuan.nama_tujuan,SDGS_Global.target_global,SDGS_Nasional.target_nasional,SDGS_IndikatorKota_90.nama_indikator_kota,SDGS_IndikatorKota_90.satuan,
                SDGS_TargetRealisasiKota_90.*,SubKegiatan_90.nama_sub_kegiatan,Kegiatan_90.nama_kegiatan,Program_90.nama_program,SKPD_90.nama_skpd FROM SDGS_TargetRealisasiKota_90
                JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
                JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
                JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
                JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
                JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
                JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
                JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
                LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
                LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
                LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                WHERE SKPD_90.id_skpd NOT IN (14,15,16)
                AND SKPD_90.id_skpd = ?
                AND SDGS_TargetRealisasiKota_90.tahun = ?
            ", array($this->newRequest->id_skpd,$this->newRequest->tahun));
        }

        $totalRow = 0;
        foreach($dataCetak as $data)
        {
            $totalRow = $totalRow+2;
        }
        $totalRow = $totalRow+5;
        return [
            // Style the first row as bold text.
            'A1:R5' => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
                    'wrapText' => true
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ]
                ]
            ],
            'A6:R'.$totalRow => [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ]
                ]
            ]
        ];
    }
    public function view(): View
    {
        $tahun = $this->newRequest->tahun;
        if($this->newRequest->all == 1)
        {
            $dataCetak = DB::select("
                SELECT 
                    SDGS_Tujuan.nama_tujuan,
                    SDGS_Global.target_global,
                    SDGS_Nasional.target_nasional,
                    SDGS_IndikatorKota_90.nama_indikator_kota,
                    SDGS_IndikatorKota_90.satuan,
                    SDGS_TargetRealisasiKota_90.*,
                    SubKegiatan_90.nama_sub_kegiatan,
                    Kegiatan_90.nama_kegiatan,
                    Program_90.nama_program,
                    SKPD_90.nama_skpd,
                    SKPD_90.kode_skpd 
                FROM SDGS_TargetRealisasiKota_90
                JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
                JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
                JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
                JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
                JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
                JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
                JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
                LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
                LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
                LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                WHERE SKPD_90.id_skpd NOT IN (14,15,16)
                AND SDGS_TargetRealisasiKota_90.tahun = ?
                ORDER BY SKPD_90.kode_skpd 
            ", array($this->newRequest->tahun));
        }else{
            $dataCetak = DB::select("
                SELECT SDGS_Tujuan.nama_tujuan,SDGS_Global.target_global,SDGS_Nasional.target_nasional,SDGS_IndikatorKota_90.nama_indikator_kota,SDGS_IndikatorKota_90.satuan,
                SDGS_TargetRealisasiKota_90.*,SubKegiatan_90.nama_sub_kegiatan,Kegiatan_90.nama_kegiatan,Program_90.nama_program,SKPD_90.nama_skpd FROM SDGS_TargetRealisasiKota_90
                JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
                JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
                JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
                JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
                JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
                JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
                JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
                LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
                LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
                LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                WHERE SKPD_90.id_skpd NOT IN (14,15,16)
                AND SKPD_90.id_skpd = ?
                AND SDGS_TargetRealisasiKota_90.tahun = ?
            ", array($this->newRequest->id_skpd,$this->newRequest->tahun));
        }

        $view = "sdgs/download/cetak_excel";

        return view($view, [
            'dataCetak' => $dataCetak,
            'tahun'  => $tahun
        ]);
    }
}

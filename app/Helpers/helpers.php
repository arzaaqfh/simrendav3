<?php

// use Request;

if (!function_exists('fetchOwnApi')) {
    function fetchOwnApi($url, $method, $data = [])
    {
        // Store the original input of the request
        $originalInput = Request::input();

        // fetch data from api url, method and data
        $request = Request::create($url, $method, $data);
        Request::replace($request->input());
        $response = Route::dispatch($request);
        $data = json_decode($response->getContent(), true);

        // Replace the input again with the original request input.
        Request::replace($originalInput);

        return $data;
    }
}

if (!function_exists('roundNominal')) {
    function roundNominal($nominal)
    {
        $satuan = array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
        $temp = '';
        $i = 0;

        while ($nominal >= 1000) {
            $nominal = ($nominal / 1000);
            $i++;
        }

        // round down to 2 decimal places
        $temp = ($nominal > 0 ? floor($nominal * 100) / 100 . ' ' . $satuan[$i] : $temp);
        return $temp;
    }
}

if (!function_exists('getSubkegRealIndTriwulan')) {
    function getSubkegRealIndTriwulan($indikator, $triwulan)
    {
        // set data
        $realisasi = 0;
        if ($indikator) {
            if ($triwulan == 4) {
                $realisasi = $indikator['targetindsubkegiatanperubahan']['realisasitargetindsubkegiatan'] ? $indikator['targetindsubkegiatanperubahan']['realisasitargetindsubkegiatan'][0]['realisasi'] : 0;
            } else {
                foreach ($indikator['targetindsubkegiatanmurni']['realisasitargetindsubkegiatan'] as $realisasiIndikator) {
                    if ($realisasiIndikator['triwulan'] == $triwulan) {
                        $realisasi = $realisasiIndikator['realisasi'];
                    }
                }
            }
        }
        return $realisasi;
    }
}

if (!function_exists('formatPersen')) {
    function formatPersen($persen)
    {
        // if have decimal
        if (strpos($persen, '.') !== false) {
            $persen = number_format($persen, 2, ',', '.');
        } else {
            $persen = number_format($persen, 0, ',', '.');
        }
        return $persen;
    }
}

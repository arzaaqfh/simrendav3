<?php

namespace App\Http\Controllers;
use \stdClass;
use Session;
use App\Http\Resources\Api\API_RKPDResource;
use App\Models\Viewuser;

// IKU, IKK, IKP Lama
use App\Models\AsistensiRenja;
use App\Models\Bidang;
use App\Models\Data_IKK_Excel;
use App\Models\JenisUrusan;
use App\Models\KategoriUrusan;
use App\Models\Kegiatan;
use App\Models\Lima_IndikatorProgram;
use App\Models\Lima_IndikatorRPJMD;
use App\Models\Lima_IndikatorSasaranRPJMD;
use App\Models\Lima_KebijakanRenstra;
use App\Models\Lima_KegiatanRenstra;
use App\Models\Lima_MisiRPJMD;
use App\Models\Lima_Periode;
use App\Models\Lima_ProgramRPJMD;
use App\Models\Lima_ProgramSKPD;
use App\Models\Lima_Renstra;
use App\Models\Lima_RPJMD;
use App\Models\Lima_SasaranRPJMD;
use App\Models\Lima_TujuanRPJMD;
use App\Models\LimaP_IndikatorProgramRPJMD;
use App\Models\LimaP_IndikatorSasaranRPJMD;
use App\Models\Log_R_MonevRKPD;
use App\Models\LogRenja;
use App\Models\MonevRKPDDetail;
use App\Models\MonevRKPDHead;
use App\Models\PrioritasRKPD;
use App\Models\Program;
use App\Models\R_IKK;
use App\Models\R_IKU;
use App\Models\Renja;
use App\Models\SasaranPrioritasRKPD;
use App\Models\SKPD;
use App\Models\StatusCapaian;
use App\Models\Urusan;

// IKU, IKK, IKP Baru
use App\Models\KetercapaianRPJMD;
use App\Models\KategoriRPJMD;
use App\Models\SatuanRPJMD;
use App\Models\Periode;
use App\Models\Rumus;
use App\Models\RPJMD;
use App\Models\MisiRPJMD;
use App\Models\TujuanRPJMD;
use App\Models\IndikatorTujuanRPJMD;
use App\Models\SasaranRPJMD;
use App\Models\IndikatorSasaranRPJMD;
use App\Models\StrategiRPJMD;
use App\Models\UrusanRPJMD;
use App\Models\ProgramRPJMD;
use App\Models\IndikatorProgramRPJMD;
use App\Models\Aspek;
use App\Models\IndikatorKinerjaRPJMD;
use App\Models\SumberData;
use App\Models\TargetRealisasiTujuanIKU;
use App\Models\TargetRealisasiSasaranIKU;
use App\Models\TargetRealisasiIKK;
use App\Models\TargetRealisasiIKP;
use App\Models\FaktorPendorongIKP;
use App\Models\FaktorPenghambatIKP;
use App\Models\TindakLanjutIKP;
use App\Models\FaktorPendorongTujuanIKU;
use App\Models\FaktorPenghambatTujuanIKU;
use App\Models\TindakLanjutTujuanIKU;
use App\Models\FaktorPendorongSasaranIKU;
use App\Models\FaktorPenghambatSasaranIKU;
use App\Models\TindakLanjutSasaranIKU;
use App\Models\Tahun;
use App\Models\SKPD_90;

use Carbon\Carbon;
use Illuminate\Http\Request;

class Administrator_Controller extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tanggal = Carbon::now();

        return view('administrator/dashboard',compact('tanggal'));
    }

    /**
     * RKPD
     *
     *
     */
    // Index
    public function indexRKPD()
    {
        $tanggal = Carbon::now();

        return view('administrator/rkpd/dashboard',compact('tanggal'));
    }

    // Target Realisasi Tujuan IKU
    public function targetRealisasiTujuanIKU()
    {
        $targetRealisasi =  TargetRealisasiTujuanIKU::get();

        return view('administrator/rkpd/targetRealisasiTujuanIKU',compact('targetRealisasi'));
    }

    // Target Realisasi Sasaran IKU
    public function targetRealisasiSasaranIKU()
    {
        $targetRealisasi =  TargetRealisasiSasaranIKU::get();

        return view('administrator/rkpd/targetRealisasiSasaranIKU',compact('targetRealisasi'));
    }
    
    // Target Realisasi IKK
    public function targetRealisasiIKK()
    {
        $targetRealisasi =  TargetRealisasiIKK::get();

        return view('administrator/rkpd/targetRealisasiIKK',compact('targetRealisasi'));
    }
    
    // Target Realisasi IKP
    public function targetRealisasiIKP()
    {
        $targetRealisasi =  TargetRealisasiIKP::get();

        return view('administrator/rkpd/targetRealisasiIKP',compact('targetRealisasi'));
    }

    // Indikator Tujuan IKU
    public function indikatorTujuanIKU()
    {
        $indikator =  IndikatorTujuanRPJMD::get();

        return view('administrator/rkpd/indikatorTujuanRPJMD',compact('indikator'));
    }

    // Indikator Sasaran IKU
    public function indikatorSasaranIKU()
    {
        $indikator =  IndikatorSasaranRPJMD::get();

        return view('administrator/rkpd/indikatorSasaranRPJMD',compact('indikator'));
    }
    
    // Indikator Kinerja
    public function indikatorKinerja()
    {
        $indikator =  IndikatorKinerjaRPJMD::get();

        return view('administrator/rkpd/indikatorKinerjaRPJMD',compact('indikator'));
    }
    
    // Indikator Program
    public function indikatorProgram()
    {
        $indikator =  IndikatorProgramRPJMD::get();

        return view('administrator/rkpd/indikatorProgramRPJMD',compact('indikator'));
    }

    // FORM
    // Target Realisasi IKP
    public function inputTargetRealisasiIKP()
    {
        $indikatorProgram = IndikatorProgramRPJMD::get();
        $ketercapaian = KetercapaianRPJMD::get();
        $kategori = KategoriRPJMD::get();
        $sumberData = SumberData::get();
        $tahun = Tahun::get();

        return view('administrator/rkpd/inputTargetRealisasiIKP',compact('indikatorProgram','ketercapaian','kategori','sumberData','tahun'));
    }
    // Indikator Program
    public function inputIndikatorProgram()
    {
        $satuan = SatuanRPJMD::get();
        $program = ProgramRPJMD::get();
        $skpd = SKPD_90::get();
        $rumus = Rumus::get();

        return view('administrator/rkpd/inputIndikatorProgram',compact('satuan','program','skpd','rumus'));
    }

    // INPUT
    // Target Realisasi IKP
    public function createTargetRealisasiIKP(Request $request)
    {
        $putTargetRealisasiIKP = new TargetRealisasiIKP();

        $putTargetRealisasiIKP->target = $request->target;
        $putTargetRealisasiIKP->realisasi = $request->realisasi;
        $putTargetRealisasiIKP->keterangan = $request->keterangan;
        $putTargetRealisasiIKP->formula = $request->formula;
        $putTargetRealisasiIKP->id_sumber_data = $request->sumberData;
        $putTargetRealisasiIKP->id_tahun = $request->tahun;
        $putTargetRealisasiIKP->id_indikator_program = $request->indikatorProgram;
        $putTargetRealisasiIKP->id_ketercapaian = $request->ketercapaian;
        $putTargetRealisasiIKP->id_kategori = $request->kategori;

        $putTargetRealisasiIKP->timestamps = false;
        $putTargetRealisasiIKP->save();

        return redirect('administrator/monev/rkpd/targetRealisasiIKP');
    }
    // Indikator Program
    public function createIndikatorProgram(Request $request)
    {
        $putIndikatorProgram = new IndikatorProgramRPJMD();

        $putIndikatorProgram->indikator_program = $request->indikatorProgram;
        $putIndikatorProgram->id_satuan = $request->satuan;
        $putIndikatorProgram->id_program = $request->program;
        $putIndikatorProgram->id_rumus = $request->rumus;
        $putIndikatorProgram->id_skpd = $request->skpd;

        $putIndikatorProgram->timestamps = false;
        $putIndikatorProgram->save();

        return redirect('administrator/monev/rkpd/indikatorProgram');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_RENJAResource;
use App\Models\LogRenja_90;
use App\Models\Kegiatan_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\IndikatorProgramRPJMD;
use App\Models\SKPD_90;
use App\Models\Satuan;
use App\Models\Renja\ViewSubKegiatanRenja;
use App\Models\Renja\ViewKegiatanRenja;
use App\Models\Renja\ViewProgramRenja;
use App\Models\Renja\ViewUrusanRenja;

use Illuminate\Support\Facades\DB;
use \stdClass;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class API_MonevRENJAController extends Controller
{    //Kegiatan
    public function getMonevKegiatan(Request $request)
    {   
        $this->id_kegiatan = $request->input('id_kegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');
        $this->nama_program = $request->input('nama_program');
        $this->nama_kegiatan = $request->input('nama_kegiatan');
        $this->nama_pd = $request->input('nama_pd');
        $this->status = $request->input('status');
        
        $data = Kegiatan_90::join('Subkegiatan_90','Subkegiatan_90.id_kegiatan','Kegiatan_90.id_kegiatan')
        ->join('LogRenja_90','LogRenja_90.id_sub_kegiatan','Subkegiatan_90.id_sub_kegiatan')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90', function($join) {
            $join->on('SKPD_90.id_skpd','Renja_90.id_skpd')->when($this->id_skpd, function ($q) {
                return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
            })->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan', 0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan', 1);
            })->when($this->nama_pd, function ($q) {
                return $q->where('nama_skpd','LIKE','%'.$this->nama_pd.'%');
            });
        })
        ->leftjoin('IndikatorKegiatan_90', function($join) {
            $join->on('IndikatorKegiatan_90.id_kegiatan','Kegiatan_90.id_kegiatan');
            $join->on('IndikatorKegiatan_90.id_renja','Renja_90.id_renja')
                ->when($this->step == 'murni', function ($q) {
                    return $q->where('is_added','<',4);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('is_emptied',0);
                });
        })->leftjoin('R_IndikatorKegiatan_90', function($join) {
            $join->on('R_IndikatorKegiatan_90.id_indikator_kegiatan','IndikatorKegiatan_90.id_indikator_kegiatan');
        })
        ->with(['program:id_program,nama_program'])
        ->when($this->nama_program, function ($q) {
            return $q->whereHas('program', function ($query) {
                $query->where('nama_program','LIKE','%'.$this->nama_program.'%');
            });
        })->when($this->id_kegiatan, function ($q) {
            return $q->where('Kegiatan_90.id_kegiatan',$this->id_kegiatan);
        })->when($this->nama_kegiatan, function ($q) {
            return $q->where('nama_kegiatan','LIKE','%'.$this->nama_kegiatan.'%');
        })->when($this->status, function ($q) {
            return $q->when($this->status == 'si-tw1', function ($query) {
                $query->whereNotNull('IndikatorKegiatan_90.indikator_kegiatan')->whereNotNull('R_IndikatorKegiatan_90.t_1');
            })->when($this->status == 'si-tw2', function ($query) {
                $query->whereNotNull('IndikatorKegiatan_90.indikator_kegiatan')->whereNotNull('R_IndikatorKegiatan_90.t_2');
            })->when($this->status == 'si-tw3', function ($query) {
                $query->whereNotNull('IndikatorKegiatan_90.indikator_kegiatan')->whereNotNull('R_IndikatorKegiatan_90.t_3');
            })->when($this->status == 'bsi-tw1', function ($query) {
                $query->WhereNull('R_IndikatorKegiatan_90.t_1')->orWhereNull('IndikatorKegiatan_90.indikator_kegiatan');
            })->when($this->status == 'bsi-tw2', function ($query) {
                $query->WhereNull('R_IndikatorKegiatan_90.t_2')->orWhereNull('IndikatorKegiatan_90.indikator_kegiatan');
            })->when($this->status == 'bsi-tw3', function ($query) {
                $query->WhereNull('R_IndikatorKegiatan_90.t_3')->orWhereNull('IndikatorKegiatan_90.indikator_kegiatan');
            })->when($this->status == 'bsi-tw4', function ($query) {
                $query->WhereNull('R_IndikatorKegiatan_90.t_4')->orWhereNull('IndikatorKegiatan_90.indikator_kegiatan');
            });
        })->select('Kegiatan_90.id_kegiatan','Kegiatan_90.nama_kegiatan','Kegiatan_90.id_program','Renja_90.id_renja','SKPD_90.id_skpd','SKPD_90.nama_skpd', DB::raw('COUNT(DISTINCT (CASE WHEN IndikatorKegiatan_90.is_added < 4 THEN IndikatorKegiatan_90.id_indikator_kegiatan END)) AS total_ind'),DB::raw('COUNT(DISTINCT IndikatorKegiatan_90.id_indikator_kegiatan) AS total_ind_perubahan'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_1 IS NOT NULL OR DATALENGTH(R_IndikatorKegiatan_90.t_1) > 0 THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS t_1inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_2 IS NOT NULL OR DATALENGTH(R_IndikatorKegiatan_90.t_2) > 0 THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS t_2inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_3 IS NOT NULL OR DATALENGTH(R_IndikatorKegiatan_90.t_3) > 0 THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS t_3inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_4 IS NOT NULL OR DATALENGTH(R_IndikatorKegiatan_90.t_4) > 0 THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS t_4inputed'),DB::raw('(SUM(DISTINCT (CASE WHEN IndikatorKegiatan_90.is_added < 4 THEN R_IndikatorKegiatan_90.total_persen END)))/ISNULL((COUNT(DISTINCT (CASE WHEN IndikatorKegiatan_90.is_added < 4 THEN IndikatorKegiatan_90.id_indikator_kegiatan END))),1) AS total_persen'),DB::raw('(SUM(DISTINCT R_IndikatorKegiatan_90.total_persen_perubahan))/ISNULL((COUNT(DISTINCT IndikatorKegiatan_90.id_indikator_kegiatan)),1) AS total_persen_perubahan'))
        ->groupBy('Kegiatan_90.id_kegiatan','Kegiatan_90.nama_kegiatan','Kegiatan_90.id_program','Renja_90.id_renja','SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();
       
        return API_RENJAResource::collection($data);
    }

    //RealisaiIndikatorKegiatan
    public function getRealisasiIndikatorKegiatan(Request $request)
    {
        $this->id_kegiatan = $request->input('id_kegiatan');
        $this->id_indikator_kegiatan = $request->input('id_indikator_kegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        if($this->id_skpd){
            if(!is_array($this->id_skpd)){
                $this->id_skpd = explode(' ', $this->id_skpd);
            }
        }
        
        $data = IndikatorKegiatan_90::whereHas('renja', function ($query) {
            $query->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('is_added','<',4);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('is_emptied',0);
            })->when($this->id_skpd, function ($q) {
                return $q->whereIn('id_skpd',$this->id_skpd);
            });
        })->with(['realisasiindikatorkeg','verifikasiIndikatorkegiatan','kegiatan.program.bidangUrusan.urusan','renja.skpd','kegiatan.apbd_kota' => function ($query) {
            $query->selectRaw('id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_kegiatan','id_renja');
        },'kegiatan.realisasi_anggaran' => function ($query) {
            $query->selectRaw('id_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_kegiatan','id_renja');
        },'kegiatan.realisasi_anggaran_perubahan' => function ($query) {
            $query->selectRaw('id_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3,SUM(r_4) as realisasi_tw4')->whereHas('logrenja90perubahan.renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_kegiatan','id_renja');
        }])->when($this->id_indikator_kegiatan, function ($q) {
                return $q->where('id_indikator_kegiatan',$this->id_indikator_kegiatan);
            })->when($this->id_kegiatan, function ($q) {
                return $q->where('id_kegiatan',$this->id_kegiatan);
            })->orderByRaw('id_kegiatan,id_indikator_kegiatan asc')->get();

            $headKegiatan = LogRenja_90::join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
            ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
            ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
            ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
            ->with(['kegiatan.program.bidangUrusan.urusan','renja.skpd','kegiatan.apbd_kota' => function ($query) {
                $query->selectRaw('id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_kegiatan','id_renja');
            },'kegiatan.realisasi_anggaran' => function ($query) {
                $query->selectRaw('id_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_kegiatan','id_renja');
            },'kegiatan.realisasi_anggaran_perubahan' => function ($query) {
                $query->selectRaw('id_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3,SUM(r_4) as realisasi_tw4')->whereHas('logrenja90perubahan.renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_kegiatan','id_renja');
            }])->when($this->id_kegiatan, function ($q) {
                return $q->where('Kegiatan_90.id_kegiatan',$this->id_kegiatan);
            })->when($this->id_skpd, function ($q) {
                return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
            })->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan', 0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan', 1);
            })->select('Kegiatan_90.id_program','Kegiatan_90.id_kegiatan','Kegiatan_90.kode_kegiatan','Kegiatan_90.nama_kegiatan','Renja_90.id_renja')
            ->groupBy('Kegiatan_90.id_program','Kegiatan_90.id_kegiatan','Kegiatan_90.kode_kegiatan','Kegiatan_90.nama_kegiatan','Renja_90.id_renja')
            ->get(); 
        
        $colDat = [
            'headindikator' => $headKegiatan,
            'indikator' => $data
        ];

        return API_RENJAResource::collection($colDat);
    }

    //SubKegiatan
    public function getMonevSubKegiatan(Request $request)
    {   
        $this->id_subkegiatan = $request->input('id_subkegiatan');
        $this->id_indikator_subkegiatan = $request->input('id_indikator_subkegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');
        $this->nama_program = $request->input('nama_program');
        $this->nama_kegiatan = $request->input('nama_kegiatan');
        $this->nama_subkegiatan = $request->input('nama_subkegiatan');
        $this->nama_pd = $request->input('nama_pd');
        $this->status = $request->input('status');

        $data = SubKegiatan_90::join('LogRenja_90','LogRenja_90.id_sub_kegiatan','Subkegiatan_90.id_sub_kegiatan')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90',function($join) {
            $join->on('SKPD_90.id_skpd','Renja_90.id_skpd')
            ->when($this->id_skpd, function ($q) {
                return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
            })->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan', 0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan', 1);
            })->when($this->nama_pd, function ($q) {
                return $q->where('nama_skpd','LIKE','%'.$this->nama_pd.'%');
            });
        })
        ->leftjoin('IndikatorSubKegiatan_90', function($join) {
            $join->on('IndikatorSubKegiatan_90.id_sub_kegiatan','SubKegiatan_90.id_sub_kegiatan');
            $join->on('IndikatorSubKegiatan_90.id_renja','Renja_90.id_renja')
                ->when($this->step == 'murni', function ($q) {
                    return $q->where('is_added','<',4);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('is_emptied',0);
                });
        })->leftjoin('R_IndikatorSubKegiatan_90','R_IndikatorSubKegiatan_90.id_indikator_subkegiatan','IndikatorSubKegiatan_90.id_indikator_subkegiatan')
        ->with(['kegiatan.program:id_program,nama_program'])
        ->when($this->id_subkegiatan, function ($q) {
            return $q->where('SubKegiatan_90.id_sub_kegiatan',$this->id_subkegiatan);
        })->when($this->nama_program, function ($q) {
            return $q->whereHas('kegiatan.program', function ($query) {
                $query->where('nama_program','LIKE','%'.$this->nama_program.'%');
            });
        })->when($this->nama_kegiatan, function ($q) {
            return $q->whereHas('kegiatan', function ($query) {
                $query->where('nama_kegiatan','LIKE','%'.$this->nama_kegiatan.'%');
            });
        })->when($this->nama_subkegiatan, function ($q) {
            return $q->where('nama_sub_kegiatan','LIKE','%'.$this->nama_subkegiatan.'%');
        })->when($this->status, function ($q) {
            return $q->when($this->status == 'si-tw1', function ($query) {
                $query->whereNotNull('IndikatorSubKegiatan_90.indikator_subkegiatan')->whereNotNull('R_IndikatorSubKegiatan_90.t_1');
            })->when($this->status == 'si-tw2', function ($query) {
                $query->whereNotNull('IndikatorSubKegiatan_90.indikator_subkegiatan')->whereNotNull('R_IndikatorSubKegiatan_90.t_2');
            })->when($this->status == 'si-tw3', function ($query) {
                $query->whereNotNull('IndikatorSubKegiatan_90.indikator_subkegiatan')->whereNotNull('R_IndikatorSubKegiatan_90.t_3');
            })->when($this->status == 'si-tw4', function ($query) {
                $query->whereNotNull('IndikatorSubKegiatan_90.indikator_subkegiatan')->whereNotNull('R_IndikatorSubKegiatan_90.t_4');
            })->when($this->status == 'bsi-tw1', function ($query) {
                $query->WhereNull('R_IndikatorSubKegiatan_90.t_1')->orWhereNull('IndikatorSubKegiatan_90.indikator_subkegiatan');
            })->when($this->status == 'bsi-tw2', function ($query) {
                $query->WhereNull('R_IndikatorSubKegiatan_90.t_2')->orWhereNull('IndikatorSubKegiatan_90.indikator_subkegiatan');
            })->when($this->status == 'bsi-tw3', function ($query) {
                $query->WhereNull('R_IndikatorSubKegiatan_90.t_3')->orWhereNull('IndikatorSubKegiatan_90.indikator_subkegiatan');
            })->when($this->status == 'bsi-tw4', function ($query) {
                $query->WhereNull('R_IndikatorSubKegiatan_90.t_4')->orWhereNull('IndikatorSubKegiatan_90.indikator_subkegiatan');
            });
        })->select('SubKegiatan_90.id_sub_kegiatan','SubKegiatan_90.nama_sub_kegiatan','SubKegiatan_90.id_kegiatan','Renja_90.id_renja','SKPD_90.id_skpd','SKPD_90.nama_skpd', DB::raw('COUNT(DISTINCT (CASE WHEN IndikatorSubKegiatan_90.is_added < 4 THEN IndikatorSubKegiatan_90.id_indikator_subkegiatan END)) AS total_ind'),DB::raw('COUNT(DISTINCT IndikatorSubKegiatan_90.id_indikator_subkegiatan) AS total_ind_perubahan'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_1 IS NOT NULL OR DATALENGTH(R_IndikatorSubKegiatan_90.t_1) > 0 THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS t_1inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_2 IS NOT NULL OR DATALENGTH(R_IndikatorSubKegiatan_90.t_2) > 0 THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS t_2inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_3 IS NOT NULL OR DATALENGTH(R_IndikatorSubKegiatan_90.t_3) > 0 THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS t_3inputed'),DB::raw('COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_4 IS NOT NULL OR DATALENGTH(R_IndikatorSubKegiatan_90.t_4) > 0 THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS t_4inputed'),DB::raw('(SUM(DISTINCT (CASE WHEN IndikatorSubKegiatan_90.is_added < 4 THEN R_IndikatorSubKegiatan_90.total_persen END)))/ISNULL((COUNT(DISTINCT (CASE WHEN IndikatorSubKegiatan_90.is_added < 4 THEN IndikatorSubKegiatan_90.id_indikator_subkegiatan END))),1) AS total_persen'),DB::raw('(SUM(DISTINCT R_IndikatorSubKegiatan_90.total_persen_perubahan))/ISNULL((COUNT(DISTINCT IndikatorSubKegiatan_90.id_indikator_subkegiatan)),1) AS total_persen_perubahan'))
        ->groupBy('SubKegiatan_90.id_sub_kegiatan','SubKegiatan_90.nama_sub_kegiatan','SubKegiatan_90.id_kegiatan','Renja_90.id_renja','SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();

        return API_RENJAResource::collection($data);
    }
    
    //RealisaiIndikatorSubKegiatan
    public function getRealisasiIndikatorSubKegiatan(Request $request)
    {
        $this->id_subkegiatan = $request->input('id_subkegiatan');
        $this->id_indikator_subkegiatan = $request->input('id_indikator_subkegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        if($this->id_skpd){
            if(!is_array($this->id_skpd)){
                $this->id_skpd = explode(' ', $this->id_skpd);
            }
        }
        
        $data = IndikatorSubKegiatan_90::whereHas('renja', function ($query) {
            $query->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('is_added','<',4);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('is_emptied',0);
            })->when($this->id_skpd, function ($q) {
                return $q->whereIn('id_skpd',$this->id_skpd);
            });
        })->with(['realisasiindikatorsubkeg','verifikasiIndikatorsubkegiatan','subkegiatan.kegiatan.program.bidangUrusan.urusan','renja.skpd','subkegiatan.apbd_kota' => function ($query) {
            $query->selectRaw('id_sub_kegiatan,id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_sub_kegiatan','id_renja');
        },'subkegiatan.realisasi_anggaran' => function ($query) {
            $query->selectRaw('id_sub_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_sub_kegiatan','id_renja');
        },'subkegiatan.realisasi_anggaran_perubahan' => function ($query) {
            $query->selectRaw('id_sub_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3,SUM(r_4) as realisasi_tw4')->whereHas('logrenja90perubahan.renja', function ($qr) {
                $qr->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                });
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan',0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan',1);
            })->groupBy('id_sub_kegiatan','id_renja');
        }])->when($this->id_indikator_subkegiatan, function ($q) {
                return $q->where('id_indikator_subkegiatan',$this->id_indikator_subkegiatan);
            })->when($this->id_subkegiatan, function ($q) {
                return $q->where('id_sub_kegiatan',$this->id_subkegiatan);
            })->orderByRaw('id_sub_kegiatan,id_indikator_subkegiatan asc')->get();


            $headSubKegiatan = LogRenja_90::join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
            ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
            ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
            ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
            ->with(['subkegiatan.kegiatan.program.bidangUrusan.urusan','renja.skpd','subkegiatan.apbd_kota' => function ($query) {
                $query->selectRaw('id_sub_kegiatan,id_renja,SUM(apbd_kota) as apbd_kota')->whereHas('renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_sub_kegiatan','id_renja');
            },'subkegiatan.realisasi_anggaran' => function ($query) {
                $query->selectRaw('id_sub_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3')->whereHas('logrenja90.renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_sub_kegiatan','id_renja');
            },'subkegiatan.realisasi_anggaran_perubahan' => function ($query) {
                $query->selectRaw('id_sub_kegiatan,id_renja,SUM(r_1) as realisasi_tw1,SUM(r_2) as realisasi_tw2,SUM(r_3) as realisasi_tw3,SUM(r_4) as realisasi_tw4')->whereHas('logrenja90perubahan.renja', function ($qr) {
                    $qr->when($this->periode_usulan, function ($q) {
                        return $q->where('periode_usulan', $this->periode_usulan);
                    });
                })->when($this->step == 'murni', function ($q) {
                    return $q->where('isPerubahan',0);
                })->when($this->step == 'perubahan', function ($q) {
                    return $q->where('isPerubahan',1);
                })->groupBy('id_sub_kegiatan','id_renja');
            }])->when($this->id_subkegiatan, function ($q) {
                return $q->where('SubKegiatan_90.id_sub_kegiatan',$this->id_subkegiatan);
            })->when($this->id_skpd, function ($q) {
                return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
            })->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->step == 'murni', function ($q) {
                return $q->where('isPerubahan', 0);
            })->when($this->step == 'perubahan', function ($q) {
                return $q->where('isPerubahan', 1);
            })->select('Kegiatan_90.id_kegiatan','SubKegiatan_90.id_sub_kegiatan','SubKegiatan_90.kode_sub_kegiatan','SubKegiatan_90.nama_sub_kegiatan','Renja_90.id_renja')
            ->groupBy('Kegiatan_90.id_kegiatan','SubKegiatan_90.id_sub_kegiatan','SubKegiatan_90.kode_sub_kegiatan','SubKegiatan_90.nama_sub_kegiatan','Renja_90.id_renja')
            ->get();
        
            $colDat = [
            'headindikator' => $headSubKegiatan,
            'indikator' => $data
        ];
        
        return API_RENJAResource::collection($colDat);
    }
    public function getRekapInputProgram(Request $request)
    {
        $tahun = $request->input('periode_usulan');
        $opd = $request->input('id_pd');

        if($opd)
        {
            $datamurni = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$opd) {
                $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                        ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                        ->with(['viewprogram' => function ($query) use ($tahun,$opd) {
                            $query->with(['program','indikator' => function ($query) use ($tahun) {
                                $query->join('SatuanRPJMD','SatuanRPJMD.id_satuan','IndikatorProgramRPJMD.id_satuan')
                                    ->with(['TargetRealisasiIKP' => function ($query) use ($tahun) {
                                        $query->whereHas('Tahun', function ($query) use ($tahun) {
                                            $query->where('tahun',$tahun);
                                        });
                                    }])->whereHas('TargetRealisasiIKP.Tahun', function ($query) use ($tahun) {
                                        $query->where('tahun',$tahun);
                                    });
                            }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                            ->when($opd, function ($q) use ($opd) {
                                return $q->where('id_skpd', $opd);
                            })
                            ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                        }])->when($opd, function ($q) use ($opd) {
                            return $q->where('id_skpd', $opd);
                        });
            }])
            ->where('periode_usulan', $tahun)
            ->when($opd, function ($q) use ($opd) {
                return $q->where('id_skpd', $opd);
            })
            ->where('isPerubahan', 0)
            ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
            ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
            ->get();
    
            $dataperubahan = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$opd) {
                $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                        ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                        ->with(['viewprogram' => function ($query) use ($tahun,$opd) {
                            $query->with(['program','indikator' => function ($query) use ($tahun) {
                                $query->join('SatuanRPJMD','SatuanRPJMD.id_satuan','IndikatorProgramRPJMD.id_satuan')
                                    ->with(['TargetRealisasiIKP' => function ($query) use ($tahun) {
                                        $query->whereHas('Tahun', function ($query) use ($tahun) {
                                            $query->where('tahun',$tahun);
                                        });
                                    }])->whereHas('TargetRealisasiIKP.Tahun', function ($query) use ($tahun) {
                                        $query->where('tahun',$tahun);
                                    });
                            }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                            ->when($opd, function ($q) use ($opd) {
                                return $q->where('id_skpd', $opd);
                            })
                            ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                        }])->when($opd, function ($q) use ($opd) {
                            return $q->where('id_skpd', $opd);
                        });
            }])
            ->where('periode_usulan', $tahun)
            ->when($opd, function ($q) use ($opd) {
                return $q->where('id_skpd', $opd);
            })
            ->where('isPerubahan', 1)
            ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
            ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
            ->get();
    
            $skpd = SKPD_90::where('id_skpd','=',$opd)->first();
            $rekapinput = array(
                "id_skpd" => $skpd->id_skpd,
                "nama_skpd" => $skpd->nama_skpd,
                "jml_program" => 0, //jumlah program
                "jml_program_haveind" => 0, //jumlah program yang memiliki indikator
                "jml_ind_program" => 0, //jumlah indikator program
                "jml_ind_prog1_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 1
                "jml_ind_prog2_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 2
                "jml_ind_prog3_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 3
                "jml_program_perubahan" => 0, //jumlah program di perubahan (triwulan 4)
                "jml_program_havind_perubahan" => 0, //jumlah program yang memiliki indikator di perubahan (triwulan 4)
                "jml_ind_program_perubahan" => 0, //jumlah indikator program di perubahan (triwulan 4)
                "jml_ind_prog4_havemonev_perubahan" => 0 //jumlah indikator program yang diisi di perubahan (triwulan 4)
            );
            $jml_program = 0;
            $jml_program_haveind = 0;
            $jml_ind_program = 0;
            $jml_ind_prog1_havemonev = 0;
            $jml_ind_prog2_havemonev = 0;
            $jml_ind_prog3_havemonev = 0;
            $jml_program_perubahan = 0;
            $jml_program_havind_perubahan = 0;
            $jml_ind_program_perubahan = 0;
            $jml_ind_prog4_havemonev_perubahan = 0;
            foreach($datamurni as $cek)
            {
                foreach($cek->viewbidangurusan as $bu)
                {
                    foreach($bu->viewprogram as $prg)
                    {
                        $cek_ind = 0; //cek program yang memiliki indikator
                        foreach($prg->indikator as $iprg)
                        {
                            $cek_r1 = 0;
                            $cek_r1_isi = 0;
                            $cek_r2 = 0;
                            $cek_r2_isi = 0;
                            $cek_r3 = 0;
                            $cek_r3_isi = 0;
                            foreach($iprg->TargetRealisasiIKP as $triprg)
                            {
                                if($triprg->triwulan == 1)
                                {
                                    if(!is_null($triprg->realisasi))
                                    {
                                        $cek_r1_isi++;
                                    }
                                    $cek_r1++;
                                }else if($triprg->triwulan == 2)
                                {
                                    if(!is_null($triprg->realisasi))
                                    {
                                        $cek_r2_isi++;
                                    }
                                    $cek_r2++;
    
                                }else if($triprg->triwulan == 3)
                                {
                                    if(!is_null($triprg->realisasi))
                                    {
                                        $cek_r3_isi++;
                                    }
                                    $cek_r3++;
                                }
                            }                        
                            if($cek_r1 > 0 && $cek_r1_isi > 0)
                            {
                                $jml_ind_prog1_havemonev++;
                            }
                            if($cek_r2 > 0 && $cek_r2_isi > 0)
                            {
                                $jml_ind_prog2_havemonev++;
                            }
                            if($cek_r3 > 0 && $cek_r3_isi > 0)
                            {
                                $jml_ind_prog3_havemonev++;
                            }
                            $cek_ind++;
                            $jml_ind_program++;
                        }
                        if($cek_ind > 0)
                        {
                            $jml_program_haveind++;
                        }
                        $jml_program++;
                    }
                }
            }
            foreach($dataperubahan as $cek)
            {
                foreach($cek->viewbidangurusan as $bu)
                {
                    foreach($bu->viewprogram as $prg)
                    {
                        $cek_ind = 0; //cek program yang memiliki indikator
                        foreach($prg->indikator as $iprg)
                        {
                            $cek_r4 = 0;
                            $cek_r4_isi = 0;
                            foreach($iprg->TargetRealisasiIKP as $triprg)
                            {
                                if($triprg->triwulan == 4)
                                {
                                    if(!is_null($triprg->realisasi))
                                    {
                                        $cek_r4_isi++;
                                    }
                                    $cek_r4++;
                                }                 
                            }
                            if($cek_r4 > 0 && $cek_r4_isi > 0)
                            {
                                $jml_ind_prog4_havemonev_perubahan++;
                            }
                            $cek_ind++;
                            $jml_ind_program_perubahan++;
                        }
                        if($cek_ind > 0)
                        {
                            $jml_program_havind_perubahan++;
                        }
                        $jml_program_perubahan++;
                    }
                }
            }
            $rekapinput['jml_program'] = $jml_program;
            $rekapinput['jml_program_haveind'] = $jml_program_haveind;
            $rekapinput['jml_ind_program'] = $jml_ind_program;
            $rekapinput['jml_ind_prog1_havemonev'] = $jml_ind_prog1_havemonev;
            $rekapinput['jml_ind_prog2_havemonev'] = $jml_ind_prog2_havemonev;
            $rekapinput['jml_ind_prog3_havemonev'] = $jml_ind_prog3_havemonev;
            $rekapinput['jml_program_perubahan'] = $jml_program_perubahan;
            $rekapinput['jml_program_havind_perubahan'] = $jml_program_havind_perubahan;
            $rekapinput['jml_ind_program_perubahan'] = $jml_ind_program_perubahan;
            $rekapinput['jml_ind_prog4_havemonev_perubahan'] = $jml_ind_prog4_havemonev_perubahan;
        }else{
            // SEMUA OPD
            $opd = SKPD_90::select('id_skpd','nama_skpd')->get();
            $irkp = 0;
            foreach($opd as $pd)
            {
                $id_skpd = $pd->id_skpd;
                $datamurni[$irkp] = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$id_skpd) {
                    $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->with(['viewprogram' => function ($query) use ($tahun,$id_skpd) {
                                $query->with(['programrpjmd','indikator' => function ($query) use ($tahun) {
                                    $query->join('SatuanRPJMD','SatuanRPJMD.id_satuan','IndikatorProgramRPJMD.id_satuan')
                                        ->with(['TargetRealisasiIKP' => function ($query) use ($tahun) {
                                            $query->whereHas('Tahun', function ($query) use ($tahun) {
                                                $query->where('tahun',$tahun);
                                            });
                                        }])->whereHas('TargetRealisasiIKP.Tahun', function ($query) use ($tahun) {
                                            $query->where('tahun',$tahun);
                                        });
                                }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                                ->when($id_skpd, function ($q) use ($id_skpd) {
                                    return $q->where('id_skpd', $id_skpd);
                                })
                                ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                            }])->when($id_skpd, function ($q) use ($id_skpd) {
                                return $q->where('id_skpd', $id_skpd);
                            });
                }])
                ->where('periode_usulan', $tahun)
                ->when($id_skpd, function ($q) use ($id_skpd) {
                    return $q->where('id_skpd', $id_skpd);
                })
                ->where('isPerubahan', 0)
                ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->get();

                $dataperubahan[$irkp] = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$id_skpd) {
                    $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->with(['viewprogram' => function ($query) use ($tahun,$id_skpd) {
                                $query->with(['programrpjmd','indikator' => function ($query) use ($tahun) {
                                    $query->join('SatuanRPJMD','SatuanRPJMD.id_satuan','IndikatorProgramRPJMD.id_satuan')
                                        ->with(['TargetRealisasiIKP' => function ($query) use ($tahun) {
                                            $query->whereHas('Tahun', function ($query) use ($tahun) {
                                                $query->where('tahun',$tahun);
                                            });
                                        }])->whereHas('TargetRealisasiIKP.Tahun', function ($query) use ($tahun) {
                                            $query->where('tahun',$tahun);
                                        });
                                }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                                ->when($id_skpd, function ($q) use ($id_skpd) {
                                    return $q->where('id_skpd', $id_skpd);
                                })
                                ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                            }])->when($id_skpd, function ($q) use ($id_skpd) {
                                return $q->where('id_skpd', $id_skpd);
                            });
                }])
                ->where('periode_usulan', $tahun)
                ->when($id_skpd, function ($q) use ($id_skpd) {
                    return $q->where('id_skpd', $id_skpd);
                })
                ->where('isPerubahan', 1)
                ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->get();
                $rekapinput[$irkp] = array(
                    "id_skpd" => $pd->id_skpd,
                    "nama_skpd" => $pd->nama_skpd,
                    "jml_program" => 0, //jumlah program
                    "jml_program_haveind" => 0, //jumlah program yang memiliki indikator
                    "jml_ind_program" => 0, //jumlah indikator program
                    "jml_ind_prog1_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 1
                    "jml_ind_prog2_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 2
                    "jml_ind_prog3_havemonev" => 0, //jumlah indikator program yang diisi di triwulan 3
                    "jml_program_perubahan" => 0, //jumlah program di perubahan (triwulan 4)
                    "jml_program_havind_perubahan" => 0, //jumlah program yang memiliki indikator di perubahan (triwulan 4)
                    "jml_ind_program_perubahan" => 0, //jumlah indikator program di perubahan (triwulan 4)
                    "jml_ind_prog4_havemonev_perubahan" => 0 //jumlah indikator program yang diisi di perubahan (triwulan 4)
                );
                $jml_program = 0;
                $jml_program_haveind = 0;
                $jml_ind_program = 0;
                $jml_ind_prog1_havemonev = 0;
                $jml_ind_prog2_havemonev = 0;
                $jml_ind_prog3_havemonev = 0;
                $jml_program_perubahan = 0;
                $jml_program_havind_perubahan = 0;
                $jml_ind_program_perubahan = 0;
                $jml_ind_prog4_havemonev_perubahan = 0;

                foreach($datamurni[$irkp] as $cek)
                {
                    foreach($cek->viewbidangurusan as $bu)
                    {
                        foreach($bu->viewprogram as $prg)
                        {
                            $cek_ind = 0; //cek program yang memiliki indikator
                            foreach($prg->indikator as $iprg)
                            {
                                $cek_r1 = 0;
                                $cek_r1_isi = 0;
                                $cek_r2 = 0;
                                $cek_r2_isi = 0;
                                $cek_r3 = 0;
                                $cek_r3_isi = 0;
                                foreach($iprg->TargetRealisasiIKP as $triprg)
                                {
                                    if($triprg->triwulan == 1)
                                    {
                                        if(!is_null($triprg->realisasi))
                                        {
                                            $cek_r1_isi++;
                                        }
                                        $cek_r1++;
                                    }else if($triprg->triwulan == 2)
                                    {
                                        if(!is_null($triprg->realisasi))
                                        {
                                            $cek_r2_isi++;
                                        }
                                        $cek_r2++;
        
                                    }else if($triprg->triwulan == 3)
                                    {
                                        if(!is_null($triprg->realisasi))
                                        {
                                            $cek_r3_isi++;
                                        }
                                        $cek_r3++;
                                    }
                                }                        
                                if($cek_r1 > 0 && $cek_r1_isi > 0)
                                {
                                    $jml_ind_prog1_havemonev++;
                                }
                                if($cek_r2 > 0 && $cek_r2_isi > 0)
                                {
                                    $jml_ind_prog2_havemonev++;
                                }
                                if($cek_r3 > 0 && $cek_r3_isi > 0)
                                {
                                    $jml_ind_prog3_havemonev++;
                                }
                                $cek_ind++;
                                $jml_ind_program++;
                            }
                            if($cek_ind > 0)
                            {
                                $jml_program_haveind++;
                            }
                            $jml_program++;
                        }
                    }
                }
                foreach($dataperubahan[$irkp] as $cek)
                {
                    foreach($cek->viewbidangurusan as $bu)
                    {
                        foreach($bu->viewprogram as $prg)
                        {
                            $cek_ind = 0; //cek program yang memiliki indikator
                            foreach($prg->indikator as $iprg)
                            {
                                $cek_r4 = 0;
                                $cek_r4_isi = 0;
                                foreach($iprg->TargetRealisasiIKP as $triprg)
                                {
                                    if($triprg->triwulan == 4)
                                    {
                                        if(!is_null($triprg->realisasi))
                                        {
                                            $cek_r4_isi++;
                                        }
                                        $cek_r4++;
                                    }                 
                                }
                                if($cek_r4 > 0 && $cek_r4_isi > 0)
                                {
                                    $jml_ind_prog4_havemonev_perubahan++;
                                }
                                $cek_ind++;
                                $jml_ind_program_perubahan++;
                            }
                            if($cek_ind > 0)
                            {
                                $jml_program_havind_perubahan++;
                            }
                            $jml_program_perubahan++;
                        }
                    }
                }
                $rekapinput[$irkp]['jml_program'] = $jml_program;
                $rekapinput[$irkp]['jml_program_haveind'] = $jml_program_haveind;
                $rekapinput[$irkp]['jml_ind_program'] = $jml_ind_program;
                $rekapinput[$irkp]['jml_ind_prog1_havemonev'] = $jml_ind_prog1_havemonev;
                $rekapinput[$irkp]['jml_ind_prog2_havemonev'] = $jml_ind_prog2_havemonev;
                $rekapinput[$irkp]['jml_ind_prog3_havemonev'] = $jml_ind_prog3_havemonev;
                $rekapinput[$irkp]['jml_program_perubahan'] = $jml_program_perubahan;
                $rekapinput[$irkp]['jml_program_havind_perubahan'] = $jml_program_havind_perubahan;
                $rekapinput[$irkp]['jml_ind_program_perubahan'] = $jml_ind_program_perubahan;
                $rekapinput[$irkp]['jml_ind_prog4_havemonev_perubahan'] = $jml_ind_prog4_havemonev_perubahan;

                $irkp++;
            }
        }
        
        return response()->json([
            'data' => $rekapinput,
        ]);
    }
    public function getRekapInputKegiatan(Request $request)
    {
        $this->id_kegiatan = $request->input('id_kegiatan');
        $this->id_indikator_kegiatan = $request->input('id_indikator_kegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        if($this->id_skpd){
            if(!is_array($this->id_skpd)){
                $this->id_skpd = explode(' ', $this->id_skpd);
            }
        }

        $data = LogRenja_90::
        selectRaw('SKPD_90.id_skpd,SKPD_90.nama_skpd,COUNT(DISTINCT(Kegiatan_90.id_kegiatan)) AS jml_kegiatan,
        COUNT(DISTINCT (CASE WHEN IndikatorKegiatan_90.indikator_kegiatan IS NOT NULL THEN Kegiatan_90.id_kegiatan END)) AS jml_kegiatan_haveind,
        COUNT(DISTINCT(CASE WHEN IndikatorKegiatan_90.indikator_kegiatan IS NOT NULL THEN IndikatorKegiatan_90.id_indikator_kegiatan END)) AS jml_ind_kegiatan,
        COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_1 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS jml_ind_keg1_havemonev,
        COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_2 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS jml_ind_keg2_havemonev,
        COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_3 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS jml_ind_keg3_havemonev')
        ->join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
        ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
        ->join('Program_90','Program_90.id_program','Kegiatan_90.id_program')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
        ->join('Bidang','Bidang.id_bidang','SKPD_90.id_bidang')
        ->leftjoin('IndikatorKegiatan_90', function ($join) {
            $join->on('IndikatorKegiatan_90.id_renja','Renja_90.id_renja')
            ->on('Kegiatan_90.id_kegiatan','IndikatorKegiatan_90.id_kegiatan');
        })->leftjoin('R_IndikatorKegiatan_90','R_IndikatorKegiatan_90.id_indikator_kegiatan','IndikatorKegiatan_90.id_indikator_kegiatan')
        ->where('LogRenja_90.isPerubahan',0)->where('IndikatorKegiatan_90.is_added','<=',4)->where('IndikatorKegiatan_90.is_emptied','!=',4)
        ->when($this->periode_usulan, function ($q) {
            return $q->where('Renja_90.periode_usulan', $this->periode_usulan);
        })->when($this->id_skpd, function ($q) {
            return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
        })
        ->groupBy('SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();

        $data_perubahan = LogRenja_90::
        selectRaw('SKPD_90.id_skpd,SKPD_90.nama_skpd,COUNT(DISTINCT(Kegiatan_90.id_kegiatan)) AS jml_kegiatan_perubahan,
        COUNT(DISTINCT (CASE WHEN IndikatorKegiatan_90.indikator_kegiatan IS NOT NULL THEN Kegiatan_90.id_kegiatan END)) AS jml_kegiatan_haveind_perubahan,
        COUNT(DISTINCT(CASE WHEN IndikatorKegiatan_90.indikator_kegiatan IS NOT NULL THEN IndikatorKegiatan_90.id_indikator_kegiatan END)) AS jml_ind_kegiatan_perubahan,
        COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_4 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS jml_ind_keg4_havemonev_perubahan')
        ->join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
        ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
        ->join('Program_90','Program_90.id_program','Kegiatan_90.id_program')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
        ->join('Bidang','Bidang.id_bidang','SKPD_90.id_bidang')
        ->leftjoin('IndikatorKegiatan_90', function ($join) {
            $join->on('IndikatorKegiatan_90.id_renja','Renja_90.id_renja')
            ->on('Kegiatan_90.id_kegiatan','IndikatorKegiatan_90.id_kegiatan');
        })->leftjoin('R_IndikatorKegiatan_90','R_IndikatorKegiatan_90.id_indikator_kegiatan','IndikatorKegiatan_90.id_indikator_kegiatan')
        ->where('LogRenja_90.isPerubahan',1)->where('IndikatorKegiatan_90.is_emptied',0)
        ->when($this->periode_usulan, function ($q) {
            return $q->where('Renja_90.periode_usulan', $this->periode_usulan);
        })->when($this->id_skpd, function ($q) {
            return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
        })
        ->groupBy('SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();

        if(count($data) > 0){
            foreach($data as $value){
                if(count($data_perubahan) > 0){
                    foreach($data_perubahan as  $val){
                        if($val['id_skpd'] == $value['id_skpd']){
                            $value['jml_kegiatan_perubahan'] = $val['jml_kegiatan_perubahan'];
                            $value['jml_kegiatan_haveind_perubahan'] = $val['jml_kegiatan_haveind_perubahan'];
                            $value['jml_ind_kegiatan_perubahan'] = $val['jml_ind_kegiatan_perubahan'];
                            $value['jml_ind_keg4_havemonev_perubahan'] = $val['jml_ind_keg4_havemonev_perubahan'];
                        }
                    }
                }else{
                    $value['jml_kegiatan_perubahan'] = 0;
                    $value['jml_kegiatan_haveind_perubahan'] = 0;
                    $value['jml_ind_kegiatan_perubahan'] = 0;
                    $value['jml_ind_keg4_havemonev_perubahan'] = 0;
                }
            }    
        }

        return response()->json([
            'data' => $data,
        ]);
    }
    public function getRekapInputSubKegiatan(Request $request)
    {
        $this->id_subkegiatan = $request->input('id_subkegiatan');
        $this->id_indikator_subkegiatan = $request->input('id_indikator_subkegiatan');
        $this->step = $request->input('step');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        if($this->id_skpd){
            if(!is_array($this->id_skpd)){
                $this->id_skpd = explode(' ', $this->id_skpd);
            }
        }

        $data = LogRenja_90::
        selectRaw('SKPD_90.id_skpd,SKPD_90.nama_skpd,COUNT(DISTINCT(SubKegiatan_90.id_sub_kegiatan)) AS jml_subkegiatan,
        COUNT(DISTINCT (CASE WHEN IndikatorSubKegiatan_90.indikator_subkegiatan IS NOT NULL THEN SubKegiatan_90.id_sub_kegiatan END)) AS jml_sub_kegiatan_haveind,
        COUNT(DISTINCT CASE WHEN IndikatorSubKegiatan_90.indikator_subkegiatan IS NOT NULL THEN IndikatorSubKegiatan_90.id_indikator_subkegiatan END) AS jml_ind_sub_kegiatan,
        COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_1 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS jml_ind_subkeg1_havemonev,
        COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_2 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS jml_ind_subkeg2_havemonev,
        COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_3 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS jml_ind_subkeg3_havemonev')
        ->join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
        ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
        ->join('Program_90','Program_90.id_program','Kegiatan_90.id_program')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
        ->join('Bidang','Bidang.id_bidang','SKPD_90.id_bidang')
        ->leftjoin('IndikatorSubKegiatan_90', function ($join) {
            $join->on('IndikatorSubKegiatan_90.id_renja','Renja_90.id_renja')
            ->on('Subkegiatan_90.id_sub_kegiatan','IndikatorSubKegiatan_90.id_sub_kegiatan');
        })->leftjoin('R_IndikatorSubKegiatan_90','R_IndikatorSubKegiatan_90.id_indikator_subkegiatan','IndikatorSubKegiatan_90.id_indikator_subkegiatan')
        ->where('LogRenja_90.isPerubahan',0)->where('IndikatorSubKegiatan_90.is_added','<=',4)->where('IndikatorSubKegiatan_90.is_emptied','!=',4)
        ->when($this->periode_usulan, function ($q) {
            return $q->where('Renja_90.periode_usulan', $this->periode_usulan);
        })->when($this->id_skpd, function ($q) {
            return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
        })
        ->groupBy('SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();

        $data_perubahan = LogRenja_90::
        selectRaw('SKPD_90.id_skpd,SKPD_90.nama_skpd,COUNT(DISTINCT(SubKegiatan_90.id_sub_kegiatan)) AS jml_subkegiatan_perubahan,
        COUNT(DISTINCT (CASE WHEN IndikatorSubKegiatan_90.indikator_subkegiatan IS NOT NULL THEN SubKegiatan_90.id_sub_kegiatan END)) AS jml_subkegiatan_havind_perubahan,
        COUNT(DISTINCT CASE WHEN IndikatorSubKegiatan_90.indikator_subkegiatan IS NOT NULL THEN IndikatorSubKegiatan_90.id_indikator_subkegiatan END) AS jml_ind_sub_kegiatan_perubahan,
        COUNT(DISTINCT (CASE WHEN R_IndikatorSubKegiatan_90.t_4 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS jml_ind_subkeg4_havemonev_perubahan')
        ->join('Subkegiatan_90','Subkegiatan_90.id_sub_kegiatan','LogRenja_90.id_sub_kegiatan')
        ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','Subkegiatan_90.id_kegiatan')
        ->join('Program_90','Program_90.id_program','Kegiatan_90.id_program')
        ->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
        ->join('SKPD_90','SKPD_90.id_skpd','Renja_90.id_skpd')
        ->join('Bidang','Bidang.id_bidang','SKPD_90.id_bidang')
        ->leftjoin('IndikatorSubKegiatan_90', function ($join) {
            $join->on('IndikatorSubKegiatan_90.id_renja','Renja_90.id_renja')
            ->on('Subkegiatan_90.id_sub_kegiatan','IndikatorSubKegiatan_90.id_sub_kegiatan');
        })->leftjoin('R_IndikatorSubKegiatan_90','R_IndikatorSubKegiatan_90.id_indikator_subkegiatan','IndikatorSubKegiatan_90.id_indikator_subkegiatan')
        ->where('LogRenja_90.isPerubahan',1)->where('IndikatorSubKegiatan_90.is_emptied',0)
        ->when($this->periode_usulan, function ($q) {
            return $q->where('Renja_90.periode_usulan', $this->periode_usulan);
        })->when($this->id_skpd, function ($q) {
            return $q->whereIn('SKPD_90.id_skpd',$this->id_skpd);
        })
        ->groupBy('SKPD_90.id_skpd','SKPD_90.nama_skpd')
        ->get();

        if(count($data) > 0){
            foreach($data as $value){
                if(count($data_perubahan) > 0){
                    foreach($data_perubahan as  $val){
                        if($val['id_skpd'] == $value['id_skpd']){
                            $value['jml_subkegiatan_perubahan'] = $val['jml_subkegiatan_perubahan'];
                            $value['jml_subkegiatan_havind_perubahan'] = $val['jml_subkegiatan_havind_perubahan'];
                            $value['jml_ind_sub_kegiatan_perubahan'] = $val['jml_ind_sub_kegiatan_perubahan'];
                            $value['jml_ind_subkeg4_havemonev_perubahan'] = $val['jml_ind_subkeg4_havemonev_perubahan'];
                        }
                    }
                }else{
                    $value['jml_subkegiatan_perubahan'] = 0;
                    $value['jml_subkegiatan_havind_perubahan'] = 0;
                    $value['jml_ind_sub_kegiatan_perubahan'] = 0;
                    $value['jml_ind_subkeg4_havemonev_perubahan'] = 0;
                }
            }    
        }

        return response()->json([
            'data' => $data,
        ]);

        return API_RENJAResource::collection($data);
    }
    public function getRekapInputAll(Request $request)
    {
        $skpd = SKPD_90::whereNotIn('id_skpd',[102,34])->get();
        $data = null;

        foreach($skpd as $pd)
        {
            // rekap input program
            $rprog_prb = DB::select("                
                SELECT
                    COUNT(DISTINCT TargetRealisasiIKP.id_indikator_program) AS jml_indikator,
                    COUNT(DISTINCT(CASE WHEN TargetRealisasiIKP.triwulan = 4 AND TargetRealisasiIKP.realisasi IS NOT NULL THEN TargetRealisasiIKP.id_target_realisasi END)) AS tw4_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                JOIN ProgramRPJMD ON ProgramRPJMD.id_program = Program_90.id_program_rpjmd
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN Tahun ON Tahun.tahun = Renja_90.periode_usulan
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_program = ProgramRPJMD.id_program
                    AND IndikatorProgramRPJMD.id_skpd = Renja_90.id_skpd
                JOIN TargetRealisasiIKP ON TargetRealisasiIKP.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                    AND TargetRealisasiIKP.id_tahun = Tahun.id_tahun
                WHERE LogRenja_90.isPerubahan = 1
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );
            $rprog = DB::select("                
                SELECT
                    COUNT(DISTINCT TargetRealisasiIKP.id_indikator_program) AS jml_indikator,
                    COUNT(DISTINCT(CASE WHEN TargetRealisasiIKP.triwulan = 1 AND TargetRealisasiIKP.realisasi IS NOT NULL THEN TargetRealisasiIKP.id_target_realisasi END)) AS tw1_diisi,
                    COUNT(DISTINCT(CASE WHEN TargetRealisasiIKP.triwulan = 2 AND TargetRealisasiIKP.realisasi IS NOT NULL THEN TargetRealisasiIKP.id_target_realisasi END)) AS tw2_diisi,
                    COUNT(DISTINCT(CASE WHEN TargetRealisasiIKP.triwulan = 3 AND TargetRealisasiIKP.realisasi IS NOT NULL THEN TargetRealisasiIKP.id_target_realisasi END)) AS tw3_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                JOIN ProgramRPJMD ON ProgramRPJMD.id_program = Program_90.id_program_rpjmd
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN Tahun ON Tahun.tahun = Renja_90.periode_usulan
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_program = ProgramRPJMD.id_program
                    AND IndikatorProgramRPJMD.id_skpd = Renja_90.id_skpd
                JOIN TargetRealisasiIKP ON TargetRealisasiIKP.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                    AND TargetRealisasiIKP.id_tahun = Tahun.id_tahun
                WHERE LogRenja_90.isPerubahan = 0
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );
            // rekap input kegiatan
            $rkg_prb = DB::select("                
                SELECT
                    COUNT(DISTINCT IndikatorKegiatan_90.id_indikator_kegiatan) AS jml_indikator_perubahan,
                    COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_4 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS tw4_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                JOIN IndikatorKegiatan_90 ON IndikatorKegiatan_90.id_kegiatan = Kegiatan_90.id_kegiatan
                    AND IndikatorKegiatan_90.id_renja = LogRenja_90.id_renja
                JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = IndikatorKegiatan_90.id_indikator_kegiatan
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                WHERE LogRenja_90.isPerubahan = 1
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );
            $rkg = DB::select("                
                SELECT
                    COUNT(DISTINCT IndikatorKegiatan_90.id_indikator_kegiatan) AS jml_indikator,
                    COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_1 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS tw1_diisi,
                    COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_2 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS tw2_diisi,
                    COUNT(DISTINCT (CASE WHEN R_IndikatorKegiatan_90.t_3 IS NOT NULL THEN R_IndikatorKegiatan_90.id_r_indikator_kegiatan END)) AS tw3_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                JOIN IndikatorKegiatan_90 ON IndikatorKegiatan_90.id_kegiatan = Kegiatan_90.id_kegiatan
                    AND IndikatorKegiatan_90.id_renja = LogRenja_90.id_renja
                JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = IndikatorKegiatan_90.id_indikator_kegiatan
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                WHERE LogRenja_90.isPerubahan = 0
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );

            // Rekap Sub Kegiatan
            $rskg_prb = DB::select("
                SELECT	
                    COUNT(IndikatorSubKegiatan_90.id_indikator_subkegiatan) AS jml_indikator_perubahan,
                    COUNT(DISTINCT(CASE WHEN R_IndikatorSubKegiatan_90.t_4 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS tw4_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN IndikatorSubKegiatan_90 ON IndikatorSubKegiatan_90.id_sub_kegiatan = SubKegiatan_90.id_sub_kegiatan
                    AND IndikatorSubKegiatan_90.id_renja = LogRenja_90.id_renja
                JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = IndikatorSubKegiatan_90.id_indikator_subkegiatan
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                WHERE LogRenja_90.isPerubahan = 1
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );
            $rskg = DB::select("
                SELECT
                    COUNT(IndikatorSubKegiatan_90.id_indikator_subkegiatan) AS jml_indikator,
                    COUNT(DISTINCT(CASE WHEN R_IndikatorSubKegiatan_90.t_1 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS tw1_diisi,
                    COUNT(DISTINCT(CASE WHEN R_IndikatorSubKegiatan_90.t_2 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS tw2_diisi,
                    COUNT(DISTINCT(CASE WHEN R_IndikatorSubKegiatan_90.t_3 IS NOT NULL THEN R_IndikatorSubKegiatan_90.id_r_indikator_subkegiatan END)) AS tw3_diisi
                FROM LogRenja_90
                JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                JOIN IndikatorSubKegiatan_90 ON IndikatorSubKegiatan_90.id_sub_kegiatan = SubKegiatan_90.id_sub_kegiatan
                    AND IndikatorSubKegiatan_90.id_renja = LogRenja_90.id_renja
                JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = IndikatorSubKegiatan_90.id_indikator_subkegiatan
                JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                WHERE LogRenja_90.isPerubahan = 0
                    AND SKPD_90.id_skpd = ?
                    AND Renja_90.periode_usulan = ?
            ", array(
                    $pd->id_skpd, 
                    $request->periode_usulan
                )
            );

            $data[] = array(
                'id_skpd' => $pd->id_skpd,
                'nama_skpd' => $pd->nama_skpd,
                'rekap_program' => $rprog,
                'rekap_program_perubahan' => $rprog_prb,
                'rekap_kegiatan' => $rkg,
                'rekap_kegiatan_perubahan' => $rkg_prb,
                'rekap_sub_kegiatan' => $rskg,
                'rekap_sub_kegiatan_perubahan' => $rskg_prb
            );        
        }

        return response()->json([
            'data' => $data,
        ]);
    }
    public function getRekapAll(Request $request)
    {
        $this->periode_usulan = $request->input('periode_usulan');

        $skpd = SKPD_90::whereNotIn('id_skpd',[102,34])->get();
        $rekapinput = null;
        foreach($skpd as $pd)
        {
            $param = new Request();
            $param->merge([                
                'periode_usulan' => $this->periode_usulan,
                'id_pd' => $pd->id_skpd
            ]);
            $rekapinput[] = array(
                'id_skpd' => $pd->id_skpd,
                'nama_skpd' => $pd->nama_skpd,
                'rekap_program' => $this->getRekapInputProgram($param),
                'rekap_kegiatan' => $this->getRekapInputKegiatan($param),
                'rekap_sub_kegiatan' => $this->getRekapInputSubKegiatan($param)
            );
        }
        
        return response()->json([
            'data' => $rekapinput,
        ]);
    } 
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\AnalisisFaktorProgram;
use App\Http\Resources\Api\API_SimrendaResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnalisisFaktorProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = AnalisisFaktorProgram::with('analisator.jafung','renja.skpd')->get();
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        //define validation rules
        $validator = Validator::make($request->all(), [
            'id_renja'      => 'required',
            'id_program_rpjmd'    => 'required',
            'triwulan'      => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        //create
        $data = AnalisisFaktorProgram::create([
            'id_renja'          => $request->id_renja,
            'id_program_rpjmd'        => $request->id_program_rpjmd,
            'id_analisator'     => $request->id_analisator,
            'faktor_pendorong'  => $request->faktor_pendorong,
            'faktor_penghambat' => $request->faktor_penghambat,
            'tindak_lanjut'     => $request->tindak_lanjut,
            'triwulan'          => $request->triwulan,
            'keterangan_cp'     => $request->keterangan_cp
        ]);
        if($data)
        {
            $hasil = new JsonResponse(response()->json([
                    'status' => 200,
                    'message' => 'Data berhasil ditambah!',
                    'data' => $data
                ])
            );
        }else{
            $hasil = new JsonResponse(response()->json([
                    'status' => 500,
                    'message' => 'Data gagal ditambah!',
                    'data' => $data
                ])
            );
        }
        return $hasil;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = AnalisisFaktorProgram::with('analisator.jafung','renja.skpd')->where('id_faktor_program',$id)->get();
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'id_renja'      => 'required',
            'id_program_rpjmd'    => 'required',
            'triwulan'      => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find user by ID
        $data = AnalisisFaktorProgram::find($id);

        //update
        $data->update([
            'id_renja'          => $request->id_renja,
            'id_program_rpjmd'        => $request->id_program_rpjmd,
            'id_analisator'     => $request->id_analisator,
            'faktor_pendorong'  => $request->faktor_pendorong,
            'faktor_penghambat' => $request->faktor_penghambat,
            'tindak_lanjut'     => $request->tindak_lanjut,
            'triwulan'          => $request->triwulan,
            'keterangan_cp'     => $request->keterangan_cp
        ]);
        
        if($data)
        {
            $hasil = new JsonResponse(response()->json([
                    'status' => 200,
                    'message' => 'Data berhasil dirubah!',
                    'data' => $data
                ])
            );
        }else{
            $hasil = new JsonResponse(response()->json([
                    'status' => 500,
                    'message' => 'Data gagal dirubah!',
                    'data' => $data
                ])
            );
        }
        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AnalisisFaktorProgram::find($id);
        $data->delete();
        if($data)
        {
            $hasil = new JsonResponse(response()->json([
                    'status' => 200,
                    'message' => 'Data berhasil dihapus!',
                    'data' => $data
                ])
            );
        }else{
            $hasil = new JsonResponse(response()->json([
                    'status' => 500,
                    'message' => 'Data gagal dihapus!',
                    'data' => $data
                ])
            );
        }
        return $hasil;
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Api\API_SimrendaResource;
use Session;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Api_GrafikMonevController extends Controller
{
    //grafik capaian
    public function capaianPerPD()
    {
        
    }

    //grafik anggaran
    public function anggaranPerPD()
    {        
        $angPD = DB::table('R_LogRenja_90')
                    ->select(
                            'nama_skpd',
                            DB::raw('SUM(r_1) AS total_r_1'),
                            DB::raw('SUM(r_2) AS total_r_2'),
                            DB::raw('SUM(r_3) AS total_r_3'),
                            DB::raw('SUM(r_4) AS total_r_4'),
                            DB::raw('(SUM(r_1)+SUM(r_2)+SUM(r_3)+SUM(r_4)) AS total_realisasi'),
                            'periode_usulan'
                        )
                    ->join('LogRenja_90', 'LogRenja_90.id_log_renja', '=', 'R_LogRenja_90.id_log_renja')
                    ->join('SubKegiatan_90', 'SubKegiatan_90.id_sub_kegiatan', '=', 'LogRenja_90.id_sub_kegiatan')
                    ->join('Kegiatan_90', 'Kegiatan_90.id_kegiatan', '=', 'SubKegiatan_90.id_kegiatan')
                    ->join('Renja_90', 'Renja_90.id_renja', '=', 'LogRenja_90.id_renja')
                    ->join('SKPD_90', 'SKPD_90.id_skpd', '=', 'Renja_90.id_skpd')
                    ->groupBy('nama_skpd')
                    ->groupBy('periode_usulan')
                    ->orderBy('periode_usulan', 'ASC')
                    ->orderBy('nama_skpd', 'ASC')
                    ->get();

        $PD = DB::table('R_LogRenja_90')
                ->select('nama_skpd')
                ->join('LogRenja_90', 'LogRenja_90.id_log_renja', '=', 'R_LogRenja_90.id_log_renja')
                ->join('SubKegiatan_90', 'SubKegiatan_90.id_sub_kegiatan', '=', 'LogRenja_90.id_sub_kegiatan')
                ->join('Kegiatan_90', 'Kegiatan_90.id_kegiatan', '=', 'SubKegiatan_90.id_kegiatan')
                ->join('Renja_90', 'Renja_90.id_renja', '=', 'LogRenja_90.id_renja')
                ->join('SKPD_90', 'SKPD_90.id_skpd', '=', 'Renja_90.id_skpd')
                ->groupBy('nama_skpd')
                ->orderBy('nama_skpd', 'ASC')
                ->get();
                
        $tahunPD = DB::table('R_LogRenja_90')
                    ->select(
                            'periode_usulan'
                        )
                    ->join('LogRenja_90', 'LogRenja_90.id_log_renja', '=', 'R_LogRenja_90.id_log_renja')
                    ->join('SubKegiatan_90', 'SubKegiatan_90.id_sub_kegiatan', '=', 'LogRenja_90.id_sub_kegiatan')
                    ->join('Kegiatan_90', 'Kegiatan_90.id_kegiatan', '=', 'SubKegiatan_90.id_kegiatan')
                    ->join('Renja_90', 'Renja_90.id_renja', '=', 'LogRenja_90.id_renja')
                    ->join('SKPD_90', 'SKPD_90.id_skpd', '=', 'Renja_90.id_skpd')
                    ->groupBy('periode_usulan')
                    ->orderBy('periode_usulan', 'ASC')
                    ->get();
        $dataAng = null;
        foreach($PD as $kPD => $vPD)
        {
            foreach($angPD as $kangPD => $valangPD)
            {
                if($vPD->nama_skpd == $valangPD->nama_skpd)
                {
                    if(!is_null($valangPD->total_realisasi))
                    {
                        $dataAng[$vPD->nama_skpd][] = $valangPD->total_realisasi;
                    }else
                    {
                        $dataAng[$vPD->nama_skpd][] = 0;
                    }
                }
            }
        }
        $dataTahun = null;
        foreach($tahunPD as $tPD => $vtPD)
        {
            $dataTahun[] = "Tahun ".$vtPD->periode_usulan;
        }
        return compact('dataAng','dataTahun');
        // return $angPD;
        // return $PD;
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use App\Http\Resources\Api\API_RENJAResource;
use App\Models\Renja\ViewSubKegiatanRenja;
use App\Models\Renja\ViewKegiatanRenja;
use App\Models\Renja\ViewProgramRenja;
use App\Models\Renja\ViewUrusanRenja;
use App\Models\AnalisisKinerjaBidangUrusan;
use App\Models\TindakLanjutBidangUrusan;
use App\Models\AnalisatorSubKegiatan;
use App\Models\FaktorPendorongSubKegiatan;
use App\Models\FaktorPenghambatSubKegiatan;
use App\Models\TindakLanjutSubKegiatan;
use App\Models\Urusan_90;
use App\Models\LogRenja_90;
use App\Models\R_LogRenja_90;
use App\Models\Kegiatan_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\R_IndikatorKegiatan_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\R_IndikatorSubKegiatan_90;
use App\Models\IndikatorProgramRPJMD;
use App\Models\TargetRealisasiIKP;
use App\Models\Tahun;
use App\Models\SKPD_90;
use App\Models\Satuan;
use App\Models\KategoriPersentase;
use App\Models\Program_90;
use App\Models\ProgramRPJMD;
use App\Models\TargetRealisasiProgramV2;

use Session;
use Auth;
use Validator;

use App\Models\CascadingKegiatan_90;
use App\Models\CascadingSubKegiatan_90;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Database\Eloquent\Builder;

class Api_RENJAController extends Controller
{
    // Data Sekretariat Daerah
    public function getMonevSekDa()
    {
        
    }
    //Monev All
    public function getMonevAll(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_skpd' => 'required',
            'tahun' => 'required',
            'isPerubahan' => 'required'
        ]);
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $d_renja = Renja_90::with('logrenja.subkegiatan.kegiatan.program')
                    ->where([
                        ['id_skpd',$request->id_skpd],
                        ['periode_usulan',$request->tahun]
                    ])->get();
            
            $tamp_prog = array();
            $tamp_keg = array();
            $tamp_subkeg = array();
            $data = null;
            foreach($d_renja as $renja)
            {
                foreach($renja->logrenja as $lrenja)
                {
                    // cek data perubahan atau bukan
                    if($lrenja->isPerubahan == $request->isPerubahan)
                    {
                        // tampung data program
                        // tamp_prog[id_program]
                        $tamp_prog[$lrenja->subkegiatan->kegiatan->program->id_program] = array(
                            $lrenja->subkegiatan->kegiatan->program
                        );
                        // tampung data kegiatan
                        // tamp_keg[id_program][id_kegiatan]
                        $tamp_keg[$lrenja->subkegiatan->kegiatan->program->id_program]
                        [$lrenja->subkegiatan->kegiatan->id_kegiatan] = array(
                            "id_kegiatan" => $lrenja->subkegiatan->kegiatan->id_kegiatan,
                            "id_program" => $lrenja->subkegiatan->kegiatan->id_program,
                            "kode_kegiatan" => $lrenja->subkegiatan->kegiatan->kode_kegiatan,
                            "nama_kegiatan" => $lrenja->subkegiatan->kegiatan->nama_kegiatan,
                            "isDeleted" => $lrenja->subkegiatan->kegiatan->isDeleted,
                            "created_at" => $lrenja->subkegiatan->kegiatan->created_at,
                            "updated_at" => $lrenja->subkegiatan->kegiatan->updated_at
                        );
                        // tampung data sub kegiatan
                        // tamp_subkeg[id_program][id_kegiatan][id_sub_kegiatan]
                        $tamp_subkeg[$lrenja->subkegiatan->kegiatan->program->id_program]
                        [$lrenja->subkegiatan->kegiatan->id_kegiatan]
                        [$lrenja->subkegiatan->id_sub_kegiatan] = array(                        
                            "id_sub_kegiatan" => $lrenja->subkegiatan->id_sub_kegiatan,
                            "id_kegiatan" => $lrenja->subkegiatan->id_kegiatan,
                            "kode_sub_kegiatan" => $lrenja->subkegiatan->kode_sub_kegiatan,
                            "nama_sub_kegiatan" => $lrenja->subkegiatan->nama_sub_kegiatan,
                            "isDeleted" => $lrenja->subkegiatan->isDeleted,
                            "created_at" => $lrenja->subkegiatan->created_at,
                            "updated_at" => $lrenja->subkegiatan->updated_at,
                            "logrenja" => array(
                                "id_log_renja" =>$lrenja->id_log_renja,
                                "id_sub_kegiatan" =>$lrenja->id_sub_kegiatan,
                                "id_renja" =>$lrenja->id_renja,
                                "apbd_kota" =>$lrenja->apbd_kota,
                                "isDeleted" =>$lrenja->isDeleted,
                                "created_at" =>$lrenja->created_at,
                                "updated_at" =>$lrenja->updated_at,
                                "isPerubahan" =>$lrenja->isPerubahan,
                                "isSPM" =>$lrenja->isSPM,
                                "isSDGS" =>$lrenja->isSDGS,
                                "renja" => array(
                                    "id_renja"=>$renja->id_renja,
                                    "id_skpd"=>$renja->id_skpd,
                                    "periode_usulan"=>$renja->periode_usulan,
                                    "isDeleted"=>$renja->isDeleted,
                                    "created_at"=>$renja->created_at,
                                    "updated_at"=>$renja->updated_at
                                )
                            )              
                        );
                    }
                }
            }

            // proses mengubah data tampung ke format: 
            // program
            // --> kegiatan
            //  ---> subkegiatan
            foreach($tamp_prog as $kprog => $prog)
            {
                $d_keg = null; //set data kegiatan
                foreach($tamp_keg[$kprog] as $kkeg => $keg) //loop data kegiatan sesuai indeks id_program
                {
                    $d_subkeg = null; //set data sub kegiatan
                    foreach($tamp_subkeg[$kprog][$kkeg] as $subkeg) //loop data sub kegiatan sesuai indeks id_program dan id_kegiatan
                    {
                        $d_subkeg[] = $subkeg; //tampung data sub kegiatan
                    }
                    // tampung data kegiatan beserta data sub kegiatan nya
                    $d_keg[] = array(
                        "id_kegiatan" => $keg['id_kegiatan'],
                        "id_program" => $keg['id_program'],
                        "kode_kegiatan" => $keg['kode_kegiatan'],
                        "nama_kegiatan" => $keg['nama_kegiatan'],
                        "isDeleted" => $keg['isDeleted'],
                        "created_at" => $keg['created_at'],
                        "updated_at" => $keg['updated_at'],
                        "subkegiatan" => $d_subkeg
                    );
                }
                // masukkan data program dan kegiatan yang telah berisi data sub kegiatan
                $data[] = array(
                    "id_program" => $prog[0]['id_program'],
                    "id_bidang_urusan" => $prog[0]['id_bidang_urusan'],
                    "kode_program" => $prog[0]['kode_program'],
                    "nama_program" => $prog[0]['nama_program'],
                    "isDeleted" => $prog[0]['isDeleted'],
                    "created_at" => $prog[0]['created_at'],
                    "updated_at" => $prog[0]['updated_at'],
                    "id_program_rpjmd" => $prog[0]['id_program_rpjmd'],
                    "kegiatan" => $d_keg
                );
            }
            return API_RENJAResource::collection($data);
        }
    }
    
    //Monev Realisasi Anggaran
    public function getRealisasiAnggaran(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_skpd' => 'required',
            'tahun' => 'required',
            'id_sub_kegiatan' => 'required',
            'isPerubahan' => 'required'
        ]);
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $data = LogRenja_90::with('renja','realisasi')
                                ->whereHas('renja',function($query) use($request) {
                                    $query->where([
                                                ['id_skpd','=',$request->id_skpd],
                                                ['periode_usulan','=',$request->tahun]
                                    ]);
                                })
                                ->where([
                                        ['id_sub_kegiatan','=',$request->id_sub_kegiatan],
                                        ['isPerubahan','=',$request->isPerubahan]
                                    ])
                                ->get();

            return API_RENJAResource::collection($data);
        }
    }
    //Monev Simpan Realisasi Anggaran
    public function saveRealisasiAnggaran(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_log_renja' => 'required',
            'triwulan' => 'required',
            'realisasi' => 'required'
        ]);
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $data = R_LogRenja_90::where('id_log_renja', $request->id_log_renja)->firstOrFail();        

            if($request->triwulan == 1)
            {
                $data->r_1 = $request->realisasi;
            }else if($request->triwulan == 2)
            {
                $data->r_2 = $request->realisasi;
            }else if($request->triwulan == 3)
            {
                $data->r_3 = $request->realisasi;
            }else if($request->triwulan == 4)
            {
                $data->r_4 = $request->realisasi;
            }

            $data->timestamps = false;
            if($data->save()){
                return new JsonResponse(response()->json([
                        'status' => 200,
                        'message' => 'Anggaran berhasil disimpan!',
                        'data' => $data
                    ])
                );
            }else{
                return new JsonResponse(response()->json([
                        'status' => 500,
                        'message' => 'Anggaran gagal disimpan!',
                        'data' => $data
                    ])
                );
            }
        }
    }
    //Subkegiatan
    public function getSubKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $subkeg = $request->subkeg;

        if(Auth::user()->id_role == 4){
            $opd = Auth::user()->viewuser->id_skpd;          
        }
        
        $data = ViewSubKegiatanRenja::with(['kegiatan.program'])
        ->where('periode_usulan', $tahun)
        ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
        ->where('id_skpd', $opd)
        ->when($subkeg, function ($query) use ($subkeg) {
            return $query->where('id_sub_kegiatan', $subkeg);
        })
        ->orderBy('ViewSubKegiatanRenja.kode_sub_kegiatan')
        ->get();
        
        return API_RENJAResource::collection($data);
    }

    public function getSubKegiatansdgs(Request $request)
    {
        $tahun = $request->periode_usulan;
        $opd = $request->id_pd;
        $subkeg = $request->nama_subkegiatan;
        $prog = $request->nama_program;
        $keg = $request->nama_kegiatan;
        
        $data = ViewSubKegiatanRenja::
        select('ViewSubKegiatanRenja.*')
        ->with(['kegiatan.program'])
        ->join('Kegiatan_90','Kegiatan_90.id_kegiatan','ViewSubKegiatanRenja.id_kegiatan')
        ->join('Program_90','Program_90.id_program','Kegiatan_90.id_program')
        //->leftjoin('SDGS_Coding_90','SDGS_Coding_90.id_log_renja','ViewSubKegiatanRenja.id_log_renja')
        ->where('periode_usulan', $tahun)
        ->where('isPerubahan', 1)
        ->where('id_skpd', $opd)
        ->when($subkeg, function ($query) use ($subkeg) {
            return $query->where('nama_sub_kegiatan', $subkeg);
        })
        ->when($keg, function ($query) use ($keg) {
            return $query->where('nama_kegiatan', $keg);
        })
        ->when($prog, function ($query) use ($prog) {
            return $query->where('nama_program', $prog);
        })
        //->orderBy('SDGS_Coding_90.id_log_renja','desc')
        ->orderBy('ViewSubKegiatanRenja.kode_sub_kegiatan')
        ->get();

        return API_RENJAResource::collection($data);
    }

    //Kegiatan
    public function getKegiatan(Request $request)
    {   
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $keg = $request->keg;

        if(Auth::user()->id_role == 4){
            $opd = Auth::user()->viewuser->id_skpd;          
        }
            
        $data = ViewKegiatanRenja::with(['program'])
        ->where('periode_usulan', $tahun)
        ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
        ->where('id_skpd', $opd)
        ->when($keg, function ($query) use ($keg) {
            return $query->where('id_kegiatan', $keg);
        })->orderBy('ViewKegiatanRenja.kode_kegiatan')
        ->get();

        return API_RENJAResource::collection($data);
    }

    //Program
    public function getProgram(Request $request)
    {   
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $prog = $request->prog;

        if(Auth::user()->id_role == 4){
            $opd = Auth::user()->viewuser->id_skpd;          
        }

        $data = ViewProgramRenja::with(['bidangurusan','program'])
        ->where('periode_usulan', $tahun)
        ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
        ->where('id_skpd', $opd)
        ->when($prog, function ($query) use ($prog) {
            return $query->where('id_program', $prog);
        })->orderBy('ViewProgramRenja.kode_program')
        ->get();

        return API_RENJAResource::collection($data);
    }

    //IndikatorSubKegiatan
    public function getIndikatorSubKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $subkeg = $request->subkeg;
        $format = $request->format;
        $all = $request->all;

        if($all == true)
        {
            $opd = SKPD_90::where('id_skpd','!=',34)
                            ->orderBy('kode_skpd')
                            ->orderBy('nama_skpd')
                            ->get();
            
            $index_1 = 0;
            foreach($opd as $pd)
            {
                $data[$index_1] = ViewSubKegiatanRenja::with(['analisisfaktor.analisator.jafung','kegiatan.program','indikator' => function ($query) {
                    $query->with('realisasiindikatorsubkeg');
                }])
                ->where('periode_usulan', $tahun)
                ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
                ->where('id_skpd', $pd->id_skpd)
                ->when($subkeg, function ($query) use ($subkeg) {
                    return $query->where('id_sub_kegiatan', $subkeg);
                })->get();

                if(count($data[$index_1]) > 0)
                {
                    $index_1++;
                }
            }

            foreach($data as $idx => $dat)
            {
                $indexInd = 0;
                foreach($dat as $cek)
                {
                    $databaru[$idx][$indexInd] = array(
                        'id_sub_kegiatan' => $cek->id_sub_kegiatan,
                        'id_kegiatan' => $cek->id_kegiatan,
                        'kode_sub_kegiatan' => $cek->kode_sub_kegiatan,
                        'nama_sub_kegiatan' => $cek->nama_sub_kegiatan,
                        'isDeleted' => $cek->isDeleted,
                        'id_skpd' => $cek->id_skpd,
                        'nama_skpd' => $cek->nama_skpd,
                        'id_renja' => $cek->id_renja,
                        'apbd_kota' => $cek->apbd_kota,
                        'r_1' => $cek->r_1,
                        'r_2' => $cek->r_2,
                        'r_3' => $cek->r_3,
                        'r_4' => $cek->r_4,
                        'isPerubahan' => $cek->isPerubahan,
                        'periode_usulan' => $cek->periode_usulan,
                        'analisisfaktor' => array(
                            0 => null,
                            1 => null,
                            2 => null,
                            3 => null
                        ),
                        'kegiatan' => $cek->kegiatan,
                        'indikator' => $cek->indikator
                    );
                    foreach($cek->analisisfaktor as $afk)
                    {
                        if($afk->triwulan == 1)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][0] = $afk;
                        }else if($afk->triwulan == 2)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][1] = $afk;
                        }else if($afk->triwulan == 3)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][2] = $afk;
                        }else if($afk->triwulan == 4)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][3] = $afk;
                        }
                    }
                    $indexInd++;
                }
            }
        }else{
            if(Auth::user()->id_role == 4){
                $opd = Auth::user()->viewuser->id_skpd;          
            }
            
            $data = ViewSubKegiatanRenja::with(['analisisfaktor.analisator.jafung','kegiatan.program','indikator' => function ($query) {
                $query->with('realisasiindikatorsubkeg');
            }])
            ->where('periode_usulan', $tahun)
            ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
            ->where('id_skpd', $opd)
            ->when($subkeg, function ($query) use ($subkeg) {
                return $query->where('id_sub_kegiatan', $subkeg);
            })->get();
            $databaru = null;
            $indexInd = 0;
            foreach($data as $cek)
            {
                $databaru[$indexInd] = array(
                    'id_sub_kegiatan' => $cek->id_sub_kegiatan,
                    'id_kegiatan' => $cek->id_kegiatan,
                    'kode_sub_kegiatan' => $cek->kode_sub_kegiatan,
                    'nama_sub_kegiatan' => $cek->nama_sub_kegiatan,
                    'isDeleted' => $cek->isDeleted,
                    'id_skpd' => $cek->id_skpd,
                    'nama_skpd' => $cek->nama_skpd,
                    'id_renja' => $cek->id_renja,
                    'apbd_kota' => $cek->apbd_kota,
                    'r_1' => $cek->r_1,
                    'r_2' => $cek->r_2,
                    'r_3' => $cek->r_3,
                    'r_4' => $cek->r_4,
                    'isPerubahan' => $cek->isPerubahan,
                    'periode_usulan' => $cek->periode_usulan,
                    'analisisfaktor' => array(
                        0 => null,
                        1 => null,
                        2 => null,
                        3 => null
                    ),
                    'kegiatan' => $cek->kegiatan,
                    'indikator' => $cek->indikator
                );
                foreach($cek->analisisfaktor as $afk)
                {
                    if($afk->triwulan == 1)
                    {
                        $databaru[$indexInd]['analisisfaktor'][0] = $afk;
                    }else if($afk->triwulan == 2)
                    {
                        $databaru[$indexInd]['analisisfaktor'][1] = $afk;
                    }else if($afk->triwulan == 3)
                    {
                        $databaru[$indexInd]['analisisfaktor'][2] = $afk;
                    }else if($afk->triwulan == 4)
                    {
                        $databaru[$indexInd]['analisisfaktor'][3] = $afk;
                    }
                }
                $indexInd++;
            }
        }
        

        if($format == 'baru')
        {
            return API_RENJAResource::collection($databaru);
        }else
        {
            return API_RENJAResource::collection($data);  
        }
    }
    
    //IndikatorKegiatan
    public function getIndikatorKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $keg = $request->keg;
        $format  = $request->format;
        $all = $request->all;

        if($all == true)
        {
            $opd = SKPD_90::where('id_skpd','!=',34)
                            ->orderBy('kode_skpd')
                            ->orderBy('nama_skpd')
                            ->get();
            $index_1 = 0;
            foreach($opd as $pd)
            {
                $data[$index_1] = ViewKegiatanRenja::with(['analisisfaktor.analisator.jafung','program','indikator' => function ($query) {
                    $query->with('realisasiindikatorkeg');
                }])
                ->where('periode_usulan', $tahun)
                ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
                ->where('id_skpd', $pd->id_skpd)
                ->when($keg, function ($query) use ($keg) {
                    return $query->where('id_kegiatan', $keg);
                })->get();

                if(count($data[$index_1]) > 0)
                {
                    $index_1++;
                }
            }
            $databaru = null;
            foreach($data as $idx => $dat)
            {
                $indexInd = 0;
                foreach($dat as $cek)
                {
                    $databaru[$idx][$indexInd] = array(
                        'id_kegiatan' => $cek->id_kegiatan,
                        'id_program' => $cek->id_program,
                        'kode_kegiatan' => $cek->kode_kegiatan,
                        'nama_kegiatan' => $cek->nama_kegiatan,
                        'isDeleted' => $cek->isDeleted,
                        'id_skpd' => $cek->id_skpd,
                        'nama_skpd' => $cek->nama_skpd,
                        'id_renja' => $cek->id_renja,
                        'apbd_kota' => $cek->apbd_kota,
                        'r_1' => $cek->r_1,
                        'r_2' => $cek->r_2,
                        'r_3' => $cek->r_3,
                        'r_4' => $cek->r_4,
                        'isPerubahan' => $cek->isPerubahan,
                        'periode_usulan' => $cek->periode_usulan,
                        'analisisfaktor' => array(
                            0 => null,
                            1 => null,
                            2 => null,
                            3 => null
                        ),
                        'program' => $cek->program,
                        'indikator' => $cek->indikator
                    );
                    foreach($cek->analisisfaktor as $afk)
                    {
                        if($afk->triwulan == 1)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][0] = $afk;
                        }else if($afk->triwulan == 2)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][1] = $afk;
                        }else if($afk->triwulan == 3)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][2] = $afk;
                        }else if($afk->triwulan == 4)
                        {
                            $databaru[$idx][$indexInd]['analisisfaktor'][3] = $afk;
                        }
                    }
                    $indexInd++;
                }
            }
        }else{
            if(Auth::user()->id_role == 4){
                $opd = Auth::user()->viewuser->id_skpd;          
            }
            
            $data = ViewKegiatanRenja::with(['analisisfaktor.analisator.jafung','program','indikator' => function ($query) {
                $query->with('realisasiindikatorkeg');
            }])
            ->where('periode_usulan', $tahun)
            ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
            ->where('id_skpd', $opd)
            ->when($keg, function ($query) use ($keg) {
                return $query->where('id_kegiatan', $keg);
            })->get();
    
            $databaru = null;
            $indexInd = 0;
            foreach($data as $cek)
            {
                $databaru[$indexInd] = array(
                    'id_kegiatan' => $cek->id_kegiatan,
                    'id_program' => $cek->id_program,
                    'kode_kegiatan' => $cek->kode_kegiatan,
                    'nama_kegiatan' => $cek->nama_kegiatan,
                    'isDeleted' => $cek->isDeleted,
                    'id_skpd' => $cek->id_skpd,
                    'nama_skpd' => $cek->nama_skpd,
                    'id_renja' => $cek->id_renja,
                    'apbd_kota' => $cek->apbd_kota,
                    'r_1' => $cek->r_1,
                    'r_2' => $cek->r_2,
                    'r_3' => $cek->r_3,
                    'r_4' => $cek->r_4,
                    'isPerubahan' => $cek->isPerubahan,
                    'periode_usulan' => $cek->periode_usulan,
                    'analisisfaktor' => array(
                        0 => null,
                        1 => null,
                        2 => null,
                        3 => null
                    ),
                    'program' => $cek->program,
                    'indikator' => $cek->indikator
                );
                foreach($cek->analisisfaktor as $afk)
                {
                    if($afk->triwulan == 1)
                    {
                        $databaru[$indexInd]['analisisfaktor'][0] = $afk;
                    }else if($afk->triwulan == 2)
                    {
                        $databaru[$indexInd]['analisisfaktor'][1] = $afk;
                    }else if($afk->triwulan == 3)
                    {
                        $databaru[$indexInd]['analisisfaktor'][2] = $afk;
                    }else if($afk->triwulan == 4)
                    {
                        $databaru[$indexInd]['analisisfaktor'][3] = $afk;
                    }
                }
                $indexInd++;
            }
        }        

        if($format == 'baru')
        {
            return API_RENJAResource::collection($databaru);
        }else
        {
            return API_RENJAResource::collection($data);  
        }
    }

    //EditIndikatorSubKegiatan
    public function getEditIndikatorSubKegiatan(Request $request)
    {
        $id_ind = $request->id_indikator_subkegiatan;

        $data = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan', $id_ind)->get();

        return API_RENJAResource::collection($data);  
    }
    
    //EditIndikatorKegiatan
    public function getEditIndikatorKegiatan(Request $request)
    {
        $id_ind = $request->id_indikator_kegiatan;

        $data = IndikatorKegiatan_90::where('id_indikator_kegiatan', $id_ind)->get();

        return API_RENJAResource::collection($data);  
    }

    //IndikatorProgram
    public function getIndikatorProgram(Request $request)
    {   
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $prog = $request->prog;
        $format = $request->format;
        $all = $request->all;

        if($all == true)
        {
            $opd = SKPD_90::where('id_skpd','!=',34)
                    ->orderBy('kode_skpd')
                    ->orderBy('nama_skpd')
                    ->get();
            $index_1 = 0;
            foreach($opd as $pd)
            {
                $id_skpd = $pd->id_skpd;
                $data[$index_1] = ViewProgramRenja::with(['analisisfaktor.analisator.jafung','bidangurusan','program',
                    'indikator' => function ($q_indikator) use ($tahun,$id_skpd) {
                    $q_indikator->whereHas('TargetRealisasiProgramV2', function ($q_target) use ($tahun,$id_skpd) {
                        $q_target->whereHas('Renja', function ($q_renja) use ($tahun,$id_skpd) {
                            $q_renja->where([
                                ['periode_usulan',$tahun],
                                ['id_skpd',$id_skpd]
                            ]);
                        });
                    })
                    ->with(['TargetRealisasiProgramV2' => function ($q_target) use ($tahun,$id_skpd) {
                        $q_target->whereHas('Renja', function ($q_renja) use ($tahun,$id_skpd) {
                            $q_renja->where([
                                ['periode_usulan',$tahun],
                                ['id_skpd',$id_skpd]
                            ]);
                        });
                    }]);  
                }])
                ->where('periode_usulan', $tahun)
                ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
                ->where('id_skpd', $id_skpd)
                ->when($prog, function ($query) use ($prog) {
                    return $query->where('id_program', $prog);
                })->get();

                if(count($data[$index_1]) > 0)
                {
                    $index_1++;
                }
            }

            // $databaru = null;
            // foreach($data as $idx => $dat)
            // {
            //     foreach($dat as $cek)
            //     {
            //         $databaru[$idx] = array(
            //             'id_program' => $cek->id_program,
            //             'id_program_rpjmd' => $cek->id_program_rpjmd,
            //             'id_bidang_urusan' => $cek->id_bidang_urusan,
            //             'kode_program' => $cek->kode_program,
            //             'nama_program' => $cek->nama_program,
            //             'isDeleted' => $cek->isDeleted,
            //             'id_skpd' => $cek->id_skpd,
            //             'nama_skpd' => $cek->nama_skpd,
            //             'id_renja' => $cek->id_renja,
            //             'apbd_kota' => $cek->apbd_kota,
            //             'r_1' => $cek->r_1,
            //             'r_2' => $cek->r_2,
            //             'r_3' => $cek->r_3,
            //             'r_4' => $cek->r_4,
            //             'isPerubahan' => $cek->isPerubahan,
            //             'periode_usulan' => $cek->periode_usulan,
            //             'bidangurusan' => $cek->bidangurusan,
            //             'programrpjmd' => $cek->programrpjmd,
            //             'analisisfaktor' => array(
            //                 0 => null,
            //                 1 => null,
            //                 2 => null,
            //                 3 => null
            //             ),
            //             'indikator' => null
            //         );
            //         foreach($cek->analisisfaktor as $afp)
            //         {
            //             if($afp->triwulan == 1)
            //             {
            //                 $databaru[$idx]['analisisfaktor'][0] = $afp;
            //             }else if($afp->triwulan == 2)
            //             {
            //                 $databaru[$idx]['analisisfaktor'][1] = $afp;
            //             }else if($afp->triwulan == 3)
            //             {
            //                 $databaru[$idx]['analisisfaktor'][2] = $afp;
            //             }else if($afp->triwulan == 4)
            //             {
            //                 $databaru[$idx]['analisisfaktor'][3] = $afp;
            //             }
            //         }
            //         $indexInd = 0; 
            //         foreach($cek->indikator as $ind)
            //         {
            //             $databaru[$idx]['indikator'][$indexInd] = array(
            //                 'id_indikator_program' => $ind->id_indikator_program,
            //                 'indikator_program' => $ind->indikator_program,
            //                 'id_satuan' => $ind->id_satuan,
            //                 'id_program' => $ind->id_program,
            //                 'id_rumus' => $ind->id_rumus,
            //                 'id_skpd' => $ind->id_skpd,
            //                 'satuan' => $ind->satuan,
            //                 'target' => null,
            //                 'target_perubahan' => null,
            //                 't_1' => null,
            //                 't_2' => null,
            //                 't_3' => null,
            //                 't_4' => null
            //             );
            //             foreach($ind->TargetRealisasiProgramV2 as $tr)
            //             {                            
            //                 $databaru[$idx]['indikator'][$indexInd]['target'] = $tr->target;
            //                 $databaru[$idx]['indikator'][$indexInd]['t_1'] = $tr->t_1;
            //                 $databaru[$idx]['indikator'][$indexInd]['t_2'] = $tr->t_2;
            //                 $databaru[$idx]['indikator'][$indexInd]['t_3'] = $tr->t_3;
            //                 $databaru[$idx]['indikator'][$indexInd]['target_perubahan'] = $tr->target_perubahan;
            //                 $databaru[$idx]['indikator'][$indexInd]['t_4'] = $tr->t_4;
            //             }
            //             $indexInd++;
            //         }
            //     }
            // }       
        }else{
            if(Auth::user()->id_role == 4){
                $opd = Auth::user()->viewuser->id_skpd;          
            }
            $data = ViewProgramRenja::with(['analisisfaktor.analisator.jafung','bidangurusan','program',            
                'indikator' => function ($q_indikator) use ($tahun,$opd) {
                    $q_indikator->whereHas('TargetRealisasiProgramV2', function ($q_target) use ($tahun,$opd) {
                        $q_target->whereHas('Renja', function ($q_renja) use ($tahun,$opd) {
                            $q_renja->where([
                                ['periode_usulan',$tahun],
                                ['id_skpd',$opd]
                            ]);
                        });
                    })
                    ->with(['TargetRealisasiProgramV2' => function ($q_target) use ($tahun,$opd) {
                        $q_target->whereHas('Renja', function ($q_renja) use ($tahun,$opd) {
                            $q_renja->where([
                                ['periode_usulan',$tahun],
                                ['id_skpd',$opd]
                            ]);
                        });
                    }]);      
                }])
            ->where('periode_usulan', $tahun)
            ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
            ->where('id_skpd', $opd)
            ->when($prog, function ($query) use ($prog) {
                return $query->where('id_program', $prog);
            })->get();

            // $databaru = null;
            // $index = 0;
            // foreach($data as $cek)
            // {
            //     $databaru[$index] = array(
            //         'id_program' => $cek->id_program,
            //         'id_program_rpjmd' => $cek->id_program_rpjmd,
            //         'id_bidang_urusan' => $cek->id_bidang_urusan,
            //         'kode_program' => $cek->kode_program,
            //         'nama_program' => $cek->nama_program,
            //         'isDeleted' => $cek->isDeleted,
            //         'id_skpd' => $cek->id_skpd,
            //         'nama_skpd' => $cek->nama_skpd,
            //         'id_renja' => $cek->id_renja,
            //         'apbd_kota' => $cek->apbd_kota,
            //         'r_1' => $cek->r_1,
            //         'r_2' => $cek->r_2,
            //         'r_3' => $cek->r_3,
            //         'r_4' => $cek->r_4,
            //         'isPerubahan' => $cek->isPerubahan,
            //         'periode_usulan' => $cek->periode_usulan,
            //         'bidangurusan' => $cek->bidangurusan,
            //         'programrpjmd' => $cek->programrpjmd,
            //         'analisisfaktor' => array(
            //             0 => null,
            //             1 => null,
            //             2 => null,
            //             3 => null
            //         ),
            //         'indikator' => null
            //     );
            //     foreach($cek->analisisfaktor as $afp)
            //     {
            //         if($afp->triwulan == 1)
            //         {
            //             $databaru[$index]['analisisfaktor'][0] = $afp;
            //         }else if($afp->triwulan == 2)
            //         {
            //             $databaru[$index]['analisisfaktor'][1] = $afp;
            //         }else if($afp->triwulan == 3)
            //         {
            //             $databaru[$index]['analisisfaktor'][2] = $afp;
            //         }else if($afp->triwulan == 4)
            //         {
            //             $databaru[$index]['analisisfaktor'][3] = $afp;
            //         }
            //     }
            //     $indexInd = 0; 
            //     foreach($cek->indikator as $ind)
            //     {
            //         $databaru[$index]['indikator'][$indexInd] = array(
            //             'id_indikator_program' => $ind->id_indikator_program,
            //             'indikator_program' => $ind->indikator_program,
            //             'id_satuan' => $ind->id_satuan,
            //             'id_program_90' => $ind->id_program_90,
            //             'id_rumus' => $ind->id_rumus,
            //             'id_skpd' => $ind->id_skpd,
            //             'satuan' => $ind->satuan,
            //             'targetrealisasi' => $ind->TargetRealisasiProgramV2
            //         );
            //         $indexInd++;
            //     }
            //     $index++;
            // }
        }
        
        // if($format == 'baru')
        // {
        //     return API_RENJAResource::collection($databaru);
        // }else{
            return API_RENJAResource::collection($data);
        // }
    }

    //IndikatorSubKegiatan
    public function getAnalisisSubKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $opd = $request->opd;
        $subkeg = $request->subkeg;

        if(Auth::user()->id_role == 4){
            $opd = Auth::user()->viewuser->id_skpd;          
        }
        
        $data = ViewSubKegiatanRenja::with(['analisisfaktor.analisator.jafung','kegiatan.program','analisisfp' => function ($query) use ($opd,$triwulan) {
            $query->whereHas('analisator', function ($query) use ($opd) {
                $query->where('id_skpd',$opd);
            })->where('triwulan',$triwulan);
        },'analisisfph' => function ($query) use ($opd,$triwulan) {
            $query->whereHas('analisator', function ($query) use ($opd) {
                $query->where('id_skpd',$opd);
            })->where('triwulan',$triwulan);
        },'analisistl' => function ($query) use ($opd,$triwulan) {
            $query->whereHas('analisator', function ($query) use ($opd) {
                $query->join('Jafung_SektorPD','Jafung_SektorPD.id_jafung','AnalisatorSubKegiatan.id_jafung')->where('Jafung_SektorPD.id_skpd',$opd);
            })->where('triwulan',$triwulan)->with('analisator.jafung');
        }])
        ->where('periode_usulan', $tahun)
        ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
        ->where('id_skpd', $opd)
        ->when($subkeg, function ($query) use ($subkeg) {
            return $query->where('id_sub_kegiatan', $subkeg);
        })->get();
        
        return API_RENJAResource::collection($data);
    }
    
    //AnalisisFaktor
    public function viewurusanrenja($tahun,$triwulan,$opd)
    {
        $data = ViewUrusanRenja::with([
            'viewbidangurusan' => function ($query) use ($tahun,$triwulan,$opd) {
                $query->select(
                    'id_bidang_urusan',
                    'id_urusan',
                    'kode_bidang_urusan',
                    'nama_bidang_urusan',
                    'isPerubahan',
                    'periode_usulan'
                )
                ->groupBy(
                    'id_bidang_urusan',
                    'id_urusan',
                    'kode_bidang_urusan',
                    'nama_bidang_urusan',
                    'isPerubahan',
                    'periode_usulan'
                )
                ->with([
                    'viewpdrenja' => function ($query) use ($tahun,$triwulan,$opd) {
                        $query->with([
                            'viewprogram' => function ($query) use ($tahun,$triwulan,$opd) {
                                $query->with([
                                    'program',
                                    'analisisfaktor' => function ($query) use ($triwulan) {
                                        $query->where('triwulan',$triwulan);
                                    },
                                    'viewkegiatan' => function ($query) use ($tahun,$triwulan,$opd) {
                                        $query->with([
                                            'analisisfaktor' => function ($query) use ($triwulan) {
                                                $query->where('triwulan',$triwulan);
                                            },
                                            'viewsubkegiatan' => function ($query) use ($tahun, $triwulan,$opd) {
                                                $query->with([
                                                    'analisisfaktor' => function ($query) use ($triwulan) {
                                                        $query->where('triwulan',$triwulan);
                                                    }
                                                ])->where('id_skpd', $opd);
                                            }
                                        ])->where('id_skpd', $opd);
                                    }
                                ])->where('id_skpd', $opd);
                            }                                                                                                                                                                                                                                                                                         
                        ])->where('id_skpd', $opd);
                    }
                ])->where('id_skpd',$opd);
            }
        ])
        ->where('periode_usulan', $tahun)
        ->where('id_skpd', $opd)
        ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
        ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
        ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
        ->get();

        return $data;
    }

    public function getAnalisisFaktor(Request $request)
    {   
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $all = $request->all;
               
        if($all == 1)
        {
            $opd = SKPD_90::get();
            ini_set('max_execution_time', 6000); //3 minutes
            foreach($opd as $pd)
            {
                $data[] = $this->viewurusanrenja($tahun,$triwulan,$pd->id_skpd);
            }
        }else
        {   
            $opd = $request->opd;
            $data = $this->viewurusanrenja($tahun,$triwulan,$opd);
        }

        return API_RENJAResource::collection($data);
    }

    //Cetak RKPD
    public function getIndikatorSubKegiatanCetak($tahun)
    {
        ini_set('max_execution_time', 10000);
        ini_set('default_socket_timeout', 10000);
        $data = DB::select("
            SELECT
                SubKegiatan_90.id_sub_kegiatan,
                SubKegiatan_90.kode_sub_kegiatan,
                IndikatorSubKegiatan_90.indikator_subkegiatan,
                IndikatorSubKegiatan_90.rumus,
                IndikatorSubKegiatan_90.satuan,
                IndikatorSubKegiatan_90.target,
                IndikatorSubKegiatan_90.target_perubahan,
                R_IndikatorSubKegiatan_90.t_1,
                R_IndikatorSubKegiatan_90.t_2,
                R_IndikatorSubKegiatan_90.t_3,
                R_IndikatorSubKegiatan_90.t_4,
                SKPD_90.kode_skpd,
                SKPD_90.nama_skpd,
                SKPD_90.nama_lain,
                SKPD_90.akronim
            FROM IndikatorSubKegiatan_90
            JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = IndikatorSubKegiatan_90.id_sub_kegiatan
            JOIN R_IndikatorSubKegiatan_90 ON R_IndikatorSubKegiatan_90.id_indikator_subkegiatan = IndikatorSubKegiatan_90.id_indikator_subkegiatan
            JOIN Renja_90 ON Renja_90.id_renja = IndikatorSubKegiatan_90.id_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ?
            ORDER BY
                SubKegiatan_90.kode_sub_kegiatan,
                SKPD_90.kode_skpd,
                IndikatorSubKegiatan_90.indikator_subkegiatan
        ", array($tahun));

        $hasil = null;
        if(count($data) > 0)
        {
            foreach($data as $dat)
            {
                $hasil[$dat->id_sub_kegiatan][] = $dat;
            }
        }
        return $hasil;
    }
    public function getIndikatorKegiatanCetak($tahun)
    {
        ini_set('max_execution_time', 10000);
        ini_set('default_socket_timeout', 10000);
        $data = DB::select("
            SELECT
                Kegiatan_90.id_kegiatan,
                Kegiatan_90.kode_kegiatan,
                IndikatorKegiatan_90.indikator_kegiatan,
                IndikatorKegiatan_90.rumus,
                IndikatorKegiatan_90.satuan,
                IndikatorKegiatan_90.target,
                IndikatorKegiatan_90.target_perubahan,
                R_IndikatorKegiatan_90.t_1,
                R_IndikatorKegiatan_90.t_2,
                R_IndikatorKegiatan_90.t_3,
                R_IndikatorKegiatan_90.t_4,
                SKPD_90.kode_skpd,
                SKPD_90.nama_skpd,
                SKPD_90.nama_lain,
                SKPD_90.akronim
            FROM IndikatorKegiatan_90
            JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = IndikatorKegiatan_90.id_kegiatan
            JOIN R_IndikatorKegiatan_90 ON R_IndikatorKegiatan_90.id_indikator_kegiatan = IndikatorKegiatan_90.id_indikator_kegiatan
            JOIN Renja_90 ON Renja_90.id_renja = IndikatorKegiatan_90.id_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ?
            ORDER BY
                Kegiatan_90.kode_kegiatan,
                SKPD_90.kode_skpd,
                IndikatorKegiatan_90.indikator_kegiatan
        ", array($tahun));

        $hasil = null;
        if(count($data) > 0)
        {
            foreach($data as $dat)
            {
                $hasil[$dat->id_kegiatan][] = $dat;
            }
        }
        return $hasil;
    }
    public function getIndikatorProgramCetak($tahun)
    {
        ini_set('max_execution_time', 10000);
        ini_set('default_socket_timeout', 10000);
        $data = DB::select("
            SELECT
                Program_90.id_program,
                Program_90.kode_program,
                IndikatorProgramRPJMD.indikator_program,
                IndikatorProgramRPJMD.id_rumus,
                TargetRealisasiProgramV2.satuan,
                TargetRealisasiProgramV2.target,
                TargetRealisasiProgramV2.target_perubahan,
                TargetRealisasiProgramV2.t_1,
                TargetRealisasiProgramV2.t_2,
                TargetRealisasiProgramV2.t_3,
                TargetRealisasiProgramV2.t_4,
                SKPD_90.kode_skpd,
                SKPD_90.nama_skpd,
                SKPD_90.nama_lain,
                SKPD_90.akronim
            FROM Program_90
            JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_program_90 = Program_90.id_program
            JOIN SKPD_90 ON SKPD_90.id_skpd = IndikatorProgramRPJMD.id_skpd
            JOIN TargetRealisasiProgramV2 ON TargetRealisasiProgramV2.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
            JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
            WHERE IndikatorProgramRPJMD.indikator_program IS NOT NULL
            AND Renja_90.periode_usulan = ?
            ORDER BY
                Program_90.kode_program,
                SKPD_90.kode_skpd,
                SKPD_90.nama_skpd,
                IndikatorProgramRPJMD.indikator_program
        ",array(
            $tahun
        ));
        $hasil = null;
        if(count($data) > 0)
        {
            foreach($data as $dat)
            {
                $hasil[$dat->id_program][] = $dat;
            }
        }
        return $hasil;
    }
    public function getCetakRKPD(Request $request)
    {   
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $all = $request->all;
        $format = $request->format;
        $sortir = $request->sortir;
        $data = null;
        $perubahan = 0;
        if($triwulan > 3)
        {
            $perubahan = 1;
        }
        if($sortir == true)
        {            
            $data = Urusan_90::whereHas('bidangurusan', function ($q_whbidangurusan) use ($tahun,$perubahan) {
                // URUSAN
                $q_whbidangurusan->whereHas('program', function ($q_whprog) use ($tahun,$perubahan) {
                    $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan) {
                        $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan) {
                            $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan) {
                                $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->where('isPerubahan',$perubahan);
                            });
                        });
                    });
                });
            })
            ->with(['bidangurusan' => function ($q_bidangurusan) use ($tahun,$perubahan) {
                // BIDANG URUSAN
                $q_bidangurusan->whereHas('program', function ($q_whprog) use ($tahun,$perubahan) {
                    $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan) {
                        $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan) {
                            $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan) {
                                $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->where('isPerubahan',$perubahan);
                            });
                        });
                    });
                })
                ->with(['program' => function ($q_prog) use ($tahun,$perubahan) {
                    // PROGRAM
                    $q_prog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan) {
                        $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan) {
                            $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan) {
                                $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->where('isPerubahan',$perubahan);
                            });
                        });
                    })
                    ->with(['anggaranmurni' => function ($q_amurni) use ($tahun) {
                            $q_amurni->where('periode_usulan',$tahun);
                        }
                        ,'anggaranperubahan' => function ($q_aperubahan) use ($tahun) {
                            $q_aperubahan->where('periode_usulan',$tahun);
                        }
                        ,'indikator' => function  ($q_iprog) use ($tahun,$perubahan) {
                        // INDIKATOR PROGRAM
                            $q_iprog->whereHas('TargetRealisasiProgramV2', function ($q_whtrprog) use ($tahun) {
                                $q_whtrprog->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                });
                            })
                            ->with(['TargetRealisasiProgramV2' => function ($q_trprog) use ($tahun) {
                                $q_trprog->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->with(['renja' => function ($q_renja) use ($tahun) {
                                    $q_renja->where('periode_usulan',$tahun)
                                    ->with('skpd');
                                }]);
                            }]);
                        }
                        ,'kegiatan' => function ($q_keg) use ($tahun,$perubahan) {
                        // KEGIATAN
                        $q_keg->whereHas('indikatorkegiatan', function ($q_whikeg) use ($tahun) {
                            $q_whikeg->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                $q_whrenja->where('periode_usulan',$tahun);
                            });
                        })
                        ->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan) {
                            $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan) {
                                $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->where('isPerubahan',$perubahan);
                            });
                        })
                        ->with(['anggaranmurni' => function ($q_amurni) use ($tahun) {
                                $q_amurni->where('periode_usulan',$tahun);
                            }
                            ,'anggaranperubahan' => function ($q_aperubahan) use ($tahun) {
                                $q_aperubahan->where('periode_usulan',$tahun);
                            }
                            ,
                            'indikatorkegiatan' => function ($q_ikeg) use ($tahun) {
                            // INDIKATOR KEGIATAN
                                $q_ikeg->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->with(['realisasiindikatorkeg',
                                    'renja' => function ($q_renja) use ($tahun) {
                                        $q_renja->where('periode_usulan',$tahun)
                                        ->with('skpd');
                                    }
                                ]);
                            }
                            ,'subkegiatan' => function ($q_subkeg) use ($tahun,$perubahan) {
                            // SUB KEGIATAN
                            $q_subkeg->whereHas('indikatorsubkegiatan', function ($q_whisubkeg) use ($tahun) {
                                $q_whisubkeg->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                });
                            })
                            ->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan) {
                                $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->where('isPerubahan',$perubahan);
                            })
                            ->with(['anggaranmurni' => function ($q_amurni) use ($tahun) {
                                    $q_amurni->where('periode_usulan',$tahun);
                                }
                                ,'anggaranperubahan' => function ($q_aperubahan) use ($tahun) {
                                    $q_aperubahan->where('periode_usulan',$tahun);
                                }
                                ,'indikatorsubkegiatan' => function ($q_isubkeg) use ($tahun) {
                                // INDIKATOR SUB KEGIATAN
                                    $q_isubkeg->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                        $q_whrenja->where('periode_usulan',$tahun);
                                    })
                                    ->with(['realisasiindikatorsubkeg'
                                    ,'renja' => function ($q_renja) use ($tahun) {
                                        $q_renja->where('periode_usulan',$tahun)
                                        ->with('skpd');
                                    }]);
                                }
                                ,'logrenja90' => function ($q_logrenja) use ($tahun,$perubahan) {
                                // LOG RENJA
                                $q_logrenja->whereHas('renja', function ($q_whrenja) use ($tahun) {
                                    $q_whrenja->where('periode_usulan',$tahun);
                                })
                                ->with([
                                    'realisasimurni' => function ($q_rmurni) {
                                        $q_rmurni->select(
                                            'R_LogRenja_90.id_r_logrenja',
                                            'R_LogRenja_90.id_log_renja',
                                            'R_LogRenja_90.r_1',
                                            'R_LogRenja_90.r_2',
                                            'R_LogRenja_90.r_3',
                                            'R_LogRenja_90.r_4'
                                        );
                                    }
                                    ,'realisasiperubahan' => function ($q_rperubahan) {
                                        $q_rperubahan->select(
                                            'R_LogRenja_90.id_r_logrenja',
                                            'R_LogRenja_90.id_log_renja',
                                            'R_LogRenja_90.r_1',
                                            'R_LogRenja_90.r_2',
                                            'R_LogRenja_90.r_3',
                                            'R_LogRenja_90.r_4'
                                        );
                                    }
                                    ,'renja' => function ($q_renja) use ($tahun,$perubahan) {
                                        // RENJA
                                    $q_renja->with('skpd')
                                    ->where('periode_usulan',$tahun);
                                }])
                                ->where('isPerubahan',$perubahan);
                            }]);
                        }]);
                    }]);
                }]);
            }])
            ->get();
        }else{
            if($all == false || is_null($all))
            {

                // ambil data salah satu perangkat daerah
                $opd = $request->opd;
    
                if(Auth::user()->id_role == 4){
                    $opd = Auth::user()->viewuser->id_skpd;          
                }
        
                $data = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$opd) {
                    $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                            ->orderBy('kode_bidang_urusan')
                            ->with(['viewprogram' => function ($query) use ($tahun,$opd) {
                                $query->orderBy('kode_program')->with(['program' => function ($query) use ($tahun,$opd) {
                                   $query->whereHas('indikator', function ($q) use ($tahun,$opd){
                                        $q->whereHas('TargetRealisasiProgramV2', function ($q) use ($tahun,$opd) {
                                            $q->whereHas('Renja', function ($q) use ($tahun,$opd) {
                                                $q->where([
                                                    ['periode_usulan',$tahun],
                                                    ['id_skpd',$opd]
                                                ]);
                                            });
                                        });
                                   })->with(['indikator' => function ($query) use ($tahun,$opd) {
                                        $query->whereHas('TargetRealisasiProgramV2', function ($q) use ($tahun,$opd) {
                                            $q->whereHas('Renja', function ($q) use ($tahun,$opd) {
                                                $q->where([
                                                    ['periode_usulan',$tahun],
                                                    ['id_skpd',$opd]
                                                ]);
                                            });
                                        })->with(['TargetRealisasiProgramV2' => function ($query) use ($tahun,$opd) {
                                            $query->whereHas('Renja', function ($q) use ($tahun,$opd) {
                                                $q->where([
                                                    ['periode_usulan',$tahun],
                                                    ['id_skpd',$opd]
                                                ]);
                                            })->with(['Renja' => function ($query) use ($tahun,$opd) {
                                                $query->where([
                                                    ['periode_usulan',$tahun],
                                                    ['id_skpd',$opd]
                                                ]);
                                            }]);
                                        }]);
                                   }]);
                                },'viewkegiatan' => function ($query) use ($tahun,$opd) {
                                    $query->orderBy('kode_kegiatan')->with(['indikator' => function ($query) use ($tahun) {
                                        $query->with('realisasiindikatorkeg');
                                        },'viewsubkegiatan' => function ($query) use ($tahun,$opd) {
                                            $query->orderBy('kode_sub_kegiatan')
                                            ->with(['indikator' => function ($query) use ($tahun,$opd) {
                                                $query->with('realisasiindikatorsubkeg');
                                                }])->when($opd, function ($q) use ($opd) {
                                                    return $q->where('id_skpd', $opd);
                                                });
                                        }])->when($opd, function ($q) use ($opd) {
                                            return $q->where('id_skpd', $opd);
                                        });
                                }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','nama_lain','akronim','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                                ->when($opd, function ($q) use ($opd) {
                                    return $q->where('id_skpd', $opd);
                                })
                                ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','nama_lain','akronim','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                            }])->when($opd, function ($q) use ($opd) {
                                return $q->where('id_skpd', $opd);
                            });
                }])
                ->where('periode_usulan', $tahun)
                ->when($opd, function ($q) use ($opd) {
                    return $q->where('id_skpd', $opd);
                })
                ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
                ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                ->orderBy('kode_urusan')
                ->get();
            }else
            {
                // ambil data seluruh perangkat daerah
                $opd = SKPD_90::orderBy('kode_skpd')->get();
    
                foreach($opd as $pd)
                {
                    $id_skpd = $pd->id_skpd;
                    $renja = ViewUrusanRenja::with(['viewbidangurusan' => function ($query) use ($tahun,$id_skpd) {
                        $query->select('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                                ->groupBy('id_bidang_urusan','id_urusan','kode_bidang_urusan','nama_bidang_urusan','isPerubahan','periode_usulan')
                                ->orderBy('kode_bidang_urusan')
                                ->with(['viewprogram' => function ($query) use ($tahun,$id_skpd) {
                                    $query->orderBy('kode_program')->with(['program' => function ($query) use ($tahun,$id_skpd) {
                                        $query->whereHas('indikator', function ($q) use ($tahun,$id_skpd){
                                            $q->whereHas('TargetRealisasiProgramV2', function ($q) use ($tahun,$id_skpd) {
                                                $q->whereHas('Renja', function ($q) use ($tahun,$id_skpd) {
                                                    $q->where([
                                                        ['periode_usulan',$tahun],
                                                        ['id_skpd',$id_skpd]
                                                    ]);
                                                });
                                            });
                                       })->with(['indikator' => function ($query) use ($tahun,$id_skpd) {
                                            $query->whereHas('TargetRealisasiProgramV2', function ($q) use ($tahun,$id_skpd) {
                                                $q->whereHas('Renja', function ($q) use ($tahun,$id_skpd) {
                                                    $q->where([
                                                        ['periode_usulan',$tahun],
                                                        ['id_skpd',$id_skpd]
                                                    ]);
                                                });
                                            })->with(['TargetRealisasiProgramV2' => function ($query) use ($tahun,$id_skpd) {
                                                $query->whereHas('Renja', function ($q) use ($tahun,$id_skpd) {
                                                    $q->where([
                                                        ['periode_usulan',$tahun],
                                                        ['id_skpd',$id_skpd]
                                                    ]);
                                                })->with(['Renja' => function ($query) use ($tahun,$id_skpd) {
                                                    $query->where([
                                                        ['periode_usulan',$tahun],
                                                        ['id_skpd',$id_skpd]
                                                    ]);
                                                }]);
                                            }]);
                                       }]);
                                    },'viewkegiatan' => function ($query) use ($tahun,$id_skpd) {
                                        $query->orderBy('kode_kegiatan')->with(['indikator' => function ($query) use ($tahun) {
                                            $query->with('realisasiindikatorkeg');
                                            },'viewsubkegiatan' => function ($query) use ($tahun,$id_skpd) {
                                                $query->orderBy('kode_sub_kegiatan')->with(['indikator' => function ($query) use ($tahun,$id_skpd) {
                                                    $query->with('realisasiindikatorsubkeg');
                                                    }])->when($id_skpd, function ($q) use ($id_skpd) {
                                                        return $q->where('id_skpd', $id_skpd);
                                                    });
                                            }])->when($id_skpd, function ($q) use ($id_skpd) {
                                                return $q->where('id_skpd', $id_skpd);
                                            });
                                    }])->select('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','nama_lain','akronim','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan')
                                    ->when($id_skpd, function ($q) use ($id_skpd) {
                                        return $q->where('id_skpd', $id_skpd);
                                    })
                                    ->groupBy('id_program','id_program_rpjmd','id_bidang_urusan','id_renja','id_skpd','nama_skpd','nama_lain','akronim','kode_program','nama_program','apbd_kota','r_1','r_2','r_3','r_4','isPerubahan','periode_usulan');
                                }])->when($id_skpd, function ($q) use ($id_skpd) {
                                    return $q->where('id_skpd', $id_skpd);
                                });
                    }])
                    ->where('periode_usulan', $tahun)
                    ->when($id_skpd, function ($q) use ($id_skpd) {
                        return $q->where('id_skpd', $id_skpd);
                    })
                    ->where('isPerubahan', ($triwulan < 4) ? 0 : 1)
                    ->select('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                    ->groupBy('id_urusan','kode_urusan','nama_urusan','isPerubahan','periode_usulan')
                    ->orderBy('kode_urusan')
                    ->get();
                    
                    $data[] = array(
                        'skpd' => array(
                            'id_skpd' => $pd->id_skpd,
                            'kode_skpd' => $pd->kode_skpd,
                            'nama_skpd' => $pd->nama_skpd,
                            'nama_lain' => $pd->nama_lain,
                            'akronim' => $pd->akronim,
                            'viewurusan' => $renja
                        )
                    );
                }
            }        
        }    
            
        return API_RENJAResource::collection($data);    
    }

    // KategoriGenerator
    public function kategoriGenerator($target,$realisasi)
    {
        $persen = 0;        
        if($target)
        {
            if(is_numeric($target)){
                $persen = ($realisasi/$target)*100;
            }
    
            $persen = ceil(floatval($persen));
            $kt = KategoriPersentase::where('batas_bawah','<=',$persen)
                        ->where('batas_atas','>=',$persen)
                        ->first();
    
            if($persen <= 0){
                $kt = KategoriPersentase::where('batas_bawah',0)->first();
            }elseif($persen >= 100){
                $kt = KategoriPersentase::where('batas_atas',100)->first();
            }
        }else{
            // buat objek dari class KategoriPersentase
            $kt = new KategoriPersentase();
            // set property
            $kt->id_kategori=0;
            $persen=0;
        }
        return array(
            'persen' => $persen,
            'kt' => $kt
        );
    }

    //SaveRealisasiIndikatorSubKegiatan
    public function saverealsubkeg(Request $request)
    {
        $dt = IndikatorSubKegiatan_90::find($request->id_indikator_subkegiatan);
        if(!is_null($request->rumus))
        {
            // simpan rumus
            $dt->rumus = $request->rumus;
            $dt->save();
        }

        // role admin dan jafung menyimpan realisasi seluruh triwulan
        $target = $dt->target;
        $target_perubahan = $dt->target_perubahan;
        $exec = null;
        if($request->realisasi1 != null)
        {
            $persenkt1 = $this->kategoriGenerator($target,$request->realisasi1);

            $exec[] = R_IndikatorSubKegiatan_90::updateOrCreate(
                ['id_indikator_subkegiatan' => $request->id_indikator_subkegiatan],
                [
                    't_1' => $request->realisasi1,
                    'persen_1' => $persenkt1['persen'],
                    'kategori_1' => $persenkt1['kt']->id_kategori
                ]
            );
        }
        
        if($request->realisasi2 != null)
        {
            $persenkt2 = $this->kategoriGenerator($target,$request->realisasi2);

            $exec[] = R_IndikatorSubKegiatan_90::updateOrCreate(
                ['id_indikator_subkegiatan' => $request->id_indikator_subkegiatan],
                [
                    't_2' => $request->realisasi2,
                    'persen_2' => $persenkt2['persen'],
                    'kategori_2' => $persenkt2['kt']->id_kategori
                ]
            );
        }
        
        if($request->realisasi3 != null)
        {
            $persenkt3 = $this->kategoriGenerator($target,$request->realisasi3);

            $exec[] = R_IndikatorSubKegiatan_90::updateOrCreate(
                ['id_indikator_subkegiatan' => $request->id_indikator_subkegiatan],
                [
                    't_3' => $request->realisasi3,
                    'persen_3' => $persenkt3['persen'],
                    'kategori_3' => $persenkt3['kt']->id_kategori
                ]
            );
        }

        if($request->realisasi4 != null)
        {
            $persenkt4 = $this->kategoriGenerator($target_perubahan,$request->realisasi4); 

            $exec[] = R_IndikatorSubKegiatan_90::updateOrCreate(
                ['id_indikator_subkegiatan' => $request->id_indikator_subkegiatan],
                [
                    't_4' => $request->realisasi4,
                    'persen_4' => $persenkt4['persen'],
                    'kategori_4' => $persenkt4['kt']->id_kategori
                ]
            );
        }

        return $exec;
    }
    public function saveinputrealindsubkeg(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_indikator_subkegiatan' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        if(is_array($request->id_indikator_subkegiatan))
        {
            for($i=0;$i<count($request->id_indikator_subkegiatan);$i++)
            {
                $newRequest = new Request();
                $newRequest->merge([
                    'id_indikator_subkegiatan' => $request->id_indikator_subkegiatan[$i],
                    'rumus' => $request->rumus[$i],
                    'realisasi1' => $request->realisasi1[$i],
                    'realisasi2' => $request->realisasi2[$i],
                    'realisasi3' => $request->realisasi3[$i],
                    'realisasi4' => $request->realisasi4[$i]
                ]);
                $exec[] = $this->saverealsubkeg($newRequest);
            }
        }else{
            $exec = $this->saverealsubkeg($request);
        }

        return $exec;
    }

    //SaveRealisasiIndikatorKegiatan
    public function saverealkeg(Request $request)
    {
        $dt = IndikatorKegiatan_90::find($request->id_indikator_kegiatan);
        if(!is_null($request->rumus))
        {
            // simpan rumus
            $dt->rumus = $request->rumus;
            $dt->save();
        }

        // role admin dan jafung menyimpan realisasi seluruh triwulan
        $target = $dt->target;
        $target_perubahan = $dt->target_perubahan;
        $exec = null;
        if($request->realisasi1 != null)
        {
            $persenkt1 = $this->kategoriGenerator($target,$request->realisasi1);
            $exec[] = R_IndikatorKegiatan_90::updateOrCreate(
                ['id_indikator_kegiatan' => $request->id_indikator_kegiatan],
                [
                    't_1' => $request->realisasi1,
                    'persen_1' => $persenkt1['persen'],
                    'kategori_1' => $persenkt1['kt']->id_kategori,
                ]
            );   
        }
        
        if($request->realisasi2 != null)
        {
            $persenkt2 = $this->kategoriGenerator($target,$request->realisasi2);
            $exec[] = R_IndikatorKegiatan_90::updateOrCreate(
                ['id_indikator_kegiatan' => $request->id_indikator_kegiatan],
                [
                    't_2' => $request->realisasi2,
                    'persen_2' => $persenkt2['persen'],
                    'kategori_2' => $persenkt2['kt']->id_kategori
                ]
            );   

        }
        
        if($request->realisasi3 != null)
        {
            $persenkt3 = $this->kategoriGenerator($target,$request->realisasi3);
            $exec[] = R_IndikatorKegiatan_90::updateOrCreate(
                ['id_indikator_kegiatan' => $request->id_indikator_kegiatan],
                [
                    't_3' => $request->realisasi3,
                    'persen_3' => $persenkt3['persen'],
                    'kategori_3' => $persenkt3['kt']->id_kategori
                ]
            );   

        }
        
        if($request->realisasi4 != null)
        {
            $persenkt4 = $this->kategoriGenerator($target_perubahan,$request->realisasi4);
            $exec[] = R_IndikatorKegiatan_90::updateOrCreate(
                ['id_indikator_kegiatan' => $request->id_indikator_kegiatan],
                [
                    't_4' => $request->realisasi4,
                    'persen_4' => $persenkt4['persen'],
                    'kategori_4' => $persenkt4['kt']->id_kategori
                ]
            );   

        }

        return $exec;
    }
    public function saveinputrealindkeg(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_indikator_kegiatan' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        if(is_array($request->id_indikator_kegiatan))
        {
            for($i=0;$i<count($request->id_indikator_kegiatan);$i++)
            {
                $newRequest = new Request();
                $newRequest->merge([
                    'id_indikator_kegiatan' => $request->id_indikator_kegiatan[$i],
                    'rumus' => $request->rumus[$i],
                    'realisasi1' => $request->realisasi1[$i],
                    'realisasi2' => $request->realisasi2[$i],
                    'realisasi3' => $request->realisasi3[$i],
                    'realisasi4' => $request->realisasi4[$i]
                ]);
                $exec[] = $this->saverealkeg($newRequest);
            }
        }else{
            $exec = $this->saverealkeg($request);
        }

        return $exec;
    }

    //SaveRealisasiIndikatorProgram
    public function saverealprog(Request $request)
    {
        // print_r($request->all());
        // // die;
        $exec = null;
        $i=0;
        $dt=null;
        
        $dt = IndikatorProgramRPJMD::
        join(
            'TargetRealisasiProgramV2',
            'TargetRealisasiProgramV2.id_indikator_program'
            ,'IndikatorProgramRPJMD.id_indikator_program'
        )
        ->join('Renja_90','Renja_90.id_renja','TargetRealisasiProgramV2.id_renja')
        ->where('IndikatorProgramRPJMD.id_indikator_program',$request->id_indikator_program)
        ->where('Renja_90.id_skpd',$request->id_skpd)
        ->where('Renja_90.periode_usulan',($request->tahun))
        ->first();
        $i++;

        $tahun = Tahun::where('tahun',$request->tahun)->first();
        $renja = Renja_90::where([
            ['id_skpd','=',$request->id_skpd],
            ['periode_usulan','=',$request->tahun]
        ])->first();

        if($request->rumus)
        {
            $ikp = IndikatorProgramRPJMD::where('id_indikator_program',$request->id_indikator_program)->first();
            if($ikp)
            {
               $ikp->id_rumus = $request->rumus;
               $ikp->timestamps = false;
               $exec[] = $ikp->save();
            }

            $trikp_rumus = TargetRealisasiProgramV2::where([
                ['id_indikator_program','=',$request->id_indikator_program],
                ['id_renja','=',$renja->id_renja]
            ])->first();
            if($trikp_rumus)
            {
                $trikp_rumus->id_rumus = $request->rumus;
                $exec[] = $trikp_rumus->save();
            }
        }

        if(!is_null($request->realisasi1))
        {
            $tr1 = TargetRealisasiProgramV2::where(
                [
                    ['id_indikator_program','=',$request->id_indikator_program],
                    ['id_renja','=',$renja->id_renja]
                ]
            )->first();
            if($tr1)
            {
                $tr1->t_1 = $request->realisasi1;
                if($tr1->save())
                {
                    $exec[] = array(
                        'kode' => 200,
                        'message' => 'Simpan realisasi tw1 berhasil!',
                        'data' => $tr1
                    );
                }else{
                    $exec[] = array(
                        'kode' => 500,
                        'message' => 'Simpan realisasi tw1 gagal!',
                        'data' => $tr1
                    );
                }
            }else{
                $exec[] = array(
                    'kode' => 500,
                    'message' => 'Data realisasi tw1 tidak ditemukan!',
                    'data' => $tr1
                );
            } 
        }
        if(!is_null($request->realisasi2))
        {        
            $tr2 = TargetRealisasiProgramV2::where(
                [
                    ['id_indikator_program','=',$request->id_indikator_program],
                    ['id_renja','=',$renja->id_renja]
                ]
            )->first();
            if($tr2)
            {
                $tr2->t_2 = $request->realisasi2;
                if($tr2->save())
                {
                    $exec[] = array(
                        'kode' => 200,
                        'message' => 'Simpan realisasi tw2 berhasil!',
                        'data' => $tr2
                    );
                }else{
                    $exec[] = array(
                        'kode' => 500,
                        'message' => 'Simpan realisasi tw2 gagal!',
                        'data' => $tr2
                    );
                }
            }else{
                $exec[] = array(
                    'kode' => 500,
                    'message' => 'Data realisasi tw2 tidak ditemukan!',
                    'data' => $tr2
                );
            }   
        }
        if(!is_null($request->realisasi3))
        {
            $tr3 = TargetRealisasiProgramV2::where(
                [
                    ['id_indikator_program','=',$request->id_indikator_program],
                    ['id_renja','=',$renja->id_renja]
                ]
            )->first();
            if($tr3)
            {
                $tr3->t_3 = $request->realisasi3;
                if($tr3->save())
                {
                    $exec[] = array(
                        'kode' => 200,
                        'message' => 'Simpan realisasi tw3 berhasil!',
                        'data' => $tr3
                    );
                }else{
                    $exec[] = array(
                        'kode' => 500,
                        'message' => 'Simpan realisasi tw3 gagal!',
                        'data' => $tr3
                    );
                }
            }else{
                $exec[] = array(
                    'kode' => 500,
                    'message' => 'Data realisasi tw3 tidak ditemukan!',
                    'data' => $tr3
                );
            }   
        }
        if(!is_null($request->realisasi4))
        {
            $tr4 = TargetRealisasiProgramV2::where(
                [
                    ['id_indikator_program','=',$request->id_indikator_program],
                    ['id_renja','=',$renja->id_renja]
                ]
            )->first();
            if($tr4)
            {
                $tr4->t_4 = $request->realisasi4;
                if($tr4->save())
                {
                    $exec[] = array(
                        'kode' => 200,
                        'message' => 'Simpan realisasi tw4 berhasil!',
                        'data' => $tr4
                    );
                }else{
                    $exec[] = array(
                        'kode' => 500,
                        'message' => 'Simpan realisasi tw4 gagal!',
                        'data' => $tr4
                    );
                }
            }else{
                $exec[] = array(
                    'kode' => 500,
                    'message' => 'Data realisasi tw4 tidak ditemukan!',
                    'data' => $tr4
                );
            }  
        }   
        return $exec;
    }
    public function saveinputrealindprog(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_indikator_program' => 'required',
            'id_skpd' => 'required',
            'tahun' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        if(is_array($request->id_indikator_program))
        {
            for($i=0;$i<count($request->id_indikator_program);$i++)
            {
                $newRequest = new Request();
                $newRequest->merge([
                    'id_indikator_program' => $request->id_indikator_program[$i],
                    'rumus' => $request->rumus[$i],
                    'id_skpd' => $request->id_skpd[$i],
                    'tahun' => $request->tahun[$i],
                    'realisasi1' => $request->realisasi1[$i],
                    'realisasi2' => $request->realisasi2[$i],
                    'realisasi3' => $request->realisasi3[$i],
                    'realisasi4' => $request->realisasi4[$i]
                ]);
                $exec[] = $this->saverealprog($newRequest);
            }
        }else{
            $exec = $this->saverealprog($request);
        }

        return $exec;
    }

    //SaveAnalisisFaktor
    public function saveinputakbidangurusan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_bidang_urusan' => 'required',
            'id_renja' => 'required',
            'faktor_pendorong' => 'required',
            'faktor_penghambat' => 'required',
            'triwulan' => 'required',
            'id_jafung' => (Auth::user()->id_role == 13) ? 'required' : '',
            'tl_triwulan' => (Auth::user()->id_role == 13) ? 'required' : ''
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        if(Auth::user()->id_role == 13){
            $exec = TindakLanjutBidangUrusan::updateOrCreate(
                ['id_bidang_urusan' => $request->id_bidang_urusan,'id_renja' => $request->id_renja,'triwulan' => $request->triwulan,'id_jafung' => $request->id_jafung],
                ['tl_triwulan' => $request->tl_triwulan,'tl_renja' => $request->tl_renja]
            );
        }else{
            $exec = AnalisisKinerjaBidangUrusan::updateOrCreate(
                ['id_bidang_urusan' => $request->id_bidang_urusan,'id_renja' => $request->id_renja,'triwulan' => $request->triwulan],
                ['faktor_pendorong' => $request->faktor_pendorong,'faktor_penghambat' => $request->faktor_penghambat]
            );
        }

        return response()->json([
            'status' => 200,
            'message' => 'Analisis Kinerja berhasil disimpan!',
            'data' => $exec,
        ]);
    }

    //SaveAnalisisFaktorSubkegiatan
    public function saveinputanalisissubkeg(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_sub_kegiatan' => 'required',
            'id_renja' => 'required',
            'triwulan' => 'required',
            'id_jafung' => (Auth::user()->id_role == 13) ? 'required' : '',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        if(Auth::user()->id_role == 13){
            $analisator = AnalisatorSubKegiatan::where('id_jafung', $request->id_jafung)->first();
            if(!isset($analisator)){
                $analisator = new AnalisatorSubKegiatan();
                $analisator->id_jafung = $request->id_jafung;
                $analisator->save();
            }

            $exec = TindakLanjutSubKegiatan::updateOrCreate(
                ['id_sub_kegiatan' => $request->id_sub_kegiatan,'id_renja' => $request->id_renja,'triwulan' => $request->triwulan,'id_analisator' => $analisator->id_analisator],
                ['dana' => $request->inputDanaTL,
                'sdm' => $request->inputSDMTL,
                'waktu_pelaksanaan' => $request->inputWaktuPelaksanaanTL,
                'peraturan_perundangan' => $request->inputPeraturanPerundanganTL,
                'sistem_pengadaan_barang_jasa' => $request->inputSistemPengadaanBarangJasaTL,
                'perijinan' => $request->inputPerijinanTL,
                'ketersediaan_lahan' => $request->inputKetersediaanLahanTL,
                'kesiapan_dukungan_masyarakat' => $request->inputKesiapanDukunganMasyarakatTL,
                'faktor_alam' => $request->inputFaktorAlamTL,
                'keterangan' => $request->inputKeteranganTL ]
            );
            
        }else{
            $pd = Renja_90::find($request->id_renja);

            $analisator = AnalisatorSubKegiatan::where('id_skpd', $pd->id_skpd)->first();
            if(!isset($analisator)){
                $analisator = new AnalisatorSubKegiatan();
                $analisator->id_skpd = $pd->id_skpd;
                $analisator->save();
            }

            $exec = FaktorPendorongSubKegiatan::updateOrCreate(
                ['id_sub_kegiatan' => $request->id_sub_kegiatan,'id_renja' => $request->id_renja,'triwulan' => $request->triwulan,'id_analisator' => $analisator->id_analisator],
                ['dana' => $request->inputDanaFP,
                'sdm' => $request->inputSDMFP,
                'waktu_pelaksanaan' => $request->inputWaktuPelaksanaanFP,
                'peraturan_perundangan' => $request->inputPeraturanPerundanganFP,
                'sistem_pengadaan_barang_jasa' => $request->inputSistemPengadaanBarangJasaFP,
                'perijinan' => $request->inputPerijinanFP,
                'ketersediaan_lahan' => $request->inputKetersediaanLahanFP,
                'kesiapan_dukungan_masyarakat' => $request->inputKesiapanDukunganMasyarakatFP,
                'faktor_alam' => $request->inputFaktorAlamFP,
                'keterangan' => $request->inputKeteranganFP ]
            );

            $exec = FaktorPenghambatSubKegiatan::updateOrCreate(
                ['id_sub_kegiatan' => $request->id_sub_kegiatan,'id_renja' => $request->id_renja,'triwulan' => $request->triwulan,'id_analisator' => $analisator->id_analisator],
                ['dana' => $request->inputDanaFPH,
                'sdm' => $request->inputSDMFPH,
                'waktu_pelaksanaan' => $request->inputWaktuPelaksanaanFPH,
                'peraturan_perundangan' => $request->inputPeraturanPerundanganFPH,
                'sistem_pengadaan_barang_jasa' => $request->inputSistemPengadaanBarangJasaFPH,
                'perijinan' => $request->inputPerijinanFPH,
                'ketersediaan_lahan' => $request->inputKetersediaanLahanFPH,
                'kesiapan_dukungan_masyarakat' => $request->inputKesiapanDukunganMasyarakatFPH,
                'faktor_alam' => $request->inputFaktorAlamFPH,
                'keterangan' => $request->inputKeteranganFPH ]
            );
        }

        return response()->json([
            'status' => 200,
            'message' => 'Analisis Kinerja Sub Kegiatan berhasil disimpan!',
            'data' => $exec,
        ]);
    }

    //SaveTambahIndikatorSubKegiatan
    public function saveaddindsubkegiatan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_sub_kegiatan' => 'required',
            'id_renja' => 'required',
            'indikator_subkegiatan' => 'required',
            'target' => 'required',
            'satuan' => 'required',
            'triwulan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        $tw = $request->triwulan;

        $id_indikator_subkegiatan = $request->id_indikator_subkegiatan;
        $param_update = ['id_sub_kegiatan' => $request->id_sub_kegiatan,'id_renja' => $request->id_renja];
        $data_update = [($tw < 4) ? 'target' : 'target_perubahan' => $request->target,
        'satuan' => $request->satuan,
        'is_added' => $tw,
        'is_emptied' => 0,
        'isDeleted' => 0,
        'is_constant' => 0];

        if($id_indikator_subkegiatan){
            $param_update['id_indikator_subkegiatan'] = $request->id_indikator_subkegiatan;
            $data_update['indikator_subkegiatan'] = $request->indikator_subkegiatan;
        }else{
            $param_update['indikator_subkegiatan'] = $request->indikator_subkegiatan;
        }

        $exec = IndikatorSubKegiatan_90::updateOrCreate(
            $param_update,
            $data_update
        );

        return response()->json([
            'status' => 200,
            'message' => 'Indikator Kinerja Sub Kegiatan berhasil disimpan!',
            'data' => $exec,
        ]);
    }

    //SaveTambahIndikatorKegiatan
    public function saveaddindkegiatan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_kegiatan' => 'required',
            'id_renja' => 'required',
            'indikator_kegiatan' => 'required',
            'target' => 'required',
            'satuan' => 'required',
            'triwulan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        $tw = $request->triwulan;

        $id_indikator_kegiatan = $request->id_indikator_kegiatan;
        $param_update = ['id_kegiatan' => $request->id_kegiatan,'id_renja' => $request->id_renja];
        $data_update = [($tw < 4) ? 'target' : 'target_perubahan' => $request->target,
        'satuan' => $request->satuan,
        'is_added' => $tw,
        'is_emptied' => 0,
        'isDeleted' => 0,
        'is_constant' => 0];

        if($id_indikator_kegiatan){
            $param_update['id_indikator_kegiatan'] = $request->id_indikator_kegiatan;
            $data_update['indikator_kegiatan'] = $request->indikator_kegiatan;
        }else{
            $param_update['indikator_kegiatan'] = $request->indikator_kegiatan;
        }

        $exec = IndikatorKegiatan_90::updateOrCreate(
            $param_update,
            $data_update
        );

        return response()->json([
            'status' => 200,
            'message' => 'Indikator Kinerja Kegiatan berhasil disimpan!',
            'data' => $exec,
        ]);
    }

    //HapusIndikatorKegiatan
    public function deleteindkegiatan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_indikator_kegiatan' => 'required',
            'triwulan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        $tw = $request->triwulan;

        if($tw < 4){            
            $exec = IndikatorKegiatan_90::where('id_indikator_kegiatan',$request->id_indikator_kegiatan)->delete();
        }else{
            $exec = IndikatorKegiatan_90::where('id_indikator_kegiatan',$request->id_indikator_kegiatan)->update(['is_emptied' => $tw]);
        }
        return response()->json([
            'status' => 200,
            'message' => 'Indikator Kinerja Kegiatan berhasil dihapus!',
            'data' => $exec,
        ]);
    }

    //HapusIndikatorSubKegiatan
    public function deleteindsubkegiatan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_indikator_subkegiatan' => 'required',
            'triwulan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        $tw = $request->triwulan;

        if($tw < 4){            
            $exec = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan',$request->id_indikator_subkegiatan)->delete();
        }else{
            $exec = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan',$request->id_indikator_subkegiatan)->update(['is_emptied' => $tw]);
        }
        return response()->json([
            'status' => 200,
            'message' => 'Indikator Kinerja Sub Kegiatan berhasil dihapus!',
            'data' => $exec,
        ]);
    }

    //PD
    public function getdata(Request $request)
    {
        $id_skpd = $request->input('id_pd');

        if(Auth::user()->id_role == 4){
            $id_skpd = Auth::user()->viewuser->id_skpd;          
        }

        $data = SKPD_90::where('SKPD_90.isDeleted', 0)->when($id_skpd, function ($q) use ($id_skpd) {
            return $q->where('id_skpd',$id_skpd);
        })->when(Auth::user()->id_role == 6, function ($q) use ($id_skpd) {
            return $q->join('Bidang','Bidang.id_bidang','SKPD_90.id_bidang')->where('Bidang.id_bidang',Auth::user()->viewuser->id_bidang);
        })->when(Auth::user()->id_role == 13, function ($q) use ($id_skpd) {
            return $q->join('Jafung_SektorPD','Jafung_SektorPD.id_skpd','SKPD_90.id_skpd')->where('Jafung_SektorPD.id_jafung',Auth::user()->viewuser->id_jafung);
        })->get();
        
        return API_RENJAResource::collection($data);
    }

    //IndikatorProgramRPJMD
    public function getIndikatorProgramRPJMD(Request $request)
    {
        $this->id_kegiatan = $request->input('id_kegiatan');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        $data = IndikatorProgramRPJMD::whereHas('ProgramRPJMD.program90.kegiatan', function ($query) {
                $query->when($this->id_kegiatan, function ($q) {
                    return $q->where('id_kegiatan', $this->id_kegiatan);
                });
            })->whereHas('TargetRealisasiIKP.Tahun', function ($query) {
                $query->when($this->periode_usulan, function ($q) {
                    return $q->where('tahun', $this->periode_usulan);
                });
            })->with(['ProgramRPJMD'])->when($this->id_skpd, function ($q) {
                return $q->where('id_skpd', $this->id_skpd);
            })->get();

            $dataindprog = CascadingKegiatan_90::whereHas('renja', function ($query) {
                $query->when($this->periode_usulan, function ($q) {
                    return $q->where('periode_usulan', $this->periode_usulan);
                })->when($this->id_skpd, function ($q) {
                    return $q->where('id_skpd',$this->id_skpd);
                });
                })->where('id_kegiatan',$this->id_kegiatan)
                ->leftjoin('IndikatorProgramRPJMD','IndikatorProgramRPJMD.id_indikator_program','CascadingKegiatan_90.id_indikator_program')
                ->get();
            
            
            $datafull[] = array(
                'data'      => $data,
                'indcascading'      => $dataindprog,
            );
            
        return API_RENJAResource::collection($datafull);
    }

    //RelasiSubKegiatan
    public function getLogRenjaBySubKegiatan(Request $request)
    {
        $this->id_subkegiatan = $request->input('id_subkegiatan');
        $this->periode_usulan = $request->input('periode_usulan');
        $this->id_skpd = $request->input('id_pd');

        $data = LogRenja_90::whereHas('renja', function ($query) {
            $query->when($this->periode_usulan, function ($q) {
                return $q->where('periode_usulan', $this->periode_usulan);
            })->when($this->id_skpd, function ($q) {
                return $q->where('id_skpd',$this->id_skpd);
            })->when($this->id_subkegiatan, function ($q) {
                return $q->where('id_sub_kegiatan',$this->id_subkegiatan);
            });
            })->get();
            
        return API_RENJAResource::collection($data);
    }

    //PD
    public function getPD(Request $request)
    {
        $this->id_skpd = $request->input('id_pd');
        $data = SKPD_90::where('isDeleted', 0)->when($this->id_skpd, function ($q) {
            return $q->whereIn('id_skpd',$this->id_skpd);
        })->get();
        return API_RENJAResource::collection($data);
    }

    //Satuan
    public function getSatuan()
    {
        $data = Satuan::where('isDeleted', 0)->get();
        return API_RENJAResource::collection($data);
    }

    //Rekap Anggaran
    public function getRekapAnggaran(Request $request)
    {
        $anggaran_murni = DB::select("
            SELECT 
                SKPD_90.nama_skpd,
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) as realisasi,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? 
                AND LogRenja_90.isPerubahan = 0
            GROUP BY
                SKPD_90.nama_skpd,
                Renja_90.periode_usulan
        ",array($request->tahun));

        $anggaran_perubahan = DB::select("
            SELECT 
                SKPD_90.nama_skpd,
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) as realisasi,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja_perubahan = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? 
                AND LogRenja_90.isPerubahan = 1
            GROUP BY
                SKPD_90.nama_skpd,
                Renja_90.periode_usulan
        ",array($request->tahun));

        $total_anggaran_murni = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) as realisasi,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 0
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));

        $total_anggaran_perubahan = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) as realisasi,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja_perubahan = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 1
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));

        $total_r_1 = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)) as total_realisasi,
                (
                    CASE
                        WHEN (SUM(ISNULL(LogRenja_90.apbd_kota,0))) != 0 THEN SUM(ISNULL(R_LogRenja_90.r_1,0))/SUM(ISNULL(LogRenja_90.apbd_kota,0))
                        ELSE 0
                    END
                )*100 as capaian,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 0
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));

        $total_r_2 = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)) as total_realisasi,
                (
                    CASE
                        WHEN (SUM(ISNULL(LogRenja_90.apbd_kota,0))) != 0 THEN SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0))/SUM(ISNULL(LogRenja_90.apbd_kota,0))
                        ELSE 0
                    END
                )*100 as capaian,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 0
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));
        
        $total_r_3 = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)) as realisasi,
                (
                    CASE
                        WHEN (SUM(ISNULL(LogRenja_90.apbd_kota,0))) != 0 THEN SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0))/SUM(ISNULL(LogRenja_90.apbd_kota,0))
                        ELSE 0
                    END
                )*100 as capaian,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 0
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));

        $total_r_4 = DB::select("
            SELECT 
                SUM(ISNULL(LogRenja_90.apbd_kota,0)) as total_anggaran,
                SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) as total_realisasi,
                (                         
                    CASE
                        WHEN (SUM(ISNULL(LogRenja_90.apbd_kota,0))) != 0 THEN SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)+ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0))/SUM(ISNULL(LogRenja_90.apbd_kota,0))
                        ELSE 0
                    END
                )*100 as capaian,
                Renja_90.periode_usulan
            FROM LogRenja_90
            JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
            JOIN R_LogRenja_90 ON R_LogRenja_90.id_log_renja_perubahan = LogRenja_90.id_log_renja
            JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
            WHERE Renja_90.periode_usulan = ? AND LogRenja_90.isPerubahan = 1
            GROUP BY
                Renja_90.periode_usulan
        ",array($request->tahun));

        $data = array(
            'agr_murni' => $anggaran_murni,
            'agr_perubahan' => $anggaran_perubahan,
            'total_agr_murni' => $total_anggaran_murni,
            'total_agr_perubahan' => $total_anggaran_perubahan,
            'total_r_1' => $total_r_1,
            'total_r_2' => $total_r_2,
            'total_r_3' => $total_r_3,
            'total_r_4' => $total_r_4
        );
        return API_RENJAResource::collection($data);
    }
    public function converter_nilai($nilai)
    {
        /**
         * Range Nilai
         *  AA  -> 90-100
         *  A   -> 80-90
         *  BB  -> 70-80
         *  B   -> 60-70
         *  CC  -> 50-60
         *  C   -> 30-50
         *  D   -> 0-30
         */
        $hasil = 0;
        if(str_contains($nilai,'AA'))
        {
            $hasil = 90;
        }else if(str_contains($nilai,'A'))
        {
            $hasil = 80;
        }else if(str_contains($nilai,'BB'))
        {
            $hasil = 70;
        }else if(str_contains($nilai,'B'))
        {
            $hasil = 60;
        }else if(str_contains($nilai,'CC'))
        {
            $hasil = 50;
        }else if(str_contains($nilai,'C'))
        {
            $hasil = 30;
        }else if(str_contains($nilai,'D'))
        {
            $hasil = 0;
        }else if(str_contains($nilai,':'))
        {
            $nilai = explode(':',$nilai);
            $hasil = $nilai[0]/$nilai[1];
        }else if(str_contains($nilai,'%'))
        {
            $nilai = explode('%',$nilai);
            $hasil = $nilai[0];
        }

        return $hasil;
    }

    public function calculator_capaian($rumus,$target,$target_perubahan,$t_1,$t_2,$t_3,$t_4)
    {
        $c_1 = 0;
        $c_2 = 0;
        $c_3 = 0;
        $c_4 = 0;

        if($rumus == 1)
        {
            // Rumus Triwulan (+)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = (($t_1/$target)*100)/4;
                $c_2 = (($t_1+$t_2/$target)*100)/4;
                $c_3 = (($t_1+$t_2+$t_3/$target)*100)/4;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = (($t_1+$t_2+$t_3+$t_4/$target_perubahan)*100)/4;
            }

        }else if($rumus == 2)
        {
            // Rumus Semester (+)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = (($t_1/$target)*100)/2;
                $c_2 = (($t_1+$t_2/$target)*100)/2;
                $c_3 = (($t_1+$t_2+$t_3/$target)*100)/2;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = (($t_1+$t_2+$t_3+$t_4/$target_perubahan)*100)/2;
            }
        }else if($rumus == 3)
        {
            // Rumus Tahunan (+)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = ($t_1/$target)*100;
                $c_2 = ($t_1+$t_2/$target)*100;
                $c_3 = ($t_1+$t_2+$t_3/$target)*100;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = ($t_1+$t_2+$t_3+$t_4/$target_perubahan)*100;
            }
        }else if($rumus = 4)
        {
            // Rumus Triwulan (-)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = (($target + ($target - $t_1)/$target)*100)/4;
                $c_2 = (($target + ($target - ($t_1+$t_2))/$target)*100)/4;
                $c_3 = (($target + ($target - ($t_1+$t_2+$t_3))/$target)*100)/4;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = (($target_perubahan + ($target_perubahan - ($t_1+$t_2+$t_3+$t_4))/$target_perubahan)*100)/4;
            }

        }else if($rumus = 5)
        {
            // Rumus Semester (-)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = (($target + ($target - $t_1)/$target)*100)/2;
                $c_2 = (($target + ($target - ($t_1+$t_2))/$target)*100)/2;
                $c_3 = (($target + ($target - ($t_1+$t_2+$t_3))/$target)*100)/2;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = (($target_perubahan + ($target_perubahan - ($t_1+$t_2+$t_3+$t_4))/$target_perubahan)*100)/2;
            }

        }else if($rumus = 6)
        {
            // Rumus Tahunan (-)
            if(!is_null($target) && $target != 0)
            {
                $c_1 = ($target + ($target - $t_1)/$target)*100;
                $c_2 = ($target + ($target - ($t_1+$t_2))/$target)*100;
                $c_3 = ($target + ($target - ($t_1+$t_2+$t_3))/$target)*100;
            }
            if(!is_null($target_perubahan) && $target_perubahan != 0)
            {
                $c_4 = ($target_perubahan + ($target_perubahan - ($t_1+$t_2+$t_3+$t_4))/$target_perubahan)*100;
            }
        }

        return array(
            'c_1' => round($c_1,2),
            'c_2' => round($c_2,2),
            'c_3' => round($c_3,2),
            'c_4' => round($c_4,2)
        );
    }
    public function execute_conversi_program(Request $request)
    {
        $tahun = $request->tahun;
        $id_skpd = $request->id_skpd;
        $data = null;
        $hasil = null;

        $data = DB::select("
                    SELECT * FROM TargetRealisasiProgramV2
                    JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
                    JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_indikator_program = TargetRealisasiProgramV2.id_indikator_program
                    JOIN Program_90 ON Program_90.id_program = IndikatorProgramRPJMD.id_program_90
                    JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                    WHERE Renja_90.periode_usulan = ?
                    ORDER BY
                        SKPD_90.kode_skpd,
                        SKPD_90.nama_skpd,
                        SKPD_90.nama_lain,
                        Program_90.kode_program,
                        IndikatorProgramRPJMD.id_indikator_program
                ",array($tahun)
            );
        
        if(!is_null($data))
        {
            $skpd = null;
            $temp_skpd = null;
            $program = null;
            $temp_prog = null;
            $data_prog = null;
            $ind_program = null;
            $temp_ind_prog = null;
            $data_ind_prog = null;
            $index = 0;
            foreach($data as $tri_prog)
            {
                $target = $tri_prog->target;
                $target_perubahan = $tri_prog->target_perubahan;
                $t_1 = $tri_prog->t_1;
                $t_2 = $tri_prog->t_2;
                $t_3 = $tri_prog->t_3;
                $t_4 = $tri_prog->t_4;
    
                if(!is_numeric($tri_prog->target))
                {
                    // cek Target
                    $target = $this->converter_nilai($tri_prog->target);
                }
                if(!is_numeric($tri_prog->target_perubahan))
                {
                    // cek Target Perubahan
                    $target_perubahan = $this->converter_nilai($tri_prog->target_perubahan);
                }
                if(!is_numeric($tri_prog->t_1))
                {
                    // cek Realisasi TW 1
                    $t_1 = $this->converter_nilai($tri_prog->t_1);
                }
                if(!is_numeric($tri_prog->t_2))
                {
                    // cek Realisasi TW 2
                    $t_2 = $this->converter_nilai($tri_prog->t_2);
                }
                if(!is_numeric($tri_prog->t_3))
                {
                    // cek Realisasi TW 3
                    $t_3 = $this->converter_nilai($tri_prog->t_3);
                }
                if(!is_numeric($tri_prog->t_4))
                {
                    // cek Realisasi TW 4
                    $t_4 = $this->converter_nilai($tri_prog->t_4);
                }
    
                if(is_null($tri_prog->target))
                {
                    // cek Target
                    $target = 0;
                }
                if(is_null($tri_prog->target_perubahan))
                {
                    // cek Target Perubahan
                    $target_perubahan = 0;
                }
                if(is_null($tri_prog->t_1))
                {
                    // cek Realisasi TW 1
                    $t_1 = 0;
                }
                if(is_null($tri_prog->t_2))
                {
                    // cek Realisasi TW 2
                    $t_2 = 0;
                }
                if(is_null($tri_prog->t_3))
                {
                    // cek Realisasi TW 3
                    $t_3 = 0;
                }
                if(is_null($tri_prog->t_4))
                {
                    // cek Realisasi TW 4
                    $t_4 = 0;
                }
                if(!is_null($ind_program) && $ind_program != $tri_prog->id_indikator_program)
                {
                    $data_ind_prog[]= $temp_ind_prog;
                }
                if(!is_null($program) && $program != $tri_prog->kode_program)
                {
                    $temp_prog['indikatorprogram'] = $data_ind_prog;
                    $data_prog[]= $temp_prog;
                    if($index < count($data)-1)
                    {
                        $data_ind_prog = null;
                    }
                }
                if(!is_null($skpd) && $skpd != $tri_prog->kode_skpd)
                {
                    $temp_skpd['program'] = $data_prog;
                    $hasil['skpd'][] = $temp_skpd;

                    if($index < count($data)-1)
                    {
                        $data_prog = null;
                    }
                }

                $temp_skpd = array(
                    'id_skpd'    => $tri_prog->id_skpd,
                    'nama_skpd'  => $tri_prog->nama_skpd,
                    'nama_lain'  => $tri_prog->nama_lain,
                    'akronim'    => $tri_prog->akronim,
                    'program'    => null
                );

                $temp_prog = array(
                    'id_program_90' => $tri_prog->id_program,
                    'kode_program'  => $tri_prog->kode_program,
                    'nama_program'  => $tri_prog->nama_program,
                    'indikatorprogram'  => null
                );
                $temp_ind_prog = array(
                    'id_indikator_program'      => $tri_prog->id_indikator_program,
                    'nama_indikator_program'    => $tri_prog->indikator_program,
                    'satuan'                    => $tri_prog->satuan,
                    'target_realisasi_program'  => array(
                        'id_tri_program'        => $tri_prog->id_tri_program,
                        'id_rumus'              => $tri_prog->id_rumus,
                        'id_renja'              => $tri_prog->id_renja,
                        'target'                => $tri_prog->target,
                        'target_perubahan'      => $tri_prog->target_perubahan,
                        't_1'                   => $tri_prog->t_1,
                        't_2'                   => $tri_prog->t_2,
                        't_3'                   => $tri_prog->t_3,
                        't_4'                   => $tri_prog->t_4,
                        'target_cnv'            => $target,
                        'target_perubahan_cnv'  => $target_perubahan,
                        't_1_cnv'               => $t_1,
                        't_2_cnv'               => $t_2,
                        't_3_cnv'               => $t_3,
                        't_4_cnv'               => $t_4,
                        'periode_usulan'        => $tri_prog->periode_usulan
                    )
                );
                $skpd = $tri_prog->kode_skpd;
                $program = $tri_prog->kode_program;
                $ind_program = $tri_prog->id_indikator_program;
                $index++;
            }
        }
        if(!is_null($hasil) && count($hasil)>0)
        {
            $temp_prog['indikatorprogram'] = $temp_ind_prog;
            $data_prog[] = $temp_prog;
            $temp_skpd['program'] = $data_prog;
            $hasil['skpd'][] = $temp_skpd;
        }
        return $hasil;

    }
}
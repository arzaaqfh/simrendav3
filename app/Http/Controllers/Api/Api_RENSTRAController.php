<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\Api\API_RENJAResource;
use App\Models\Renja\ViewSubKegiatanRenja;
use App\Models\Renja\ViewKegiatanRenja;
use App\Models\Renja\ViewProgramRenja;
use App\Models\Renja\ViewUrusanRenja;
use App\Models\AnalisisKinerjaBidangUrusan;
use App\Models\TindakLanjutBidangUrusan;
use App\Models\AnalisatorSubKegiatan;
use App\Models\FaktorPendorongSubKegiatan;
use App\Models\FaktorPenghambatSubKegiatan;
use App\Models\TindakLanjutSubKegiatan;

use App\Models\LogRenja_90;
use App\Models\R_LogRenja_90;
use App\Models\Kegiatan_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\R_IndikatorKegiatan_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\R_IndikatorSubKegiatan_90;
use App\Models\IndikatorProgramRPJMD;
use App\Models\TargetRealisasiIKP;
use App\Models\Tahun;
use App\Models\SKPD_90;
use App\Models\Satuan;
use App\Models\KategoriPersentase;
use App\Models\Program_90;
use App\Models\ProgramRPJMD;

use App\Models\Sasaran;
use App\Models\IndikatorSasaran;
use App\Models\TargetRealisasiSasaran;
use App\Models\Tujuan;
use App\Models\IndikatorTujuan;
use App\Models\TargetRealisasiTujuan;
use App\Models\RelSasTuj;
use App\Models\RelTujProg;
use App\Models\Periode;

use Session;
use Auth;
use Validator;

use App\Models\CascadingKegiatan_90;
use App\Models\CascadingSubKegiatan_90;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Database\Eloquent\Builder;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Api_RENSTRAController extends Controller
{
    // Get Indikator Tujuan
    public function getIndikatorTujuan(Request $request)
    {
        $indikator_tujuan = DB::select("
            SELECT
                Misi.id_misi,
                Sasaran.id_sasaran as id_tujuan,
                IndikatorSasaran.id_indikator_sasaran as id_indikator_tujuan,
                IndikatorSasaran.indikator_sasaran as indikator_tujuan,
                TargetRealisasiSasaran.*
            FROM TargetRealisasiSasaran
            JOIN IndikatorSasaran ON IndikatorSasaran.id_indikator_sasaran = TargetRealisasiSasaran.id_indikator_sasaran
            JOIN Sasaran ON Sasaran.id_sasaran = IndikatorSasaran.id_sasaran
            JOIN Periode ON Periode.id_periode = TargetRealisasiSasaran.id_periode
            JOIN Misi ON Misi.id_misi = Sasaran.id_misi
            WHERE Periode.id_periode = ?
        ",array($request->id_periode));

        $data = null;
        foreach($indikator_tujuan as $ituj)
        {
            $data[$ituj->id_misi][$ituj->id_tujuan][] = array(
                'indikator_tujuan' => $ituj->indikator_tujuan,
                'awal' => $ituj->awal,
                't_1' => $ituj->t_1,
                't_2' => $ituj->t_2,
                't_3' => $ituj->t_3,
                't_4' => $ituj->t_4,
                't_5' => $ituj->t_5,
                't_1_perubahan' => $ituj->t_1_perubahan,
                't_2_perubahan' => $ituj->t_2_perubahan,
                't_3_perubahan' => $ituj->t_3_perubahan,
                't_4_perubahan' => $ituj->t_4_perubahan,
                't_5_perubahan' => $ituj->t_5_perubahan,
                'akhir' => $ituj->akhir,
                'satuan' => $ituj->satuan,
                'id_rumus' => $ituj->id_rumus,
                'r_1' => $ituj->r_1,
                'r_2' => $ituj->r_2,
                'r_3' => $ituj->r_3,
                'r_4' => $ituj->r_4,
                'r_5' => $ituj->r_5
            );
        }

        return $data;
    }

    // Get Indikator Sasaran
    public function getIndikatorSasaran(Request $request)
    {
        if(is_null($request->pd))
        {
            $indikator_sasaran = DB::select("
                SELECT
                    Misi.id_misi,
                    Sasaran.id_sasaran as id_tujuan,
                    Tujuan.id_tujuan as id_sasaran,
                    IndikatorTujuan.id_indikator_tujuan as id_indikator_sasaran,
                    IndikatorTujuan.indikator_tujuan as indikator_sasaran,
                    TargetRealisasiTujuan.*,
                    SKPD_90.*
                FROM TargetRealisasiTujuan
                JOIN IndikatorTujuan ON IndikatorTujuan.id_indikator_tujuan = TargetRealisasiTujuan.id_indikator_tujuan
                JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
                JOIN RelSasTuj ON RelSasTuj.id_tujuan = Tujuan.id_tujuan
                JOIN Sasaran ON Sasaran.id_sasaran = RelSasTuj.id_sasaran
                JOIN Periode ON Periode.id_periode = TargetRealisasiTujuan.id_periode
                JOIN Misi ON Misi.id_misi = Tujuan.id_misi
                JOIN SKPD_90 ON SKPD_90.id_skpd = TargetRealisasiTujuan.id_skpd
                WHERE Periode.id_periode = ?
                ORDER BY
                    SKPD_90.kode_skpd,
                    SKPD_90.nama_skpd
            ",array($request->id_periode));
        }else{
            $indikator_sasaran = DB::select("
                SELECT
                    Misi.id_misi,
                    Sasaran.id_sasaran as id_tujuan,
                    Tujuan.id_tujuan as id_sasaran,
                    IndikatorTujuan.id_indikator_tujuan as id_indikator_sasaran,
                    IndikatorTujuan.indikator_tujuan as indikator_sasaran,
                    TargetRealisasiTujuan.*,
                    SKPD_90.*
                FROM TargetRealisasiTujuan
                JOIN IndikatorTujuan ON IndikatorTujuan.id_indikator_tujuan = TargetRealisasiTujuan.id_indikator_tujuan
                JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
                JOIN RelSasTuj ON RelSasTuj.id_tujuan = Tujuan.id_tujuan
                JOIN Sasaran ON Sasaran.id_sasaran = RelSasTuj.id_sasaran
                JOIN Periode ON Periode.id_periode = TargetRealisasiTujuan.id_periode
                JOIN Misi ON Misi.id_misi = Tujuan.id_misi
                JOIN SKPD_90 ON SKPD_90.id_skpd = TargetRealisasiTujuan.id_skpd
                WHERE Periode.id_periode = ?
                    AND SKPD_90.id_skpd = ?
                ORDER BY
                    SKPD_90.kode_skpd,
                    SKPD_90.nama_skpd
            ",array($request->id_periode,$request->pd));
        }

        $data = null;
        foreach($indikator_sasaran as $isas)
        {
            $data[$isas->id_misi][$isas->id_tujuan][$isas->id_sasaran][] = array(
                'indikator_sasaran' => $isas->indikator_sasaran,
                'awal' => $isas->awal,
                't_1' => $isas->t_1,
                't_2' => $isas->t_2,
                't_3' => $isas->t_3,
                't_4' => $isas->t_4,
                't_5' => $isas->t_5,
                't_1_perubahan' => $isas->t_1_perubahan,
                't_2_perubahan' => $isas->t_2_perubahan,
                't_3_perubahan' => $isas->t_3_perubahan,
                't_4_perubahan' => $isas->t_4_perubahan,
                't_5_perubahan' => $isas->t_5_perubahan,
                'akhir' => $isas->akhir,
                'satuan' => $isas->satuan,
                'id_rumus' => $isas->id_rumus,
                'r_1' => $isas->r_1,
                'r_2' => $isas->r_2,
                'r_3' => $isas->r_3,
                'r_4' => $isas->r_4,
                'r_5' => $isas->r_5,
                'perangkat_daerah' => array(
                    'kode_skpd' => $isas->kode_skpd,
                    'nama_skpd' => $isas->nama_skpd,
                    'nama_lain' => $isas->nama_lain,
                    'akronim' => $isas->akronim
                )
            );
        }

        return $data;
    }

    
    // Get Indikator Program
    public function getIndikatorProgram(Request $request)
    {
        // if(is_null($request->pd))
        // {
            $indikator_program = DB::select("
                SELECT
                    Misi.id_misi,
                    Sasaran.id_sasaran as id_tujuan,
                    Tujuan.id_tujuan as id_sasaran,
                    Program_90.id_program as id_program_90,
                    Program_90.id_program_rpjmd as id_program_rpjmd,
                    IndikatorProgramRPJMD.indikator_program,
                    IndikatorProgramRPJMD.id_rumus,
                    Satuan.satuan,
                    TargetRealisasiIKP.target,
                    TargetRealisasiIKP.target_perubahan,
                    TargetRealisasiIKP.realisasi,
                    TargetRealisasiIKP.triwulan,
                    Tahun.tahun,
                    SKPD_90.*
                FROM TargetRealisasiIKP
                JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_indikator_program = TargetRealisasiIKP.id_indikator_program
                JOIN ProgramRPJMD ON ProgramRPJMD.id_program = IndikatorProgramRPJMD.id_program
                JOIN Program_90 ON Program_90.id_program_rpjmd = ProgramRPJMD.id_program
                JOIN RelTujProg ON RelTujProg.id_program_90 = Program_90.id_program
                JOIN Tujuan ON Tujuan.id_tujuan = RelTujProg.id_tujuan
                JOIN RelSasTuj ON RelSasTuj.id_tujuan = Tujuan.id_tujuan
                JOIN Sasaran ON Sasaran.id_sasaran = RelSasTuj.id_sasaran
                JOIN Misi ON Misi.id_misi = Tujuan.id_misi
                JOIN SKPD_90 ON SKPD_90.id_skpd = IndikatorProgramRPJMD.id_skpd
                JOIN Tahun ON Tahun.id_tahun = TargetRealisasiIKP.id_tahun
                JOIN Satuan ON Satuan.id_satuan = IndikatorProgramRPJMD.id_satuan
                WHERE Tahun.tahun >= ?
                    AND Tahun.tahun <= ?
                ORDER BY
                    Tahun.tahun,
                    SKPD_90.kode_skpd,
                    SKPD_90.nama_skpd,
                    IndikatorProgramRPJMD.id_indikator_program,
                    TargetRealisasiIKP.triwulan
            ",array($request->tahun_awal,$request->tahun_akhir));
        // }else{
        // }

        // $data = null;
        // foreach($indikator_program as $iprog)
        // {
        //     $data[$iprog->id_misi][$iprog->id_tujuan][$iprog->id_sasaran][$iprog->id_program_90][] = array(
        //         'indikator_program' => $iprog->indikator_program,
        //         't_1' => $iprog->target,
        //         't_2' => $iprog->target,
        //         't_3' => $iprog->target,
        //         't_4' => $iprog->target,
        //         't_5' => $iprog->target,
        //         't_1_perubahan' => $iprog->target_perubahan,
        //         't_2_perubahan' => $iprog->target_perubahan,
        //         't_3_perubahan' => $iprog->target_perubahan,
        //         't_4_perubahan' => $iprog->target_perubahan,
        //         't_5_perubahan' => $iprog->target_perubahan,
        //         'satuan' => $iprog->satuan,
        //         'id_rumus' => $iprog->id_rumus,
        //         'r_1' => $iprog->realisasi,
        //         'r_2' => $iprog->realisasi,
        //         'r_3' => $iprog->realisasi,
        //         'r_4' => $iprog->realisasi,
        //         'r_5' => $iprog->realisasi,
        //         'perangkat_daerah' => array(
        //             'kode_skpd' => $iprog->kode_skpd,
        //             'nama_skpd' => $iprog->nama_skpd,
        //             'nama_lain' => $iprog->nama_lain,
        //             'akronim' => $iprog->akronim
        //         )
        //     );
        // }

        return $indikator_program;
    }

    // Get Data Cetak RENSTRA
    public function getCetak(Request $request)
    {
        //
        $pd = $request->id_skpd;

        $tahun = $request->tahun;
        $periode  = Periode::where([
                ['tahun_awal','<=',$tahun],
                ['tahun_akhir','>=',$tahun]
            ])->first();
        $newRequest = new Request();
        $newRequest->merge([
            'id_periode' => $periode->id_periode,
            'pd' => $pd,
            'tahun_awal' => $periode->tahun_awal,
            'tahun_akhir' => $periode->tahun_akhir
        ]);

        $deretTahun = null;
        for($i=$periode->tahun_awal;$i<=$periode->tahun_akhir;$i++)
        {
            $deretTahun[] = (int) $i;
        }
        $indikator_tujuan = $this->getIndikatorTujuan($newRequest);
        $indikator_sasaran = $this->getIndikatorSasaran($newRequest);
        $indikator_program = $this->getIndikatorProgram($newRequest);

        return $indikator_program;
        die;
        $dasar = null;
        if(is_null($pd) == 1)
        {
            $dasar = DB::select("
                        SELECT
                            Misi.id_misi,
                            Misi.misi,
                            Sasaran.id_sasaran AS id_tujuan,
                            Sasaran.sasaran AS tujuan,
                            Tujuan.id_tujuan AS id_sasaran,
                            Tujuan.tujuan AS sasaran,
                            Program_90.id_program,
                            Program_90.kode_program_v2 AS kode_program,
                            Program_90.nama_program_v2 AS program,
                            Kegiatan_90.id_kegiatan,
                            Kegiatan_90.kode_kegiatan,
                            Kegiatan_90.nama_kegiatan,
                            SubKegiatan_90.id_sub_kegiatan,
                            SubKegiatan_90.kode_sub_kegiatan,
                            SubKegiatan_90.nama_sub_kegiatan,
                            SUM(ISNULL(LogRenja_90.apbd_kota,0)) AS apbd_kota,
                            SUM(
                                ISNULL(R_LogRenja_90.r_1,0)+
                                ISNULL(R_LogRenja_90.r_2,0)+
                                ISNULL(R_LogRenja_90.r_3,0)+
                                ISNULL(R_LogRenja_90.r_4,0)
                            ) AS realisasi,
                            Renja_90.periode_usulan
                        FROM LogRenja_90
                        JOIN R_LogRenja_90 ON R_LogRenja_90.id_r_logrenja = LogRenja_90.id_log_renja OR R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja 
                        JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                        JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                        JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                        JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                        JOIN RelTujProg ON RelTujProg.id_program_90 = Program_90.id_program
                        JOIN Tujuan ON Tujuan.id_tujuan = RelTujProg.id_tujuan
                        JOIN RelSasTuj ON RelSasTuj.id_tujuan = Tujuan.id_tujuan
                        JOIN Sasaran ON Sasaran.id_sasaran = RelSasTuj.id_sasaran AND Sasaran.id_misi = Tujuan.id_misi
                        JOIN Misi ON Misi.id_misi = Sasaran.id_misi
                        WHERE Renja_90.periode_usulan >= ?
                            AND Renja_90.periode_usulan <= ?
                        GROUP BY
                            Misi.id_misi,
                            Misi.misi,
                            Sasaran.id_sasaran,
                            Sasaran.sasaran,
                            Tujuan.id_tujuan,
                            Tujuan.tujuan,
                            Program_90.id_program,
                            Program_90.kode_program_v2,
                            Program_90.nama_program_v2,
                            Kegiatan_90.id_kegiatan,
                            Kegiatan_90.kode_kegiatan,
                            Kegiatan_90.nama_kegiatan,
                            SubKegiatan_90.id_sub_kegiatan,
                            SubKegiatan_90.kode_sub_kegiatan,
                            SubKegiatan_90.nama_sub_kegiatan,
                            Renja_90.periode_usulan
                    ", array($periode->tahun_awal, $periode->tahun_akhir));
        }else{
            $dasar = DB::select("
                        SELECT
                            Misi.id_misi,
                            Misi.misi,
                            Sasaran.id_sasaran AS id_tujuan,
                            Sasaran.sasaran AS tujuan,
                            Tujuan.id_tujuan AS id_sasaran,
                            Tujuan.tujuan AS sasaran,
                            Program_90.id_program,
                            Program_90.kode_program_v2 AS kode_program,
                            Program_90.nama_program_v2 AS program,
                            Kegiatan_90.id_kegiatan,
                            Kegiatan_90.kode_kegiatan,
                            Kegiatan_90.nama_kegiatan,
                            SubKegiatan_90.id_sub_kegiatan,
                            SubKegiatan_90.kode_sub_kegiatan,
                            SubKegiatan_90.nama_sub_kegiatan,
                            SUM(ISNULL(LogRenja_90.apbd_kota,0)) AS apbd_kota,
                            SUM(
                                ISNULL(R_LogRenja_90.r_1,0)+
                                ISNULL(R_LogRenja_90.r_2,0)+
                                ISNULL(R_LogRenja_90.r_3,0)+
                                ISNULL(R_LogRenja_90.r_4,0)
                            ) AS realisasi,
                            Renja_90.periode_usulan
                        FROM LogRenja_90
                        JOIN R_LogRenja_90 ON R_LogRenja_90.id_r_logrenja = LogRenja_90.id_log_renja OR R_LogRenja_90.id_log_renja = LogRenja_90.id_log_renja 
                        JOIN Renja_90 ON Renja_90.id_renja = LogRenja_90.id_renja
                        JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
                        JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
                        JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
                        JOIN RelTujProg ON RelTujProg.id_program_90 = Program_90.id_program
                        JOIN Tujuan ON Tujuan.id_tujuan = RelTujProg.id_tujuan
                        JOIN RelSasTuj ON RelSasTuj.id_tujuan = Tujuan.id_tujuan
                        JOIN Sasaran ON Sasaran.id_sasaran = RelSasTuj.id_sasaran AND Sasaran.id_misi = Tujuan.id_misi
                        JOIN Misi ON Misi.id_misi = Sasaran.id_misi
                        WHERE Renja_90.periode_usulan >= ?
                            AND Renja_90.periode_usulan <= ?
                            AND Renja_90.id_skpd = ?
                        GROUP BY
                            Misi.id_misi,
                            Misi.misi,
                            Sasaran.id_sasaran,
                            Sasaran.sasaran,
                            Tujuan.id_tujuan,
                            Tujuan.tujuan,
                            Program_90.id_program,
                            Program_90.kode_program_v2,
                            Program_90.nama_program_v2,
                            Kegiatan_90.id_kegiatan,
                            Kegiatan_90.kode_kegiatan,
                            Kegiatan_90.nama_kegiatan,
                            SubKegiatan_90.id_sub_kegiatan,
                            SubKegiatan_90.kode_sub_kegiatan,
                            SubKegiatan_90.nama_sub_kegiatan,
                            Renja_90.periode_usulan
                    ", array($periode->tahun_awal, $periode->tahun_akhir, $pd));
        }
        

        $data = null;
        // variable-variable untuk membantu proses olah data misi 
        $index_misi = 0;
        $temp_id_misi = '-';
        $temp_misi = '-';
        $temp_apbd_misi = 0;
        $temp_r_misi = 0;
        $temp_apbd1_misi = 0;    
        $temp_apbd2_misi = 0;    
        $temp_apbd3_misi = 0;    
        $temp_apbd4_misi = 0;    
        $temp_apbd5_misi = 0;
        $temp_r1_misi = 0;
        $temp_r2_misi = 0;
        $temp_r3_misi = 0;
        $temp_r4_misi = 0;
        $temp_r5_misi = 0;
        
        // variable-variable untuk membantu proses olah data tujuan
        $index_tujuan = 0;
        $temp_id_tujuan = '-';
        $temp_tujuan = '-';
        $temp_apbd_tujuan = 0;
        $temp_r_tujuan = 0;
        $temp_apbd1_tujuan = 0;    
        $temp_apbd2_tujuan = 0;    
        $temp_apbd3_tujuan = 0;    
        $temp_apbd4_tujuan = 0;    
        $temp_apbd5_tujuan = 0;
        $temp_r1_tujuan = 0;
        $temp_r2_tujuan = 0;
        $temp_r3_tujuan = 0;
        $temp_r4_tujuan = 0;
        $temp_r5_tujuan = 0;
        $tujuan = null;

        // variable-variable untuk membantu proses olah data sasaran
        $index_sasaran = 0;
        $temp_id_sasaran = '-';
        $temp_sasaran = '-';
        $temp_apbd_sasaran = 0;
        $temp_r_sasaran = 0;
        $temp_apbd1_sasaran = 0;    
        $temp_apbd2_sasaran = 0;    
        $temp_apbd3_sasaran = 0;    
        $temp_apbd4_sasaran = 0;    
        $temp_apbd5_sasaran = 0;
        $temp_r1_sasaran = 0;
        $temp_r2_sasaran = 0;
        $temp_r3_sasaran = 0;
        $temp_r4_sasaran = 0;
        $temp_r5_sasaran = 0;
        $sasaran = null;

        // variable-variable untuk membantu proses olah data program
        $index_program = 0;
        $temp_id_program = '-';
        $temp_kode_program = '-';
        $temp_program = '-';
        $temp_apbd_prog = 0;
        $temp_r_prog = 0;
        $temp_apbd1_prog = 0;    
        $temp_apbd2_prog = 0;    
        $temp_apbd3_prog = 0;    
        $temp_apbd4_prog = 0;    
        $temp_apbd5_prog = 0;
        $temp_r1_prog = 0;
        $temp_r2_prog = 0;
        $temp_r3_prog = 0;
        $temp_r4_prog = 0;
        $temp_r5_prog = 0;
        $program = null;

        // variable-variable untuk membantu proses olah data kegiatan
        $index_kegiatan = 0;
        $temp_id_kegiatan = '-';
        $temp_kode_kegiatan = '-';
        $temp_kegiatan = '-';
        $temp_apbd_keg = 0;
        $temp_r_keg = 0;
        $temp_apbd1_keg = 0;    
        $temp_apbd2_keg = 0;    
        $temp_apbd3_keg = 0;    
        $temp_apbd4_keg = 0;    
        $temp_apbd5_keg = 0;
        $temp_r1_keg = 0;
        $temp_r2_keg = 0;
        $temp_r3_keg = 0;
        $temp_r4_keg = 0;
        $temp_r5_keg = 0;
        $kegiatan = null;

        // variable-variable untuk membantu proses olah data sub kegiatan
        $index_sub_kegiatan = 0;
        $temp_id_sub_kegiatan = '-';
        $temp_kode_sub_kegiatan = '-';
        $temp_sub_kegiatan = '-';
        $temp_apbd_subkeg = 0;
        $temp_r_subkeg = 0;
        $temp_apbd1_subkeg = 0;    
        $temp_apbd2_subkeg = 0;    
        $temp_apbd3_subkeg = 0;    
        $temp_apbd4_subkeg = 0;    
        $temp_apbd5_subkeg = 0;
        $temp_r1_subkeg = 0;
        $temp_r2_subkeg = 0;
        $temp_r3_subkeg = 0;
        $temp_r4_subkeg = 0;
        $temp_r5_subkeg = 0;
        $sub_kegiatan = null;

        foreach($dasar as $dat)
        {
            // Sub Kegiatan
            if($temp_id_sub_kegiatan != '-' && $temp_id_sub_kegiatan != $dat->id_sub_kegiatan)
            {
                $sub_kegiatan[] = array(
                    'id_sub_kegiatan' => $temp_id_sub_kegiatan,
                    'kode_sub_kegiatan' => $temp_kode_sub_kegiatan,
                    'nama_sub_kegiatan' => $temp_sub_kegiatan,
                    'apbd' => $temp_apbd_subkeg,
                    'realisasi' => $temp_r_subkeg,
                    'apbd_1' => $temp_apbd1_subkeg,
                    'apbd_2' => $temp_apbd2_subkeg,
                    'apbd_3' => $temp_apbd3_subkeg,
                    'apbd_4' => $temp_apbd4_subkeg,
                    'apbd_5' => $temp_apbd5_subkeg,
                    'realisasi_1' => $temp_r1_subkeg,
                    'realisasi_2' => $temp_r2_subkeg,
                    'realisasi_3' => $temp_r3_subkeg,
                    'realisasi_4' => $temp_r4_subkeg,
                    'realisasi_5' => $temp_r5_subkeg
                );
                
                $temp_apbd_subkeg = 0;
                $temp_r_subkeg = 0;    
                $temp_apbd1_subkeg = 0;    
                $temp_apbd2_subkeg = 0;    
                $temp_apbd3_subkeg = 0;    
                $temp_apbd4_subkeg = 0;    
                $temp_apbd5_subkeg = 0;
                $temp_r1_subkeg = 0;
                $temp_r2_subkeg = 0;
                $temp_r3_subkeg = 0;
                $temp_r4_subkeg = 0;
                $temp_r5_subkeg = 0;
            }

            // Kegiatan
            if($temp_id_kegiatan != '-' && $temp_id_kegiatan != $dat->id_kegiatan)
            {
                $kegiatan[] = array(
                    'id_kegiatan' => $temp_id_kegiatan,
                    'kode_kegiatan' => $temp_kode_kegiatan,
                    'nama_kegiatan' => $temp_kegiatan,
                    'apbd' => $temp_apbd_keg,
                    'realisasi' => $temp_r_keg,
                    'apbd_1' => $temp_apbd1_keg,
                    'apbd_2' => $temp_apbd2_keg,
                    'apbd_3' => $temp_apbd3_keg,
                    'apbd_4' => $temp_apbd4_keg,
                    'apbd_5' => $temp_apbd5_keg,
                    'realisasi_1' => $temp_r1_keg,
                    'realisasi_2' => $temp_r2_keg,
                    'realisasi_3' => $temp_r3_keg,
                    'realisasi_4' => $temp_r4_keg,
                    'realisasi_5' => $temp_r5_keg,
                    'sub_kegiatan' => $sub_kegiatan
                );
                
                $temp_apbd_keg= 0;
                $temp_r_keg = 0;
                $temp_apbd1_keg = 0;    
                $temp_apbd2_keg = 0;    
                $temp_apbd3_keg = 0;    
                $temp_apbd4_keg = 0;    
                $temp_apbd5_keg = 0;
                $temp_r1_keg = 0;
                $temp_r2_keg = 0;
                $temp_r3_keg = 0;
                $temp_r4_keg = 0;
                $temp_r5_keg = 0;

                $sub_kegiatan = null;
            }

            // Program
            if($temp_id_program != '-' && $temp_id_program != $dat->id_program)
            {
                $program[] = array(
                    'id_program' => $temp_id_program,
                    'kode_program' => $temp_kode_program,
                    'nama_program' => $temp_program,
                    'apbd' => $temp_apbd_prog,
                    'realisasi' => $temp_r_prog,
                    'apbd_1' => $temp_apbd1_prog,
                    'apbd_2' => $temp_apbd2_prog,
                    'apbd_3' => $temp_apbd3_prog,
                    'apbd_4' => $temp_apbd4_prog,
                    'apbd_5' => $temp_apbd5_prog,
                    'realisasi_1' => $temp_r1_prog,
                    'realisasi_2' => $temp_r2_prog,
                    'realisasi_3' => $temp_r3_prog,
                    'realisasi_4' => $temp_r4_prog,
                    'realisasi_5' => $temp_r5_prog,
                    'kegiatan' => $kegiatan
                );
                
                $temp_apbd_prog= 0;
                $temp_r_prog = 0;
                $temp_apbd1_prog = 0;    
                $temp_apbd2_prog = 0;    
                $temp_apbd3_prog = 0;    
                $temp_apbd4_prog = 0;    
                $temp_apbd5_prog = 0;
                $temp_r1_prog = 0;
                $temp_r2_prog = 0;
                $temp_r3_prog = 0;
                $temp_r4_prog = 0;
                $temp_r5_prog = 0;

                $kegiatan = null;
            }

            // Sasaran
            if($temp_id_sasaran != '-' && $temp_id_sasaran != $dat->id_sasaran)
            {
                $isas = null;
                if(array_key_exists($temp_id_misi,$indikator_sasaran))
                {
                    if(array_key_exists($temp_id_tujuan,$indikator_sasaran[$temp_id_misi]))
                    {                     
                        if(array_key_exists($temp_id_sasaran,$indikator_sasaran[$temp_id_misi][$temp_id_tujuan]))
                        {
                            $isas = $indikator_sasaran[$temp_id_misi][$temp_id_tujuan][$temp_id_sasaran];
                        }   
                    }                    
                }
                $sasaran[] = array(
                    'id_sasaran' => $temp_id_sasaran,
                    'nama_sasaran' => $temp_sasaran,
                    'apbd' => $temp_apbd_sasaran,
                    'realisasi' => $temp_r_sasaran,
                    'apbd_1' => $temp_apbd1_sasaran,
                    'apbd_2' => $temp_apbd2_sasaran,
                    'apbd_3' => $temp_apbd3_sasaran,
                    'apbd_4' => $temp_apbd4_sasaran,
                    'apbd_5' => $temp_apbd5_sasaran,
                    'realisasi_1' => $temp_r1_sasaran,
                    'realisasi_2' => $temp_r2_sasaran,
                    'realisasi_3' => $temp_r3_sasaran,
                    'realisasi_4' => $temp_r4_sasaran,
                    'realisasi_5' => $temp_r5_sasaran,
                    'indikator' => $isas,
                    'program' => $program
                );
                
                $temp_apbd_sasaran= 0;
                $temp_r_sasaran = 0;
                $temp_apbd1_sasaran = 0;    
                $temp_apbd2_sasaran = 0;    
                $temp_apbd3_sasaran = 0;    
                $temp_apbd4_sasaran = 0;    
                $temp_apbd5_sasaran = 0;
                $temp_r1_sasaran = 0;
                $temp_r2_sasaran = 0;
                $temp_r3_sasaran = 0;
                $temp_r4_sasaran = 0;
                $temp_r5_sasaran = 0;

                $program = null;
            }

            // Tujuan
            if($temp_id_tujuan != '-' && $temp_id_tujuan != $dat->id_tujuan)
            {
                $tujuan[] = array(
                    'id_tujuan' => $temp_id_tujuan,
                    'nama_tujuan' => $temp_tujuan,
                    'apbd' => $temp_apbd_tujuan,
                    'realisasi' => $temp_r_tujuan,
                    'apbd_1' => $temp_apbd1_tujuan,
                    'apbd_2' => $temp_apbd2_tujuan,
                    'apbd_3' => $temp_apbd3_tujuan,
                    'apbd_4' => $temp_apbd4_tujuan,
                    'apbd_5' => $temp_apbd5_tujuan,
                    'realisasi_1' => $temp_r1_tujuan,
                    'realisasi_2' => $temp_r2_tujuan,
                    'realisasi_3' => $temp_r3_tujuan,
                    'realisasi_4' => $temp_r4_tujuan,
                    'realisasi_5' => $temp_r5_tujuan,
                    'indikator' => $indikator_tujuan[$temp_id_misi][$temp_id_tujuan],
                    'sasaran' => $sasaran
                );
                
                $temp_apbd_tujuan= 0;
                $temp_r_tujuan = 0;
                $temp_apbd1_tujuan = 0;    
                $temp_apbd2_tujuan = 0;    
                $temp_apbd3_tujuan = 0;    
                $temp_apbd4_tujuan = 0;    
                $temp_apbd5_tujuan = 0;
                $temp_r1_tujuan = 0;
                $temp_r2_tujuan = 0;
                $temp_r3_tujuan = 0;
                $temp_r4_tujuan = 0;
                $temp_r5_tujuan = 0;

                $sasaran = null;
            }

            // Misi
            if($temp_id_misi != '-' && $temp_id_misi != $dat->id_misi)
            {
                $data['misi'][] = array(
                    'id_misi' => $temp_id_misi,
                    'misi' => $temp_misi,
                    'apbd' => $temp_apbd_misi,
                    'realisasi' => $temp_r_misi,
                    'apbd_1' => $temp_apbd1_misi,
                    'apbd_2' => $temp_apbd2_misi,
                    'apbd_3' => $temp_apbd3_misi,
                    'apbd_4' => $temp_apbd4_misi,
                    'apbd_5' => $temp_apbd5_misi,
                    'realisasi_1' => $temp_r1_misi,
                    'realisasi_2' => $temp_r2_misi,
                    'realisasi_3' => $temp_r3_misi,
                    'realisasi_4' => $temp_r4_misi,
                    'realisasi_5' => $temp_r5_misi,
                    'tujuan' => $tujuan
                );

                $temp_apbd_misi= 0;
                $temp_r_misi = 0;
                $temp_apbd1_misi = 0;    
                $temp_apbd2_misi = 0;    
                $temp_apbd3_misi = 0;    
                $temp_apbd4_misi = 0;    
                $temp_apbd5_misi = 0;
                $temp_r1_misi = 0;
                $temp_r2_misi = 0;
                $temp_r3_misi = 0;
                $temp_r4_misi = 0;
                $temp_r5_misi = 0;

                $tujuan = null;
            }

            // Anggaran per 1 tahun
            
            if($deretTahun[0] == $dat->periode_usulan)
            {
                $temp_apbd1_subkeg = $temp_apbd1_subkeg+$dat->apbd_kota;
                $temp_r1_subkeg = $temp_r1_subkeg+$dat->realisasi;
                $temp_apbd1_keg = $temp_apbd1_keg+$dat->apbd_kota;
                $temp_r1_keg = $temp_r1_keg+$dat->realisasi;
                $temp_apbd1_prog = $temp_apbd1_prog+$dat->apbd_kota;
                $temp_r1_prog = $temp_r1_prog+$dat->realisasi;
                $temp_apbd1_sasaran = $temp_apbd1_sasaran+$dat->apbd_kota;
                $temp_r1_sasaran = $temp_r1_sasaran+$dat->realisasi;
                $temp_apbd1_tujuan = $temp_apbd1_tujuan+$dat->apbd_kota;
                $temp_r1_tujuan = $temp_r1_tujuan+$dat->realisasi;
                $temp_apbd1_misi = $temp_apbd1_misi+$dat->apbd_kota;
                $temp_r1_misi = $temp_r1_misi+$dat->realisasi;
            }
            if($deretTahun[1] == $dat->periode_usulan)
            {
                $temp_apbd2_subkeg = $temp_apbd2_subkeg+$dat->apbd_kota;
                $temp_r2_subkeg = $temp_r2_subkeg+$dat->realisasi;
                $temp_apbd2_keg = $temp_apbd2_keg+$dat->apbd_kota;
                $temp_r2_keg = $temp_r2_keg+$dat->realisasi;
                $temp_apbd2_prog = $temp_apbd2_prog+$dat->apbd_kota;
                $temp_r2_prog = $temp_r2_prog+$dat->realisasi;
                $temp_apbd2_sasaran = $temp_apbd2_sasaran+$dat->apbd_kota;
                $temp_r2_sasaran = $temp_r2_sasaran+$dat->realisasi;
                $temp_apbd2_tujuan = $temp_apbd2_tujuan+$dat->apbd_kota;
                $temp_r2_tujuan = $temp_r2_tujuan+$dat->realisasi;
                $temp_apbd2_misi = $temp_apbd2_misi+$dat->apbd_kota;
                $temp_r2_misi = $temp_r2_misi+$dat->realisasi;
            }
            if($deretTahun[2] == $dat->periode_usulan)
            {
                $temp_apbd3_subkeg = $temp_apbd3_subkeg+$dat->apbd_kota;
                $temp_r3_subkeg = $temp_r3_subkeg+$dat->realisasi;
                $temp_apbd3_keg = $temp_apbd3_keg+$dat->apbd_kota;
                $temp_r3_keg = $temp_r3_keg+$dat->realisasi;
                $temp_apbd3_prog = $temp_apbd3_prog+$dat->apbd_kota;
                $temp_r3_prog = $temp_r3_prog+$dat->realisasi;
                $temp_apbd3_sasaran = $temp_apbd3_sasaran+$dat->apbd_kota;
                $temp_r3_sasaran = $temp_r3_sasaran+$dat->realisasi;
                $temp_apbd3_tujuan = $temp_apbd3_tujuan+$dat->apbd_kota;
                $temp_r3_tujuan = $temp_r3_tujuan+$dat->realisasi;
                $temp_apbd3_misi = $temp_apbd3_misi+$dat->apbd_kota;
                $temp_r3_misi = $temp_r3_misi+$dat->realisasi;
            }
            if($deretTahun[3] == $dat->periode_usulan)
            {
                $temp_apbd4_subkeg = $temp_apbd4_subkeg+$dat->apbd_kota;
                $temp_r4_subkeg = $temp_r4_subkeg+$dat->realisasi;
                $temp_apbd4_keg = $temp_apbd4_keg+$dat->apbd_kota;
                $temp_r4_keg = $temp_r4_keg+$dat->realisasi;
                $temp_apbd4_prog = $temp_apbd4_prog+$dat->apbd_kota;
                $temp_r4_prog = $temp_r4_prog+$dat->realisasi;
                $temp_apbd4_sasaran = $temp_apbd4_sasaran+$dat->apbd_kota;
                $temp_r4_sasaran = $temp_r4_sasaran+$dat->realisasi;
                $temp_apbd4_tujuan = $temp_apbd4_tujuan+$dat->apbd_kota;
                $temp_r4_tujuan = $temp_r4_tujuan+$dat->realisasi;
                $temp_apbd4_misi = $temp_apbd4_misi+$dat->apbd_kota;
                $temp_r4_misi = $temp_r4_misi+$dat->realisasi;
            }
            if($deretTahun[4] == $dat->periode_usulan)
            {
                $temp_apbd5_subkeg = $temp_apbd5_subkeg+$dat->apbd_kota;
                $temp_r5_subkeg = $temp_r5_subkeg+$dat->realisasi;
                $temp_apbd5_keg = $temp_apbd5_keg+$dat->apbd_kota;
                $temp_r5_keg = $temp_r5_keg+$dat->realisasi;
                $temp_apbd5_prog = $temp_apbd5_prog+$dat->apbd_kota;
                $temp_r5_prog = $temp_r5_prog+$dat->realisasi;
                $temp_apbd5_sasaran = $temp_apbd5_sasaran+$dat->apbd_kota;
                $temp_r5_sasaran = $temp_r5_sasaran+$dat->realisasi;
                $temp_apbd5_tujuan = $temp_apbd5_tujuan+$dat->apbd_kota;
                $temp_r5_tujuan = $temp_r5_tujuan+$dat->realisasi;
                $temp_apbd5_misi = $temp_apbd5_misi+$dat->apbd_kota;
                $temp_r5_misi = $temp_r5_misi+$dat->realisasi;
            }

            // Temporary Untuk Data Sub Kegiatan
            $temp_id_sub_kegiatan = $dat->id_sub_kegiatan;
            $temp_kode_sub_kegiatan = $dat->kode_sub_kegiatan;
            $temp_sub_kegiatan = $dat->nama_sub_kegiatan;
            $temp_apbd_subkeg = $temp_apbd_subkeg+$dat->apbd_kota;
            $temp_r_subkeg = $temp_r_subkeg+$dat->realisasi;

            // Temporary Untuk Data Kegiatan
            $temp_id_kegiatan = $dat->id_kegiatan;
            $temp_kode_kegiatan = $dat->kode_kegiatan;
            $temp_kegiatan = $dat->nama_kegiatan;
            $temp_apbd_keg = $temp_apbd_keg+$dat->apbd_kota;
            $temp_r_keg = $temp_r_keg+$dat->realisasi;

            // Temporary Untuk Data Program
            $temp_id_program = $dat->id_program;
            $temp_kode_program = $dat->kode_program;
            $temp_program = $dat->program;
            $temp_apbd_prog = $temp_apbd_prog+$dat->apbd_kota;
            $temp_r_prog = $temp_r_prog+$dat->realisasi;

            // Temporary Untuk Data Sasaran
            $temp_id_sasaran = $dat->id_sasaran;
            $temp_sasaran = $dat->sasaran;
            $temp_apbd_sasaran = $temp_apbd_sasaran+$dat->apbd_kota;
            $temp_r_sasaran = $temp_r_sasaran+$dat->realisasi;

            // Temporary Untuk Data Tujuan
            $temp_id_tujuan = $dat->id_tujuan;
            $temp_tujuan = $dat->tujuan;
            $temp_apbd_tujuan = $temp_apbd_tujuan+$dat->apbd_kota;
            $temp_r_tujuan = $temp_r_tujuan+$dat->realisasi;

            // Temporary Untuk Data Misi
            $temp_id_misi = $dat->id_misi;
            $temp_misi = $dat->misi;     
            $temp_apbd_misi = $temp_apbd_misi+$dat->apbd_kota;
            $temp_r_misi = $temp_r_misi+$dat->realisasi;       
        }
        return API_RENJAResource::collection($data);
    }

    public function get_from_python(Request $request)
    {
        $tahun_awal = $request->tahun_awal;
        $tahun_akhir = $request->tahun_akhir;
        $string = "my_function(".$tahun_awal.",".$tahun_akhir.")";
        $path = storage_path('python\query.py');

        $process = shell_exec("python ".$path);
        // $process = new Process("python 'f:\query.py'  \"{$tahun_awal}\"");
        // $process = new Process("python -c 'import query; query.my_function(".$tahun_awal.",".$tahun_akhir.")'");
        // $process->run();
        // // executes after the command finishes
        // if (!$process->isSuccessful()) {
        //     throw new ProcessFailedException($process);
        // }
        return response()->json($process);
        // return storage_path();
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use Session;
use Illuminate\Support\Facades\DB;

// IKU, IKK, IKP Lama
use App\Models\AsistensiRenja;
use App\Models\Bidang;
use App\Models\Data_IKK_Excel;
use App\Models\JenisUrusan;
use App\Models\KategoriUrusan;
use App\Models\Kegiatan;
use App\Models\Lima_IndikatorProgram;
use App\Models\Lima_IndikatorRPJMD;
use App\Models\Lima_IndikatorSasaranRPJMD;
use App\Models\Lima_KebijakanRenstra;
use App\Models\Lima_KegiatanRenstra;
use App\Models\Lima_MisiRPJMD;
use App\Models\Lima_Periode;
use App\Models\Lima_ProgramRPJMD;
use App\Models\Lima_ProgramSKPD;
use App\Models\Lima_Renstra;
use App\Models\Lima_RPJMD;
use App\Models\Lima_SasaranRPJMD;
use App\Models\Lima_TujuanRPJMD;
use App\Models\LimaP_IndikatorProgramRPJMD;
use App\Models\LimaP_IndikatorSasaranRPJMD;
use App\Models\Log_R_MonevRKPD;
use App\Models\LogRenja;
use App\Models\MonevRKPDDetail;
use App\Models\MonevRKPDHead;
use App\Models\PrioritasRKPD;
use App\Models\Program;
use App\Models\R_IKK;
use App\Models\R_IKU;
use App\Models\Renja;
use App\Models\SasaranPrioritasRKPD;
use App\Models\SKPD;
use App\Models\StatusCapaian;
use App\Models\Urusan;

// IKU, IKK, IKP Baru
use App\Models\KetercapaianRPJMD;
use App\Models\KategoriRPJMD;
use App\Models\SatuanRPJMD;
use App\Models\Periode;
use App\Models\Rumus;
use App\Models\RPJMD;
use App\Models\MisiRPJMD;
use App\Models\TujuanRPJMD;
use App\Models\IndikatorTujuanRPJMD;
use App\Models\SasaranRPJMD;
use App\Models\IndikatorSasaranRPJMD;
use App\Models\StrategiRPJMD;
use App\Models\UrusanRPJMD;
use App\Models\ProgramRPJMD;
use App\Models\IndikatorProgramRPJMD;
use App\Models\Aspek;
use App\Models\IndikatorKinerjaRPJMD;
use App\Models\SumberData;
use App\Models\TargetRealisasiTujuanIKU;
use App\Models\TargetRealisasiSasaranIKU;
use App\Models\TargetRealisasiIKK;
use App\Models\TargetRealisasiIKP;
use App\Models\FaktorPendorongIKP;
use App\Models\FaktorPenghambatIKP;
use App\Models\TindakLanjutIKP;
use App\Models\FaktorPendorongTujuanIKU;
use App\Models\FaktorPenghambatTujuanIKU;
use App\Models\TindakLanjutTujuanIKU;
use App\Models\FaktorPendorongSasaranIKU;
use App\Models\FaktorPenghambatSasaranIKU;
use App\Models\TindakLanjutSasaranIKU;
use App\Models\Tahun;
use App\Models\SKPD_90;
use App\Models\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Api_RKPDController extends Controller
{
    //Tabel IKU    
    public function getIKU()
    {
        $data = Lima_IndikatorSasaranRPJMD::get();
        return API_SimrendaResource::collection($data);
    } 
    //Tabel IKK    
    public function getIKK()
    {
        $data = Data_IKK_Excel::get();
        return API_SimrendaResource::collection($data);
    } 
    //Tabel IKP
    public function getIKP()
    {        
        $data = Lima_ProgramRPJMD::get();
        return API_SimrendaResource::collection($data);
    }
    //Tabel SKPD
    public function getSKPD()
    {        
        $data = SKPD::get();
        return API_SimrendaResource::collection($data);
    }

    /********************************* 
     *      MDOUL MONEV RKPD         *
    **********************************/
    public $thn = null;
    public $id_skpd = null;
    // Monev Hasil IKU Tujuan
    public function monevHasilIKUTujuan($tahun,$pd)
    {
        $this->thn = $tahun;
        $ikuTujuan =  TargetRealisasiTujuanIKU::with(
                                                    'IndikatorTujuanRPJMD',
                                                    'IndikatorTujuanRPJMD.SatuanRPJMD',
                                                    'Tahun',
                                                    'KetercapaianRPJMD',
                                                    'KategoriRPJMD'
                                                )
                                                ->whereHas('Tahun', function($q)
                                                {
                                                    $q->where('tahun',$this->thn);
                                                })
                                                ->get();
        return API_SimrendaResource::collection($ikuTujuan);
    }
    
    // Monev Hasil IKU Sasaran
    public function monevHasilIKUSasaran($tahun,$pd)
    {
        $this->thn = $tahun;
        $this->id_skpd = SKPD_90::select('id_skpd')->where('nama_skpd','=',$pd)->first();
        $ikuSasaran =  TargetRealisasiSasaranIKU::with(
                                                    'IndikatorSasaranRPJMD',
                                                    'IndikatorSasaranRPJMD.SatuanRPJMD',
                                                    'Tahun',
                                                    'KetercapaianRPJMD',
                                                    'KategoriRPJMD'
                                                )
                                                ->whereHas('Tahun', function($q)
                                                {
                                                    $q->where('tahun',$this->thn);
                                                })
                                                ->get();
        return API_SimrendaResource::collection($ikuSasaran);
    }

    // Monev Hasil IKK
    public function monevHasilIKK($tahun,$pd)
    {
        $this->thn = $tahun;
        $ikk = null;
        $dataMerge = null;
        $aspek = Aspek::get();
        $this->id_skpd = $pd;

        if($this->thn > 2020)
        {
            if(!is_null($this->id_skpd) && $this->id_skpd != '1' && $this->id_skpd != '539')
            {
                $ikk = TargetRealisasiIKK::whereHas('IndikatorKinerjaRPJMD', function($q)
                {
                    $q->whereIn('id_skpd', [$this->id_skpd]);
        
                })
                ->whereHas('Tahun', function($q)
                {
                    $q->where('Tahun',$this->thn);
                })
                ->get();
            }else{
                $ikk = TargetRealisasiIKK::whereHas('Tahun', function($q)
                {
                    $q->where('Tahun',$this->thn);
                })
                ->get();
            }

            foreach($ikk as $v)
            {
                $dataMerge[] = array(
                    'id_target_realisasi' => $v->id_target_realisasi,
                    'indikator_kinerja' => $v->IndikatorKinerjaRPJMD->indikator_kinerja,
                    'target' => $v->target,
                    'realisasi' => $v->realisasi,
                    'satuan' => $v->IndikatorKinerjaRPJMD->SatuanRPJMD->satuan,
                    'id_ketercapaian' => $v->id_ketercapaian,
                    'id_kategori' => $v->id_kategori,
                    'ketercapaian' => $v->KetercapaianRPJMD->ketercapaian,
                    'kategori' => $v->KategoriRPJMD->kategori,
                    'tahun' => $v->Tahun->tahun,
                    'nama_skpd' => $v->IndikatorKinerjaRPJMD->SKPD->nama_skpd,
                    'id_rumus' => $v->IndikatorKinerjaRPJMD->id_rumus
                );
            }
        }
        else if($this->thn <= 2020)
        {
            if(!is_null($this->id_skpd) && $this->id_skpd != '1' && $this->id_skpd != '539')
            {
                $ikkLama = DB::table('Data_IKK_Excel')
                            ->join('R_IKK', 'R_IKK.id_ikk', '=', 'Data_IKK_Excel.id_ikk')
                            ->join('SKPD', 'SKPD.akronim', '=', 'Data_IKK_Excel.sumber_data')
                            ->join('Bidang', 'Bidang.id_bidang', '=', 'SKPD.id_bidang')
                            ->orderBy('Data_IKK_Excel.aspek', 'ASC')
                            ->orderBy('Data_IKK_Excel.fokus', 'ASC')
                            ->where('SKPD.id_skpd',$this->id_skpd)
                            ->get();
            }else{
                $ikkLama = DB::table('Data_IKK_Excel')
                            ->join('R_IKK', 'R_IKK.id_ikk', '=', 'Data_IKK_Excel.id_ikk')
                            ->join('SKPD', 'SKPD.akronim', '=', 'Data_IKK_Excel.sumber_data')
                            ->join('Bidang', 'Bidang.id_bidang', '=', 'SKPD.id_bidang')
                            ->orderBy('Data_IKK_Excel.aspek', 'ASC')
                            ->orderBy('Data_IKK_Excel.fokus', 'ASC')
                            ->get();
            }

            foreach($ikkLama as $v)
            {
                if($v->formula == 'positif')
                {
                    $id_rumus = 1;
                }else
                {
                    $id_rumus = 2;
                }            
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_kinerja' => $v->ikk,
                    'target' => $v->t2018,
                    'realisasi' => $v->r_1,
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => '-',
                    'kategori' => '-',
                    'tahun' => '2018',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => $id_rumus
                );            
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_kinerja' => $v->ikk,
                    'target' => $v->t2019,
                    'realisasi' => $v->r_2,
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => '-',
                    'kategori' => '-',
                    'tahun' => '2019',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => $id_rumus
                );            
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_kinerja' => $v->ikk,
                    'target' => $v->t2020,
                    'realisasi' => $v->r_3,
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => '-',
                    'kategori' => '-',
                    'tahun' => '2020',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => $id_rumus
                );
            }
        }

        return API_SimrendaResource::collection($dataMerge);
    }

    // Monev Hasil IKP
    public function monevHasilIKP($tahun,$pd)
    {
        $this->thn = $tahun;
        $ikp = null;
        $dataMerge = null;
        $this->id_skpd = $pd;

        if($this->thn > 2020)
        {
            if(!is_null($this->id_skpd) && $this->id_skpd != '1' && $this->id_skpd != '539')
            {
                $ikp = TargetRealisasiIKP::whereHas('IndikatorProgramRPJMD', function($q)
                {
                    $q->whereIn('id_skpd', [$this->id_skpd]);
        
                })->whereHas('Tahun', function($q) {
                    $q->where('tahun', $this->thn);
                })->get();
            }else{
                $ikp = TargetRealisasiIKP::whereHas('Tahun', function($q) {
                    $q->where('tahun', $this->thn);
                })->get();
            }

            foreach($ikp as $v)
            {
                $dataMerge[] = array(
                    'id_target_realisasi' => $v->id_target_realisasi,
                    'indikator_program' => $v->IndikatorProgramRPJMD->indikator_program,
                    'target' => $v->target,
                    'realisasi' => $v->realisasi,
                    'satuan' => $v->IndikatorProgramRPJMD->SatuanRPJMD->satuan,
                    'id_ketercapaian' => $v->KetercapaianRPJMD->id_ketercapaian,
                    'id_kategori' => $v->KategoriRPJMD->id_kategori,
                    'ketercapaian' => $v->KetercapaianRPJMD->ketercapaian,
                    'kategori' => $v->KategoriRPJMD->kategori,
                    'tahun' => $v->Tahun->tahun,
                    'nama_skpd' => $v->IndikatorProgramRPJMD->SKPD->nama_skpd,
                    'id_rumus' => $v->IndikatorProgramRPJMD->id_rumus
                );
            }
        }else if($this->thn <= 2020)
        {      
            if(!is_null($this->id_skpd) && $this->id_skpd != '1' && $this->id_skpd != '539')
            {      
                $trIKPLama = DB::table('Lima_ProgramRPJMD')
                    ->join('Program', 'Lima_ProgramRPJMD.id_program', '=', 'Program.id_program')
                    ->join('Urusan', 'Urusan.id_urusan', '=', 'Program.id_urusan')
                    ->join('Lima_IndikatorRPJMD', 'Lima_ProgramRPJMD.id_program_rpjmd', '=', 'Lima_IndikatorRPJMD.id_program_rpjmd')
                    ->join('Lima_IndikatorProgram', 'Lima_IndikatorProgram.id_indikator_program', '=', 'Lima_IndikatorRPJMD.id_indikator_program')
                    ->join('Lima_ProgramSKPD', 'Lima_ProgramSKPD.id_program_rpjmd', '=', 'Lima_ProgramRPJMD.id_program_rpjmd')
                    ->join('SKPD', 'SKPD.id_skpd', '=', 'Lima_ProgramSKPD.id_skpd')
                    ->join('Bidang', 'Bidang.id_bidang', '=', 'SKPD.id_bidang')
                    ->leftJoin('MonevRKPDHead', function($join) {
                            $join->on('MonevRKPDHead.id_program_rpjmd', '=', 'Lima_ProgramRPJMD.id_program_rpjmd');
                            $join->on('MonevRKPDHead.id_skpd', '=', 'SKPD.id_skpd');
                            $join->where('MonevRKPDHead.tahun', '=', '2019');
                        })
                    ->leftJoin('MonevRKPDDetail', function($join) {
                            $join->on('MonevRKPDDetail.id_monev_rkpd', '=', 'MonevRKPDHead.id_monev_rkpd');
                            $join->on('MonevRKPDDetail.id_indikator_program', '=', 'Lima_IndikatorProgram.id_indikator_program');
                        })
                    ->select(
                            'Bidang.bidang',
                            'SKPD.akronim', 
                            'Lima_ProgramRPJMD.id_program_rpjmd',
                            'Program.nama_program',
                            'Lima_IndikatorProgram.indikator_program',
                            'Lima_IndikatorProgram.id_indikator_program',
                            'SKPD.id_skpd',
                            'Lima_IndikatorRPJMD.t_1',
                            'Lima_IndikatorRPJMD.t_2',
                            'Lima_IndikatorRPJMD.t_3',
                            'Lima_IndikatorRPJMD.t_4',
                            'Lima_IndikatorRPJMD.t_5',
                            'Lima_IndikatorRPJMD.kondisi_awal',
                            'Lima_IndikatorRPJMD.t_a',
                            'Lima_IndikatorProgram.satuan',
                            'Urusan.nama_urusan',
                            'Urusan.id_urusan',
                            'Lima_ProgramRPJMD.rp_1',
                            'Lima_ProgramRPJMD.rp_2',
                            'Lima_ProgramRPJMD.rp_3',
                            'Lima_ProgramRPJMD.rp_4',
                            'Lima_ProgramRPJMD.rp_5',
                            'MonevRKPDHead.id_urusan',
                            'MonevRKPDDetail.realisasi_capaian', 
                            'MonevRKPDDetail.sumber_data',
                            'MonevRKPDDetail.tindak_lanjut',
                            'MonevRKPDDetail.faktor_pendorong',
                            'MonevRKPDDetail.faktor_penghambat',
                            'MonevRKPDDetail.formula',
                            'MonevRKPDDetail.status_capaian',
                            'MonevRKPDDetail.id_monev_rkpd_d', 
                            'Urusan.nama_urusan'
                        )
                    ->where('SKPD.isDeleted', '=', '0')
                    ->where('SKPD.id_skpd',$this->id_skpd)
                    ->groupBy(
                            'Bidang.bidang',
                            'SKPD.akronim', 
                            'Lima_ProgramRPJMD.id_program_rpjmd',
                            'Program.nama_program',
                            'Lima_IndikatorProgram.indikator_program',
                            'Lima_IndikatorProgram.id_indikator_program',
                            'SKPD.id_skpd',
                            'Lima_IndikatorRPJMD.t_1',
                            'Lima_IndikatorRPJMD.t_2',
                            'Lima_IndikatorRPJMD.t_3',
                            'Lima_IndikatorRPJMD.t_4',
                            'Lima_IndikatorRPJMD.t_5',
                            'Lima_IndikatorRPJMD.kondisi_awal',
                            'Lima_IndikatorRPJMD.t_a',
                            'Lima_IndikatorProgram.satuan',
                            'Urusan.nama_urusan',
                            'Urusan.id_urusan',
                            'Lima_ProgramRPJMD.rp_1',
                            'Lima_ProgramRPJMD.rp_2',
                            'Lima_ProgramRPJMD.rp_3',
                            'Lima_ProgramRPJMD.rp_4',
                            'Lima_ProgramRPJMD.rp_5',
                            'MonevRKPDHead.id_urusan',
                            'MonevRKPDDetail.realisasi_capaian', 
                            'MonevRKPDDetail.sumber_data',
                            'MonevRKPDDetail.tindak_lanjut',
                            'MonevRKPDDetail.faktor_pendorong',
                            'MonevRKPDDetail.faktor_penghambat',
                            'MonevRKPDDetail.formula',
                            'MonevRKPDDetail.status_capaian',
                            'MonevRKPDDetail.id_monev_rkpd_d',
                            'Lima_IndikatorRPJMD.id_indikator_rpjmd', 
                            'Urusan.nama_urusan'
                        )
                    ->get();
            }else{  
                $trIKPLama = DB::table('Lima_ProgramRPJMD')
                    ->join('Program', 'Lima_ProgramRPJMD.id_program', '=', 'Program.id_program')
                    ->join('Urusan', 'Urusan.id_urusan', '=', 'Program.id_urusan')
                    ->join('Lima_IndikatorRPJMD', 'Lima_ProgramRPJMD.id_program_rpjmd', '=', 'Lima_IndikatorRPJMD.id_program_rpjmd')
                    ->join('Lima_IndikatorProgram', 'Lima_IndikatorProgram.id_indikator_program', '=', 'Lima_IndikatorRPJMD.id_indikator_program')
                    ->join('Lima_ProgramSKPD', 'Lima_ProgramSKPD.id_program_rpjmd', '=', 'Lima_ProgramRPJMD.id_program_rpjmd')
                    ->join('SKPD', 'SKPD.id_skpd', '=', 'Lima_ProgramSKPD.id_skpd')
                    ->join('Bidang', 'Bidang.id_bidang', '=', 'SKPD.id_bidang')
                    ->leftJoin('MonevRKPDHead', function($join) {
                            $join->on('MonevRKPDHead.id_program_rpjmd', '=', 'Lima_ProgramRPJMD.id_program_rpjmd');
                            $join->on('MonevRKPDHead.id_skpd', '=', 'SKPD.id_skpd');
                            $join->where('MonevRKPDHead.tahun', '=', '2019');
                        })
                    ->leftJoin('MonevRKPDDetail', function($join) {
                            $join->on('MonevRKPDDetail.id_monev_rkpd', '=', 'MonevRKPDHead.id_monev_rkpd');
                            $join->on('MonevRKPDDetail.id_indikator_program', '=', 'Lima_IndikatorProgram.id_indikator_program');
                        })
                    ->select(
                            'Bidang.bidang',
                            'SKPD.akronim', 
                            'Lima_ProgramRPJMD.id_program_rpjmd',
                            'Program.nama_program',
                            'Lima_IndikatorProgram.indikator_program',
                            'Lima_IndikatorProgram.id_indikator_program',
                            'SKPD.id_skpd',
                            'Lima_IndikatorRPJMD.t_1',
                            'Lima_IndikatorRPJMD.t_2',
                            'Lima_IndikatorRPJMD.t_3',
                            'Lima_IndikatorRPJMD.t_4',
                            'Lima_IndikatorRPJMD.t_5',
                            'Lima_IndikatorRPJMD.kondisi_awal',
                            'Lima_IndikatorRPJMD.t_a',
                            'Lima_IndikatorProgram.satuan',
                            'Urusan.nama_urusan',
                            'Urusan.id_urusan',
                            'Lima_ProgramRPJMD.rp_1',
                            'Lima_ProgramRPJMD.rp_2',
                            'Lima_ProgramRPJMD.rp_3',
                            'Lima_ProgramRPJMD.rp_4',
                            'Lima_ProgramRPJMD.rp_5',
                            'MonevRKPDHead.id_urusan',
                            'MonevRKPDDetail.realisasi_capaian', 
                            'MonevRKPDDetail.sumber_data',
                            'MonevRKPDDetail.tindak_lanjut',
                            'MonevRKPDDetail.faktor_pendorong',
                            'MonevRKPDDetail.faktor_penghambat',
                            'MonevRKPDDetail.formula',
                            'MonevRKPDDetail.status_capaian',
                            'MonevRKPDDetail.id_monev_rkpd_d', 
                            'Urusan.nama_urusan'
                        )
                    ->where('SKPD.isDeleted', '=', '0')
                    ->groupBy(
                            'Bidang.bidang',
                            'SKPD.akronim', 
                            'Lima_ProgramRPJMD.id_program_rpjmd',
                            'Program.nama_program',
                            'Lima_IndikatorProgram.indikator_program',
                            'Lima_IndikatorProgram.id_indikator_program',
                            'SKPD.id_skpd',
                            'Lima_IndikatorRPJMD.t_1',
                            'Lima_IndikatorRPJMD.t_2',
                            'Lima_IndikatorRPJMD.t_3',
                            'Lima_IndikatorRPJMD.t_4',
                            'Lima_IndikatorRPJMD.t_5',
                            'Lima_IndikatorRPJMD.kondisi_awal',
                            'Lima_IndikatorRPJMD.t_a',
                            'Lima_IndikatorProgram.satuan',
                            'Urusan.nama_urusan',
                            'Urusan.id_urusan',
                            'Lima_ProgramRPJMD.rp_1',
                            'Lima_ProgramRPJMD.rp_2',
                            'Lima_ProgramRPJMD.rp_3',
                            'Lima_ProgramRPJMD.rp_4',
                            'Lima_ProgramRPJMD.rp_5',
                            'MonevRKPDHead.id_urusan',
                            'MonevRKPDDetail.realisasi_capaian', 
                            'MonevRKPDDetail.sumber_data',
                            'MonevRKPDDetail.tindak_lanjut',
                            'MonevRKPDDetail.faktor_pendorong',
                            'MonevRKPDDetail.faktor_penghambat',
                            'MonevRKPDDetail.formula',
                            'MonevRKPDDetail.status_capaian',
                            'MonevRKPDDetail.id_monev_rkpd_d',
                            'Lima_IndikatorRPJMD.id_indikator_rpjmd', 
                            'Urusan.nama_urusan'
                        )
                    ->get();
            }

            foreach($trIKPLama as $v)
            {
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_program' => $v->indikator_program,
                    'target' => $v->t_1,
                    'realisasi' => '-',
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => $v->status_capaian,
                    'kategori' => '-',
                    'tahun' => '2018',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => '-'
                );
    
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_program' => $v->indikator_program,
                    'target' => $v->t_2,
                    'realisasi' => '-',
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => $v->status_capaian,
                    'kategori' => '-',
                    'tahun' => '2019',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => '-'
                );
    
                $dataMerge[] = array(
                    'id_target_realisasi' => '-',
                    'indikator_program' => $v->indikator_program,
                    'target' => $v->t_3,
                    'realisasi' => '-',
                    'satuan' => $v->satuan,
                    'id_ketercapaian' => '-',
                    'id_kategori' => '-',
                    'ketercapaian' => $v->status_capaian,
                    'kategori' => '-',
                    'tahun' => '2020',
                    'nama_skpd' => $v->akronim,
                    'id_rumus' => '-'
                );
            }
        }    
        
        return API_SimrendaResource::collection($dataMerge);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SPMResource;
use App\Models\Viewuser;
use App\Models\SPM_Anggaran;
use App\Models\SPM_Capaian;
use App\Models\SPM_Dasar_Hukum;
use App\Models\SPM_Indikator_Pencapaian;
use App\Models\SPM_Jenis_Layanan;
use App\Models\SPM_Landasan_Hukum;
use App\Models\SPM_Pelayanan;
use App\Models\SPM_Rincian_Jenis_Layanan;
use App\Models\Urusan_90;
use App\Models\BidangUrusan_90;
use App\Models\Program_90;
use App\Models\Kegiatan_90;
use App\Models\SubKegiatan_90;
use App\Models\Tahun;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Api_SPMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewUser(){
        $data = Viewuser::get();
        return API_SPMResource::collection($data);
    }    
    public function anggaran()
    {
        $capaian = SPM_Anggaran::get();
                    
        return API_SPMResource::collection($capaian);        
    }
    public function capaian()
    {
        $capaian = SPM_Capaian::get();

        return API_SPMResource::collection($capaian);        
    }
    public function dasarHukum()
    {
        $dasarHukum = SPM_Dasar_Hukum::get();
        return API_SPMResource::collection($dasarHukum);        
    }
    public function indikator()
    {
        $indikator = SPM_Indikator_Pencapaian::get();
        return API_SPMResource::collection($indikator);        
    }
    public function jenisLayanan()
    {
        $jenisLayanan = SPM_Jenis_Layanan::get();
        return API_SPMResource::collection($jenisLayanan);        
    }
    public function landasanHukum()
    {
        $landasanHukum = SPM_Landasan_Hukum::get();
        
        return API_SPMResource::collection($landasanHukum);        
    }
    public function pelayanan()
    {
        $pelayanan = SPM_Pelayanan::get();
        return API_SPMResource::collection($pelayanan);        
    }
    public function rincian()
    {
        $rincian = SPM_Rincian_Jenis_Layanan::get();
        return API_SPMResource::collection($rincian);        
    }
    public function urusan()
    {
        $urusan = Urusan_90::get();
        return API_SPMResource::collection($urusan);        
    }
    public function statusBy(Request $request)
    {
        $bidangUrusan = BidangUrusan_90::select('*')
                                ->where('id_bidang_urusan',1)
                                ->orWhere('id_bidang_urusan',2)
                                ->orWhere('id_bidang_urusan',3)
                                ->orWhere('id_bidang_urusan',4)
                                ->orWhere('id_bidang_urusan',5)
                                ->orWhere('id_bidang_urusan',6)
                                ->get();
        $id_tahun = $request->selectTahun;
        $status = array();
        foreach($bidangUrusan as $bu)
        {
            $status[] = array(
                            'id_bidang_urusan'      => $bu->id_bidang_urusan,
                            'nama_bidang_urusan'    => $bu->nama_bidang_urusan,
                            'status'                => SPM_Capaian::where('id_bidang_urusan',$bu->id_bidang_urusan)
                                                                    ->where('id_tahun',$id_tahun)
                                                                    ->find(1)
                        );
        }
        return API_SPMResource::collection($status);        
    }
}
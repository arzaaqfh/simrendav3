<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;
use App\Models\SettingInput;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Jafung;
use App\Models\Jafung_SektorPD;
use App\Models\SKPD_90;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if (!$this->guard()->attempt(array($fieldType => $request->username, 'password' => $request->password)))
        {
            return new JsonResponse(['status' => 401,'message' => 'Unauthorized'], 401);
        }

        $user = User::where('id_user',Auth::user()->id_user)->with('user_skpd.skpd')->first();
        $settingInput = SettingInput::get();
        
        $token = $user->createToken('auth_token')->plainTextToken;

        return new JsonResponse([
            'message' => 'Hi '.$user->name.' !',
            'data' => $user,
            'settingInput' => $settingInput,
            'access_token' => $token,
            'token_type' => 'Bearer'
        ]);
    }

    // method for user logout and delete token
    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'You have successfully logged out and the token was successfully deleted'
        ];
    }

    protected function guard()
    {
        return Auth::guard();
    }
}

<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\BidangUrusan_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BidangUrusan_90Controller extends Controller
{
    /*
    |------------------| 
    |                  |
    | Bidang Urusan 90 |
    |                  |
    |------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = BidangUrusan_90::with([
                                                'urusan'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new BidangUrusan_90();

        $data->kode_bidang_urusan = $request->kodeBidangUrusan;
        $data->nama_bidang_urusan = $request->namaBidangUrusan;
        $data->id_urusan = $request->urusan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = BidangUrusan_90::with([
                                        'urusan'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = BidangUrusan_90::findOrFail($id);
        

        $data->kode_bidang_urusan = $request->kodeBidangUrusan;
        $data->nama_bidang_urusan = $request->namaBidangUrusan;
        $data->id_urusan = $request->urusan;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BidangUrusan_90::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

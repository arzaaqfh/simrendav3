<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\FaktorPendorongIKP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaktorPendorongIKPController extends Controller
{
    /*
    |----------------------|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    | Faktor Pendorong IKP |
    |                      |
    |----------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = FaktorPendorongIKP::with([
                                                'TargetRealisasiIKP'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new FaktorPendorongIKP();

        $data->dana = $request->dana;
        $data->sdm = $request->sdm;
        $data->waktu_pelaksanaan = $request->waktuPelaksanaan;
        $data->peraturan_perundangan = $request->peraturanPerundangan;
        $data->sistem_pengadaan_barang_jasa = $request->sistemPengadaanBarangJasa;
        $data->perijinan = $request->perijinan;
        $data->ketersediaan_lahan = $request->ketersediaanLahan;
        $data->faktor_alam = $request->faktorAlam;
        $data->id_target_realisasi = $request->targetRealisasi;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FaktorPendorongIKP::with([
                                        'TargetRealisasiIKP'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = FaktorPendorongIKP::findOrFail($id);
        
        $data->dana = $request->dana;
        $data->sdm = $request->sdm;
        $data->waktu_pelaksanaan = $request->waktuPelaksanaan;
        $data->peraturan_perundangan = $request->peraturanPerundangan;
        $data->sistem_pengadaan_barang_jasa = $request->sistemPengadaanBarangJasa;
        $data->perijinan = $request->perijinan;
        $data->ketersediaan_lahan = $request->ketersediaanLahan;
        $data->faktor_alam = $request->faktorAlam;
        $data->id_target_realisasi = $request->targetRealisasi;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = FaktorPendorongIKP::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

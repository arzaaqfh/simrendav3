<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\IndikatorKinerjaRPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndikatorKinerjaRPJMDController extends Controller
{    
    /*
    |-------------------------| 
    |                         |
    | Indikator Kinerja RPJMD |
    |                         |
    |-------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = IndikatorKinerjaRPJMD::with([
                                                'Aspek',
                                                'SatuanRPJMD',
                                                'Rumus',
                                                'SKPD'
                                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new IndikatorKinerjaRPJMD();

        $data->indikator_kinerja = $request->indikatorKinerja;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_aspek = $request->aspek;
        $data->id_skpd = $request->skpd;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = IndikatorKinerjaRPJMD::with([
                                            'Aspek',
                                            'SatuanRPJMD',
                                            'Rumus',
                                            'SKPD'
                                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = IndikatorKinerjaRPJMD::findOrFail($id);
        
        $data->indikator_kinerja = $request->indikatorKinerja;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_aspek = $request->aspek;
        $data->id_skpd = $request->skpd;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IndikatorKinerjaRPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

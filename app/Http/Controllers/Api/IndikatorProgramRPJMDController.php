<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\IndikatorProgramRPJMD;
use App\Models\TargetRealisasiProgramV2;
use App\Models\TargetRealisasiIKP;
use App\Models\FaktorPendorongIKP;
use App\Models\FaktorPenghambatIKP;
use App\Models\TindakLanjutIKP;
use App\Models\RelasiStrategiIndikatorProgramRPJMD;
use App\Models\Tahun;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndikatorProgramRPJMDController extends Controller
{
    /*
    |-------------------------| 
    |                         |
    | Indikator Program RPJMD |
    |                         |
    |-------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = IndikatorProgramRPJMD::with([
                                                'ProgramRPJMD',
                                                'SatuanRPJMD',
                                                'Rumus',
                                                'SKPD'
                                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // tambah data baru
        $data = new IndikatorProgramRPJMD();

        $data->indikator_program = $request->indikator_program;
        $data->id_satuan = 0;
        $data->id_rumus = 0;
        $data->id_program = 0;
        $data->id_skpd = 0;
        $data->id_program_90 = $request->id_program_90; 
        
        if($data->save())
        {
            $ikpBaru = IndikatorProgramRPJMD::orderBy('id_indikator_program','DESC')->first();
            if($ikpBaru)
            {
                $data_tri_prog = new TargetRealisasiProgramV2();

                $data_tri_prog->id_indikator_program = $ikpBaru->id_indikator_program;
                $data_tri_prog->id_renja = $request->id_renja;
                $data_tri_prog->id_rumus = 3;
                if($request->triwulan < 4)
                {
                    $data_tri_prog->target = $request->target;
                }else{
                    $data_tri_prog->target_perubahan = $request->target;
                }
                $data_tri_prog->satuan = $request->satuan;

                if($data_tri_prog->save())
                {
                    $hasil[] = new API_SimrendaResource($data_tri_prog);
                }else{
                    return array(
                        'kode' => '500',
                        'message' => 'Proses tambah data GAGAL!'
                    );
                }
            }
            $hasil[] = new API_SimrendaResource($data);
        }else{
            return array(
                'kode' => '500',
                'message' => 'Proses tambah data GAGAL!'
            );
        }

        return $hasil;        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = IndikatorProgramRPJMD::with([
                                            'ProgramRPJMD',
                                            'SatuanRPJMD',
                                            'Rumus',
                                            'SKPD'
                                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasil = null;
        $ikpLama = IndikatorProgramRPJMD::where('id_indikator_program',$id)->first();
        $trProg = TargetRealisasiProgramV2::where([
            ['id_indikator_program','=',$id],
            ['id_renja','=',$request->id_renja]         
        ])->get();
        
        if($ikpLama && $trProg)
        {
            // tambah data baru
            $data = new IndikatorProgramRPJMD();

            $data->indikator_program = $request->indikator_program;
            $data->id_satuan = 0;
            $data->id_rumus = 0;
            $data->id_program = 0;
            $data->id_skpd = 0;
            $data->id_program_90 = $request->id_program_90; 

            if($data->save())
            {
                $dataBaru = IndikatorProgramRPJMD::orderBy('id_indikator_program','DESC')->first();  
                foreach($trProg as $tr)
                {
                    $dataTr = TargetRealisasiProgramV2::find($tr->id_tri_program);
                    if($request->rumus != null)
                    {
                        $dataTr->id_rumus = $request->rumus;
                    }
                    if($request->target != null)
                    {
                        if($request->triwulan != 4)
                        {
                            $dataTr->target = $request->target;
                        }else{
                            $dataTr->target_perubahan = $request->target;
                        }
                    }
                    if($request->satuan != null)
                    {
                        $dataTr->satuan = $request->satuan;
                    }
                    $dataTr->id_indikator_program = $dataBaru->id_indikator_program;
                    if($dataTr->save())
                    {
                        $hasil[] = array(
                            'kode' => '200',
                            'message' => 'Proses update target realisasi berhasil!',
                            'data' => $dataTr
                        );
                    }
                }
                $hasil[] = array(
                    'kode' => '200',
                    'message' => 'Proses update indikator program berhasil!',
                    'data' => $data
                );
            }            
        }else{
            $hasil = array(
                'kode' => '500',
                'message' => 'Proses tambah data GAGAL! ( data indikator program atau target realisasi tidak ditemukan)',
                'data' => $request->all()
            );
        }   
        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 1. cari data target realisasi yang berelasi
        $datatr = TargetRealisasiIKP::where('id_indikator_program',$id);
        foreach($datatr->get() as $tr)
        {
            // 2. hapus faktor pendorong, penghambat dan tindak lanjut yang berelasi
            $fp = FaktorPendorongIKP::where('id_target_realisasi',$tr->id_target_realisasi)->delete();
            $fph = FaktorPenghambatIKP::where('id_target_realisasi',$tr->id_target_realisasi)->delete();
            $tl = TindakLanjutIKP::where('id_target_realisasi',$tr->id_target_realisasi)->delete();
        }
        // 3. hapus target realisasi yang berelasi
        $datatr->delete();
        // 4. hapus data relasi dengan data strategi
        $datarstr = RelasiStrategiIndikatorProgramRPJMD::where('id_indikator_program',$id)->delete();
        // 5. cari data indikator program
        $data = IndikatorProgramRPJMD::findOrFail($id);
        // 6. hapus indikator program
        if($data->delete()){
            return new API_SimrendaResource($data);
        }
    }
}

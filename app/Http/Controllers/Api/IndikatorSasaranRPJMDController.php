<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\IndikatorSasaranRPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndikatorSasaranRPJMDController extends Controller
{
    /*
    |-----------------------------| 
    |                             |
    | Indikator Sasaran IKU RPJMD |
    |                             |
    |-----------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = IndikatorSasaranRPJMD::with([
                                                'SasaranRPJMD',
                                                'SatuanRPJMD',
                                                'Rumus'
                                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new IndikatorSasaranRPJMD();

        $data->indikator_sasaran = $request->indikatorSasaran;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_sasaran = $request->sasaran;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = IndikatorSasaranRPJMD::with([
                                            'SasaranRPJMD',
                                            'SatuanRPJMD',
                                            'Rumus'
                                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = IndikatorSasaranRPJMD::findOrFail($id);
        
        $data->indikator_sasaran = $request->indikatorSasaran;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_sasaran = $request->sasaran;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IndikatorSasaranRPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

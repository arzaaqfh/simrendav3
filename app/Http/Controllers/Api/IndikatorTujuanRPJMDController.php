<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\IndikatorTujuanRPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndikatorTujuanRPJMDController extends Controller
{
    /*
    |----------------------------| 
    |                            |
    | Indikator Tujuan IKU RPJMD |
    |                            |
    |----------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = IndikatorTujuanRPJMD::with([
                                                'TujuanRPJMD',
                                                'SatuanRPJMD',
                                                'Rumus'
                                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new IndikatorTujuanRPJMD();

        $data->indikator_tujuan = $request->indikatorTujuan;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_tujuan = $request->tujuan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = IndikatorTujuanRPJMD::with([
                                        'TujuanRPJMD',
                                        'SatuanRPJMD',
                                        'Rumus'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = IndikatorTujuanRPJMD::findOrFail($id);
        
        $data->indikator_tujuan = $request->indikatorTujuan;
        $data->id_satuan = $request->satuan;
        $data->id_rumus = $request->rumus;
        $data->id_tujuan = $request->tujuan;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = IndikatorTujuanRPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

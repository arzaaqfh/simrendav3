<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use App\Models\Jafung;
use App\Models\Jafung_SektorPD;
use Validator;
use App\Http\Resources\Api\API_SimrendaResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Jafung_90Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data  = User::get();
        return API_SimrendaResource::collection($data);
    }
    public function getUserJafung()
    {
        $data = Jafung::with('level_jafung.user','jafung_sektorpd.skpd')->get();
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_user' => 'required'
        ]);

        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $data  = User::with('level.jafung.jafung_sektorpd.skpd')->where('id_user',$request->id_user)->get();
            return API_SimrendaResource::collection($data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateJafung(Request $request)
    {        
        $validator = Validator::make($request->all(),[
            'id_jafung' => 'required',
            'id_skpd' => 'required'
        ]);
        
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $hasil = null;
            foreach($request as $val)
            {
                // hapus yang lama
                $data  = Jafung_SektorPD::where('id_jafung',$val->get('id_jafung'));
                $data->delete();

                // tambahkan yang baru
                $dataBaru = new Jafung_SektorPD();
        
                $dataBaru->id_jafung = $val->get('id_jafung');
                $dataBaru->id_skpd = $val->get('id_skpd');
        
                $dataBaru->timestamps = false;

                if($dataBaru->save())
                {
                    $hasil[] = response()->json([
                                    'status' => 200,
                                    'message' => 'akses jafung berhasil dimasukkan!',
                                    'data' => $dataBaru,
                                ]);
                }
            }
            return $hasil;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

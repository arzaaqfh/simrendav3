<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Kegiatan_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Kegiatan_90Controller extends Controller
{
    /*
    |-------------| 
    |             |
    | Kegiatan 90 |
    |             |
    |-------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Kegiatan_90::with([
                                                'program',
                                                'indikatorKegiatan',
                                                'subKegiatan',
                                                'apbd_kota',
                                                'realisasi_anggaran'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new Kegiatan_90();

        $data->kode_kegiatan = $request->kodeKegiatan;
        $data->nama_kegiatan = $request->namaKegiatan;
        $data->id_program = $request->program;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kegiatan_90::with([
                                        'program',
                                        'indikatorKegiatan',
                                        'subKegiatan',
                                        'apbd_kota',
                                        'realisasi_anggaran'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Kegiatan_90::findOrFail($id);        

        $data->kode_kegiatan = $request->kodeKegiatan;
        $data->nama_kegiatan = $request->namaKegiatan;
        $data->id_program = $request->program;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kegiatan_90::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

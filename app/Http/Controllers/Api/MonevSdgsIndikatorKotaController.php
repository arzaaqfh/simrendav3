<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Indikator_Kota_90;
use App\Models\Sdgs_Target_Realisasi_Kota_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class MonevSdgsIndikatorKotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Sdgs_Indikator_Kota_90::select('SDGS_Nasional.id_target_nasional','SDGS_Nasional.id_target_global',DB::raw('CAST(SDGS_Nasional.target_nasional AS varchar(max)) AS target_nasional'),'SDGS_IndikatorKota_90.id_indikator_kota','SDGS_IndikatorKota_90.nama_indikator_kota','SDGS_IndikatorKota_90.satuan','SDGS_TargetRealisasiKota_90.target','SDGS_TargetRealisasiKota_90.realisasi_target_1','SDGS_TargetRealisasiKota_90.realisasi_target_2','SDGS_TargetRealisasiKota_90.sumber_data','SDGS_TargetRealisasiKota_90.verified_by','SDGS_TargetRealisasiKota_90.id_target_realisasi_kota','Jafung.nama')
        ->join('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->join('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->join('SDGS_TargetRealisasiKota_90', function ($join) use ($request) {
            $join->on('SDGS_TargetRealisasiKota_90.id_indikator_kota', '=', 'SDGS_IndikatorKota_90.id_indikator_kota')->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun);
        })
        ->join('SDGS_LogIndikatorKota_90', function ($join) {
            $join->on('SDGS_LogIndikatorKota_90.id_target_realisasi_kota', '=', 'SDGS_TargetRealisasiKota_90.id_target_realisasi_kota')->where('SDGS_LogIndikatorKota_90.isDeleted',0);
        })
        ->leftJoin('Jafung','Jafung.id_jafung','SDGS_TargetRealisasiKota_90.verified_by')
        ->when($request->id_skpd, function ($q) use ($request){
            return $q->whereIn('SDGS_LogIndikatorKota_90.id_skpd',$request->id_skpd);
        })
        ->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun)
        ->groupBy('SDGS_Nasional.id_target_nasional','SDGS_Nasional.id_target_global',DB::raw('CAST(SDGS_Nasional.target_nasional AS varchar(max))'),'SDGS_IndikatorKota_90.id_indikator_kota','SDGS_IndikatorKota_90.nama_indikator_kota','SDGS_IndikatorKota_90.satuan','SDGS_TargetRealisasiKota_90.target','SDGS_TargetRealisasiKota_90.realisasi_target_1','SDGS_TargetRealisasiKota_90.realisasi_target_2','SDGS_TargetRealisasiKota_90.sumber_data','SDGS_TargetRealisasiKota_90.verified_by','SDGS_TargetRealisasiKota_90.id_target_realisasi_kota','Jafung.nama')
        ->orderbyRaw('CAST(SDGS_Nasional.target_nasional AS varchar(max)) asc','SDGS_IndikatorKota_90.nama_indikator asc')->get();
        
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_realisasi_kota.'" data-original-title="Monev" class="edit btn btn-success btn-sm monevIndkotaSdgs">Monev</a>';
                            return $btn;
                    })
                    ->addColumn('status_verifikasi', function($row){
                        if($row->verified_by){
                           $v = '<i class="text-success h5 ti-check-box"></i>';
                        }else{
                            $v = '<i class="text-danger h5 ti-control-stop"></i>';
                        }
                        return $v;
                    })
                    ->rawColumns(['action','status_verifikasi'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $verified = $request->verified;
        if($request->file('lampiran')){
            $file = $request->file('lampiran');
            
            $nama_ind = Sdgs_Indikator_Kota_90::join('SDGS_TargetRealisasiKota_90','SDGS_TargetRealisasiKota_90.id_indikator_kota','SDGS_IndikatorKota_90.id_indikator_kota')
            ->where('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota',$request->id_target_realisasi_kota)->first();

            $tujuan_upload = 'lampiran_sdgs';
            $filename = 'Lampiran Monev Indikator Kota_'.$nama_ind->nama_indikator_kota.'.'.$file->getClientOriginalExtension();
            $file->move($tujuan_upload,$filename);
            $aray_update = ['realisasi_target_1' => $request->realisasi_target_1,'capaian_target_1' => $request->capaian_target_1,'realisasi_anggaran_1' => $request->realisasi_anggaran_1,'capaian_anggaran_1' => $request->capaian_anggaran_1,'realisasi_target_2' => $request->realisasi_target_2,'capaian_target_2' => $request->capaian_target_2,'realisasi_anggaran_2' => $request->realisasi_anggaran_2,'capaian_anggaran_2' => $request->capaian_anggaran_2,'catatan' => $request->catatan,'sumber_data' => $request->sumber_data,'lampiran' => $filename,'verified_by' => $verified];    
        }else if($request->lampiran_emptied == 1){
            $aray_update = ['realisasi_target_1' => $request->realisasi_target_1,'capaian_target_1' => $request->capaian_target_1,'realisasi_anggaran_1' => $request->realisasi_anggaran_1,'capaian_anggaran_1' => $request->capaian_anggaran_1,'realisasi_target_2' => $request->realisasi_target_2,'capaian_target_2' => $request->capaian_target_2,'realisasi_anggaran_2' => $request->realisasi_anggaran_2,'capaian_anggaran_2' => $request->capaian_anggaran_2,'catatan' => $request->catatan,'sumber_data' => $request->sumber_data,'lampiran' => null,'verified_by' => $verified];
        }else{
            $aray_update = ['realisasi_target_1' => $request->realisasi_target_1,'capaian_target_1' => $request->capaian_target_1,'realisasi_anggaran_1' => $request->realisasi_anggaran_1,'capaian_anggaran_1' => $request->capaian_anggaran_1,'realisasi_target_2' => $request->realisasi_target_2,'capaian_target_2' => $request->capaian_target_2,'realisasi_anggaran_2' => $request->realisasi_anggaran_2,'capaian_anggaran_2' => $request->capaian_anggaran_2,'catatan' => $request->catatan,'sumber_data' => $request->sumber_data,'verified_by' => $verified];
        }
        
        $data = Sdgs_Target_Realisasi_Kota_90::updateOrCreate(
            ['id_target_realisasi_kota' => $request->id_target_realisasi_kota],$aray_update
        );

        if($data)
        {
            return response()->json(['success'=>'Monev Indikator Kota Berhasil Tersimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

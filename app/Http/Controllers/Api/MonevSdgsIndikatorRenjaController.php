<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Indikator_Kota_90;
use App\Models\Sdgs_Target_Realisasi_Kota_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class MonevSdgsIndikatorRenjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $data = Sdgs_Coding_90::select('*')
        ->join('SDGS_LogIndikatorKota_90', function ($join) use ($request) {
            $join->on('SDGS_LogIndikatorKota_90.id_log_indikatorkota', '=', 'Sdgs_Coding_90.id_log_indikatorkota')
            ->where('SDGS_LogIndikatorKota_90.isDeleted',0)
            ->where('SDGS_Coding_90.isDeleted',0);
        })
        ->join('SDGS_TargetRealisasiKota_90', function ($join) {
            $join->on('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota', '=', 'SDGS_LogIndikatorKota_90.id_target_realisasi_kota')->where('SDGS_TargetRealisasiKota_90.tahun',2021);
        })
        ->leftJoin('SDGS_IndikatorKota_90','SDGS_IndikatorKota_90.id_indikator_kota','=','SDGS_TargetRealisasiKota_90.id_indikator_kota')
        ->leftJoin('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->leftJoin('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->leftJoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->leftJoin('LogRenja_90','LogRenja_90.id_log_renja','=','SDGS_Coding_90.id_log_renja')
        ->leftJoin('Renja_90','Renja_90.id_renja','=','LogRenja_90.id_renja')
        ->leftJoin('SubKegiatan_90','SubKegiatan_90.id_sub_kegiatan','=','LogRenja_90.id_sub_kegiatan')
        ->leftJoin('Kegiatan_90','Kegiatan_90.id_kegiatan','=','SubKegiatan_90.id_kegiatan')
        ->leftJoin('Program_90','Program_90.id_program','=','Kegiatan_90.id_program')
        ->when($request->id_skpd, function ($q) use ($request){
            return $q->whereIn('SDGS_LogIndikatorKota_90.id_skpd',$request->id_skpd);
        })
        ->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->orderby('SDGS_Nasional.id_target_global','asc')->get();
        
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                            $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_log_indikatorkota.'" data-original-title="Coding" class="btn btn-primary btn-sm CodingSdgs">Coding</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }
}

<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use App\Models\IndikatorSasaran;
use App\Models\Program_90;
use App\Models\Kegiatan_90;
use App\Models\SubKegiatan_90;
use App\Models\Renja_90;
use App\Models\TaggingData;
use App\Models\Sasaran;
use App\Models\Tujuan;
use App\Models\RelSasTuj;
use App\Models\R_IndSaTu;
use App\Models\R_IndTuProg;
use App\Models\R_IndProgKeg;
use App\Models\R_IndKegSubKeg;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Api\API_RENJAResource;

class PerencanaanController extends Controller
{
    //
    /**
     * Get Sasaran
     */
    public function getSasaran(Request $request)
    {
        $tahun = $request->tahun;
        $id_sasaran = $request->id_sasaran;

        if(!is_null($id_sasaran))
        {
            $data = DB::select("SELECT
                Sasaran.id_sasaran,
                Sasaran.sasaran,
                IndikatorSasaran.id_indikator_sasaran,
                indikatorsasaran.indikator_sasaran,
                R_IndSaTu.id_rel
            FROM TargetRealisasiSasaran
            JOIN IndikatorSasaran ON IndikatorSasaran.id_indikator_sasaran = TargetRealisasiSasaran.id_indikator_sasaran
            JOIN Sasaran ON Sasaran.id_sasaran = IndikatorSasaran.id_sasaran
            JOIN Periode ON Periode.id_periode = TargetRealisasiSasaran.id_periode
            LEFT JOIN R_IndSaTu ON R_IndSaTu.id_indikator_sasaran = IndikatorSasaran.id_indikator_sasaran
                AND R_IndSaTu.tahun = ?
            WHERE Periode.tahun_akhir >= ?
                AND Periode.tahun_awal <= ?
                AND Sasaran.id_sasaran = ?
            GROUP BY
                Sasaran.id_sasaran,
                Sasaran.sasaran,
                IndikatorSasaran.id_indikator_sasaran,
                indikatorsasaran.indikator_sasaran,
                R_IndSaTu.id_rel
            ",array($tahun,$tahun,$tahun,$id_sasaran));

        }else{
            $data = DB::select("SELECT
                Sasaran.id_sasaran,
                Sasaran.sasaran,
                IndikatorSasaran.id_indikator_sasaran,
                indikatorsasaran.indikator_sasaran,
                R_IndSaTu.id_rel
            FROM TargetRealisasiSasaran
            JOIN IndikatorSasaran ON IndikatorSasaran.id_indikator_sasaran = TargetRealisasiSasaran.id_indikator_sasaran
            JOIN Sasaran ON Sasaran.id_sasaran = IndikatorSasaran.id_sasaran
            JOIN Periode ON Periode.id_periode = TargetRealisasiSasaran.id_periode
            LEFT JOIN R_IndSaTu ON R_IndSaTu.id_indikator_sasaran = IndikatorSasaran.id_indikator_sasaran
                AND R_IndSaTu.tahun = ?
            WHERE Periode.tahun_akhir >= ?
                AND Periode.tahun_awal <= ?
            GROUP BY
                Sasaran.id_sasaran,
                Sasaran.sasaran,
                IndikatorSasaran.id_indikator_sasaran,
                indikatorsasaran.indikator_sasaran,
                R_IndSaTu.id_rel
            ",array($tahun,$tahun,$tahun));
        }        

        $t_id_sasaran = null; //tampung id sasaran
        $t_id_indsasaran = null; //tampung id tujuan

        $t_dat_sasaran = null; //tampung data sasaran
        $t_dat_indsasaran = null; //tampung data tujuan

        $t_hasil_indsasaran = null; //tampung hasil data tujuan
        
        $index_sasaran = 0;
        $index_indsasaran = 0;
        $index_data = 0;
        $hasil = null;
        foreach($data as $dat)
        {
            // Proses konversi data
            
            // Simpan data
            if(!is_null($t_id_indsasaran) && ($dat->id_indikator_sasaran != $t_id_indsasaran))
            {
                // indikator sasaran
                $t_hasil_indsasaran[$index_indsasaran] = $t_dat_indsasaran;
                $index_indsasaran++;
            }
            if(!is_null($t_id_sasaran) && ($dat->id_sasaran != $t_id_sasaran))
            {
                // sasaran
                $t_dat_sasaran['indikatorsasaran'] = $t_hasil_indsasaran;
                $hasil['sasaran'][$index_sasaran] = $t_dat_sasaran;
                $index_sasaran++;

                if($index_data > count($data)-1)
                {
                    $index_indsasaran = 0;
                    $t_hasil_indsasaran = null;
                }
            }
            
            // tampung id dan data
            $t_id_sasaran = $dat->id_sasaran;
            $t_id_indsasaran = $dat->id_indikator_sasaran;

            $t_dat_indsasaran = array(
                'id_indikator_sasaran' => $dat->id_indikator_sasaran,
                'indikator_sasaran' => $dat->indikator_sasaran,
                'id_rel' => $dat->id_rel
            );
            $t_dat_sasaran = array(
                'id_sasaran' => $dat->id_sasaran,
                'sasaran' => $dat->sasaran,
                'indikatorsasaran' => null
            );
            $index_data++;
        }
        
        $t_hasil_indsasaran[$index_indsasaran] = $t_dat_indsasaran;
        $t_dat_sasaran['indikatorsasaran'] = $t_hasil_indsasaran;
        $hasil['sasaran'][$index_sasaran] = $t_dat_sasaran;

        return $hasil;
    }

    /**
     * Get Tujuan
     */
    public function getTujuan(Request $request)
    {
        $tahun = $request->tahun;
        $id_sasaran = $request->id_sasaran;
        $id_tujuan = $request->id_tujuan;

        if(!is_null($id_tujuan))
        {
            $data = DB::select("SELECT
                Tujuan.id_tujuan,
                Tujuan.tujuan,
                IndikatorTujuan.id_indikator_tujuan,
                IndikatorTujuan.indikator_tujuan,
                R_IndTuProg.id_rel
            FROM TargetRealisasiTujuan
            JOIN IndikatorTujuan ON IndikatorTujuan.id_indikator_tujuan = TargetRealisasiTujuan.id_indikator_tujuan
            JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
            JOIN Periode ON Periode.id_periode = TargetRealisasiTujuan.id_periode
            JOIN RelSasTuj ON RelSasTuj.id_tujuan = tujuan.id_tujuan
            LEFT JOIN R_IndTuProg ON R_IndTuProg.id_indikator_tujuan = IndikatorTujuan.id_indikator_tujuan
                AND R_IndTuProg.tahun = ?
            WHERE Periode.tahun_akhir >= ?
                AND Periode.tahun_awal <= ?
                AND RelSasTuj.id_sasaran = ?
                AND RelSasTuj.id_tujuan = ?
            GROUP BY
                Tujuan.id_tujuan,
                Tujuan.tujuan,
                IndikatorTujuan.id_indikator_tujuan,
                IndikatorTujuan.indikator_tujuan,
                R_IndTuProg.id_rel
            ",array($tahun,$tahun,$tahun,$id_sasaran,$id_tujuan));

        }else{
            $data = DB::select("SELECT
                Tujuan.id_tujuan,
                Tujuan.tujuan,
                IndikatorTujuan.id_indikator_tujuan,
                IndikatorTujuan.indikator_tujuan,
                R_IndTuProg.id_rel
            FROM TargetRealisasiTujuan
            JOIN IndikatorTujuan ON IndikatorTujuan.id_indikator_tujuan = TargetRealisasiTujuan.id_indikator_tujuan
            JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
            JOIN Periode ON Periode.id_periode = TargetRealisasiTujuan.id_periode
            JOIN RelSasTuj ON RelSasTuj.id_tujuan = tujuan.id_tujuan
            LEFT JOIN R_IndTuProg ON R_IndTuProg.id_indikator_tujuan = IndikatorTujuan.id_indikator_tujuan
                AND R_IndTuProg.tahun = ?
            WHERE Periode.tahun_akhir >= ?
                AND Periode.tahun_awal <= ?
                AND RelSasTuj.id_sasaran = ?
            GROUP BY
                Tujuan.id_tujuan,
                Tujuan.tujuan,
                IndikatorTujuan.id_indikator_tujuan,
                IndikatorTujuan.indikator_tujuan,
                R_IndTuProg.id_rel
            ",array($tahun,$tahun,$tahun,$id_sasaran));
        }        

        $t_id_tujuan = null; //tampung id tujuan
        $t_id_indtujuan = null; //tampung id indikator tujuan

        $t_dat_tujuan = null; //tampung data tujuan
        $t_dat_indtujuan = null; //tampung data indikator tujuan

        $t_hasil_indtujuan = null; //tampung hasil data indikator tujuan
        
        $index_tujuan = 0;
        $index_indtujuan = 0;
        $index_data = 0;
        $hasil = null;
        foreach($data as $dat)
        {
            // Proses konversi data
            
            // Simpan data
            if(!is_null($t_id_indtujuan) && ($dat->id_indikator_tujuan != $t_id_indtujuan))
            {
                // indikator tujuan
                $t_hasil_indtujuan[$index_indtujuan] = $t_dat_indtujuan;
                $index_indtujuan++;
            }
            if(!is_null($t_id_tujuan) && ($dat->id_tujuan != $t_id_tujuan))
            {
                // tujuan
                $t_dat_tujuan['indikatortujuan'] = $t_hasil_indtujuan;
                $hasil['tujuan'][$index_tujuan] = $t_dat_tujuan;
                $index_tujuan++;

                if($index_data > count($data)-1)
                {
                    $index_indtujuan = 0;
                    $t_hasil_indtujuan = null;
                }
            }
            
            // tampung id dan data
            $t_id_tujuan = $dat->id_tujuan;
            $t_id_indtujuan = $dat->id_indikator_tujuan;

            $t_dat_indtujuan = array(
                'id_indikator_tujuan' => $dat->id_indikator_tujuan,
                'indikator_tujuan' => $dat->indikator_tujuan,
                'id_rel' => $dat->id_rel
            );
            $t_dat_tujuan = array(
                'id_tujuan' => $dat->id_tujuan,
                'tujuan' => $dat->tujuan,
                'indikatortujuan' => null
            );
            $index_data++;
        }
        
        $t_hasil_indtujuan[$index_indtujuan] = $t_dat_indtujuan;
        $t_dat_tujuan['indikatortujuan'] = $t_hasil_indtujuan;
        $hasil['tujuan'][$index_tujuan] = $t_dat_tujuan;

        return $hasil;
    }

    /**
     * Get Program
     */
    public function getProgram(Request $request)
    {
        $tahun = $request->tahun;
        $id_tujuan = $request->id_tujuan;
        $id_program = $request->id_program;

        if(!is_null($id_program))
        {
            $data = DB::select("SELECT
                Program_90.id_program,
                Program_90.nama_program,
                Program_90.kode_program,
                IndikatorProgramRPJMD.id_indikator_program,
                IndikatorProgramRPJMD.indikator_program	,
                R_IndProgKeg.id_rel
            FROM TargetRealisasiProgramV2
            JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_indikator_program = TargetRealisasiProgramV2.id_indikator_program
            JOIN Program_90 ON Program_90.id_program = IndikatorProgramRPJMD.id_program_90
            JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
            JOIN RelTujProg ON RelTujProg.id_program_90 = Program_90.id_program
            LEFT JOIN R_IndProgKeg ON R_IndProgKeg.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                AND R_IndProgKeg.id_renja = TargetRealisasiProgramV2.id_renja
            WHERE Renja_90.periode_usulan = ?
                AND RelTujProg.id_tujuan = ?
                AND RelTujProg.id_program_90 = ?
            GROUP BY
                Program_90.id_program,
                Program_90.kode_program,
                Program_90.nama_program,
                IndikatorProgramRPJMD.id_indikator_program,
                IndikatorProgramRPJMD.indikator_program	,
                R_IndProgKeg.id_rel
            ",array($tahun,$id_tujuan,$id_program));

        }else{
            $data = DB::select("SELECT
                Program_90.id_program,
                Program_90.kode_program,
                Program_90.nama_program,
                IndikatorProgramRPJMD.id_indikator_program,
                IndikatorProgramRPJMD.indikator_program	,
                R_IndProgKeg.id_rel
            FROM TargetRealisasiProgramV2
            JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_indikator_program = TargetRealisasiProgramV2.id_indikator_program
            JOIN Program_90 ON Program_90.id_program = IndikatorProgramRPJMD.id_program_90
            JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
            JOIN RelTujProg ON RelTujProg.id_program_90 = Program_90.id_program
            LEFT JOIN R_IndProgKeg ON R_IndProgKeg.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                AND R_IndProgKeg.id_renja = TargetRealisasiProgramV2.id_renja
            WHERE Renja_90.periode_usulan = ?
                AND RelTujProg.id_tujuan = ?
            GROUP BY
                Program_90.id_program,
                Program_90.kode_program,
                Program_90.nama_program,
                IndikatorProgramRPJMD.id_indikator_program,
                IndikatorProgramRPJMD.indikator_program	,
                R_IndProgKeg.id_rel
            ",array($tahun,$id_tujuan));
        }        

        $t_id_program = null; //tampung id program
        $t_id_indprogram = null; //tampung id indikator program

        $t_dat_program = null; //tampung data program
        $t_dat_indprogram = null; //tampung data indikator program

        $t_hasil_indprogram = null; //tampung hasil data indikator program
        
        $index_program = 0;
        $index_indprogram = 0;
        $index_data = 0;
        $hasil = null;
        foreach($data as $dat)
        {
            // Proses konversi data
            
            // Simpan data
            if(!is_null($t_id_indprogram) && ($dat->id_indikator_program != $t_id_indprogram))
            {
                // indikator program
                $t_hasil_indprogram[$index_indprogram] = $t_dat_indprogram;
                $index_indprogram++;
            }
            if(!is_null($t_id_program) && ($dat->id_program != $t_id_program))
            {
                // program
                $t_dat_program['indikatorprogram'] = $t_hasil_indprogram;
                $hasil['program'][$index_program] = $t_dat_program;
                $index_program++;

                if($index_data > count($data)-1)
                {
                    $index_indprogram = 0;
                    $t_hasil_indprogram = null;
                }
            }
            
            // tampung id dan data
            $t_id_program = $dat->id_program;
            $t_id_indprogram = $dat->id_indikator_program;

            $t_dat_indprogram = array(
                'id_indikator_program' => $dat->id_indikator_program,
                'indikator_program' => $dat->indikator_program,
                'id_rel' => $dat->id_rel
            );
            $t_dat_program = array(
                'id_program' => $dat->id_program,
                'kode_program' => $dat->kode_program,
                'nama_program' => $dat->nama_program,
                'indikatorprogram' => null
            );
            $index_data++;
        }
        
        $t_hasil_indprogram[$index_indprogram] = $t_dat_indprogram;
        $t_dat_program['indikatorprogram'] = $t_hasil_indprogram;
        $hasil['program'][$index_program] = $t_dat_program;

        return $hasil;
    }
    /**
     * Get Tujuan
     */
    public function getKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $id_program = $request->id_program;
        $id_kegiatan = $request->id_kegiatan;

        if(!is_null($id_kegiatan))
        {
            $data = DB::select("SELECT
                Kegiatan_90.id_kegiatan,
                Kegiatan_90.kode_kegiatan,
                Kegiatan_90.nama_kegiatan,
                IndikatorKegiatan_90.id_indikator_kegiatan,
                IndikatorKegiatan_90.indikator_kegiatan,
                R_IndKegSubKeg.id_rel
            FROM IndikatorKegiatan_90
            JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = IndikatorKegiatan_90.id_kegiatan
            JOIN Renja_90 ON Renja_90.id_renja = IndikatorKegiatan_90.id_renja
            JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
            LEFT JOIN R_IndKegSubKeg ON R_IndKegSubKeg.id_indikator_kegiatan = IndikatorKegiatan_90.id_indikator_kegiatan
                AND R_IndKegSubKeg.id_renja = IndikatorKegiatan_90.id_renja
            WHERE Renja_90.periode_usulan = ?
                AND Program_90.id_program = ?
                AND Kegiatan_90.id_kegiatan = ?
            GROUP BY
                Kegiatan_90.id_kegiatan,
                Kegiatan_90.kode_kegiatan,
                Kegiatan_90.nama_kegiatan,
                IndikatorKegiatan_90.id_indikator_kegiatan,
                IndikatorKegiatan_90.indikator_kegiatan,
                R_IndKegSubKeg.id_rel
            ",array($tahun,$id_program,$id_kegiatan));

        }else{
            $data = DB::select("SELECT
                Kegiatan_90.id_kegiatan,
                Kegiatan_90.kode_kegiatan,
                Kegiatan_90.nama_kegiatan,
                IndikatorKegiatan_90.id_indikator_kegiatan,
                IndikatorKegiatan_90.indikator_kegiatan,
                R_IndKegSubKeg.id_rel
            FROM IndikatorKegiatan_90
            JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = IndikatorKegiatan_90.id_kegiatan
            JOIN Renja_90 ON Renja_90.id_renja = IndikatorKegiatan_90.id_renja
            JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
            LEFT JOIN R_IndKegSubKeg ON R_IndKegSubKeg.id_indikator_kegiatan = IndikatorKegiatan_90.id_indikator_kegiatan
                AND R_IndKegSubKeg.id_renja = IndikatorKegiatan_90.id_renja
            WHERE Renja_90.periode_usulan = ?
                AND Program_90.id_program = ?
            GROUP BY
                Kegiatan_90.id_kegiatan,
                Kegiatan_90.kode_kegiatan,
                Kegiatan_90.nama_kegiatan,
                IndikatorKegiatan_90.id_indikator_kegiatan,
                IndikatorKegiatan_90.indikator_kegiatan,
                R_IndKegSubKeg.id_rel
            ",array($tahun,$id_program));
        }        

        $t_id_kegiatan = null; //tampung id kegiatan
        $t_id_indkegiatan = null; //tampung id indikator kegiatan

        $t_dat_kegiatan = null; //tampung data kegiatan
        $t_dat_indkegiatan = null; //tampung data indikator kegiatan

        $t_hasil_indkegiatan = null; //tampung hasil data indikator kegiatan
        
        $index_kegiatan = 0;
        $index_indkegiatan = 0;
        $index_data = 0;
        $hasil = null;
        foreach($data as $dat)
        {
            // Proses konversi data
            
            // Simpan data
            if(!is_null($t_id_indkegiatan) && ($dat->id_indikator_kegiatan != $t_id_indkegiatan))
            {
                // indikator kegiatan
                $t_hasil_indkegiatan[$index_indkegiatan] = $t_dat_indkegiatan;
                $index_indkegiatan++;
            }
            if(!is_null($t_id_kegiatan) && ($dat->id_kegiatan != $t_id_kegiatan))
            {
                // kegiatan
                $t_dat_kegiatan['indikatorkegiatan'] = $t_hasil_indkegiatan;
                $hasil['kegiatan'][$index_kegiatan] = $t_dat_kegiatan;
                $index_kegiatan++;

                if($index_data > count($data)-1)
                {
                    $index_indkegiatan = 0;
                    $t_hasil_indkegiatan = null;
                }
            }
            
            // tampung id dan data
            $t_id_kegiatan = $dat->id_kegiatan;
            $t_id_indkegiatan = $dat->id_indikator_kegiatan;

            $t_dat_indkegiatan = array(
                'id_indikator_kegiatan' => $dat->id_indikator_kegiatan,
                'indikator_kegiatan' => $dat->indikator_kegiatan,
                'id_rel' => $dat->id_rel
            );
            $t_dat_kegiatan = array(
                'id_kegiatan' => $dat->id_kegiatan,
                'kode_kegiatan' => $dat->kode_kegiatan,
                'nama_kegiatan' => $dat->nama_kegiatan,
                'indikatorkegiatan' => null
            );
            $index_data++;
        }
        
        $t_hasil_indkegiatan[$index_indkegiatan] = $t_dat_indkegiatan;
        $t_dat_kegiatan['indikatorkegiatan'] = $t_hasil_indkegiatan;
        $hasil['kegiatan'][$index_kegiatan] = $t_dat_kegiatan;

        return $hasil;        
    }
    /**
     * Get Tujuan
     */
    public function getSubKegiatanPohonKinerja(Request $request)
    {
        
    }
    /**
     * Get Daftar Indikator Tujuan
     */
    public function getIndikatorTujuan(Request $request)
    {
        $tahun = $request->tahun;
        $id_skpd = $request->id_skpd;
        $id_tujuan = $request->id_tujuan;

        $data = DB::select("SELECT
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan,
                        R_IndTuProg.id_rel
                    FROM IndikatorTujuan
                    JOIN TargetRealisasiTujuan ON TargetRealisasiTujuan.id_indikator_tujuan = IndikatorTujuan.id_indikator_tujuan
                    JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
                    JOIN SKPD_90 ON SKPD_90.id_skpd = TargetRealisasiTujuan.id_skpd
                    JOIN Periode ON Periode.id_periode = TargetRealisasiTujuan.id_periode
                    LEFT JOIN R_IndTuProg ON R_IndTuProg.id_indikator_tujuan = IndikatorTujuan.id_indikator_tujuan 
                    JOIN Renja_90 ON Renja_90.id_renja = R_IndTuProg.id_renja
                    WHERE Periode.tahun_akhir >= ?
                        AND Periode.tahun_awal <= ?
                        AND SKPD_90.id_skpd = ?
                        AND Tujuan.id_tujuan = ?
                    GROUP BY
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan,
                        R_IndTuProg.id_rel
                    ORDER BY
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan              
                ",array($tahun,$tahun,$id_skpd,$id_tujuan));
        
        return $data;
    }

    /**
     * Get Daftar Indikator Sasaran
     */
    public function getIndikatorSasaran(Request $request)
    {
        $tahun = $request->tahun;
        $id_skpd = $request->id_skpd;
        $id_sasaran = $request->id_sasaran;

        $data = DB::select("SELECT
                        IndikatorSasaran.id_indikator_sasaran,
                        IndikatorSasaran.indikator_sasaran,
                        R_IndSaTu.id_rel
                    FROM IndikatorSasaran
                    JOIN TargetRealisasiSasaran ON TargetRealisasiSasaran.id_indikator_sasaran = IndikatorSasaran.id_indikator_sasaran
                    JOIN Sasaran ON Sasaran.id_sasaran = IndikatorSasaran.id_sasaran
                    JOIN Periode ON Periode.id_periode = TargetRealisasiSasaran.id_periode
                    LEFT JOIN R_IndSaTu ON R_IndSaTu.id_indikator_sasaran = IndikatorSasaran.id_indikator_sasaran 
                    JOIN Renja_90 ON Renja_90.id_renja = R_IndSaTu.id_renja
                    WHERE Periode.tahun_akhir >= ?
                        AND Periode.tahun_awal <= ?
                        AND Sasaran.id_sasaran = ?
                        AND Renja_90.periode_usulan = ?
                        AND Renja_90.id_skpd = ?
                    GROUP BY
                        IndikatorSasaran.id_indikator_sasaran,
                        IndikatorSasaran.indikator_sasaran,
                        R_IndSaTu.id_rel
                    ORDER BY
                        IndikatorSasaran.id_indikator_sasaran,
                        IndikatorSasaran.indikator_sasaran            
                ",array($tahun,$tahun,$id_sasaran,$tahun,$id_skpd));
        
        return $data;
    }

    /**
     * Get Daftar Indikator Program
     */
    public function getIndikatorProgram()
    {
        $hasil = null;
        $data = DB::select("SELECT
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program,
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd
                    FROM IndikatorProgramRPJMD
                    JOIN TargetRealisasiProgramV2 ON TargetRealisasiProgramV2.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                    JOIN Program_90 ON Program_90.id_program = IndikatorProgramRPJMD.id_program_90
                    JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
                    JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                    GROUP BY
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd,
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program
                    ORDER BY
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd,
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program             
                ");

        $id_skpd = null;
        $dat_skpd = null;
        $id_program = null;
        $dat_program = null;
        $hasil_program = null;
        $id_indikator_program = null;
        $dat_indikator_program = null;
        $hasil_indikator_program = null;
        $index = 0;
        foreach($data as $dat)
        {
            if(!is_null($id_indikator_program) && $id_indikator_program != $dat->id_indikator_program)
            {
                $hasil_indikator_program[] = $dat_indikator_program;
            }

            if(!is_null($id_program) && $id_program != $dat->id_program)
            {
                $dat_program['indikatorprogram'] = $hasil_indikator_program;
                $hasil_program[] = $dat_program;
                if($index < count($data))
                {
                    $hasil_indikator_program = null;
                }
            }

            if(!is_null($id_skpd) && $id_skpd != $dat->id_skpd)
            {
                $dat_skpd['program'] = $hasil_program;
                $hasil['skpd'][] = $dat_skpd;

                if($index < count($data))
                {
                    $hasil_program = null;
                }
            }

            $id_skpd = $dat->id_skpd;
            $dat_skpd = array(
                'id_skpd' => $dat->id_skpd,
                'nama_skpd'    => $dat->nama_skpd,
                'program' => null
            );
            $id_program = $dat->id_program;
            $dat_program = array(
                'id_program'        => $dat->id_program,
                'nama_program'      => $dat->nama_program,
                'indikatorprogram'  => null
            );
            $id_indikator_program = $dat->id_indikator_program;
            $dat_indikator_program = array(
                'id_indikator_program' => $dat->id_indikator_program,
                'indikator_program'    => $dat->indikator_program
            );
            $index++;
        }
        if(!is_null($hasil))
        {
            $hasil_program['indikatorprogram'] = $hasil_indikator_program;
            $dat_skpd['program'] = $hasil_program;
            $hasil['skpd'][] = $dat_skpd;
        }

        return $hasil;
    }

    /**
     * Get Select Option Tagging
     */
    public function taggingList()
    {
        $data = array(
            array('title' => 'SDGs','value' => 'sdgs'),
            array('title' => 'SPM','value' => 'spm'),
            array('title' => 'Kemiskinan','value' => 'kemiskinan'),
            array('title' => 'Stunting','value' => 'stunting'),
            array('title' => 'Pendidikan','value' => 'pendidikan'),
            array('title' => 'Kesehatan','value' => 'kesehatan'),
            array('title' => 'Kota Layak Anak','value' => 'kota_layak_anak'),
            array('title' => 'Ketahanan Pangan','value' => 'ketahanan_pangan'),
            array('title' => 'Inflasi','value' => 'inflasi'),
            array('title' => 'Investasi','value' => 'investasi'),
            array('title' => 'Ketenagakerjaan','value' => 'ketenagakerjaan'),
            array('title' => 'Prioritas Nasional','value' => 'prioritas_nasional'),
            array('title' => 'Prioritas Provinsi','value' => 'prioritas_provinsi'),
            array('title' => 'Prioritas RPJMD','value' => 'prioritas_rpjmd'),
            array('title' => 'Prioritas Walikota','value' => 'prioritas_walikota'),
            array('title' => 'Prioritas RKPD','value' => 'prioritas_rkpd'),
            array('title' => 'DAK','value' => 'dak'),
            array('title' => 'BANKEU','value' => 'bankeu'),
            array('title' => 'DIF','value' => 'dif'),
            array('title' => 'POKIR','value' => 'pokir'),
            array('title' => 'Infrastruktur Pelayanan Dasar','value' => 'infrastruktur_pelayanan_dasar'),
            array('title' => 'GRK','value' => 'grk')
        );

        return $data;
    }
    /**
     * Get Daftar Sub Kegiatan
     */
    public function getSubKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $id_skpd = $request->id_skpd;
        $perubahan = 0;

        if($triwulan > 3)
        {
            $perubahan = 1;
        }

        $data = Program_90::whereHas('kegiatan', function ($q_keg) use ($tahun,$perubahan,$id_skpd) {
            $q_keg->whereHas('subkegiatan', function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                });
            });
        })
        ->with(['kegiatan' => function ($q_keg) use ($tahun,$perubahan,$id_skpd) {
            $q_keg->whereHas('subkegiatan', function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                });
            })
            ->with(['subkegiatan' => function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                })
                ->with(['taggingdata'
                    ,'logrenja90' => function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan)
                    ->with(['renja' => function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ])
                        ->with('skpd');
                    }]);
                }]);
            }]);
        }])->get();

        return API_RENJAResource::collection($data);
    }

    /**
     * Save Tagging Data
     */
    public function saveTaggingData(Request $request)
    {
        $id_skpd = $request->id_skpd;
        $tahun = $request->tahun;
        $id_sub_kegiatan = $request->id_sub_kegiatan;
        $daftar_tagging = array(
            'sdgs',
            'spm',
            'kemiskinan',
            'stunting',
            'pendidikan',
            'kesehatan',
            'kota_layak_anak',
            'ketahanan_pangan',
            'inflasi',
            'investasi',
            'ketenagakerjaan',
            'prioritas_nasional',
            'prioritas_provinsi',
            'prioritas_rpjmd',
            'prioritas_walikota',
            'prioritas_rkpd',
            'dak',
            'bankeu',
            'dif',
            'pokir',
            'infrastruktur_pelayanan_dasar',
            'grk'
        );

        if(!is_null($id_skpd) && !is_null($tahun))
        {
            $renja = Renja_90::where([
                    ['periode_usulan',$tahun],
                    ['id_skpd',$id_skpd]
                ])->first();
            
            if(!is_null($renja) && !is_null($id_sub_kegiatan))
            {
                $cek = TaggingData::where([
                        ['id_renja',$renja->id_renja],
                        ['id_sub_kegiatan',$id_sub_kegiatan]
                    ])->first();

                if(!is_null($cek))
                {
                    // update
                    $data = TaggingData::where('id_tagging',$cek->id_tagging)->first();
                    
                    // reset tagging
                    foreach($daftar_tagging as $dtag)
                    {
                        $data->$dtag = null;
                    }
                    foreach($request->tagging as $tag)
                    {
                        $data->$tag = 1;
                    }
                    $data->id_renja = $renja->id_renja;
                    $data->id_sub_kegiatan = $id_sub_kegiatan;

                    if($data->save())
                    {
                        return $data;
                    }
                }else{
                    // tambah
                    $data = new TaggingData();
                    
                    foreach($request->tagging as $tag)
                    {
                        $data->$tag = 1;
                    }
                    $data->id_renja = $renja->id_renja;
                    $data->id_sub_kegiatan = $id_sub_kegiatan;
    
                    if($data->save())
                    {
                        return $data;
                    }
                }
            }
        }
    }

    /**
     * Save Pohon Kinerja
     */
    public function savePohonKinerja(Request $request)
    {
        $id_indikator_sasaran = $request->id_indikator_sasaran;
        $id_indikator_tujuan = $request->id_indikator_tujuan;
        $id_indikator_program = $request->id_indikator_program;
        $id_indikator_kegiatan = $request->id_indikator_kegiatan;
        $id_indikator_subkegiatan = $request->id_indikator_subkegiatan;
        $id_skpd = $request->id_skpd;
        $tahun = $request->tahun;
        $hasil = null;
        if(!is_null($tahun) && !is_null($id_skpd))
        {
            $renja = Renja_90::where([
                ['periode_usulan',$tahun],
                ['id_skpd',$id_skpd]
            ])->first();
            if(!is_null($renja))
            {
                $id_renja = $renja->id_renja;
                // cek indikator apakah sudah ada atau belum
                $data_r_indsatu = R_IndSaTu::where([
                    ['id_indikator_sasaran',$id_indikator_sasaran],
                    ['id_indikator_tujuan',$id_indikator_tujuan],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indtuprog = R_IndTuProg::where([
                    ['id_indikator_tujuan',$id_indikator_tujuan],
                    ['id_indikator_program',$id_indikator_program],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indprogkeg = R_IndProgKeg::where([
                    ['id_indikator_program',$id_indikator_program],
                    ['id_indikator_kegiatan',$id_indikator_kegiatan],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indkegsubkeg = R_IndKegSubKeg::where([
                    ['id_indikator_kegiatan',$id_indikator_kegiatan],
                    ['id_indikator_subkegiatan',$id_indikator_subkegiatan],
                    ['id_renja',$id_renja]
                ])->first();
    
                // jika data indikator belum ada
                if(is_null($data_r_indsatu))
                {
                    $data_r_indsatu = new R_IndSaTu();
                }
                if(is_null($data_r_indtuprog))
                {
                    $data_r_indtuprog = new R_IndTuProg();
                }
                if(is_null($data_r_indprogkeg))
                {
                    $data_r_indprogkeg = new R_IndProgKeg();
                }
                if(is_null($data_r_indkegsubkeg))
                {
                    $data_r_indkegsubkeg = new R_IndKegSubKeg();
                }
    
                // input data atau update data
                $data_r_indsatu->id_indikator_sasaran = $id_indikator_sasaran;
                $data_r_indsatu->id_indikator_tujuan = $id_indikator_tujuan;
                $data_r_indsatu->id_renja = $id_renja;

                $data_r_indtuprog->id_indikator_tujuan = $id_indikator_tujuan;
                $data_r_indtuprog->id_indikator_program = $id_indikator_program;
                $data_r_indtuprog->id_renja = $id_renja;

                $data_r_indprogkeg->id_indikator_program = $id_indikator_program;
                $data_r_indprogkeg->id_indikator_kegiatan = $id_indikator_kegiatan;
                $data_r_indprogkeg->id_renja = $id_renja;

                $data_r_indkegsubkeg->id_indikator_kegiatan = $id_indikator_kegiatan;
                $data_r_indkegsubkeg->id_indikator_subkegiatan = $id_indikator_subkegiatan;
                $data_r_indkegsubkeg->id_renja = $id_renja;
                
    
                if($data_r_indsatu->save())
                {
                    $hasil[] = $data_r_indsatu;
                }            
                if($data_r_indtuprog->save())
                {
                    $hasil[] = $data_r_indtuprog;
                }
                if($data_r_indprogkeg->save())
                {
                    $hasil[] = $data_r_indprogkeg;
                }
                if($data_r_indkegsubkeg->save())
                {
                    $hasil[] = $data_r_indkegsubkeg;
                }
            }
        }        

        if(is_null($hasil))
        {
            $hasil = array('message' => 'Proses Simpan Data Gagal!');
        }
        return $hasil;
    }
}

<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use App\Models\IndikatorSasaran;
use App\Models\Program_90;
use App\Models\Kegiatan_90;
use App\Models\SubKegiatan_90;
use App\Models\Renja_90;
use App\Models\TaggingData;
use App\Models\Sasaran;
use App\Models\Tujuan;
use App\Models\RelSasTuj;
use App\Models\R_IndSaTu;
use App\Models\R_IndTuProg;
use App\Models\R_IndProgKeg;
use App\Models\R_IndKegSubKeg;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Api\API_RENJAResource;

class PerencanaanController extends Controller
{
    //
    /**
     * Get Sasaran
     */
    public function getSasaran(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $id_skpd = $request->id_skpd;
        $perubahan = 0;
        if($triwulan > 3)
        {
            $perubahan = 1;
        }
        $data = Sasaran::whereHas('indikatorsasaran', function ($q_whisasaran) use ($tahun) {
                $q_whisasaran->whereHas('targetrealisasisasaran', function ($q_whtrsasaran) use ($tahun) {
                    $q_whtrsasaran->with('periode')
                    ->whereHas('periode', function ($q_whperiode) use ($tahun) {
                        $q_whperiode->where([
                            ['tahun_awal','<=',$tahun],
                            ['tahun_akhir','>=',$tahun]
                        ]);
                    });
                });
            })
            ->whereHas('relsastuj', function ($q_whrelsastuj) use ($tahun,$perubahan,$id_skpd) {
            // SASARAN
            $q_whrelsastuj->whereHas('tujuan', function ($q_whtujuan) use ($tahun,$perubahan,$id_skpd) {
                $q_whtujuan->whereHas('reltujprog', function ($q_whreltujprog) use ($tahun,$perubahan,$id_skpd) {
                    $q_whreltujprog->whereHas('program', function ($q_whprog) use ($tahun,$perubahan,$id_skpd) {
                        $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan,$id_skpd) {
                            $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            });
                        });
                    });
                });
            });
        })
        ->with(['indikatorsasaran' => function ($q_isasaran) use ($tahun,$id_skpd) {
            // INDIKATOR SASARAN
                $q_isasaran->whereHas('targetrealisasisasaran', function ($q_whtrsasaran) use ($tahun,$id_skpd) {
                    $q_whtrsasaran->with('periode')
                    ->whereHas('periode', function ($q_whperiode) use ($tahun) {
                        $q_whperiode->where([
                            ['tahun_awal','<=',$tahun],
                            ['tahun_akhir','>=',$tahun]
                        ]);
                    });
                })
                ->with(['rindsatu' => function ($q_rindsatu) use ($tahun,$id_skpd) {
                        $q_rindsatu->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                            $q_whrenja->where([
                                ['periode_usulan',$tahun],
                                ['id_skpd',$id_skpd]
                            ]);
                        });
                    }
                    ,'targetrealisasisasaran' => function ($q_trsasaran) use ($tahun) {
                    // TARGET REALISASI SASARAN
                    $q_trsasaran->with('periode')
                    ->whereHas('periode', function ($q_whperiode) use ($tahun) {
                        $q_whperiode->where([
                            ['tahun_awal','<=',$tahun],
                            ['tahun_akhir','>=',$tahun]
                        ]);
                    });
                }]);
            }
            ,'relsastuj' => function ($q_relsastuj) use ($tahun,$perubahan,$id_skpd) {
            // RELASI SASARAN TUJUAN
            $q_relsastuj->whereHas('tujuan', function ($q_whtujuan) use ($tahun,$perubahan,$id_skpd) {
                $q_whtujuan->whereHas('reltujprog', function ($q_whreltujprog) use ($tahun,$perubahan,$id_skpd) {
                    $q_whreltujprog->whereHas('program', function ($q_whprog) use ($tahun,$perubahan,$id_skpd) {
                        $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan,$id_skpd) {
                            $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            });
                        });
                    });
                });
            })
            ->with(['tujuan' => function ($q_tujuan) use ($tahun,$perubahan,$id_skpd) {
                // TUJUAN
                $q_tujuan->whereHas('indikatortujuan', function ($q_whitujuan) use ($tahun,$id_skpd) {
                    $q_whitujuan->whereHas('targetrealisasitujuan', function ($q_whtrtujuan) use ($tahun,$id_skpd) {
                        $q_whtrtujuan->with('periode')
                        ->whereHas('periode', function ($q_whperiode) use ($tahun,$id_skpd) {
                            $q_whperiode->where([
                                ['tahun_awal','<=',$tahun],
                                ['tahun_akhir','>=',$tahun],
                                ['id_skpd','=',$id_skpd]
                            ]);
                        });
                    });
                })
                ->whereHas('reltujprog', function ($q_whreltujprog) use ($tahun,$perubahan,$id_skpd) {
                    $q_whreltujprog->whereHas('program', function ($q_whprog) use ($tahun,$perubahan,$id_skpd) {
                        $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan,$id_skpd) {
                            $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            });
                        });
                    });
                })
                ->with(['indikatortujuan' => function ($q_itujuan) use ($tahun,$id_skpd) {
                    // INDIKATOR TUJUAN
                        $q_itujuan->whereHas('targetrealisasitujuan', function ($q_whtrtujuan) use ($tahun,$id_skpd) {
                            $q_whtrtujuan->whereHas('periode', function ($q_whperiode) use ($tahun,$id_skpd) {
                                $q_whperiode->where([
                                    ['tahun_awal','<=',$tahun],
                                    ['tahun_akhir','>=',$tahun],
                                    ['id_skpd','=',$id_skpd]
                                ]);
                            });
                        })
                        ->with(['rindtuprog' => function ($q_rindtuprog) use ($tahun,$id_skpd) {
                                $q_rindtuprog->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                    $q_whrenja->where([
                                        ['periode_usulan',$tahun],
                                        ['id_skpd',$id_skpd]
                                    ]);
                                });
                            }
                            ,'targetrealisasitujuan' => function ($q_trtujuan) use ($tahun,$id_skpd) {
                            // TARGET REALISASI TUJUAN
                            $q_trtujuan->whereHas('periode', function ($q_whperiode) use ($tahun,$id_skpd) {
                                $q_whperiode->where([
                                    ['tahun_awal','<=',$tahun],
                                    ['tahun_akhir','>=',$tahun],
                                    ['id_skpd','=',$id_skpd]
                                ]);
                            });
                        }]);
                    }
                    ,'reltujprog' => function ($q_reltujprog) use ($tahun,$perubahan,$id_skpd) {
                    // RELASI TUJUAN PROGRAM
                    $q_reltujprog->whereHas('program', function ($q_whprog) use ($tahun,$perubahan,$id_skpd) {
                        $q_whprog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan,$id_skpd) {
                            $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            });
                        });
                    })
                    ->with(['program' => function ($q_prog) use ($tahun,$perubahan,$id_skpd) {
                        // PROGRAM                        
                        $q_prog->whereHas('kegiatan', function ($q_whkeg) use ($tahun,$perubahan,$id_skpd) {
                            $q_whkeg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            });
                        })
                        ->with(['indikator' => function ($q_iprog) use ($tahun,$id_skpd) {
                                // INDIKATOR PROGRAM 
                                $q_iprog->whereHas('TargetRealisasiProgramV2', function ($q_whtrprog) use ($tahun,$id_skpd) {
                                    $q_whtrprog->whereHas('Renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    });
                                })
                                ->with(['rindprogkeg' => function ($q_indprogkeg) use ($tahun,$id_skpd) {
                                        $q_indprogkeg->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                            $q_whrenja->where([
                                                ['periode_usulan',$tahun],
                                                ['id_skpd',$id_skpd]
                                            ]);
                                        });
                                    }
                                    ,'TargetRealisasiProgramV2' => function ($q_trprog) use ($tahun,$id_skpd) {
                                    // TARGET REALISASI PROGRAM
                                    $q_trprog->whereHas('Renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    });
                                }]);
                            }
                            ,'kegiatan' => function ($q_keg) use ($tahun,$perubahan,$id_skpd) {
                            // KEGIATAN
                            $q_keg->whereHas('subkegiatan', function ($q_whsubkeg) use ($tahun,$perubahan,$id_skpd) {
                                $q_whsubkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd){
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                });
                            })
                            ->with(['indikatorkegiatan' => function ($q_ikeg) use ($tahun,$id_skpd) {
                                    // INDIKATOR KEGIATAN
                                    $q_ikeg->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->with(['rindkegsubkeg' => function ($q_rindkegsubkeg) use ($tahun,$id_skpd) {
                                            $q_rindkegsubkeg->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                                $q_whrenja->where([
                                                    ['periode_usulan',$tahun],
                                                    ['id_skpd',$id_skpd]
                                                ]);
                                            });
                                        }
                                        ,'realisasiindikatorkeg'
                                    ]);
                                }
                                ,'subkegiatan' => function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                                // SUB KEGIATAN
                                $q_subkeg->whereHas('logrenja90', function ($q_whlogrenja) use ($tahun,$perubahan,$id_skpd) {
                                    $q_whlogrenja->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan);
                                })
                                ->with(['indikatorsubkegiatan' => function ($q_isubkeg) use ($tahun,$id_skpd) {
                                        // INDIKATOR SUB KEGIATAN
                                        $q_isubkeg->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                            $q_whrenja->where([
                                                ['periode_usulan',$tahun],
                                                ['id_skpd',$id_skpd]
                                            ]);
                                        })->with('realisasiindikatorsubkeg');
                                    },
                                    'logrenja90' => function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                                    // LOGRENJA
                                    $q_logrenja->with([
                                        'realisasimurni' => function ($q_rmurni) {
                                            $q_rmurni->select(
                                                'R_LogRenja_90.id_r_logrenja',
                                                'R_LogRenja_90.id_log_renja',
                                                'R_LogRenja_90.r_1',
                                                'R_LogRenja_90.r_2',
                                                'R_LogRenja_90.r_3',
                                                'R_LogRenja_90.r_4'
                                            );
                                        },
                                        'realisasiperubahan' => function ($q_rperubahan) {
                                            $q_rperubahan->select(
                                                'R_LogRenja_90.id_r_logrenja',
                                                'R_LogRenja_90.id_log_renja',
                                                'R_LogRenja_90.r_1',
                                                'R_LogRenja_90.r_2',
                                                'R_LogRenja_90.r_3',
                                                'R_LogRenja_90.r_4'
                                            );
                                        }
                                    ])
                                    ->whereHas('renja', function ($q_whrenja) use ($tahun,$id_skpd) {
                                        $q_whrenja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ]);
                                    })
                                    ->where('isPerubahan',$perubahan)
                                    ->with(['renja' => function ($q_renja) use ($tahun,$id_skpd) {
                                        // RENJA
                                        $q_renja->where([
                                            ['periode_usulan',$tahun],
                                            ['id_skpd',$id_skpd]
                                        ])
                                        ->with('skpd');
                                    }]);
                                }]);
                            }]);
                        }]);
                    }]);
                }]);
            }]);
        }])
        ->get();

        return API_RENJAResource::collection($data);
    }

    /**
     * Get Daftar Indikator Tujuan
     */
    public function getIndikatorTujuan()
    {
        $hasil = null;
        $data = DB::select("SELECT
                        Tujuan.id_tujuan,
                        Tujuan.tujuan,
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan,
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd
                    FROM IndikatorTujuan
                    JOIN TargetRealisasiTujuan ON TargetRealisasiTujuan.id_indikator_tujuan = IndikatorTujuan.id_indikator_tujuan
                    JOIN Tujuan ON Tujuan.id_tujuan = IndikatorTujuan.id_tujuan
                    JOIN SKPD_90 ON SKPD_90.id_skpd = TargetRealisasiTujuan.id_skpd
                    GROUP BY
                        Tujuan.id_tujuan,
                        Tujuan.tujuan,
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan,
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd
                    ORDER BY
                        Tujuan.id_tujuan,
                        Tujuan.tujuan,
                        IndikatorTujuan.id_indikator_tujuan,
                        IndikatorTujuan.indikator_tujuan,
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd                
                ");

        $id_tujuan = null;
        $dat_tujuan = null;
        $id_indikator_tujuan = null;
        $dat_indikator_tujuan = null;
        $hasil_indikator_tujuan = null;
        $id_skpd = null;
        $dat_skpd = null;
        $hasil_skpd = null;
        
        foreach($data as $dat)
        {
            if(!is_null($id_skpd) && $id_skpd != $dat->id_skpd)
            {
                $hasil_skpd[] = $dat_skpd;
            }

            if(!is_null($id_indikator_tujuan) && $id_indikator_tujuan != $dat->id_indikator_tujuan)
            {
                $dat_indikator_tujuan['skpd'] = $hasil_skpd;
                $hasil_indikator_tujuan[] = $dat_indikator_tujuan;
                $hasil_skpd = null;
            }

            if(!is_null($id_tujuan) && $id_tujuan != $dat->id_tujuan)
            {
                $dat_tujuan['indikatortujuan'] = $hasil_indikator_tujuan;
                $hasil['tujuan'][] = $dat_tujuan;
                $hasil_indikator_tujuan = null;
            }

            $id_tujuan = $dat->id_tujuan;
            $dat_tujuan = array(
                'id_tujuan' => $dat->id_tujuan,
                'tujuan'    => $dat->tujuan,
                'indikatortujuan' => null
            );
            $id_indikator_tujuan = $dat->id_indikator_tujuan;
            $dat_indikator_tujuan = array(
                'id_indikator_tujuan' => $dat->id_indikator_tujuan,
                'indikator_tujuan'    => $dat->indikator_tujuan,
                'skpd' => null
            );
            $id_skpd = $dat->id_skpd;
            $dat_skpd = array(
                'id_skpd' => $dat->id_skpd,
                'nama_skpd'    => $dat->nama_skpd
            );
        }
        if(!is_null($hasil))
        {
            $dat_indikator_tujuan['skpd'] = $dat_skpd;
            $dat_tujuan['indikatortujuan'] = $dat_indikator_tujuan;
            $hasil['tujuan'][] = $dat_tujuan;
        }

        return $hasil;
    }

    /**
     * Get Daftar Indikator Sasaran
     */
    public function getIndikatorSasaran()
    {
        $hasil = null;
        $data = IndikatorSasaran::with('sasaran')->get();
        return $data;
    }

    /**
     * Get Daftar Indikator Program
     */
    public function getIndikatorProgram()
    {
        $hasil = null;
        $data = DB::select("SELECT
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program,
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd
                    FROM IndikatorProgramRPJMD
                    JOIN TargetRealisasiProgramV2 ON TargetRealisasiProgramV2.id_indikator_program = IndikatorProgramRPJMD.id_indikator_program
                    JOIN Program_90 ON Program_90.id_program = IndikatorProgramRPJMD.id_program_90
                    JOIN Renja_90 ON Renja_90.id_renja = TargetRealisasiProgramV2.id_renja
                    JOIN SKPD_90 ON SKPD_90.id_skpd = Renja_90.id_skpd
                    GROUP BY
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd,
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program
                    ORDER BY
                        SKPD_90.id_skpd,
                        SKPD_90.nama_skpd,
                        Program_90.id_program,
                        Program_90.nama_program,
                        IndikatorProgramRPJMD.id_indikator_program,
                        IndikatorProgramRPJMD.indikator_program             
                ");

        $id_skpd = null;
        $dat_skpd = null;
        $id_program = null;
        $dat_program = null;
        $hasil_program = null;
        $id_indikator_program = null;
        $dat_indikator_program = null;
        $hasil_indikator_program = null;
        $index = 0;
        foreach($data as $dat)
        {
            if(!is_null($id_indikator_program) && $id_indikator_program != $dat->id_indikator_program)
            {
                $hasil_indikator_program[] = $dat_indikator_program;
            }

            if(!is_null($id_program) && $id_program != $dat->id_program)
            {
                $dat_program['indikatorprogram'] = $hasil_indikator_program;
                $hasil_program[] = $dat_program;
                if($index < count($data))
                {
                    $hasil_indikator_program = null;
                }
            }

            if(!is_null($id_skpd) && $id_skpd != $dat->id_skpd)
            {
                $dat_skpd['program'] = $hasil_program;
                $hasil['skpd'][] = $dat_skpd;

                if($index < count($data))
                {
                    $hasil_program = null;
                }
            }

            $id_skpd = $dat->id_skpd;
            $dat_skpd = array(
                'id_skpd' => $dat->id_skpd,
                'nama_skpd'    => $dat->nama_skpd,
                'program' => null
            );
            $id_program = $dat->id_program;
            $dat_program = array(
                'id_program'        => $dat->id_program,
                'nama_program'      => $dat->nama_program,
                'indikatorprogram'  => null
            );
            $id_indikator_program = $dat->id_indikator_program;
            $dat_indikator_program = array(
                'id_indikator_program' => $dat->id_indikator_program,
                'indikator_program'    => $dat->indikator_program
            );
            $index++;
        }
        if(!is_null($hasil))
        {
            $hasil_program['indikatorprogram'] = $hasil_indikator_program;
            $dat_skpd['program'] = $hasil_program;
            $hasil['skpd'][] = $dat_skpd;
        }

        return $hasil;
    }

    /**
     * Get Daftar Sub Kegiatan
     */
    public function getSubKegiatan(Request $request)
    {
        $tahun = $request->tahun;
        $triwulan = $request->triwulan;
        $id_skpd = $request->id_skpd;
        $perubahan = 0;

        if($triwulan > 3)
        {
            $perubahan = 1;
        }

        $data = Program_90::whereHas('kegiatan', function ($q_keg) use ($tahun,$perubahan,$id_skpd) {
            $q_keg->whereHas('subkegiatan', function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                });
            });
        })
        ->with(['kegiatan' => function ($q_keg) use ($tahun,$perubahan,$id_skpd) {
            $q_keg->whereHas('subkegiatan', function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                });
            })
            ->with(['subkegiatan' => function ($q_subkeg) use ($tahun,$perubahan,$id_skpd) {
                $q_subkeg->whereHas('logrenja90', function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan);
                })
                ->with(['logrenja90' => function ($q_logrenja) use ($tahun,$perubahan,$id_skpd) {
                    $q_logrenja->whereHas('renja', function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ]);
                    })
                    ->where('isPerubahan',$perubahan)
                    ->with(['renja' => function ($q_renja) use ($tahun,$id_skpd) {
                        $q_renja->where([
                            ['periode_usulan',$tahun],
                            ['id_skpd',$id_skpd]
                        ])
                        ->with('skpd');
                    }]);
                }]);
            }]);
        }])->get();

        return API_RENJAResource::collection($data);
    }

    /**
     * Save Tagging Data
     */
    public function saveTaggingData(Request $request)
    {
        $id_skpd = $request->id_skpd;
        $tahun = $request->tahun;
        $id_sub_kegiatan = $request->id_sub_kegiatan;
        $daftar_tagging = array(
            'sdgs',
            'spm',
            'kemiskinan',
            'stunting',
            'pendidikan',
            'kesehatan',
            'kota_layak_anak',
            'ketahanan_pangan',
            'inflasi',
            'investasi',
            'ketenagakerjaan',
            'prioritas_nasional',
            'prioritas_provinsi',
            'prioritas_rpjmd',
            'prioritas_walikota',
            'prioritas_rkpd',
            'dak',
            'bankeu',
            'dif',
            'pokir',
            'infrastruktur_pelayanan_dasar',
            'grk'
        );

        if(!is_null($id_skpd) && !is_null($tahun))
        {
            $renja = Renja_90::where([
                    ['periode_usulan',$tahun],
                    ['id_skpd',$id_skpd]
                ])->first();
            
            if(!is_null($renja) && !is_null($id_sub_kegiatan))
            {
                $cek = TaggingData::where([
                        ['id_renja',$renja->id_renja],
                        ['id_sub_kegiatan',$id_sub_kegiatan]
                    ])->first();

                if(!is_null($cek))
                {
                    // update
                    $data = TaggingData::where('id_tagging',$cek->id_tagging)->first();
                    
                    // reset tagging
                    foreach($daftar_tagging as $dtag)
                    {
                        $data->$dtag = null;
                    }
                    foreach($request->tagging as $tag)
                    {
                        $data->$tag = 1;
                    }
                    $data->id_renja = $renja->id_renja;
                    $data->id_sub_kegiatan = $id_sub_kegiatan;

                    if($data->save())
                    {
                        return $data;
                    }
                }else{
                    // tambah
                    $data = new TaggingData();
                    
                    foreach($request->tagging as $tag)
                    {
                        $data->$tag = 1;
                    }
                    $data->id_renja = $renja->id_renja;
                    $data->id_sub_kegiatan = $id_sub_kegiatan;
    
                    if($data->save())
                    {
                        return $data;
                    }
                }
            }
        }
    }

    /**
     * Save Pohon Kinerja
     */
    public function savePohonKinerja(Request $request)
    {
        $id_indikator_sasaran = $request->id_indikator_sasaran;
        $id_indikator_tujuan = $request->id_indikator_tujuan;
        $id_indikator_program = $request->id_indikator_program;
        $id_indikator_kegiatan = $request->id_indikator_kegiatan;
        $id_indikator_subkegiatan = $request->id_indikator_subkegiatan;
        $id_skpd = $request->id_skpd;
        $tahun = $request->tahun;
        $hasil = null;
        if(!is_null($tahun) && !is_null($id_skpd))
        {
            $renja = Renja_90::where([
                ['periode_usulan',$tahun],
                ['id_skpd',$id_skpd]
            ])->first();
            if(!is_null($renja))
            {
                $id_renja = $renja->id_renja;
                // cek indikator apakah sudah ada atau belum
                $data_r_indsatu = R_IndSaTu::where([
                    ['id_indikator_sasaran',$id_indikator_sasaran],
                    ['id_indikator_tujuan',$id_indikator_tujuan],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indtuprog = R_IndTuProg::where([
                    ['id_indikator_tujuan',$id_indikator_tujuan],
                    ['id_indikator_program',$id_indikator_program],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indprogkeg = R_IndProgKeg::where([
                    ['id_indikator_program',$id_indikator_program],
                    ['id_indikator_kegiatan',$id_indikator_kegiatan],
                    ['id_renja',$id_renja]
                ])->first();
        
                $data_r_indkegsubkeg = R_IndKegSubKeg::where([
                    ['id_indikator_kegiatan',$id_indikator_kegiatan],
                    ['id_indikator_subkegiatan',$id_indikator_subkegiatan],
                    ['id_renja',$id_renja]
                ])->first();
    
                // jika data indikator belum ada
                if(is_null($data_r_indsatu))
                {
                    $data_r_indsatu = new R_IndSaTu();
                }
                if(is_null($data_r_indtuprog))
                {
                    $data_r_indtuprog = new R_IndTuProg();
                }
                if(is_null($data_r_indprogkeg))
                {
                    $data_r_indprogkeg = new R_IndProgKeg();
                }
                if(is_null($data_r_indkegsubkeg))
                {
                    $data_r_indkegsubkeg = new R_IndKegSubKeg();
                }
    
                // input data atau update data
                $data_r_indsatu->id_indikator_sasaran = $id_indikator_sasaran;
                $data_r_indsatu->id_indikator_tujuan = $id_indikator_tujuan;
                $data_r_indsatu->id_renja = $id_renja;

                $data_r_indtuprog->id_indikator_tujuan = $id_indikator_tujuan;
                $data_r_indtuprog->id_indikator_program = $id_indikator_program;
                $data_r_indtuprog->id_renja = $id_renja;

                $data_r_indprogkeg->id_indikator_program = $id_indikator_program;
                $data_r_indprogkeg->id_indikator_kegiatan = $id_indikator_kegiatan;
                $data_r_indprogkeg->id_renja = $id_renja;

                $data_r_indkegsubkeg->id_indikator_kegiatan = $id_indikator_kegiatan;
                $data_r_indkegsubkeg->id_indikator_subkegiatan = $id_indikator_subkegiatan;
                $data_r_indkegsubkeg->id_renja = $id_renja;
                
    
                if($data_r_indsatu->save())
                {
                    $hasil[] = $data_r_indsatu;
                }            
                if($data_r_indtuprog->save())
                {
                    $hasil[] = $data_r_indtuprog;
                }
                if($data_r_indprogkeg->save())
                {
                    $hasil[] = $data_r_indprogkeg;
                }
                if($data_r_indkegsubkeg->save())
                {
                    $hasil[] = $data_r_indkegsubkeg;
                }
            }
        }        

        if(is_null($hasil))
        {
            $hasil = array('message' => 'Proses Simpan Data Gagal!');
        }
        return $hasil;
    }
}

<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Periode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PeriodeController extends Controller
{
    /*
    |---------| 
    |         |
    | Periode |
    |         |
    |---------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Periode::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new Periode();

        $data->tahun_awal = $request->tahunAwal;
        $data->tahun_akhir = $request->tahunAkhir;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Periode::findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Periode::findOrFail($id);

        $data->tahun_awal = $request->tahunAwal;
        $data->tahun_akhir = $request->tahunAkhir;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Periode::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Program_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Program_90Controller extends Controller
{
    /*
    |------------| 
    |            |
    | Program_90 |
    |            |
    |------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Program_90::with([
                                                'bidangUrusan',
                                                'kegiatan'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new Program_90();

        $data->kode_program = $request->kodeProgram;
        $data->nama_program = $request->namaProgram;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_program_rpjmd = $request->programRPJMD;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Program_90::with([
                                        'bidangUrusan',
                                        'kegiatan'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Program_90::findOrFail($id);
        
        $data->kode_program = $request->kodeProgram;
        $data->nama_program = $request->namaProgram;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_program_rpjmd = $request->programRPJMD;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Program_90::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

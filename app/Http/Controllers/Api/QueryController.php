<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Renja\ViewProgramRenja;
use App\Models\Renja\ViewKegiatanRenja;
use App\Models\Renja\ViewSubKegiatanRenja;
use App\Models\Tahun;

use Illuminate\Http\Request;

class QueryController extends Controller
{
    //
    public function getAnggaranProgram()
    {
        $data = ViewProgramRenja::orderBy('periode_usulan')
            ->orderBy('id_skpd')
            ->orderBy('kode_program')
            ->get();

        return $data;
    }
    public function getAnggaranKegiatan()
    {
        $data = ViewKegiatanRenja::with('program')
            ->orderBy('periode_usulan')
            ->orderBy('id_skpd')
            ->orderBy('kode_kegiatan')
            ->get();

        return $data;
    }
    public function getAnggaranSubKegiatan()
    {
        $data = ViewSubKegiatanRenja::with('kegiatan.program')
            ->orderBy('periode_usulan')
            ->orderBy('id_skpd')
            ->orderBy('kode_sub_kegiatan')
            ->get();

        return $data;
    }
    public function showByAnggaranProgram(Request $request)
    {
        $data = null;
        if($request->all == 0 && $request->tahun != null && $request->id_skpd != null)
        {
            $data = ViewProgramRenja::where([
                ['periode_usulan','=',$request->tahun],
                ['id_skpd','=',$request->id_skpd]
            ])
            ->orderBy('kode_program')
            ->get();
        }else if($request->all == 1 && $request->tahun != null)
        {
            $data = ViewProgramRenja::where('periode_usulan',$request->tahun)
            ->orderBy('id_skpd')
            ->orderBy('kode_program')
            ->get();
        }

        return $data;
    }
    public function showByAnggaranKegiatan(Request $request)
    {
        $data = null;
        if($request->all == 0 && $request->tahun != null && $request->id_skpd != null)
        {
            $data = ViewKegiatanRenja::where([
                ['periode_usulan','=',$request->tahun],
                ['id_skpd','=',$request->id_skpd]
            ])
            ->with('program')
            ->orderBy('kode_kegiatan')
            ->get();
        }else if($request->all == 1 && $request->tahun != null)
        {
            $data = ViewKegiatanRenja::where('periode_usulan',$request->tahun)
            ->with('program')
            ->orderBy('id_skpd')
            ->orderBy('kode_kegiatan')
            ->get();
        }

        return $data;
    }
    public function showByAnggaranSubKegiatan(Request $request)
    {
        $data = null;
        if($request->all == 0 && $request->tahun != null && $request->id_skpd != null)
        {
            $data = ViewSubKegiatanRenja::where([
                ['periode_usulan','=',$request->tahun],
                ['id_skpd','=',$request->id_skpd]
            ])
            ->with('kegiatan.program')
            ->orderBy('kode_sub_kegiatan')
            ->get();
        }else if($request->all == 1 && $request->tahun != null)
        {
            $data = ViewSubKegiatanRenja::where('periode_usulan',$request->tahun)
            ->with('kegiatan.program')
            ->orderBy('id_skpd')
            ->orderBy('kode_sub_kegiatan')
            ->get();
        }

        return $data;
    }

    public function showByFisikProgram(Request $request)
    {
        $tahun = Tahun::where('tahun',$request->tahun)->first();
        $all = $request->all;
        $data = null;
        if($tahun)
        {
            if($all == 1)
            {
                $data = ViewProgramRenja::with('indikator.TargetRealisasiIKP')
                    ->whereHas('indikator.TargetRealisasiIKP', function ($qr) use ($tahun) {
                        $qr->where('id_tahun', $tahun->id_tahun);
                    })
                    ->where('periode_usulan',$tahun->tahun)
                    ->get();
            }else if($all == 0 && $request->id_skpd != null)
            {
                $data = ViewProgramRenja::with('indikator.TargetRealisasiIKP')
                    ->whereHas('indikator.TargetRealisasiIKP', function ($qr) use ($tahun) {
                        $qr->where('id_tahun',$tahun->id_tahun);
                    })
                    ->where([
                            ['periode_usulan','=',$tahun->tahun],
                            ['id_skpd','=',$request->id_skpd]
                        ])
                    ->get();
            }
        }
        return $data;
    }
}

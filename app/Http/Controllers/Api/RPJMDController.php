<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\RPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RPJMDController extends Controller
{
    /*
    |-------| 
    |       |
    | RPJMD |
    |       |
    |-------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = RPJMD::with([
                            'Periode'
                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new RPJMD();

        $data->visi_kota = $request->visiKota;
        $data->pen_visi_kota = $request->penVisiKota;
        $data->id_periode = $request->periode;        

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = RPJMD::with([
                            'Periode'
                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = RPJMD::findOrFail($id);

        $data->visi_kota = $request->visiKota;
        $data->pen_visi_kota = $request->penVisiKota;
        $data->id_periode = $request->periode;     

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = RPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RenderController extends Controller
{
    public function rpd()
    {   
        $results = DB::select('SELECT Urusan_90.nama_urusan as urusan,UrusanRPJMD.nama_urusan,Program_90.nama_program,ProgramRPJMD.id_program,IndikatorProgramRPJMD.indikator_program,TargetRealisasiIKP.target,SatuanRPJMD.satuan,SUM(CAST(ap.totalp as FLOAT)) uang FROM ProgramRPJMD
                JOIN IndikatorProgramRPJMD ON IndikatorProgramRPJMD.id_program = ProgramRPJMD.id_program
                JOIN TargetRealisasiIKP ON TargetRealisasiIKP.id_indikator_program  = IndikatorProgramRPJMD.id_indikator_program AND TargetRealisasiIKP.id_tahun = 10
                JOIN SatuanRPJMD ON SatuanRPJMD.id_satuan = IndikatorProgramRPJMD.id_satuan
                JOIN SKPD_90 ON SKPD_90.id_skpd = IndikatorProgramRPJMD.id_skpd
                JOIN Program_90 ON Program_90.id_program_rpjmd = ProgramRPJMD.id_program
                JOIN UrusanRPJMD ON UrusanRPJMD.id_urusan = ProgramRPJMD.id_urusan
                JOIN Urusan_90 ON Urusan_90.kode_urusan = SUBSTRING(UrusanRPJMD.nomor, 1, 1)
                JOIN (SELECT program,totalp FROM masterrpd24) ap ON ap.program = Program_90.nama_program 
                GROUP BY Urusan_90.nama_urusan,UrusanRPJMD.nama_urusan,Program_90.nama_program,ProgramRPJMD.id_program,IndikatorProgramRPJMD.indikator_program,TargetRealisasiIKP.target,SatuanRPJMD.satuan');
        
        return response()->json([
            'data' => $results,
        ]);
    }

    public function renjakeg(Request $request)
    {   
        //$results = DB::select('SELECT * FROM ViewKegiatanRenja JOIN Program_90 ON Program_90.id_program = ViewKegiatanRenja.id_program WHERE periode_usulan = 2024 AND Program_90.nama_program = ?',[$request->program]);
        $kegiatan = 'kegiatan';
        $results = DB::select('SELECT * FROM ViewKegiatanRenja 
        JOIN Program_90 ON Program_90.id_program = ViewKegiatanRenja.id_program 
        LEFT JOIN (SELECT dumy_text,kd_urusan,kd_bidang,kode_prog,kode_keg,koder,SUM(CAST(uang as FLOAT)) as uang FROM dumy_rkpd
                WHERE kode_sub = ? GROUP BY dumy_text,kd_urusan,kd_bidang,kode_prog,kode_keg,koder) ap ON ap.koder = ViewKegiatanRenja.kode_kegiatan
        WHERE periode_usulan = 2024',[$kegiatan]);
        
        return response()->json([
            'data' => $results,
        ]);
    }

    public function renjaprog(Request $request)
    {   
        //$results = DB::select('SELECT * FROM ViewKegiatanRenja JOIN Program_90 ON Program_90.id_program = ViewKegiatanRenja.id_program WHERE periode_usulan = 2024 AND Program_90.nama_program = ?',[$request->program]);
        $program = 'program';
        $results = DB::select('SELECT * FROM ViewProgramRenja
        LEFT JOIN (SELECT dumy_text,kd_urusan,kd_bidang,kode_prog,koder,SUM(CAST(uang as FLOAT)) as uang FROM dumy_rkpd
        WHERE kode_keg = ?
				GROUP BY dumy_text,kd_urusan,kd_bidang,kode_prog,koder) ap ON ap.koder = ViewProgramRenja.kode_program
				WHERE periode_usulan = 2024',[$program]);
        
        return response()->json([
            'data' => $results,
        ]);
    }

    public function renjasub(Request $request)
    {   
        //$results = DB::select('SELECT * FROM ViewKegiatanRenja JOIN Program_90 ON Program_90.id_program = ViewKegiatanRenja.id_program WHERE periode_usulan = 2024 AND Program_90.nama_program = ?',[$request->program]);
        $kegiatan = 'kegiatan';
        $program = 'program';
        $bidang = 'bidang';
        $urusan = 'urusan';
        $kd_urusan = 'kd_urusan';
        
        $results = DB::select('SELECT * FROM ViewSubKegiatanRenja 
                JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = ViewSubKegiatanRenja.id_kegiatan
                LEFT JOIN IndikatorSubkegiatan_90 ON  IndikatorSubkegiatan_90.id_sub_kegiatan = ViewSubKegiatanRenja.id_sub_kegiatan AND IndikatorSubkegiatan_90.id_renja = ViewSubKegiatanRenja.id_renja
            LEFT JOIN (SELECT dumy_text,kd_urusan,kd_bidang,kode_prog,kode_keg,kode_sub,koder,SUM(CAST(uang as FLOAT)) as uang FROM dumy_rkpd
                WHERE kode_sub != ? AND kode_keg != ? AND kode_prog != ? AND kd_bidang != ? AND kd_urusan != ? AND indikator is not null
                        GROUP BY dumy_text,kd_urusan,kd_bidang,kode_prog,kode_keg,kode_sub,koder) ap ON koder = ViewSubKegiatanRenja.kode_sub_kegiatan
                WHERE periode_usulan = 2024',[$kegiatan,$program,$bidang,$urusan,$kd_urusan]);
        
        return response()->json([
            'data' => $results,
        ]);
    }
}

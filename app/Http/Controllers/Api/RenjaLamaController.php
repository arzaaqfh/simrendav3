<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\API_RENJAResource;
use App\Models\Urusan;
use Illuminate\Http\Request;

class RenjaLamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Urusan::with('program.kegiatan')->get();
        
        
        return API_RENJAResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        echo 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo 'store';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tahun)
    {
        //
        $data = Urusan::with([
            'program' => function ($q) use ($tahun) {
                $q->with([
                    'kegiatan' => function ($q) use ($tahun) {
                        $q->with([
                            'logrenja' => function ($q) use ($tahun) {
                                $q->whereHas('renja', function ($q) use ($tahun) {
                                    $q->where('periode_usulan',$tahun);
                                });
                            }
                        ]);
                    }
                ]);
            }
        ])
        ->where('isDeleted',0)
        ->get();

        return API_RENJAResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        echo 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        echo 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        echo 'destroy';
    }
}

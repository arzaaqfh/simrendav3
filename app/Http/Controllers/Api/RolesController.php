<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Validator;
use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Roles::get();
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {            
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'nama_roles' => 'required'
        ]);
        
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $hasil = null;

            $data = new Roles();
            $data->nama_roles = $request->nama_roles;
            $data->keterangan = $request->keterangan;

            if($data->save())
            {
                $hasil = response()->json([
                                'status' => 200,
                                'message' => 'role berhasil ditambahkan!',
                                'data' => $data,
                            ]);
            }
            return $hasil;            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Roles::where('id_role',$id)->get();
        
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'nama_roles' => 'required'
        ]);
        
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $hasil = null;

            $data = Roles::find($id);
            if($data)
            {
                $data->nama_roles = $request->nama_roles;
                $data->keterangan = $request->keterangan;
    
                if($data->save())
                {
                    $hasil = response()->json([
                                    'status' => 200,
                                    'message' => 'role berhasil diupdate!',
                                    'data' => $data,
                                ]);
                }else{
                    $hasil = response()->json([
                                    'status' => 500,
                                    'message' => 'role gagal diupdate!',
                                    'data' => $data,
                                ]);
                }                
            }
            return $hasil;            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Roles::find($id);
        if($data->delete())
        {
            $hasil = response()->json([
                                'status' => 200,
                                'message' => 'role berhasil dihapus!',
                                'data' => $data,
                            ]);             
        }else{
            $hasil = response()->json([
                            'status' => 500,
                            'message' => 'role gagal dihapus!',
                            'data' => $data,
                        ]);
        }
        return $hasil;  
    }
}

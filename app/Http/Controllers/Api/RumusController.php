<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Rumus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RumusController extends Controller
{
    /*
    |-------| 
    |       |
    | Rumus |
    |       |
    |-------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Rumus::orderBy('rumus')->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }
    public function indexPositif()
    {
        $getData = Rumus::orderBy('rumus')->where('isPositif',1)->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new Rumus();

        $data->rumus = $request->rumus;      

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Rumus::findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Rumus::findOrFail($id);

        $data->rumus = $request->rumus;   

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Rumus::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

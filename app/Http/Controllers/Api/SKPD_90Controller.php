<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SKPD_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SKPD_90Controller extends Controller
{
    /*
    |---------| 
    |         |
    | SKPD 90 |
    |         |
    |---------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SKPD_90::with([
                                'Bidang'
                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SKPD_90();
        $id_skpd =  SKPD_90::orderBy('id_skpd','DESC')->first()->id_skpd+1;
        $data->id_skpd = $id_skpd;
        $data->kode_skpd = $request->kodeSkpd;
        $data->nama_skpd = $request->namaSkpd;
        $data->id_bidang = $request->bidang;
    
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SKPD_90::with([
                            'Bidang'
                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SKPD_90::findOrFail($id);
    
        $data->kode_skpd = $request->kodeSkpd;
        $data->nama_skpd = $request->namaSkpd;
        $data->id_bidang = $request->bidang;

        $data->timestamps = true;
        if($data->save()){
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SKPD_90::findOrFail($id);
        if($data->delete()){
            return new API_SimrendaResource($data);
        }
    }
}

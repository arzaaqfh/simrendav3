<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Anggaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_AnggaranController extends Controller
{
    /*
    |--------------| 
    |              |
    | SPM Anggaran |
    |              |
    |--------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Anggaran::with([
                                                'bidangUrusan',
                                                'jenisLayanan',
                                                'kegiatan',
                                                'subKegiatan',
                                                'tahun'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Anggaran();

        $data->pagu = $request->pagu;
        $data->realisasi = $request->realisasi;
        $data->serapan = $request->serapan;
        $data->id_urusan = $request->urusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_kegiatan = $request->kegiatan;
        $data->id_sub_kegiatan = $request->subKegiatan;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Anggaran::with([
                                        'bidangUrusan',
                                        'jenisLayanan',
                                        'kegiatan',
                                        'subKegiatan',
                                        'tahun'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Anggaran::findOrFail($id);
        
        $data->pagu = $request->pagu;
        $data->realisasi = $request->realisasi;
        $data->serapan = $request->serapan;
        $data->id_urusan = $request->urusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_kegiatan = $request->kegiatan;
        $data->id_sub_kegiatan = $request->subKegiatan;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Anggaran::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

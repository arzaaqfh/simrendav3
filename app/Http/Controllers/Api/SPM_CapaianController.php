<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Capaian;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_CapaianController extends Controller
{
    /*
    |-------------| 
    |             |
    | SPM Capaian |
    |             |
    |-------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Capaian::with([
                                                'bidangUrusan',
                                                'jenisLayanan',
                                                'rincian',
                                                'indikator',
                                                'tahun'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Capaian();

        $data->jml_dibutuhkan = $request->jmlDibutuhkan;
        $data->jml_tersedia = $request->jmlTersedia;
        $data->jml_blm_tersedia = $request->jmlBlmTersedia;
        $data->persentase_capaian = $request->persentaseCapaian;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_rincian = $request->rincian;
        $data->id_indikator = $request->indikator;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Capaian::with([
                                        'bidangUrusan',
                                        'jenisLayanan',
                                        'rincian',
                                        'indikator',
                                        'tahun'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Capaian::findOrFail($id);
        
        $data->jml_dibutuhkan = $request->jmlDibutuhkan;
        $data->jml_tersedia = $request->jmlTersedia;
        $data->jml_blm_tersedia = $request->jmlBlmTersedia;
        $data->persentase_capaian = $request->persentaseCapaian;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_rincian = $request->rincian;
        $data->id_indikator = $request->indikator;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Capaian::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

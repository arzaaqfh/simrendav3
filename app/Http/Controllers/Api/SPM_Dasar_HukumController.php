<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Dasar_Hukum;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_Dasar_HukumController extends Controller
{
    /*
    |-----------------| 
    |                 |
    | SPM Dasar Hukum |
    |                 |
    |-----------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Dasar_Hukum::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Dasar_Hukum();

        $data->nama_peraturan = $request->namaPeraturan;
        $data->dokumen = $request->dokumen;
        $data->nomor = $request->nomor;
        $data->tahun = $request->tahun;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Dasar_Hukum::findOrFail($id)->get();
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Dasar_Hukum::findOrFail($id);
        
        $data->nama_peraturan = $request->namaPeraturan;
        $data->dokumen = $request->dokumen;
        $data->nomor = $request->nomor;
        $data->tahun = $request->tahun;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Dasar_Hukum::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

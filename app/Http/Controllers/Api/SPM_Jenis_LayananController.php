<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Jenis_Layanan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_Jenis_LayananController extends Controller
{
    /*
    |-------------------| 
    |                   |
    | SPM Jenis Layanan |
    |                   |
    |-------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Jenis_Layanan::with([
                                                'bidangUrusan',
                                                'indikator'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Jenis_Layanan();

        $data->nama_jenis = $request->namaJenis;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_indikator = $request->indikator;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Jenis_Layanan::with([
                                        'bidangUrusan',
                                        'indikator'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Jenis_Layanan::findOrFail($id);
        
        $data->nama_jenis = $request->namaJenis;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_indikator = $request->indikator;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Jenis_Layanan::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

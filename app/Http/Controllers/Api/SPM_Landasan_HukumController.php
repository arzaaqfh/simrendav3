<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Landasan_Hukum;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_Landasan_HukumController extends Controller
{
    /*
    |--------------------| 
    |                    |
    | SPM Landasan Hukum |
    |                    |
    |--------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Landasan_Hukum::with([
                                                'bidangUrusan',
                                                'dasarHukum'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Landasan_Hukum();

        $data->id_dasar_hukum = $request->dasarHukum;
        $data->id_urusan = $request->bidangUrusan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Landasan_Hukum::with([
                                        'bidangUrusan',
                                        'dasarHukum'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Landasan_Hukum::findOrFail($id);
        
        $data->id_dasar_hukum = $request->dasarHukum;
        $data->id_urusan = $request->bidangUrusan;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Landasan_Hukum::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

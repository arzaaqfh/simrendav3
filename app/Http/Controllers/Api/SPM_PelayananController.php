<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Pelayanan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_PelayananController extends Controller
{
    /*
    |---------------| 
    |               |
    | SPM Pelayanan |
    |               |
    |---------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Pelayanan::with([
                                                'bidangUrusan',
                                                'jenisLayanan',
                                                'tahun'
                                            ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Pelayanan();

        $data->total_dilayani = $request->totalDilayani;
        $data->total_terlayani = $request->totalTerlayani;
        $data->total_belum_terlayani = $request->totalBelumTerlayani;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Pelayanan::with([
                                        'bidangUrusan',
                                        'jenisLayanan',
                                        'tahun'
                                    ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Pelayanan::findOrFail($id);
        
        $data->total_dilayani = $request->totalDilayani;
        $data->total_terlayani = $request->totalTerlayani;
        $data->total_belum_terlayani = $request->totalBelumTerlayani;
        $data->id_bidang_urusan = $request->bidangUrusan;
        $data->id_jenis_layanan = $request->jenisLayanan;
        $data->id_tahun = $request->tahun;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Pelayanan::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

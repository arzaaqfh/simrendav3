<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\SPM_Rincian_Jenis_Layanan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SPM_Rincian_Jenis_LayananController extends Controller
{
    /*
    |---------------------------| 
    |                           |
    | SPM Rincian Jenis Layanan |
    |                           |
    |---------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = SPM_Rincian_Jenis_Layanan::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new SPM_Rincian_Jenis_Layanan();

        $data->nama_rincian = $request->namaRincian;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SPM_Rincian_Jenis_Layanan::findOrFail($id)->get();
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SPM_Rincian_Jenis_Layanan::findOrFail($id);
        
        $data->nama_rincian = $request->namaRincian;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SPM_Rincian_Jenis_Layanan::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

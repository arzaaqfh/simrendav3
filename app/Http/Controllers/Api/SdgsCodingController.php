<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Coding_90;
use App\Models\Sdgs_Indikator_Kota_90;
use App\Models\Sdgs_Target_Realisasi_Kota_90;
use App\Models\Sdgs_Log_Indikator_Kota_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Session;

class SdgsCodingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Sdgs_Coding_90::select('SDGS_Coding_90.id_coding','SDGS_Nasional.*','SDGS_IndikatorKota_90.*','Program_90.*','Kegiatan_90.*','SubKegiatan_90.*','SKPD_90.*','LogRenja_90.*','SDGS_TargetRealisasiKota_90.*','SDGS_LogIndikatorKota_90.*')
        ->rightJoin('SDGS_LogIndikatorKota_90', function ($join) use ($request) {
            $join->on('SDGS_LogIndikatorKota_90.id_log_indikatorkota', '=', 'Sdgs_Coding_90.id_log_indikatorkota')
            ->where('SDGS_LogIndikatorKota_90.isDeleted',0)
            ->where('SDGS_Coding_90.isDeleted',0);
        })
        ->join('SDGS_TargetRealisasiKota_90', function ($join) use ($request) {
            $join->on('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota', '=', 'SDGS_LogIndikatorKota_90.id_target_realisasi_kota')->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun);
        })
        ->leftJoin('SDGS_IndikatorKota_90','SDGS_IndikatorKota_90.id_indikator_kota','=','SDGS_TargetRealisasiKota_90.id_indikator_kota')
        ->leftJoin('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->leftJoin('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->leftJoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->leftJoin('LogRenja_90','LogRenja_90.id_log_renja','=','SDGS_Coding_90.id_log_renja')
        ->leftJoin('Renja_90','Renja_90.id_renja','=','LogRenja_90.id_renja')
        ->leftJoin('SubKegiatan_90','SubKegiatan_90.id_sub_kegiatan','=','LogRenja_90.id_sub_kegiatan')
        ->leftJoin('Kegiatan_90','Kegiatan_90.id_kegiatan','=','SubKegiatan_90.id_kegiatan')
        ->leftJoin('Program_90','Program_90.id_program','=','Kegiatan_90.id_program')
        ->when($request->id_skpd, function ($q) use ($request){
            return $q->whereIn('SDGS_LogIndikatorKota_90.id_skpd',$request->id_skpd);
        })
        ->when($request->id_target_realisasi, function ($q) use ($request){
            return $q->where('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota',$request->id_target_realisasi);
        })
        ->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun)
        ->orderbyRaw('CAST(SDGS_Nasional.target_nasional AS varchar(max)) asc','SDGS_IndikatorKota_90.nama_indikator asc')->get();
        
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                            $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_log_indikatorkota.'" data-original-title="Coding" class="btn btn-primary btn-sm CodingSdgs">Edit</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $data = Sdgs_Coding_90::where('id_log_indikatorkota', $request->id_log_indikatorkota)
        ->whereIn('id_log_renja',function($query) use($request){
            $query->select('id_log_renja')->from('LogRenja_90')->join('Renja_90','Renja_90.id_renja','LogRenja_90.id_renja')
            ->where('Renja_90.id_skpd',$request->id_pd)->where('Renja_90.periode_usulan',$request->tahun)->where('LogRenja_90.isPerubahan',1);
        })
        ->update(['isDeleted' => 1]);

        if($request->input_ck_listcoding != ''){
            $arr = explode(",", $request->input_ck_listcoding);
            if(count($arr) > 0){
                for($i=0;$i<count($arr);$i++){
                    Sdgs_Coding_90::updateOrCreate(
                        ['id_log_indikatorkota' => $request->id_log_indikatorkota,'id_log_renja' => $arr[$i]],
                        ['isDeleted' => 0]
                    );
                }
            }
        }

        $logIndKota = Sdgs_Log_Indikator_Kota_90::findOrFail($request->id_log_indikatorkota);
        if($logIndKota){
            $dCoding = Sdgs_Coding_90::select(DB::raw('SUM(ISNULL(LogRenja_90.apbd_kota,0)) as anggaran'),DB::raw('SUM(ISNULL(R_LogRenja_90.r_1,0)+ISNULL(R_LogRenja_90.r_2,0)) AS realisasi_anggaran_1'),DB::raw('SUM(ISNULL(R_LogRenja_90.r_3,0)+ISNULL(R_LogRenja_90.r_4,0)) AS realisasi_anggaran_2'))
                ->join('SDGS_LogIndikatorKota_90', function ($join) use ($request) {
                    $join->on('SDGS_LogIndikatorKota_90.id_log_indikatorkota', '=', 'Sdgs_Coding_90.id_log_indikatorkota')
                    ->where('SDGS_LogIndikatorKota_90.isDeleted',0)
                    ->where('SDGS_Coding_90.isDeleted',0);
                })
                ->join('SDGS_TargetRealisasiKota_90', function ($join) use ($request) {
                    $join->on('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota', '=', 'SDGS_LogIndikatorKota_90.id_target_realisasi_kota')->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun);
                })
                ->join('LogRenja_90','LogRenja_90.id_log_renja','=','SDGS_Coding_90.id_log_renja')
                ->join('R_LogRenja_90','R_LogRenja_90.id_log_renja_perubahan','=','LogRenja_90.id_log_renja')
                ->where('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota',$logIndKota->id_target_realisasi_kota)->get();
        
        }
        
        

        if(count($dCoding) > 0){
            $data = Sdgs_Target_Realisasi_Kota_90::updateOrCreate(
                ['id_target_realisasi_kota' => $logIndKota->id_target_realisasi_kota],['anggaran' => $dCoding[0]->anggaran,'realisasi_anggaran_1' => $dCoding[0]->realisasi_anggaran_1,'realisasi_anggaran_2' => $dCoding[0]->realisasi_anggaran_2]
            );
        }else{
            $data = Sdgs_Target_Realisasi_Kota_90::updateOrCreate(
                ['id_target_realisasi_kota' => $logIndKota->id_target_realisasi_kota],['anggaran' => 0,'realisasi_anggaran_1' => 0,'capaian_anggaran_1' => 0,'realisasi_anggaran_2' => 0,'capaian_anggaran_2' => 0]
            );
        }

        return response()->json(['success'=>'Data Berhasil di Simpan!']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showbyLogIndikatorKota($id)
    {
        $data = Sdgs_Target_Realisasi_Kota_90::select('SDGS_Nasional.*','SDGS_Global.*','SDGS_IndikatorKota_90.*','SKPD_90.*','SDGS_TargetRealisasiKota_90.*','SDGS_LogIndikatorKota_90.*')
        ->join('SDGS_IndikatorKota_90','SDGS_IndikatorKota_90.id_indikator_kota','=','SDGS_TargetRealisasiKota_90.id_indikator_kota')
        ->join('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->join('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->join('SDGS_Global','SDGS_Global.id_target_global','=','SDGS_Nasional.id_target_global')
        ->leftjoin('SDGS_LogIndikatorKota_90', function ($join) {
            $join->on('SDGS_LogIndikatorKota_90.id_target_realisasi_kota', '=', 'SDGS_TargetRealisasiKota_90.id_target_realisasi_kota')->where('SDGS_LogIndikatorKota_90.isDeleted',0);
        })
        ->leftjoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->where('SDGS_LogIndikatorKota_90.id_log_indikatorkota',$id)->where('SDGS_IndikatorKota_90.isDeleted',0)->where('SDGS_LogIndikatorKota_90.isDeleted',0)
        ->orderby('SDGS_Nasional.id_target_global','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    public function showbyPd(Request $request,$id)
    {
        $data = Sdgs_Coding_90::select('*')->leftJoin('LogRenja_90','LogRenja_90.id_log_renja','=','SDGS_Coding_90.id_log_renja')
        ->leftJoin('SubKegiatan_90','SubKegiatan_90.id_sub_kegiatan','=','LogRenja_90.id_sub_kegiatan')
        ->leftJoin('Kegiatan_90','Kegiatan_90.id_kegiatan','=','SubKegiatan_90.id_kegiatan')
        ->leftJoin('Program_90','Program_90.id_program','=','Kegiatan_90.id_program')
        ->leftJoin('Renja_90','Renja_90.id_renja','=','LogRenja_90.id_renja')
        ->leftJoin('SKPD_90','SKPD_90.id_skpd','=','Renja_90.id_skpd')
        ->where('SDGS_Coding_90.isDeleted',0)
        ->when($request->id_log_indikatorkota, function ($q) use ($request){
            return $q->where('SDGS_Coding_90.id_log_indikatorkota',$request->id_log_indikatorkota);
        })
        ->where('SKPD_90.id_skpd',$id)
        ->get();
        
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

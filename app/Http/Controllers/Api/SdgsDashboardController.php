<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Coding_90;
use App\Models\Sdgs_Indikator_Kota_90;
use App\Models\Sdgs_Indikator_Provinsi;
use App\Models\Sdgs_Log_Indikator_Kota_90;
use App\Models\Sdgs_Target_Realisasi_Kota_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class SdgsDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPerangkatDaerahSdgs(Request $request)
    {
        $data = DB::select('SELECT COUNT(DISTINCT(SKPD_90.nama_skpd)) AS total_skpd,COUNT(DISTINCT(CASE WHEN SDGS_TargetRealisasiKota_90.id_target_realisasi_kota IS NOT NULL THEN SKPD_90.nama_skpd END)) AS jumlah_indikator_kota,
        COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_renja IS NOT NULL THEN SKPD_90.nama_skpd END))-COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_renja IS NULL THEN SKPD_90.nama_skpd END)) AS jumlah_pemetaan,
        COUNT(DISTINCT(CASE WHEN SDGS_TargetRealisasiKota_90.realisasi_target_1 IS NOT NULL OR SDGS_TargetRealisasiKota_90.realisasi_target_2 IS NOT NULL THEN SKPD_90.nama_skpd END)) AS sudah_monev FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd 
        LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
        WHERE SKPD_90.id_skpd NOT IN (14,15,16,CASE WHEN '.$request->tahun.' <= 2021 THEN 130 ELSE 0 END) AND SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun);

        return response()->json([
            'data' => $data,
        ]);
    }

    public function getDetailPerangkatDaerahSdgs(Request $request)
    {
        $data = DB::select('SELECT SKPD_90.id_skpd,SKPD_90.nama_skpd,COUNT(DISTINCT(SDGS_TargetRealisasiKota_90.id_target_realisasi_kota)) AS jumlah_indikator_kota,COUNT(DISTINCT(CASE WHEN SDGS_TargetRealisasiKota_90.realisasi_target_1 IS NOT NULL OR SDGS_TargetRealisasiKota_90.realisasi_target_2 IS NOT NULL THEN SDGS_TargetRealisasiKota_90.id_target_realisasi_kota END)) AS jumlah_indikator_kota_termonev,
        COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_indikatorkota IS NOT NULL THEN SDGS_TargetRealisasiKota_90.id_target_realisasi_kota END)) AS jumlah_indikator_kota_terpetakan FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd 
        LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
        WHERE SKPD_90.id_skpd NOT IN (14,15,16,CASE WHEN '.$request->tahun.' <= 2021 THEN 130 ELSE 0 END)  AND SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun.'
        GROUP BY SKPD_90.id_skpd,SKPD_90.nama_skpd');

        return response()->json([
            'data' => $data,
        ]);
    }

    public function getInfoDetailPerangkatDaerahSdgs(Request $request)
    {
        $data = Sdgs_Target_Realisasi_Kota_90::select('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota','SDGS_IndikatorKota_90.nama_indikator_kota','SDGS_Coding_90.id_log_indikatorkota','SDGS_TargetRealisasiKota_90.realisasi_target_1','SDGS_TargetRealisasiKota_90.realisasi_target_2')
        ->join('SDGS_IndikatorKota_90','SDGS_IndikatorKota_90.id_indikator_kota','=','SDGS_TargetRealisasiKota_90.id_indikator_kota')
        ->join('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->join('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->join('SDGS_Global','SDGS_Global.id_target_global','=','SDGS_Nasional.id_target_global')
        ->leftjoin('SDGS_LogIndikatorKota_90', function ($join) {
            $join->on('SDGS_LogIndikatorKota_90.id_target_realisasi_kota', '=', 'SDGS_TargetRealisasiKota_90.id_target_realisasi_kota')->where('SDGS_LogIndikatorKota_90.isDeleted',0);
        })
        ->leftjoin('SDGS_Coding_90', function ($join) {
            $join->on('SDGS_Coding_90.id_log_indikatorkota', '=', 'SDGS_LogIndikatorKota_90.id_log_indikatorkota')->where('SDGS_Coding_90.isDeleted',0);
        })
        ->leftjoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->when($request->id_skpd, function ($q) use ($request){
            return $q->where('SKPD_90.id_skpd',$request->id_skpd);
        })
        ->where('SDGS_IndikatorKota_90.isDeleted',0)->where('SDGS_LogIndikatorKota_90.isDeleted',0)
        ->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun)
        ->groupby('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota','SDGS_IndikatorKota_90.nama_indikator_kota','SDGS_Coding_90.id_log_indikatorkota','SDGS_TargetRealisasiKota_90.realisasi_target_1','SDGS_TargetRealisasiKota_90.realisasi_target_2')
        ->orderby('SDGS_IndikatorKota_90.nama_indikator_kota','asc')->get();

        return response()->json([
            'data' => $data,
        ]);
    }

    public function getPemetaanSdgs(Request $request)
    {
        $data = DB::select('SELECT COUNT(DISTINCT(SDGS_Tujuan.id_sdgs)) AS jumlah_tujuan_global,
        COUNT(DISTINCT(SDGS_Global.id_target_global)) AS jumlah_tujuan_nasional,
        COUNT(DISTINCT(SDGS_Nasional.id_target_nasional)) AS jumlah_indikator_nasional,
        COUNT(DISTINCT(SDGS_TargetRealisasiKota_90.id_target_realisasi_kota)) AS jumlah_indikator_kota,
        COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_renja IS NOT NULL THEN Program_90.id_program END)) AS jumlah_program,
        COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_renja IS NOT NULL THEN Kegiatan_90.id_kegiatan END)) AS jumlah_kegiatan,
        COUNT(DISTINCT(CASE WHEN SDGS_Coding_90.id_log_renja IS NOT NULL THEN SubKegiatan_90.id_sub_kegiatan END)) AS jumlah_subkegiatan
        FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
        JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
        JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
        JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
        LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
        LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
        LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
        LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
        WHERE SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun);

        return response()->json([
            'data' => $data,
        ]);
    }

    public function getDetailPemetaanSdgs(Request $request)
    {
        $data = Sdgs_Coding_90::select('SDGS_Coding_90.id_coding','SDGS_Nasional.*','SDGS_IndikatorKota_90.*','Program_90.*','Kegiatan_90.*','SubKegiatan_90.*','SKPD_90.*','LogRenja_90.*','SDGS_TargetRealisasiKota_90.*','SDGS_LogIndikatorKota_90.*')
        ->rightJoin('SDGS_LogIndikatorKota_90', function ($join) use ($request) {
            $join->on('SDGS_LogIndikatorKota_90.id_log_indikatorkota', '=', 'Sdgs_Coding_90.id_log_indikatorkota')
            ->where('SDGS_LogIndikatorKota_90.isDeleted',0)
            ->where('SDGS_Coding_90.isDeleted',0);
        })
        ->join('SDGS_TargetRealisasiKota_90', function ($join) {
            $join->on('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota', '=', 'SDGS_LogIndikatorKota_90.id_target_realisasi_kota')->where('SDGS_TargetRealisasiKota_90.tahun',2021);
        })
        ->leftJoin('SDGS_IndikatorKota_90','SDGS_IndikatorKota_90.id_indikator_kota','=','SDGS_TargetRealisasiKota_90.id_indikator_kota')
        ->leftJoin('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->leftJoin('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->leftJoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->leftJoin('LogRenja_90','LogRenja_90.id_log_renja','=','SDGS_Coding_90.id_log_renja')
        ->leftJoin('Renja_90','Renja_90.id_renja','=','LogRenja_90.id_renja')
        ->leftJoin('SubKegiatan_90','SubKegiatan_90.id_sub_kegiatan','=','LogRenja_90.id_sub_kegiatan')
        ->leftJoin('Kegiatan_90','Kegiatan_90.id_kegiatan','=','SubKegiatan_90.id_kegiatan')
        ->leftJoin('Program_90','Program_90.id_program','=','Kegiatan_90.id_program')
        ->when($request->id_skpd, function ($q) use ($request){
            return $q->whereIn('SDGS_LogIndikatorKota_90.id_skpd',$request->id_skpd);
        })
        ->when($request->id_target_realisasi, function ($q) use ($request){
            return $q->where('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota',$request->id_target_realisasi);
        })
        ->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun)
        ->orderbyRaw('CAST(SDGS_Nasional.target_nasional AS varchar(max)) asc','SDGS_IndikatorKota_90.nama_indikator asc')->get();
        

        return response()->json([
            'data' => $data,
        ]);
    }

    public function capaianPaguIndKota(Request $request)
    {
        $data = DB::select('SELECT CAST(SDGS_Nasional.target_nasional AS varchar(max)) AS target_nasional,SDGS_TargetRealisasiKota_90.id_target_realisasi_kota,SDGS_IndikatorKota_90.nama_indikator_kota,SKPD_90.nama_skpd,SUM(ISNULL(SDGS_TargetRealisasiKota_90.anggaran,0)) AS total_anggaran,SUM(ISNULL(SDGS_TargetRealisasiKota_90.realisasi_anggaran_1,0)) AS realisasi_anggaran_sem_1,SUM(ISNULL(SDGS_TargetRealisasiKota_90.realisasi_anggaran_2,0)) AS realisasi_anggaran_sem_2 
        FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
        JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
        JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
        JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
        WHERE SKPD_90.id_skpd NOT IN (14,15,16,CASE WHEN '.$request->tahun.' <= 2021 THEN 130 ELSE 0 END) AND SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun.'
        GROUP BY CAST(SDGS_Nasional.target_nasional AS varchar(max)),SDGS_TargetRealisasiKota_90.id_target_realisasi_kota,SDGS_IndikatorKota_90.nama_indikator_kota,SKPD_90.nama_skpd');

        return response()->json([
            'data' => $data,
        ]);
    }

    public function capaianKinerjaIndKota(Request $request)
    {
        $data = DB::select('SELECT CAST(SDGS_Nasional.target_nasional AS varchar(max)) AS target_nasional,SDGS_TargetRealisasiKota_90.id_target_realisasi_kota,SDGS_IndikatorKota_90.nama_indikator_kota,SKPD_90.nama_skpd,SDGS_TargetRealisasiKota_90.target,SDGS_IndikatorKota_90.satuan,SDGS_TargetRealisasiKota_90.realisasi_target_1,SDGS_TargetRealisasiKota_90.realisasi_target_2,SDGS_TargetRealisasiKota_90.capaian_target_1,SDGS_TargetRealisasiKota_90.capaian_target_2
        FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
        JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
        JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
        JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
        WHERE SKPD_90.id_skpd NOT IN (14,15,16,CASE WHEN '.$request->tahun.' <= 2021 THEN 130 ELSE 0 END) AND SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun);

        return response()->json([
            'data' => $data,
        ]);
    }

    public function cetakRealisasiSdgs(Request $request)
    {
        $data = DB::select('SELECT SDGS_Tujuan.nama_tujuan,SDGS_Global.target_global,SDGS_Nasional.target_nasional,SDGS_IndikatorKota_90.nama_indikator_kota,SDGS_IndikatorKota_90.satuan,
        SDGS_TargetRealisasiKota_90.*,SubKegiatan_90.nama_sub_kegiatan,Kegiatan_90.nama_kegiatan,Program_90.nama_program,SKPD_90.nama_skpd FROM SDGS_TargetRealisasiKota_90
        JOIN SDGS_IndikatorKota_90 ON SDGS_IndikatorKota_90.id_indikator_kota = SDGS_TargetRealisasiKota_90.id_indikator_kota
        JOIN SDGS_IndikatorProvinsi ON SDGS_IndikatorProvinsi.id_indikator_provinsi = SDGS_IndikatorKota_90.id_indikator_provinsi
        JOIN SDGS_Nasional ON SDGS_Nasional.id_target_nasional = SDGS_IndikatorProvinsi.id_target_nasional
        JOIN SDGS_Global ON SDGS_Global.id_target_global = SDGS_Nasional.id_target_global
        JOIN SDGS_Tujuan ON SDGS_Tujuan.id_sdgs = SDGS_Global.id_sdgs
        JOIN SDGS_LogIndikatorKota_90 ON SDGS_LogIndikatorKota_90.id_target_realisasi_kota = SDGS_TargetRealisasiKota_90.id_target_realisasi_kota AND SDGS_LogIndikatorKota_90.isDeleted = 0
        JOIN SKPD_90 ON SKPD_90.id_skpd = SDGS_LogIndikatorKota_90.id_skpd
        LEFT JOIN SDGS_Coding_90 ON SDGS_Coding_90.id_log_indikatorkota = SDGS_LogIndikatorKota_90.id_log_indikatorkota AND SDGS_Coding_90.isDeleted = 0
        LEFT JOIN LogRenja_90 ON LogRenja_90.id_log_renja = SDGS_Coding_90.id_log_renja
        LEFT JOIN SubKegiatan_90 ON SubKegiatan_90.id_sub_kegiatan = LogRenja_90.id_sub_kegiatan
        LEFT JOIN Kegiatan_90 ON Kegiatan_90.id_kegiatan = SubKegiatan_90.id_kegiatan
        LEFT JOIN Program_90 ON Program_90.id_program = Kegiatan_90.id_program
        WHERE SKPD_90.id_skpd NOT IN (14,15,16,CASE WHEN '.$request->tahun.' <= 2021 THEN 130 ELSE 0 END) AND SDGS_TargetRealisasiKota_90.tahun = '.$request->tahun);

        return response()->json([
            'data' => $data,
        ]);
    }
}

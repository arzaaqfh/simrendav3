<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Indikator_Global;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;


class SdgsIndikatorGlobalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sdgs_Indikator_Global::select('*')->join('Sdgs_Tujuan','Sdgs_Global.id_sdgs','=','Sdgs_Tujuan.id_sdgs')->where('SDGS_GLobal.isDeleted',0)->orderby('urutan','asc')->get();

        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_global.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editGlobalSdgs">Edit</a>';
                            $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_global.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteGlobalSdgs">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

        //return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Sdgs_Indikator_Global();

        $data->id_sdgs = $request->id_sdgs;
        $data->target_global = $request->target_global;
        $data->isDeleted = 0;

        $data->timestamps = false;
        if($data->save())
        {
            return response()->json(['success'=>'Target Nasional Berhasil Tersimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Sdgs_Indikator_Global::select('*')->join('Sdgs_Tujuan','Sdgs_Global.id_sdgs','=','Sdgs_Tujuan.id_sdgs')
        ->where('Sdgs_Global.id_target_global',$id)->where('SDGS_GLobal.isDeleted',0)
        ->orderby('urutan','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    public function showByTujuan($id)
    {
        $data = Sdgs_Indikator_Global::select('*')->join('Sdgs_Tujuan','Sdgs_Global.id_sdgs','=','Sdgs_Tujuan.id_sdgs')
        ->where('SDGS_GLobal.isDeleted',0)->when($id != 0, function ($q) use ($id) {
            return $q->where('Sdgs_Tujuan.id_sdgs',$id);
        })
        ->orderby('urutan','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Sdgs_Indikator_Global::where('id_target_global', $id)
            ->update(['target_global' => $request->input('target_global'),'id_sdgs' => $request->input('id_sdgs')]);

        if($data)
        {
            return response()->json(['success'=>'Target Nasional Berhasil Tersimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Sdgs_Indikator_Global::where('id_target_global', $request->input('id'))
        ->update(['isDeleted' => 1]);

        if($data)
        {
            return response()->json(['success'=>'Data Berhasil di Hapus!']);
        }
    }
}

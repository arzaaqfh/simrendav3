<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Indikator_Kota_90;
use App\Models\Sdgs_Indikator_Provinsi;
use App\Models\Sdgs_Log_Indikator_Kota_90;
use App\Models\Sdgs_Target_Realisasi_Kota_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\DB;

class SdgsIndikatorKotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $data = Sdgs_Indikator_Kota_90::select(
            'SDGS_IndikatorKota_90.*',
            'SDGS_Nasional.*',
            'SKPD_90.*',
            'SDGS_TargetRealisasiKota_90.*'
            )
        ->join('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->join('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->join('SDGS_TargetRealisasiKota_90', function ($join) use ($request) {
            $join->on('SDGS_TargetRealisasiKota_90.id_indikator_kota', '=', 'SDGS_IndikatorKota_90.id_indikator_kota')->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun);
        })
        ->join('SDGS_LogIndikatorKota_90', function ($join) {
            $join->on('SDGS_LogIndikatorKota_90.id_target_realisasi_kota', '=', 'SDGS_TargetRealisasiKota_90.id_target_realisasi_kota')->where('SDGS_LogIndikatorKota_90.isDeleted',0);
        })
        ->leftjoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun)
        ->orderbyRaw('CAST(SDGS_Nasional.target_nasional AS varchar(max)) asc','SDGS_IndikatorKota_90.nama_indikator asc')->get();
        
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_realisasi_kota.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editIndkotaSdgs">Edit</a>';
                            $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_realisasi_kota.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteIndkotaSdgs">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    public function master(Request $request)
    {
        $data = Sdgs_Indikator_Kota_90::where('SDGS_IndikatorKota_90.isDeleted',0)
        ->join('Sdgs_IndikatorProvinsi','Sdgs_IndikatorProvinsi.id_indikator_provinsi','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->when($request->id_target_nasional, function ($q) use ($request) {
            return $q->where('Sdgs_IndikatorProvinsi.id_target_nasional', $request->id_target_nasional);
        })
        ->when($request->id, function ($q) use ($request) {
            return $q->where('id_indikator_kota', $request->id);
        })
        ->get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ind_provinsi = Sdgs_Indikator_Provinsi::where('id_target_nasional',$request->input('id_target_nasional'))->first();

        if(!$ind_provinsi){
            $data_prov = new Sdgs_Indikator_Provinsi();
            $data_prov->id_target_nasional = $request->input('id_target_nasional');
            $data_prov->nama_indikator_provinsi = 'Undefiend';
            $data_prov->isDeleted = 0;
            $data_prov->save();

            $id_prov = $data_prov->id_indikator_provinsi;
        }else{
            $id_prov = $ind_provinsi->id_indikator_provinsi;
        }

        if(is_numeric($request->id_indikator_kota)){
            $id_kota = $request->id_indikator_kota;
        }else{
            $data = new Sdgs_Indikator_Kota_90();
            $data->id_indikator_provinsi = $id_prov;
            $data->nama_indikator_kota = $request->id_indikator_kota;
            $data->satuan = $request->satuan;
            $data->isDeleted = 0;
            $data->save();

            $id_kota = $data->id_indikator_kota;
        }
        
        if($id_kota)
        {
            $data_target = new Sdgs_Target_Realisasi_Kota_90();
            $data_target->id_indikator_kota = $id_kota;
            $data_target->target = $request->target;
            $data_target->tahun = $request->tahun;
            $data_target->save();

            if(COUNT($request->perangkat_daerah) > 0){
                Sdgs_Log_Indikator_Kota_90::where('id_target_realisasi_kota', $data_target->id_target_realisasi_kota)
                ->update(['isDeleted' => 1]);
    
                for ($i=0; $i < COUNT($request->perangkat_daerah); $i++) { 
                    Sdgs_Log_Indikator_Kota_90::updateOrCreate(
                        ['id_target_realisasi_kota' => $data_target->id_target_realisasi_kota,'id_skpd' => $request->perangkat_daerah[$i]],
                        ['isDeleted' => 0]
                    );
                }
            }
            return response()->json(['success'=>'Indikator Kota Berhasil Tersimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $data = Sdgs_Indikator_Kota_90::select('SDGS_Tujuan.*','SDGS_Nasional.*','SDGS_Global.*','SDGS_IndikatorKota_90.*','SKPD_90.*','SDGS_TargetRealisasiKota_90.*','Jafung.nama as nama_jafung')
        ->join('SDGS_IndikatorProvinsi','SDGS_IndikatorProvinsi.id_indikator_provinsi','=','SDGS_IndikatorKota_90.id_indikator_provinsi')
        ->join('SDGS_Nasional','SDGS_Nasional.id_target_nasional','=','SDGS_IndikatorProvinsi.id_target_nasional')
        ->join('SDGS_Global','SDGS_Global.id_target_global','=','SDGS_Nasional.id_target_global')
        ->join('SDGS_Tujuan','SDGS_Tujuan.id_sdgs','=','SDGS_Global.id_sdgs')
        ->leftjoin('SDGS_TargetRealisasiKota_90', function ($join) use ($request) {
            $join->on('SDGS_TargetRealisasiKota_90.id_indikator_kota', '=', 'SDGS_IndikatorKota_90.id_indikator_kota')->where('SDGS_TargetRealisasiKota_90.tahun',$request->tahun);
        })
        ->leftjoin('SDGS_LogIndikatorKota_90', function ($join) {
            $join->on('SDGS_LogIndikatorKota_90.id_target_realisasi_kota', '=', 'SDGS_TargetRealisasiKota_90.id_target_realisasi_kota')->where('SDGS_LogIndikatorKota_90.isDeleted',0);
        })
        ->leftjoin('SKPD_90','SKPD_90.id_skpd','SDGS_LogIndikatorKota_90.id_skpd')
        ->leftJoin('Jafung','Jafung.id_jafung','SDGS_TargetRealisasiKota_90.verified_by')
        ->where('SDGS_TargetRealisasiKota_90.id_target_realisasi_kota',$id)->where('SDGS_IndikatorKota_90.isDeleted',0)
        ->orderby('SDGS_Nasional.id_target_global','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ind_provinsi = Sdgs_Indikator_Provinsi::where('id_target_nasional',$request->input('id_target_nasional'))->first();

        if(!$ind_provinsi){
            $data_prov = new Sdgs_Indikator_Provinsi();
            $data_prov->id_target_nasional = $request->input('id_target_nasional');
            $data_prov->nama_indikator_provinsi = 'Undefiend';
            $data_prov->isDeleted = 0;
            $data_prov->save();

            $id_prov = $data_prov->id_indikator_provinsi;
        }else{
            $id_prov = $ind_provinsi->id_indikator_provinsi;
        }

        if(is_numeric($request->id_indikator_kota)){
            $id_kota = $request->id_indikator_kota;
            $data = Sdgs_Indikator_Kota_90::where('id_indikator_kota', $id_kota)
            ->update(['id_indikator_provinsi' => $id_prov,'satuan' => $request->satuan]);

        }else{
            $data = new Sdgs_Indikator_Kota_90();
            $data->id_indikator_provinsi = $id_prov;
            $data->nama_indikator_kota = $request->id_indikator_kota;
            $data->satuan = $request->satuan;
            $data->isDeleted = 0;
            $data->save();

            $id_kota = $data->id_indikator_kota;

        }

        $data_target = Sdgs_Target_Realisasi_Kota_90::updateOrCreate(
            ['id_target_realisasi_kota' => $request->id_target_realisasi_kota],
            ['id_indikator_kota' => $id_kota,'target' => $request->target]
        );

        if($data_target)
        {
            if(COUNT($request->perangkat_daerah) > 0){
                Sdgs_Log_Indikator_Kota_90::where('id_target_realisasi_kota', $data_target->id_target_realisasi_kota)
                ->update(['isDeleted' => 1]);
    
                for ($i=0; $i < COUNT($request->perangkat_daerah); $i++) { 
                    Sdgs_Log_Indikator_Kota_90::updateOrCreate(
                        ['id_target_realisasi_kota' => $data_target->id_target_realisasi_kota,'id_skpd' => $request->perangkat_daerah[$i]],
                        ['isDeleted' => 0]
                    );
                }
            }
            return response()->json(['success'=>'Indikator Kota Berhasil Tersimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Sdgs_Target_Realisasi_Kota_90::where('id_target_realisasi_kota', $request->id)
        ->delete();

        if($data)
        {
            return response()->json(['success'=>'Data Berhasil di Hapus!']);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Indikator_Nasional;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class SdgsIndikatorNasionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sdgs_Indikator_Nasional::select('*')
        ->join('Sdgs_Global','Sdgs_Global.id_target_global','=','Sdgs_Nasional.id_target_global')
        ->join('Sdgs_Tujuan','Sdgs_Tujuan.id_sdgs','=','Sdgs_Global.id_sdgs')
        ->where('Sdgs_Nasional.isDeleted',0)
        ->orderby('Sdgs_Nasional.id_target_global','asc')->get();
        
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_nasional.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editNasionalSdgs">Edit</a>';
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_target_nasional.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteNasionalSdgs">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Sdgs_Indikator_Nasional();

        $data->id_target_global = $request->id_target_global;
        $data->target_nasional = $request->target_nasional;
        $data->isDeleted = 0;

        $data->timestamps = false;
        if($data->save())
        {
            return response()->json(['success'=>'Indikator Nasional Berhasil Tersimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Sdgs_Indikator_Nasional::select('*')->join('Sdgs_Global','Sdgs_Global.id_target_global','=','Sdgs_Nasional.id_target_global')
        ->where('Sdgs_Nasional.id_target_nasional',$id)->where('Sdgs_Nasional.isDeleted',0)
        ->orderby('Sdgs_Nasional.id_target_global','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    public function showByIndglobal($id)
    {
        $data = Sdgs_Indikator_Nasional::select('*')->join('Sdgs_Global','Sdgs_Global.id_target_global','=','Sdgs_Nasional.id_target_global')
        ->where('Sdgs_Global.id_target_global',$id)->where('Sdgs_Nasional.isDeleted',0)
        ->orderby('Sdgs_Nasional.id_target_global','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Sdgs_Indikator_Nasional::where('id_target_nasional', $id)
            ->update(['target_nasional' => $request->input('target_nasional'),'id_target_global' => $request->input('id_target_global')]);

        if($data)
        {
            return response()->json(['success'=>'Indikator Nasional Berhasil Tersimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Sdgs_Indikator_Nasional::where('id_target_nasional', $request->input('id'))
        ->update(['isDeleted' => 1]);

        if($data)
        {
            return response()->json(['success'=>'Data Berhasil di Hapus!']);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\Sdgs_Tujuan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class SdgsTujuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sdgs_Tujuan::where('isdeleted',0)->orderby('urutan','asc')->get();

        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                            $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_sdgs.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editTujuanSdgs">Edit</a>';
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_sdgs.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteTujuanSdgs">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Sdgs_Tujuan();

        $data->nama_tujuan = $request->nama_tujuan;
        $data->urutan = $request->urutan;
        $data->isDeleted = 0;

        $data->timestamps = false;
        if($data->save())
        {
            return response()->json(['success'=>'Tujuan Global Berhasil Tersimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Sdgs_Tujuan::where('isdeleted',0)->where('id_sdgs',$id)->orderby('urutan','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    public function showAll()
    {
        $data = Sdgs_Tujuan::where('isdeleted',0)->orderby('urutan','asc')->get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Sdgs_Tujuan::where('id_sdgs', $id)
            ->update(['nama_tujuan' => $request->input('nama_tujuan'),'urutan' => $request->input('urutan')]);

        if($data)
        {
            return response()->json(['success'=>'Tujuan Global Berhasil Tersimpan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Sdgs_Tujuan::where('id_sdgs', $request->input('id'))
            ->update(['isDeleted' => 1]);

        if($data)
        {
            return response()->json(['success'=>'Data Berhasil di Hapus!']);
        }
    }
}

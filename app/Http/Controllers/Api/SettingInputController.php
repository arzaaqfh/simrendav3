<?php

namespace App\Http\Controllers\Api;
use App\Models\SettingInput;
use App\Http\Resources\Api\API_SimrendaResource;
use Illuminate\Http\JsonResponse;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = SettingInput::get();

        return API_SimrendaResource::collection($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'triwulan' => 'required',
            'mulai' => 'required',
            'akhir' => 'required',
            'jenisInput' => 'required',
            'tahun' => 'required'
        ]);
        if($validator->fails())
        {
            return new JsonResponse(['status' => 500,'message' => $validator->errors()], 500);
        }else{
            $data = SettingInput::where('jenisInput',$request->jenisInput)->first();

            $data->triwulan = $request->triwulan;
            $data->mulai = $request->mulai;
            $data->akhir = $request->akhir;
            $data->jenisInput = $request->jenisInput;
            $data->tahun = $request->tahun;
    
            $data->timestamps = false;
            if($data->save()){
                return new API_SimrendaResource($data);
            }
        }
    }
}

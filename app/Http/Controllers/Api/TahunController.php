<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Tahun;

use Session;
use Auth;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TahunController extends Controller
{
    /*
    |-------| 
    |       |
    | Tahun |
    |       |
    |-------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Tahun::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'tahun' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => $validator->errors(),
            ]);      
        }

        $exec = Tahun::updateOrCreate(
            ['tahun' => $request->get('tahun')]
        );
        return $exec;
        // return response()->json([
        //     'status' => 200,
        //     'message' => 'Realisasi Indikator Sub Kegiatan berhasil disimpan!',
        //     'data' => $exec,
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Tahun::findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Tahun::findOrFail($id);
    
        $data->tahun = $request->tahun;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Tahun::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\TargetRealisasiProgramV2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TargetRealisasiProgramV2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $getData = TargetRealisasiProgramV2::with([
                'IndikatorProgramRPJMD.Program_90',
                'Renja.SKPD'
            ])->get();
        $data = API_SimrendaResource::collection($getData);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $getData = TargetRealisasiProgramV2::with([
            'IndikatorProgramRPJMD.Program_90',
            'Renja.SKPD'
        ])
        ->where('id_tri_program',$id)
        ->get();
        $data = API_SimrendaResource::collection($getData);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $getData = TargetRealisasiProgramV2::where('id_tri_program',$id)->first();

        $exec = null;
        $hasil = null;
        if($getData)
        {
            $getData->target = $request->target;
            $getData->target_perubahan = $request->target_perubahan;
            $getData->t_1 = $request->t_1;
            $getData->t_2 = $request->t_2;
            $getData->t_3 = $request->t_3;
            $getData->t_4 = $request->t_4;
            $getData->satuan = $request->satuan;

            $exec = $getData->save();
        }
        if($exec && $exec != null){
            $hasil = new API_SimrendaResource($getData);
        }
        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = TargetRealisasiProgramV2::findOrFail($id);
        if($data->delete()){
            return new API_SimrendaResource($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TargetRealisasiSasaran;
use App\Models\SKPD_90;
use Illuminate\Http\Request;
use App\Http\Resources\Api\API_SimrendaResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class TargetRealisasiSasaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = TargetRealisasiSasaran::with('indikatorsasaran.sasaran.misi')->get();
        return API_SimrendaResource::collection($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $hasil = null;
        $data = new TargetRealisasiSasaran();

        $data->awal = $request->awal;
        $data->t_1 = $request->t_1;
        $data->t_2 = $request->t_2;
        $data->t_3 = $request->t_3;
        $data->t_4 = $request->t_4;
        $data->t_5 = $request->t_5;
        $data->r_1 = $request->r_1;
        $data->r_2 = $request->r_2;
        $data->r_3 = $request->r_3;
        $data->r_4 = $request->r_4;
        $data->r_5 = $request->r_5;
        $data->akhir = $request->akhir;
        $data->catatan = $request->catatan;
        $data->id_periode = $request->periode;
        $data->id_indikator_tujuan = $request->indikator_tujuan;
        $data->id_rumus = $request->rumus;
        $data->id_skpd = $request->skpd;

        if($data->save())
        {
            $hasil = array(
                'kode' => 200,
                'message' => 'Tambah data BERHASIL!',
                'data' => $data
            );
        }else{
            $hasil = array(
                'kode' => 500,
                'message' => 'Tambah data GAGAL!',
                'data' => $data
            );
        }

        return $hasil;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = TargetRealisasiSasaran::where('id_tri_sasaran',$id)->with('indikatorsasaran.sasaran.misi')->get();
        return API_SimrendaResource::collection($data);
    }

    public function customshow(Request $request)
    { 
        //define validation rules
        $validator = Validator::make($request->all(), [
            'id_periode'  => 'required',
            'all' => 'required'
        ]);
        
        $hasil = NULL;
        if($validator)
        {
            // Mengambil Dari Seluruh Perangkat Daerah
            if($request->all == 1)
            {                
                // default format
                $trit = TargetRealisasiSasaran::where('id_periode',$request->id_periode)
                        ->with('indikatorsasaran','rumus','periode')
                        ->orderBy('id_skpd')
                        ->get();
                $data = null;
                foreach($trit as $tr)
                {
                    $data[] = array(
                        'periode' => $tr->periode,
                        'rumus' => $tr->rumus,
                        'indikatorsasaran' => $tr->indikatorsasaran,
                        'targetrealisasisasaran' => array(
                            'id_tri_sasaran' => $tr->id_tri_sasaran,
                            'awal' => $tr->awal,
                            't_1' => $tr->t_1,
                            't_2' => $tr->t_2,
                            't_3' => $tr->t_3,
                            't_4' => $tr->t_4,
                            't_5' => $tr->t_5,
                            't_1_perubahan' => $tr->t_1_perubahan,
                            't_2_perubahan' => $tr->t_2_perubahan,
                            't_3_perubahan' => $tr->t_3_perubahan,
                            't_4_perubahan' => $tr->t_4_perubahan,
                            't_5_perubahan' => $tr->t_5_perubahan,
                            'r_1' => $tr->r_1,
                            'r_2' => $tr->r_2,
                            'r_3' => $tr->r_3,
                            'r_4' => $tr->r_4,
                            'r_5' => $tr->r_5,
                            'akhir' => $tr->akhir,
                            'catatan' => $tr->catatan
                        )
                    );
                }
                $hasil = $data;                
            }else{
                // Mengambil Dari Salah Satu Periode
                if($request->id_periode)
                {
                    $trit = TargetRealisasiSasaran::where('id_periode',$request->id_periode)
                        ->with('indikatorsasaran','rumus','periode')
                        ->get();
                    $data = null;
                    foreach($trit as $tr)
                    {
                        $data[] = array(
                            'periode' => $tr->periode,
                            'rumus' => $tr->rumus,
                            'indikatorsasaran' => $tr->indikatorsasaran,
                            'targetrealisasisasaran' => array(
                                'id_tri_sasaran' => $tr->id_tri_sasaran,
                                'awal' => $tr->awal,
                                't_1' => $tr->t_1,
                                't_2' => $tr->t_2,
                                't_3' => $tr->t_3,
                                't_4' => $tr->t_4,
                                't_5' => $tr->t_5,
                                'r_1' => $tr->r_1,
                                'r_2' => $tr->r_2,
                                'r_3' => $tr->r_3,
                                'r_4' => $tr->r_4,
                                'r_5' => $tr->r_5,
                                'akhir' => $tr->akhir,
                                'catatan' => $tr->catatan
                            )
                        );
                    }
                    if(!$data)
                    {
						// $hasil = array(
						//     'kode' => 500,
						//     'message' => 'Data tidak tersedia!'
						// );
						return response()->json(['error_message' => 'Data Tidak Tersedia'], 422);
                    }else{
                        $hasil = $data;
                    }
                }else{
                    $hasil[] = array(
                        'kode' => 500,
                        'message' => 'Mengambil data GAGAL!'
                    );
                }
            }
        }else{
            $hasil[] = array(
                'kode' => 500,
                'message' => 'Validasi GAGAL!'
            );
        }

        return $hasil;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $id adalah id_tri_sasaran
        $data = TargetRealisasiSasaran::find($id);
        $hasil = null;

        if($data)
        {
            $cek = 0;
            if($request->r_1 != NULL)
            {
                $data->r_1 = $request->r_1;
                $cek++;
            }
            if($request->r_2 != NULL)
            {
                $data->r_2 = $request->r_2;
                $cek++;
            }
            if($request->r_3 != NULL)
            {
                $data->r_3 = $request->r_3;
                $cek++;
            }
            if($request->r_4 != NULL)
            {
                $data->r_4 = $request->r_4;
                $cek++;
            }
            if($request->r_5 != NULL)
            {
                $data->r_5 = $request->r_5;
                $cek++;
            }
            if($request->rumus != null)
            {
                $data->id_rumus = $request->rumus;
                $cek++;
            }
            if($request->catatan != null)
            {
                $data->catatan = $request->catatan;
                $cek++;
            }

            if($cek > 0)
            {
                // simpan perubahan jika memang ada yang diubah
                if($data->save())
                {
                    $hasil = array(
                        'kode' => 200,
                        'message' => 'Update data BERHASIL!',
                        'data' => $data
                    );
                }else{
                    $hasil = array(
                        'kode' => 500,
                        'message' => 'Update data GAGAL!',
                        'data' => $data
                    );
                }
            }
        }
        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = TargetRealisasiSasaran::find($id);
        $hasil = null;
        if($data)
        {
            if($data->delete())
            {
                $hasil = array(
                    'kode' => 200,
                    'message' => 'Hapus data BERHASIL!',
                    'data' => $data
                );
            }else{
                $hasil = array(
                    'kode' => 500,
                    'message' => 'Hapus data GAGAL!',
                    'data' => $data
                );
            }
        }
        return $hasil;
    }
}

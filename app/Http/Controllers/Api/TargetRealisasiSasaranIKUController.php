<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\TargetRealisasiSasaranIKU;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TargetRealisasiSasaranIKUController extends Controller
{
    /*
    |------------------------------| 
    |                              |
    | Target Realisasi IKU Sasaran |
    |                              |
    |------------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = TargetRealisasiSasaranIKU::with([
                                                    'IndikatorSasaranRPJMD',                                                    
                                                    'IndikatorSasaranRPJMD.SatuanRPJMD',
                                                    'KetercapaianRPJMD',
                                                    'KategoriRPJMD',
                                                    'Tahun'
                                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new TargetRealisasiSasaranIKU();

        $data->target_sbp = $request->targetSBP;
        $data->target_ssp = $request->targetSSP;
        $data->realisasi = $request->realisasi;
        $data->keterangan = $request->keterangan;
        $data->formula = $request->formula;
        $data->id_sumber_data = $request->sumberData;
        $data->id_tahun = $request->tahun;
        $data->id_indikator_sasaran = $request->indikatorSasaran;
        $data->id_ketercapaian = $request->ketercapaian;
        $data->id_kategori = $request->kategori;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TargetRealisasiSasaranIKU::with([
                                            'IndikatorSasaranRPJMD',                                                    
                                            'IndikatorSasaranRPJMD.SatuanRPJMD',
                                            'KetercapaianRPJMD',
                                            'KategoriRPJMD',
                                            'Tahun'
                                        ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = TargetRealisasiSasaranIKU::findOrFail($id);
        
        $data->target_sbp = $request->targetSBP;
        $data->target_ssp = $request->targetSSP;
        $data->realisasi = $request->realisasi;
        $data->keterangan = $request->keterangan;
        $data->formula = $request->formula;
        $data->id_sumber_data = $request->sumberData;
        $data->id_tahun = $request->tahun;
        $data->id_indikator_sasaran = $request->indikatorSasaran;
        $data->id_ketercapaian = $request->ketercapaian;
        $data->id_kategori = $request->kategori;
        
        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TargetRealisasiSasaranIKU::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

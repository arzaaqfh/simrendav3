<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\TujuanRPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TujuanRPJMDController extends Controller
{
    /*
    |--------------| 
    |              |
    | Tujuan RPJMD |
    |              |
    |--------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = TujuanRPJMD::with([
                                    'MisiRPJMD'
                                ])->get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new TujuanRPJMD();

        $data->tujuan = $request->tujuan;
        $data->id_misi = $request->misi;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TujuanRPJMD::with([
                                'MisiRPJMD'
                            ])->findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = TujuanRPJMD::findOrFail($id);
        
        $data->tujuan = $request->tujuan;
        $data->id_misi = $request->misi;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TujuanRPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

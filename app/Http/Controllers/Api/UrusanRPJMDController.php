<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\UrusanRPJMD;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UrusanRPJMDController extends Controller
{
    /*
    |--------------| 
    |              |
    | Urusan RPJMD |
    |              |
    |--------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = UrusanRPJMD::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new UrusanRPJMD();

        $data->nomor = $request->nomor;
        $data->nama_urusan = $request->namaUrusan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = UrusanRPJMD::findOrFail($id);
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = UrusanRPJMD::findOrFail($id);
        
        $data->nomor = $request->nomor;
        $data->nama_urusan = $request->misi;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UrusanRPJMD::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

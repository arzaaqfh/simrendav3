<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\Urusan_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Urusan_90Controller extends Controller
{
    /*
    |-----------| 
    |           |
    | Urusan 90 |
    |           |
    |-----------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Urusan_90::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new Urusan_90();

        $data->kode_urusan = $request->kodeUrusan;
        $data->nama_urusan = $request->namaUrusan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Urusan_90::findOrFail($id)->get();
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Urusan_90::findOrFail($id);
        
        $data->kode_urusan = $request->kodeUrusan;
        $data->nama_urusan = $request->namaUrusan;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Urusan_90::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;
use App\Models\UsersSKPD;
use App\Http\Resources\Api\API_SimrendaResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserSKPDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = UsersSKPD::with('user','skpd')->get();
        
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasil = null;
        // input banyak data
        $length = count($request->all()['id_user']);
        for($i=0;$i<$length;$i++)
        {
            $data = new UsersSKPD();            
            $data->id_user = $request->all()['id_user'][$i];
            $data->id_skpd = $request->all()['id_skpd'][$i];
            
            if($data->save())
            {
                $hasil[] = new JsonResponse(array(
                    'kode' => '200',
                    'message' => 'Proses tambah data SUKSES!',
                    'data' => $data
                ));
            }else{
                $hasil[] = new JsonResponse(array(
                    'kode' => '500',
                    'message' => 'Proses tambah data GAGAL!',
                    'data' => $data
                ));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = UsersSKPD::where('id',$id)->with('user','skpd')->get();
        
        return API_SimrendaResource::collection($data);
    }
    public function showByUser($id)
    {
        //
        $data = UsersSKPD::where('id_user',$id)->with('user','skpd')->get();
        
        return API_SimrendaResource::collection($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = UsersSKPD::find($id);
        $data->id_user = $request->id_user;
        $data->id_skpd = $request->id_skpd;
        
        if($data->save())
        {
            return JsonResponse(array(
                'kode' => '200',
                'message' => 'Proses tambah data SUKSES!',
                'data' => $data
            ));
        }else{
            return JsonResponse(array(
                'kode' => '500',
                'message' => 'Proses tambah data GAGAL!',
                'data' => $data
            ));
        }
    }
    public function updateByUser(Request $request)
    {
        $hasil = null;
        // hapus data lama
        $dataLama = UsersSKPD::where('id_user',$request->id_user);
        if($dataLama->get())
        {
            if($dataLama->delete())
            {
                $hasil[] = new JsonResponse(array(
                    'kode' => '200',
                    'message' => 'Proses hapus data SUKSES!',
                    'data' => $dataLama
                ));
            }else{
                $hasil[] = new JsonResponse(array(
                    'kode' => '500',
                    'message' => 'Proses hapus data GAGAL!',
                    'data' => $dataLama
                ));
            }
        }
        // masukkan data baru
        $length = count($request->id_skpd);
        for($i=0;$i<$length;$i++)
        {
            $data = new UsersSKPD();            
            $data->id_user = $request->id_user;
            $data->id_skpd = $request->all()['id_skpd'][$i];
            
            if($data->save())
            {
                $hasil[] = new JsonResponse(array(
                    'kode' => '200',
                    'message' => 'Proses tambah data SUKSES!',
                    'data' => $data
                ));
            }else{
                $hasil[] = new JsonResponse(array(
                    'kode' => '500',
                    'message' => 'Proses tambah data GAGAL!',
                    'data' => $data
                ));
            }
        }

        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UsersSKPD::find($id);
        if($data)
        {            
            if($data->delete())
            {
                return new JsonResponse(array(
                    'kode' => '200',
                    'message' => 'Proses delete data SUKSES!',
                    'data' => $data
                ));
            }else{
                return new JsonResponse(array(
                    'kode' => '500',
                    'message' => 'Proses delete data GAGAL!',
                    'data' => $data
                ));
            }
        }
    }
}

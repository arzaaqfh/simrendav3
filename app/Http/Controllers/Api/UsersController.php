<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\JsonResponse;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\Api\API_SimrendaResource;
use App\Models\User;
use App\Models\Level;
use App\Models\Jafung;
use App\Models\RolesJenisLevel;
use App\Models\UsersSKPD;
use App\Models\Roles;
use App\Models\SKPD_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::with('user_skpd.skpd')->get();
        return API_SimrendaResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = new User();

        $user->nip = $request->nip;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->id_role = $request->role;
        $user->isAktif = 1;
        $user->isDeleted = 0;
        $user->musrenbang = 0;
        $user->nama_pengguna = $request->nama_pengguna;

        if($user->save())
        {
            $dskpd = SKPD_90::get();
            $userbaru = User::orderBy('id_user','DESC')->first();

            if($request->semua_perangkat_daerah == 'on')
            {
                foreach($dskpd as $skpd)
                {
                    $updbaru = new UsersSKPD();
                    $updbaru->id_user = $userbaru->id_user;
                    $updbaru->id_skpd = $skpd->id_skpd;
                    if($updbaru->save())
                    {
                        $hasil[] = array(
                            'kode' => 200,
                            'message' => 'Tambah akses skpd SUKSES!',
                            'data' => $updbaru
                        );
                    }else{
                        $hasil[] = array(
                            'kode' => 500,
                            'message' => 'Tambah akses skpd GAGAL!',
                            'data' => $updbaru
                        );
                    }
                }

            }else if($request->perangkat_daerah){
                
                $daftarpd = null;
                if(!is_array($request->perangkat_daerah) && strlen($request->perangkat_daerah) > 0)
                {
                    $daftarpd = explode(',',$request->perangkat_daerah);
                }else{
                    $daftarpd = $request->perangkat_daerah;
                }

                foreach($daftarpd as $id_skpd)
                {
                    if($id_skpd)
                    {

                        $updbaru = new UsersSKPD();
                        $updbaru->id_user = $userbaru->id_user;
                        $updbaru->id_skpd = $id_skpd;
                        if($updbaru->save())
                        {
                            $hasil = array(
                                'kode' => 200,
                                'message' => 'Tambah akses skpd SUKSES!',
                                'data' => $updbaru
                            );
                        }else{
                            $hasil = array(
                                'kode' => 500,
                                'message' => 'Tambah akses skpd GAGAL!',
                                'data' => $updbaru
                            );
                        }
                    }
                }
            }

            $hasil[] = array(
                'kode' => 200,
                'message' => 'Tambah data user BERHASIL!',
                'data' => $user
            );
        }else{
            $hasil[] = array(
                'kode' => 500,
                'message' => 'Tambah data user GAGAL!',
                'data' => $user
            );
        }

        return $hasil;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = User::where('id_user',$id)->with('user_skpd.skpd')->get();
        
        return API_SimrendaResource::collection($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dskpd = SKPD_90::get();
        $uskpd = UsersSKPD::where('id_user',$id)->delete();
        $user = User::where('id_user',$id)->first();
        $hasil = null;

        if($user)
        {
            $user->username = $request->username;
            if($request->password != null)
            {
                $user->password = bcrypt($request->password);
            }
            $user->id_role = $request->role;
            $user->nama_pengguna = $request->nama_pengguna;
            $user->nip = $request->nip;

            if($user->save())
            {
                $hasil[] = array(
                    'kode' => 200,
                    'message' => 'Update data user SUKSES!',
                    'data' => $user
                );
            }else{
                $hasil[] = array(
                    'kode' => 500,
                    'message' => 'Update data user GAGAL!',
                    'data' => $user
                );
            }
        }
        if($request->semua_perangkat_daerah == 'on')
        {
            foreach($dskpd as $skpd)
            {
                $updbaru = new UsersSKPD();
                $updbaru->id_user = $id;
                $updbaru->id_skpd = $skpd->id_skpd;
                if($updbaru->save())
                {
                    $hasil[] = array(
                        'kode' => 200,
                        'message' => 'Update data akses skpd SUKSES!',
                        'data' => $updbaru
                    );
                }else{
                    $hasil[] = array(
                        'kode' => 500,
                        'message' => 'Update data akses skpd GAGAL!',
                        'data' => $updbaru
                    );
                }
                
            }

        }else{
            if($request->perangkat_daerah)
            {
                $daftarpd = null;
                if(!is_array($request->perangkat_daerah) && strlen($request->perangkat_daerah) > 0)
                {
                    $daftarpd = explode(',',$request->perangkat_daerah);
                }else{
                    $daftarpd = $request->perangkat_daerah;
                }

                foreach($daftarpd as $skpd)
                {
                    if($skpd)
                    {
                        $updbaru = new UsersSKPD();
                        $updbaru->id_user = $id;
                        $updbaru->id_skpd = $skpd;
                        if($updbaru->save())
                        {
                            $hasil[] = array(
                                'kode' => 200,
                                'message' => 'Update data akses skpd SUKSES!',
                                'data' => $updbaru
                            );
                        }else{
                            $hasil[] = array(
                                'kode' => 500,
                                'message' => 'Update data akses skpd GAGAL!',
                                'data' => $updbaru
                            );
                        }
                    }
                }
            }            
        }

        return $hasil;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $userskpd = UsersSKPD::where('id_user',$id)->delete();
        $user = User::where('id_user',$id)->delete();
        $hasil = null;

        if($userskpd)
        {
            $hasil[] = array(
                'kode' => 200,
                'message' => 'Delete data akses skpd SUKSES!',
                'data' => $userskpd
            );
        }else{
            $hasil[] = array(
                'kode' => 500,
                'message' => 'Delete data akses skpd GAGAL!',
                'data' => $userskpd
            );
        }
        
        if($user)
        {
            $hasil[] = array(
                'kode' => 200,
                'message' => 'Delete data user SUKSES!',
                'data' => $user
            );
        }else{
            $hasil[] = array(
                'kode' => 500,
                'message' => 'Delete data user GAGAL!',
                'data' => $user
            );
        }

        return $hasil;
    }
}

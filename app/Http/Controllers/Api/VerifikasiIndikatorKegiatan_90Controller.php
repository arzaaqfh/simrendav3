<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\Api\API_SimrendaResource;

use App\Models\VerifikasiIndikatorKegiatan_90;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerifikasiIndikatorKegiatan_90Controller extends Controller
{
    /*
    |----------------------------------| 
    |                                  |
    | Verifikasi Indikator Kegiatan 90 |
    |                                  |
    |----------------------------------|
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = VerifikasiIndikatorKegiatan_90::get();
        $data = API_SimrendaResource::collection($getData);
        
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = new VerifikasiIndikatorKegiatan_90();

        $data->status_verifikasi = $request->statusVerifikasi;
        $data->verifikator = $request->verifikator;
        $data->catatan = $request->catatan;
        $data->id_indikator_kegiatan = $request->indikatorKegiatan;

        $data->timestamps = false;        
        if($data->save())
        {
            return new API_SimrendaResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = VerifikasiIndikatorKegiatan_90::findOrFail($id)->get();
        return new API_SimrendaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = VerifikasiIndikatorKegiatan_90::findOrFail($id);
        
        $data->status_verifikasi = $request->statusVerifikasi;
        $data->verifikator = $request->verifikator;
        $data->catatan = $request->catatan;
        $data->id_indikator_kegiatan = $request->indikatorKegiatan;

        $data->timestamps = false;
        if($data->save()){
            return new PostResource($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = VerifikasiIndikatorKegiatan_90::findOrFail($id);
        if($data->delete()){
            return new PostResource($data);
        }
    }
}

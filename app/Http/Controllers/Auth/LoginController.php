<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Resources\Api\API_RENJAResource;
use App\Models\BatasWaktuInput;
use App\Models\SettingInput;
use App\Models\UsersSKPD;

use Session; 
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Api\AuthController;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    

    protected $loginPath = '/';
     
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $data['login'] = json_decode(
            app(
                'App\Http\Controllers\Api\AuthController'
            )
                ->login($request)
                ->content()
        );

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        if ($data['login']->message != 'Unauthorized') {
            if ($request->hasSession()) {
                $request->session()->put('token', $data['login']->access_token);
                $request->session()->put('auth.password_confirmed_at', time());

                //set default periode_usulan dan tahun monev
                $settingInput = SettingInput::get();
                $periode_usulan = $settingInput->first()['tahun'];
                $tahun_monev = $settingInput->first()['tahun'];
                $tahun_sdgs = 2022;
                $triwulan = $settingInput->first()['triwulan'];
                $tab_realisasi = '';
                $perubahan = 'off';
                $id_user = Auth::user()->id_user;
                $id_pd_jfp = null;
                $users = DB::table('ViewUsers')->where('id_user',$id_user)->get();
                foreach ($users as $key => $value) {
                    $request->session()->put('id_role', $value->id_role);
                    $request->session()->put('nama_roles', $value->nama_roles);
                    $array = [];
                    if($value->id_role == 6){
                        $q_pd = DB::table('SKPD_90')->where('id_bidang',$value->id_bidang)->get();
                        foreach($q_pd as $k_pd => $pd){
                            array_push($array,(int)$pd->id_skpd);
                        }
                        $request->session()->put('id_skpd', $array);
                    }else if($value->id_role == 13){
                        $q_pd = UsersSKPD::where('id_user',$id_user)->get();
                        foreach($q_pd as $k_pd => $pd){
                            array_push($array,(int)$pd->id_skpd);
                        }
                        $request->session()->put('id_skpd', $array);
                        $request->session()->put('id_jafung', $value->id_jafung);
                        $request->session()->put('pilih_id_skpd', $array[0]); //set id_skpd
                    }else if($value->id_role == 4){
                        array_push($array,(int)$value->id_skpd);
                        $request->session()->put('id_skpd', $array);
                    }else if($value->id_role == 1){                        
                        $request->session()->put('pilih_id_skpd', 110); //set id_skpd
                    }
                    
                    $batasWaktuInput = array(
                        "mulai" => SettingInput::where('jenisInput','renja')->first()['mulai'],
                        "akhir" => SettingInput::where('jenisInput','renja')->first()['akhir']
                    );
                    $request->session()->put('tab_renja', 'subkeg'); //set tab input renja yang dibuka = sub kegiatan
                    $request->session()->put('bwi', $batasWaktuInput); //set batas waktu input
                    $request->session()->put('id_bidang', $value->id_bidang);
                    $request->session()->put('periode_usulan', $periode_usulan);
                    $request->session()->put('tahun_monev', $tahun_monev);
                    $request->session()->put('tahun_sdgs', $tahun_sdgs);
                    $request->session()->put('triwulan', $triwulan);
                    $request->session()->put('perubahan', $perubahan); //set input monev untuk perubahan
                }
            }
            return $this->sendLoginResponse($request);
        }else {
            $this->incrementLoginAttempts($request);
            return $this->sendFailedLoginResponse($request);
        }
    }

    use AuthenticatesUsers;

    // By default, Laravel uses the email field for authentication.
    // If you would like to customize this, you may define a username method on your LoginController:
    public function username()
    {
        return 'username';
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/home');
        }
    }

    public function redirectPath()
    {        
        return '/home';        
    } 
}

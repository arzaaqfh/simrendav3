<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],            
            'nip' => ['required', 'string', 'max:25'],
            'username' => ['required', 'string', 'max:50'],
            'id_role' => ['required', 'integer'],
            'nama_pengguna' => ['required', 'string', 'max:50'],
            'level_user' => ['required', 'integer'],
            'musrenbang' => ['required', 'integer']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),            
            'nip' => $data['nip'],
            'username' => $data['username'],
            'id_role' => $data['id_role'],
            'nama_pengguna' => $data['nama_pengguna'],
            'level_user' => $data['level_user'],
            'musrenbang' => $data['musrenbang']
        ]);
    }

    public function showRegistrationForm()
    {
        $roles = Roles::all();
        return view("auth.register", compact("roles"));
    }
}

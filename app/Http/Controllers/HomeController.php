<?php

namespace App\Http\Controllers;

use App\Models\Tahun;
use App\Models\BatasWaktuInput;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->id_role == 13){
            return redirect()->action([Sdgs_Controller::class,'dashboardrekap']);
        }else if(Auth::user()->id_role == 1){
            return redirect()->action([MonevRENJA_Controller::class, 'settingInput']);
        }else{
            return redirect()->action([Sdgs_Controller::class, 'index']);
        }
    }

    public function session(Request $request)
    {
        $tahun = Tahun::orderBy('id_tahun','DESC')->first()->get();
        $this->middleware('auth');

        if($request->input('periode_usulan')){
            $request->session()->put('periode_usulan', $request->input('periode_usulan'));
        }else if($request->input('tahun_monev')){
            $request->session()->put('tahun_monev', $request->input('tahun_monev'));
            $request->session()->put('pilih_id_skpd', $request->input('pilih_id_skpd'));
        }else if($request->input('tahun_spm')){
            $tahun = Tahun::where('id_tahun',$request->input('tahun_spm'))->get();
            $request->session()->put('tahun_spm', $tahun[0]->tahun);
        }else if($request->input('tahun_sdgs')){
            $request->session()->put('tahun_sdgs', $request->input('tahun_sdgs'));
        }else if($request->input('pilih_id_skpd'))
        {
            $request->session()->put('pilih_id_skpd', $request->input('pilih_id_skpd'));
        }
    }
}

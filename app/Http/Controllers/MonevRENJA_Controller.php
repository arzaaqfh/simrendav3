<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Collection;
// use Excel;
use App\Exports\CetakFormatRKPD;
use App\Http\Resources\Api\API_SimrendaResource;
use Illuminate\Support\Facades\Auth;
use \stdClass;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Exports\CetakRenja;
use App\Exports\CetakRenjaAFSubKeg;
use App\Exports\CetakRenjaAFBdgUrs;
use App\Exports\ExportPreview;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportMonevMurni;
use App\Imports\ImportMonevPerubahan;
use App\Imports\ImportPreview;
use App\Models\UsersSKPD;
use Illuminate\Support\Facades\Input;

use App\Models\Urusan_90;
use App\Models\Tahun;
use App\Models\SKPD_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\Kegiatan_90;
use App\Models\Program_90;
use App\Models\ProgramRPJMD;
use App\Models\BidangUrusan_90;
use App\Models\R_IndikatorKegiatan_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\R_IndikatorSubKegiatan_90;
use App\Models\FaktorPendorongKegiatan;
use App\Models\FaktorPenghambatKegiatan;
use App\Models\TindakLanjutKegiatan;
use App\Models\FaktorPendorongSubKegiatan;
use App\Models\FaktorPenghambatSubKegiatan;
use App\Models\TindakLanjutSubKegiatan;
use App\Models\AnalisatorKegiatan;
use App\Models\AnalisatorSubKegiatan;
use App\Models\User;
use App\Models\Jafung;
use App\Models\Jafung_SektorPD;
use App\Models\CascadingKegiatan_90;
use App\Models\CascadingSubKegiatan_90;
use App\Models\VerifikasiIndikatorKegiatan_90;
use App\Models\VerifikasiIndikatorSubKegiatan_90;
use App\Models\BatasWaktuInput;
use App\Models\SettingInput;
use App\Models\Roles;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
// use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

class MonevRENJA_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    /*
        Preview File
    */
    public function previewFile(Request $request)
    {
        // Validate the uploaded file
        $request->validate([
            'berkasPreview' => 'required|mimes:xlsx,xls',
        ]);
 
        // Get the uploaded file
        $file = $request->file('berkasPreview');
 
        // Process the Excel file
        ini_set('max_execution_time', 1200);
        $data = Excel::import(new ImportPreview, $file);
        $hasil = 0;
        if($data)
        {
            $hasil = 1;
        }
        return $hasil;
    }
    /*
        Download Preview File
    */
    public function downloadPreviewFile()
    {
        return Excel::download(new ExportPreview, 'PreviewMonev.xlsx');
    }
    /**
     * Display a listing of the resource.
     *
     * return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Session::get('id_role') != 1){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        // $batasWaktuInput = BatasWaktuInput::where('jenisInput','renja')->get();
        // Session::put('triwulan',1); //mengubah session triwulan untuk kegiatan input realisasi
        // dd(Session::get('bwi')['mulai']);
        // die;
        return view('monev/renja/dashboard',compact('id_pd'));
    }
    
    /*----------------------------------------- 
        Ke Halaman Input Batas Waktu Input
    -----------------------------------------*/
    public function batasWaktu()
    {
        return view('monev/renja/batasWaktu');
    }

    /*----------------------------------------- 
        Ke Halaman Setting Input
    -----------------------------------------*/
    public function settingInput()
    {
        $settingInput = SettingInput::first();
        return view('monev/renja/settingInput', compact('settingInput'));
    }

    /*----------------------------------------- 
        Ke Halaman Setting Jafung
    -----------------------------------------*/
    public function settingUser()
    {
        $user = User::with('user_skpd.skpd')->orderBy('id_role')->get();
        return view('monev/renja/settingUser', compact('user'));
    }

    public function settingUserCreate()
    {
        $roles = Roles::get();
        $skpds = SKPD_90::get();
        return view('monev/renja/settingUserCreate', compact('roles','skpds'));
    }
    
    public function settingUserDelete($id)
    {
        $userskpd = UsersSKPD::where('id_user',$id)->delete();
        $user = User::where('id_user',$id)->delete();

        return redirect('monev/renja/settinguser');
    }

    public function settingUserEdit($id_user)
    {
        $user = User::where('id_user',$id_user)->with('user_skpd.skpd')->orderBy('id_role')->first();
        $dskpd = SKPD_90::get();
        $roles = Roles::get();

        return view('monev/renja/edituserakses',compact('user','dskpd','roles'));
    }
    public function settingUserAdd(Request $request)
    {
        $user = new User();

        $user->nip = $request->nip;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->id_role = $request->role;
        $user->isAktif = 1;
        $user->isDeleted = 0;
        $user->musrenbang = 0;
        $user->nama_pengguna = $request->nama_pengguna;

        if($user->save())
        {
            $dskpd = SKPD_90::get();
            $userbaru = User::orderBy('id_user','DESC')->first();

            if($request->semua_perangkat_daerah == 'on')
            {
                foreach($dskpd as $skpd)
                {
                    $updbaru = new UsersSKPD();
                    $updbaru->id_user = $userbaru->id_user;
                    $updbaru->id_skpd = $skpd->id_skpd;
                    $updbaru->save();
                }

            }else if($request->perangkat_daerah){
                foreach($request->perangkat_daerah as $id_skpd)
                {
                    if($id_skpd)
                    {
                        $updbaru = new UsersSKPD();
                        $updbaru->id_user = $userbaru->id_user;
                        $updbaru->id_skpd = $id_skpd;
                        $updbaru->save();
                    }
                }
            }

            $hasil[] = array(
                'kode' => 200,
                'message' => 'Tambah data BERHASIL!',
                'data' => $user
            );
        }else{
            $hasil[] = array(
                'kode' => 500,
                'message' => 'Tambah data GAGAL!',
                'data' => $user
            );
        }

        return redirect('monev/renja/settinguser');
    }
    /*----------------------------------------- 
        Ke Halaman Realisasi RENJA
    -----------------------------------------*/
    public function realisasiRenja(Request $request)
    {  
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = '';
        }else{
            $id_skpd = $request->session()->get('pilih_id_skpd');
        }

        // Membuat request baru untuk mengambil data sub kegiatan, kegiatan, program beserta indikatornya
        $newRequest = new Request();        
        $newRequest->merge([
            'token'     => $request->session()->get('token'),
            'triwulan'  => Session::get('triwulan'),
            // 'triwulan'  => 3,
            'tahun'     => $request->session()->get('tahun_monev'),
            'opd'       => $id_skpd,
            'subkeg'    => '',
            'format'    => ''
        ]);

        /*--------------------------------------------------------------
            MENGAMBIL DATA REALISASI SUB KEGIATAN
        --------------------------------------------------------------*/
        // Ambil data sub kegiatan
        $data = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getSubKegiatan($newRequest)
                    ->toJson()
                );
                // echo '<pre>';
                // print_r($newRequest);
                // print_r($data);
                // die;
        // Ambil data indikator sub kegiatan
        $data2 = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getIndikatorSubKegiatan($newRequest)
                    ->toJson()
                );
        // Menampung data indikator dengan id_sub_kegiatan sebagai index array nya
        $data2Join = null;
        $statSubKeg = null; //status input indikator sub kegiatan: 0 kosong, 1 on progres, 2 tuntas
        foreach($data2 as $val)
        {
            $data2Join[$val->id_sub_kegiatan] = $val->indikator;
            $cekInput[1] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 1
                            {
                                if(!is_null($var->realisasiindikatorsubkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_update = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorsubkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorsubkeg->t_1))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInput[2] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 2
                            {
                                if(!is_null($var->realisasiindikatorsubkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_update = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorsubkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorsubkeg->t_2))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInput[3] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 3
                            {
                                if(!is_null($var->realisasiindikatorsubkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_update = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorsubkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorsubkeg->t_3))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInput[4] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 4
                            {
                                if(!is_null($var->realisasiindikatorsubkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_update = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorsubkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorsubkeg->t_4))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            for($i=1; $i<=4; $i++)
            {
                if(count($cekInput[$i]) > 0)
                {
                    if(count($cekInput[$i]) == count($val->indikator)) //input tuntas
                    {
                        $statSubKeg[$i][$val->id_sub_kegiatan] = 2;
                    }else if(count($cekInput[$i]) < count($val->indikator)) //input on progress
                    {
                        $statSubKeg[$i][$val->id_sub_kegiatan] = 1;
                    }else{ //data kosong
                        $statSubKeg[$i][$val->id_sub_kegiatan] = 0;
                    }
                }else{ //data kosong         
                    $statSubKeg[$i][$val->id_sub_kegiatan] = 0;
                }
            }
        }
        // dd($statSubKeg);
        $subKeg = $data;
        $indSubKeg = $data2Join;
        /*--------------------------------------------------------------*/

        /*--------------------------------------------------------------
            MENGAMBIL DATA REALISASI KEGIATAN
        --------------------------------------------------------------*/
        // Ambil data kegiatan
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getKegiatan($newRequest)
            ->toJson()
        );
        // Ambil data indikator kegiatan
        $data2 = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getIndikatorKegiatan($newRequest)
                    ->toJson()
                );
        // Menampung data indikator dengan id_kegiatan sebagai index array nya
        $data2Join = null;
        $statKeg = null; //status input indikator sub kegiatan: 0 kosong, 1 on progres, 2 tuntas
        foreach($data2 as $val)
        {
            $data2Join[$val->id_kegiatan] = $val->indikator;
            $cekInputKeg[1] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 1
                            {
                                if(!is_null($var->realisasiindikatorkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_updatekeg = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorkeg->t_1))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInputKeg[2] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 2
                            {
                                if(!is_null($var->realisasiindikatorkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_updatekeg = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorkeg->t_2))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInputKeg[3] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 3
                            {
                                if(!is_null($var->realisasiindikatorkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_updatekeg = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorkeg->t_3))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            $cekInputKeg[4] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 4
                            {
                                if(!is_null($var->realisasiindikatorkeg)){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_updatekeg = date("Y-m-d H:i:s",strtotime($var->realisasiindikatorkeg->updated_at));
                                    if(!is_null($var->realisasiindikatorkeg->t_4))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );
            for($i=1;$i<=4;$i++)
            {
                if(count($cekInputKeg[$i]) > 0)
                {
                    if(count($cekInputKeg[$i]) == count($val->indikator)) //input tuntas
                    {
                        $statKeg[$i][$val->id_kegiatan] = 2;
                    }else if(count($cekInputKeg[$i]) < count($val->indikator)) //input on progress
                    {
                        $statKeg[$i][$val->id_kegiatan] = 1;
                    }else{ //data kosong
                        $statKeg[$i][$val->id_kegiatan] = 0;
                    }
                }else{ //data kosong         
                    $statKeg[$i][$val->id_kegiatan] = 0;
                }
            }
        }
        // dd($statKeg);
        $keg = $data;
        $indKeg = $data2Join;
        /*--------------------------------------------------------------*/

        /*--------------------------------------------------------------
            MENGAMBIL DATA REALISASI PROGRAM
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getProgram($newRequest)
            ->toJson()
        );
        // Ambil data indikator program
        $data2 = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getIndikatorProgram($newRequest)
                    ->toJson()
                );
        // Menampung data indikator dengan id_program sebagai index array nya
        $data2Join = null;
        $statProg = null; //status input indikator program: 0 kosong, 1 on progres, 2 tuntas
        foreach($data2 as $val)
        {
            $data2Join[$val->id_program] = $val->indikator;
            $cekInputProg[1] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 1
                            {
                                if(!is_null($var->target_realisasi_i_k_p) && count($var->target_realisasi_i_k_p)>0){
                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                    $di_updateprog = date("Y-m-d H:i:s",strtotime($var->target_realisasi_i_k_p[count($var->target_realisasi_i_k_p)-1]->updated_at));
                                    if(array_key_exists(0,$var->target_realisasi_i_k_p) && !is_null($var->target_realisasi_i_k_p[0]->realisasi))
                                    {
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }else{
                                    return 0;
                                }
                            }
                        );            
            $cekInputProg[2] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 2
                        {
                            if(!is_null($var->target_realisasi_i_k_p) && count($var->target_realisasi_i_k_p)>0){
                                $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                $di_updateprog = date("Y-m-d H:i:s",strtotime($var->target_realisasi_i_k_p[count($var->target_realisasi_i_k_p)-1]->updated_at));
                                if(array_key_exists(1,$var->target_realisasi_i_k_p) && !is_null($var->target_realisasi_i_k_p[1]->realisasi))
                                {
                                    return 1;
                                }else{
                                    return 0;
                                }
                            }else{
                                return 0;
                            }
                        }
                    );            
            $cekInputProg[3] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 3
                    {
                        if(!is_null($var->target_realisasi_i_k_p) && count($var->target_realisasi_i_k_p)>0){
                            $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                            $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                            $di_updateprog = date("Y-m-d H:i:s",strtotime($var->target_realisasi_i_k_p[count($var->target_realisasi_i_k_p)-1]->updated_at));
                            if(array_key_exists(2,$var->target_realisasi_i_k_p) && !is_null($var->target_realisasi_i_k_p[2]->realisasi))
                            {
                                return 1;
                            }else{
                                return 0;
                            }
                        }else{
                            return 0;
                        }
                    }
                );
            $cekInputProg[4] = array_filter($val->indikator,function ($var) //cek yang sudah diinput triwulan 4
                {
                    if(!is_null($var->target_realisasi_i_k_p) && count($var->target_realisasi_i_k_p)>0){
                        $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                        $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                        $di_updateprog = date("Y-m-d H:i:s",strtotime($var->target_realisasi_i_k_p[count($var->target_realisasi_i_k_p)-1]->updated_at));
                        if(array_key_exists(3,$var->target_realisasi_i_k_p) && !is_null($var->target_realisasi_i_k_p[3]->realisasi))
                        {
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        return 0;
                    }
                }
            );
            for($i=1;$i<=4;$i++)
            {
                if(count($cekInputProg[$i]) > 0)
                {
                    if(count($cekInputProg[$i]) == count($val->indikator)) //input tuntas
                    {
                        $statProg[$i][$val->id_program] = 2;
                    }else if(count($cekInputProg[$i]) < count($val->indikator)) //input on progress
                    {
                        $statProg[$i][$val->id_program] = 1;
                    }else{ //data kosong
                        $statProg[$i][$val->id_program] = 0;
                    }
                }else{ //data kosong         
                    $statProg[$i][$val->id_program] = 0;
                }
            }
        }
        $prog = $data;
        $indProg = $data2Join;
        // dd($statProg);
        /*--------------------------------------------------------------*/

        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER BIDANG URUSAN
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisFaktor($newRequest)
            ->toJson()
        );
        $anlFkt = $data;
        /*--------------------------------------------------------------*/

        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER SUB KEGIATAN
        --------------------------------------------------------------*/
        // Ambil data analisis faktor sub kegiatan        
        $param_tw1 = new Request();        
        $param_tw1->merge(['token'=>$newRequest->get('token'), 'triwulan'=>1, 'tahun'=>$newRequest->get('tahun'), 'opd'=>$newRequest->get('opd'), 'subkeg'=>$newRequest->get('subkeg')]);
        $param_tw2 = new Request();        
        $param_tw2->merge(['token'=>$newRequest->get('token'), 'triwulan'=>2, 'tahun'=>$newRequest->get('tahun'), 'opd'=>$newRequest->get('opd'), 'subkeg'=>$newRequest->get('subkeg')]);
        $param_tw3 = new Request();        
        $param_tw3->merge(['token'=>$newRequest->get('token'), 'triwulan'=>3, 'tahun'=>$newRequest->get('tahun'), 'opd'=>$newRequest->get('opd'), 'subkeg'=>$newRequest->get('subkeg')]);
        $param_tw4 = new Request();        
        $param_tw4->merge(['token'=>$newRequest->get('token'), 'triwulan'=>4, 'tahun'=>$newRequest->get('tahun'), 'opd'=>$newRequest->get('opd'), 'subkeg'=>$newRequest->get('subkeg')]);
        
        $data_tw1 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($param_tw1)
            ->toJson()
        );
        $data_tw2 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($param_tw2)
            ->toJson()
        );
        $data_tw3 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($param_tw3)
            ->toJson()
        );
        $data_tw4 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($param_tw4)
            ->toJson()
        );
        $anlSKG = null;
        for($i=0;$i<count($data_tw1);$i++)
        {
            $anlTW1 = null;
            $anlTW2 = null;
            $anlTW3 = null;
            $anlTW4 = null;
            if(array_key_exists($i,$data_tw1) )
            {
                $anlTW1 = $data_tw1[$i];
            }
            if(array_key_exists($i,$data_tw2) )
            {
                $anlTW2 = $data_tw2[$i];
            }
            if(array_key_exists($i,$data_tw3) )
            {
                $anlTW3 = $data_tw3[$i];
            }
            if(array_key_exists($i,$data_tw4) )
            {
                $anlTW4 = $data_tw4[$i];
            }
            $anlSKG[1][$data_tw1[$i]->id_sub_kegiatan] = $anlTW1;
            $anlSKG[2][$data_tw1[$i]->id_sub_kegiatan] = $anlTW2;
            $anlSKG[3][$data_tw1[$i]->id_sub_kegiatan] = $anlTW3;
            $anlSKG[4][$data_tw1[$i]->id_sub_kegiatan] = $anlTW4;
        }
        // dd($anlSKG);
        $statAnlSKG = null; //status input indikator program: 0 kosong, 1 on progres, 2 tuntas
        for($i=0;$i<count($data_tw1);$i++)
        {
            if(!empty($data_tw1) && array_key_exists($i,$data_tw1))
            {
                $cekInputAnlSKG[1]['fp'] = array_filter($data_tw1[$i]->analisisfp,function ($var) //cek faktor pendorong yang sudah diinput tw1
                                {
                                    if(!empty($var)){
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }
                            );
                $cekInputAnlSKG[1]['fph'] = array_filter($data_tw1[$i]->analisisfph,function ($var) //cek faktor penghambat yang sudah diinput tw1
                                {
                                    if(!empty($var)){
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }
                            );
            }
            if(!empty($data_tw2) && array_key_exists($i,$data_tw2))
            {
                $cekInputAnlSKG[2]['fp'] = array_filter($data_tw2[$i]->analisisfp,function ($var) //cek faktor pendorong yang sudah diinput tw2
                            {
                                if(!empty($var)){
                                    return 1;
                                }else{
                                    return 0;
                                }
                            }
                        );
                $cekInputAnlSKG[2]['fph'] = array_filter($data_tw2[$i]->analisisfph,function ($var) //cek faktor penghambat yang sudah diinput tw2
                            {
                                if(!empty($var)){
                                    return 1;
                                }else{
                                    return 0;
                                }
                            }
                        );
            }
            if(!empty($data_tw3) && array_key_exists($i,$data_tw3))
            {
                $cekInputAnlSKG[3]['fp'] = array_filter($data_tw3[$i]->analisisfp,function ($var) //cek faktor pendorong yang sudah diinput tw3
                                    {
                                        if(!empty($var)){
                                            return 1;
                                        }else{
                                            return 0;
                                        }
                                    }
                                );
                $cekInputAnlSKG[3]['fph'] = array_filter($data_tw3[$i]->analisisfph,function ($var) //cek faktor penghambat yang sudah diinput tw3
                                {
                                    if(!empty($var)){
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }
                            );
            }
            if(!empty($data_tw4) && array_key_exists($i,$data_tw4))
            {
                $cekInputAnlSKG[4]['fp'] = array_filter($data_tw4[$i]->analisisfp,function ($var) //cek faktor pendorong yang sudah diinput tw4
                                    {
                                        if(!empty($var)){
                                            return 1;
                                        }else{
                                            return 0;
                                        }
                                    }
                                );
                $cekInputAnlSKG[4]['fph'] = array_filter($data_tw4[$i]->analisisfph,function ($var) //cek faktor penghambat yang sudah diinput tw4
                                {
                                    if(!empty($var)){
                                        return 1;
                                    }else{
                                        return 0;
                                    }
                                }
                            );
            }
            if(array_key_exists(1, $cekInputAnlSKG))
            {
                if(count($cekInputAnlSKG[1]['fp']) > 0 || count($cekInputAnlSKG[1]['fph']) > 0 )
                {
                    $statAnlSKG[1][$data_tw1[$i]->id_sub_kegiatan] = 2;
                }else{ //data kosong         
                    $statAnlSKG[1][$data_tw1[$i]->id_sub_kegiatan] = 0;
                }
            }else{
                $statAnlSKG[1][$data_tw1[$i]->id_sub_kegiatan] = 0;
            }
            if(array_key_exists(2, $cekInputAnlSKG))
            {
                if(count($cekInputAnlSKG[2]['fp']) > 0 || count($cekInputAnlSKG[2]['fph']) > 0 )
                {
                    $statAnlSKG[2][$data_tw1[$i]->id_sub_kegiatan] = 2;
                }else{ //data kosong         
                    $statAnlSKG[2][$data_tw1[$i]->id_sub_kegiatan] = 0;
                }
            }else{
                $statAnlSKG[2][$data_tw1[$i]->id_sub_kegiatan] = 0;
            }
            if(array_key_exists(3, $cekInputAnlSKG))
            {
                if(count($cekInputAnlSKG[3]['fp']) > 0 || count($cekInputAnlSKG[3]['fph']) > 0 )
                {
                    $statAnlSKG[3][$data_tw1[$i]->id_sub_kegiatan] = 2;
                }else{ //data kosong         
                    $statAnlSKG[3][$data_tw1[$i]->id_sub_kegiatan] = 0;
                }
            }else{
                $statAnlSKG[3][$data_tw1[$i]->id_sub_kegiatan] = 0;
            }
            if(array_key_exists(4, $cekInputAnlSKG))
            {
                if(count($cekInputAnlSKG[4]['fp']) > 0 || count($cekInputAnlSKG[4]['fph']) > 0 )
                {
                    $statAnlSKG[4][$data_tw1[$i]->id_sub_kegiatan] = 2;
                }else{ //data kosong         
                    $statAnlSKG[4][$data_tw1[$i]->id_sub_kegiatan] = 0;
                }
            }else{
                $statAnlSKG[4][$data_tw1[$i]->id_sub_kegiatan] = 0;
            }            
        }
        // dd($data_tw1,$data_tw2,$data_tw3,$data_tw4,$statAnlSKG);
        /*--------------------------------------------------------------*/
        //menampung hasil cek seluruh input realisasi indikator sub kegiatan, kegiatan dan program
        // 0 belum seluruhnya diisi, 1 sudah seluruhnya diisi
        $cekInputAll = array( 
            'subKeg'    => 0, 
            'keg'       => 0,
            'prog'      => 0
        );
        if(!is_null($statSubKeg))
        {
            $cekAllSubKeg = array_filter($statSubKeg, function ($var){ // cek sub kegiatan yang belum diisi
                if($var == 0 || $var == 1)
                {
                    return 1;
                }else{
                    return 0;
                }
            });
            $cekInputAll['subKeg'] = count($cekAllSubKeg) == 0 ? 1:0;
        }
        if(!is_null($statKeg))
        {
            $cekAllKeg = array_filter($statKeg, function ($var){ // cek kegiatan yang belum diisi
                if($var == 0 || $var == 1)
                {
                    return 1;
                }else{
                    return 0;
                }
            });
            $cekInputAll['keg'] = count($cekAllKeg) == 0 ? 1:0;
        }
        if(!is_null($statProg))
        {
            $cekAllProg = array_filter($statProg, function ($var){ // cek program yang belum diisi
                if($var == 0 || $var == 1)
                {
                    return 1;
                }else{
                    return 0;
                }
            });
            $cekInputAll['prog'] = count($cekAllProg) == 0 ? 1:0;
        }
        // dd($cekInputAll);
        // dd($anlSKG[2]);
        $rumus = json_decode(
            app(
                'App\Http\Controllers\Api\RumusController'
            )
            ->indexPositif()
            ->toJson()
        );
        return view('monev/renja/realisasi',[
            'subKeg'    => $subKeg,
            'indSubKeg' => $indSubKeg,
            'statSubKeg'=> $statSubKeg, 
            'keg'       => $keg,
            'indKeg'    => $indKeg, 
            'statKeg'   => $statKeg, 
            'prog'      => $prog,
            'indProg'   => $indProg,
            'statProg'  => $statProg, 
            'anlFkt'    => $anlFkt,
            'anlSKG'    => $anlSKG,
            'statAnlSKG'=> $statAnlSKG,
            'statAll'   => $cekInputAll,
            'token'     => $request->session()->get('token'),
            'rumus'     => $rumus
        ]);
    }

    /*----------------------------------------- 
        Menyimpan Realisasi Indikator Sub Kegiatan
    -----------------------------------------*/
    public function inputRIndSubKeg(Request $request)
    {
        $newRequest = new Request();
        $newRequest2 = new Request();
        $hasil = null;
        $inputR = null;
        // simpan realisasi indikator sub kegiatan
        for($i=0;$i<count($request->id_indikator_subkegiatan);$i++)
        {      
            // set nilai realisasi sesuai triwulan
            if(Session::get('triwulan') == 1)
            {
                $inputR = $request->t_1[$i];
            }else if(Session::get('triwulan') == 2)
            {
                $inputR = $request->t_2[$i];
            }else if(Session::get('triwulan') == 3)
            {
                $inputR = $request->t_3[$i];
            }else if(Session::get('triwulan') == 4)
            {
                $inputR = $request->t_4[$i];
            }

            if(is_null($inputR))
            {
                $realisasi = 0;
            }else{
                $realisasi = $inputR;
            }
            $rumus = 4; //default rumusnya: -
            if(!is_null($request->rumus[$i]))
            {
                $rumus = $request->rumus[$i];
            }
            $newRequest->merge([
                'token'                     => $request->session()->get('token'),
                'id_indikator_subkegiatan'  => $request->id_indikator_subkegiatan[$i],
                'triwulan'                  => Session::get('triwulan'),
                'realisasi'                 => $realisasi,
                'rumus'                     => $rumus
            ]);
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->saveinputrealindsubkeg($newRequest)
                ->content()
            );

            // jika admin yang masuk atau sedang input monev perubahan
            if(Auth::user()->id_role == 1)
            {
                // simpan realisasi triwulan 1
                if(!is_null($request->t_1[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                     => $request->session()->get('token'),
                        'id_indikator_subkegiatan'  => $request->id_indikator_subkegiatan[$i],
                        'triwulan'                  => 1,
                        'realisasi'                 => $request->t_1[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindsubkeg($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 2
                if(!is_null($request->t_2[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                     => $request->session()->get('token'),
                        'id_indikator_subkegiatan'  => $request->id_indikator_subkegiatan[$i],
                        'triwulan'                  => 2,
                        'realisasi'                 => $request->t_2[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindsubkeg($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 3
                if(!is_null($request->t_3[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                     => $request->session()->get('token'),
                        'id_indikator_subkegiatan'  => $request->id_indikator_subkegiatan[$i],
                        'triwulan'                  => 3,
                        'realisasi'                 => $request->t_3[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindsubkeg($newRequest)
                        ->content()
                    );
                }
                
                // simpan realisasi triwulan 4
                if(!is_null($request->t_4[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                     => $request->session()->get('token'),
                        'id_indikator_subkegiatan'  => $request->id_indikator_subkegiatan[$i],
                        'triwulan'                  => 4,
                        'realisasi'                 => $request->t_4[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindsubkeg($newRequest)
                        ->content()
                    );
                    
                }
            }
            
            $hasil[] = $data;
        }

        return $hasil;
    }

    /*----------------------------------------- 
        Mmenyimpan Realisasi Indikator Kegiatan
    -----------------------------------------*/
    public function inputRIndKeg(Request $request)
    {
        $newRequest = new Request();
        $newRequest2 = new Request();
        $hasil = null;
        $inputR = null;
       
        for($i=0;$i<count($request->id_indikator_kegiatan);$i++)
        { // set nilai realisasi sesuai triwulan
            if(Session::get('triwulan') == 1)
            {
                $inputR = $request->t_1[$i];
            }else if(Session::get('triwulan') == 2)
            {
                $inputR = $request->t_2[$i];
            }else if(Session::get('triwulan') == 3)
            {
                $inputR = $request->t_3[$i];
            }else if(Session::get('triwulan') == 4)
            {
                $inputR = $request->t_4[$i];
            }

            if(is_null($inputR))
            {
                $realisasi = 0;
            }else{
                $realisasi = $inputR;
            }
            $rumus = 4; //default rumusnya: -
            if(!is_null($request->rumus[$i]))
            {
                $rumus = $request->rumus[$i];
            }
            $newRequest->merge([
                'token'                  => $request->token,
                'id_indikator_kegiatan'  => $request->id_indikator_kegiatan[$i],
                'triwulan'               => Session::get('triwulan'),
                'realisasi'              => $realisasi,
                'rumus'                  => $rumus
            ]);
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->saveinputrealindkeg($newRequest)
                ->content()
            );
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->saveinputrealindkeg($newRequest)
                ->content()
            );

            // jika admin yang masuk atau sedang input monev perubahan
            if(Auth::user()->id_role == 1)
            {
                // simpan realisasi triwulan 1
                if(!is_null($request->t_1[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                  => $request->session()->get('token'),
                        'id_indikator_kegiatan'  => $request->id_indikator_kegiatan[$i],
                        'triwulan'               => 1,
                        'realisasi'              => $request->t_1[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindkeg($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 2
                if(!is_null($request->t_2[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                  => $request->session()->get('token'),
                        'id_indikator_kegiatan'  => $request->id_indikator_kegiatan[$i],
                        'triwulan'               => 2,
                        'realisasi'              => $request->t_2[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindkeg($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 3
                if(!is_null($request->t_3[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                  => $request->session()->get('token'),
                        'id_indikator_kegiatan'  => $request->id_indikator_kegiatan[$i],
                        'triwulan'               => 3,
                        'realisasi'              => $request->t_3[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindkeg($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 4
                if(!is_null($request->t_4[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                  => $request->session()->get('token'),
                        'id_indikator_kegiatan'  => $request->id_indikator_kegiatan[$i],
                        'triwulan'               => 4,
                        'realisasi'              => $request->t_4[$i]
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindkeg($newRequest)
                        ->content()
                    );
                }
            }
            $hasil[] = $data;
        }

        return $hasil;
    }
    
    /*----------------------------------------- 
        Menyimpan Realisasi Indikator Program
    -----------------------------------------*/
    public function inputRIndProg(Request $request)
    {
        $newRequest = new Request();
        $newRequest2 = new Request();
        $hasil = null;
        $inputR = null;
        for($i=0;$i<count($request->id_indikator_program);$i++)
        {
            // set nilai realisasi sesuai triwulan
            if(Session::get('triwulan') == 1)
            {
                $inputR = $request->t_1[$i];
            }else if(Session::get('triwulan') == 2)
            {
                $inputR = $request->t_2[$i];
            }else if(Session::get('triwulan') == 3)
            {
                $inputR = $request->t_3[$i];
            }else if(Session::get('triwulan') == 4)
            {
                $inputR = $request->t_4[$i];
            }
            if(is_null($inputR))
            {
                $realisasi = 0;
            }else{
                $realisasi = $inputR;
            }
            if(Auth::user()->id_role == 1)
            {
                $id_skpd = Session::get('pilih_id_skpd');
            }else{
                $id_skpd = Auth::user()->viewuser->id_skpd;
            }
            $rumus = 4; //default rumusnya: -
            if(!is_null($request->rumus[$i]))
            {
                $rumus = $request->rumus[$i];
            }
            $newRequest->merge([
                'token'                 => $request->token,
                'id_indikator_program'  => $request->id_indikator_program[$i],
                'triwulan'              => Session::get('triwulan'),
                'realisasi'             => $realisasi,
                'id_skpd'               => $id_skpd,
                'tahun'                 => $request->tahun,
                'rumus'                 => $rumus
            ]);
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->saveinputrealindprog($newRequest)
                ->content()
            );

            // jika admin yang masuk atau sedang input monev perubahan
            if(Auth::user()->id_role == 1)
            {
                // simpan realisasi triwulan 1
                if(!is_null($request->t_1[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                 => $request->token,
                        'id_indikator_program'  => $request->id_indikator_program[$i],
                        'triwulan'              => 1,
                        'realisasi'             => $request->t_1[$i],
                        'id_skpd'               => $id_skpd,
                        'tahun'                 => $request->tahun
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindprog($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 2
                if(!is_null($request->t_2[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                 => $request->token,
                        'id_indikator_program'  => $request->id_indikator_program[$i],
                        'triwulan'              => 2,
                        'realisasi'             => $request->t_2[$i],
                        'id_skpd'               => $id_skpd,
                        'tahun'                 => $request->tahun
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindprog($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 3
                if(!is_null($request->t_3[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                 => $request->token,
                        'id_indikator_program'  => $request->id_indikator_program[$i],
                        'triwulan'              => 3,
                        'realisasi'             => $request->t_3[$i],
                        'id_skpd'               => $id_skpd,
                        'tahun'                 => $request->tahun
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindprog($newRequest)
                        ->content()
                    );
                }
                // simpan realisasi triwulan 4
                if(!is_null($request->t_4[$i]))
                {
                    $newRequest = new Request();
                    $newRequest->merge([
                        'token'                 => $request->token,
                        'id_indikator_program'  => $request->id_indikator_program[$i],
                        'triwulan'              => 4,
                        'realisasi'             => $request->t_4[$i],
                        'id_skpd'               => $id_skpd,
                        'tahun'                 => $request->tahun
                    ]);
                    $data = json_decode(
                        app(
                            'App\Http\Controllers\Api\Api_RENJAController'
                        )
                        ->saveinputrealindprog($newRequest)
                        ->content()
                    );
                }
            }
            $hasil[] = $data;
        }
        return $hasil;
    }

    /*----------------------------------------- 
        Menyimpan Analisis Faktor
    -----------------------------------------*/
    public function inputAnlFaktor(Request $request)
    {
        $newRequest = new Request();
        $hasil = null;
        $newRequest->merge([
            'id_bidang_urusan'  => $request->id_bidang_urusan,
            'id_renja'          => $request->id_renja,
            'faktor_pendorong'  => $request->faktor_pendorong,
            'faktor_penghambat' => $request->faktor_penghambat,
            'triwulan'          => Session::get('triwulan'),
            'id_jafung'         => (Auth::user()->id_role == 13) ? Session::get('id_jafung') : '',
            'tl_triwulan'       => (Auth::user()->id_role == 13) ? $request->tl_triwulan : ''
        ]);
        $hasil = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->saveinputakbidangurusan($newRequest)
                    ->content()
                );
        return $hasil;
    }

    /*----------------------------------------- 
        Menyimpan Realisasi Analisis Faktor Sub Kegiatan
    -----------------------------------------*/
    public function inputAFSubKeg(Request $request)
    {
        $newRequest = new Request();
        $hasil = null;

        $newRequest->merge([
            'token' => $request->session()->get('token'),
            'id_sub_kegiatan' => $request->id_sub_kegiatan,
            'id_renja' => $request->id_renja,
            'triwulan' => Session::get('triwulan'),
            'id_jafung' => (Auth::user()->id_role == 13) ? Session::get('id_jafung') : '',
            'id_skdp' => (Auth::user()->id_role == 1) ? Session::get('pilih_id_skpd') : ''
        ]);
        // $hasil = $newRequest->all();
        // membuat request sesuai id role
        if(Auth::user()->id_role != 13)
        {
            $newRequest->merge([
                'inputKeteranganFP' => $request->input('inputKeteranganFP'),
                'inputKeteranganFPH' => $request->input('inputKeteranganFPH'),
            ]);
        }else{            
            $newRequest->merge([
                'inputKeteranganTL' => $request->input('inputKeteranganTL')
            ]);
        }
        // simpan analisis faktor sub kegiatan
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->saveinputanalisissubkeg($newRequest)
            ->content()
        );        
        $hasil[] = $data;

        return $hasil;
    }

    /*----------------------------------------- 
        Menyimpan Batas Waktu Input
    -----------------------------------------*/
    public function batasWaktuInput(Request $request)
    {
        $cek = BatasWaktuInput::where('jenisInput','renja')->get();
        
        if(count($cek)>0)
        {
            $uptBWI = BatasWaktuInput::where('jenisInput','renja')->firstOrFail();

            $uptBWI->mulai = date_format(date_create($request->mulai),"Y/m/d H:i:s");
            $uptBWI->akhir = date_format(date_create($request->akhir),"Y/m/d H:i:s");

            $uptBWI->timestamps = false;
            $uptBWI->save();
        }else{
            $putBWI = new BatasWaktuInput();
    
            $putBWI->mulai = date_format(date_create($request->mulai),"Y/m/d H:i:s");
            $putBWI->akhir = date_format(date_create($request->akhir),"Y/m/d H:i:s");
            $putBWI->jenisInput = 'renja';
    
            $putBWI->timestamps = false;
            $putBWI->save();
        }
        $request->session()->forget('bwi');
        $request->session()->put('bwi', BatasWaktuInput::where('jenisInput','renja')->get());
        return view('monev/renja/batasWaktu');
    }
    
    /*----------------------------------------- 
        Menyimpan Setting User
    -----------------------------------------*/
    public function settingUserSave(Request $request)
    {
        $dskpd = SKPD_90::get();
        $uskpd = UsersSKPD::where('id_user',$request->id_user)->delete();
        $user = User::where('id_user',$request->id_user)->first();
        
        if($user)
        {
            $user->username = $request->username;
            if($request->password != null)
            {
                $user->password = bcrypt($request->password);
            }
            $user->id_role = $request->role;
            $user->nama_pengguna = $request->nama_pengguna;
            $user->nip = $request->nip;
            
            $user->save();
        }
        if($request->semua_perangkat_daerah == 'on')
        {
            foreach($dskpd as $skpd)
            {
                $updbaru = new UsersSKPD();
                $updbaru->id_user = $request->id_user;
                $updbaru->id_skpd = $skpd->id_skpd;
                $updbaru->save();
            }

        }else{
            if($request->perangkat_daerah)
            {
                foreach($request->perangkat_daerah as $skpd)
                {
                    if($skpd)
                    {
                        $updbaru = new UsersSKPD();
                        $updbaru->id_user = $request->id_user;
                        $updbaru->id_skpd = $skpd;
                        $updbaru->save();
                    }
                }
            }
        }

        return redirect($request->linkpage);
    }

    /*----------------------------------------- 
        Menyimpan Setting Input
    -----------------------------------------*/
    public function settingInputSave(Request $request)
    {
        $cek = SettingInput::where('jenisInput',$request->jenisInput)->get();
        if(count($cek)>0)
        {
            $uptSI = SettingInput::where('jenisInput','renja')->firstOrFail();

            $uptSI->triwulan = $request->triwulan;
            $uptSI->tahun = $request->tahun;
            $uptSI->mulai = date_format(date_create($request->mulai),"Y/m/d H:i:s");
            $uptSI->akhir = date_format(date_create($request->akhir),"Y/m/d H:i:s");

            $uptSI->timestamps = false;
            $uptSI->save();
        }else{
            $uptSI = new SettingInput();
    
            $uptSI->triwulan = $request->triwulan;
            $uptSI->tahun = $request->tahun;
            $uptSI->mulai = date_format(date_create($request->mulai),"Y/m/d H:i:s");
            $uptSI->akhir = date_format(date_create($request->akhir),"Y/m/d H:i:s");
            $uptSI->jenisInput = $request->jenisInput;
    
            $uptSI->timestamps = false;
            $uptSI->save();
        }
        $request->session()->forget('periode_usulan');
        $request->session()->forget('tahun_monev');
        $request->session()->forget('triwulan');
        $request->session()->forget('bwi');
        $request->session()->put('periode_usulan', SettingInput::where('jenisInput',$request->jenisInput)->first()['tahun']);
        $request->session()->put('tahun_monev', SettingInput::where('jenisInput',$request->jenisInput)->first()['tahun']);
        $request->session()->put('triwulan', SettingInput::where('jenisInput',$request->jenisInput)->first()['triwulan']);
        $request->session()->put('bwi', array(
            'mulai' => SettingInput::where('jenisInput',$request->jenisInput)->first()['mulai'],
            'akhir' => SettingInput::where('jenisInput',$request->jenisInput)->first()['akhir'],
        ));
        
        $settingInput = SettingInput::first();
        return view('monev/renja/settingInput',compact('settingInput'));
    }
    /*----------------------------------------- 
        Tambah Indikator Sub Kegiatan
    -----------------------------------------*/
    public function addindsubkegiatan(Request $request)
    {
        $newRequest = new Request();
        $hasil = null;

        $newRequest->merge([
            'token' => $request->session()->get('token'),
            'id_sub_kegiatan' => $request->id_sub_kegiatan,
            'id_renja' => $request->id_renja,
            'indikator_subkegiatan' => $request->indikator_subkegiatan,
            'target' => $request->target,
            'satuan' => $request->satuan,
            'triwulan' => Session::get('triwulan'),
            'id_jafung' => (Auth::user()->id_role == 13) ? Session::get('id_jafung') : '',
            'id_skdp' => (Auth::user()->id_role == 1) ? Session::get('pilih_id_skpd') : ''
        ]);
        
        // simpan indikator sub kegiatan
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->saveaddindsubkegiatan($newRequest)
            ->content()
        );        
        $hasil[] = $data;
        
        return $hasil;
    }

    /*----------------------------------------- 
        Tambah Indikator Kegiatan
    -----------------------------------------*/
    public function addindkegiatan(Request $request)
    {
        $newRequest = new Request();
        $hasil = null;

        $newRequest->merge([
            'token' => $request->session()->get('token'),
            'id_kegiatan' => $request->id_kegiatan,
            'id_renja' => $request->id_renja,
            'indikator_kegiatan' => $request->indikator_kegiatan,
            'target' => $request->target,
            'satuan' => $request->satuan,
            'triwulan' => Session::get('triwulan'),
            'id_jafung' => (Auth::user()->id_role == 13) ? Session::get('id_jafung') : '',
            'id_skdp' => (Auth::user()->id_role == 1) ? Session::get('pilih_id_skpd') : ''
        ]);

        // simpan indikator kegiatan
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->saveaddindkegiatan($newRequest)
            ->content()
        );        
        $hasil[] = $data;
        
        return $hasil;
    }

    /*----------------------------------------- 
        Ke Halaman Import Monev Renja
    -----------------------------------------*/
    public function importMonev()
    {
        return view('monev/renja/importMonev');
    }
    
    /*----------------------------------------- 
       Proses Import Data Dari File
    -----------------------------------------*/
    public function importFile(Request $request)
    {
        // Validate the uploaded file
        $request->validate([
            'berkasImport' => 'required|mimes:xlsx,xls',
        ]);
 
        // Get the uploaded file
        $file = $request->file('berkasImport');
 
        // Process the Excel file
        ini_set('max_execution_time', 600);
        if($request->get('jenisData') == 'perubahan')
        {
            // 1. Import Monev Perubahan
            $collection = Excel::import(new ImportMonevPerubahan, $file);
        }else if($request->get('jenisData') == 'murni')
        {
            // 2. Import Monev Murni
            $collection = Excel::import(new ImportMonevMurni, $file);
        }
        return view('monev/renja/importMonev');
    }

    /*----------------------------------------- 
        Ke Halaman Rekap Renja
    -----------------------------------------*/
    public function rekapRenja(Request $request)
    {
        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = '';
        }else{
            $id_skpd = $request->session()->get('pilih_id_skpd');
        }

        // Membuat request baru untuk mengambil data sub kegiatan, kegiatan, program beserta indikatornya
        $newRequest = new Request();        
        $newRequest->merge([
            'token'     => $request->session()->get('token'),
            'triwulan'  => Session::get('triwulan'),
            'tahun'     => $request->session()->get('tahun_monev'),
            'opd'       => $id_skpd,
            'subkeg'    => ''
        ]);
        
        // Ambil data indikator program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getIndikatorProgram($newRequest)
            ->toJson()
        );
        // Ambil data indikator kegiatan
        $data2 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getIndikatorKegiatan($newRequest)
            ->toJson()
        );
        // Ambil data indikator sub kegiatan
        $data3 = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getIndikatorSubKegiatan($newRequest)
            ->toJson()
        );
        
        // Menampung data
        $program = $data;
        $kegiatan = null;
        $indKeg = null;
        $subKegiatan = null;
        foreach($data2 as $val)
        {
            $indKeg[$val->id_kegiatan] = $val->indikator;
        }
        foreach($data3 as $val)
        {
            $kegiatan[$val->kegiatan->program->id_program][$val->kegiatan->id_kegiatan] = array(
                'id_kegiatan' => $val->kegiatan->id_kegiatan,
                'kode_kegiatan' => $val->kegiatan->kode_kegiatan,
                'nama_kegiatan' => $val->kegiatan->nama_kegiatan,
                'nama_skpd' => $val->nama_skpd,
                'id_program' => $val->kegiatan->program->id_program
            );
            $subKegiatan[$val->kegiatan->id_kegiatan][$val->id_sub_kegiatan] = array(
                'id_sub_kegiatan' => $val->id_sub_kegiatan,
                'kode_sub_kegiatan' => $val->kode_sub_kegiatan,
                'nama_sub_kegiatan' => $val->nama_sub_kegiatan,
                'nama_skpd' => $val->nama_skpd,
                'indikator' => $val->indikator
            );
        }

        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            $id_skpd = Auth::user()->viewuser->id_skpd;// id_skpd untuk link cetak
        }
        return view('monev/renja/rekapRenja',compact('id_skpd','program','kegiatan','subKegiatan','indKeg'));
    }
    
    /*----------------------------------------- 
        Cetak Rekap: Renja dll
    -----------------------------------------*/
    public function cetakRekap($jenis_data,$file_type,$id_skpd,$all,$triwulan)
    {
        $typeFile = $file_type;
        $jenisData = $jenis_data;
        $do = redirect()->action([MonevRENJA_Controller::class, 'rekapRenja']);

        if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
        {
            // $id_skpd = Auth::user()->viewuser->id_skpd;
            $id_skpd = Session::get('id_skpd')[0];
        }

        // Membuat request baru untuk mengambil data sub kegiatan, kegiatan, program beserta indikatornya
        $newRequest = new Request();        
        $newRequest->merge([
            'token'     => Session::get('token'),
            'triwulan'  => $triwulan,
            'tahun'     => Session::get('tahun_monev'),
            'opd'       => $id_skpd,
            'subkeg'    => '',
            'format'    => 'lama',
            'all'       => $all
        ]);
        
        /*--------------------------------------------------------------
            MENGAMBIL DATA SKPD
        --------------------------------------------------------------*/
        $daftar_skpd = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getdata($newRequest)
            ->toJson()
        );
        /*--------------------------------------------------------------*/
        /*--------------------------------------------------------------
            MENGAMBIL DATA CETAK
        --------------------------------------------------------------*/
        if($all == 'true')
        {
            // Ambil data program
            foreach($daftar_skpd as $skpd)
            {
                $newRequestAll = new Request();
                $newRequestAll->merge([
                    'opd'       => $skpd->id_skpd,
                    'token'     => Session::get('token'),
                    'triwulan'  => $triwulan,
                    'tahun'     => Session::get('tahun_monev'),
                    'subkeg'    => '',
                    'format'    => 'lama',
                    'all'       => false
                ]); 
                $data[] = json_decode(
                    app(
                        'App\Http\Controllers\Api\Api_RENJAController'
                    )
                    ->getCetakRKPD($newRequestAll)
                    ->toJson()
                );
            }
        }else{
            // Ambil data program
            $data = json_decode(
                app(
                    'App\Http\Controllers\Api\Api_RENJAController'
                )
                ->getCetakRKPD($newRequest)
                ->toJson()
            );
        }
        $dataCetak = $data;
        /*--------------------------------------------------------------*/
        // dd($dataCetak);
        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER SUB KEGIATAN
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisSubKegiatan($newRequest)
            ->toJson()
        );
        $anlSKG = null;
        foreach($data as $afSKG)
        {
            $anlSKG[$afSKG->id_sub_kegiatan] = $afSKG;
        }
        /*--------------------------------------------------------------*/
        /*--------------------------------------------------------------
            MENGAMBIL DATA ANALISIS FAKTOR PER BIDANG URUSAN
        --------------------------------------------------------------*/
        // Ambil data program
        $data = json_decode(
            app(
                'App\Http\Controllers\Api\Api_RENJAController'
            )
            ->getAnalisisFaktor($newRequest)
            ->toJson()
        );
        $anlFkt = $data;
        /*--------------------------------------------------------------*/
        if($all == 'true')
        {
            $nama_skpd = $daftar_skpd;
        }else{
            $nama_skpd = SKPD_90::select('nama_skpd')->where('id_skpd', $id_skpd)->first()->nama_skpd;
        }
        if($typeFile == 'pdf') //download pdf
        {
            if($jenisData == 'renja') //data RENJA
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                if($all == 'true')
                {
                    // dd($dataCetak);
                    $file = PDF::loadView('monev.renja.download.pdf_rekapRENJAAll',compact('dataCetak','nama_skpd','triwulan'));
                }else{
                    $file = PDF::loadView('monev.renja.download.pdf_rekapRENJA',compact('dataCetak','nama_skpd','triwulan'));
                }
            }else if($jenisData == 'renja_af_skg')
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                $file = PDF::loadView('monev.renja.download.pdf_rekapAnalisisFaktor_SubKegiatan_RENJA',compact('dataCetak','triwulan','anlSKG','nama_skpd'));
            }else if($jenisData == 'renja_af_bu')
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                $file = PDF::loadView('monev.renja.download.pdf_rekapAnalisisFaktor_BidangUrusan_RENJA',compact('dataCetak','triwulan','anlFkt','nama_skpd'));
            }
            $do = $file->stream();
        }else if($typeFile == 'excel'){ //download excel
            if($jenisData == 'renja') //data RENJA
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                if($all == 'true')
                {
                    // dd($dataCetak);
                    $do = Excel::download(new CetakRenja($newRequest), 'excel_rekapRENJAAll.xlsx');
                }else{
                    $do = Excel::download(new CetakRenja($newRequest), 'excel_rekapRENJA.xlsx');
                }
            }else if($jenisData == 'renja_af_skg')
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                $do = Excel::download(new CetakRenjaAFSubKeg($newRequest), 'excel_rekapAnalisisFaktor_SubKegiatan_RENJA.xlsx');
            }else if($jenisData == 'renja_af_bu')
            {
                ini_set('max_execution_time', 300);
                ini_set("memory_limit","512M");
                $do = Excel::download(new CetakRenjaAFBdgUrs($newRequest), 'excel_rekapAnalisisFaktor_BidangUrusan_RENJA.xlsx');
            }
        }
        
        return $do;
    }

    public function monevHasilMurni(Request $request)
    {
        if($request->session()->get('id_role') != 1){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
// 
        $status_input_keg = $request->input('status_input_keg');
        $status_input_subkeg = $request->input('status_input_subkeg');
        $status_input_pd = $request->input('status_nama_pd');
        $status_input_pd_subkeg = $request->input('status_nama_pd_subkeg');
        
        return view('monev/renja/murni',compact('id_pd','status_input_pd','status_input_pd_subkeg','status_input_keg','status_input_subkeg'));
    }

    public function monevHasilPerubahan(Request $request)
    {
        if($request->session()->get('id_role') != 1){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
// 
        $status_input_keg = $request->input('status_input_keg');
        $status_input_subkeg = $request->input('status_input_subkeg');
        $status_input_pd = $request->input('status_nama_pd');
        $status_input_pd_subkeg = $request->input('status_nama_pd_subkeg');
        
        return view('monev/renja/perubahan',compact('id_pd','status_input_pd','status_input_pd_subkeg','status_input_keg','status_input_subkeg'));
    }

    public function inputRealisasiIndKegiatan(Request $request)
    {
        $data = $request->input('data_realisasi');
        $triwulan = $request->input('tw');
        $isnew = $request->input('isnew');
        return view('monev/renja/inputrealisasiindikatorkegiatan',compact('data','triwulan','isnew'));
    }

    public function inputAnalisisKegiatan(Request $request)
    {
        $renja = Renja_90::where('periode_usulan',Session::get('tahun_monev'))->where('id_skpd',$request->input('id_pd'))->first();
        //untuk PD
        $faktorPD = FaktorPendorongKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();
        $faktorPH = FaktorPenghambatKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();
        $tindakLJ = TindakLanjutKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();

        //Untuk Bidang
        $faktorPD_B = FaktorPendorongKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();
        $faktorPH_B = FaktorPenghambatKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();
        $tindakLJ_B = TindakLanjutKegiatan::where('id_kegiatan',$request->input('id_kegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();

        //Untuk Jafung
        $id_kegiatan = $request->input('id_kegiatan');
        $analisisJF = AnalisatorKegiatan::with('jafung','fpk','fphk','ftlk')->whereHas('fpk', function ($query) use ($renja,$id_kegiatan) {
            $query->where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->orwhereHas('fphk', function ($query) use ($renja,$id_kegiatan) {
            $query->where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->orwhereHas('ftlk', function ($query) use ($renja,$id_kegiatan) {
            $query->where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->whereNotNull('id_jafung')->get();

        
        if(Session::get('id_role') == 13 && count($analisisJF) > 0){
            //
        }else if(Session::get('id_role') == 13 && count($analisisJF) == 0){
            $jf = Jafung_SektorPD::where('id_jafung',Session::get('id_jafung'))->where('id_skpd', $renja->id_skpd)->with('jafung')->get();
            $analisisJF = $jf;
            
            
        }
        return view('monev/renja/inputanalisiskegiatan',compact('faktorPD','faktorPH','tindakLJ','faktorPD_B','faktorPH_B','tindakLJ_B','analisisJF'));
    }

    public function saveRealisasiKegiatan(Request $request)
    {
        //dd($request);
        
        //insert indiaktor
        $renja = Renja_90::where('periode_usulan',Session::get('tahun_monev'))->where('id_skpd',$request->input('id_pd'))->first();
        $id_kegiatan = $request->input('id_kegiatan');
        $id_indikator = $request->input('id_indikator_kegiatan');
        $old_data = count(array_filter($id_indikator, function($value) {
            return $value != 0;  
        }));
        $indikator_kegiatan = $request->input('nama_indikator');
        $satuan = $request->input('satuan');
        $realisasi = $request->input('realisasi');
        $persentase = $request->input('persentase');
        $isConstant = $request->input('isConstant');
        $target_perubahan = $request->input('target_perubahan');
        $emptied = $request->input('indkeg_emptied');
        $ind_prog = $request->input('indprog_checked');
        $status_verifikasi = $request->input('status_verifikasi');
        $catatan_verifikasi = $request->input('catatan_verifikasi');
        $is_confirm = $request->input('is_confirm');
        $id_skpd = $request->input('id_pd');
        $tw = $request->input('triwulan');
        $tw_alias = ($tw < 4) ? 1 : 2;

        $id_role = Session::get('id_role');
        $id_bidang = Session::get('id_bidang');
        $id_jafung = Session::get('id_jafung');
        
        for($i=0;$i<count($id_indikator);$i++){
            $id_indikator_kegiatan = $id_indikator[$i];

            if($id_role == 13){
                $jf = Jafung::find($id_jafung);
                $vikeg = VerifikasiIndikatorKegiatan_90::updateOrCreate(['id_indikator_kegiatan' => $id_indikator_kegiatan],['catatan_'.$tw_alias => $catatan_verifikasi[$i],'status_verifikasi_'.$tw_alias => $status_verifikasi[$i],'is_confirm_'.$tw_alias => $is_confirm[$i],'verified_by_'.$tw_alias => $jf->nama]);
                $rikeg = R_IndikatorKegiatan_90::updateOrCreate(['id_indikator_kegiatan' => $id_indikator_kegiatan],['persen_'.$tw => $persentase[$i]]);
            }elseif($id_role <= 4){
                if($id_indikator[$i] == 0){
                    $ikeg = IndikatorKegiatan_90::create(['id_kegiatan' => $id_kegiatan,'id_renja' => $renja->id_renja,'indikator_kegiatan'=> $indikator_kegiatan[$i-$old_data],'target'=> 0,'satuan'=> $satuan[$i-$old_data],'is_added'=> 4,'is_emptied'=> 0,'isDeleted'=> 0,'target_perubahan'=> $target_perubahan[$i],'is_constant'=> $isConstant[$i]]);
                    $id_indikator_kegiatan = $ikeg->id_indikator_kegiatan;
                    $rikeg = R_IndikatorKegiatan_90::create(['id_indikator_kegiatan' => $id_indikator_kegiatan,'id_renja' => $renja->id_renja,'t_'.$tw => $realisasi[$i],'persen_'.$tw => $persentase[$i]]);
                }else{
                    $rikeg = R_IndikatorKegiatan_90::updateOrCreate(['id_indikator_kegiatan' => $id_indikator_kegiatan],['t_'.$tw => $realisasi[$i], 'persen_'.$tw => $persentase[$i]]);
                }
            }elseif($id_role <= 6){
                $rikeg = R_IndikatorKegiatan_90::updateOrCreate(['id_indikator_kegiatan' => $id_indikator_kegiatan],['persen_'.$tw => $persentase[$i]]);
            }
            
            
            $data_ind = R_IndikatorKegiatan_90::where('id_indikator_kegiatan',$id_indikator_kegiatan)->first();
            
            if($isConstant[$i] == 0){
                $total_persen = $data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3;
                $total_persen_perubahan = $data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3+$data_ind->persen_4;
            }else{
                $total_persen = ($data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3)/3;
                $total_persen_perubahan = ($data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3+$data_ind->persen_4)/4;
            }
            
            $arr_update = ['is_constant' => $isConstant[$i]];
            if($tw == 4 && $id_role <= 4){
                $arr_update = ['target_perubahan' => $target_perubahan[$i],'is_constant' => $isConstant[$i]];
            }
            
            $ikeg = IndikatorKegiatan_90::where('id_indikator_kegiatan', $id_indikator_kegiatan)
                ->update($arr_update);
            $rikeg = R_IndikatorKegiatan_90::where('id_indikator_kegiatan', $id_indikator_kegiatan)
                    ->update(['total_persen' => $total_persen,'total_persen_perubahan' => $total_persen_perubahan]);
        }

        if($emptied){
            $arr = explode(",",$emptied);
            for($y=0;$y<count($arr);$y++){
                $ikeg = IndikatorKegiatan_90::where('id_indikator_kegiatan', $arr[$y])
                ->update(['is_emptied' => 4]);
            }
        }

        if($ind_prog){
            $arr = explode(",",$ind_prog);
            //input update indikator program rpjmd
            $dataindprog = CascadingKegiatan_90::whereNotIn('id_indikator_program',$arr)->where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->get();
            if(count($dataindprog) > 0){
                foreach($dataindprog as $key => $value){
                    CascadingKegiatan_90::where('id_cascading_kegiatan',$value->id_cascading_kegiatan)->delete();
                }
            }else{
                for($i=0;$i<count($arr);$i++){
                    CascadingKegiatan_90::updateOrCreate(['id_kegiatan' => $id_kegiatan, 'id_renja' => $renja->id_renja, 'id_indikator_program' => $arr[$i]]);
                }
            } 
        }else{
            CascadingKegiatan_90::where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->delete();
        }

        $analisator = AnalisatorKegiatan::when($id_role == 4, function ($q) use ($id_skpd) {
                return $q->where('id_skpd', $id_skpd);
            })->when($id_role == 6, function ($q) use ($id_bidang) {
                return $q->where('id_bidang', $id_bidang);
            })->when($id_role == 13, function ($q) use ($id_jafung) {
                return $q->where('id_jafung', $id_jafung);
            })->first();

        if(!isset($analisator)){
            $analisator = new AnalisatorKegiatan();
            if($id_role == 4){
                $analisator->id_skpd = $id_skpd;
            }else if($id_role == 6){
                $analisator->id_bidang = $id_bidang;
            }else if($id_role == 13){
                $analisator->id_jafung = $id_jafung;
            }
            $analisator->save();
        }

        //faktorpendorong
        FaktorPendorongKegiatan::updateOrCreate(
            ['id_kegiatan' => $request->input('id_kegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaFP'),
            'sdm' => $request->input('inputSDMFP'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanFP'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganFP'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaFP'),
            'perijinan' => $request->input('inputPerijinanFP'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanFP'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatFP'),
            'faktor_alam' => $request->input('inputFaktorAlamFP'),
            'keterangan' => $request->input('inputKeteranganFP') ]);
        
        //faktorpenghambat
        FaktorPenghambatKegiatan::updateOrCreate(
            ['id_kegiatan' => $request->input('id_kegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaFPH'),
            'sdm' => $request->input('inputSDMFPH'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanFPH'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganFPH'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaFPH'),
            'perijinan' => $request->input('inputPerijinanFPH'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanFPH'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatFPH'),
            'faktor_alam' => $request->input('inputFaktorAlamFPH'),
            'keterangan' => $request->input('inputKeteranganFPH') ]);

        //tindaklanjut
        TindakLanjutKegiatan::updateOrCreate(
            ['id_kegiatan' => $request->input('id_kegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaTL'),
            'sdm' => $request->input('inputSDMTL'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanTL'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganTL'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaTL'),
            'perijinan' => $request->input('inputPerijinanTL'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanTL'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatTL'),
            'faktor_alam' => $request->input('inputFaktorAlamTL'),
            'keterangan' => $request->input('inputKeteranganTL') ]);
        
            return response()->json([
                'kode' => 200,
                'status' => 'sukses',
                'message' => 'Data Realisasi Indikator Kegiatan Berhasil Disimpan.',
            ]);
    }
    
    public function inputRealisasiIndSubKegiatan(Request $request)
    {
        $data = $request->input('data_realisasi');
        $triwulan = $request->input('tw');
        $isnew = $request->input('isnew');
        
        return view('monev/renja/inputrealisasiindikatorsubkegiatan',compact('data','triwulan','isnew'));
    }

    public function inputAnalisisSubKegiatan(Request $request)
    {
        $renja = Renja_90::where('periode_usulan',Session::get('tahun_monev'))->where('id_skpd',$request->input('id_pd'))->first();
        //untuk PD
        $faktorPD = FaktorPendorongSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();
        $faktorPH = FaktorPenghambatSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();
        $tindakLJ = TindakLanjutSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_skpd', $renja->id_skpd);
        })->get();

        //Untuk Bidang
        $faktorPD_B = FaktorPendorongSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();
        $faktorPH_B = FaktorPenghambatSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();
        $tindakLJ_B = TindakLanjutSubKegiatan::where('id_sub_kegiatan',$request->input('id_subkegiatan'))->where('id_renja',$renja->id_renja)->where('triwulan',$request->input('tw'))->whereHas('analisator', function ($query) use ($renja) {
            $query->where('id_bidang', $renja->skpd->id_bidang);
        })->get();

        //Untuk Jafung
        $id_sub_kegiatan = $request->input('id_subkegiatan');
        $analisisJF = AnalisatorSubKegiatan::with('jafung','fpk','fphk','ftlk')->whereHas('fpk', function ($query) use ($renja,$id_sub_kegiatan) {
            $query->where('id_sub_kegiatan',$id_sub_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->orwhereHas('fphk', function ($query) use ($renja,$id_sub_kegiatan) {
            $query->where('id_sub_kegiatan',$id_sub_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->orwhereHas('ftlk', function ($query) use ($renja,$id_sub_kegiatan) {
            $query->where('id_sub_kegiatan',$id_sub_kegiatan)->where('id_renja',$renja->id_renja)->whereHas('analisator', function ($q) {
                $q->whereNotNull('id_jafung');
            });
        })->whereNotNull('id_jafung')->get();

        
        if(Session::get('id_role') == 13 && count($analisisJF) > 0){
            //
        }else if(Session::get('id_role') == 13 && count($analisisJF) == 0){
            $jf = Jafung_SektorPD::where('id_jafung',Session::get('id_jafung'))->where('id_skpd', $renja->id_skpd)->with('jafung')->get();
            $analisisJF = $jf;
            
            
        }
        return view('monev/renja/inputanalisissubkegiatan',compact('faktorPD','faktorPH','tindakLJ','faktorPD_B','faktorPH_B','tindakLJ_B','analisisJF'));
    }

    public function saveRealisasiSubKegiatan(Request $request)
    {
        //dd($request);
        //insert indiaktor
        $renja = Renja_90::where('periode_usulan',Session::get('tahun_monev'))->where('id_skpd',$request->input('id_pd_subkegiatan'))->first();
        $id_sub_kegiatan = $request->input('id_subkegiatan');
        $id_indikator = $request->input('id_indikator_subkegiatan');
        $old_data = count(array_filter($id_indikator, function($value) {
            return $value != 0;  
        }));
        $indikator_subkegiatan = $request->input('nama_indikator');
        $satuan = $request->input('satuan');
        $realisasi = $request->input('realisasi');
        $persentase = $request->input('persentase');
        $isConstant = $request->input('isConstant');
        $target_perubahan = $request->input('target_perubahan');
        $emptied = $request->input('indsubkeg_emptied');
        $ind_keg = $request->input('indkeg_checked');
        $status_verifikasi = $request->input('status_verifikasi');
        $catatan_verifikasi = $request->input('catatan_verifikasi');
        $is_confirm = $request->input('is_confirm');
        $id_skpd = $request->input('id_pd_subkegiatan');
        $tw = $request->input('triwulan');
        $tw_alias = ($tw < 4) ? 1 : 2;

        $id_role = Session::get('id_role');
        $id_bidang = Session::get('id_bidang');
        $id_jafung = Session::get('id_jafung');
        
        for($i=0;$i<count($id_indikator);$i++){
            $id_indikator_subkegiatan = $id_indikator[$i]; 

            if($id_role == 13){
                $jf = Jafung::find($id_jafung);
                $visubkeg = VerifikasiIndikatorSubKegiatan_90::updateOrCreate(['id_indikator_subkegiatan' => $id_indikator_subkegiatan],['catatan_'.$tw_alias => $catatan_verifikasi[$i],'status_verifikasi_'.$tw_alias => $status_verifikasi[$i],'is_confirm_'.$tw_alias => $is_confirm[$i],'verified_by_'.$tw_alias => $jf->nama]);
                $risubkeg = R_IndikatorSubKegiatan_90::updateOrCreate(['id_indikator_subkegiatan' => $id_indikator_subkegiatan],['persen_'.$tw => $persentase[$i]]);
            }elseif($id_role <= 4){
                if($id_indikator[$i] == 0){
                    $isubkeg = IndikatorSubKegiatan_90::create(['id_sub_kegiatan' => $id_sub_kegiatan,'id_renja' => $renja->id_renja,'indikator_subkegiatan'=> $indikator_subkegiatan[$i-$old_data],'target'=> 0,'satuan'=> $satuan[$i-$old_data],'is_added'=> 4,'is_emptied'=> 0,'isDeleted'=> 0,'target_perubahan'=> $target_perubahan[$i],'is_constant'=> $isConstant[$i]]);
                    $id_indikator_sub_kegiatan = $ikeg->id_indikator_sub_kegiatan;
                    $risubkeg = R_IndikatorSubKegiatan_90::create(['id_indikator_subkegiatan' => $id_indikator_subkegiatan,'id_renja' => $renja->id_renja,'t_'.$tw => $realisasi[$i],'persen_'.$tw => $persentase[$i]]);
                }else{
                    $risubkeg = R_IndikatorSubKegiatan_90::updateOrCreate(['id_indikator_subkegiatan' => $id_indikator_subkegiatan],['t_'.$tw => $realisasi[$i], 'persen_'.$tw => $persentase[$i]]);
                }
            }elseif($id_role <= 6){
                $risubkeg = R_IndikatorSubKegiatan_90::updateOrCreate(['id_indikator_subkegiatan' => $id_indikator_subkegiatan],['persen_'.$tw => $persentase[$i]]);
            }

            $data_ind = R_IndikatorSubKegiatan_90::where('id_indikator_subkegiatan',$id_indikator_subkegiatan)->first();
            
            if($isConstant[$i] == 0){
                $total_persen = $data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3;
                $total_persen_perubahan = $data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3+$data_ind->persen_4;
            }else{
                $total_persen = ($data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3)/3;
                $total_persen_perubahan = ($data_ind->persen_1+$data_ind->persen_2+$data_ind->persen_3+$data_ind->persen_4)/4;
            }
            
            $arr_update = ['is_constant' => $isConstant[$i]];
            if($tw == 4 && $id_role <= 4){
                $arr_update = ['target_perubahan' => $target_perubahan[$i],'is_constant' => $isConstant[$i]];
            }
            
            $ikeg = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan', $id_indikator_subkegiatan)
                ->update($arr_update);
            $rikeg = R_IndikatorSubKegiatan_90::where('id_indikator_subkegiatan', $id_indikator_subkegiatan)
                    ->update(['total_persen' => $total_persen,'total_persen_perubahan' => $total_persen_perubahan]);
        }

        if($emptied){
            $arr = explode(",",$emptied);
            for($y=0;$y<count($arr);$y++){
                $ikeg = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan', $arr[$y])
                ->update(['is_emptied' => 4]);
            }
        }

        if($ind_keg){
            $arr = explode(",",$ind_keg);
            //input update indikator kegiatan
            $dataindkeg = CascadingSubKegiatan_90::whereNotIn('id_indikator_kegiatan',$arr)->where('id_sub_kegiatan',$id_sub_kegiatan)->where('id_renja',$renja->id_renja)->get();
            if(count($dataindkeg) > 0){
                foreach($dataindkeg as $key => $value){
                    CascadingSubKegiatan_90::where('id_cascading_subkegiatan',$value->id_cascading_subkegiatan)->delete();
                }
            }else{
                for($i=0;$i<count($arr);$i++){
                    CascadingSubKegiatan_90::updateOrCreate(['id_sub_kegiatan' => $id_sub_kegiatan, 'id_renja' => $renja->id_renja, 'id_indikator_kegiatan' => $arr[$i]]);
                }
            } 
        }else{
            CascadingSubKegiatan_90::where('id_sub_kegiatan',$id_sub_kegiatan)->where('id_renja',$renja->id_renja)->delete();
        }

        $analisator = AnalisatorSubKegiatan::when($id_role == 4, function ($q) use ($id_skpd) {
                return $q->where('id_skpd', $id_skpd);
            })->when($id_role == 6, function ($q) use ($id_bidang) {
                return $q->where('id_bidang', $id_bidang);
            })->when($id_role == 13, function ($q) use ($id_jafung) {
                return $q->where('id_jafung', $id_jafung);
            })->first();

        if(!isset($analisator)){
            $analisator = new AnalisatorSubKegiatan();
            if($id_role == 4){
                $analisator->id_skpd = $id_skpd;
            }else if($id_role == 6){
                $analisator->id_bidang = $id_bidang;
            }else if($id_role == 13){
                $analisator->id_jafung = $id_jafung;
            }
            $analisator->save();
        }

        //faktorpendorong
        FaktorPendorongSubKegiatan::updateOrCreate(
            ['id_sub_kegiatan' => $request->input('id_subkegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaFP'),
            'sdm' => $request->input('inputSDMFP'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanFP'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganFP'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaFP'),
            'perijinan' => $request->input('inputPerijinanFP'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanFP'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatFP'),
            'faktor_alam' => $request->input('inputFaktorAlamFP'),
            'keterangan' => $request->input('inputKeteranganFP') ]);
        
        //faktorpenghambat
        FaktorPenghambatSubKegiatan::updateOrCreate(
            ['id_sub_kegiatan' => $request->input('id_subkegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaFPH'),
            'sdm' => $request->input('inputSDMFPH'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanFPH'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganFPH'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaFPH'),
            'perijinan' => $request->input('inputPerijinanFPH'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanFPH'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatFPH'),
            'faktor_alam' => $request->input('inputFaktorAlamFPH'),
            'keterangan' => $request->input('inputKeteranganFPH') ]);

        //tindaklanjut
        TindakLanjutSubKegiatan::updateOrCreate(
            ['id_sub_kegiatan' => $request->input('id_subkegiatan'), 'id_renja' => $renja->id_renja, 'triwulan' => $tw, 'id_analisator' => $analisator->id_analisator],
            ['dana' => $request->input('inputDanaTL'),
            'sdm' => $request->input('inputSDMTL'),
            'waktu_pelaksanaan' => $request->input('inputWaktuPelaksanaanTL'),
            'peraturan_perundangan' => $request->input('inputPeraturanPerundanganTL'),
            'sistem_pengadaan_barang_jasa' => $request->input('inputSistemPengadaanBarangJasaTL'),
            'perijinan' => $request->input('inputPerijinanTL'),
            'ketersediaan_lahan' => $request->input('inputKetersediaanLahanTL'),
            'kesiapan_dukungan_masyarakat' => $request->input('inputKesiapanDukunganMasyarakatTL'),
            'faktor_alam' => $request->input('inputFaktorAlamTL'),
            'keterangan' => $request->input('inputKeteranganTL') ]);

            return response()->json([
                'kode' => 200,
                'status' => 'sukses',
                'message' => 'Data Realisasi Indikator Sub Kegiatan Berhasil Disimpan.',
            ]);
        
    }

    public function exportRkpd(Request $request)
    {
        return Excel::download(new CetakFormatRKPD, 'RKPD TW 3 Tahun 2022.xlsx');
    }
}

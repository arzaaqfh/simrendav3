<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kegiatan_90;
use App\Models\LogRenja_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\IndikatorProgramRPJMD;
use App\Models\SKPD_90;
use App\Models\Satuan;
use App\Models\CascadingKegiatan_90;
use App\Models\CascadingSubKegiatan_90;
use App\Models\VerifikasiIndikatorKegiatan_90;
use App\Models\VerifikasiIndikatorSubKegiatan_90;

class Monev_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('monev/dashboard');
    }
    
    // Grafik Monev
    public function grafikMonev(Request $request)
    {   
        $pd = SKPD_90::pluck('nama_skpd');
        // $names = [0 => 'name1', 1 => 'name2'];
        $classes = null;
        foreach($pd as $rpd)
        {
            $classes[] = array('50','60','70','90','100');
        }

        $merged = collect($pd)->zip($classes)->transform(function ($values) {
            return [
                'name' => $values[0],
                'data' => $values[1],
            ];
        });

        // dd($merged->all());
        $hpd = json_encode($merged);
        // return $merged;
        // return $pd;
        // return $hpd;
        return view('monev/grafik/dashboard', compact('hpd'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Musrenbang_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('musrenbang/dashboard');
    }
}

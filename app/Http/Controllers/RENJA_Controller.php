<?php

namespace App\Http\Controllers;
use \stdClass;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Kegiatan_90;
use App\Models\LogRenja_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\Renja_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\IndikatorProgramRPJMD;
use App\Models\SKPD_90;
use App\Models\Satuan;
use App\Models\CascadingKegiatan_90;
use App\Models\CascadingSubKegiatan_90;
use App\Models\VerifikasiIndikatorKegiatan_90;
use App\Models\VerifikasiIndikatorSubKegiatan_90;
use App\Models\BatasWaktu;

class RENJA_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    private $id_skpd;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        return view('perencanaan/renja/dashboard');
    }

    public function renjaFinal(Request $request)
    {
        if($request->session()->get('id_role') != 1){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        return view('perencanaan/renja/final',compact('id_pd'));
    }

    public function renjaCascading()
    {
        return view('perencanaan/renja/cascadingrenja');
    }

    private $pd_keg;
    public function saveIndikatorKegiatan(Request $request)
    {
        //dd($request);
        $renja = Renja_90::where('periode_usulan',Session::get('periode_usulan'))->where('id_skpd',$request->input('pd_keg'))->first();
        $id_kegiatan = $request->input('id_kegiatan');
        $id_indikator = $request->input('id_indikator_kegiatan');
        $indikator = $request->input('nama_indikator_kegiatan');
        $target = $request->input('target_indikator_kegiatan');
        $satuan = $request->input('satuan_indikator_kegiatan');

        $delete_indkeg = $request->input('deleted_indkeg');
        $id_ind_rpjmd = $request->input('id_ind_program_rpjmd');

        //input update indikator
        for($i=0;$i<count($id_indikator);$i++){
            if($id_indikator[$i] == 0){
                $cek = IndikatorKegiatan_90::where('id_renja',$renja->id_renja)->where('id_kegiatan',$id_kegiatan)->whereNull('indikator_kegiatan')->first();
                if($cek){
                    $isubkeg = IndikatorKegiatan_90::where('id_indikator_kegiatan', $cek->id_indikator_kegiatan)
                        ->update(['indikator_kegiatan' => $indikator[$i],'target' => $target[$i],'satuan' => $satuan[$i],'is_added' => 0,'is_emptied' => 0,'isDeleted' => 0 ]);
                }else{
                    $putInd = new IndikatorKegiatan_90();
                    $putInd->id_kegiatan = $id_kegiatan;
                    $putInd->id_renja = $renja->id_renja;
                    $putInd->indikator_kegiatan = $indikator[$i];
                    $putInd->target = $target[$i];
                    $putInd->satuan = $satuan[$i];
                    $putInd->is_added = 0;
                    $putInd->is_emptied = 0;
                    $putInd->isDeleted = 0;
                    $putInd->save();
                }
            }else if($indikator[$i] != null){
                $ikeg = IndikatorKegiatan_90::updateOrCreate(
                    ['id_indikator_kegiatan' => $id_indikator[$i],'id_kegiatan' => $id_kegiatan, 'id_renja' => $renja->id_renja],
                    ['indikator_kegiatan' => $indikator[$i],'target' => $target[$i],'satuan' => $satuan[$i],'is_added' => 0,'is_emptied' => 0,'isDeleted' => 0 ]);
            }
        }

        //input update indikator program rpjmd
        if($id_ind_rpjmd){
            $dataindprog = CascadingKegiatan_90::whereNotIn('id_indikator_program',$id_ind_rpjmd)->where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->get();
            if(count($dataindprog) > 0){
                foreach($dataindprog as $key => $value){
                    CascadingKegiatan_90::where('id_cascading_kegiatan',$value->id_cascading_kegiatan)->delete();
                }
            }else{
                for($i=0;$i<count($id_ind_rpjmd);$i++){
                    CascadingKegiatan_90::updateOrCreate(['id_kegiatan' => $id_kegiatan, 'id_renja' => $renja->id_renja, 'id_indikator_program' => $id_ind_rpjmd[$i]]);
                }
            }    
        }else{
            CascadingKegiatan_90::where('id_kegiatan',$id_kegiatan)->where('id_renja',$renja->id_renja)->delete();
        }

        //delete indikator
        if($delete_indkeg[0]){
            $deleted = json_decode($delete_indkeg[0]);
            if(count($deleted) > 0){
                for($i=0;$i<count($deleted);$i++){
                    IndikatorKegiatan_90::where('id_indikator_kegiatan',$deleted[$i])->delete();
                } 
            }
        }

        return response()->json([
            'kode' => 200,
            'status' => 'sukses',
            'message' => 'Data Indikator Kegiatan Berhasil Disimpan.',
        ]);
    }

    private $pd_subkeg;
    public function saveIndikatorSubKegiatan(Request $request)
    {
        //dd($request);
        $renja = Renja_90::where('periode_usulan',Session::get('periode_usulan'))->where('id_skpd',$request->input('pd_subkeg'))->first();
        $id_subkegiatan = $request->input('id_subkegiatan');
        $id_indikator = $request->input('id_indikator_subkegiatan');
        $indikator = $request->input('nama_indikator_subkegiatan');
        $target = $request->input('target_indikator_subkegiatan');
        $satuan = $request->input('satuan_indikator_subkegiatan');

        $delete_indsubkeg = $request->input('deleted_indsubkeg');
        $id_ind_keg = $request->input('id_indikator_kegiatan');
        $realsi_sub = $request->input('relasisubkeg');

        //input update indikator
        for($i=0;$i<count($id_indikator);$i++){
            if($id_indikator[$i] == 0){
                $cek = IndikatorSubKegiatan_90::where('id_renja',$renja->id_renja)->where('id_sub_kegiatan',$id_subkegiatan)->whereNull('indikator_subkegiatan')->first();
                if($cek){
                    $isubkeg = IndikatorSubKegiatan_90::where('id_indikator_subkegiatan', $cek->id_indikator_subkegiatan)
                        ->update(['indikator_subkegiatan' => $indikator[$i],'target' => $target[$i],'satuan' => $satuan[$i],'is_added' => 0,'is_emptied' => 0,'isDeleted' => 0 ]);
                }else{
                    $putInd = new IndikatorSubKegiatan_90();
                    $putInd->id_sub_kegiatan = $id_subkegiatan;
                    $putInd->id_renja = $renja->id_renja;
                    $putInd->indikator_subkegiatan = $indikator[$i];
                    $putInd->target = $target[$i];
                    $putInd->satuan = $satuan[$i];
                    $putInd->is_added = 0;
                    $putInd->is_emptied = 0;
                    $putInd->isDeleted = 0;
                    $putInd->save();
                }
            }else if($indikator[$i] != null){
                $isubkeg = IndikatorSubKegiatan_90::updateOrCreate(
                    ['id_indikator_subkegiatan' => $id_indikator[$i],'id_sub_kegiatan' => $id_subkegiatan, 'id_renja' => $renja->id_renja],
                    ['indikator_subkegiatan' => $indikator[$i],'target' => $target[$i],'satuan' => $satuan[$i],'is_added' => 0,'is_emptied' => 0,'isDeleted' => 0 ]);
            }
        }

        //input update indikator kegiatan
        if($id_ind_keg){
            $dataindkeg = CascadingSubKegiatan_90::whereNotIn('id_indikator_kegiatan',$id_ind_keg)->where('id_sub_kegiatan',$id_subkegiatan)->where('id_renja',$renja->id_renja)->get();
            if(count($dataindkeg) > 0){
                foreach($dataindkeg as $key => $value){
                    CascadingSubKegiatan_90::where('id_cascading_subkegiatan',$value->id_cascading_subkegiatan)->delete();
                }
            }else{
                for($i=0;$i<count($id_ind_keg);$i++){
                    CascadingSubKegiatan_90::updateOrCreate(['id_sub_kegiatan' => $id_subkegiatan, 'id_renja' => $renja->id_renja, 'id_indikator_kegiatan' => $id_ind_keg[$i]]);
                }
            }    
        }else{
            CascadingSubKegiatan_90::where('id_sub_kegiatan',$id_subkegiatan)->where('id_renja',$renja->id_renja)->delete();
        }

        //relasi subkegiatan
        if($realsi_sub){
            for($i=0;$i<count($realsi_sub);$i++){
                if($realsi_sub[$i] == 'SPM'){
                    LogRenja_90::where('id_sub_kegiatan',$id_subkegiatan)->where('id_renja',$renja->id_renja)->update(['isSPM' => 1]);        
                }else if($realsi_sub[$i] == 'SDGS'){
                    LogRenja_90::where('id_sub_kegiatan',$id_subkegiatan)->where('id_renja',$renja->id_renja)->update(['isSDGS' => 1]);        
                }
            }    
        }else{
            LogRenja_90::where('id_sub_kegiatan',$id_subkegiatan)->where('id_renja',$renja->id_renja)->update(['isSPM' => 0,'isSDGS' => 0]);
        }

        //delete indikator
        if($delete_indsubkeg[0]){
            $deleted = json_decode($delete_indsubkeg[0]);
            if(count($deleted) > 0){
                for($i=0;$i<count($deleted);$i++){
                    IndikatorSubKegiatan_90::where('id_indikator_subkegiatan',$deleted[$i])->delete();
                } 
            }
        }

        return response()->json([
            'kode' => 200,
            'status' => 'sukses',
            'message' => 'Data Indikator Kegiatan Berhasil Disimpan.',
        ]);
    }
    
    //verifikasi indikator kegiatan 
    public function doVerIndKeg(Request $request)
    {
        $arrayStat = array();
        foreach($request->multiselectVerIndKeg as $id_indikator_kegiatan)
        {
            $data = VerifikasiIndikatorKegiatan_90::where('id_indikator_kegiatan',$id_indikator_kegiatan)->firstOrFail();
            if(is_null($data))
            {
                $data = new VerifikasiIndikatorKegiatan_90();
            }

            $data->status_verifikasi = $request->statusVerIndKeg;
            $data->verifikator = $request->verifikator_ind_keg;
            $data->catatan = $request->catatan_ver_ind_keg;
            $data->id_indikator_kegiatan = $id_indikator_kegiatan;

            $data->timestamps = false;    
                
            if($data->save())
            {
                $arrayStat[] = array('status' => 'berhasil', 'id_indikator_kegiatan' => $id_indikator_kegiatan);
            }
        }

        return response()->json([
            'data' => $arrayStat,
            'kode' => 200,
            'status' => 'sukses',
            'message' => 'Data Verifikasi Indikator Kegiatan Berhasil Disimpan.',
        ]);
    }
    
    //verifikasi indikator sub kegiatan 
    public function doVerIndSubKeg(Request $request)
    {
        $arrayStat = array();
        foreach($request->multiselectVerIndSubKeg as $id_indikator_subkegiatan)
        {
            $data = VerifikasiIndikatorSubKegiatan_90::where('id_indikator_subkegiatan',$id_indikator_subkegiatan)->first();
            if(is_null($data))
            {
                $data = new VerifikasiIndikatorSubKegiatan_90();
            }

            $data->status_verifikasi = $request->statusVerIndSubKeg;
            $data->verifikator = $request->verifikator_ind_subkeg;
            $data->catatan = $request->catatan_ver_ind_subkeg;
            $data->id_indikator_subkegiatan = $id_indikator_subkegiatan;

            $data->timestamps = false;    
                
            if($data->save())
            {
                $arrayStat[] = array('status' => 'berhasil', 'id_indikator_subkegiatan' => $id_indikator_subkegiatan);
            }
        }

        return response()->json([
            'data' => $arrayStat,
            'kode' => 200,
            'status' => 'sukses',
            'message' => 'Data Verifikasi Sub Indikator Kegiatan Berhasil Disimpan.',
        ]);
    }
}

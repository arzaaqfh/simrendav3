<?php

namespace App\Http\Controllers;
use \stdClass;
use Session;
use App\Http\Resources\Api\API_RKPDResource;
use App\Models\Viewuser;
use Illuminate\Support\Facades\DB;

// IKU, IKK, IKP Lama
use App\Models\AsistensiRenja;
use App\Models\Bidang;
use App\Models\Data_IKK_Excel;
use App\Models\JenisUrusan;
use App\Models\KategoriUrusan;
use App\Models\Kegiatan;
use App\Models\Lima_IndikatorProgram;
use App\Models\Lima_IndikatorRPJMD;
use App\Models\Lima_IndikatorSasaranRPJMD;
use App\Models\Lima_KebijakanRenstra;
use App\Models\Lima_KegiatanRenstra;
use App\Models\Lima_MisiRPJMD;
use App\Models\Lima_Periode;
use App\Models\Lima_ProgramRPJMD;
use App\Models\Lima_ProgramSKPD;
use App\Models\Lima_Renstra;
use App\Models\Lima_RPJMD;
use App\Models\Lima_SasaranRPJMD;
use App\Models\Lima_TujuanRPJMD;
use App\Models\LimaP_IndikatorProgramRPJMD;
use App\Models\LimaP_IndikatorSasaranRPJMD;
use App\Models\Log_R_MonevRKPD;
use App\Models\LogRenja;
use App\Models\MonevRKPDDetail;
use App\Models\MonevRKPDHead;
use App\Models\PrioritasRKPD;
use App\Models\Program;
use App\Models\R_IKK;
use App\Models\R_IKU;
use App\Models\Renja;
use App\Models\SasaranPrioritasRKPD;
use App\Models\SKPD;
use App\Models\StatusCapaian;
use App\Models\Urusan;

// IKU, IKK, IKP Baru
use App\Models\KetercapaianRPJMD;
use App\Models\KategoriRPJMD;
use App\Models\SatuanRPJMD;
use App\Models\Periode;
use App\Models\Rumus;
use App\Models\RPJMD;
use App\Models\MisiRPJMD;
use App\Models\TujuanRPJMD;
use App\Models\IndikatorTujuanRPJMD;
use App\Models\SasaranRPJMD;
use App\Models\IndikatorSasaranRPJMD;
use App\Models\StrategiRPJMD;
use App\Models\UrusanRPJMD;
use App\Models\ProgramRPJMD;
use App\Models\IndikatorProgramRPJMD;
use App\Models\Aspek;
use App\Models\IndikatorKinerjaRPJMD;
use App\Models\SumberData;
use App\Models\TargetRealisasiTujuanIKU;
use App\Models\TargetRealisasiSasaranIKU;
use App\Models\TargetRealisasiIKK;
use App\Models\TargetRealisasiIKP;
use App\Models\FaktorPendorongIKP;
use App\Models\FaktorPenghambatIKP;
use App\Models\TindakLanjutIKP;
use App\Models\FaktorPendorongTujuanIKU;
use App\Models\FaktorPenghambatTujuanIKU;
use App\Models\TindakLanjutTujuanIKU;
use App\Models\FaktorPendorongSasaranIKU;
use App\Models\FaktorPenghambatSasaranIKU;
use App\Models\TindakLanjutSasaranIKU;
use App\Models\Tahun;
use App\Models\SKPD_90;

use Illuminate\Http\Request;

class RKPD_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }
    private $id_skpd;
    private $dataSKPD;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Data IKU
        $misi = Lima_MisiRPJMD::get();
        $iku = null;
        foreach($misi as $data)
        {
            foreach($data->tujuanRPJMD as $dataTujuan)
            {
                foreach($dataTujuan->sasaranRPJMD as $dataSasaran)
                {
                    $iku[$data->id_misi_rpjmd][] = Lima_IndikatorSasaranRPJMD::where('id_sasaran_rpjmd','=',$dataSasaran->id_sasaran_rpjmd)->get();
                }
            }
        }

        // Data IKK
        $ikk = TargetRealisasiIKK::get();

        // Data IKP
        $ikp = TargetRealisasiIKP::get();

        $skpd = SKPD_90::get();

        // Rekapitulasi Input Realisasi IKP dan IKK
        $ikpBelum = null; // jumlah realisasi ikp yang belum diinput
        $ikkBelum = null; // jumlah realisasi ikk yang belum diinput
        $jmlIKP = null; // jumlah indikator program
        $jmlIKK = null; // jumlah indikator kinerja

        $this->id_skpd = Session::get('id_skpd');

        foreach($skpd as $data)
        {
            $this->dataSKPD = $data->id_skpd;
            $hitungIKP = TargetRealisasiIKP::where('realisasi','=','-')
                                        ->where('id_tahun','=',7)
                                        ->whereHas('IndikatorProgramRPJMD', function($q)
                                        {
                                            $q->where('id_skpd', '=',$this->dataSKPD);
                                
                                        })->get();
            $jmlIKP[$this->dataSKPD] = count(TargetRealisasiIKP::where('id_tahun','=',7)
                                        ->whereHas('IndikatorProgramRPJMD', function($q)
                                        {
                                            $q->where('id_skpd', '=',$this->dataSKPD);
                                
                                        })->get());

            $hitungIKK = TargetRealisasiIKK::where('realisasi','=','-')
                                        ->where('id_tahun','=',7)
                                        ->whereHas('IndikatorKinerjaRPJMD', function($q)
                                        {
                                            $q->where('id_skpd', '=',$this->dataSKPD);
                                
                                        })->get();
                                        
            $jmlIKK[$this->dataSKPD] = count(TargetRealisasiIKK::where('id_tahun','=',7)
                                        ->whereHas('IndikatorKinerjaRPJMD', function($q)
                                        {
                                            $q->where('id_skpd', '=',$this->dataSKPD);
                                
                                        })->get());

            $ikpBelum[$this->dataSKPD] = count($hitungIKP);
            $ikkBelum[$this->dataSKPD] = count($hitungIKK);
        }

        $jmlKtpIKP = array(
            'tercapai'          => count(TargetRealisasiIKP::where([['id_ketercapaian','=',2],['id_tahun','=',8]])->get()),
            'tidak_tercapai'    => count(TargetRealisasiIKP::where([['id_ketercapaian','=',3],['id_tahun','=',8]])->get()),
            'melebihi_capaian'  => count(TargetRealisasiIKP::where([['id_ketercapaian','=',4],['id_tahun','=',8]])->get()),
            'tidak_ada_data'    => count(TargetRealisasiIKP::where([['id_ketercapaian','=',5],['id_tahun','=',8]])->get())
        ); // jumlah ketercapaian IKP

        $jmlKtpIKK = array(
            'tercapai'          => count(TargetRealisasiIKK::where([['id_ketercapaian','=',2],['id_tahun','=',8]])->get()),
            'tidak_tercapai'    => count(TargetRealisasiIKK::where([['id_ketercapaian','=',3],['id_tahun','=',8]])->get()),
            'melebihi_capaian'  => count(TargetRealisasiIKK::where([['id_ketercapaian','=',4],['id_tahun','=',8]])->get()),
            'tidak_ada_data'    => count(TargetRealisasiIKK::where([['id_ketercapaian','=',5],['id_tahun','=',8]])->get())
        );  // jumlah ketercapaian IKK

        // Ketercapaian IKU Tujuan
        $jmlKtpIKUTujuan = array(
            '2022' => array(
                'tercapai'          => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',2],['id_tahun','=',8]])->get()),
                'tidak_tercapai'    => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',3],['id_tahun','=',8]])->get()),
                'melebihi_capaian'  => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',4],['id_tahun','=',8]])->get()),
                'tidak_ada_data'    => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',5],['id_tahun','=',8]])->get())
            ),
            '2021' => array(
                'tercapai'          => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',2],['id_tahun','=',7]])->get()),
                'tidak_tercapai'    => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',3],['id_tahun','=',7]])->get()),
                'melebihi_capaian'  => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',4],['id_tahun','=',7]])->get()),
                'tidak_ada_data'    => count(TargetRealisasiTujuanIKU::where([['id_ketercapaian','=',5],['id_tahun','=',7]])->get())
            ),
        ); 
        return view(
                'monev/rkpd/dashboard',
                compact(
                    'iku',
                    'ikk',
                    'ikp',
                    'misi',
                    'skpd',
                    'ikpBelum',
                    'ikkBelum',
                    'jmlIKP',
                    'jmlIKK',
                    'jmlKtpIKP',
                    'jmlKtpIKK',
                    'jmlKtpIKUTujuan'
                )
            );
    }
    // Monev Hasil IKU Tujuan
    public function monevHasilIKUTujuan()
    {
        $ikuTujuan =  TargetRealisasiTujuanIKU::get();

        return view('monev/rkpd/monevHasilIKUTujuan',compact('ikuTujuan'));
    }

    // Input Monev IKU Tujuan (view)
    public function inputMonevHasilIKUTujuan($id)
    {
        $ketercapaian = KetercapaianRPJMD::get();
        $kategori = KategoriRPJMD::get();
        $sumberData = SumberData::get();
        
        $faktorPD = null;
        $faktorPH = null;
        $tindakLJ = null;
        $iku = null;
        
        $ikuTujuan = TargetRealisasiTujuanIKU::where('id_target_realisasi',$id)->get();            
        $faktorPD = FaktorPendorongTujuanIKU::where('id_target_realisasi',$id)->get();
        $faktorPH = FaktorPenghambatTujuanIKU::where('id_target_realisasi',$id)->get();
        $tindakLJ = TindakLanjutTujuanIKU::where('id_target_realisasi',$id)->get();
        
        return view('monev/rkpd/inputIKUTujuan', compact('ikuTujuan','ketercapaian','kategori','sumberData','faktorPD','faktorPH','tindakLJ'));
    }
    // Update Monev IKU Tujuan (process)
    public function updateMonevHasilIKUTujuan(Request $request)
    {
        $iku = null;
        
        $iku = TargetRealisasiTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

        $hitungFaktorPendorong = count(FaktorPendorongTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPendorong > 0)
        {
            $faktorPendorong = FaktorPendorongTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();
            $faktorPendorong->dana = $request->inputDanaFP;
            $faktorPendorong->sdm = $request->inputSDMFP;
            $faktorPendorong->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $faktorPendorong->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $faktorPendorong->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $faktorPendorong->perijinan = $request->inputPerijinanFP;
            $faktorPendorong->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $faktorPendorong->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $faktorPendorong->faktor_alam = $request->inputFaktorAlamFP;
            
            $faktorPendorong->timestamps = false;
            $faktorPendorong->save();
        }else{
            $putFPd = new FaktorPendorongTujuanIKU();

            $putFPd->dana = $request->inputDanaFP;
            $putFPd->sdm = $request->inputSDMFP;
            $putFPd->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $putFPd->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $putFPd->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $putFPd->perijinan = $request->inputPerijinanFP;
            $putFPd->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $putFPd->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $putFPd->faktor_alam = $request->inputFaktorAlamFP;
            $putFPd->id_target_realisasi = $request->id_target_realisasi;

            $putFPd->timestamps = false;
            $putFPd->save();
        }

        $hitungFaktorPenghambat = count(FaktorPenghambatTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPenghambat)
        {
            $faktorPenghambat = FaktorPenghambatTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $faktorPenghambat->dana = $request->inputDanaFPH;
            $faktorPenghambat->sdm = $request->inputSDMFPH;
            $faktorPenghambat->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $faktorPenghambat->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $faktorPenghambat->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $faktorPenghambat->perijinan = $request->inputPerijinanFPH;
            $faktorPenghambat->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $faktorPenghambat->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $faktorPenghambat->faktor_alam = $request->inputFaktorAlamFPH;
            
            $faktorPenghambat->timestamps = false;
            $faktorPenghambat->save();
        }else{
            $putFPh = new FaktorPenghambatTujuanIKU();

            $putFPh->dana = $request->inputDanaFPH;
            $putFPh->sdm = $request->inputSDMFPH;
            $putFPh->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $putFPh->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $putFPh->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $putFPh->perijinan = $request->inputPerijinanFPH;
            $putFPh->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $putFPh->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $putFPh->faktor_alam = $request->inputFaktorAlamFPH;
            $putFPh->id_target_realisasi = $request->id_target_realisasi;

            $putFPh->timestamps = false;
            $putFPh->save();
        }

        $hitungTindakLanjut = count(TindakLanjutTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungTindakLanjut)
        {
            $tindakLanjut = TindakLanjutTujuanIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $tindakLanjut->dana = $request->inputDanaTL;
            $tindakLanjut->sdm = $request->inputSDMTL;
            $tindakLanjut->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $tindakLanjut->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $tindakLanjut->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $tindakLanjut->perijinan = $request->inputPerijinanTL;
            $tindakLanjut->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $tindakLanjut->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $tindakLanjut->faktor_alam = $request->inputFaktorAlamTL;
            
            $tindakLanjut->timestamps = false;
            $tindakLanjut->save();
        }else{
            $putTL = new TindakLanjutTujuanIKU();

            $putTL->dana = $request->inputDanaTL;
            $putTL->sdm = $request->inputSDMTL;
            $putTL->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $putTL->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $putTL->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $putTL->perijinan = $request->inputPerijinanTL;
            $putTL->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $putTL->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $putTL->faktor_alam = $request->inputFaktorAlamTL;
            $putTL->id_target_realisasi = $request->id_target_realisasi;

            $putTL->timestamps = false;
            $putTL->save();
        }
        
        $iku->realisasi = $request->realisasi;
        $iku->keterangan = $request->keterangan;
        $iku->id_ketercapaian = $request->ketercapaian;
        $iku->id_kategori = $request->kategori;

        if($request->sumberDataLainnya != "")
        {
            $putSumberData = new SumberData();
            $putSumberData->sumber_data = $request->sumberDataLainnya;
            $putSumberData->timestamps = false;
            $putSumberData->save();

            $sData = SumberData::where('sumber_data',$request->sumberDataLainnya)->get();
            foreach($sData as $dataSD)
            {
                $iku->id_sumber_data = $dataSD->id_sumber_data;
            }
        }else{
            $iku->id_sumber_data = $request->sumberData;
        }

        $iku->timestamps = false;
        $iku->save();
        
        return redirect('monev/rkpd/ikuTujuan');
    }    

    // Monev Hasil IKU Sasaran
    public function monevHasilIKUSasaran()
    {
        $ikuSasaran =  TargetRealisasiSasaranIKU::get();

        return view('monev/rkpd/monevHasilIKUSasaran',compact('ikuSasaran'));
    }

    // Input Monev IKU Sasaran (view)
    public function inputMonevHasilIKUSasaran($id)
    {
        $ketercapaian = KetercapaianRPJMD::get();
        $kategori = KategoriRPJMD::get();
        $sumberData = SumberData::get();
        
        $faktorPD = null;
        $faktorPH = null;
        $tindakLJ = null;
        $iku = null;
        
        $ikuSasaran = TargetRealisasiSasaranIKU::where('id_target_realisasi',$id)->get();            
        $faktorPD = FaktorPendorongSasaranIKU::where('id_target_realisasi',$id)->get();
        $faktorPH = FaktorPenghambatSasaranIKU::where('id_target_realisasi',$id)->get();
        $tindakLJ = TindakLanjutSasaranIKU::where('id_target_realisasi',$id)->get();
        
        return view('monev/rkpd/inputIKUSasaran', compact('ikuSasaran','ketercapaian','kategori','sumberData','faktorPD','faktorPH','tindakLJ'));
    }
    // Update Monev IKU Sasaran (process)
    public function updateMonevHasilIKUSasaran(Request $request)
    {
        $iku = null;
        
        $iku = TargetRealisasiSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

        $hitungFaktorPendorong = count(FaktorPendorongSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPendorong > 0)
        {
            $faktorPendorong = FaktorPendorongSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();
            $faktorPendorong->dana = $request->inputDanaFP;
            $faktorPendorong->sdm = $request->inputSDMFP;
            $faktorPendorong->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $faktorPendorong->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $faktorPendorong->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $faktorPendorong->perijinan = $request->inputPerijinanFP;
            $faktorPendorong->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $faktorPendorong->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $faktorPendorong->faktor_alam = $request->inputFaktorAlamFP;
            
            $faktorPendorong->timestamps = false;
            $faktorPendorong->save();
        }else{
            $putFPd = new FaktorPendorongSasaranIKU();

            $putFPd->dana = $request->inputDanaFP;
            $putFPd->sdm = $request->inputSDMFP;
            $putFPd->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $putFPd->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $putFPd->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $putFPd->perijinan = $request->inputPerijinanFP;
            $putFPd->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $putFPd->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $putFPd->faktor_alam = $request->inputFaktorAlamFP;
            $putFPd->id_target_realisasi = $request->id_target_realisasi;

            $putFPd->timestamps = false;
            $putFPd->save();
        }

        $hitungFaktorPenghambat = count(FaktorPenghambatSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPenghambat)
        {
            $faktorPenghambat = FaktorPenghambatSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $faktorPenghambat->dana = $request->inputDanaFPH;
            $faktorPenghambat->sdm = $request->inputSDMFPH;
            $faktorPenghambat->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $faktorPenghambat->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $faktorPenghambat->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $faktorPenghambat->perijinan = $request->inputPerijinanFPH;
            $faktorPenghambat->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $faktorPenghambat->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $faktorPenghambat->faktor_alam = $request->inputFaktorAlamFPH;
            
            $faktorPenghambat->timestamps = false;
            $faktorPenghambat->save();
        }else{
            $putFPh = new FaktorPenghambatSasaranIKU();

            $putFPh->dana = $request->inputDanaFPH;
            $putFPh->sdm = $request->inputSDMFPH;
            $putFPh->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $putFPh->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $putFPh->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $putFPh->perijinan = $request->inputPerijinanFPH;
            $putFPh->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $putFPh->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $putFPh->faktor_alam = $request->inputFaktorAlamFPH;
            $putFPh->id_target_realisasi = $request->id_target_realisasi;

            $putFPh->timestamps = false;
            $putFPh->save();
        }

        $hitungTindakLanjut = count(TindakLanjutSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungTindakLanjut)
        {
            $tindakLanjut = TindakLanjutSasaranIKU::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $tindakLanjut->dana = $request->inputDanaTL;
            $tindakLanjut->sdm = $request->inputSDMTL;
            $tindakLanjut->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $tindakLanjut->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $tindakLanjut->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $tindakLanjut->perijinan = $request->inputPerijinanTL;
            $tindakLanjut->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $tindakLanjut->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $tindakLanjut->faktor_alam = $request->inputFaktorAlamTL;
            
            $tindakLanjut->timestamps = false;
            $tindakLanjut->save();
        }else{
            $putTL = new TindakLanjutSasaranIKU();

            $putTL->dana = $request->inputDanaTL;
            $putTL->sdm = $request->inputSDMTL;
            $putTL->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $putTL->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $putTL->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $putTL->perijinan = $request->inputPerijinanTL;
            $putTL->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $putTL->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $putTL->faktor_alam = $request->inputFaktorAlamTL;
            $putTL->id_target_realisasi = $request->id_target_realisasi;

            $putTL->timestamps = false;
            $putTL->save();
        }
        
        $iku->realisasi = $request->realisasi;
        $iku->keterangan = $request->keterangan;
        $iku->id_ketercapaian = $request->ketercapaian;
        $iku->id_kategori = $request->kategori;

        if($request->sumberDataLainnya != "")
        {
            $putSumberData = new SumberData();
            $putSumberData->sumber_data = $request->sumberDataLainnya;
            $putSumberData->timestamps = false;
            $putSumberData->save();

            $sData = SumberData::where('sumber_data',$request->sumberDataLainnya)->get();
            foreach($sData as $dataSD)
            {
                $iku->id_sumber_data = $dataSD->id_sumber_data;
            }
        }else{
            $iku->id_sumber_data = $request->sumberData;
        }

        $iku->timestamps = false;
        $iku->save();
        
        return redirect('monev/rkpd/ikuSasaran');
    }   

    // Monev Hasil IKK
    public function monevHasilIKK(Request $request)
    {
        return view('monev/rkpd/monevHasilIKK');
    }

    // InputMonevIKK
    public function inputMonevHasilIKK($id)
    {
        $ikk = TargetRealisasiIKK::where('id_target_realisasi',$id)->get(); 
        $ketercapaian = KetercapaianRPJMD::get();
        $kategori = KategoriRPJMD::get();
        $sumberData = SumberData::get();

        return view('monev/rkpd/inputIKK', compact('ikk','ketercapaian','kategori','sumberData'));
    }
    // Update Monev IKK (process)
    public function updateMonevHasilIKK(Request $request)
    {
        $ikk = TargetRealisasiIKK::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();
        
        $ikk->realisasi = $request->realisasi;
        $ikk->keterangan = $request->keterangan;
        $ikk->id_ketercapaian = $request->ketercapaian;
        $ikk->id_kategori = $request->kategori;
        
        if($request->sumberDataLainnya != "")
        {
            $putSumberData = new SumberData();
            $putSumberData->sumber_data = $request->sumberDataLainnya;
            $putSumberData->timestamps = false;
            $putSumberData->save();

            $sData = SumberData::where('sumber_data',$request->sumberDataLainnya)->get();
            foreach($sData as $dataSD)
            {
                $ikk->id_sumber_data = $dataSD->id_sumber_data;
            }
        }else{
            $ikk->id_sumber_data = $request->sumberData;
        }
        
        $ikk->timestamps = false;
        $ikk->save();

        return redirect('monev/rkpd/ikk');
    }

    // Monev Hasil IKP
    public function monevHasilIKP(Request $request)
    {
        return view('monev/rkpd/monevHasilIKP');
    }
    // InputMonevIKP
    public function inputmonevHasilIKP($id)
    {
        $ikp = TargetRealisasiIKP::where('id_target_realisasi',$id)->get();
        $ketercapaian = KetercapaianRPJMD::get();
        $kategori = KategoriRPJMD::get();
        $sumberData = SumberData::get();
        $faktorPD = FaktorPendorongIKP::where('id_target_realisasi',$id)->get();
        $faktorPH = FaktorPenghambatIKP::where('id_target_realisasi',$id)->get();
        $tindakLJ = TindakLanjutIKP::where('id_target_realisasi',$id)->get();

        return view('monev/rkpd/inputIKP',compact('ikp','ketercapaian','kategori','sumberData','faktorPD','faktorPH','tindakLJ'));
    }
    // Update Monev IKP (process)
    public function updateMonevHasilIKP(Request $request)
    {
        $ikp = TargetRealisasiIKP::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();
        
        $ikp->realisasi = $request->realisasi;
        $ikp->formula = $request->formula;
        $ikp->id_ketercapaian = $request->ketercapaian;
        $ikp->id_kategori = $request->kategori;
        
        if($request->sumberDataLainnya != "")
        {
            $putSumberData = new SumberData();
            $putSumberData->sumber_data = $request->sumberDataLainnya;
            $putSumberData->timestamps = false;
            $putSumberData->save();

            $sData = SumberData::where('sumber_data',$request->sumberDataLainnya)->get();
            foreach($sData as $dataSD)
            {
                $ikp->id_sumber_data = $dataSD->id_sumber_data;
            }
        }else{
            $ikp->id_sumber_data = $request->sumberData;
        }
        
        $ikp->timestamps = false;
        $ikp->save();        
        
        $hitungFaktorPendorong = count(FaktorPendorongIKP::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPendorong > 0)
        {
            $faktorPendorong = FaktorPendorongIKP::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();
            $faktorPendorong->dana = $request->inputDanaFP;
            $faktorPendorong->sdm = $request->inputSDMFP;
            $faktorPendorong->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $faktorPendorong->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $faktorPendorong->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $faktorPendorong->perijinan = $request->inputPerijinanFP;
            $faktorPendorong->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $faktorPendorong->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $faktorPendorong->faktor_alam = $request->inputFaktorAlamFP;
            
            $faktorPendorong->timestamps = false;
            $faktorPendorong->save();
        }else{
            $putFPd = new FaktorPendorongIKP();

            $putFPd->dana = $request->inputDanaFP;
            $putFPd->sdm = $request->inputSDMFP;
            $putFPd->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFP;
            $putFPd->peraturan_perundangan = $request->inputPeraturanPerundanganFP;
            $putFPd->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFP;
            $putFPd->perijinan = $request->inputPerijinanFP;
            $putFPd->ketersediaan_lahan = $request->inputKetersediaanLahanFP;
            $putFPd->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFP;
            $putFPd->faktor_alam = $request->inputFaktorAlamFP;
            $putFPd->id_target_realisasi = $request->id_target_realisasi;

            $putFPd->timestamps = false;
            $putFPd->save();
        }

        $hitungFaktorPenghambat = count(FaktorPenghambatIKP::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungFaktorPenghambat)
        {
            $faktorPenghambat = FaktorPenghambatIKP::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $faktorPenghambat->dana = $request->inputDanaFPH;
            $faktorPenghambat->sdm = $request->inputSDMFPH;
            $faktorPenghambat->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $faktorPenghambat->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $faktorPenghambat->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $faktorPenghambat->perijinan = $request->inputPerijinanFPH;
            $faktorPenghambat->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $faktorPenghambat->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $faktorPenghambat->faktor_alam = $request->inputFaktorAlamFPH;
            
            $faktorPenghambat->timestamps = false;
            $faktorPenghambat->save();
        }else{
            $putFPh = new FaktorPenghambatIKP();

            $putFPh->dana = $request->inputDanaFPH;
            $putFPh->sdm = $request->inputSDMFPH;
            $putFPh->waktu_pelaksanaan = $request->inputWaktuPelaksanaanFPH;
            $putFPh->peraturan_perundangan = $request->inputPeraturanPerundanganFPH;
            $putFPh->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaFPH;
            $putFPh->perijinan = $request->inputPerijinanFPH;
            $putFPh->ketersediaan_lahan = $request->inputKetersediaanLahanFPH;
            $putFPh->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatFPH;
            $putFPh->faktor_alam = $request->inputFaktorAlamFPH;
            $putFPh->id_target_realisasi = $request->id_target_realisasi;

            $putFPh->timestamps = false;
            $putFPh->save();
        }

        $hitungTindakLanjut = count(TindakLanjutIKP::where('id_target_realisasi',$request->id_target_realisasi)->get());
        if($hitungTindakLanjut)
        {
            $tindakLanjut = TindakLanjutIKP::where('id_target_realisasi',$request->id_target_realisasi)->firstOrFail();

            $tindakLanjut->dana = $request->inputDanaTL;
            $tindakLanjut->sdm = $request->inputSDMTL;
            $tindakLanjut->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $tindakLanjut->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $tindakLanjut->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $tindakLanjut->perijinan = $request->inputPerijinanTL;
            $tindakLanjut->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $tindakLanjut->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $tindakLanjut->faktor_alam = $request->inputFaktorAlamTL;
            
            $tindakLanjut->timestamps = false;
            $tindakLanjut->save();
        }else{
            $putTL = new TindakLanjutIKP();

            $putTL->dana = $request->inputDanaTL;
            $putTL->sdm = $request->inputSDMTL;
            $putTL->waktu_pelaksanaan = $request->inputWaktuPelaksanaanTL;
            $putTL->peraturan_perundangan = $request->inputPeraturanPerundanganTL;
            $putTL->sistem_pengadaan_barang_jasa = $request->inputSistemPengadaanBarangJasaTL;
            $putTL->perijinan = $request->inputPerijinanTL;
            $putTL->ketersediaan_lahan = $request->inputKetersediaanLahanTL;
            $putTL->kesiapan_dukungan_masyarakat = $request->inputKesiapanDukunganMasyarakatTL;
            $putTL->faktor_alam = $request->inputFaktorAlamTL;
            $putTL->id_target_realisasi = $request->id_target_realisasi;

            $putTL->timestamps = false;
            $putTL->save();
        }
        
        return redirect('monev/rkpd/ikp');
    }
}

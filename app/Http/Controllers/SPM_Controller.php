<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Http\Resources\SPM_Resource;
use App\Http\Controllers\API_SPM_Controller;
use App\Models\SPM_Anggaran;
use App\Models\SPM_Capaian;
use App\Models\SPM_Dasar_Hukum;
use App\Models\SPM_Indikator_Pencapaian;
use App\Models\SPM_Jenis_Layanan;
use App\Models\SPM_Landasan_Hukum;
use App\Models\SPM_Pelayanan;
use App\Models\SPM_Rincian_Jenis_Layanan;
use App\Models\SPM_R_jenisRincian;
use App\Models\Urusan_90;
use App\Models\BidangUrusan_90;
use App\Models\Program_90;
use App\Models\Kegiatan_90;
use App\Models\SPM_Urusan;
use App\Models\Tahun;
use App\Models\IndikatorSubKegiatan_90;

class SPM_Controller extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();        
        return view('spm/dashboard',compact('tahun'));
    }

    public function hitungPersen($jml,$total)
    {
        $hasil = null;
        if(!is_null($total))
        {
            $persen = null;
            if($total == 0)
            {
                $persen = 0;
            }else{
                $persen = ($jml/$total)*100;
            }

            if($persen >= 81 && $persen <= 100)
            {
                $hasil = array(
                    'persen' => number_format($persen,2,'.',''),
                    'text' => 'Baik',
                    'class' => 'btn-success'
                );
            }else if($persen >= 51 && $persen <= 80)
            {
                $hasil = array(
                    'persen' => number_format($persen,2,'.',''),
                    'text' => 'Cukup',
                    'class' => 'btn-warning'
                );
            }else if($persen > 0 && $persen <= 50)
            {
                $hasil = array(
                    'persen' => number_format($persen,2,'.',''),
                    'text' => 'Kurang',
                    'class' => 'btn-danger'
                );
            }else{
                $hasil = array(
                    'persen' => number_format(0,2,'.',''),
                    'text' => 'Tidak Ada',
                    'class' => 'btn-dark'
                ); 
            }
        }else{
            $hasil = array(
                'persen' => number_format(0,2,'.',''),
                'text' => 'Tidak Ada',
                'class' => 'btn-dark'
            ); 
        }

        return $hasil;
    }
    public function penerapan()
    {        
        $data = BidangUrusan_90::select('*')
                                ->where('id_bidang_urusan',1)
                                ->orWhere('id_bidang_urusan',2)
                                ->orWhere('id_bidang_urusan',3)
                                ->orWhere('id_bidang_urusan',4)
                                ->orWhere('id_bidang_urusan',5)
                                ->orWhere('id_bidang_urusan',6)
                                ->get();

        $tahun = Tahun::orderBy('tahun','DESC')
                            ->get();
        if(is_null(session('tahun_spm')))
        {
            $tahun_spm = $tahun;
        }else{
            $tahun_spm = Tahun::where('tahun','=',session('tahun_spm'))->get();
        }                            
        $status = array();
        foreach($data as $bu)
        {
            $status[] = array(
                            'id_bidang_urusan'      => $bu->id_bidang_urusan,
                            'nama_bidang_urusan'    => $bu->nama_bidang_urusan,
                            'statusCpn'             => SPM_Capaian::where('id_bidang_urusan',$bu->id_bidang_urusan)
                                                                    ->where('id_tahun',$tahun_spm[0]->id_tahun)
                                                                    ->find(1),
                            'statusAng'             => SPM_Anggaran::where('id_bidang_urusan',$bu->id_bidang_urusan)
                                                                    ->where('id_tahun',$tahun_spm[0]->id_tahun)
                                                                    ->find(1),
                            'id_tahun'              => $tahun_spm[0]->id_tahun
                        );
        }
        $tI_1 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',1]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',1]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',1]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',1]])->count()
                ); //total input id_bidang_urusan = 1
        $tI_2 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',2]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',2]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',2]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',2]])->count()
                ); //total input id_bidang_urusan = 2
        $tI_3 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',3]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',3]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',3]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',3]])->count()
                ); //total input id_bidang_urusan = 3
        $tI_4 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',4]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',4]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',4]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',4]])->count()
                ); //total input id_bidang_urusan = 4
        $tI_5 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',5]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',5]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',5]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',5]])->count()
                ); //total input id_bidang_urusan = 5
        $tI_6 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',6]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',6]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',6]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['id_bidang_urusan','=',6]])->count()
                ); //total input id_bidang_urusan = 6

        $jI_1 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',1]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',1]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',1]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',1]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 1
        $jI_2 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',2]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',2]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',2]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',2]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 2
        $jI_3 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',3]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',3]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',3]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',3]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 3
        $jI_4 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',4]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',4]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',4]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',4]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 4
        $jI_5 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',5]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',5]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',5]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',5]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 5
        $jI_6 = array(
                    'tw_1' => SPM_Capaian::where([['tw','=',1],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',6]])->count(),
                    'tw_2' => SPM_Capaian::where([['tw','=',2],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',6]])->count(),
                    'tw_3' => SPM_Capaian::where([['tw','=',3],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',6]])->count(),
                    'tw_4' => SPM_Capaian::where([['tw','=',4],['id_tahun','=',$tahun_spm[0]->id_tahun],['jml_tersedia','!=',0],['id_bidang_urusan','=',6]])->count()
                ); //jumlah yg diinput id_bidang_urusan = 6

        $persenInput = array(
            'id_bidang_urusan_1' => array(
                'tw_1' => $this->hitungPersen($jI_1['tw_1'],$tI_1['tw_1']),
                'tw_2' => $this->hitungPersen($jI_1['tw_2'],$tI_1['tw_2']),
                'tw_3' => $this->hitungPersen($jI_1['tw_3'],$tI_1['tw_3']),
                'tw_4' => $this->hitungPersen($jI_1['tw_4'],$tI_1['tw_4'])
            ),
            'id_bidang_urusan_2' => array(
                'tw_1' => $this->hitungPersen($jI_2['tw_1'],$tI_2['tw_1']),
                'tw_2' => $this->hitungPersen($jI_2['tw_2'],$tI_2['tw_2']),
                'tw_3' => $this->hitungPersen($jI_2['tw_3'],$tI_2['tw_3']),
                'tw_4' => $this->hitungPersen($jI_2['tw_4'],$tI_2['tw_4'])
            ),
            'id_bidang_urusan_3' => array(
                'tw_1' => $this->hitungPersen($jI_3['tw_1'],$tI_3['tw_1']),
                'tw_2' => $this->hitungPersen($jI_3['tw_2'],$tI_3['tw_2']),
                'tw_3' => $this->hitungPersen($jI_3['tw_3'],$tI_3['tw_3']),
                'tw_4' => $this->hitungPersen($jI_3['tw_4'],$tI_3['tw_4'])
            ),
            'id_bidang_urusan_4' => array(
                'tw_1' => $this->hitungPersen($jI_4['tw_1'],$tI_4['tw_1']),
                'tw_2' => $this->hitungPersen($jI_4['tw_2'],$tI_4['tw_2']),
                'tw_3' => $this->hitungPersen($jI_4['tw_3'],$tI_4['tw_3']),
                'tw_4' => $this->hitungPersen($jI_4['tw_4'],$tI_4['tw_4'])
            ),
            'id_bidang_urusan_5' => array(
                'tw_1' => $this->hitungPersen($jI_5['tw_1'],$tI_5['tw_1']),
                'tw_2' => $this->hitungPersen($jI_5['tw_2'],$tI_5['tw_2']),
                'tw_3' => $this->hitungPersen($jI_5['tw_3'],$tI_5['tw_3']),
                'tw_4' => $this->hitungPersen($jI_5['tw_4'],$tI_5['tw_4'])
            ),
            'id_bidang_urusan_6' => array(
                'tw_1' => $this->hitungPersen($jI_6['tw_1'],$tI_6['tw_1']),
                'tw_2' => $this->hitungPersen($jI_6['tw_2'],$tI_6['tw_2']),
                'tw_3' => $this->hitungPersen($jI_6['tw_3'],$tI_6['tw_3']),
                'tw_4' => $this->hitungPersen($jI_6['tw_4'],$tI_6['tw_4'])
            )
        );

        return view('spm/penerapan', compact(
            'data',
            'tahun',
            'status',
            'persenInput'
        ));
    }

    public function jenisLayanan()
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();   
        $data = SPM_Jenis_Layanan::get();
        return view('spm/jenisLayanan', compact('data','tahun'));
    }

    public function rincianJenisLayanan()
    {    
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();      
        $data = SPM_Rincian_Jenis_Layanan::get();
        return view('spm/rincianJenisLayanan', compact('data','tahun'));
    }

    public function indikatorPencapaian()
    {     
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();     
        $data = SPM_Indikator_Pencapaian::get();
        return view('spm/indikatorPencapaian', compact('data','tahun'));
    }
    
    public function dasarHukum()
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();  
        $data = SPM_Dasar_Hukum::get();
        return view('spm/dasarHukum', compact('data','tahun'));
    }

    public function landasanHukum()
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();  
        $data = SPM_Landasan_Hukum::get();
        return view('spm/landasanHukum', compact('data','tahun'));
    }
    
    public function relasiJenisRincian()
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();  
        $data = SPM_R_jenisrincian::get();
        return view('spm/relasiJenisRincian', compact('data','tahun'));
    }

    public function detailSPM($id_bidang_urusan, $id_tahun, $tw)
    {
        $tahun = Tahun::orderBy('tahun','DESC')
                        ->get();
                         
        $capaian = SPM_Capaian::select('id_jenis_layanan')
                                ->where([
                                    ['id_bidang_urusan', '=', $id_bidang_urusan],
                                    ['id_tahun', '=', $id_tahun],
                                    ['tw', '=', $tw]
                                ])
                                ->groupBy('id_jenis_layanan')
                                ->get();
        $dataCpn = null;
        $prsIP = 0;
        if(count($capaian)>0)
        {
            foreach($capaian as $cpn)
            {
                $dataCpn[$cpn->jenisLayanan->nama_jenis][] = array(
                                'pelayanan' => SPM_Pelayanan::where([
                                                    ['id_bidang_urusan', '=', $id_bidang_urusan],
                                                    ['id_jenis_layanan', '=', $cpn->id_jenis_layanan],
                                                    ['id_tahun', '=', $id_tahun],
                                                    ['tw', '=', $tw]
                                                ])->get(),
                                'capaian' => SPM_Capaian::where([
                                                    ['id_bidang_urusan', '=', $id_bidang_urusan],
                                                    ['id_jenis_layanan', '=', $cpn->id_jenis_layanan],
                                                    ['id_tahun', '=', $id_tahun],
                                                    ['tw', '=', $tw]
                                                ])->get()           
                            );
            }

            foreach($dataCpn as $key => $dat)
            {
                // Persentase Pelayanan
                $prsPly = 0;
                $prsPly80 = 0;
                foreach($dat[0]['pelayanan'] as $ply)
                {
                    $prsPly = ($ply->total_terlayani/$ply->total_dilayani)*100;
                    $prsPly80 = ($prsPly*80)/100;
                }
                $dataCpn[$key]['prsPly'] = $prsPly;
                $dataCpn[$key]['prsPly80'] = $prsPly80;
                // Persentase Mutu 
                $prsMutu = 0;
                $prsMutu20 = 0;
                foreach($dat[0]['capaian'] as $cpn)
                {
                    $prsMutu = $prsMutu + (($cpn->jml_tersedia/$cpn->jml_dibutuhkan)*100);
                }
                $prsMutu = $prsMutu/count($dat[0]['capaian']);
                $prsMutu20 = ($prsMutu*20)/100;

                $dataCpn[$key]['prsMutu'] = $prsMutu;
                $dataCpn[$key]['prsMutu20'] = $prsMutu20;

                $dataCpn[$key]['prs8020'] = $prsPly80+$prsMutu20;
                $prsIP = $prsIP+$dataCpn[$key]['prs8020'];
            }
            $prsIP = $prsIP/count($dataCpn);
        }

        // Kategori Indeks Pencapaian
        $ktgCpn = "-";
        $bgKtgCpn = "bg-dark";
        if((int) $prsIP == 100)
        {
            $ktgCpn = "TUNTAS PARIPURNA";
            $bgKtgCpn = "bg-primary";
        }else if((int) $prsIP >= 90 && (int) $prsIP <= 99)
        {
            $ktgCpn = "TUNTAS UTAMA";
            $bgKtgCpn = "bg-success";
        }else if((int) $prsIP >= 80 && (int) $prsIP <= 89)
        {
            $ktgCpn = "TUNTAS MADYA";
            $bgKtgCpn = "bg-info";
        }else if((int) $prsIP >= 70 && (int) $prsIP <= 79)
        {
            $ktgCpn = "TUNTAS PRATAMA";
            $bgKtgCpn = "bg-secondary";
        }else if((int) $prsIP >= 60 && (int) $prsIP <= 69)
        {
            $ktgCpn = "TUNTAS MUDA";
            $bgKtgCpn = "bg-warning";
        }else if((int) $prsIP < 60)
        {
            $ktgCpn = "BELUM TUNTAS";
            $bgKtgCpn = "bg-danger";
        }
        
        $tahunAngKeg = Tahun::select('tahun')->where('id_tahun','=',$id_tahun)->get();
        $anggaran = DB::table('LogRenja_90')
                        ->join('R_LogRenja_90', 'R_LogRenja_90.id_log_renja', '=', 'LogRenja_90.id_log_renja')
                        ->join('SubKegiatan_90', 'SubKegiatan_90.id_sub_kegiatan', '=', 'LogRenja_90.id_sub_kegiatan')
                        ->join('Kegiatan_90', 'Kegiatan_90.id_kegiatan', '=', 'SubKegiatan_90.id_kegiatan')
                        ->join('Program_90', 'Program_90.id_program', '=', 'Kegiatan_90.id_program')
                        ->join('BidangUrusan_90', 'BidangUrusan_90.id_bidang_urusan', '=', 'Program_90.id_bidang_urusan')
                        ->join('Renja_90', 'Renja_90.id_renja', '=', 'LogRenja_90.id_renja')
                        ->where([
                                ['LogRenja_90.isSPM','=', 1],
                                ['Renja_90.periode_usulan', '=', $tahunAngKeg[0]->tahun],
                                ['BidangUrusan_90.id_bidang_urusan', '=', $id_bidang_urusan]
                            ])
                        ->get();
        $dataAngKeg = null;
        foreach($anggaran as $ang)
        {
            $realisasiAnggaran = '-';
            if($tw == 1)
            {
                $realisasiAnggaran = $ang->r_1;
            }else if($tw == 2)
            {
                $realisasiAnggaran = $ang->r_2;
            }else if($tw == 3)
            {
                $realisasiAnggaran = $ang->r_3;
            }else if($tw == 4)
            {
                $realisasiAnggaran = $ang->r_4;
            }

            $dataAngKeg[$ang->nama_program][$ang->nama_kegiatan][] = array(
                'nama_sub_kegiatan' => $ang->nama_sub_kegiatan,
                'anggaran' => $ang->apbd_kota,
                'realisasi_anggaran' => $realisasiAnggaran
            );
        }

        $dataAng = null;
        // $dataAngKeg = null;
        $landasanHukum = SPM_Landasan_Hukum::where('id_bidang_urusan',$id_bidang_urusan)->get();
        $bidangUrusan = BidangUrusan_90::where('id_bidang_urusan',$id_bidang_urusan)->get();
        // echo "<pre>";
        // print_r($dataAngKeg);
        // echo "</pre>";
        return view('spm/detailSPM', compact(
            'dataCpn',
            'dataAng',
            'dataAngKeg',
            'tahun',
            'tw',
            'landasanHukum',
            'bidangUrusan',
            'prsIP',
            'ktgCpn',
            'bgKtgCpn',
            'id_bidang_urusan'
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyJenisLayanan($id)
    {
        $id_jenis_layanan = decrypt($id);
        $jenisLayanan = SPM_Jenis_Layanan::findOrFail($id_jenis_layanan);
        $capaian = SPM_Capaian::where('id_jenis_layanan','=',$id_jenis_layanan)->first();
        
        if($capaian === null){    
            if($jenisLayanan->delete()){
                $stat = "GAGAL";
            }else{
                $stat = "BERHASIL";
            }
        }else{
            $stat = "GAGAL";
        }

        return redirect('spm/jenisLayanan');
    }

    public function destroyRincianJenisLayanan($id)
    {
        $id_rincian = decrypt($id);
        $rincianJenisLayanan = SPM_Rincian_Jenis_Layanan::findOrFail($id_rincian);
        $capaian = SPM_Capaian::where('id_rincian','=',$id_rincian)->first();
        
        if($capaian === null){    
            if($rincianJenisLayanan->delete()){
                $stat = "GAGAL";
            }else{
                $stat = "BERHASIL";
            }
        }else{
            $stat = "GAGAL";
        }

        return redirect('spm/rincianJenisLayanan');
    }

    public function destroyIndikatorPencapaian($id)
    {
        $id_indikator = decrypt($id);
        $indikatorPencapaian = SPM_Indikator_Pencapaian::findOrFail($id_indikator);
        $capaian = SPM_Capaian::where('id_indikator','=',$id_indikator)->first();
        
        if($capaian === null){    
            if($indikatorPencapaian->delete()){
                $stat = "GAGAL";
            }else{
                $stat = "BERHASIL";
            }
        }else{
            $stat = "GAGAL";
        }

        return redirect('spm/indikatorPencapaian');
    }

    public function destroyRelasiJenisRincian($id)
    {
        $id_r_jenisrincian = decrypt($id);
        $relasiJenisRincian = SPM_R_jenisRincian::findOrFail($id_r_jenisrincian);
        
        if($relasiJenisRincian->delete()){
            $stat = "GAGAL";
        }else{
            $stat = "BERHASIL";
        }

        return redirect('spm/relasiJenisRincian');
    }

    // Menambah Data Capaian
    public function addFormCapaian($id_bidang_urusan,$tw,$jmlRincian)
    {
        $jenisLayanan = SPM_Jenis_Layanan::get();
        $rJenisLayanan = SPM_Rincian_Jenis_Layanan::get();
        $tahun = Tahun::first()->orderBy('id_tahun','DESC')->get();
        return view('spm/addFormCapaian',compact('id_bidang_urusan','tw','jenisLayanan','rJenisLayanan','tahun','jmlRincian'));
    }

    public function doAddFormCapaian(Request $request)
    {
        if(!is_null($request))
        {
            if($request->jenisData == 0)
            {
                $trantibum = 0;
                $kebencanaan = 0;
                $damkar = 0;
            }else if($request->jenisData == 1)
            {
                $trantibum = 1;
                $kebencanaan = 0;
                $damkar = 0;
            }else if($request->jenisData == 2)
            {
                $trantibum = 0;
                $kebencanaan = 1;
                $damkar = 0;
            }else if($request->jenisData == 3)
            {
                $trantibum = 0;
                $kebencanaan = 0;
                $damkar = 1;
            }else{
                $trantibum = 0;
                $kebencanaan = 0;
                $damkar = 0;
            }

            $pelayanan = new SPM_Pelayanan;
            $pelayanan->total_dilayani = $request->total_dilayani;
            $pelayanan->total_terlayani = $request->total_terlayani;
            $pelayanan->total_belum_terlayani = ($request->total_dilayani-$request->total_terlayani);
            $pelayanan->id_bidang_urusan = $request->id_bidang_urusan;
            $pelayanan->id_jenis_layanan = $request->jenisLayanan;
            $pelayanan->id_tahun = $request->tahun;
            $pelayanan->tw = $request->tw;
            $pelayanan->trantibum = $trantibum;
            $pelayanan->kebencanaan = $kebencanaan;
            $pelayanan->damkar = $damkar;
            $pelayanan->save();
            
            $i=0;
            foreach($request->rJenisLayanan as $rjl)
            {
                $capaian = new SPM_Capaian;
                $capaian->jml_dibutuhkan = $request->jml_dibutuhkan[$i];
                $capaian->jml_tersedia = $request->jml_tersedia[$i];
                $capaian->jml_blm_tersedia = ($request->jml_dibutuhkan[$i]-$request->jml_tersedia[$i]);
                $capaian->persentase_capaian = ($request->jml_tersedia[$i]/$request->jml_dibutuhkan[$i])*100;
                $capaian->id_bidang_urusan = $request->id_bidang_urusan;
                $capaian->id_jenis_layanan = $request->jenisLayanan;
                $capaian->id_rincian = $rjl;
                $capaian->id_indikator = 22;
                $capaian->id_tahun = $request->tahun;
                $capaian->tw = $request->tw;
                $capaian->trantibum = $trantibum;
                $capaian->kebencanaan = $kebencanaan;
                $capaian->damkar = $damkar;
                $capaian->save();

                $i++;
            }
        }
        return Redirect::to(url('spm/penerapan'));
    }

    // Update Capaian SPM
    public function updateCapaian(Request $request)
    {        
        foreach($request->total_dilayani as $key => $td)
        {
            $pelayanan = SPM_Pelayanan::where('id_pelayanan',$key)->firstOrFail();
            
            $pelayanan->total_dilayani = $td;
            $pelayanan->total_terlayani = $request->total_terlayani[$key];
            $pelayanan->total_belum_terlayani = $td-$request->total_terlayani[$key];

            $pelayanan->timestamps = false;
            $pelayanan->save();
        }

        foreach($request->jml_dibutuhkan as $key => $jd)
        {
            $capaian = SPM_Capaian::where('id_capaian',$key)->firstOrFail();

            $capaian->jml_dibutuhkan = $jd;
            $capaian->jml_tersedia = $request->jml_tersedia[$key];
            $capaian->jml_blm_tersedia = $jd-$request->jml_tersedia[$key];

            $capaian->timestamps = false;
            $capaian->save();
        }

        return redirect()->back();
    }
}

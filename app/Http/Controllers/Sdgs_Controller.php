<?php

namespace App\Http\Controllers;
use App\Http\Resources\SPM_Resource;
use Illuminate\Http\Request;
use Session;

use App\Models\Sdgs_Tujuan;
use App\Models\Sdgs_Indikator_Global;
use App\Models\Sdgs_Indikator_Nasional;
use App\Models\Sdgs_Indikator_Provinsi;
use App\Models\SKPD_90;
use App\Exports\SdgsExport;
use Maatwebsite\Excel\Facades\Excel;


class Sdgs_Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Supaya hanya user yang terotentikasi saja yg dapat mengakses
    }
    //dashboard Controller
    public function index()
    {
        return view('sdgs/dashboard');
    }
    //tujuansdgs
    public function tujuansdgs()
    {
        //$data = Sdgs_Tujuan::where('isdeleted',0)->orderby('urutan','asc')->get();
        return view('sdgs/tujuansdgs');
    }

    //indikatorglobalsdgs
    public function indikatorglobalsdgs()
    {
        //$data = Sdgs_Indikator_Global::select('*')->join('Sdgs_Tujuan','Sdgs_Global.id_sdgs','=','Sdgs_Tujuan.id_sdgs')->orderby('urutan','asc')->get();
        return view('sdgs/indikatorglobalsdgs');
    }

    //indikatornasionalsdgs
    public function indikatornasionalsdgs()
    {
        //$data = Sdgs_Indikator_Nasional::select('*')->join('Sdgs_Global','Sdgs_Global.id_target_global','=','Sdgs_Nasional.id_target_global')->orderby('Sdgs_Nasional.id_target_global','asc')->get();
        return view('sdgs/indikatornasionalsdgs');
    }

    //indikatorprovinsisdgs
    public function indikatorprovinsisdgs()
    {
        //$data = Sdgs_Indikator_Provinsi::select('*')->join('Sdgs_Nasional','Sdgs_IndikatorProvinsi.id_target_nasional','=','Sdgs_Nasional.id_target_nasional')->orderby('Sdgs_IndikatorProvinsi.id_target_nasional','asc')->get();
        return view('sdgs/indikatorprovinsisdgs');
    }
    //indikatorkotasdgs
    public function indikatorkotasdgs()
    {
        $data = array();
        return view('sdgs/indikatorkotasdgs', compact('data'));
    }
    //Codingsdgssubkegiatan
    public function codingsdgs()
    {
        if(Session::get('id_role') != 1 || Session::get('id_role') != 15){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        return view('sdgs/codingsdgs', compact('id_pd'));
    }
    //Monevsubkegiatan
    public function monevsdgs()
    {
        if(Session::get('id_role') != 1 || Session::get('id_role') != 15){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        return view('sdgs/monevsdgs', compact('id_pd'));
    }
    //
    public function dashboardrekap()
    {
        if(Session::get('id_role') != 1 || Session::get('id_role') != 15){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        return view('sdgs/dashboard/rekapsdgs', compact('id_pd'));
    }
    //
    public function dashboardrekapdetail($param)
    {
        if(Session::get('id_role') != 1 || Session::get('id_role') != 15){
            $id_pd = json_encode(Session::get('id_skpd'));
        }else{
            $id_pd = 0;
        }
        if($param == 'pd'){
            return view('sdgs/dashboard/detailpdrekapsdgs', compact('id_pd'));
        }else if($param == 'pemetaan'){
            return view('sdgs/dashboard/detailpemetaanrekapsdgs', compact('id_pd'));
        }else{
            return '404';
        }
    }
    //Monevrenja
    public function monevrenja()
    {
        $data = array();
        return view('sdgs/monevrenja', compact('data'));
    }
    // Cetak SDGS
    public function cetaksdgs(Request $request)
    {
        ini_set('max_execution_time', 300);
        ini_set("memory_limit","512M");
        if($request->all = 1)
        {
            $do = Excel::download(new SdgsExport($request), 'RekapSDGS_SemuaPD.xlsx');            
        }else{
            $skpd = SKPD_90::where('id_skpd',$request->id_skpd)->first();
            $do = Excel::download(new SdgsExport($request), 'RekapSDGS_'.$skpd->nama_skpd.'.xlsx');
        }
        return $do;
    }
}

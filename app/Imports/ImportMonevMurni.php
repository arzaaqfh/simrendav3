<?php

namespace App\Imports;

use App\Models\BidangUrusan_90;
use App\Models\Tahun;
use App\Models\Renja_90;
use App\Models\LogRenja_90;
use App\Models\R_LogRenja_90;
use App\Models\SKPD_90;
use App\Models\SubKegiatan_90;
use App\Models\IndikatorSubKegiatan_90;
use App\Models\R_IndikatorSubKegiatan_90;
use App\Models\Kegiatan_90;
use App\Models\IndikatorKegiatan_90;
use App\Models\Program_90;
use App\Models\ProgramRPJMD;
use App\Models\IndikatorProgramRPJMD;
use App\Models\TargetRealisasiIKP;
use App\Models\Urusan_90;
use App\Models\UrusanRPJMD;
use App\Models\SatuanRPJMD;

use Session;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ImportMonevMurni implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $notifikasi = array(); // untuk menampung status BERHASIL atau TIDAK dari proses INPUT
        
        /*
            PROSES TAMPUNG DATA YANG AKAN DIIMPORT KE DATABASE
        */
        $input_skpd = array();
        $temp_skpd = '';
        $input_ind_sub_kegiatan = array();
        $temp_sub_kegiatan = '';
        $input_ind_kegiatan = array();
        $temp_kegiatan = '';
        $input_ind_program = array();
        $temp_program = '';
        $input_anggaran = array();
        $temp_anggaran = 0;
        for($i=9;$i<count($collection);$i++)
        {
            /*
                SKPD
            */
            if(
                $collection[$i][0] == '' &&
                $collection[$i][1] == '' &&
                $collection[$i][2] == '' &&
                $collection[$i][3] == '' &&
                $collection[$i][4] == '' &&
                $collection[$i][5] == '' &&
                $collection[$i][6] != '' &&
                $collection[$i][7] == ''
            )
            {
                if(
                    !str_contains($collection[$i][6],'Puskesmas') &&
                    !str_contains($collection[$i][6],'BLUD') &&
                    !str_contains($collection[$i][6],'UPTD') &&
                    !str_contains($collection[$i][6],'Kelurahan')
                )
                {
                    /* 
                        tampung skpd yang tidak mengandung kata :
                            - Puskesmas
                            - BLUD
                            - UPTD
                            - Kelurahan
                    */
                    $temp_skpd = $collection[$i][6];
                }
                if(!in_array($temp_skpd,$input_skpd))
                {
                    // tambahkan skpd yang belum dimasukkan untuk diinput
                    $input_skpd[] = $temp_skpd;
                }
            }
            /*
                PROGRAM
            */
            if(
                $collection[$i][0] != '' &&
                $collection[$i][1] != '' &&
                $collection[$i][2] != '' &&
                $collection[$i][3] != '' &&
                $collection[$i][4] == '' &&
                $collection[$i][5] == '' &&
                $collection[$i][6] != '' &&
                $collection[$i][7] != ''
            )
            {
                // tampung kode program
                $temp_program = $collection[$i][1].'.'.$collection[$i][2].'.'.$collection[$i][3];
                // INDIKATOR PROGRAM
                if(
                    $collection[$i][7] != '-' &&
                    $collection[$i][7] != 'n/a' &&
                    $collection[$i][7] != '' &&
                    $collection[$i][7] != 'undefined' &&
                    !str_contains($temp_program, 'x') &&                
                    !str_contains($temp_program, 'X')
                )
                {
                    $x_iprog = preg_split("/\r\n|\n|\r/",$collection[$i][7]); // pecahkan indikator program
                    $x_tsiprog = preg_split("/\r\n|\n|\r/",$collection[$i][11]); // pecahkan target satuan indikator program
    
                    for($x=0;$x<count($x_iprog);$x++)
                    {
                        $ts = explode(' ',$x_tsiprog[$x]); // pisahkan antara target dan satuan
                        $target = '-';
                        $satuan = '-';
                        if(count($ts) == 2)
                        {
                            $target = $ts[0];
                            $satuan = str_replace('%','persen',$ts[1]);
                        }
                        // cari id satuan                        
                        $d_satuan = SatuanRPJMD::where('satuan',$satuan)->first();
                        $id_satuan = '';
                        if($d_satuan)
                        {
                            // ambil id satuan jika ada
                            $id_satuan = $d_satuan->id_satuan;
                        }else{
                            // tambahkan satuan baru jika tidak ada
                            $t_satuan = new SatuanRPJMD();
                            $t_satuan->satuan = $satuan;
                            $t_satuan->timestamps = false;
                            if($t_satuan->save())
                            {
                                // ambil id satuan yang telah ditambahkan
                                $d_satuan = SatuanRPJMD::where('satuan',$satuan)->first();
                                $id_satuan = $d_satuan->id_satuan;
                            }
                        }

                        // ganti nama indikator program
                        $indikator_program = str_replace('Prosentase','Persentase',$x_iprog[$x]);
                        $indikator_program = str_replace('undefined','',$indikator_program);
                        
                        // cari id indikator program
                        $indProgram = IndikatorProgramRPJMD::where('indikator_program',$indikator_program)->first();
                        $id_indikator_program = '';
                        if($indProgram)
                        {
                            $id_indikator_program = $indProgram->id_indikator_program;
                        }
                        $input_ind_program[] = array(
                            'kode_program' => $temp_program,
                            'indikator_program' => $indikator_program,
                            'target' => $target,
                            'satuan' => $satuan,
                            'skpd' => $temp_skpd,
                            'id_indikator_program' => $id_indikator_program,
                            'id_satuan' => $id_satuan
                        );
                    }
                }
            }

            /*
                KEGIATAN
            */
            if(
                $collection[$i][0] == '' &&
                $collection[$i][1] != '' &&
                $collection[$i][2] != '' &&
                $collection[$i][3] != '' &&
                $collection[$i][4] != '' &&
                $collection[$i][5] == '' &&
                $collection[$i][6] != '' &&
                $collection[$i][7] != ''
            )
            {
                // tampung kode kegiatan
                $temp_kegiatan = $collection[$i][1].'.'.$collection[$i][2].'.'.$collection[$i][3].'.'.$collection[$i][4];
                // INDIKATOR KEGIATAN
                if(
                    $collection[$i][7] != '-' &&
                    $collection[$i][7] != 'n/a' &&
                    $collection[$i][7] != '' &&
                    $collection[$i][7] != 'undefined' &&
                    !str_contains($temp_kegiatan, 'x') &&                
                    !str_contains($temp_kegiatan, 'X')
                )
                {
                    $x_ikeg = preg_split("/\r\n|\n|\r/",$collection[$i][7]); // pecahkan indikator kegiatan
                    $x_tsikeg = preg_split("/\r\n|\n|\r/",$collection[$i][11]); // pecahkan target satuan indikator kegiatan
    
                    for($x=0;$x<count($x_ikeg);$x++)
                    {
                        $ts = explode(' ',$x_tsikeg[$x]); // pisahkan antara target dan satuan
                        $target = '-';
                        $satuan = '-';
                        if(count($ts) == 2)
                        {
                            $target = $ts[0];
                            $satuan = $ts[1];
                        }
                        $input_ind_kegiatan[] = array(
                            'kode_kegiatan' => $temp_kegiatan,
                            'indikator_kegiatan' => str_replace('undefined','',$x_ikeg[$x]),
                            'target' => $target,
                            'satuan' => $satuan,
                            'skpd' => $temp_skpd
                        );
                    }
                }
            }
            /*
                SUB KEGIATAN
            */
            if(
                $collection[$i][0] == '' &&
                $collection[$i][1] != '' &&
                $collection[$i][2] != '' &&
                $collection[$i][3] != '' &&
                $collection[$i][4] != '' &&
                $collection[$i][5] != '' &&
                $collection[$i][6] != '' &&
                $collection[$i][7] == ''
            )
            {
                // ANGGARAN
                if(
                    $temp_sub_kegiatan != '' &&
                    !str_contains($temp_sub_kegiatan, 'x') &&                
                    !str_contains($temp_sub_kegiatan, 'X'))
                {
                    $input_anggaran[] = array(
                        'kode_sub_kegiatan' => $temp_sub_kegiatan,
                        'anggaran' => $temp_anggaran,
                        'skpd' => $temp_skpd
                    );
                }

                // tampung kode sub kegiatan
                $temp_sub_kegiatan = $collection[$i][1].'.'.$collection[$i][2].'.'.$collection[$i][3].'.'.$collection[$i][4].'.'.substr($collection[$i][5], 2,2);
                /* 
                    anggaran diset ke 0
                    data anggaran disimpan per sub kegiatan
                */
                $temp_anggaran = 0;
            }
            // INDIKATOR SUB KEGIATAN
            if(
                $collection[$i][0] == '' &&
                $collection[$i][1] == '' &&
                $collection[$i][2] == '' &&
                $collection[$i][3] == '' &&
                $collection[$i][4] == '' &&
                $collection[$i][5] == '' &&
                $collection[$i][6] == '' &&
                $collection[$i][7] != '' &&
                !str_contains($temp_sub_kegiatan, 'x') &&                
                !str_contains($temp_sub_kegiatan, 'X')
            )
            {
                // tampung indikator sub kegiatan
                $ts = explode(' ',$collection[$i][11]); // pisahkan antara target dan satuan
                $target = '-'; // target indikator
                $satuan = '-'; // satuan indikator
                if(count($ts) == 2)
                {
                    $target = $ts[0]; // ambil target
                    $satuan = $ts[1]; // ambil satuan
                }
                // simpan yang akan di input
                $input_ind_sub_kegiatan[] = array(
                    'kode_sub_kegiatan' => $temp_sub_kegiatan,
                    'indikator_sub_kegiatan' => $collection[$i][7],
                    'target' => $target,
                    'satuan' => $satuan,
                    'skpd' => $temp_skpd
                );
                // hitung anggaran
                // angka anggaran di konversi
                $cleanString = preg_replace('/([^0-9\.,])/i', '', $collection[$i][12]);
                $onlyNumbersString = preg_replace('/([^0-9])/i', '', $collection[$i][12]);

                $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

                $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
                $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

                $agr = str_replace(',', '.', $removedThousandSeparator);
                $temp_anggaran = (float) $temp_anggaran + $agr;
            }
        }

        /*
            IMPORT KE DATABASE
        */
        // TAHUN
        $tahun = intval(trim(substr($collection[2][0],6,4)," ")); //simpan tahun
        $cek = Tahun::where('tahun',$tahun)->get(); //cek tahun
        if(is_null($cek->first()))
        {
            $id_tahun = Tahun::all()->count()+1;
            $data = new Tahun;
            $data->id_tahun = $id_tahun;
            $data->tahun = $tahun;
            $data->timestamps = false;
            if($data->save())
            {
                $notifikasi['tahun'] = array('status' => 'BERHASIL');
            }else{
                $notifikasi['tahun'] = array('status' => 'GAGAL');
            }
        }else{
            $notifikasi['tahun'] = array('status' => 'SUDAH ADA');
        }

        // RENJA
        foreach($input_skpd as $skpd)
        {
            $id_skpd = SKPD_90::where('nama_skpd',$skpd)->first()['id_skpd'];
            $cek_renja = Renja_90::where([
                    ['periode_usulan',$tahun],
                    ['id_skpd',$id_skpd],
                ])->first(); 
            if(!$cek_renja)
            {
                // tambahkan data renja jika belum ada di database
                $i_renja = new Renja_90();
                $i_renja->id_skpd = $id_skpd;
                $i_renja->periode_usulan = $tahun;
                $i_renja->isDeleted = 0;
                if($i_renja->save())
                {
                    $notifikasi['renja'][] = array(
                        'id_skpd' => $id_skpd,
                        'periode_usulan' => $tahun,
                        'status' => 'BERHASIL'
                    );
                }else{
                    $notifikasi['renja'][] = array(
                        'id_skpd' => $id_skpd,
                        'periode_usulan' => $tahun,
                        'status' => 'GAGAL'
                    );
                }
            }else{
                $notifikasi['renja'][] = array(
                    'id_skpd' => $id_skpd,
                    'periode_usulan' => $tahun,
                    'status' => 'SUDAH ADA'
                );
            }
        }

        // INDIKATOR SUB KEGIATAN
        foreach($input_ind_sub_kegiatan as $isk)
        {
            // tambahkan indikator sub kegiatan baru
            $i_isk = new IndikatorSubKegiatan_90();
            $i_isk->id_sub_kegiatan = SubKegiatan_90::where(
                    'kode_sub_kegiatan',$isk['kode_sub_kegiatan']
                )->first()['id_sub_kegiatan'];
            $i_isk->id_renja = Renja_90::where([
                    ['id_skpd',SKPD_90::where('nama_skpd',$isk['skpd'])->first()['id_skpd']],
                    ['periode_usulan',$tahun]
                ])->first()['id_renja'];
            $i_isk->indikator_subkegiatan = $isk['indikator_sub_kegiatan'];
            $i_isk->target = $isk['target'];
            $i_isk->satuan = $isk['satuan'];
            $i_isk->is_added = 0;
            $i_isk->is_emptied = 0;
            $i_isk->isDeleted = 0;
            $i_isk->is_constant = 0;

            if($i_isk->save())
            {
                $notifikasi['indikator_sub_kegiatan'][] = array(
                    'indikator' => $isk['indikator_sub_kegiatan'],
                    'status' => 'BERHASIL'
                );
            }else{
                $notifikasi['indikator_sub_kegiatan'][] = array(
                    'indikator' => $isk['indikator_sub_kegiatan'],
                    'status' => 'GAGAL'
                );
            }
        }
        
        // INDIKATOR KEGIATAN
        foreach($input_ind_kegiatan as $ik)
        {
            // tambahkan indikator kegiatan baru
            $i_ik = new IndikatorKegiatan_90();
            $i_ik->id_kegiatan = Kegiatan_90::where(
                    'kode_kegiatan',$ik['kode_kegiatan']
                )->first()['id_kegiatan'];
            $i_ik->id_renja = Renja_90::where([
                    ['id_skpd',SKPD_90::where('nama_skpd',$ik['skpd'])->first()['id_skpd']],
                    ['periode_usulan',$tahun]
                ])->first()['id_renja'];
            $i_ik->indikator_kegiatan = $ik['indikator_kegiatan'];
            $i_ik->target = $ik['target'];
            $i_ik->satuan = $ik['satuan'];
            $i_ik->is_added = 0;
            $i_ik->is_emptied = 0;
            $i_ik->isDeleted = 0;
            $i_ik->is_constant = 0;

            if($i_ik->save())
            {
                $notifikasi['indikator_kegiatan'][] = array(
                    'indikator' => $ik['indikator_kegiatan'],
                    'status' => 'BERHASIL'
                );
            }else{
                $notifikasi['indikator_kegiatan'][] = array(
                    'indikator' => $ik['indikator_kegiatan'],
                    'status' => 'GAGAL'
                );
            }
        }

        // INDIKATOR PROGRAM
        foreach($input_ind_program as $ip)
        {
            if($ip['id_indikator_program'])
            {
                // tambahkan target realisasi indikator program yang baru
                $i_trikp = new TargetRealisasiIKP();
                $i_trikp->target = $ip['target'];
                $i_trikp->id_sumber_data = 1;
                $i_trikp->id_tahun = $cek->first()['id_tahun'];
                $i_trikp->id_indikator_program = $ip['id_indikator_program'];
                $i_trikp->id_ketercapaian = 1;
                $i_trikp->id_kategori = 1;
                $i_trikp->triwulan = 1;

                if($i_trikp->save())
                {
                    $notifikasi['indikator_program'][] = array(
                        'indikator' => $ip['indikator_program'],
                        'status' => 'BERHASIL'
                    );
                }else{
                    $notifikasi['indikator_program'][] = array(
                        'indikator' => $ip['indikator_program'],
                        'status' => 'GAGAL'
                    );
                }
            }else{               
                $notifikasi['indikator_program_tidak_di_database'][] = array(
                    'indikator' => $ip['indikator_program'],
                    'target' => $ip['target'],
                    'satuan' => $ip['satuan'],
                    'sebab' => 'indikator program tidak ada di database',
                    'status' => 'GAGAL'
                );
            }    
        }

        // ANGGARAN
        foreach($input_anggaran as $agr)
        {
            // ambil data skpd
            $d_skpd = SKPD_90::where('nama_skpd',$agr['skpd'])->first();

            // ambil data renja
            $d_renja = Renja_90::where([
                ['id_skpd',$d_skpd->id_skpd],
                ['periode_usulan',$tahun]
            ])->first();

            // ambil data sub kegiatan
            $d_sub_keg = SubKegiatan_90::where([
                ['kode_sub_kegiatan',$agr['kode_sub_kegiatan']]
            ])->first();

            // cek data anggaran apakah sudah ada di database atau tidak
            $c_agr = LogRenja_90::where([
                    ['id_sub_kegiatan',$d_sub_keg->id_sub_kegiatan],
                    ['id_renja',$d_renja->id_renja]
                ])->first();

            if(!$c_agr)
            {
                // jika data belum ada
                $i_agr = new LogRenja_90();
                $i_agr->id_sub_kegiatan = $d_sub_keg->id_sub_kegiatan;
                $i_agr->id_renja = $d_renja->id_renja;
                $i_agr->apbd_kota = (float) $agr['anggaran'];
                $i_agr->isDeleted = 0;
                $i_agr->isPerubahan = 0;

                if($i_agr->save())
                {
                    // jika anggaran berhasil disimpan
                    $notifikasi['anggaran'][] = array(
                        'kode_sub_kegiatan' => $agr['kode_sub_kegiatan'],
                        'status' => 'BERHASIL'
                    );
                    // membuat realisasi anggaran baru
                    $d_nagr = LogRenja_90::orderBy('id_log_renja', 'desc')->first();
                    $i_ragr = new R_LogRenja_90();
                    $i_ragr->id_log_renja = $d_nagr->id_log_renja;
                    $i_ragr->timestamps = false;
                    if($i_ragr->save())
                    {                        
                        $notifikasi['realisasi_anggaran'][] = array(
                            'kode_sub_kegiatan' => $agr['kode_sub_kegiatan'],
                            'status' => 'BERHASIL DIBUAT'
                        );
                    }else{                  
                        $notifikasi['realisasi_anggaran'][] = array(
                            'kode_sub_kegiatan' => $agr['kode_sub_kegiatan'],
                            'status' => 'GAGAL DIBUAT'
                        );
                    }
                }else{
                    // jika anggaran gagal disimpan
                    $notifikasi['anggaran'][] = array(
                        'kode_sub_kegiatan' => $agr['kode_sub_kegiatan'],
                        'status' => 'GAGAL'
                    );
                }
            }else{          
                // jika data sudah ada 
                $notifikasi['anggaran'][] = array(
                    'kode_sub_kegiatan' => $agr['kode_sub_kegiatan'],
                    'sebab' => 'anggaran sudah ada di database',
                    'status' => 'GAGAL'
                );
            }
        }
        // echo "<pre>";
        // print_r($notifikasi);
        // die;
        return $notifikasi; 
    }
}

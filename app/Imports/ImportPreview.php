<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\TempExportMonev;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ImportPreview implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        TempExportMonev::truncate();
        //menerjemahkan data
        $urusan = null;
        $bidangUrusan = null;
        $program = null;
        $indikatorProgram = null;
        $kegiatan = null;
        $indikatorKegiatan = null;
        $subKegiatan = null;
        $indikatorSubKegiatan = null;
        $pd = null;
        $dataFormated = array();
        for($i=9;$i<count($collection);$i++)
        {
            $kode = $collection[$i][1].'.'.$collection[$i][2].'.'.$collection[$i][3].'.'.$collection[$i][4].'.'.$collection[$i][5];
            // print_r($collection[$i]);
            if(
                $collection[$i][0] == ''
                && $collection[$i][1] == '' 
                && $collection[$i][2] == ''
                && $collection[$i][3] == ''
                && $collection[$i][4] == ''
                && $collection[$i][5] == ''
                && $collection[$i][6] != ''
            )
            {
                // perangkat daerah
                // echo $collection[$i][6].'<br/>';
                $pd = $collection[$i][6]; // tampung perangkat daerah
            }

            if(
                $collection[$i][1] != '' 
                && $collection[$i][2] == ''
                && $collection[$i][3] == ''
                && $collection[$i][4] == ''
                && $collection[$i][5] == ''
                && $collection[$i][6] != ''
            )
            {                
                // urusan
                // echo $kode.' '.$collection[$i][6].'<br/>';
                $urusan = $collection[$i][6]; // tampung urusan
            }else if(
                $collection[$i][1] != '' 
                && $collection[$i][2] != ''
                && $collection[$i][3] == ''
                && $collection[$i][4] == ''
                && $collection[$i][5] == ''
                && $collection[$i][6] != ''
            )
            {
                // bidang urusan
                // echo $kode.' '.$collection[$i][6].'<br/>';
                $bidangUrusan = $collection[$i][6]; // tampung bidang urusan
            }else if(
                $collection[$i][1] != '' 
                && $collection[$i][2] != ''
                && $collection[$i][3] != ''
                && $collection[$i][4] == ''
                && $collection[$i][5] == ''
                && $collection[$i][6] != ''
            )
            {
                // program
                // echo $kode.' '.$collection[$i][6].'<br/>';
                $program = $collection[$i][6]; // tampung program
                $temp_nama_indikator_program = preg_split("/\\r\\n|\\r|\\n/", $collection[$i][7]);
                $temp_target_indikator_program = preg_split("/\\r\\n|\\r|\\n/", $collection[$i][11]);
                $index = 0;
                foreach($temp_target_indikator_program as $val)
                {
                    // indikator program
                    // nama, target dan satuan
                    $target = '-';
                    $satuan = '-';
                    if($val != '' && $val != '-')
                    {
                        $target = explode(' ',$val)[0];
                        $satuan = explode(' ',$val)[1];
                    }
                    // echo $temp_nama_indikator_program[$index].' | target: '.$target.' | satuan: '.$satuan;
                    // echo '<br/>';
                    // tampung dari urusan sampai indikator program                    
                    $dataFormated[] = array(
                        'urusan'                => $urusan,
                        'bidangUrusan'          => $bidangUrusan,
                        'program'               => $program,
                        'indikatorProgram'      => $temp_nama_indikator_program[$index],
                        'targetIP'              => $target,
                        'satuanIP'              => $satuan,
                        'kegiatan'              => '',
                        'indikatorKegiatan'     => '',
                        'targetIK'              => '',
                        'satuanIK'              => '',
                        'subKegiatan'           => '',
                        'indikatorSubKegiatan'  => '',
                        'targetISK'             => '',
                        'satuanISK'             => '',
                        'anggaran'              => '',
                        'perangkatDaerah'       => $pd 
                    );
                    $index++;
                }
            }else  if(
                $collection[$i][1] != '' 
                && $collection[$i][2] != ''
                && $collection[$i][3] != ''
                && $collection[$i][4] != ''
                && $collection[$i][5] == ''
                && $collection[$i][6] != ''
            )
            {
                // kegiatan
                // echo $kode.' '.$collection[$i][6].'<br/>';
                $kegiatan = $collection[$i][6]; //tampung kegiatan 
                $temp_nama_indikator_kegiatan = preg_split("/\\r\\n|\\r|\\n/", $collection[$i][7]);
                $temp_target_indikator_kegiatan = preg_split("/\\r\\n|\\r|\\n/", $collection[$i][11]);
                $index = 0;
                foreach($temp_target_indikator_kegiatan as $val)
                {
                    // indikator kegiatan
                    // nama, target dan satuan
                    $target = '-';
                    $satuan = '-';
                    if($val != '' && $val != '-')
                    {
                        $target = explode(' ',$val)[0];
                        $satuan = explode(' ',$val)[1];
                    }
                    // echo $temp_nama_indikator_kegiatan[$index].' | target: '.$target.' | satuan: '.$satuan;
                    // echo '<br/>';
                    
                    $dataFormated[] = array(
                        'urusan'                => $urusan,
                        'bidangUrusan'          => $bidangUrusan,
                        'program'               => $program,
                        'indikatorProgram'      => '',
                        'targetIP'              => '',
                        'satuanIP'              => '',
                        'kegiatan'              => $kegiatan,
                        'indikatorKegiatan'     => $temp_nama_indikator_kegiatan[$index],
                        'targetIK'              => $target,
                        'satuanIK'              => $satuan,
                        'subKegiatan'           => '',
                        'indikatorSubKegiatan'  => '',
                        'targetISK'             => '',
                        'satuanISK'             => '',
                        'anggaran'              => '',
                        'perangkatDaerah'       => $pd 
                    );
                    $index++;
                }
            }else  if(
                $collection[$i][1] != '' 
                && $collection[$i][2] != ''
                && $collection[$i][3] != ''
                && $collection[$i][4] != ''
                && $collection[$i][5] != ''
                && $collection[$i][6] != ''
            )
            {
                // sub kegiatan
                // echo $kode.' '.$collection[$i][6].'<br/>';
                $subKegiatan = $collection[$i][6]; // tampung sub kegiatan
            }else  if(
                $collection[$i][1] == '' 
                && $collection[$i][2] == ''
                && $collection[$i][3] == ''
                && $collection[$i][4] == ''
                && $collection[$i][5] == ''
                && $collection[$i][6] == ''
            )
            {
                // sub kegiatan
                // echo $collection[$i][7].'<br/>';
                $temp_target_indikator_kegiatan = preg_split("/\\r\\n|\\r|\\n/", $collection[$i][11]);
                foreach($temp_target_indikator_kegiatan as $val)
                {
                    // indikator sub kegiatan
                    // nama, target dan satuan
                    $target = '-';
                    $satuan = '-';
                    if($val != '' && $val != '-')
                    {
                        $target = explode(' ',$val)[0];
                        $satuan = explode(' ',$val)[1];
                    }
                    // echo $collection[$i][7].' | target: '.$target.' | satuan: '.$satuan;
                    // echo '<br/>';
                    $dataFormated[] = array(
                        'urusan'                => $urusan,
                        'bidangUrusan'          => $bidangUrusan,
                        'program'               => $program,
                        'indikatorProgram'      => '',
                        'targetIP'              => '',
                        'satuanIP'              => '',
                        'kegiatan'              => $kegiatan,
                        'indikatorKegiatan'     => '',
                        'targetIK'              => '',
                        'satuanIK'              => '',
                        'subKegiatan'           => $subKegiatan,
                        'indikatorSubKegiatan'  => $collection[$i][7],
                        'targetISK'             => $target,
                        'satuanISK'             => $satuan,
                        'anggaran'              => (float) str_replace('.','',str_replace(',00','',$collection[$i][12])),
                        'perangkatDaerah'       => $pd 
                    );
                    $index++;
                }
            }
        }
        
        foreach($dataFormated as $val)
        {
            $export = new TempExportMonev();
            $export->urusan = $val['urusan'];
            $export->bidang_urusan = $val['bidangUrusan']; 
            $export->program = $val['program']; 
            $export->indikator_program = $val['indikatorProgram']; 
            $export->target_ip = $val['targetIP']; 
            $export->satuan_ip = $val['satuanIP']; 
            $export->kegiatan = $val['kegiatan']; 
            $export->indikator_kegiatan = $val['indikatorKegiatan']; 
            $export->target_ik = $val['targetIK']; 
            $export->satuan_ik = $val['satuanIK']; 
            $export->sub_kegiatan = $val['subKegiatan']; 
            $export->indikator_subkegiatan = $val['indikatorSubKegiatan']; 
            $export->target_isk = $val['targetISK']; 
            $export->satuan_isk = $val['satuanISK']; 
            $export->anggaran = $val['anggaran']; 
            $export->perangkat_daerah = $val['perangkatDaerah'];
            $export->timestamps = false; 
            $export->save();
        }
    }
}

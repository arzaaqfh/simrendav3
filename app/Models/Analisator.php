<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Analisator extends Model
{
    use HasFactory;
    protected $table = 'Analisator';
    protected $primaryKey = 'id_analisator'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_renja',
        'id_jafung',
        'triwulan'
    ];
    /**
     * Get Jafung
     */
    public function jafung()
    {
        return $this->belongsTo(Jafung::class, 'id_jafung');
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisatorKegiatan extends Model
{
    use HasFactory;
    protected $table = 'AnalisatorKegiatan';
    protected $primaryKey = 'id_analisator'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_skpd',
	    'id_bidang',
	    'id_jafung',
        'created_at',
	    'updated_at'
    ];

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }

    /**
     * Get Bidang
     */
    public function bidang()
    {
        return $this->belongsTo(Bidang::class, 'id_bidang');
    }

    /**
     * Get Jafung
     */
    public function jafung()
    {
        return $this->belongsTo(Jafung::class, 'id_jafung');
    }

    /**
     * Get FPK
     */
    public function fpk()
    {
        return $this->hasMany(FaktorPendorongKegiatan::class, 'id_analisator');
    }

    /**
     * Get FPHK
     */
    public function fphk()
    {
        return $this->hasMany(FaktorPenghambatKegiatan::class, 'id_analisator');
    }

    /**
     * Get FTLK
     */
    public function ftlk()
    {
        return $this->hasMany(TindakLanjutKegiatan::class, 'id_analisator');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisisFaktorProgram extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    protected $table = 'AnalisisFaktorProgram';
    protected $primaryKey = 'id_faktor_program';
    protected $fillable = [
        'id_renja',
        'id_program_90',
        'id_program_rpjmd',
        'faktor_pendorong',
        'faktor_penghambat',
        'tindak_lanjut',
        'triwulan',
        'id_analisator',
        'keterangan_cp'
    ];
    /**
     * Get Analisator
     */
    public function analisator()
    {
        return $this->belongsTo(Analisator::class, 'id_analisator');
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisisFaktorSubKegiatan extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    protected $table = 'AnalisisFaktorSubKegiatan';
    protected $primaryKey = 'id_faktor_subkegiatan';
    protected $fillable = [
        'id_renja',
        'id_sub_kegiatan',
        'faktor_pendorong',
        'faktor_penghambat',
        'tindak_lanjut',
        'triwulan',
        'id_analisator',
        'keterangan_cp'
    ];

    /**
     * Get Analisator
     */
    public function analisator()
    {
        return $this->belongsTo(Analisator::class, 'id_analisator');
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
    /**
     * Get Sub Kegiatan
     */
    public function subkegiatan()
    {
        return $this->belongsTo(SubKegiatan_90::class, 'id_sub_kegiatan');
    }
}

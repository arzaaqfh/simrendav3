<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisisKinerjaBidangUrusan extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'AnalisisKinerjaBidangUrusan';
    protected $primaryKey = 'id_faktor_pd';
    protected $fillable = [
        'id_bidang_urusan',
        'id_renja',
        'faktor_pendorong',
        'faktor_penghambat',
        'triwulan',
    ];
}

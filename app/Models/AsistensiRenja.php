<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsistensiRenja extends Model
{
    use HasFactory;
    protected $table = 'AsistensiRenja';
    protected $primaryKey = 'id_asistensi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'asistensi',
        'periode'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspek extends Model
{
    use HasFactory;
    protected $table = 'Aspek';
    protected $primaryKey = 'id_aspek'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'aspek'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatasWaktu extends Model
{
    use HasFactory;
    protected $table = 'BatasWaktu';
    protected $primaryKey = 'id_batas_waktu'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'batas_waktu',
	    'periode_usulan',
	    'isDeleted',
        'kegiatan'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bidang extends Model
{
    use HasFactory;
    protected $table = 'Bidang';
    protected $primaryKey = 'id_bidang'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'bidang'
    ];
}

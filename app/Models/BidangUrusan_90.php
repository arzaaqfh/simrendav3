<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BidangUrusan_90 extends Model
{
    use HasFactory;
    protected $table = 'BidangUrusan_90';
    protected $primaryKey = 'id_bidang_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_urusan',
        'kode_bidang_urusan',
        'nama_bidang_urusan'
    ];

    /**
     * Get Urusan
     */
    public function urusan()
    {
        return $this->belongsTo(Urusan_90::class, 'id_urusan');
    }

    /**
     * Get Program
     */
    public function program()
    {
        return $this->HasMany(Program_90::class, 'id_bidang_urusan', 'id_bidang_urusan');
    }
}

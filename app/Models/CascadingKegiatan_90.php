<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CascadingKegiatan_90 extends Model
{
    use HasFactory;
    protected $table = 'CascadingKegiatan_90';
    protected $primaryKey = 'id_cascading_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_kegiatan',
        'id_renja',
        'id_indikator_program',
        'created_at',
        'updated_at'
    ];

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_kegiatan');
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }

    /**
     * Get IndikatorProgam RPJMD
     */
    public function indikatorprogramrpjmd()
    {
        return $this->belongsTo(IndikatorProgramRPJMD::class, 'id_indikator_program');
    }
}

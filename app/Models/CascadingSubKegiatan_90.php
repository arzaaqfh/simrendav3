<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CascadingSubKegiatan_90 extends Model
{
    use HasFactory;
    protected $table = 'CascadingSubKegiatan_90';
    protected $primaryKey = 'id_cascading_subkegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sub_kegiatan',
        'id_renja',
        'id_indikator_kegiatan',
        'created_at',
        'updated_at'
    ];

    /**
     * Get Sub Kegiatan
     */
    public function subkegiatan()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_sub_kegiatan');
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }

    /**
     * Get Indikator Kegiatan
     */
    public function indikatorkegiatan()
    {
        return $this->belongsTo(IndikatorKegiatan_90::class, 'id_indikator_program_rpjmd');
    }use HasFactory;
}

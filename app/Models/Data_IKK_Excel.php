<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data_IKK_Excel extends Model
{
    use HasFactory;
    protected $table = 'Data_IKK_Excel';
    protected $primaryKey = 'id_ikk'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_status_capaian',
        'aspek',
        'fokus',
        'bidang_urusan',
        'ikk',
        'satuan',
        'kondisi_awal',
        't2018',
        'r2018',
        'c2018',
        't2019',
        't2020',
        't2021',
        't2022',
        'sumber_data',
        'formula',
        'similarity'
    ];

    /**
     * Get Status Capaian
     */
    public function statusCapaian()
    {
        return $this->belongsTo(StatusCapaian::class, 'id_status_capaian');
    }
    /**
     * Get R IKK
     */
    public function rIKK()
    {
        return $this->hasMany(R_IKK::class, 'id_ikk');
    }
    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD::class, 'akronim','sumber_data');
    }
}

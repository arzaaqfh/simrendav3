<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorPendorongIKP extends Model
{
    use HasFactory;
    protected $table = 'FaktorPendorongIKP';
    protected $primaryKey = 'id_faktor_pd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'dana',
        'sdm',
        'waktu_pelaksanaan',
        'peraturan_perundangan',
        'sistem_pengadaan_barang_jasa',
        'perijinan',
        'ketersediaan_lahan',
        'kesiapan_dukungan_masyarakat',
        'faktor_alam',
        'id_target_realisasi'
    ];

    /**
     * Get Target Realisasi
     */
    public function TargetRealisasiIKP()
    {
        return $this->belongsTo(TargetRealisasiIKP::class, 'id_target_realisasi');
    }
}

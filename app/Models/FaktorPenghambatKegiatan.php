<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorPenghambatKegiatan extends Model
{
    use HasFactory;
    protected $table = 'FaktorPenghambatKegiatan';
    protected $primaryKey = 'id_faktor_ph'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_renja',
	    'id_kegiatan',
	    'id_analisator',
        'dana',
        'sdm',
        'waktu_pelaksanaan',
        'peraturan_perundangan',
        'sistem_pengadaan_barang_jasa',
        'perijinan',
        'ketersediaan_lahan',
        'kesiapan_dukungan_masyarakat',
        'faktor_alam',
        'keterangan',
        'triwulan',
        'created_at',
	    'updated_at'
    ];

    /**
     * Get Analisator
     */
    public function analisator()
    {
        return $this->belongsTo(AnalisatorKegiatan::class, 'id_analisator');
    }
}

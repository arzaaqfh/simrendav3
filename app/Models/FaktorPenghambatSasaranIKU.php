<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorPenghambatSasaranIKU extends Model
{
    use HasFactory;
    protected $table = 'FaktorPenghambatSasaranIKU';
    protected $primaryKey = 'id_faktor_ph'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'dana',
        'sdm',
        'waktu_pelaksanaan',
        'peraturan_perundangan',
        'sistem_pengadaan_barang_jasa',
        'perijinan',
        'ketersediaan_lahan',
        'kesiapan_dukungan_masyarakat',
        'faktor_alam',
        'id_target_realisasi'
    ];

    /**
     * Get Target Realisasi
     */
    public function TargetRealisasiSasaranIKU()
    {
        return $this->belongsTo(TargetRealisasiSasaranIKU::class, 'id_target_realisasi');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorKegiatan extends Model
{
    use HasFactory;
    protected $table = 'IndikatorKegiatan';
    protected $primaryKey = 'id_indikator_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_log_renja',
        'indikator_kegiatan',
        'target',
        'satuan',
        'is_added',
        'is_emptied',
        'formula',
        'rumus'
    ];

    /**
     * Get Realisasi Indikator Kegiatan Murni
     */
    public function rindkegmurni()
    {
       return $this->hasMany(R_IndikatorKegiatan::class, 'id_indikator_kegiatan');
    }
    
    /**
     * Get Realisasi Indikator Kegiatan Perubahan
     */
    public function rindkegperubahan()
    {
       return $this->hasMany(R_IndikatorKegiatan::class, 'id_indikator_kegiatan_perubahan','id_indikator_kegiatan');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorKegiatan_90 extends Model
{
	use HasFactory;
    use \Awobaz\Compoships\Compoships;
    
    protected $table = 'IndikatorKegiatan_90';
    protected $primaryKey = 'id_indikator_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_kegiatan',
        'id_renja',
        'indikator_kegiatan',
        'target',
        'satuan',
        'is_added',
        'is_emptied',
        'is_constant',
        'formula',
        'rumus',
        'isDeleted',
        'created_at',
        'updated_at',
        'target_perubahan'
    ];

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_kegiatan');
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }

    /**
     * Get Realisasi Indikator Kegiatan
     */
    public function realisasiindikatorkeg()
    {
        return $this->belongsTo(R_IndikatorKegiatan_90::class, 'id_indikator_kegiatan','id_indikator_kegiatan');
    }

    /**
     * Get Verifikasi Indikator Kegiatan
     */
    public function verifikasiIndikatorkegiatan()
    {
        return $this->belongsTo(VerifikasiIndikatorKegiatan_90::class, 'id_indikator_kegiatan', 'id_indikator_kegiatan');
    }
    /**
     * Relasi Indikator Program Kegiatan
     */
    public function rindprogkeg()
    {
        return $this->belongsTo(R_IndProgKeg::class,'id_indikator_kegiatan');
    }
    /**
     * Relasi Indikator Kegiatan Sub Kegiatan
     */
    public function rindkegsubkeg()
    {
        return $this->hasMany(R_IndKegSubKeg::class,'id_indikator_kegiatan');
    }
}

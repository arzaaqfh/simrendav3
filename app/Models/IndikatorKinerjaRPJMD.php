<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorKinerjaRPJMD extends Model
{
    use HasFactory;
    protected $table = 'IndikatorKinerjaRPJMD';
    protected $primaryKey = 'id_indikator_kinerja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'indikator_kinerja',
        'id_aspek',
        'id_satuan',
        'id_rumus',
        'id_skpd'
    ];

    /**
     * Get Aspek
     */
    public function Aspek()
    {
        return $this->belongsTo(Aspek::class, 'id_aspek');
    }
    /**
     * Get Satuan
     */
    public function SatuanRPJMD()
    {
        return $this->belongsTo(SatuanRPJMD::class, 'id_satuan');
    }
    /**
     * Get Rumus
     */
    public function Rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }
}

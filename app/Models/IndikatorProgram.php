<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorProgram extends Model
{
    use HasFactory;
    protected $table = 'IndikatorProgram';
    protected $primaryKey = 'id_indikator_program_renja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_log_renja',
        'indikator_program',
        'target',
        'satuan'
    ];
}

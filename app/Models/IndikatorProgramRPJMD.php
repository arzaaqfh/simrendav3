<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorProgramRPJMD extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    protected $table = 'IndikatorProgramRPJMD';
    protected $primaryKey = 'id_indikator_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'indikator_program',
        'id_satuan',
        'id_program',
        'id_rumus',
        'id_skpd',
        'id_program_90'
    ];

    /**
     * Get Satuan
     */
    public function SatuanRPJMD()
    {
        return $this->belongsTo(SatuanRPJMD::class, 'id_satuan');
    }
    /**
     * Get Program
     */
    public function ProgramRPJMD()
    {
        return $this->belongsTo(ProgramRPJMD::class, 'id_program');
    }
    /**
     * Get Program 90
     */
    public function Program_90()
    {
        return $this->belongsTo(Program_90::class, 'id_program_90');
    }
    /**
     * Get Rumus
     */
    public function Rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }

    /**
     * Get TargetRealisasiIKP
     */
    public function TargetRealisasiIKP()
    {
        return $this->hasMany(TargetRealisasiIKP::class, 'id_indikator_program');
    }
    
    /**
     * Get TargetRealisasiProgramV2 (Versi Terbaru Dari Tabel TargetRealisasiIKP)
     */
    public function TargetRealisasiProgramV2()
    {
        return $this->hasMany(TargetRealisasiProgramV2::class, 'id_indikator_program');
    }

    /**
     * Get CascadingKegiatan
     */
    public function cascadingkegiatan()
    {
        return $this->hasMany(CascadingKegiatan_90::class, 'id_indikator_program');
    }
    /**
     * Relasi Indikator Tujuan Program
     */
    public function rindtuprog()
    {
        return $this->belongsTo(R_IndTuProg::class,'id_indikator_program');
    }
    /**
     * Relasi Indikator Program Kegiatan
     */
    public function rindprogkeg()
    {
        return $this->hasMany(R_IndProgKeg::class,'id_indikator_program');
    }
}

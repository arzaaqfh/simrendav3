<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorSasaran extends Model
{
    use HasFactory;
    protected $table = 'IndikatorSasaran';
    protected $primaryKey = 'id_indikator_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sasaran',
        'indikator_sasaran',
        'created_at',
        'updated_at'
    ];

    /**
     * Target Realisasi Sasaran
     */
    public function targetrealisasisasaran()
    {
        return $this->hasMany(TargetRealisasiSasaran::class, 'id_indikator_sasaran');
    }
    /**
     * Sasaran
     */
    public function sasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_sasaran');
    }
    /**
     * Relasi Indikator Sasaran Tujuan
     */
    public function rindsatu()
    {
        return $this->hasMany(R_IndSaTu::class,'id_indikator_sasaran');
    }
}

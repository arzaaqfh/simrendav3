<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorSasaranRPJMD extends Model
{
    use HasFactory;
    protected $table = 'IndikatorSasaranRPJMD';
    protected $primaryKey = 'id_indikator_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'indikator_sasaran',
        'id_satuan',
        'id_sasaran',
        'id_rumus'
    ];

    /**
     * Get Satuan
     */
    public function SatuanRPJMD()
    {
        return $this->belongsTo(SatuanRPJMD::class, 'id_satuan');
    }
    /**
     * Get Sasaran
     */
    public function SasaranRPJMD()
    {
        return $this->belongsTo(SasaranRPJMD::class, 'id_sasaran');
    }
    /**
     * Get Rumus
     */
    public function Rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
}

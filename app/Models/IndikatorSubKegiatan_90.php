<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorSubKegiatan_90 extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    protected $table = 'IndikatorSubKegiatan_90';
    protected $primaryKey = 'id_indikator_subkegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sub_kegiatan',
        'id_renja',
        'indikator_subkegiatan',
        'target',
        'satuan',
        'is_added',
        'is_emptied',
        'is_constant',
        'formula',
        'rumus',
        'isDeleted',
        'created_at',
        'updated_at',
        'target_perubahan'
    ];

    /**
     * Get Sub Kegiatan
     */
    public function subkegiatan()
    {
        return $this->belongsTo(SubKegiatan_90::class, 'id_sub_kegiatan');
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja','id_renja');
    }

    /**
     * Get Realisasi Indikator Sub Kegiatan
     */
    public function realisasiindikatorsubkeg()
    {
        return $this->belongsTo(R_IndikatorSubKegiatan_90::class, 'id_indikator_subkegiatan','id_indikator_subkegiatan');
    }

    /**
     * Get Verifikasi Indikator Kegiatan
     */
    public function verifikasiIndikatorsubkegiatan()
    {
        return $this->belongsTo(VerifikasiIndikatorSubKegiatan_90::class, 'id_indikator_subkegiatan', 'id_indikator_subkegiatan');
    }
    /**
     * Relasi Indikator Kegiatan Sub Kegiatan
     */
    public function rindkegsubkeg()
    {
        return $this->belongsTo(R_IndKegSubKeg::class,'id_indikator_subkegiatan');
    }
}

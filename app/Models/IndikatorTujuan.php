<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorTujuan extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;
    protected $table = 'IndikatorTujuan';
    protected $primaryKey = 'id_indikator_tujuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'indikator_tujuan',
        'id_tujuan'
    ];  

    /**
     * Target Realisasi Tujuan
     */
    public function targetrealisasitujuan()
    {
        return $this->hasMany(TargetRealisasiTujuan::class, 'id_indikator_tujuan');
    }
    /**
     * Tujuan
     */
    public function tujuan()
    {
        return $this->belongsTo(Tujuan::class, 'id_tujuan');
    }
    /**
     * Relasi Indikator Sasaran Tujuan
     */
    public function rindsatu()
    {
        return $this->belongsTo(R_IndSaTu::class,'id_indikator_tujuan');
    }
    /**
     * Relasi Indikator Tujuan Program
     */
    public function rindtuprog()
    {
        return $this->hasMany(R_IndTuProg::class,'id_indikator_tujuan');
    }
}

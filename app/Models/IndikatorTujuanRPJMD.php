<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndikatorTujuanRPJMD extends Model
{
    use HasFactory;
    protected $table = 'IndikatorTujuanRPJMD';
    protected $primaryKey = 'id_indikator_tujuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'indikator_tujuan',
        'id_satuan',
        'id_tujuan',
        'id_rumus'
    ];

    /**
     * Get Satuan
     */
    public function SatuanRPJMD()
    {
        return $this->belongsTo(SatuanRPJMD::class, 'id_satuan');
    }
    /**
     * Get Tujuan
     */
    public function TujuanRPJMD()
    {
        return $this->belongsTo(TujuanRPJMD::class, 'id_tujuan');
    }
    /**
     * Get Rumus
     */
    public function Rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
}

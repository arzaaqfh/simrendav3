<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jafung extends Model
{
    use HasFactory;
    protected $table = 'Jafung';
    protected $primaryKey = 'id_jafung'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nama',
	    'nip',
	    'isDeleted'
    ];

    /**
     * Get Jafung Sektor PD
     */
    public function jafung_sektorpd()
    {
        return $this->hasMany(Jafung_SektorPD::class, 'id_jafung');
    }
    
    /**
     * Level Jafung
     */
    public function level_jafung()
    {
        return $this->belongsTo(Level::class, 'id_jafung', 'data_level');
    }
}

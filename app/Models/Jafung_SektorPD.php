<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jafung_SektorPD extends Model
{
    use HasFactory;
    protected $table = 'Jafung_SektorPD';
    protected $primaryKey = 'id_jafung_sektorpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_jafung',
	    'id_skpd'
    ];

    /**
     * Get Jafung
     */
    public function jafung_detil()
    {
        return $this->belongsTo(Jafung::class, 'id_jafung','id_jafung');
    }
    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }
}

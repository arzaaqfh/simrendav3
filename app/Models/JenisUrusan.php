<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisUrusan extends Model
{
    use HasFactory;    
    protected $table = 'JenisUrusan';
    protected $primaryKey = 'id_jenis_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'jenis_urusan',
        'keterangan'
    ];
}

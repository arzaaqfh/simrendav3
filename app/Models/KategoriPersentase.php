<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriPersentase extends Model
{
    use HasFactory;
    protected $table = 'KategoriPersentase';
    protected $primaryKey = 'id_kategori'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'kategori',
	    'batas_bawah',
        'batas_atas',
        'created_at',
	    'updated_at'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriRPJMD extends Model
{
    use HasFactory;
    protected $table = 'KategoriRPJMD';
    protected $primaryKey = 'id_kategori'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'kategori'
    ];
}

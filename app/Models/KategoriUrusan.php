<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriUrusan extends Model
{
    use HasFactory;
    protected $table = 'KategoriUrusan';
    protected $primaryKey = 'id_kategori_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_jenis_urusan',
        'kode_kategori',
        'kategori_urusan'
    ];

    /**
     * Get Jenis Urusan
     */
    public function jenisUrusan()
    {
        return $this->belongsTo(JenisUrusan::class, 'id_jenis_urusan');
    }
}

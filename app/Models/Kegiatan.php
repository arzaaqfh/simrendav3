<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    use HasFactory;
    protected $table = 'Kegiatan';
    protected $primaryKey = 'id_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program',
        'kode_kegiatan',
        'nama_kegiatan',
        'isDeleted',
    ];

    /**
     * Get Program
     */
    public function program()
    {
        return $this->belongsTo(Program::class, 'id_program');
    }
    /**
     * Get Log Renja
     */
    public function logrenja()
    {
        return $this->hasMany(LogRenja::class, 'id_kegiatan');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan_90 extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    use HasFactory;
    protected $table = 'Kegiatan_90';
    protected $primaryKey = 'id_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program',
        'kode_kegiatan',
        'nama_kegiatan',
        'isDeleted'
    ];

    /**
     * Get Program
     */
    public function program()
    {
        return $this->belongsTo(Program_90::class, 'id_program');
    }

    /**
     * Get IndikatorKegiatan
     */
    public function indikatorkegiatan()
    {
        return $this->HasMany(IndikatorKegiatan_90::class, 'id_kegiatan', 'id_kegiatan');
    }

    /**
     * Get SubKegiatan
     */
    public function subkegiatan()
    {
        return $this->HasMany(SubKegiatan_90::class, 'id_kegiatan', 'id_kegiatan');
    }

    /**
     * Get APBD Kota
     */
    public function apbd_kota()
    {
        return $this->hasManyThrough(LogRenja_90::class,SubKegiatan_90::class,'id_kegiatan','id_sub_kegiatan');
    }
    
    /**
     * Get RealisasiUangKegiatan
     */
    public function realisasi_anggaran()
    {
        return $this->hasManyDeep(R_LogRenja_90::class,
        [SubKegiatan_90::class,
        LogRenja_90::class],['id_kegiatan','id_sub_kegiatan','id_log_renja'],['id_kegiatan','id_sub_kegiatan','id_log_renja']);
    }

    public function realisasi_anggaran_perubahan()
    {
        return $this->hasManyDeep(R_LogRenja_90::class,
        [SubKegiatan_90::class,
        LogRenja_90::class],['id_kegiatan','id_sub_kegiatan','id_log_renja_perubahan'],['id_kegiatan','id_sub_kegiatan','id_log_renja']);
    }

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->hasManyDeep(SKPD_90::class,
        [SubKegiatan_90::class,
        LogRenja_90::class,Renja_90::class],['id_kegiatan','id_sub_kegiatan','id_renja','id_skpd'],['id_kegiatan','id_sub_kegiatan','id_renja','id_skpd']);
    }
    
    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->hasMany(AnalisisFaktorKegiatan::class,'id_kegiatan');
    }
    /**
     * Get Anggaran Murni
     */
    public function anggaranmurni()
    {
        return $this->hasMany(ViewAnggaranMurniKegiatan::class, 'id_kegiatan');
    }
    /**
     * Get Anggaran Perubahan
     */
    public function anggaranperubahan()
    {
        return $this->hasMany(ViewAnggaranPerubahanKegiatan::class, 'id_kegiatan');
    }
}

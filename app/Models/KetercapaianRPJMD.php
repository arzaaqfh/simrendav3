<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetercapaianRPJMD extends Model
{
    use HasFactory;
    protected $table = 'KetercapaianRPJMD';
    protected $primaryKey = 'id_ketercapaian'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'ketercapaian'
    ];
}

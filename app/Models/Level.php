<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;
    protected $table = 'Level';
    protected $primaryKey = 'id_level'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'jenis_level',
        'data_level'
    ];
    
    /**
     * Jafung
     */
    public function jafung()
    {
        return $this->HasMany(Jafung::class, 'id_jafung', 'data_level');
    }

    /**
     * Get User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id_level', 'level_user');
    }

    // /**
    //  * Get IndikatorKegiatan
    //  */
    // public function indikatorkegiatan()
    // {
    //     return $this->HasMany(IndikatorKegiatan_90::class, 'id_kegiatan', 'id_kegiatan');
    // }

    // /**
    //  * Get SubKegiatan
    //  */
    // public function subkegiatan()
    // {
    //     return $this->HasMany(SubKegiatan_90::class, 'id_kegiatan', 'id_kegiatan');
    // }

    // /**
    //  * Get APBD Kota
    //  */
    // public function apbd_kota()
    // {
    //     return $this->hasManyThrough(LogRenja_90::class,SubKegiatan_90::class,'id_kegiatan','id_sub_kegiatan');
    // }
    
    // /**
    //  * Get RealisasiUangKegiatan
    //  */
    // public function realisasi_anggaran()
    // {
    //     return $this->hasManyDeep(R_LogRenja_90::class,
    //     [SubKegiatan_90::class,
    //     LogRenja_90::class],['id_kegiatan','id_sub_kegiatan','id_log_renja'],['id_kegiatan','id_sub_kegiatan','id_log_renja']);
    // }

    // public function realisasi_anggaran_perubahan()
    // {
    //     return $this->hasManyDeep(R_LogRenja_90::class,
    //     [SubKegiatan_90::class,
    //     LogRenja_90::class],['id_kegiatan','id_sub_kegiatan','id_log_renja_perubahan'],['id_kegiatan','id_sub_kegiatan','id_log_renja']);
    // }

    // /**
    //  * Get SKPD
    //  */
    // public function skpd()
    // {
    //     return $this->hasManyDeep(SKPD_90::class,
    //     [SubKegiatan_90::class,
    //     LogRenja_90::class,Renja_90::class],['id_kegiatan','id_sub_kegiatan','id_renja','id_skpd'],['id_kegiatan','id_sub_kegiatan','id_renja','id_skpd']);
    // }
}

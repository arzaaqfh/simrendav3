<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LimaP_IndikatorProgramRPJMD extends Model
{
    use HasFactory;
    protected $table = 'LimaP_IndikatorProgramRPJMD';
    protected $primaryKey = 'id_indikator_program_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_urusan',
        'id_program_rpjmd',
        'id_indikator_program',
        'indikator_program',
        'satuan',
        'awal',
        't_1',
        't_2',
        't_3',
        't_4',
        't_5',
        'akhir',
        'delta'
    ];

    /**
     * Get Urusan
     */
    public function urusan()
    {
        return $this->belongsTo(Urusan::class, 'id_urusan');
    }
    /**
     * Get Program RPJMD
     */
    public function programRPJMD()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }
    /**
     * Get Indikator Program
     */
    public function indikatorProgram()
    {
        return $this->belongsTo(Lima_IndikatorProgram::class, 'id_indikator_program');
    }
}

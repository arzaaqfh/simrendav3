<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LimaP_IndikatorSasaranRPJMD extends Model
{
    use HasFactory;
    protected $table = 'LimaP_IndikatorSasaranRPJMD';
    protected $primaryKey = 'id_indikator_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sasaran_rpjmd',
        'indikator_sasaran',
        't_1',
        't_2',
        't_3',
        't_4',
        't_5',
        't_a',
        'kondisi_awal',
        'delta',
        's_a',
        'rumus',
        'formula'
    ];

    /**
     * Get Sasaran RPJMD
     */
    public function sasaranRPJMD()
    {
        return $this->belongsTo(Lima_IndikatorSasaranRPJMD::class, 'id_sasaran_rpjmd');
    }
}

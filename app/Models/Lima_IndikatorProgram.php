<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_IndikatorProgram extends Model
{
    use HasFactory;
    protected $table = 'Lima_IndikatorProgram';
    protected $primaryKey = 'id_indikator_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program_rpjmd',
        'indikator_program',
        'satuan'
    ];
    
    /**
     * Get Program RPJMD
     */
    public function programRPJMD()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }
}

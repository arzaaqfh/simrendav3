<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_IndikatorRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_IndikatorRPJMD';
    protected $primaryKey = 'id_indikator_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program_rpjmd',
        'id_indikator_program',
        'id_ind_rpjmd_his',
        't_1',
        't_2',
        't_3',
        't_4',
        't_5',
        't_a',
        'rp_1',
        'rp_2',
        'rp_3',
        'rp_4',
        'rp_5',
        'rp_a',
        'kondisi_awal',
        'delta'
    ];

    /**
     * Get Program RPJMD
     */
    public function programRPJMD()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }
    /**
     * Get Indikator Program
     */
    public function indikatorProgram()
    {
        return $this->belongsTo(Lima_IndikatorProgram::class, 'id_indikator_program');
    }
}

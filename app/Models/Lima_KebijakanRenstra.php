<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_KebijakanRenstra extends Model
{
    use HasFactory;
    protected $table = 'Lima_KebijakanRenstra';
    protected $primaryKey = 'id_kebijakan_renstra'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program_skpd',
        'kebijakan'
    ];

    /**
     * Get Program SKPD
     */
    public function programSKPD()
    {
        return $this->belongsTo(Lima_ProgramSKPD::class, 'id_program_skpd');
    }
}

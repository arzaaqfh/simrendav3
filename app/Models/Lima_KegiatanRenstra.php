<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_KegiatanRenstra extends Model
{
    use HasFactory;
    protected $table = 'Lima_KegiatanRenstra';
    protected $primaryKey = 'id_kegiatan_renstra'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_rpjmd',
        'id_asistensi',
        'id_kegiatan',
        'id_skpd_pendukung',
        'id_renstra',
        'id_kebijakan',
        'tanggal',
        'lokasi',
        'rp_1',
        'rp_2',
        'rp_3',
        'rp_4',
        'rp_5',
        'rp_a',
        'status_coding',
        'status_renja'
    ];

    /**
     * Get Indikator RPJMD
     */
    public function indikatorRPJMD()
    {
        return $this->belongsTo(Lima_IndikatorRPJMD::class, 'id_indikator_rpjmd');
    }
    /**
     * Get Asistensi
     */
    public function asistensi()
    {
        return $this->belongsTo(AsistensiRenja::class, 'id_asistensi');
    }
    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'id_kegiatan');
    }
    /**
     * Get Renstra
     */
    public function renstra()
    {
        return $this->belongsTo(Lima_Renstra::class, 'id_renstra');
    }
    /**
     * Get Kebijakan
     */
    public function kebijakan()
    {
        return $this->belongsTo(Lima_KebijakanRenstra::class, 'id_kebijakan');
    }
}

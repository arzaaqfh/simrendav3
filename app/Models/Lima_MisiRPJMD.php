<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_MisiRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_MisiRPJMD';
    protected $primaryKey = 'id_misi_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_rpjmd',
        'misi_rpjmd',
        'pen_misi_rpjmd'
    ];

    // Belongs To (anak ke bapak)
    /**
     * Get RPJMD
     */
    public function RPJMD()
    {
        return $this->belongsTo(Lima_RPJMD::class, 'id_rpjmd');
    }
    
    // Has Many (bapak ke anak)
    /**
     * Get Tujuan RPJMD
     */
    public function tujuanRPJMD()
    {
        return $this->hasMany(Lima_TujuanRPJMD::class, 'id_misi_rpjmd');
    }
}

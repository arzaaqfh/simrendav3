<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_Periode extends Model
{
    use HasFactory;
    protected $table = 'Lima_Periode';
    protected $primaryKey = 'id_periode'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'tahun_awal',
        'tahun_akhir'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_ProgramRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_ProgramRPJMD';
    protected $primaryKey = 'id_program_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_strategi_rpjmd',
        'id_skpd',
        'id_program',
        'id_asistensi',
        'is_kirim',
        'rp_1',
        'rp_2',
        'rp_3',
        'rp_4',
        'rp_5',
        'rp_a'
    ];

    /**
     * Get Strategi RPJMD
     */
    public function strategiRPJMD()
    {
        return $this->belongsTo(Lima_StrategiRPJMD::class, 'id_strategi_rpjmd');
    }
    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }
    /**
     * Get Program
     */
    public function program()
    {
        return $this->belongsTo(Program::class, 'id_program');
    }
    /**
     * Get Asistensi Renja
     */
    public function asistensiRenja()
    {
        return $this->belongsTo(AsistensiRenja::class, 'id_asistensi');
    }
    /**
     * Get Indikator RPJMD
     */
    public function indikatorRPJMD()
    {
        return $this->hasMany(Lima_IndikatorRPJMD::class, 'id_program_rpjmd');
    }    
    /**
     * Get Program SKPD
     */
    public function programSKPD()
    {
        return $this->hasMany(Lima_ProgramSKPD::class, 'id_program_rpjmd');
    }   
    /**
     * Get Monev RKPD Head
     */
    public function monevRKPDHead()
    {
        return $this->hasMany(MonevRKPDHead::class, 'id_program_rpjmd');
    }
}

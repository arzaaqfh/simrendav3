<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_ProgramSKPD extends Model
{
    use HasFactory;
    protected $table = 'Lima_ProgramSKPD';
    protected $primaryKey = 'id_program_skpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_program_rpjmd',
        'id_skpd',
        'tahun_1',
        'tahun_2',
        'tahun_3',
        'tahun_4',
        'tahun_5'
    ];

    /**
     * Get Program RPJMD
     */
    public function programRPJMD()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }

    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_RPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_RPJMD';
    protected $primaryKey = 'id_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_periode',
        'visi_kota',
        'pen_visi_kota'
    ];

    /**
     * Get Periode
     */
    public function periode()
    {
        return $this->belongsTo(Lima_Periode::class, 'id_periode');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_Renstra extends Model
{
    use HasFactory;
    protected $table = 'Lima_Renstra';
    protected $primaryKey = 'id_renstra'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_skpd',
        'keterangan'
    ];

    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_SasaranRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_SasaranRPJMD';
    protected $primaryKey = 'id_sasaran_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_tujuan_rpjmd',
        'sasaran_rpjmd'
    ];

    // Belongs To (anak ke bapak)
    /**
     * Get Tujuan RPJMD
     */
    public function tujuanRPJMD()
    {
        return $this->belongsTo(Lima_TujuanRPJMD::class, 'id_tujuan_rpjmd');
    }
    
    // Has Many (bapak ke anak)
    /**
     * Get Indikator Sasaran RPJMD
     */
    public function indikatorSasaranRPJMD()
    {
        return $this->hasMany(Lima_IndikatorSasaranRPJMD::class, 'id_sasaran_rpjmd');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_StrategiRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_StrategiRPJMD';
    protected $primaryKey = 'id_strategi_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sasaran_rpjmd',
        'strategi_rpjmd'
    ];

    /**
     * Get Sasaran RPJMD
     */
    public function sasaranRPJMD()
    {
        return $this->belongsTo(Lima_SasaranRPJMD::class, 'id_sasaran_rpjmd');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lima_TujuanRPJMD extends Model
{
    use HasFactory;
    protected $table = 'Lima_TujuanRPJMD';
    protected $primaryKey = 'id_tujuan_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_misi_rpjmd',
        'tujuan_rpjmd'
    ];

    // Belongs To (anak ke bapak)
    /**
     * Get Misi RPJMD
     */
    public function misiRPJMD()
    {
        return $this->belongsTo(Lima_MisiRPJMD::class, 'id_misi_rpjmd');
    }

    // Has Many (bapak ke anak)
    /**
     * Get Sasaran RPJMD
     */
    public function sasaranRPJMD()
    {
        return $this->hasMany(Lima_SasaranRPJMD::class, 'id_tujuan_rpjmd');
    }
}

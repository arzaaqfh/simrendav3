<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogRenja extends Model
{
    use HasFactory;
    protected $table = 'LogRenja';
    protected $primaryKey = 'id_log_renja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_renja',
        'id_kegiatan',
        'id_sasaran_prioritas_rkpd',
        'id_jenis_kegiatan',
        'id_asistensi',
        'tanggal',
        'lokasi',
        'tolak_ukur_hasil_program',
        'target_hasil_program_volume',
        'target_hasil_program_satuan',
        'tolak_ukur_hasil_kegiatan',
        'target_hasil_kegiatan_volume',
        'target_hasil_kegiatan_satuan',
        'apbd_kota',
        'apbd_propinsi',
        'apbn',
        'dana_lain_lain',
        'dana_prakiraan_maju',
        'tahun_prakiraan_maju',
        'target_prakiraan_maju_volume',
        'target_prakiraan_maju_satuan',
        'catatan_penting',
        'isFisik',
        'tolak_ukur_1',
        'tolak_ukur_2',
        'tolak_ukur_3',
        'target_volume_3',
        'target_volume_2',
        'target_volume_1',
        'target_satuan_1',
        'target_satuan_3',
        'target_satuan_2',
        'target_prakiraan_maju_volume1',
        'target_prakiraan_maju_satuan1',
        'status_renja',
        'isDeleted',
        'isKegiatan',
        'isLokasi',
        'isTarget',
        'isPagu',
        'isProgram',
        'kelompok_sasaran',
        'sdm',
        'lama_kegiatan',
        'pagu_sipkd',
        'satuan_waktu',
        'status_coding_rkpd',
        'id_kegiatan_renstra',
        'id_program_rpjmd',
        'alasan_perubahan',
        'apbd_bl',
        'id_kegiatan90'
    ];

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja::class, 'id_renja');
    }
    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'id_kegiatan');
    }
    /**
     * Get Kegiatan 90
     */
    public function kegiatan90()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_kegiatan', 'id_kegiatan90');
    }
    /**
     * Get Sasaran Prioritas RKPD
     */
    public function sasaranprioritasrkpd()
    {
        return $this->belongsTo(SasaranPrioritasRKPD::class, 'id_sasaran_prioritas_rkpd');
    }
    /**
     * Get Asistensi
     */
    public function asistensi()
    {
        return $this->belongsTo(AsistensiRenja::class, 'id_asistensi');
    }
    /**
     * Get Kegiatan Renstra
     */
    public function kegiatanrenstra()
    {
        return $this->belongsTo(Lima_KegiatanRenstra::class, 'id_asistensi');
    }
    /**
     * Get Program RPJMD
     */
    public function programrpjmd()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }
    /**
     * Get Indikator Kegiatan
     */
    public function indikatorkegiatan()
    {
        return $this->belongsTo(IndikatorKegiatan::class, 'id_log_renja');
    }
    /**
     * Get Indikator Program
     */
    public function indikatorprogram()
    {
        return $this->belongsTo(IndikatorProgram::class, 'id_log_renja');
    }
    /**
     * Get Realisasi Log Renja 2019-2020 Murni
     */
    public function rlogrenja2019_2020_murni()
    {
        return $this->hasMany(R_LogRenja2019::class, 'id_log_renja');
    }
    /**
     * Get Realisasi Log Renja 2019-2020 Perubahan
     */
    public function rlogrenja2019_2020_perubahan()
    {
        return $this->hasMany(R_LogRenja2019::class, 'id_log_renja_perubahan', 'id_log_renja');
    }
    /**
     * Get Realisasi Log Renja 2017-2018 Murni
     */
    public function rlogrenja2017_2018_murni()
    {
        return $this->hasMany(RealisasiLogRenja::class, 'id_log_renja');
    }
    /**
     * Get Realisasi Log Renja 2017-2018 Perubahan
     */
    public function rlogrenja2017_2018_perubahan()
    {
        return $this->hasMany(RealisasiLogRenja::class, 'id_log_renja_perubahan', 'id_log_renja');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogRenja_90 extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    
	use HasFactory;
    protected $table = 'LogRenja_90';
    protected $primaryKey = 'id_log_renja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sub_kegiatan',
        'id_renja',
        'apbd_kota',
        'isDeleted',
        'created_at',
        'updated_at',
        'isPerubahan',
        'isSPM',
        'isSDGS'
    ];

    /**
     * Get SubKegiatan
     */
    public function subkegiatan()
    {
        return $this->belongsTo(SubKegiatan_90::class, 'id_sub_kegiatan');
    }

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->hasManyDeep(Kegiatan_90::class,
        [SubKegiatan_90::class],['id_sub_kegiatan','id_kegiatan'],['id_kegiatan','id_sub_kegiatan']);
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }

    /**
     * Get Realisai Anggaran Murni
     */
    public function realisasimurni()
    {
        return $this->hasOneThrough(R_LogRenja_90::class,LogRenja_90::class, 'id_log_renja','id_log_renja');
    }
    /**
     * Get Realisai Anggaran Perubahan
     */
    public function realisasiperubahan()
    {
        return $this->hasOneThrough(R_LogRenja_90::class,LogRenja_90::class, 'id_log_renja','id_log_renja_perubahan');
    }
}

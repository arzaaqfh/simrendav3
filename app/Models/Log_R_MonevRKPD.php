<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log_R_MonevRKPD extends Model
{
    use HasFactory;
    protected $table = 'Log_R_MonevRKPD';
    protected $primaryKey = 'id_log_r_monevrkpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_r_iku',
        'id_r_ikk',
        'id_monev_rkpd_d',
        'id_role'
    ];

    /**
     * Get R IKU
     */
    public function rIKU()
    {
        return $this->belongsTo(R_IKU::class, 'id_r_iku');
    }
    /**
     * Get R IKK
     */
    public function rIKK()
    {
        return $this->belongsTo(R_IKK::class, 'id_r_ikk');
    }
    /**
     * Get Monev RKPD Detail
     */
    public function monevRKPDDetail()
    {
        return $this->belongsTo(MonevRKPDDetail::class, 'id_monev_rkpd_d');
    }
    /**
     * Get Roles
     */
    public function role()
    {
        return $this->belongsTo(Roles::class, 'id_role');
    }
}

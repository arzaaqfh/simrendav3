<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiRPJMD extends Model
{
    use HasFactory;
    protected $table = 'MisiRPJMD';
    protected $primaryKey = 'id_misi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nomor',
        'misi'
    ];
}

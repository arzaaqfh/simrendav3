<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonevRKPDDetail extends Model
{
    use HasFactory;
    protected $table = 'MonevRKPDDetail';
    protected $primaryKey = 'id_monev_rkpd_d'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_monev_rkpd',
        'id_indikator_program',
        'indikator_2018',
        'realisasi_capaian',
        'formula',
        'sumber_data',
        'status_capaian',
        'faktor_penghambat',
        'faktor_pendorong',
        'tindak_lanjut'
    ];

    /**
     * Get Monev RKPD
     */
    public function monevRKPD()
    {
        return $this->belongsTo(MonevRKPDHead::class, 'id_monev_rkpd');
    }
    /**
     * Get Indikator Program
     */
    public function indikatorProgram()
    {
        return $this->belongsTo(Lima_IndikatorProgram::class, 'id_indikator_program');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonevRKPDHead extends Model
{
    use HasFactory;
    protected $table = 'MonevRKPDHead';
    protected $primaryKey = 'id_monev_rkpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_skpd',
        'id_urusan',
        'id_program_rpjmd',
        'id_program_2018',
        'id_kategori_urusan'
    ];

    /**
     * Get SKPD
     */
    public function SKPD()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }
    /**
     * Get Urusan
     */
    public function urusan()
    {
        return $this->belongsTo(Urusan::class, 'id_urusan');
    }    
    /**
     * Get Program RPJMD
     */
    public function programRPJMD()
    {
        return $this->belongsTo(Lima_ProgramRPJMD::class, 'id_program_rpjmd');
    }    
    /**
     * Get Kategori Urusan
     */
    public function kategoriUrusan()
    {
        return $this->belongsTo(KategoriUrusan::class, 'id_kategori_urusan');
    }    
    /**
     * Get Monev RKPD Detail
     */
    public function monevRKPDDetail()
    {
        return $this->hasMany(MonevRKPDDetail::class, 'id_monev_rkpd');
    }
}

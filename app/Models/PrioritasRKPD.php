<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrioritasRKPD extends Model
{
    use HasFactory;
    protected $table = 'PrioritasRKPD';
    protected $primaryKey = 'id_prioritas_rkpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'periode_usulan',
        'prioritas_rkpd'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use HasFactory;
    protected $table = 'Program';
    protected $primaryKey = 'id_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_urusan',
        'kode_program',
        'nama_program',
        'isDeleted'
    ];

    /**
     * Get Urusan
     */
    public function urusan()
    {
        return $this->belongsTo(Urusan::class, 'id_urusan');
    }
    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->hasMany(Kegiatan::class, 'id_program');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramRPJMD extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    protected $table = 'ProgramRPJMD';
    protected $primaryKey = 'id_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'program',
        'id_urusan'
    ];

    /**
     * Get Urusan
     */
    public function UrusanRPJMD()
    {
        return $this->belongsTo(UrusanRPJMD::class, 'id_urusan');
    }

    public function program90()
    {
        return $this->belongsTo(Program_90::class, 'id_program', 'id_program_rpjmd');
    }
    
    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->hasMany(AnalisisFaktorProgram::class,'id_program');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program_90 extends Model
{
    use HasFactory;
    protected $table = 'Program_90';
    protected $primaryKey = 'id_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['
        id_bidang_urusan',
        'kode_program',
        'nama_program',
        'id_program_rpjmd',
        'isDeleted'
    ];

    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(bidangUrusan_90::class, 'id_bidang_urusan');
    }

    public function kegiatan()
    {
        return $this->hasMany(Kegiatan_90::class, 'id_program');
    }

    public function indikator()
    {
        return $this->hasMany(IndikatorProgramRPJMD::class, 'id_program_90','id_program');
    }
    /**
     * Get Anggaran Murni
     */
    public function anggaranmurni()
    {
        return $this->hasMany(ViewAnggaranMurniProgram::class, 'id_program');
    }
    /**
     * Get Anggaran Perubahan
     */
    public function anggaranperubahan()
    {
        return $this->hasMany(ViewAnggaranPerubahanProgram::class, 'id_program');
    }
}

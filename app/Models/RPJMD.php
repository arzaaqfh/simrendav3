<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RPJMD extends Model
{
    use HasFactory;
    protected $table = 'RPJMD';
    protected $primaryKey = 'id_rpjmd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'visi_kota',
        'pen_visi_kota',
        'id_periode'
    ];

    /**
     * Get Periode
     */
    public function Periode()
    {
        return $this->belongsTo(Periode::class, 'id_periode');
    }
}

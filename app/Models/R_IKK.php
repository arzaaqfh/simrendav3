<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IKK extends Model
{
    use HasFactory;
    protected $table = 'R_IKK';
    protected $primaryKey = 'id_ikk'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'r_5',
        'tk_cap1',
        'tk_cap2',
        'tk_cap3',
        'tk_cap4',
        'tk_cap5',
        'status_1',
        'status_2',
        'status_3',
        'status_4',
        'status_5',
        'keterangan_1',
        'keterangan_2',
        'keterangan_3',
        'keterangan_4',
        'keterangan_5'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IKU extends Model
{
    use HasFactory;
    protected $table = 'R_IKU';
    protected $primaryKey = 'id_r_iku'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_sasaran',
        'sd_1',
        'sd_2',
        'sd_3',
        'sd_4',
        'sd_5',
        'rt_1',
        'rt_2',
        'rt_3',
        'rt_4',
        'rt_5',
        's_cap1',
        's_cap2',
        's_cap3',
        's_cap4',
        's_cap5',
        'tk_cap1',
        'tk_cap2',
        'tk_cap3',
        'tk_cap4',
        'tk_cap5',
        'ket_1',
        'ket_2',
        'ket_3',
        'ket_4',
        'ket_5'
    ];

    /**
     * Get Indikator Sasaran
     */
    public function indikatorSasaran()
    {
        return $this->belongsTo(Lima_IndikatorSasaranRPJMD::class, 'id_indikator_sasaran');
    }
}

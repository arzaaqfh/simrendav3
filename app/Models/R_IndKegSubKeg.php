<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IndKegSubKeg extends Model
{
    use HasFactory;
    protected $table = 'R_IndKegSubKeg';
    protected $primaryKey = 'id_rel'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_rel',
        'id_indikator_kegiatan',
        'id_indikator_subkegiatan',
        'id_sub_kegiatan',
        'id_renja',
        'created_at',
        'updated_at'
    ];  
    public function indikatorkegiatan()
    {
        return $this->hasMany(IndikatorKegiatan::class, 'id_indikator_tujuan');
    } 
    public function indikatorsubkegiatan()
    {
        return $this->belongsTo(IndikatorSubKegiatan::class, 'id_indikator_program');
    }  
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
}

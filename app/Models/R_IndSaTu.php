<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IndSaTu extends Model
{
    use HasFactory;
    protected $table = 'R_IndSaTu';
    protected $primaryKey = 'id_rel'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_rel',
        'id_indikator_sasaran',
        'id_indikator_tujuan',
        'id_tujuan',
        'tahun',
        'created_at',
        'updated_at'
    ];
    public function indikatorsasaran()
    {
        return $this->belongsTo(IndikatorSasaran::class, 'id_indikator_sasaran');
    }
    public function indikatortujuan()
    {
        return $this->hasMany(IndikatorTujuan::class, 'id_indikator_tujuan');
    }
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
}

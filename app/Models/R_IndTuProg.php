<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IndTuProg extends Model
{
    use HasFactory;
    protected $table = 'R_IndTuProg';
    protected $primaryKey = 'id_rel'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_rel',
        'id_indikator_tujuan',
        'id_indikator_program',
        'id_program',
        'tahun',
        'created_at',
        'updated_at'
    ];   

    public function indikatortujuan()
    {
        return $this->hasMany(IndikatorTujuan::class, 'id_indikator_tujuan');
    }
    public function indikatorprogram()
    {
        return $this->belongsTo(IndikatorProgramRPJMD::class, 'id_indikator_program');
    }
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IndikatorKegiatan extends Model
{
    use HasFactory;
    protected $table = 'R_IndikatorKegiatan';
    protected $primaryKey = 'id_r_indikator_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_ndikator_kegiatan',
        'id_r_logrenja',
        't_1',
        't_2',
        't_3',
        't_4',
        'id_indikator_kegiatan_perubahan'
    ];

    /**
     * Get Realisasi Log Renja 2019-2020
     */
    public function rlogrenja2019_2020()
    {
        return $this->hasMany(R_LogRenja2019::class,'id_r_logrenja');
    }
    /**
     * Get Realisasi Log Renja 2017-2018
     */
    public function rlogrenja2017_2018()
    {
        return $this->hasMany(RealisasiLogRenja::class,'id_realisasi', 'id_r_logrenja');
    }
}

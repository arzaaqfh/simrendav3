<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_IndikatorKegiatan_90 extends Model
{
    use HasFactory;
    protected $table = 'R_IndikatorKegiatan_90';
    protected $primaryKey = 'id_r_indikator_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_kegiatan',
        'id_renja',
        't_1',
        't_2',
        't_3',
        't_4',
        'persen_1',
        'persen_2',
        'persen_3',
        'persen_4',
        'total_persen',
        'total_persen_perubahan',
        'kategori_1',
        'kategori_2',        
        'kategori_3',
        'kategori_4',
        'created_at',
        'updated_at'
    ];

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }

    /**
     * Get Indikator Kegiatan
     */
    public function indikatorkegiatan()
    {
        return $this->belongsTo(IndikatorKegiatan_90::class, 'id_indikator_kegiatan');
    }
}
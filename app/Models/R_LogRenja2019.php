<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_LogRenja2019 extends Model
{
    use HasFactory;
    protected $table = 'R_LogRenja2019';
    protected $primaryKey = 'id_r_logrenja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'r_fisik_1',
        'r_fisik_2',
        'r_fisik_3',
        'r_fisik_4',
        'tindak_lanjut_1',
        'tindak_lanjut_2',
        'tindak_lanjut_3',
        'tindak_lanjut_4',
        'kesimpulan_1',
        'kesimpulan_2',
        'kesimpulan_3',
        'kesimpulan_4',
        'permasalahan_1',
        'permasalahan_2',
        'permasalahan_3',
        'permasalahan_4',
        'catatan_bidang_1',
        'catatan_bidang_2',
        'catatan_bidang_3',
        'catatan_bidang_4',
        'id_log_renja_perubahan',
        'faktor_pendorong_1',
        'faktor_pendorong_2',
        'faktor_pendorong_3',
        'faktor_pendorong_4',
        'catatan_pd_1',
        'catatan_pd_2',
        'catatan_pd_3',
        'catatan_pd_4',
        'progress_pd_1',
        'progress_pd_2',
        'progress_pd_3',
        'progress_pd_4',
        'progress_b_1',
        'progress_b_2',
        'progress_b_3',
        'progress_b_4',
        'permasalahan_b_1',
        'permasalahan_b_2',
        'permasalahan_b_3',
        'permasalahan_b_4',
        'faktor_pendorong_b_1',
        'faktor_pendorong_b_2',
        'faktor_pendorong_b_3',
        'faktor_pendorong_b_4',
        'kesimpulan_b_1',
        'kesimpulan_b_2',
        'kesimpulan_b_3',
        'kesimpulan_b_4',
        'tindak_lanjut_b_1',
        'tindak_lanjut_b_2',
        'tindak_lanjut_b_3',
        'tindak_lanjut_b_4'
    ];
}

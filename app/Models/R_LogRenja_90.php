<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class R_LogRenja_90 extends Model
{
    use HasFactory;
    protected $table = 'R_LogRenja_90';
    protected $primaryKey = 'id_r_logrenja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_log_renja',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'id_log_renja_perubahan'
    ];

    /**
     * Get SubKegiatan
     */
    public function logrenja90()
    {
        return $this->belongsTo(LogRenja_90::class, 'id_log_renja','id_log_renja');
    }

    public function logrenja90perubahan()
    {
        return $this->belongsTo(LogRenja_90::class, 'id_log_renja_perubahan','id_log_renja');
    }
}


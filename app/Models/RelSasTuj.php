<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelSasTuj extends Model
{
    use HasFactory;
    protected $table = 'RelSasTuj';
    protected $primaryKey = 'id_relasi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sasaran',
        'id_tujuan',
        'created_at',
        'updated_at'
    ];
    /**
     * Get Sasaran
     */
    public function sasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_sasaran');
    }
    /**
     * Get Tujuan
     */
    public function tujuan()
    {
        return $this->hasMany(Tujuan::class, 'id_tujuan');
    }
}

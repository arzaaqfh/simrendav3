<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelTujProg extends Model
{
    use HasFactory;
    protected $table = 'RelTujProg';
    protected $primaryKey = 'id_relasi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_tujuan',
        'id_program_90',
        'created_at',
        'updated_at'
    ];
    /**
     * Get Tujuan
     */
    public function tujuan()
    {
        return $this->belongsTo(Tujuan::class, 'id_tujuan');
    }
    /**
     * Get Program
     */
    public function program()
    {
        return $this->hasMany(Program_90::class, 'id_program', 'id_program_90');
    }
}

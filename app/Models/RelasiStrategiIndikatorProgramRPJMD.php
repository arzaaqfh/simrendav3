<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelasiStrategiIndikatorProgramRPJMD extends Model
{
    use HasFactory;
    protected $table = 'RelasiStrategiIndikatorProgramRPJMD';
    protected $primaryKey = 'id_relasi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_strategi',
        'id_indikator_program'
    ];

    /**
     * Get Indikator Program
     */
    public function indikatorprogramrpjmd()
    {
        return $this->belongsTo(IndikatorProgramRPJMD::class, 'id_indikator_program','id_indikator_program');
    }
}

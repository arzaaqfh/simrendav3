<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Renja extends Model
{
    use HasFactory;
    protected $table = 'Renja';
    protected $primaryKey = 'id_renja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_skpd',
        'id_renstra',
        'periode_usulan'
    ];

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->belongsTo(SKPD::class, 'id_skpd');
    }
}

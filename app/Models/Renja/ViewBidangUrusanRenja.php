<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewBidangUrusanRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewBidangUrusanRenja';
    protected $fillable = [
        'id_bidang_urusan',
        'id_urusan',
        'kode_bidang_urusan',
        'nama_bidang_urusan',
        'id_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'id_renja',
        'apbd_kota',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }

    /**
     * Get ViewProgram
     */
    public function viewprogramrenja()
    {
        return $this->HasMany(\App\Models\Renja\ViewProgramRenja::class, ['id_bidang_urusan', 'id_renja'],['id_bidang_urusan','id_renja']);
    }

    /**
     * Get Program
     */
    public function viewprogram()
    {
        return $this->HasMany(\App\Models\Renja\ViewProgramRenja::class, ['id_bidang_urusan', 'isPerubahan', 'periode_usulan'],['id_bidang_urusan','isPerubahan', 'periode_usulan']);
    }

    /**
     * Get Renja
     */
    public function viewpdrenja()
    {
        return $this->HasMany(\App\Models\Renja\ViewPDRenja::class, ['id_bidang_urusan', 'isPerubahan', 'periode_usulan'],['id_bidang_urusan','isPerubahan', 'periode_usulan']);
    }

    /**
     * Get Urusan
     */
    public function urusan()
    {
        return $this->belongsTo(\App\Models\Urusan::class, 'id_urusan','id_urusan');
    }
}

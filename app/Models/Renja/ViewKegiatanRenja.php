<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewKegiatanRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
    
    protected $table = 'ViewKegiatanRenja';
    protected $fillable = [
        'id_kegiatan',
        'id_program',
        'kode_kegiatan',
        'nama_kegiatan',
        'isDeleted',
        'id_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'id_renja',
        'apbd_kota',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get Program
     */
    public function program()
    {
        return $this->belongsTo(\App\Models\Program_90::class, 'id_program');
    }

    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->HasMany(\App\Models\AnalisisFaktorKegiatan::class, ['id_kegiatan','id_renja'],['id_kegiatan','id_renja']);
    }

    /**
     * Get IndikatorKegiatan
     */
    public function indikator()
    {
        return $this->HasMany(\App\Models\IndikatorKegiatan_90::class, ['id_kegiatan', 'id_renja'],['id_kegiatan','id_renja']);
    }

    /**
     * Get SubKegiatan
     */
    public function viewsubkegiatan()
    {
        return $this->HasMany(\App\Models\Renja\ViewSubKegiatanRenja::class, ['id_kegiatan','id_skpd','isPerubahan','periode_usulan'],['id_kegiatan','id_skpd','isPerubahan','periode_usulan']);
    }
    
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }
}

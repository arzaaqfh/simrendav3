<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewPDRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewPDRenja';
    protected $fillable = [
        'id_renja',
        'id_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'apbd_kota',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get PD
     */
    public function skpd()
    {
        return $this->belongsTo(\App\Models\SKPD_90::class, 'id_skpd','id_skpd');
    }

    /**
     * Get Program
     */
    public function viewprogram()
    {
        return $this->HasMany(\App\Models\Renja\ViewProgramRenja::class, ['id_bidang_urusan','id_renja', 'isPerubahan', 'periode_usulan'],['id_bidang_urusan','id_renja','isPerubahan', 'periode_usulan']);
    }

    /**
     * Get AnalisisFaktor
     */
    public function analisisfaktor()
    {
        return $this->HasMany(\App\Models\AnalisisKinerjaBidangUrusan::class, ['id_bidang_urusan','id_renja'],['id_bidang_urusan','id_renja']);
    }

    /**
     * Get TindakLanjut
     */
    public function tindaklanjut()
    {
        return $this->HasMany(\App\Models\TindakLanjutBidangUrusan::class, ['id_bidang_urusan','id_renja'],['id_bidang_urusan','id_renja']);
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }
}

<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewProgramRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewProgramRenja';
    protected $fillable = [
        'id_program',
        'id_program_rpjmd',
        'id_bidang_urusan',
        'kode_program',
        'nama_program',
        'isDeleted',
        'id_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'id_renja',
        'apbd_kota',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get BidangUrusan
     */
    public function bidangurusan()
    {
        return $this->belongsTo(\App\Models\BidangUrusan_90::class, 'id_bidang_urusan');
    }

    /**
     * Get Program
     */
    public function program()
    {
        return $this->belongsTo(\App\Models\Program_90::class, 'id_program','id_program');
    }

    /**
     * Get IndikatorProgram
     */
    public function indikator()
    {
        return $this->HasMany(\App\Models\IndikatorProgramRPJMD::class, ['id_program_90'],['id_program']);
    }

    /**
     * Get Analisis Faktor
     */
    public function analisisfaktorgetCetakRKPD()
    {
        return $this->HasMany(\App\Models\AnalisisFaktorProgram::class, ['id_program_90','id_renja'],['id_program','id_renja']);
    }
    
    /**
     * Get Kegiatan
     */
    public function viewkegiatan()
    {
        return $this->HasMany(\App\Models\Renja\ViewKegiatanRenja::class, ['id_program','id_skpd','isPerubahan','periode_usulan'],['id_program','id_skpd','isPerubahan','periode_usulan']);
    }

    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->HasMany(\App\Models\AnalisisFaktorProgram::class, ['id_program_90','id_renja'],['id_program','id_renja']);
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }
}

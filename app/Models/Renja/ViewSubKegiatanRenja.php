<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewSubKegiatanRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewSubKegiatanRenja';
    protected $fillable = [
        'id_sub_kegiatan',
        'id_kegiatan',
        'kode_sub_kegiatan',
        'nama_sub_kegiatan',
        'isDeleted',
        'id_skpd',
        'kode_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'id_log_renja',
        'id_renja',
        'apbd_kota',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }
    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(\App\Models\Kegiatan_90::class, 'id_kegiatan');
    }

    /**
     * Get IndikatorSubKegiatan
     */
    public function indikator()
    {
        return $this->HasMany(\App\Models\IndikatorSubKegiatan_90::class, ['id_sub_kegiatan', 'id_renja'],['id_sub_kegiatan','id_renja']);
    }

    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->HasMany(\App\Models\AnalisisFaktorSubKegiatan::class, ['id_sub_kegiatan','id_renja'],['id_sub_kegiatan','id_renja']);
    }

    /**
     * Get AnalisisFaktorPendorongSubKegiatan
     */
    public function analisisfp()
    {
        return $this->HasMany(\App\Models\FaktorPendorongSubKegiatan::class, ['id_sub_kegiatan', 'id_renja'],['id_sub_kegiatan','id_renja']);
    }

    /**
     * Get AnalisisFaktorPenghambatSubKegiatan
     */
    public function analisisfph()
    {
        return $this->HasMany(\App\Models\FaktorPenghambatSubKegiatan::class, ['id_sub_kegiatan', 'id_renja'],['id_sub_kegiatan','id_renja']);
    }

    /**
     * Get AnalisisTindakLanjutSubKegiatan
     */
    public function analisistl()
    {
        return $this->HasMany(\App\Models\TindakLanjutSubKegiatan::class, ['id_sub_kegiatan', 'id_renja'],['id_sub_kegiatan','id_renja']);
    }
}

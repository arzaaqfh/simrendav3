<?php

namespace App\Models\Renja;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewUrusanRenja extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewUrusanRenja';
    protected $fillable = [
        'id_urusan',
        'kode_urusan',
        'nama_urusan',
        'id_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'id_renja',
        'apbd_kota',
        'isPerubahan',
        'periode_usulan'
    ];

    /**
     * Get BidangUrusanRenja
     */
    public function viewbidangurusanrenja()
    {
        return $this->HasMany(\App\Models\Renja\ViewBidangUrusanRenja::class, ['id_urusan', 'id_renja'],['id_urusan','id_renja']);
    }

    /**
     * Get BidangUrusan
     */
    public function viewbidangurusan()
    {
        return $this->HasMany(\App\Models\Renja\ViewBidangUrusanRenja::class, ['id_urusan', 'isPerubahan', 'periode_usulan'], ['id_urusan', 'isPerubahan', 'periode_usulan']);
    }
    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->HasMany(\App\Models\Renja_90::class, 'id_renja', 'id_renja');
    }
}

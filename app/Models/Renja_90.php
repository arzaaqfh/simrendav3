<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Renja_90 extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use \Awobaz\Compoships\Compoships;
    
    use HasFactory;
    protected $table = 'Renja_90';
    protected $primaryKey = 'id_renja'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_skpd',
        'periode_usulan',
        'isDeleted',
        'created_at',
        'updated_at',
    ];

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }

    /**
     * Get LogRenja_90
     */
    public function logrenja()
    {
        return $this->hasMany(LogRenja_90::class, 'id_renja','id_renja');
    }

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->hasManyDeep(Kegiatan_90::class,
        [LogRenja_90::class,
        SubKegiatan_90::class],['id_renja','id_sub_kegiatan','id_kegiatan'],['id_renja','id_sub_kegiatan','id_kegiatan']);
    }
}

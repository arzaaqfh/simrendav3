<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesJenisLevel extends Model
{
    use HasFactory;
    
    protected $table = "RolesJenisLevel";
}

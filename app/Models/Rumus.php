<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rumus extends Model
{
    use HasFactory;
    protected $table = 'Rumus';
    protected $primaryKey = 'id_rumus'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'rumus',
        'isPositif'
    ];
}

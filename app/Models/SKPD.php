<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SKPD extends Model
{
    use HasFactory;
    protected $table = 'SKPD';
    protected $primaryKey = 'id_skpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_bidang',
        'nama_skpd',
        'kode_skpd',
        'akronim',
        'nip_kepala',
        'jabatan_kepala',
        'level',
        'urut',
        'id_bidang2',
        'kode_sipkd'
    ];

    /**
     * Get Bidang
     */
    public function bidang()
    {
        return $this->belongsTo(Bidang::class, 'id_bidang');
    }
}

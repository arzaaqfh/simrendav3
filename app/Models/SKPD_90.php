<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class SKPD_90 extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    use HasFactory;
    protected $table = 'SKPD_90';
    protected $primaryKey = 'id_skpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'kode_skpd',
        'nama_skpd',
        'nama_lain',
        'akronim',
        'isDeleted',
        'created_at',
        'updated_at',
        'id_bidang'
    ];

    /**
     * Get Bidang
     */
    public function bidang()
    {
        return $this->belongsTo(Bidang::class, 'id_bidang');
    }

    /**
     * Get Renja
     */
    public function renja()
    {
        return $this->hasMany(Renja_90::class, 'id_skpd');
    }

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->hasManyDeep(Kegiatan_90::class,
        [Renja_90::class,
        LogRenja_90::class,
        SubKegiatan_90::class],['id_skpd','id_renja','id_sub_kegiatan','id_kegiatan'],['id_skpd','id_renja','id_sub_kegiatan','id_kegiatan']);
    }

    /**
     * Get Jafung
     */
    public function jafung()
    {
        return $this->belongsTo(Jafung_SektorPD::class, 'id_skpd');
    }

    /**
     * Get IndikatorKegiatan
     */
    public function indikatorkegiatan()
    {
        return $this->hasManyDeep(IndikatorKegiatan_90::class,
        [Renja_90::class,
        LogRenja_90::class,
        SubKegiatan_90::class,
        Kegiatan_90::class],['id_skpd','id_renja','id_sub_kegiatan','id_kegiatan','id_kegiatan'],['id_skpd','id_renja','id_sub_kegiatan','id_kegiatan','id_kegiatan']);
    }
}

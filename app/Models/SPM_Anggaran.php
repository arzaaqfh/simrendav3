<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Anggaran extends Model
{
    use HasFactory;
    protected $table = 'SPM_Anggaran';
    protected $primaryKey = 'id_anggaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'pagu',
        'realisasi',
        'serapan',
        'id_bidang_urusan',
        'id_jenis_layanan',
        'id_kegiatan',
        'id_sub_kegiatan',
        'id_tahun',
        'tw',
        'trantibum',
        'kebencanaan',
        'damkar'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
    
    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan_90::class, 'id_bidang_urusan');
    }      
    /**
     * Get Jenis Layanan
     */
    public function jenisLayanan()
    {
        return $this->belongsTo(SPM_Jenis_Layanan::class, 'id_jenis_layanan');
    }
    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_kegiatan');
    }
    /**
     * Get Sub Kegiatan
     */
    public function subKegiatan()
    {
        return $this->belongsTo(SubKegiatan_90::class, 'id_sub_kegiatan');
    }
    /**
     * Get Tahun
     */
    public function tahun()
    {
        return $this->belongsTo(Tahun::class, 'id_tahun');
    }
}

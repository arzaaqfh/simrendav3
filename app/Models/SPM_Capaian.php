<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Capaian extends Model
{
    use HasFactory;
    protected $table = 'SPM_Capaian';
    protected $primaryKey = 'id_capaian'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'jml_dibutuhkan',
        'jml_tersedia',
        'jml_blm_tersedia',
        'persentase_capaian',
        'id_bidang_urusan',
        'id_jenis_layanan',
        'id_rincian',
        'id_indikator',
        'id_tahun',
        'tw',
        'trantibum',
        'kebencanaan',
        'damkar'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan_90::class, 'id_bidang_urusan');
    }       
    /**
     * Get Jenis Layanan
     */
    public function jenisLayanan()
    {
        return $this->belongsTo(SPM_Jenis_Layanan::class, 'id_jenis_layanan');
    }
    /**
     * Get Rincian Jenis Layanan
     */
    public function rincian()
    {
        return $this->belongsTo(SPM_Rincian_Jenis_Layanan::class, 'id_rincian');
    }
    /**
     * Get Indikator Pencapaian
     */
    public function indikator()
    {
        return $this->belongsTo(SPM_Indikator_Pencapaian::class, 'id_indikator');
    }
    /**
     * Get Tahun
     */
    public function tahun()
    {
        return $this->belongsTo(Tahun::class, 'id_tahun');
    }
}

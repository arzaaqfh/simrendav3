<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Dasar_Hukum extends Model
{
    use HasFactory;
    protected $table = 'SPM_Dasar_Hukum';
    protected $primaryKey = 'id_dasar_hukum'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nama_peraturan',
        'dokumen',
        'nomor',
        'tahun'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

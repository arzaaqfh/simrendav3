<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Indikator_Pencapaian extends Model
{
    use HasFactory;
    protected $table = 'SPM_Indikator_Pencapaian';
    protected $primaryKey = 'id_indikator'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['nama_indikator'];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

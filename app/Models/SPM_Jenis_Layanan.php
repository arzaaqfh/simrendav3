<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Jenis_Layanan extends Model
{
    use HasFactory;
    protected $table = 'SPM_Jenis_Layanan';
    protected $primaryKey = 'id_jenis_layanan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nama_jenis',
        'id_bidang_urusan',
        'id_indikator'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan_90::class, 'id_bidang_urusan');
    }   
    /**
     * Get IndikatorPencapaian
     */
    public function indikator()
    {
        return $this->belongsTo(SPM_Indikator_Pencapaian::class,'id_indikator');
    }
}

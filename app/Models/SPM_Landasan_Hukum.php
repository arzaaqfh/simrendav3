<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Landasan_Hukum extends Model
{
    use HasFactory;
    protected $table = 'SPM_Landasan_Hukum';
    protected $primaryKey = 'id_landasan_hukum'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_dasar_hukum',
        'id_bidang_urusan'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
    
    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan_90::class, 'id_bidang_urusan', 'id_bidang_urusan');
    }   
    /**
     * Get Dasarhukum
     */
    public function dasarHukum()
    {
        return $this->belongsTo(SPM_Dasar_Hukum::class,'id_dasar_hukum');
    }
}

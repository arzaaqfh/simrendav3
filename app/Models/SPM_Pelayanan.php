<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Pelayanan extends Model
{
    use HasFactory;
    protected $table = 'SPM_Pelayanan';
    protected $primaryKey = 'id_pelayanan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'total_dilayani',
        'total_terlayani',
        'total_belum_terlayani',
        'id_bidang_urusan',
        'id_jenis_layanan',
        'id_tahun',
        'tw',
        'trantibum',
        'kebencanaan',
        'damkar'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Bidang Urusan
     */
    public function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan_90::class, 'id_bidang_urusan');
    }    
    /**
     * Get Jenis Layanan
     */
    public function jenisLayanan()
    {
        return $this->belongsTo(SPM_Jenis_Layanan::class, 'id_jenis_layanan');
    }    
    /**
     * Get Tahun
     */
    public function tahun()
    {
        return $this->belongsTo(Tahun::class, 'id_tahun');
    }
}

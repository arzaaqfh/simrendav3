<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_R_jenisRincian extends Model
{
    use HasFactory;
    protected $table = 'SPM_R_jenisRincian';
    protected $primaryKey = 'id_r_jenisrincian'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_jenis_layanan',
        'id_rincian'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Jenis Layanan
     */
    public function jenisLayanan()
    {
        return $this->belongsTo(SPM_Jenis_Layanan::class, 'id_jenis_layanan');
    }
    /**
     * Get Rincian Jenis Layanan
     */
    public function rincian()
    {
        return $this->belongsTo(SPM_Rincian_Jenis_Layanan::class, 'id_rincian');
    }
}

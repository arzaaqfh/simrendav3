<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Rincian_Jenis_Layanan extends Model
{
    use HasFactory;
    protected $table = 'SPM_Rincian_Jenis_Layanan';
    protected $primaryKey = 'id_rincian'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['nama_rincian'];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

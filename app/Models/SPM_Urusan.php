<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SPM_Urusan extends Model
{
    use HasFactory;
    protected $table = 'SPM_Urusan';
    protected $primaryKey = 'id_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['nama_urusan'];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

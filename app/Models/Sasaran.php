<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sasaran extends Model
{
    use HasFactory;
    protected $table = 'Sasaran';
    protected $primaryKey = 'id_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'sasaran',
        'id_misi',
        'created_at',
        'updated_at'
    ];    

    /**
     * Indikator Sasaran
     */
    public function indikatorsasaran()
    {
        return $this->hasMany(IndikatorSasaran::class, 'id_sasaran');
    }
    /**
     * Misi
     */
    public function misi()
    {
        return $this->belongsTo(Misi::class, 'id_misi');
    }

    /**
     * Relasi Sasaran Tujuan
     */
    public function relsastuj()
    {
        return $this->hasMany(RelSasTuj::class, 'id_sasaran');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SasaranPrioritasRKPD extends Model
{
    use HasFactory;
    protected $table = 'SasaranPrioritasRKPD';
    protected $primaryKey = 'id_sasaran_prioritas_rkpd'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_prioritas_rkpd',
        'sasaran_prioritas'
    ];

    /**
     * Get Prioritas RKPD
     */
    public function prioritasRKPD()
    {
        return $this->belongsTo(PrioritasRKPD::class, 'id_prioritas_rkpd');
    }
}

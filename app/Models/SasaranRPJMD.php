<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SasaranRPJMD extends Model
{
    use HasFactory;
    protected $table = 'SasaranRPJMD';
    protected $primaryKey = 'id_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'sasaran',
        'id_tujuan'
    ];

    /**
     * Get Tujuan
     */
    public function TujuanRPJMD()
    {
        return $this->belongsTo(TujuanRPJMD::class, 'id_tujuan');
    }
}

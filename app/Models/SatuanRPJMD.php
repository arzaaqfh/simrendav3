<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SatuanRPJMD extends Model
{
    use HasFactory;
    protected $table = 'SatuanRPJMD';
    protected $primaryKey = 'id_satuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'satuan'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SDGS_Coding_90 extends Model
{
    use HasFactory;
    protected $table = 'Sdgs_Coding_90';
    protected $primaryKey = 'id_coding'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_log_indikatorkota',
        'id_log_renja',
        'isDeleted'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Indikator_Global extends Model
{
    use HasFactory;
    protected $table = 'Sdgs_Global';
    protected $primaryKey = 'id_target_global'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'target_global',
        'id_sdgs',
        'isdeleted'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

        /**
     * Get Bidang Urusan
     */
    public function tujuanSdgs()
    {
         return $this->belongsTo(Sdgs_Tujuan::class, 'id_sdgs');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Indikator_Kota extends Model
{
    use HasFactory;
    protected $table = 'SDGS_IndikatorKota';
    protected $primaryKey = 'id_indikator_kota'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_provinsi',
        'nama_indikator_kota',
        'kondisi_awal',
        't1',
        't2',
        't3',
        't4',
        't5',
        'satuan',
        'sumber_data',
        'perangkat_daerah',
        'isDeleted',
        'rt1',
        'rt2',
        'rt3',
        'rt4',
        'rt5'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Indikator Provinsi SDGS
     */
    public function IndProvinsiSdgs()
    {
         return $this->belongsTo(Sdgs_Indikator_Provinsi::class, 'id_indikator_provinsi');
    }
}

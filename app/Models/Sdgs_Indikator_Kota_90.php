<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Indikator_Kota_90 extends Model
{
    use HasFactory;
    protected $table = 'SDGS_IndikatorKota_90';
    protected $primaryKey = 'id_indikator_kota'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_provinsi',
        'nama_indikator_kota',
        'satuan',
        'isDeleted'
    ];

}

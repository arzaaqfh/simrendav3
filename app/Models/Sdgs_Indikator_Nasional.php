<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Indikator_Nasional extends Model
{
    use HasFactory;
    protected $table = 'Sdgs_Nasional';
    protected $primaryKey = 'id_target_nasional'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'target_nasional',
        'id_target_global',
        'isdeleted'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

    /**
     * Get Bidang Urusan
     */
    public function GlobalSdgs()
    {
         return $this->belongsTo(Sdgs_Global::class, 'id_target_global');
    }

}
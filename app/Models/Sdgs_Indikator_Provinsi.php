<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Indikator_Provinsi extends Model
{
    use HasFactory;
    protected $table = 'Sdgs_IndikatorProvinsi';
    protected $primaryKey = 'id_indikator_provinsi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'target_provinsi',
        'id_target_nasional',
        'isdeleted'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent

}
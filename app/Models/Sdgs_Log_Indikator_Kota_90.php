<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Log_Indikator_Kota_90 extends Model
{
    use HasFactory;
    protected $table = 'SDGS_LogIndikatorKota_90';
    protected $primaryKey = 'id_log_indikatorkota'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_target_realisasi_kota',
        'id_skpd',
        'isDeleted',
    ];

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->hasMany(SKPD_90::class, 'id_skpd');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Target_Realisasi_Kota_90 extends Model
{
    use HasFactory;
    protected $table = 'SDGS_TargetRealisasiKota_90';
    protected $primaryKey = 'id_target_realisasi_kota'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_kota',
        'target',
        'anggaran',
        'realisasi_target_1',
        'capaian_target_1',
        'realisasi_anggaran_1',
        'capaian_anggaran_1',
        'realisasi_target_2',
        'capaian_target_2',
        'realisasi_anggaran_2',
        'capaian_anggaran_2',
        'catatan',
        'sumber_data',
        'lampiran',
        'tahun',
        'verified_by'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdgs_Tujuan extends Model
{
    use HasFactory;
    protected $table = 'Sdgs_Tujuan';
    protected $primaryKey = 'id_sdgs'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nama_tujuan',
        'urutan',
        'isdeleted'
    ];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingInput extends Model
{
    use HasFactory;
    protected $table = 'SettingInput';
    protected $primaryKey = 'id_setting_input'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'triwulan',
        'mulai',
        'akhir',
        'jenisInput',	
        'tahun'
    ];
}

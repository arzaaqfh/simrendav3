<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusCapaian extends Model
{
    use HasFactory;
    protected $table = 'status_capaian';
    protected $primaryKey = 'id_status_capaian'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'status_capaian'
    ];
}

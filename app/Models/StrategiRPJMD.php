<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StrategiRPJMD extends Model
{
    use HasFactory;
    protected $table = 'StrategiRPJMD';
    protected $primaryKey = 'id_strategi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'strategi'
    ];
}

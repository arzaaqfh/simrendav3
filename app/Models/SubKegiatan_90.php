<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubKegiatan_90 extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    
    use HasFactory;
    protected $table = 'SubKegiatan_90';
    protected $primaryKey = 'id_sub_kegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_kegiatan',
        'kode_sub_kegiatan',
        'nama_sub_kegiatan',
        'isDeleted'
    ];

    /**
     * Get Kegiatan
     */
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan_90::class, 'id_kegiatan');
    }

    public function logrenja90()
    {
        return $this->belongsTo(LogRenja_90::class, 'id_sub_kegiatan','id_sub_kegiatan');
    }

    /**
     * Get IndikatorKegiatan
     */
    public function indikatorsubkegiatan()
    {
        return $this->HasMany(IndikatorSubKegiatan_90::class, 'id_sub_kegiatan', 'id_sub_kegiatan');
    }

    /**
     * Get APBD Kota
     */
    public function apbd_kota()
    {
        return $this->HasMany(LogRenja_90::class, 'id_sub_kegiatan','id_sub_kegiatan');
    }

    /**
     * Get Realisasi Anggaran
     */
    public function realisasi_anggaran()
    {
        return $this->hasManyThrough(R_LogRenja_90::class,LogRenja_90::class,'id_sub_kegiatan','id_log_renja');
    }

    public function realisasi_anggaran_perubahan()
    {
        return $this->hasManyThrough(R_LogRenja_90::class,LogRenja_90::class,'id_sub_kegiatan','id_log_renja_perubahan','id_sub_kegiatan','id_log_renja');
    }

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->hasManyDeep(SKPD_90::class,
        [LogRenja_90::class,Renja_90::class],['id_sub_kegiatan','id_renja','id_skpd'],['id_sub_kegiatan','id_renja','id_skpd']);
    }

    /**
     * Get Analisis Faktor
     */
    public function analisisfaktor()
    {
        return $this->hasMany(AnalisisFaktorSubKegiatan::class,'id_sub_kegiatan');
    }
    /**
     * Get Anggaran Murni
     */
    public function anggaranmurni()
    {
        return $this->hasMany(ViewAnggaranMurniSubKegiatan::class, 'id_sub_kegiatan');
    }
    /**
     * Get Anggaran Perubahan
     */
    public function anggaranperubahan()
    {
        return $this->hasMany(ViewAnggaranPerubahanSubKegiatan::class, 'id_sub_kegiatan');
    }
    /**
     * Get Tagging Data
     */
    public function taggingdata()
    {
        return $this->hasMany(TaggingData::class,'id_sub_kegiatan');
    }
}

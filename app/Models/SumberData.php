<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SumberData extends Model
{
    use HasFactory;
    protected $table = 'SumberData';
    protected $primaryKey = 'id_sumber_data'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'sumber_data'
    ];
}

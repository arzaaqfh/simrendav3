<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaggingData extends Model
{
    use HasFactory;
    protected $table = 'TaggingData';
    protected $primaryKey = 'id_tagging'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_sub_kegiatan',
        'sdgs',
        'spm',
        'kemiskinan',
        'stunting',
        'pendidikan',
        'kesehatan',
        'kota_layak_anak',
        'ketahanan_pangan',
        'inflasi',
        'investasi',
        'ketenagakerjaan',
        'prioritas_nasional',
        'prioritas_provinsi',
        'prioritas_rpjmd',
        'prioritas_walikota',
        'prioritas_rkpd',
        'dak',
        'bankeu',
        'dif',
        'pokir',
        'infrastruktur_pelayanan_dasar',
        'grk',
        'created_at',
        'updated_at'
    ];
}

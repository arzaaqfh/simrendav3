<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    use HasFactory;
    protected $table = 'Tahun';
    protected $primaryKey = 'id_tahun'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['tahun'];
    public $timestamps = false; //supaya kolom created_at dan updated_at tidak dimanage langsung oleh eloquent
}

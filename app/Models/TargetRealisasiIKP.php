<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasiIKP extends Model
{
    use HasFactory;
    protected $table = 'TargetRealisasiIKP';
    protected $primaryKey = 'id_target_realisasi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'target',
        'realisasi',
        'keterangan',
        'formula',
        'id_sumber_data',
        'id_tahun',
        'id_indikator_program',
        'id_ketercapaian',
        'id_kategori',
        'triwulan',
        'target_perubahan'
    ];

    /**
     * Get Sumber Data
     */
    public function SumberData()
    {
        return $this->belongsTo(SumberData::class, 'id_sumber_data');
    }
    /**
     * Get Tahun
     */
    public function Tahun()
    {
        return $this->belongsTo(Tahun::class, 'id_tahun');
    }
    /**
     * Get Indikator Program
     */
    public function IndikatorProgramRPJMD()
    {
        return $this->belongsTo(IndikatorProgramRPJMD::class, 'id_indikator_program');
    }
    /**
     * Get Ketercapaian
     */
    public function KetercapaianRPJMD()
    {
        return $this->belongsTo(KetercapaianRPJMD::class, 'id_ketercapaian');
    }
    /**
     * Get Kategori
     */
    public function KategoriRPJMD()
    {
        return $this->belongsTo(KategoriRPJMD::class, 'id_kategori');
    }
}

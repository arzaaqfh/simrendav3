<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasiProgramV2 extends Model
{
    use HasFactory;
    protected $table = 'TargetRealisasiProgramV2';
    protected $primaryKey = 'id_tri_program'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_program',
        'id_renja',
        'id_rumus',
        'target',
        'target_perubahan',
        't_1',
        't_2',
        't_3',
        't_4',
        'satuan',
        'updated_at',
        'created_at'
    ];

    /**
     * Get Indikator Program
     */
    public function IndikatorProgramRPJMD()
    {
        return $this->belongsTo(IndikatorProgramRPJMD::class, 'id_indikator_program');
    }

    /**
     * Get Renja
     */
    public function Renja()
    {
        return $this->belongsTo(Renja_90::class, 'id_renja');
    }
    /**
     * Get Rumus
     */
    public function Rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
}

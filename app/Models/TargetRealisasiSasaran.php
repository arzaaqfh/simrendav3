<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasiSasaran extends Model
{
    use HasFactory;
    protected $table = 'TargetRealisasiSasaran';
    protected $primaryKey = 'id_tri_sasaran'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'awal',
        't_1',
        't_2',
        't_3',
        't_4',
        't_5',
        'akhir',
        'satuan',
        'id_periode',
        'id_indikator_sasaran',
        'id_rumus',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'r_5',
        'catatan',        
        't_1_perubahan',
        't_2_perubahan',
        't_3_perubahan',
        't_4_perubahan',
        't_5_perubahan',
        'created_at',
        'updated_at'
    ];

    
    /**
     * Indikator Sasaran
     */
    public function indikatorsasaran()
    {
        return $this->belongsTo(IndikatorSasaran::class, 'id_indikator_sasaran');
    }
    /**
     * Periode
     */
    public function periode()
    {
        return $this->belongsTo(Periode::class, 'id_periode');
    }
    /**
     * Rumus
     */
    public function rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
}

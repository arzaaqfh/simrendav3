<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasiSasaranIKU extends Model
{
    use HasFactory;
    protected $table = 'TargetRealisasiSasaranIKU';
    protected $primaryKey = 'id_target_realisasi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'target_sbp',
        'target_ssp',
        'realisasi',
        'keterangan',
        'id_sumber_data',
        'id_tahun',
        'id_indikator_sasaran',
        'id_ketercapaian',
        'id_kategori'
    ];

    /**
     * Get Sumber Data
     */
    public function SumberData()
    {
        return $this->belongsTo(SumberData::class, 'id_sumber_data');
    }
    /**
     * Get Tahun
     */
    public function Tahun()
    {
        return $this->belongsTo(Tahun::class, 'id_tahun');
    }
    /**
     * Get Indikator Sasaran
     */
    public function IndikatorSasaranRPJMD()
    {
        return $this->belongsTo(IndikatorSasaranRPJMD::class, 'id_indikator_sasaran');
    }
    /**
     * Get Ketercapaian
     */
    public function KetercapaianRPJMD()
    {
        return $this->belongsTo(KetercapaianRPJMD::class, 'id_ketercapaian');
    }
    /**
     * Get Kategori
     */
    public function KategoriRPJMD()
    {
        return $this->belongsTo(KategoriRPJMD::class, 'id_kategori');
    }
}

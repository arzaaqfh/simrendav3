<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasiTujuan extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;
    protected $table = 'TargetRealisasiTujuan';
    protected $primaryKey = 'id_tri_tujuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'awal',
        't_1',
        't_2',
        't_3',
        't_4',
        't_5',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'r_5',
        't_1',
        't_2',
        't_3',
        't_4',
        'akhir',
        'satuan',
        'id_periode',
        'id_indikator_tujuan',
        'id_rumus',
        'id_skpd',
        'catatan'
    ];  

    /**
     * Indikator Tujuan
     */
    public function indikatortujuan()
    {
        return $this->belongsTo(IndikatorTujuan::class, 'id_indikator_tujuan');
    }
    /**
     * Periode
     */
    public function periode()
    {
        return $this->belongsTo(Periode::class, 'id_periode');
    }
    /**
     * Rumus
     */
    public function rumus()
    {
        return $this->belongsTo(Rumus::class, 'id_rumus');
    }
    /**
     * SKPD
     */
    public function skpd()
    {
        return $this->belongsTo(SKPD_90::class, 'id_skpd');
    }
}

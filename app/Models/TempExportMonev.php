<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempExportMonev extends Model
{
    use HasFactory;
    protected $table = 'TempExportMonev';
    protected $primaryKey = 'id_temp_exp'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'urusan',
        'bidang_urusan',
        'program',
        'indikator_program',
        'target_ip',
        'satuan_ip',
        'kegiatan',
        'indikator_kegiatan',
        'target_ik',
        'satuan_ik',
        'sub_kegiatan',
        'indikator_subkegiatan',
        'target_isk',
        'satuan_isk',
        'anggaran',
        'perangkat_daerah'
    ];
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TindakLanjutBidangUrusan extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'TindakLanjutBidangUrusan';
    protected $primaryKey = 'id_tindak_lanjut';
    protected $fillable = [
        'id_bidang_urusan',
        'id_renja',
        'tl_triwulan',
        'tl_renja',
        'triwulan',
        'id_jafung',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tujuan extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use HasFactory;
    protected $table = 'Tujuan';
    protected $primaryKey = 'id_tujuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'tujuan',
        'id_misi'
    ];  

    /**
     * Indikator Tujuan
     */
    public function indikatortujuan()
    {
        return $this->hasMany(IndikatorTujuan::class, 'id_tujuan');
    }
    /**
     * Misi
     */
    public function misi()
    {
        return $this->belongsTo(Misi::class, 'id_misi');
    }

    /**
     * Relasi Tujuan Program
     */
    public function reltujprog()
    {
        return $this->hasMany(RelTujProg::class, 'id_tujuan');
    }
}

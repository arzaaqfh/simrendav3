<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanRPJMD extends Model
{
    use HasFactory;
    protected $table = 'TujuanRPJMD';
    protected $primaryKey = 'id_tujuan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'tujuan',
        'id_misi'
    ];

    /**
     * Get Misi RPJMD
     */
    public function MisiRPJMD()
    {
        return $this->belongsTo(MisiRPJMD::class, 'id_misi');
    }
}

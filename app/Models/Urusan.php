<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Urusan extends Model
{
    use HasFactory;
    protected $table = 'Urusan';
    protected $primaryKey = 'id_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_kategori_urusan1',
        'kode_urusan',
        'nama_urusan',
        'isDeleted',
        'id_kategori_urusan2',
        'tahun',
        'urutan'
    ];

    /**
     * Get Program
     */
    public function program()
    {
        return $this->hasMany(Program::class, 'id_urusan');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrusanRPJMD extends Model
{
    use HasFactory;
    protected $table = 'UrusanRPJMD';
    protected $primaryKey = 'id_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'nomor',
        'nama_urusan'
    ];
}

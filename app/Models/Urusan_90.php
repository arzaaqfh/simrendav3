<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Urusan_90 extends Model
{
    use HasFactory;
    protected $table = 'Urusan_90';
    protected $primaryKey = 'id_urusan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = ['kode_urusan','nama_urusan'];

    /**
     * Get BidangUrusan
     */
    public function bidangurusan()
    {
        return $this->HasMany(BidangUrusan_90::class, 'id_urusan', 'id_urusan');
    }
}

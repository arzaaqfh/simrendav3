<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Http\Middleware\TrustHosts;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    protected $primaryKey = 'id_user'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nip',
        'username',
        'id_role',
        'nama_pengguna',
        'email',
        'password',
        'level_user',
        'musrenbang',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get ViewUser
     */
    public function viewuser()
    {
        return $this->belongsTo(\App\Models\Viewuser::class, 'id_user');
    }
    
    /**
     * Level
     */
    public function level_user()
    {
        return $this->HasMany(Level::class, 'id_level', 'level_user');
    }

    /**
     * UserSKPD
     */
    public function user_skpd()
    {
        return $this->HasMany(UsersSKPD::class, 'id_user', 'id_user');
    }
}

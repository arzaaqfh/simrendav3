<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersSKPD extends Model
{
    use HasFactory;
    protected $table = 'Users_SKPD';
    protected $primaryKey = 'id'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_user',
        'id_skpd'
    ];

    /**
     * Get User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user','id_user');
    }

    /**
     * Get SKPD
     */
    public function skpd()
    {
        return $this->HasMany(SKPD_90::class, 'id_skpd','id_skpd');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerifikasiIndikatorKegiatan_90 extends Model
{
    use HasFactory;
    protected $table = 'VerifikasiIndikatorKegiatan_90';
    protected $primaryKey = 'id_verifikasi_indikatorkegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_kegiatan',
        'catatan_1',
        'status_verifikasi_1',
        'verified_by_1',
        'is_confirm_1',
        'catatan_2',
        'status_verifikasi_2',
        'verified_by_2',
        'is_confirm_2',
        'verifikator',
        'updated_at',
        'created_at'
    ];

    /**
     * Get Indikator Kegiatan
     */
    public function IndikatorKegiatan()
    {
        return $this->belongsTo(IndikatorKegiatan_90::class, 'id_indikator_kegiatan');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerifikasiIndikatorSubKegiatan_90 extends Model
{
    use HasFactory;
    protected $table = 'VerifikasiIndikatorSubKegiatan_90';
    protected $primaryKey = 'id_verifikasi_indikatorsubkegiatan'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'id_indikator_subkegiatan',
        'catatan_1',
        'status_verifikasi_1',
        'verified_by_1',
        'is_confirm_1',
        'catatan_2',
        'status_verifikasi_2',
        'verified_by_2',
        'is_confirm_2',
        'verifikator',
        'updated_at',
        'created_at'
    ];

    /**
     * Get Indikator Sub Kegiatan
     */
    public function IndikatorSubKegiatan()
    {
        return $this->belongsTo(IndikatorSubKegiatan_90::class, 'id_indikator_subkegiatan');
    }
}

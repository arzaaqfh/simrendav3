<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAnggaranMurniSubKegiatan extends Model
{
    use HasFactory;
    protected $table = 'viewanggaranmurnisubkegiatan';
    protected $fillable = [
        'id_sub_kegiatan',
        'apbd_kota',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'periode_usulan'
    ];  
}

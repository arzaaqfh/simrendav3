<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAnggaranPerubahanProgram extends Model
{
    use HasFactory;
    protected $table = 'viewanggaranperubahanprogram';
    protected $fillable = [
        'id_program',
        'apbd_kota',
        'r_1',
        'r_2',
        'r_3',
        'r_4',
        'periode_usulan'
    ];  
}

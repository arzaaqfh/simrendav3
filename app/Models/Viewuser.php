<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Viewuser extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    protected $table = 'ViewUsers';
    protected $primaryKey = 'id_user';
    protected $fillable = [
        'id_user',
        'nama_pegawai',
        'username',
        'password',
        'nip',
        'id_role',
        'nama_roles',
        'musrenbang',
        'level_user',
        'data_level',
        'nama_pengguna',
        'id_bidang',
        'id_skpd',
        'id_kota_kabupaten',
        'id_kecamatan',
        'id_kelurahan',
        'id_rw',
        'id_anggota_dprd',
        'id_jafung',
        'tahun'
    ];
}

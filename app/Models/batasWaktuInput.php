<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class batasWaktuInput extends Model
{
    use HasFactory;
    protected $table = 'batasWaktuInput';
    protected $primaryKey = 'id_bwi'; //karena kolom id tidak dinamai 'id' jadi primary key nya diset
    protected $fillable = [
        'mulai',
	    'akhir',
	    'jenisInput'
    ];
}

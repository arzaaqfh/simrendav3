CREATE TABLE Misi (
    id_misi INT NOT NULL IDENTITY PRIMARY KEY,
    nomor INT NOT NULL,
    misi VARCHAR(MAX) NOT NULL,
    id_periode INT NOT NULL,
    id_visi INT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,    
    FOREIGN KEY (id_periode) REFERENCES Periode(id_periode)
);

CREATE TABLE Tujuan (
    id_tujuan INT NOT NULL IDENTITY PRIMARY KEY,
    tujuan VARCHAR(MAX) NOT NULL,
    id_misi INT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,    
    FOREIGN KEY (id_misi) REFERENCES Misi(id_misi)
);

CREATE TABLE IndikatorTujuan (
    id_indikator_tujuan INT NOT NULL IDENTITY PRIMARY KEY,
    indikator_tujuan VARCHAR(MAX) NOT NULL,
    id_tujuan INT NOT NULL,
    FOREIGN KEY (id_tujuan) REFERENCES Tujuan(id_tujuan),
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL
);

CREATE TABLE TargetRealisasiTujuan (
    id_tri_tujuan INT NOT NULL IDENTITY PRIMARY KEY,
    awal VARCHAR(MAX) NULL,
    t_1 VARCHAR(MAX) NULL,
    t_2 VARCHAR(MAX) NULL,
    t_3 VARCHAR(MAX) NULL,
    t_4 VARCHAR(MAX) NULL,
    t_5 VARCHAR(MAX) NULL,
    r_1 VARCHAR(MAX) NULL,
    r_2 VARCHAR(MAX) NULL,
    r_3 VARCHAR(MAX) NULL,
    r_4 VARCHAR(MAX) NULL,
    r_5 VARCHAR(MAX) NULL,
    akhir VARCHAR(MAX) NULL,
    satuan VARCHAR(MAX) NULL,
    catatan TEXT NULL,
    id_periode INT NOT NULL,
    id_indikator_tujuan INT NOT NULL,
    id_rumus INT NOT NULL,
    id_skpd INT NOT NULL,
    FOREIGN KEY (id_periode) REFERENCES Periode(id_periode),
    FOREIGN KEY (id_indikator_tujuan) REFERENCES IndikatorTujuan(id_indikator_tujuan),
    FOREIGN KEY (id_rumus) REFERENCES Rumus(id_rumus),
    FOREIGN KEY (id_skpd) REFERENCES SKPD_90(id_skpd),
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL
);

INSERT INTO Misi (nomor,misi,id_periode,created_at,updated_at) VALUES (1,'[ MISI 1]Mewujudkan Kualitas Kehidupan Masyarakat Berakhlak Mulia, Berbudaya, Menerapkan Ilmu dan Teknologi, Memiliki Jejaring Sosial, Produktif dan Unggul',2,GETDATE(),GETDATE())
INSERT INTO Misi (nomor,misi,id_periode,created_at,updated_at) VALUES (2,'[ MISI 2] Mewujudkan Tata Kelola Pemerintahan yang Baik',2,GETDATE(),GETDATE())
INSERT INTO Misi (nomor,misi,id_periode,created_at,updated_at) VALUES (3,'[ MISI 3] Meningkatkan Perekonomian yang Berdaya Saing serta Berbasis Inovasi Daerah',2,GETDATE(),GETDATE())
INSERT INTO Misi (nomor,misi,id_periode,created_at,updated_at) VALUES (4,'[ MISI 4] Mewujudkan Keserasian Pembangunan yang Berkeadilan',2,GETDATE(),GETDATE())
INSERT INTO Misi (nomor,misi,id_periode,created_at,updated_at) VALUES (5,'[ MISI 5] Mewujudkan Pembangunan Berkelanjutan yang Sesuai Daya Dukung dan Daya Tampung Lingkungan',2,GETDATE(),GETDATE())

INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya aksesibilitas dan pemerataan kualitas pendidikan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya kualitas pelayanan kesehatan masyarakat',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya pengarusutamaan gender',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Perlindungan Terhadap Anak',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Pengendalian penduduk yang sesuai dengan daya tampung kota',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya partisipasi masyarakat dalam pembangunan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Partisipasi Pemuda dalam Pembangunan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Pemberdayaan Pelaku kebudayaan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Partisipasi Masyarakat dalam Berolahraga',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya keswadayaan Masyarakat',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Ketentraman dan Ketertiban Masyarakat',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Kualitas dan Inovasi Pelayanan Publik',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Kualitas Pengawasan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Kualitas Pengelolaan Pemerintahan Daerah',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya kualitas perencanaan kinerja pembangunan daerah',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Optimalisasi Tata Kelola Keuangan dan Barang Milik Daerah',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Mempercepat Pemulihan dan Pertumbuhan Ekonomi melalui Optimalisasi Potensi Daerah',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Mempercepat peningkatan pemerataan pendapatan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya kesempatan kerja',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatkan akses layanan dasar dan akses ekonomi bagi Masyarakat berpenghasilan rendah',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Berkurangnya luasan banjir',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Kualitas Lingkungan',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Kualitas Permukiman',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya pelayanan transportasi yang terpadu dan berkualitas',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Pengelolaan Keanekaragaman Hayati',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Terwujudnya Kesesuaian Pemanfaatan Ruang dengan Perencanaan Tata Ruang di Kota Cimahi',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Terwujudnya Kualitas Bangunan Gedung dalam Kondisi Baik',GETDATE(),GETDATE())
INSERT INTO Tujuan (tujuan,created_at,updated_at) VALUES ('Meningkatnya Ketahanan Bencana',GETDATE(),GETDATE())

INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Harapan lama sekolah',1,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Pembangunan Literasi Masyarakat',1,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Rata-rata Kemampuan Literasi dan Numerasi Pendidikan Dasar (SD dan SMP) Berdasarkan asesmen Nasional',1,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Rata-rata Lama Sekolah (RLS)',1,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Akreditasi Rumah Sakit',2,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Keluarga sehat',2,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase paartisipasi perempuan dalam pengambilan keputusan',3,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase partisipasi perempuan dalam kegiatan ekonomi',3,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase partisipasi perempuan dalam kegiatan politik',3,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Capaian Kota Layak Anak',4,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Contraceptive Prevalence Rate (CPR)',5,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase partisipasi masyarakat dalam perencanaan pembangunan',6,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Partisipasi Pemuda terhadap Pembangunan',7,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Pelaku kebudayaan yang diberdayakan',8,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Prosentasi masyarakat yang aktif berolahraga',9,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Jumlah partisipasi swadaya masyarakat di Kecamatan Cimahi Tengah',10,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Jumlah partisipasi swadaya masyarakat di Kecamatan Cimahi Utara',10,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Presentase Jumlah partisipasi swadaya masyarakat di Kecamatan Cimahi Selatan',10,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Gangguan Keamanan, Ketentraman dan Ketertiban Masyarakat yang dapat diselesaikan',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Pembinaan Politik Dalam Negeri',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Organisasi Kemasyarakatan/Kelompok Masyarakat yang Terbina/Terfasilitasi',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Partisipasi Masyarakat dalam Ketentraman dan Ketertiban Masyarakat Cimahi Selatan',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Partisipasi Masyarakat dalam Ketentraman dan Ketertiban Masyarakat Cimahi Tengah',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Partisipasi Masyarakat dalam Ketentraman dan Ketertiban Masyarakat Cimahi Utara',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Penurunan Angka Gangguan Katentraman dan Ketertiban Masyarakat ',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Potensi Konflik terkait Ideologi, Politik, Ekonomi, Sosial Budaya dan Trantibmas yang dapat dicegah dan ditangani',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Waktu respon tanggap kebakaran',11,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Keamanan Informasi (KAMI)',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Keterbukaan Infromasi Publik',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Pembangunan Statistik',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Profesionalisme ASN',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Sistem Pemerintahan Berbasis Elektronik (SPBE)',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Badan Penanggulangan Bencana Daerah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Badan Pengelola Pendapatan Daerah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Bakesbangpol',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi BAPPELITBANGDA',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi BKPSDMD',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi BPKAD',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Arsip Daerah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Kebudayaan, Pariwisata, Pemuda dan Olahraga',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Kesehatan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Lingkungan Hidup',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Pangan dan Pertanian',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Pemberdayaan Perempuan, Perlindungan Anak dan Pengendalian Penduduk dan Keluarga Berencana',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Pendidikan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Perhubungan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Dinas Sosial',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Disdagkoperin',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Disdukcapil',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Diskominfo',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Disnaker',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi DPKP',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi DPMPTSP',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi DPUPR',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Inspektorat',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Kecamatan Cimahi Selatan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Kecamatan Cimahi Tengah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Kecamatan Cimahi Utara',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi RSUD Cibabat',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi SATPOLPP-DAMKAR',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Sekertariat DPRD',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai IKM Administrasi Kependudukan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase hasil kelitbangan yang dimanfaatkan dalam perencanaan pembangunan daerah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Rata-rata Cakupan Kepemilikan Dokumen Kependudukan (KTP-el, KIA, Akte kelahiran 0-18 tahun)',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Perangkat Daerah yang Memanfaatkan Data Kependudukan berupa  hak akses data dan atau  tabulasi data',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Penerbitan Dokumen Administrasi Pemerintahan Tepat Waktu Cimahi Selatan',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Penerbitan Dokumen Administrasi Pemerintahan Tepat Waktu Cimahi Tengah',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Penerbitan Dokumen Administrasi Pemerintahan Tepat Waktu Cimahi Utara',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks LAKE',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Tingkat Kepuasan Masyarakat Kota Cimahi terhadap Kinerja DPRD',12,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Maturitas SPIP Terintegrasi',13,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Peningkatan Ketaatan Terhadap Peraturan Perundang-undangan',13,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Evaluasi Reformasi Birokrasi Sekretariat Daerah',14,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai LPPD',14,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai Rata-rata IKM Kota',14,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai SAKIP Kota',14,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase keselarasan kinerja antar dokumen perencanaan',15,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Rasio Utilisasi Aset Daerah',16,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Kesesuaian Laporan Keuangan Perangkat Daerah dengan Standar Akuntansi Pemerintahan',16,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Penetapan APBD tepat waktu',16,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase IKM yang berdaya saing',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Ketersediaan Bahan Pokok Masyarakat di Pasar',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Pelaku Usaha yang Berpotensi Ekspor',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Sarana dan Prasarana Perdagangan Layak Fungsi',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase PAD Terhadap Pendapatan Daerah',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Realisasi Investasi (dalam milyar rupiah)',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Jumlah Kunjungan Wisatawan Mancanegara dan Nusantara',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Lama Kunjungan Wisatawan Mancanegara dan Nusantara',17,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Nilai PPH Ketersedian',18,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Skor PPH Ketersedian',18,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Koperasi Sehat',18,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase UKM Naik Kelas',18,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Peningkatan Nilai PDRB Sektor Pertanian',18,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Tenaga Kerja Yang Ditempatkan',19,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Cakupan PPKS yang mendapatkan bantuan kerawanan pangan',20,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Cakupan PPKS yang mendapatkan fasilitasi dasar untuk memulai usaha',20,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Cakupan PPKS yang mendapatkan pelayanan infrastruktur Dasar',20,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Cakupan PPKS yang mendapatkan pelayanan Dasar Sosial',20,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase luasan banjir',21,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Kualitas Lingkungan Hidup',22,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Rasio luas kawasan kumuh',23,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Tingkat Kemantapan Jalan',24,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Tingkat Pelayanan Jalan (Level of Services)',24,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase luasan kehati Kota Cimahi yang dikelola',25,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Kesesuaian Pemanfaatan Ruang dengan Rencana Tata Ruang',26,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Penyelesaian Administrasi Pertanahan',26,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Persentase Bangunan Gedung Negara dengan Kondisi Baik',27,GETDATE(),GETDATE())
INSERT INTO IndikatorTujuan (indikator_tujuan,id_tujuan,created_at,updated_at) VALUES ('Indeks Ketahanan Daerah',28,GETDATE(),GETDATE())

INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('13,81','13,82','13,83','13,84','13,85','N/A','13,85','tahun',2,1,3,110,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('0','1','1,5','1,5','2','N/A','2','indeks',2,2,3,129,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('1,85','2,05','2,06','2,08','2,10','N/A','2,10','nilai',2,3,3,110,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('11,08','11,09','11,10','11,11','11,12','N/A','11,12','tahun',2,4,3,110,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('Utama Tingkat','Paripurna Tingkat','Paripurna Tingkat','Paripurna Tingkat','Paripurna Tingkat','N/A','Paripurna Tingkat','Paripurna Tingkat',2,5,3,2,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('0,28','0,28','0,32','0,36','0,40','N/A','0,40','indeks',2,6,3,37,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('37,19','37,50','38','38,50','39','N/A','39','persen',2,7,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('30,65','31,65','32,65','33,65','34,65','N/A','34,65','persen',2,8,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('26,67','26,67','26,67','26,67','26,67','N/A','26,67','persen',2,9,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('50','51','52','53','54','N/A','54','persen',2,10,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('64,31','65','66','67','68','N/A','68','persen',2,11,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','100','100','100','100','N/A','100','persen',2,12,3,4,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('11,2','11,2','11,3','11,4','11,5','N/A','11,5','persen ',2,13,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','50','60','85','100','N/A','100','persen',2,14,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('45,5','46','46,2','46,5','46,7','N/A','46,7','persen',2,15,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('85','85','85','85','85','N/A','85','persen',2,16,3,15,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('85','85','85','85','85','N/A','85','persen',2,17,3,14,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('85','85','85','85','85','N/A','85','persen',2,18,3,16,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,19,3,113,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,20,3,10,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','45','50','55','60','N/A','60','persen',2,21,3,10,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('75','75','75','75','75','N/A','75','persen',2,22,3,16,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('80','80','80','80','80','N/A','80','persen',2,23,3,15,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('75','75','75','75','75','N/A','75','persen',2,24,3,14,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('850','800','750','700','650','N/A','650','angka',2,25,3,113,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,26,3,10,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('15','15','15','15','15','N/A','15','menit',2,27,3,113,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','351','370','390','400','N/A','400','Indeks',2,28,3,122,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('74,91','80','84','88','92','N/A','92','persen',2,29,3,122,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('2','2','3','3','4','N/A','4','Level/Tingkat Kematangan',2,30,3,122,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('50,00','50,00','50,50','51,00','51,50','N/A','51,50','Indeks',2,31,3,123,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('2,75','3,00','3,20','3,40','3,60','N/A','3,60','Indeks',2,32,3,122,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','35','35','40','45','N/A','45','Nilai',2,33,3,104,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','30','30','35','35','N/A','35','Nilai',2,34,3,124,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','50','50,50','51','60','N/A','60','Nilai',2,35,3,10,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('63,97','64,50','65,00','65,50','66,00','N/A','66,00','Nilai',2,36,3,4,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,00','60,50','61,00','61,50','N/A','62,00','Nilai',2,37,3,123,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,0','60,5','61,0','61,5','N/A','61,5','Nilai',2,38,3,105,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('50','51','51,5','51,7','52','N/A','52','Nilai',2,39,3,129,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','50','50,4','50,5','50,6','N/A','50,6','Nilai',2,40,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('24,88','24,90','24,93','24,95','24,98','N/A','24,98','Nilai',2,41,3,37,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','50','50,5','51','51,5','N/A','51,5','Nilai',2,42,3,121,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','30','30','35','40','N/A','40','Nilai',2,43,3,118,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','30','35','35','40','N/A','40','Nilai',2,44,3,130,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','35','35','35','35','N/A','35','Nilai',2,45,3,110,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','50,1','50,50','51','51,50','N/A','51,50','Nilai',2,46,3,5,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','35','35','40','40','N/A','40','Nilai',2,47,3,114,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','30','30','35','35','N/A','35','Nilai',2,48,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('28,88','38','42','47','51','N/A','51','Nilai',2,49,3,116,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','55','55,5','56','56,5','N/A','56,5','Nilai',2,50,3,122,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60','60','60','60','N/A','60','Nilai',2,51,3,117,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','55','56','57','58','N/A','58','Nilai',2,52,3,112,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('35,67','35,7','35,8','35,9','36','N/A','36','Nilai',2,53,3,120,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','55','56','57','58','N/A','58','Nilai',2,54,3,111,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','63','64','65','66','N/A','66','Nilai',2,55,3,12,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,00','60,50','61,00','61,50','N/A','62,00','Nilai',2,56,3,16,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,00','60,50','61,00','61,50','N/A','62,00','Nilai',2,57,3,15,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,00','60,50','61,00','61,50','N/A','62,00','Nilai',2,58,3,14,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('25,5','25,55','25,57','25,60','25,62','N/A','25,62','Nilai',2,59,3,2,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,73','60,75','60,78','60,80','N/A','60,80','Nilai',2,60,3,113,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','60,73','60,75','60,78','60,80','N/A','60,80','Nilai',2,61,3,90,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('79,01','81','82','82,5','83','N/A','83','Nilai',2,62,3,116,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','75','75','75','75','N/A','75','persen',2,63,3,4,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('94,88','96,25','96,88','97,50','98,13','N/A','98,13','persen',2,64,3,116,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,65,3,116,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('80','80','80','80','80','N/A','80','persen',2,66,3,16,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('80','80','80','80','80','N/A','80','persen',2,67,3,15,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('80','80','80','80','80','N/A','80','persen',2,68,3,14,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('64,17','41','57','67','73','N/A','73','indeks',2,69,3,129,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','B','B','B','B','N/A','B','Nilai',2,70,3,90,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','3','3','3','3','N/A','3','Nilai',2,71,3,12,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','5','5','5','5','N/A','20','persen',2,72,3,12,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('59,22','60,73','60,75','60,78','60,80','N/A','60,80','Nilai',2,73,3,34,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('Tinggi','Tinggi','Tinggi','Tinggi','Tinggi','N/A','Tinggi','Nilai',2,74,3,34,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('82,37','82,37','82,37','82,37','82,37','N/A','82,37','Nilai',2,75,3,34,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('B','B','B','B','B','N/A','B','Nilai',2,76,3,34,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,77,3,4,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('64,40','72,0','73,5','75,0','76,5','N/A','76,5','persen',2,78,3,105,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,79,3,105,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,80,3,105,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','8','8','8','8','N/A','8','persen',2,81,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,82,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('5','6','7','7','7','N/A','32','persen',2,83,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('25','30','40','45','50','N/A','50','persen',2,84,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('28,24','28,73','29,22','29,72','29,72','N/A','29,72','persen',2,85,3,124,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','6,781','7,081','7,381','7,681','N/A','7,681','milyar rupiah',2,86,3,120,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('5.685','41.030','43.085','45.252','47.522','N/A','176.889','orang',2,87,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('2','2','2','2','2','N/A','2','hari',2,88,3,119,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('2318','2350','2350','2380','2380','N/A','2380','nilai',2,89,3,118,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('67,8','67,9','68','68','68','N/A','68','nilai',2,90,3,118,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('48,75','48,75','51,25','53,75','56,25','N/A','56,25','persen',2,91,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('21','21','26','31','36','N/A','36','persen',2,92,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('1,32','1,32','1,32','1,32','1,33','N/A','1,33','persen',2,93,3,118,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('77,6','78','79','80','80','N/A','80','persen',2,94,3,117,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,95,3,118,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','5','5','5','5','N/A','20','persen',2,96,3,115,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('11','34','56','78','100','N/A','100','persen',2,97,3,112,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('100','100','100','100','100','N/A','100','persen',2,98,3,114,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('0,22','0,219','0,217','0,216','0,216','N/A','0,216','persen',2,99,3,112,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('41,718','42,18','42,37','42,56','42,75','N/A','42,75','indeks',2,100,3,121,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('0,005','0,004','0,003','0,001','0,000','N/A','0,000','rasio',2,101,3,112,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('94,87','95,12','95,37','95,62','96','N/A','96','persen ',2,102,3,111,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('C','C','C','C','C','N/A','C','predikat',2,103,3,5,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('1,13','1,14','1,15','1,16','1,17','N/A','1,17','persen',2,104,3,121,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('62','62,2','62,4','62,6','62,8','N/A','62,8','persen ',2,105,3,111,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('N/A','100','100','100','100','N/A','100','persen',2,106,3,112,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('72,5','73','73,5','74','74,5','N/A','74,5','persen',2,107,3,111,GETDATE(),GETDATE())
INSERT INTO TargetRealisasiTujuan (awal,t_1,t_2,t_3,t_4,t_5,akhir,satuan,id_periode,id_indikator_tujuan,id_rumus,id_skpd,created_at,updated_at) VALUES ('0,76','0,77','0,77','0,78','0,79','N/A','0,79','(Level III / sedang) indeks',2,108,3,104,GETDATE(),GETDATE())

UPDATE TargetRealisasiTujuan SET awal = NULL WHERE awal = 'N/A'
UPDATE TargetRealisasiTujuan SET t_1 = NULL WHERE t_1 = 'N/A'
UPDATE TargetRealisasiTujuan SET t_2 = NULL WHERE t_2 = 'N/A'
UPDATE TargetRealisasiTujuan SET t_3 = NULL WHERE t_3 = 'N/A'
UPDATE TargetRealisasiTujuan SET t_4 = NULL WHERE t_4 = 'N/A'
UPDATE TargetRealisasiTujuan SET t_5 = NULL WHERE t_5 = 'N/A'
UPDATE TargetRealisasiTujuan SET akhir = NULL WHERE akhir = 'N/A'

UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 1
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 2
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 3
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 4
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 5
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 6
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 7
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 8
UPDATE Tujuan SET id_misi = 1 WHERE id_tujuan = 9
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 10
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 11
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 12
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 13
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 14
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 15
UPDATE Tujuan SET id_misi = 2 WHERE id_tujuan = 16
UPDATE Tujuan SET id_misi = 3 WHERE id_tujuan = 17
UPDATE Tujuan SET id_misi = 3 WHERE id_tujuan = 18
UPDATE Tujuan SET id_misi = 3 WHERE id_tujuan = 19
UPDATE Tujuan SET id_misi = 4 WHERE id_tujuan = 20
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 21
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 22
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 23
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 24
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 24
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 25
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 26
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 27
UPDATE Tujuan SET id_misi = 5 WHERE id_tujuan = 28

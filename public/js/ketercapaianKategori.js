$(function(){            
    $('#realisasi').on('input', function() {
        capaian();
    });

    function capaian(){
        var target = null; 
        var realisasi = parseInt($('#realisasi').val());
        var persen="";
        var rumus=$('#rumus').val();

        if ($('#target_ssp').length)
        {
            target = parseInt($('#target_ssp').val());
        }else{
            target = parseInt($('#target').val());
        }

        if(isNaN(target) || isNaN(realisasi)){
            persen=" ";
        }else{
            persen = ((realisasi/target) * 100).toFixed(2);
        }

        // ketercapaian
        if(rumus == 1) //rumus positif
        {
            if(persen > 100)
            {
                $('#ketercapaian').val(4); //melebihi capaian
            }else if(persen == 100)
            {
                $('#ketercapaian').val(2); //tercapai
            }else
            {
                $('#ketercapaian').val(3); //tidak tercapai
            }

        }else{
            if(persen > 100)
            {
                $('#ketercapaian').val(3); //tidak tercapai
            }else if(persen == 100)
            {
                $('#ketercapaian').val(2); //tercapai
            }else
            {
                $('#ketercapaian').val(4); //melebihi capaian
            }
        }

        // kategori
        if(rumus == 1) //rumus positif
        {
            if(persen > 100 || (persen >= 91 && persen <= 100))
            {
                $('#kategori').val(2); //sangat tinggi
            }else if(persen >= 76 && persen <= 90)
            {
                $('#kategori').val(3); //tinggi
            }else if(persen >= 66 && persen <= 75)
            {
                $('#kategori').val(4); //sedang
            }else if(persen >= 51 && persen <= 65)
            {
                $('#kategori').val(5); //rendah
            }else{
                $('#kategori').val(6); //sangat rendah
            }
        }else{
            if(persen > 100 || (persen >= 91 && persen <= 100))
            {
                $('#kategori').val(6); //sangat rendah
            }else if(persen >= 76 && persen <= 90)
            {
                $('#kategori').val(5); //rendah
            }else if(persen >= 66 && persen <= 75)
            {
                $('#kategori').val(4); //sedang
            }else if(persen >= 51 && persen <= 65)
            {
                $('#kategori').val(3); //tinggi
            }else{
                $('#kategori').val(2); //sangat tinggi
            }
        }
    }
});
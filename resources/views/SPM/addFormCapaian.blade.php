@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('spm.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <div class="row">
                <div class="col-12">
                    <div class="white_card mb_30">               
                        <div class="white_card_body">            
                            <form name="inputFormCapaian" action="{{ url('spm/doAddFormCapaian') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id_bidang_urusan" value="{{ $id_bidang_urusan }}" />
                                <input type="hidden" name="tw" value="{{ $tw }}" />
                                <div class="row">
                                    <div class="col col-md-1">
                                        <label for="tahun"><h5>Tahun</h5></label><br/>
                                        <div class="common_select">
                                            <select class="nice_Select wide" name="tahun" id="tahun">
                                                @foreach ($tahun as $thn)
                                                    <option value="{{ $thn->id_tahun }}">{{ $thn->tahun }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-md-2">
                                        <label><h5>Jenis Data</h5></label><br/>
                                        <div class="common_select">
                                            <select class="nice_Select wide" name="jenisData">
                                                <option value="0">Data Lainnya</option>
                                                <option value="1">Trantibum</option>
                                                <option value="2">Kebencanaan</option>
                                                <option value="3">Damkar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-md-5">
                                        <label for="jenisLayanan"><h5>Jenis Layanan</h5></label><br/>
                                        <div class="common_select">
                                            <select class="nice_Select wide" name="jenisLayanan" id="jenisLayanan">
                                                @foreach ($jenisLayanan as $jL)
                                                    <option value="{{ $jL->id_jenis_layanan }}">{{ $jL->nama_jenis }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-md-2">
                                        <label><h5>Total Dilayani</h5></label><br/>
                                        <input type="text" name="total_dilayani" class="form-control"/>
                                    </div>
                                    <div class="col col-md-2">
                                        <label><h5>Total Terlayani</h5></label><br/>
                                        <input type="text" name="total_terlayani" class="form-control"/>
                                    </div>
                                </div><br/>
                                <div class="row">
                                    <div class="col col-md-2">
                                        <label><h5>No</h5></label>
                                    </div>
                                    <div class="col col-md-4" >
                                        <label><h5>Rincian Jenis Layanan</h5></label>
                                    </div>
                                    <div class="col col-md-3" >
                                        <label><h5>Jumlah Dibutuhkan</h5></label>
                                    </div>
                                    <div class="col col-md-3" >
                                        <label><h5>Jumlah Tersedia</h5></label>
                                    </div>
                                </div>
                                @for ($i=0;$i<$jmlRincian;$i++)
                                <hr/>
                                    <div class="row">
                                        <div class="col col-md-2">
                                            <h2>{{ $i+1 }}</h2>
                                        </div>
                                        <div class="col col-md-4">
                                            <div class="common_select">
                                                <select class="nice_Select wide" name="rJenisLayanan[]">
                                                    @foreach ($rJenisLayanan as $rJL)
                                                        <option value="{{ $rJL->id_rincian }}">{{ $rJL->nama_rincian }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-md-3">
                                            <input type="text" name="jml_dibutuhkan[]" class="form-control"/>
                                        </div>
                                        <div class="col col-md-3">
                                            <input type="text" name="jml_tersedia[]" class="form-control"/>
                                        </div>
                                    </div>
                                @endFor
                                <div style="bottom:50px; right:45px; position:fixed;">
                                    <input type="submit" class="btn btn-primary" value="TAMBAH" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
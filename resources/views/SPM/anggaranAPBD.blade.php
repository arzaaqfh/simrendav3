<div class="QA_section">
    <div class="QA_table mb_30">
        <table class="table table-bordered lms_table_active">
            <thead>
                <tr>
                    <th class="align-top" width="5%"><center><b>NO.</b></center></th>
                    <th class="align-top" width="30%"><center><b>PENDANAAN</b></center></th>
                    <th class="align-top" width="5%"><center><b>PAGU</b></center></th>
                    <th class="align-top" width="10%"><center><b>ALOKASI ANGGARAN SATKER <br/> PERANGKAT DAERAH DENGAN  <br/> APBD</b></center></th>
                    <th class="align-top" width="10%"><center><b>ALOKASI ANGGARAN PENERAPAN <br/> SPM DENGAN APBD</b></center></th>
                    <th class="align-top" width="10%"><center><b>ALOKASI ANGGARAN PENERAPAN <br/> SPM DENGAN ANGGARAN <br/> SATKER PERANGKAT DAERAH</b></center></th>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataAng))
                    <tr>
                        <td class="align-top">1</td>
                        <td>ALOKASI ANGGARAN PENDAPATAN BELANJA DAERAH KOTA CIMAHI</td>
                        <td><input class="inputAngApbd" name="paguPdp" type="int" value="0"/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="align-top">2</td>
                        <td>ALOKASI ANGGARAN SATKER PERANGKAT DAERAH</td>
                        <td><input class="inputAngApbd" name="paguSatkerPd" type="int" value="0"/></td>
                        <td><center>- %</center></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="align-top">3</td>
                        <td>ALOKASI ANGGARAN PENERAPAN SPM PADA SATKER PD</td>
                        <td><input class="inputAngApbd" name="paguPnpSatker" type="int" value="0"/></td>
                        <td></td>
                        <td><center>- %</center></td>
                        <td><center>- %</center></td>
                    </tr>
                    <tr>
                        <td class="align-top">4</td>
                        <td>ALOKASI ANGGARAN PENERAPAN SPM SUMBER LAINNYA YANG SAH DAN TIDAK MENGIKAT (CONTOH : DANA DESA, KERJASAMA, DANA BAGI HASIL DAN LAIN-LAIN)</td>
                        <td><input type="int" value="0" disabled/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>A. APBN</td>
                        <td><input class="inputAngApbd" name="paguPnpApbn" type="int" value="0" disabled/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>B. DAK</td>
                        <td><input class="inputAngApbd" name="paguPnpDak" type="int" value="0" disabled/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>C. LAINNYA</td>
                        <td><input class="inputAngApbd" name="paguPnpLain" type="int" value="0" disabled/></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @else
                    <tr>
                        <td colspan="6">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

@if (!is_null($dataAng))
<div style="bottom:50px; right:45px; position:fixed;">
    <button id="btnUbahAngApbd" class="btn btn-primary">UBAH</button>
    <div class="btnSimpanAngApbd">
        <a href="{{ url('#') }}" class="btn btn-success">SIMPAN</a>
        <button id="btnBatalAngApbd" class="btn btn-danger">BATAL</button>
    </div>
</div>
<script>
    $( document ).ready(function() {
        $(".inputAngApbd").prop('disabled', true);
        $(".btnSimpanAngApbd").prop('hidden', true);
    });
    
    $("#btnUbahAngApbd").click( function (){
        $(".inputAngApbd").prop('disabled', false);
        $(".btnSimpanAngApbd").prop('hidden', false);
        $("#btnUbahAngApbd").prop('hidden', true);
    });

    $("#btnBatalAngApbd").click( function (){
        $(".inputAngApbd").prop('disabled', true);
        $(".btnSimpanAngApbd").prop('hidden', true);
        $("#btnUbahAngApbd").prop('hidden', false);
    });
</script>
@endif
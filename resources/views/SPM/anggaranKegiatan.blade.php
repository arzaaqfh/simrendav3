<div class="QA_section">
    <div class="QA_table mb_30">
        <table class="table table-bordered lms_table_active">
            <thead>
                <tr>
                    <th width="5%"><center><b>NO.</b></center></th>
                    <th><center><b>PROGRAM</b></center></th>
                    <th><center><b>KEGIATAN</b></center></th>
                    <th><center><b>SUB KEGIATAN</b></center></th>
                    <th><center><b>SATUAN</b></center></th>
                    <th><center><b>ANGGARAN (RUPIAH)</b></center></th>
                    <th><center><b>REALISASI ANGGARAN (RUPIAH)</b></center></th>
                    <th><center><b>PERSENTASE</b></center></th>
                </tr>
                <tr>
                    <td><center><b>(1)</b></center></td>
                    <td><center><b>(2)</b></center></td>
                    <td><center><b>(3)</b></center></td>
                    <td><center><b>(4)</b></center></td>
                    <td><center><b>(5)</b></center></td>
                    <td><center><b>(6)</b></center></td>
                    <td><center><b>(7)</b></center></td>
                    <td><center><b>(8)</b></center></td>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataAngKeg))
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><center><b>-</b></center></td>
                    <td><center><b>-</b></center></td>
                    <td><center><b>- %</b></center></td>
                </tr>
                @php
                    $noProg = 0   
                @endphp
                @foreach($dataAngKeg as $key => $keg)
                <tr>
                    <td><center><b>{{ $noProg+1 }}</b></center></td>
                    <td colspan="3"><b>{{ $key }}</b></td>
                    <td></td>
                    <td><center><b>-</b></center></td>
                    <td><center><b>-</b></center></td>
                    <td><center><b>- %</b></center></td>
                </tr>
                    @php
                        $noKeg = 0   
                    @endphp
                    @foreach($keg as $key2 => $subKeg)
                    <tr>
                        <td></td>
                        <td><center><b>{{ $noKeg+1 }}</b></center></td>
                        <td colspan="3"><b>{{ $key2 }}</b></td>
                        <td><center><b>-</b></center></td>
                        <td><center><b>-</b></center></td>
                        <td><center><b>- %</b></center></td>
                    </tr>
                        @php
                            $noSubKeg = 0;   
                        @endphp
                        @foreach($subKeg as $angSubKeg)                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td><center><b>{{ $noSubKeg+1 }}</b></center></td>
                            <td><b>{{ $angSubKeg['nama_sub_kegiatan'] }}</b></td>
                            <td><b>-</b></td>
                            <td>
                                @if (!is_null($angSubKeg['anggaran']))
                                {{ number_format($angSubKeg['anggaran'], 2, ',', '.')}}
                                @else
                                KOSONG
                                @endif
                            </td>
                            <td>
                                @if (!is_null($angSubKeg['realisasi_anggaran']))
                                {{ number_format($angSubKeg['realisasi_anggaran'], 2, ',', '.')}}
                                @else
                                KOSONG
                                @endif
                            </td>
                            <td><b>- %</b></td>
                        </tr>
                        @php
                            $noSubKeg++; 
                        @endphp
                        @endforeach 
                    @php
                        $noKeg++; 
                    @endphp             
                    @endforeach
                @php
                    $noProg++; 
                @endphp
                @endforeach
                @else
                    <tr>
                        <td colspan="10">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

{{-- @if (!is_null($dataAngKeg))
<div style="bottom:50px; right:45px; position:fixed;">
    <button id="btnUbahCpn" class="btn btn-primary">UBAH</button>
    <div class="btnSimpanCpn">
        <a href="{{ url('#') }}" class="btn btn-success">SIMPAN</a>
        <button id="btnBatalCpn" class="btn btn-danger">BATAL</button>
    </div>
</div>
<script>
    $( document ).ready(function() {
        $(".inputCpn").prop('disabled', true);
        $(".btnSimpanCpn").prop('hidden', true);
    });
    
    $("#btnUbahCpn").click( function (){
        $(".inputCpn").prop('disabled', false);
        $(".btnSimpanCpn").prop('hidden', false);
        $("#btnUbahCpn").prop('hidden', true);
    });

    $("#btnBatalCpn").click( function (){
        $(".inputCpn").prop('disabled', true);
        $(".btnSimpanCpn").prop('hidden', true);
        $("#btnUbahCpn").prop('hidden', false);
    });
</script>
@endif --}}
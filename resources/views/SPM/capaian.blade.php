<form id="formUpdateCpn" method="POST" action="{{ url('spm/capaian/update') }}">
{{ csrf_field() }}
@if ($id_bidang_urusan != 5)
<div class="QA_section">
    <div class="QA_table mb_30">
        <table class="table lms_table_active">
            <thead>
                <tr>
                    <th width="5%"><center><b>NO.</b></center></th>
                    <th width="20%"><center><b>INDIKATOR KINERJA / JENIS LAYANAN SPM</b></center></th>
                    <th width="50%" colspan="3"><center><b>INDIKATOR PENCAPAIAN / OUTPUT</b></center></th>
                    <th width="5%"><center><b>TOTAL PENCAPAIAN</b></center></th>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataCpn))
                    <tr>
                        <td><center><b>(1)</b></center></td>
                        <td><center><b>(2)</b></center></td>
                        <td colspan="3"><center><b>(3)</b></center></td>
                        <td><center><b>(4)</b></center></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="bg-light"><center><b>KATEGORI INDEKS PENCAPAIAN SPM</b></center></td>
                        <td class="{{ $bgKtgCpn }} text-white" colspan="3">
                            <center>
                                <b>
                                    {{ $ktgCpn }}
                                </b>
                            </center>
                        </td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$prsIP, 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    @php 
                        $number = 0;
                    @endphp
                    @foreach ($dataCpn as $key => $dat)
                    <tr>
                        <td><b>{{ $number+1 }}.</b></td>
                        <td class="bg-light"><b>{{ $key }}</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prs8020'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    
                        @foreach ($dat[0]['pelayanan'] as $ply)
                        
                        @php
                            $prsLayanan = ($ply->total_terlayani/$ply->total_dilayani)*100;
                            $dlpPrsLayanan = ($prsLayanan*80)/100;
                        @endphp    
                        <tr>
                            <td></td>
                            <td class="bg-light"><b>PERSENTASE PENCAPAIAN PENERIMA LAYANAN DASAR (80%)</b></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Harus Dilayani</b></center></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Terlayani</b></center></td>
                            <td class="bg-light"><center><b>Yang Belum Terlayani</b></center></td>
                            <td class="align-right bg-light">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly80'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>A. JUMLAH YANG HARUS DILAYANI :</b></td>
                            <td>
                                <input class="inputCpn" name="total_dilayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_dilayani }}"/>
                            </td>
                            <td>
                                <input class="inputCpn" name="total_terlayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_terlayani }}"/>
                            </td>
                            <td><b>{{ $ply->total_dilayani-$ply->total_terlayani }}</b></td>
                            <td class="align-right">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        @endforeach
                    <tr>
                        <td></td>
                        <td class="bg-light"><b>PERSENTASE PENCAPAIAN MUTU MINIMAL LAYANAN DASAR (20%)</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prsMutu20'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>B. Jumlah Mutu Barang / Jasa / SDM</b></td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Dibutuhkan</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Belum Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle"></td>
                    </tr>
                        @php
                            $numberCpn = 0;
                        @endphp
                        @foreach ($dat[0]['capaian'] as $cpn)
                            @php
                                $prsMutu = ($cpn->jml_tersedia/$cpn->jml_dibutuhkan)*100;
                            @endphp
                        <tr>
                            <td></td>
                            <td><b>{{ $numberCpn+1 }} . {{ $cpn->rincian->nama_rincian }}</b></td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_dibutuhkan[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_dibutuhkan }}"/>
                            </td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_tersedia[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_tersedia }}"/>
                            </td>
                            <td class="align-right">
                                <b>
                                    {{ $cpn->jml_dibutuhkan-$cpn->jml_tersedia }}
                                </b>
                            </td>
                            <td class="align-right">
                                <b>{{ number_format((float)$prsMutu, 2, '.', '') }} %</b>
                            </td>
                        </tr>
                            @php
                                $numberCpn++;
                            @endphp
                        @endforeach
                    @php
                        $number++;
                    @endphp                                        
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@else
<div class="QA_section">
    <div class="row bg-primary">
        <div class="col"><br/>
            <h5 style="color: white;"><b>KATEGORI PENCAPAIAN TRANTIBUM</b></h5>
        </div>
    </div>
    <div class="QA_table mb_30">
        <table class="table ">
            <thead>
                <tr>
                    <th width="5%"><center><b>NO.</b></center></th>
                    <th width="20%"><center><b>INDIKATOR KINERJA / JENIS LAYANAN SPM</b></center></th>
                    <th width="50%" colspan="3"><center><b>INDIKATOR PENCAPAIAN / OUTPUT</b></center></th>
                    <th width="5%"><center><b>TOTAL PENCAPAIAN</b></center></th>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataCpn))
                    <tr>
                        <td><center><b>(1)</b></center></td>
                        <td><center><b>(2)</b></center></td>
                        <td colspan="3"><center><b>(3)</b></center></td>
                        <td><center><b>(4)</b></center></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="bg-light text-dark"><center><b>KATEGORI INDEKS PENCAPAIAN SPM</b></center></td>
                        <td class="{{ $bgKtgCpn }} text-white" colspan="3">
                            <center>
                                <b>
                                    {{ $ktgCpn }}
                                </b>
                            </center>
                        </td>
                        <td class="align-right bg-light text-dark">
                            <b>
                                {{ number_format((float)$prsIP, 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    @php 
                        $number = 0;
                    @endphp
                    @foreach ($dataCpn as $key => $dat)
                    <tr>
                        <td><b>{{ $number+1 }}.</b></td>
                        <td class="bg-light text-dark"><b>{{ $key }}</b></td>
                        <td class="bg-light text-dark" colspan="3"></td>
                        <td class="align-right bg-light text-dark">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prs8020'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    
                        @foreach ($dat[0]['pelayanan'] as $ply)
                        
                        @php
                            $prsLayanan = ($ply->total_terlayani/$ply->total_dilayani)*100;
                            $dlpPrsLayanan = ($prsLayanan*80)/100;
                        @endphp    
                        <tr>
                            <td></td>
                            <td class="bg-light text-dark"><b>PERSENTASE PENCAPAIAN PENERIMA LAYANAN DASAR (80%)</b></td>
                            <td class="bg-light text-dark"><center><b>Jumlah Total Yang Harus Dilayani</b></center></td>
                            <td class="bg-light text-dark"><center><b>Jumlah Total Yang Terlayani</b></center></td>
                            <td class="bg-light text-dark"><center><b>Yang Belum Terlayani</b></center></td>
                            <td class="align-right bg-light text-dark">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly80'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>A. JUMLAH YANG HARUS DILAYANI :</b></td>
                            <td>
                                <input class="inputCpn" name="total_dilayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_dilayani }}"/>
                            </td>
                            <td>
                                <input class="inputCpn" name="total_terlayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_terlayani }}"/>
                            </td>
                            <td><b>{{ $ply->total_dilayani-$ply->total_terlayani }}</b></td>
                            <td class="align-right">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        @endforeach
                    <tr>
                        <td></td>
                        <td class="bg-light text-dark"><b>PERSENTASE PENCAPAIAN MUTU MINIMAL LAYANAN DASAR (20%)</b></td>
                        <td class="bg-light text-dark" colspan="3"></td>
                        <td class="align-right bg-light text-dark">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prsMutu20'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>B. Jumlah Mutu Barang / Jasa / SDM</b></td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Dibutuhkan</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Belum Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle"></td>
                    </tr>
                        @php
                            $numberCpn = 0;
                        @endphp
                        @foreach ($dat[0]['capaian'] as $cpn)
                            @php
                                $prsMutu = ($cpn->jml_tersedia/$cpn->jml_dibutuhkan)*100;
                            @endphp
                        <tr>
                            <td></td>
                            <td><b>{{ $numberCpn+1 }} . {{ $cpn->rincian->nama_rincian }}</b></td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_dibutuhkan[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_dibutuhkan }}"/>
                            </td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_tersedia[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_tersedia }}"/>
                            </td>
                            <td class="align-right">
                                <b>
                                    {{ $cpn->jml_dibutuhkan-$cpn->jml_tersedia }}
                                </b>
                            </td>
                            <td class="align-right">
                                <b>{{ number_format((float)$prsMutu, 2, '.', '') }} %</b>
                            </td>
                        </tr>
                            @php
                                $numberCpn++;
                            @endphp
                        @endforeach
                    @php
                        $number++;
                    @endphp                                        
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="QA_section">
    <div class="row bg-danger">
        <div class="col"><br/>
            <h5 style="color: white;"><b>KATEGORI PENCAPAIAN KEBENCANAAN</b></h5>
        </div>
    </div>
    <div class="QA_table mb_30">
        <table class="table ">
            <thead>
                <tr>
                    <th width="5%"><center><b>NO.</b></center></th>
                    <th width="20%"><center><b>INDIKATOR KINERJA / JENIS LAYANAN SPM</b></center></th>
                    <th width="50%" colspan="3"><center><b>INDIKATOR PENCAPAIAN / OUTPUT</b></center></th>
                    <th width="5%"><center><b>TOTAL PENCAPAIAN</b></center></th>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataCpn))
                    <tr>
                        <td><center><b>(1)</b></center></td>
                        <td><center><b>(2)</b></center></td>
                        <td colspan="3"><center><b>(3)</b></center></td>
                        <td><center><b>(4)</b></center></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="bg-light"><center><b>KATEGORI INDEKS PENCAPAIAN SPM</b></center></td>
                        <td class="{{ $bgKtgCpn }} text-white" colspan="3">
                            <center>
                                <b>
                                    {{ $ktgCpn }}
                                </b>
                            </center>
                        </td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$prsIP, 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    @php 
                        $number = 0;
                    @endphp
                    @foreach ($dataCpn as $key => $dat)
                    <tr>
                        <td><b>{{ $number+1 }}.</b></td>
                        <td class="bg-light"><b>{{ $key }}</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prs8020'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    
                        @foreach ($dat[0]['pelayanan'] as $ply)
                        
                        @php
                            $prsLayanan = ($ply->total_terlayani/$ply->total_dilayani)*100;
                            $dlpPrsLayanan = ($prsLayanan*80)/100;
                        @endphp    
                        <tr>
                            <td></td>
                            <td class="bg-light"><b>PERSENTASE PENCAPAIAN PENERIMA LAYANAN DASAR (80%)</b></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Harus Dilayani</b></center></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Terlayani</b></center></td>
                            <td class="bg-light"><center><b>Yang Belum Terlayani</b></center></td>
                            <td class="align-right bg-light">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly80'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>A. JUMLAH YANG HARUS DILAYANI :</b></td>
                            <td>
                                <input class="inputCpn" name="total_dilayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_dilayani }}"/>
                            </td>
                            <td>
                                <input class="inputCpn" name="total_terlayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_terlayani }}"/>
                            </td>
                            <td><b>{{ $ply->total_dilayani-$ply->total_terlayani }}</b></td>
                            <td class="align-right">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        @endforeach
                    <tr>
                        <td></td>
                        <td class="bg-light"><b>PERSENTASE PENCAPAIAN MUTU MINIMAL LAYANAN DASAR (20%)</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prsMutu20'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>B. Jumlah Mutu Barang / Jasa / SDM</b></td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Dibutuhkan</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Belum Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle"></td>
                    </tr>
                        @php
                            $numberCpn = 0;
                        @endphp
                        @foreach ($dat[0]['capaian'] as $cpn)
                            @php
                                $prsMutu = ($cpn->jml_tersedia/$cpn->jml_dibutuhkan)*100;
                            @endphp
                        <tr>
                            <td></td>
                            <td><b>{{ $numberCpn+1 }} . {{ $cpn->rincian->nama_rincian }}</b></td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_dibutuhkan[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_dibutuhkan }}"/>
                            </td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_tersedia[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_tersedia }}"/>
                            </td>
                            <td class="align-right">
                                <b>
                                    {{ $cpn->jml_dibutuhkan-$cpn->jml_tersedia }}
                                </b>
                            </td>
                            <td class="align-right">
                                <b>{{ number_format((float)$prsMutu, 2, '.', '') }} %</b>
                            </td>
                        </tr>
                            @php
                                $numberCpn++;
                            @endphp
                        @endforeach
                    @php
                        $number++;
                    @endphp                                        
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="QA_section">
    <div class="row bg-success">
        <div class="col"><br/>
            <h5 style="color: white;"><b>KATEGORI PENCAPAIAN DAMKAR</b></h5>
        </div>
    </div>
    <div class="QA_table mb_30">
        <table class="table ">
            <thead>
                <tr>
                    <th width="5%"><center><b>NO.</b></center></th>
                    <th width="20%"><center><b>INDIKATOR KINERJA / JENIS LAYANAN SPM</b></center></th>
                    <th width="50%" colspan="3"><center><b>INDIKATOR PENCAPAIAN / OUTPUT</b></center></th>
                    <th width="5%"><center><b>TOTAL PENCAPAIAN</b></center></th>
                </tr>
            </thead>
            <tbody>
                @if (!is_null($dataCpn))
                    <tr>
                        <td><center><b>(1)</b></center></td>
                        <td><center><b>(2)</b></center></td>
                        <td colspan="3"><center><b>(3)</b></center></td>
                        <td><center><b>(4)</b></center></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="bg-light"><center><b>KATEGORI INDEKS PENCAPAIAN SPM</b></center></td>
                        <td class="{{ $bgKtgCpn }} text-white" colspan="3">
                            <center>
                                <b>
                                    {{ $ktgCpn }}
                                </b>
                            </center>
                        </td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$prsIP, 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    @php 
                        $number = 0;
                    @endphp
                    @foreach ($dataCpn as $key => $dat)
                    <tr>
                        <td><b>{{ $number+1 }}.</b></td>
                        <td class="bg-light"><b>{{ $key }}</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prs8020'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    
                        @foreach ($dat[0]['pelayanan'] as $ply)
                        
                        @php
                            $prsLayanan = ($ply->total_terlayani/$ply->total_dilayani)*100;
                            $dlpPrsLayanan = ($prsLayanan*80)/100;
                        @endphp    
                        <tr>
                            <td></td>
                            <td class="bg-light"><b>PERSENTASE PENCAPAIAN PENERIMA LAYANAN DASAR (80%)</b></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Harus Dilayani</b></center></td>
                            <td class="bg-light"><center><b>Jumlah Total Yang Terlayani</b></center></td>
                            <td class="bg-light"><center><b>Yang Belum Terlayani</b></center></td>
                            <td class="align-right bg-light">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly80'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>A. JUMLAH YANG HARUS DILAYANI :</b></td>
                            <td>
                                <input class="inputCpn" name="total_dilayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_dilayani }}"/>
                            </td>
                            <td>
                                <input class="inputCpn" name="total_terlayani[{{ $ply->id_pelayanan }}]" type="int" value="{{ $ply->total_terlayani }}"/>
                            </td>
                            <td><b>{{ $ply->total_dilayani-$ply->total_terlayani }}</b></td>
                            <td class="align-right">
                                <b>
                                    {{ number_format((float)$dataCpn[$key]['prsPly'], 2, '.', '')}} %
                                </b>
                            </td>
                        </tr>
                        @endforeach
                    <tr>
                        <td></td>
                        <td class="bg-light"><b>PERSENTASE PENCAPAIAN MUTU MINIMAL LAYANAN DASAR (20%)</b></td>
                        <td class="bg-light" colspan="3"></td>
                        <td class="align-right bg-light">
                            <b>
                                {{ number_format((float)$dataCpn[$key]['prsMutu20'], 2, '.', '')}} %
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>B. Jumlah Mutu Barang / Jasa / SDM</b></td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Dibutuhkan</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle">
                            <center>
                            <b>Jumlah Mutu Barang / Jasa yang Belum Tersedia</b><br/>
                            </center>
                        </td>
                        <td class="align-middle"></td>
                    </tr>
                        @php
                            $numberCpn = 0;
                        @endphp
                        @foreach ($dat[0]['capaian'] as $cpn)
                            @php
                                $prsMutu = ($cpn->jml_tersedia/$cpn->jml_dibutuhkan)*100;
                            @endphp
                        <tr>
                            <td></td>
                            <td><b>{{ $numberCpn+1 }} . {{ $cpn->rincian->nama_rincian }}</b></td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_dibutuhkan[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_dibutuhkan }}"/>
                            </td>
                            <td class="align-right">
                                <input class="inputCpn" name="jml_tersedia[{{$cpn->id_capaian}}]" type="int" value="{{ $cpn->jml_tersedia }}"/>
                            </td>
                            <td class="align-right">
                                <b>
                                    {{ $cpn->jml_dibutuhkan-$cpn->jml_tersedia }}
                                </b>
                            </td>
                            <td class="align-right">
                                <b>{{ number_format((float)$prsMutu, 2, '.', '') }} %</b>
                            </td>
                        </tr>
                            @php
                                $numberCpn++;
                            @endphp
                        @endforeach
                    @php
                        $number++;
                    @endphp                                        
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <center>
                                DATA BELUM ADA
                            </center>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endif
</form>

@if (!is_null($dataCpn))
<div style="bottom:50px; right:45px; position:fixed;">
    <button id="btnUbahCpn" class="btn btn-primary">UBAH</button>
    <div class="btnSimpanCpn">
        <button id="btnSimpanCpn" class="btn btn-success">SIMPAN</button>
        <button id="btnBatalCpn" class="btn btn-danger">BATAL</button>
    </div>
</div>
<script>
    $("#btnSimpanCpn").click( function (){
        $("#formUpdateCpn").submit();
    });

    $( document ).ready(function() {
        $(".inputCpn").prop('disabled', true);
        $(".btnSimpanCpn").prop('hidden', true);
    });
    
    $("#btnUbahCpn").click( function (){
        $(".inputCpn").prop('disabled', false);
        $(".btnSimpanCpn").prop('hidden', false);
        $("#btnUbahCpn").prop('hidden', true);
    });

    $("#btnBatalCpn").click( function (){
        $(".inputCpn").prop('disabled', true);
        $(".btnSimpanCpn").prop('hidden', true);
        $("#btnUbahCpn").prop('hidden', false);
    });
</script>
@endif
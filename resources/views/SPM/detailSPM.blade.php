@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('spm.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SPM</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm/penerapan') }}">Penerapan SPM</a></li>
                                <li class="breadcrumb-item active">Detail SPM</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">                    
                    <div class="white_card card_height_10 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Detail SPM</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_20 mb_30">
                        <div class="white_card_header">
                            <div class="row">
                                <div class="col col-sm-2"><b>Kabupaten / Kota</b></div>
                                <div class="col col-sm-10">: Cimahi</div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2"><b>Bidang Urusan</b></div>
                                <div class="col col-sm-10">:
                                    @foreach ($bidangUrusan as $bu)
                                        {{ $bu->nama_bidang_urusan }}
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2"><b>Dasar Hukum</b></div>
                                <div class="col col-sm-10"> :<br/>
                                    <ul>
                                        @foreach ($landasanHukum as $lh)
                                        <li>
                                            @php
                                                if(is_null(session('tahun_spm')))
                                                {
                                                    $tahun_spm = $tahun->first()->tahun;
                                                }else{
                                                    $tahun_spm = session('tahun_spm');
                                                }
                                            @endphp
                                            @if ($tahun_spm >= 2022 && ($lh->dasarHukum->id_dasar_hukum == 10 || $lh->dasarHukum->id_dasar_hukum == 8))
                                                <a href="{{ url('storage/doct/PMDN59.pdf') }}">
                                                    Peraturan Menteri Dalam Negeri Republik Indonesia Nomor 59 Tahun 2021 Tentang Penerapan Standar Pelayanan Minimal
                                                </a>
                                            @else
                                                <a href="{{ url('storage/doct/'.$lh->dasarHukum->dokumen) }}">
                                                    {{ $lh->dasarHukum->nama_peraturan }}
                                                </a>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2"><b>Triwulan</b></div>
                                <div class="col col-sm-10">: {{ $tw }}</div>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>            
            <div class="row">
                <div class="col-12">
                    <div class="white_card mb_30">
                        <div class="white_card_header">
                            <div class="bulder_tab_wrapper">
                                <ul  class="nav" id="myTabDetailSPM" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" id="spm-capaian-tab" data-bs-toggle="tab" href="#spm-capaian" role="tab" aria-controls="spm-capaian" aria-selected="true">
                                            Capaian SPM
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="spm-anggaran-apbd-tab" data-bs-toggle="tab" href="#spm-anggaran-apbd" role="tab" aria-controls="spm-anggaran-apbd" aria-selected="true">
                                            Data APBD, Anggaran Satker PD dan Anggaran SPM pada Satker PD
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="spm-anggaran-kegiatan-tab" data-bs-toggle="tab" href="#spm-anggaran-kegiatan" role="tab" aria-controls="spm-anggaran-kegiatan" aria-selected="false">
                                            Rincian Kegiatan dan Anggaran SPM
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>                   
                        <div class="white_card_body">            
                            <div class="tab-content" id="myTabDetailSPMContent" style="padding-top: 25px;">
                                <div class="tab-pane fade" id="spm-capaian" role="tabpanel" aria-labelledby="spm-capaian-tab">
                                    @include('SPM.capaian')
                                </div>
                                <div class="tab-pane fade" id="spm-anggaran-apbd" role="tabpanel" aria-labelledby="spm-anggaran-apbd-tab">
                                    @include('SPM.anggaranAPBD')
                                </div>
                                <div class="tab-pane fade" id="spm-anggaran-kegiatan" role="tabpanel" aria-labelledby="spm-anggaran-kegiatan-tab">
                                    @include('SPM.anggaranKegiatan')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                  
        </div>
    </div>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('spm.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Komponen Lainnya</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Indikator Pencapaian</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input type="text" placeholder="Search content here...">
                                                    </div>
                                                    <button type="submit"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="add_button ms-2">
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#addcategory" class="btn_1">Add New</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table class="table lms_table_active">
                                        <thead>
                                            <tr>
                                                <th scope="col" width="5%">No.</th>
                                                <th scope="col">Nama Indikator Pencapaian</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data as $key => $dshow)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $dshow->nama_indikator }}</td>
                                                <td>
                                                    <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#deleteModal_{{ $dshow->id_indikator }}">
                                                        Delete
                                                    </a> 
                                                    <!-- Delete Modal-->
                                                    <div class="modal fade" id="deleteModal_{{ $dshow->id_indikator }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{ $dshow->id_indikator }}"
                                                    aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="deleteModalLabel_{{ $dshow->id_indikator }}">Anda akan menghapus "{{ $dshow->nama_indikator }}"?</h5>
                                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">Klik tombol "Delete" di bawah ini untuk menghapus data yang dipilih.</div>
                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>                                                                
                                                                    <a class="btn btn-danger" href="{{ url('spm/indikatorPencapaian/destroy/'.encrypt($dshow->id_indikator)) }}">Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
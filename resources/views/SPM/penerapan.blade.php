@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('spm.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Penerapan SPM</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SPM</a></li>
                                <li class="breadcrumb-item active">Penerapan SPM</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_10 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Tingkat Keterisian Aplikasi</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_30 mb_30">
                        <div class="white_card_body">
                            <div class="row alert alert-success">
                                <div class="col col-md-2">
                                    Baik
                                </div>
                                <div class="col col-md-2">
                                    81-100 %
                                </div>
                            </div>
                            <div class="row alert alert-warning">
                                <div class="col col-md-2">
                                    Cukup
                                </div>
                                <div class="col col-md-2">
                                    51-80 %
                                </div>
                            </div>
                            <div class="row alert alert-danger">
                                <div class="col col-md-2">
                                    Kurang
                                </div>
                                <div class="col col-md-2">
                                    <= 50 %
                                </div>
                            </div>
                            <div class="row alert alert-dark">
                                <div class="col col-md-2">
                                    Tidak Ada
                                </div>
                                <div class="col col-md-2">
                                    0 %
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_10 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Data Bidang Urusan</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_80 mb_30">
                        <div class="white_card_body">
                            <div class="QA_section">  
                                <div class="QA_table mb_30">
                                    <table class="table lms_table_active">
                                        <thead>
                                            <tr>
                                                <th class="align-middle" width="5%" rowspan="2">No.</th>
                                                <th class="align-middle" width="45%" rowspan="2">Bidang Urusan</th>
                                                <th class="align-middle" width="50%" colspan="4">                                                    
                                                @php
                                                    if(is_null(session('tahun_spm')))
                                                    {
                                                        $tahun_spm = $tahun->first()->tahun;
                                                    }else{
                                                        $tahun_spm = session('tahun_spm');
                                                    }
                                                @endphp
                                                    <center>
                                                        Status Input <b>({{ $tahun_spm }})</b>
                                                    </center>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle"><center>TW 1</center></th>
                                                <th class="align-middle"><center>TW 2</center></th>
                                                <th class="align-middle"><center>TW 3</center></th>
                                                <th class="align-middle"><center>TW 4</center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($status as $key => $dshow)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $dshow['nama_bidang_urusan'] }}</td>
                                                <td>
                                                    <a href="{{ url('spm/detailSPM/'.$dshow['id_bidang_urusan'].'/'.$dshow['id_tahun'].'/1') }}"
                                                        class="btn {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_1']['class'] }} btn-xs"
                                                        style="width: 100%"
                                                    >
                                                        {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_1']['text'] }}<br/> 
                                                        ({{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_1']['persen'] }} %)
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('spm/detailSPM/'.$dshow['id_bidang_urusan'].'/'.$dshow['id_tahun'].'/2') }}"
                                                        class="btn {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_2']['class'] }} btn-xs"
                                                        style="width: 100%"
                                                    >
                                                        {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_2']['text'] }}<br/> 
                                                        ({{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_2']['persen'] }} %)
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('spm/detailSPM/'.$dshow['id_bidang_urusan'].'/'.$dshow['id_tahun'].'/3') }}"
                                                        class="btn {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_3']['class'] }} btn-xs"
                                                        style="width: 100%"
                                                    >
                                                        {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_3']['text'] }}<br/> 
                                                        ({{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_3']['persen'] }} %)
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('spm/detailSPM/'.$dshow['id_bidang_urusan'].'/'.$dshow['id_tahun'].'/4') }}"
                                                        class="btn {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_4']['class'] }} btn-xs"
                                                        style="width: 100%"
                                                    >
                                                        {{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_4']['text'] }}<br/> 
                                                        ({{ $persenInput['id_bidang_urusan_'.$dshow['id_bidang_urusan']]['tw_4']['persen'] }} %)
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    @if (Auth::user()->nama_pengguna == 'Admin' || Auth::user()->nama_pengguna == 'Admin 2')
                    <div class="white_card card_height_10 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Tambahkan Data Capaian</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_30 mb_30">
                        <div class="white_card_body">
                            <div class="row">
                                <div class="col">
                                    <label for="bidangUrusan"><h5>Bidang Urusan</h5></label><br/>
                                    <select name="bidangUrusan" id="bidangUrusan" class="form-control">
                                    @foreach ($data as $key => $bu)
                                        <option value="{{ $bu->id_bidang_urusan }}">{{ $bu->nama_bidang_urusan }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="tw"><h5>Triwulan</h5></label><br/>
                                    <select name="tw" id="tw" class="form-control">
                                        <option value="1">Triwulan 1</option>
                                        <option value="2">Triwulan 2</option>
                                        <option value="3">Triwulan 3</option>
                                        <option value="4">Triwulan 4</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="jmlRincian"><h5>Jumlah Rincian</h5></label><br/>
                                    <input type="text" id="jmlRincian" name="jmlRincian" value="" placeholder="jumlah rincian" class="form-control"/>
                                </div>
                                <div class="col">
                                    <label for="jmlRincian"><h5>Aksi</h5></label><br/>
                                    <button class="btn btn-primary" id="btnAddFormCapaian">TAMBAH</button>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#btnAddFormCapaian').click( function () {
            var bidangUrusan = $('select[name=bidangUrusan] option').filter(':selected').val();
            var tw = $('select[name=tw] option').filter(':selected').val();
            var jmlRincian = $('#jmlRincian').val();
            
            window.location = "{{ url('spm/addFormCapaian') }}/"+bidangUrusan+"/"+tw+"/"+jmlRincian;
        });
    </script>
    <!-- Footer -->
    @include('layouts.footer')
@endsection
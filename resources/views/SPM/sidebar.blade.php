<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-primary">
            <a class="large_logo" href="{{ url('home') }}">
                <img src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="34px">
                <h5 class="text-light"><b>Standar Pelayanan Minimal</b></h5>
            </a>
             
        <a class="small_logo" href="{{ url('home') }}">
            <img src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>SPM</b></h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">        
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4> 
        <li>
            <a href="{{ url('home') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-home" ></i>
                </div>
                <div class="nav_title">
                    <span>Home</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/spm') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard</span>
                </div>
            </a>
        </li>

        <h4 class="menu-text"><span>MENU UTAMA</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/5.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Hukum </span>
                </div>
            </a>
            <ul>
              <li>
                <a href="{{ url('/spm/dasarHukum') }}">Dasar Hukum</a>
              </li>
              <li>
                <a href="{{ url('/spm/landasanHukum') }}">Landasan Hukum</a>
              </li>
            </ul>
        </li>        
        <li class="">
            <a href="{{ url('/spm/penerapan') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/2.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Penerapan SPM </span>
                </div>
            </a>
        </li>

        <h4 class="menu-text"><span>MENU LAINNYA</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li class="">
            <a  class="has-arrow" href="#" aria-expanded="false">
              <div class="nav_icon_small">
                  <img src="{{ asset('storage/img/menu-icon/4.svg') }}" alt="">
              </div>
              <div class="nav_title">
                  <span>Komponen Lainnya </span>
              </div>
            </a>
            <ul>
              <li>
                  <a href="{{ url('/spm/jenisLayanan') }}">Jenis Layanan (JL)</a>
              </li>
              <li>
                  <a href="{{ url('/spm/rincianJenisLayanan') }}">Rincian Jenis Layanan (RJL)</a>
              </li>
              <li>
                  <a href="{{ url('/spm/indikatorPencapaian') }}">Indikator Pencapaian (IP)</a>
              </li>
              <li>
                  <a href="{{ url('/spm/relasiJenisRincian') }}">Relasi JL & RJL</a>
              </li>
            </ul>
        </li>

      </ul>
</nav>
 <!--/ sidebar  -->
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('administrator.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('administrator') }}">Administrator</a></li>
                                <li class="breadcrumb-item active">RKPD</li>
                            </ol>
                        </div>
                        <div class="page_title_right">
                            <div class="page_date_button">
                                {{ date_format($tanggal, 'd M Y') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="app">
                <urusan_90-component></urusan_90-component>
            </div>
        </div>
    </div>     

    <!-- Footer -->
    @include('layouts.footer')
@endsection
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('administrator.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <form method="POST" action="{{ url('administrator/monev/rkpd/create/indikatorProgram') }}">
                        {{ csrf_field() }}
                        <div class="white_card card_height_100 mb_30">
                            <div class="white_card_header">
                                <div class="box_header m-0">
                                    <div class="main-title">
                                        <h3 class="m-0">Indikator Program</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="white_card_body">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label class="form-label" for="indikatorProgram">Indikator Program</label>
                                            <input name="indikatorProgram" type="text" class="form-control" id="indikatorProgram" placeholder="" value="-">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label class="form-label" for="program">Program</label>
                                            <div class="common_select">
                                                <select name="program" id="program" class="nice_Select wide mb_30">
                                                    <option selected="">Pilih...</option>
                                                    @foreach ($program as $dataProgram)
                                                        <option value="{{ $dataProgram->id_program }}">
                                                            {{ $dataProgram->program }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">                         
                                        <div class="col-md-4">
                                            <label class="form-label" for="satuan">Satuan</label>
                                            <div class="common_select">
                                                <select name="satuan" id="satuan" class="nice_Select wide mb_30">
                                                    <option selected="">Pilih...</option>
                                                    @foreach ($satuan as $dataSatuan)
                                                        <option value="{{ $dataSatuan->id_satuan }}">
                                                            {{ $dataSatuan->satuan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label" for="rumus">Rumus</label>
                                            <div class="common_select">
                                                <select name="rumus" id="rumus" class="nice_Select wide mb_30">
                                                    <option>Pilih...</option>
                                                    @foreach ($rumus as $dataRumus)
                                                        <option value="{{ $dataRumus->id_rumus }}">
                                                            {{ $dataRumus->rumus }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label" for="skpd">SKPD</label>
                                            <div class="common_select">
                                                <select name="skpd" id="skpd" class="nice_Select wide mb_30">
                                                <option selected="">Pilih...</option>
                                                    @foreach ($skpd as $dataSkpd)
                                                        <option value="{{ $dataSkpd->id_skpd }}">
                                                            {{ $dataSkpd->nama_skpd }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="white_card card_height_100 mb_30">
                            <div class="white_card_body">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="{{ url('administrator/monev/rkpd/indikatorProgram') }}" class="btn btn-secondary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   

    <!-- Footer -->
    @include('layouts.footer')
@endsection
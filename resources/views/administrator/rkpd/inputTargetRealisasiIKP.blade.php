@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('administrator.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <form method="POST" action="{{ url('administrator/monev/rkpd/create/targetRealisasiIKP') }}">
                        {{ csrf_field() }}
                        <div class="white_card card_height_100 mb_30">
                            <div class="white_card_header">
                                <div class="box_header m-0">
                                    <div class="main-title">
                                        <h3 class="m-0">Target Realisasi IKP</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="white_card_body">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label class="form-label" for="indikatorProgram">Indikator Program</label>
                                            <div class="common_select">
                                                <select name="indikatorProgram" id="indikatorProgram" class="nice_Select wide mb_30">
                                                    <option selected="">Pilih...</option>
                                                    @foreach ($indikatorProgram as $indikatorP)
                                                        <option value="{{ $indikatorP->id_indikator_program }}">
                                                            {{$indikatorP->indikator_program .' | '}} <b>({{ $indikatorP->SKPD->nama_skpd }})</b>
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-2">
                                            <label class="form-label" for="target">Target</label>
                                            <input name="target" type="text" class="form-control" id="target" placeholder="" value="-">
                                        </div>
                                        <div class="col-md-2">
                                            <label class="form-label" for="realisasi">Realisasi</label>
                                            <input name="realisasi" type="text" class="form-control" id="realisasi" placeholder="" value="-">
                                        </div> 
                                        <div class="col-md-2">
                                            <label class="form-label" for="tahun">Tahun</label>
                                            <div class="common_select">
                                                <select name="tahun" id="tahun" class="nice_Select wide mb_30">
                                                    <option selected="">Pilih...</option>
                                                    @foreach ($tahun as $dataTahun)
                                                        <option value="{{ $dataTahun->id_tahun }}">
                                                            {{ $dataTahun->tahun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>                             
                                        <div class="col-md-2">
                                            <label class="form-label" for="sumberData">Sumber Data</label>
                                            <div class="common_select">
                                                <select name="sumberData" id="sumberData" class="nice_Select wide mb_30">
                                                    <option selected="">Pilih...</option>
                                                    @foreach ($sumberData as $dataSumberData)
                                                        <option value="{{ $dataSumberData->id_sumber_data }}">
                                                            {{ $dataSumberData->sumber_data }}
                                                        </option>
                                                    @endforeach
                                                    <option value="lainnya">Lainnya</option>
                                                </select>
                                            </div>
                                            <input class="form-control" type="text" id="sumberDataLainnya" name="sumberDataLainnya" placeholder="sumber lain"/>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="form-label" for="ketercapaian">Ketercapaian</label>
                                            <div class="common_select">
                                                <select name="ketercapaian" id="ketercapaian" class="nice_Select wide mb_30">
                                                    <option>Pilih...</option>
                                                    @foreach ($ketercapaian as $dataKetercapaian)
                                                        <option value="{{ $dataKetercapaian->id_ketercapaian }}">
                                                            {{ $dataKetercapaian->ketercapaian }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="form-label" for="kategori">Kategori</label>
                                            <div class="common_select">
                                                <select name="kategori" id="kategori" class="nice_Select wide mb_30">
                                                <option selected="">Pilih...</option>
                                                    @foreach ($kategori as $dataKategori)
                                                        <option value="{{ $dataKategori->id_kategori }}">
                                                            {{ $dataKategori->kategori }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label class="form-label" for="formula">Formula</label>
                                            <textarea class="form-control" name="formula" id="formula" cols="30" rows="10">-</textarea>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12">
                                            <label class="form-label" for="keterangan">Keterangan</label>
                                            <textarea class="form-control" name="keterangan" id="keterangan" cols="30" rows="10">-</textarea>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="white_card card_height_100 mb_30">
                            <div class="white_card_body">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="{{ url('administrator/monev/rkpd/targetRealisasiIKP') }}" class="btn btn-secondary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   

    <!-- Footer -->
    @include('layouts.footer')
@endsection
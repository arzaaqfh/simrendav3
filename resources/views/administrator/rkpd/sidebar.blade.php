<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-green-custom-1">
            <a class="large_logo" href="{{ url('home') }}">
                <img src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="34px">
                <h5 class="text-light"><b>Administrator-RKPD</b></h5>
            </a>
             
        <a class="small_logo" href="{{ url('home') }}">
            <img src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>Admin-RKPD</b></h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu" class="metismenu">        
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4> 
        <li>
            <a href="{{ url('home') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-home" ></i>
                </div>
                <div class="nav_title">
                    <span>Home</span>
                </div>
            </a>
        </li> 
        <li>
            <a href="{{ url('/administrator') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Administrator</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Admin RKPD</span>
                </div>
            </a>
        </li>
        
        <h4 class="menu-text"><span>Daftar Tabel</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/targetRealisasiTujuanIKU') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Target Realisasi Tujuan IKU</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/targetRealisasiSasaranIKU') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Target Realisasi Sasaran IKU</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/targetRealisasiIKK') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Target Realisasi IKK</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/targetRealisasiIKP') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Target Realisasi IKP</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/indikatorTujuanIKU') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Indikator Tujuan IKU</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/indikatorSasaranIKU') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Indikator Sasaran IKU</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/indikatorKinerja') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Indikator Kinerja</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('/administrator/monev/rkpd/indikatorProgram') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Indikator Program</span>
                </div>
            </a>
        </li>
    </ul>
</nav>
<!--/ sidebar  -->

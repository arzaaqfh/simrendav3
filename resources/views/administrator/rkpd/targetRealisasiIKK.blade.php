@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('administrator.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('administrator') }}">Administrator</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('administrator/monev/rkpd') }}">RKPD</a></li>
                                <li class="breadcrumb-item active">Target Realisasi IKK</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Tabel</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Target Realisasi IKK</h4>
                                </div>
                                <div class="QA_table mb_30">
                                    <div class="dataTables_btn btn-group" role="group" aria-label="Basic example">
                                    
                                    </div>
                                    <table id="tabelTargetRealisasiIKK" class="table multiplegroup tableFiltered">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Indikator Kinerja</th>
                                                <th>Target</th>
                                                <th>Realisasi</th>
                                                <th>Satuan</th>
                                                <th>Ketercapaian</th>
                                                <th>Kategori</th>
                                                <th>Tahun</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   

    <!-- Footer -->
    @include('layouts.footer')
@endsection
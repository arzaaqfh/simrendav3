@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('administrator.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('administrator') }}">Administrator</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('administrator/monev/rkpd') }}">RKPD</a></li>
                                <li class="breadcrumb-item active">Target Realisasi Sasaran IKU</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Tabel</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Target Realisasi Sasaran IKU</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" placeholder="Search content here...">
                                                    </div>
                                                    <button type="submit"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table class="table table_trsasaraniku">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Indikator Sasaran</th>
                                                <th>Target (Sebelum Perubahan)</th>
                                                <th>Target (Sesudah Perubahan)</th>
                                                <th>Realisasi</th>
                                                <th>Satuan</th>
                                                <th>Ketercapaian dan Kategori</th>
                                                <th>Tahun</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>                          
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_trsasaraniku').DataTable({
                dom: 'lrtip',                
                lengthMenu: [10, 20, 50, 100],
                ajax: {
                    url:"{{ url('/api/get/TargetRealisasiSasaranIKU') }}"
                },
                columns: [
                    {data: 'id_target_realisasi',orderable: false,searchable: false},
                    {data: 'indikator_sasaran_r_p_j_m_d.indikator_sasaran', name: 'indikator_sasaran'},
                    {data: 'target_sbp', name: 'target_sebelum_perubahan'},
                    {data: 'target_ssp', name: 'target_sesudah_perubahan'},
                    {data: 'realisasi', name: 'realisasi'},
                    {data: 'indikator_sasaran_r_p_j_m_d.satuan_r_p_j_m_d.satuan', name: 'satuan'},
                    {
                        data: 'target_ssp',
                        render: function( data, type, full, meta) {
                            var bg_ketercapaian = "bg-light";
                            var bg_kategori = "bg-light";
                            var persen = 0;
                            var target = parseFloat(full.target_ssp.replace(',','.'));  
                            var realisasi = parseFloat(full.realisasi.replace(',','.'));
                            var returnText = "";
                            
                            if(target != NaN && realisasi != NaN && target > 0)
                            {
                                if(full.indikator_tujuan_r_p_j_m_d.id_rumus == 1) //rumus positif
                                {
                                    persen = Math.round((realisasi/target)*100);
                                }else{
                                    persen = 100-Math.round((realisasi/target)*100);
                                }
                            }
                            // ketercapaian
                            if(full.id_ketercapaian == 2)
                            {
                                bg_ketercapaian = "bg-success"; // tercapai
                            }else if(full.id_ketercapaian == 3)
                            {
                                bg_ketercapaian = "bg-danger"; // tidak tercapai
                            }else if(full.id_ketercapaian == 4)
                            {
                                bg_ketercapaian = "bg-info"; // melebihi capaian
                            }else if(full.id_ketercapaian == 5)
                            {
                                bg_ketercapaian = "bg-warning"; // tidak ada data
                            }else
                            {
                                bg_ketercapaian = "bg-light"; // -
                            }

                            // kategori
                            if(full.id_kategori == 2)
                            {
                                bg_kategori = "bg-green-custom-1"; // sangat tinggi
                            }else if(full.id_kategori == 3)
                            {
                                bg_kategori = "bg-success"; // tinggi
                            }else if(full.id_kategori == 4)
                            {
                                bg_kategori = "bg-orange"; // sedang
                            }else if(full.id_kategori == 5)
                            {
                                bg_kategori = "bg-warning"; // rendah
                            }else if(full.id_kategori == 6)
                            {
                                bg_kategori = "bg-danger"; // sangat rendah
                            }else
                            {
                                bg_kategori = "bg-light"; // -
                            }

                            returnText =    "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <span class='badge "+bg_ketercapaian+" mb-2'>"+full.ketercapaian_r_p_j_m_d.ketercapaian+"</span>"+
                                            "    </div>"+
                                            "</div>"+
                                            "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <span class='badge "+bg_kategori+" mb-2'>"+full.kategori_r_p_j_m_d.kategori+"</span>"+
                                            "    </div>"+
                                            "</div>"+
                                            "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <br/>"+
                                            "        <div class='single_progressbar'>"+
                                            "            <div id='bar"+full.id_target_realisasi+"' class='barfiller'>"+
                                            "                <div class='tipWrap' style='display: inline;'>"+
                                            "                    <span class='tip' style='left: 116.095px; transition: left 2.2s ease-in-out 0s;'>"+persen+"</span>"+
                                            "                </div>"+
                                            "                <span class='fill' data-percentage='"+persen+"' style='background: rgb(253, 60, 151); width: 139.611px; transition: width 2.2s ease-in-out 0s;'></span>"+
                                            "            </div>"+
                                            "        </div>"+
                                            "    </div>"+
                                            "</div>";
                            return returnText;
                        } 
                    },
                    {data: 'tahun.tahun', name: 'tahun'},                            
                ]
            });
            
            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        })
    </script>
    <!-- Footer -->
    @include('layouts.footer')
@endsection
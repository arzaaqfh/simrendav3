@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <input class="form-control" id="username" type="text" placeholder="username" required />
                    <input class="form-control" id="password" type="password" required />
                    <input class="btn btn-primary" id="btnlogin" type="button" value="LogIn">
                    <input class="btn btn-danger" id="btnlogout" type="button" value="LogOut">
                </div>
            </div>
        </div>
    </div>
    <div id="api_box" class="row justify-content-center">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <select class="form-control" id="method">
                            <option value="POST">POST</option>
                            <option value="GET">GET</option>
                        </select>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon3">api/</span>
                            </div>
                            <input type="text" class="form-control" id="url_api" aria-describedby="basic-addon3">
                        </div>
                        <input class="btn btn-danger" id="btn_execute" type="button" value="execute">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var data;

    $("#api_box").hide();

    $("#btnlogin").on('click', function () {
        var username = $("#username").val();
        var password = $("#password").val();
        $.ajax({
            url: 'http://localhost/simrendav3/public/api/auth/login',
            method: 'GET',
            data: {'username': username,'password': password},
            success: function(response) {
                // Handle the API response here
                data = response;
                $("#api_box").show();
            },
            error: function(xhr, status, error) {
                // Handle errors here
                console.error(status, error);
            }
        });
    });
    $("#btnlogout").on("click", function () {
        $.ajax({
            url: 'http://localhost/simrendav3/public/api/auth/logout',
            method: 'POST',
            data: {},
            // headers: {"Authorization": localStorage.getItem('token')},
            headers: {"Authorization": "Bearer "+data.access_token},
            success: function(response) {
                // Handle the API response here
                console.log(response);
                data = null;
                $("#api_box").hide();
            },
            error: function(xhr, status, error) {
                // Handle errors here
                console.error(status, error);
            }
        });        
    });
    $("#btn_execute").on("click", function () {
        $.ajax({
            url: 'http://localhost/simrendav3/public/api/'+$("#url_api").val(),
            method: $("#method").find(":selected").val(),
            // data: {},
            // headers: {"Authorization": localStorage.getItem('token')},
            headers: {"Authorization": "Bearer "+data.access_token},
            success: function(response) {
                // Handle the API response here
                console.log(response);
            },
            error: function(xhr, status, error) {
                // Handle errors here
                console.error(status, error);
            }
        });         
    });
</script>
@endsection

<section class="main_content dashboard_part large_header_bg">
    <!-- menu  -->
    <div class="container-fluid g-0">
        <div class="row">
            <div class="col-lg-12 p-0 ">
                <div class="header_iner d-flex justify-content-between align-items-center">
                    <div class="sidebar_icon d-lg-none">
                        <i class="ti-menu"></i>
                    </div>
                    <label class="form-label switch_toggle d-none d-lg-block" for="checkbox">
                        <input type="checkbox" id="checkbox">
                        <div class="slider round open_miniSide"></div>
                    </label>
                    <div class="header_right d-flex justify-content-between align-items-center">
                        @if (request()->segment(1) == 'perencanaan')
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                <a class="btn btn-primary rounded-pill bell_notification_clicker" href="#">
                                    Tahun Perencanaan {{ session()->get('periode_usulan') }}
                                </a>
                                <!-- Menu_NOtification_Wrap  -->
                                <div class="Menu_NOtification_Wrap">
                                    <div class="notification_Header">
                                        <h4>Pilih Tahun</h4>
                                    </div>
                                    <div class="Notification_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label" for="#">Tahun Perencanaan</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="tahun_perencanaan">
                                                        <option value="2021">2021</option>
                                                        <option value="2022">2022</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Menu_NOtification_Wrap  -->
                            </li>
                        </div>
                        @elseif (request()->segment(1) == 'monev')
                        <div class="header_notification_warp d-flex align-items-center">
                            @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                            <li style="margin-right: 10px"><!-- Menu SKPD -->
                                <select class="form-select" style="width: 400px;" id="skpd_monev">
                                    <option value="">Pilih Perangkat Daerah</option>
                                </select>
                            </li>
                            <script>
                                var urlSKPD = "{{url('/api/master/perangkatdaerah/get_data')}}";
                                var tokenUrl = "{{ Session::get('token') }}";
                                
                                $.ajax({
                                    type: 'GET',
                                    url: urlSKPD,
                                    beforeSend: function(xhr) {
                                        xhr.setRequestHeader('Authorization', 'Bearer ' + tokenUrl);
                                    },
                                    data: {
                                        'id_pd': ''
                                    },
                                    success: function(responses) {
                                        $skpd = responses.data.sort(function(a, b) {
                                            if (a.nama_skpd < b.nama_skpd) {
                                                return -1;
                                            }
                                            if (a.nama_skpd > b.nama_skpd) {
                                                return 1;
                                            }
                                            return 0;
                                        });
                                        $.each($skpd, function(index, item) {
                                            $('#skpd_monev').append($('<option>', {
                                                value: item.id_skpd,
                                                text: item.nama_skpd
                                            }));
                                        });
                                    }
                                });
                            </script>
                            @endif
                            <li style="margin-right: 10px"><!-- Menu Triwulan -->
                                <select class="form-select" id="twl_monev">
                                    <option value="1" @php if(Session::get('triwulan')==1){echo 'selected' ;} @endphp>TRIWULAN 1</option>
                                    <option value="2" @php if(Session::get('triwulan')==2){echo 'selected' ;} @endphp>TRIWULAN 2</option>
                                    <option value="3" @php if(Session::get('triwulan')==3){echo 'selected' ;} @endphp>TRIWULAN 3</option>
                                    <option value="4" @php if(Session::get('triwulan')==4){echo 'selected' ;} @endphp>TRIWULAN 4</option>
                                </select>
                            </li>
                            <li>
                                <a class="btn btn-primary rounded-pill bell_notification_clicker" href="#">
                                    Tahun Monev {{ session()->get('tahun_monev') }}
                                </a>
                                <!-- Menu_NOtification_Wrap  -->
                                <div class="Menu_NOtification_Wrap">
                                    <div class="notification_Header">
                                        <h4>Pilih Tahun</h4>
                                    </div>
                                    <div class="Notification_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label" for="#">Tahun Monev</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="tahun_monev">
                                                        <option value="2021">2021</option>
                                                        <option value="2022">2022</option>
                                                        <option value="2023">2023</option>
                                                        <option value="2024">2024</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Menu_NOtification_Wrap  -->
                            </li>
                        </div>
                        @elseif (request()->segment(1) == 'spm')
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                @php
                                if(is_null(session('tahun_spm')))
                                {
                                $tahun_spm = $tahun->first()->tahun;
                                }else{
                                $tahun_spm = session('tahun_spm');
                                }
                                @endphp
                                <a class="btn btn-primary rounded-pill bell_notification_clicker" href="#">
                                    Tahun SPM {{ $tahun_spm }}
                                </a>
                                <!-- Menu_NOtification_Wrap  -->
                                <div class="Menu_NOtification_Wrap">
                                    <div class="notification_Header">
                                        <h4>Pilih Tahun</h4>
                                    </div>
                                    <div class="Notification_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label" for="#">Tahun</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb30" id="tahun_spm">
                                                        @foreach ($tahun as $thn)
                                                        <option value="{{ $thn->id_tahun }}">{{ $thn->tahun }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Menu_NOtification_Wrap  -->
                            </li>
                        </div>
                        @elseif (request()->segment(1) == 'sdgs')
                        <div class="header_notification_warp align-items-center">
                            <li>
                                <div class="col">
                                    <div class="common_select">
                                        <select class="nice_Select" id="tahun_sdgs" name="tahun_sdgs">
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </div>
                        @endif

                        @guest
                        @if (Route::has('login'))
                        <li class="nav-item dropdown no-arrow">
                            <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @endif

                        @if (Route::has('register'))
                        <li class="nav-item dropdown no-arrow">
                            <a class="dropdown-item" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <div class="profile_info">
                            <img src="{{ asset('storage/img/profile_picture.png') }}" alt="#">
                            <div class="profile_info_iner">
                                <div class="profile_author_name">
                                    {{-- <p>{{ Auth::user()->role }} </p> --}}
                                    <h5>{{ Auth::user()->nama_pengguna }}</h5>
                                </div>
                                <div class="profile_info_details">
                                    <a href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a><!-- 
                                        <a href="#">
                                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Settings
                                        </a>
                                        <a href="#">
                                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Activity Log
                                        </a> -->
                                    <div class="dropdown-divider"></div>
                                    @if (Auth::user()->id_role == 1)
                                    <a href="{{ route('register') }}">
                                        <i class="fas fa-id-card fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Add Account
                                    </a>
                                    @endif
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endguest
                    </div>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
        <!--/ menu  -->

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="logoutModalTitle">Anda akan keluar?</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Klik tombol "Logout" di bawah ini untuk keluar.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- TIMER --}}
        <div id="batasWaktu" class="alert bg-dark text-light rounded-pill" style="position: fixed;
    top: -35px;
    left: 50%;
    z-index: 999;
    width: 200px;
    height: 75px;
    text-align: center">
        </div>
        <script type="text/javascript">
            var bwi_awal = new Date("{{ Session::get('bwi')['mulai'] }}").getTime();
            var bwi_akhir = new Date("{{ Session::get('bwi')['akhir'] }}").getTime();
            var now = new Date().getTime();

            // Set the date we're counting down to
            var countDownDate = bwi_akhir;

            // Update the count down every 1 second
            if (now >= bwi_awal) {
                var x = setInterval(function() {

                    // Get today's date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = countDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Display the result in the element with id="demo"
                    document.getElementById("batasWaktu").innerHTML = "<br/>" + days + "d " + hours + "h " +
                        minutes + "m " + seconds + "s ";

                    // If the count down is finished, write some text
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("batasWaktu").innerHTML = "<br/>BERAKHIR";
                    }
                }, 1000);
            } else {
                document.getElementById("batasWaktu").innerHTML = "<br/> -d -h -m -s";
            }


            $(document).ready(function() {

                $('#tahun_perencanaan').val(<?= session()->get('periode_usulan'); ?>);
                $('#tahun_perencanaan').niceSelect('update');

                $('#tahun_monev').val(<?= session()->get('tahun_monev'); ?>);
                $('#tahun_monev').niceSelect('update');

                $('#tahun_sdgs').val(<?= session()->get('tahun_sdgs'); ?>);
                $('#tahun_sdgs').niceSelect('update');

                $('#tahun_spm').val(<?= session()->get('tahun_spm'); ?>);
                $('#tahun_spm').niceSelect('update');

                // RENJA
                $('#skpd_monev').val(<?= session()->get('pilih_id_skpd'); ?>);
            });

            $("#tahun_perencanaan").change(function() {
                $.ajax({
                    url: "{{ url('/set/session') }}",
                    data: {
                        periode_usulan: this.value,
                        pilih_id_skpd: $('#skpd_monev').val(<?= session()->get('pilih_id_skpd'); ?>)
                    },
                    success: function(response) {
                        location.reload();
                    }
                });
            });

            $("#tahun_monev").change(function() {
                $.ajax({
                    url: "{{ url('/set/session') }}",
                    data: {
                        tahun_monev: this.value
                    },
                    success: function(response) {
                        location.reload();
                    }
                });
            });

            $("#tahun_sdgs").change(function() {
                $.ajax({
                    url: "{{ url('/set/session') }}",
                    data: {
                        tahun_sdgs: this.value
                    },
                    success: function(response) {
                        location.reload();
                    }
                });
            });

            $("#tahun_spm").change(function() {
                $.ajax({
                    url: "{{ url('/set/session') }}",
                    data: {
                        tahun_spm: this.value
                    },
                    success: function(response) {
                        location.reload();
                    }
                });
            });

            $("#skpd_monev").change(function() {
                $.ajax({
                    url: "{{ url('/set/session') }}",
                    data: {
                        pilih_id_skpd: this.value
                    },
                    success: function(response) {
                        location.reload();
                    }
                });
            });
        </script>
@extends('layouts.app') 

@section('content')
<!-- COVER -->
<header class="masthead" style="padding-left: 50px; padding-top: 50px; padding-right: 50px">
    <div class="row mb-5">
        <div class="col col-lg-6 mb-3">
            <img width="35%" class="float-left" src="{{ url('storage/img/simrenda-full.png') }}" alt="Logo Kota Cimahi">
        </div>
        <!-- <div class="col col-lg-6">
            <img class="float-right" src="{{ url('storage/img/stop-narkoba.png') }}" alt="Logo Stop Narkoba" width="150px" height="150px">
        </div> -->
    </div>
    <div style="color: #ffffff" class="row mt-5">
        <div class="col-lg-6">
            <h1 class="display-3 mb-4" style="color: #fff;">Selamat Datang</h1>
            <p class="mb-5" style="color: #fff;font-size: 12px;font-weight: 500;">
                SIMRENDA MERUPAKAN SISTEM E-PLANNING DAN E-MONEV YANG DIKEMBANGKAN UNTUK MENINGKATKAN KUALITAS PERENCANAAN<br>
                DENGAN TUJUAN MENGHASILKAN DOKUMEN PERENCANAAN YANG MENJADI ACUAN <br/>
                BAGI PEMANGKU KEPENTINGAN DALAM RANGKA MENINGKATKAN PARTISIPASI DAN TRANSPARANSI
            </p>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-4">
            <!-- sign_in  -->
            <div class="modal-content cs_modal shadow-lg">
                <div class="modal-header justify-content-right theme_bg_1">
                    <h5 class="modal-title text_white">{{ __('Login') }}</h5>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="">
                            <input id="username" type="text" class="form-control  @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="Username">

                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn_1 full_width text-center">Log in</button>
                    </form>
                </div>
            </div>
            <!-- <a href="{{url('simrendata')}}" target="_blank" class="btn btn-secondary">
                SIMRENDATA
            </a> -->
        </div>
    </div>
</header>


<!-- Footer -->
<footer class="footer large text-center text-white" style="box-shadow: 0 -7px 6px rgba(0, 0, 0, 0.12), 0 -1px 4px rgba(0, 0, 0, 0.24); background-color: #202020; padding-top: 15px; padding-bottom: 15px;">
    <div class="container">
        &copy; 2021 simrenda.cimahikota.go.id | Bappelitbangda Kota Cimahi
    </div>
</footer>
@endsection

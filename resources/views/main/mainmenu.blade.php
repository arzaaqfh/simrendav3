@extends('layouts.app')

@section('content')
<!-- DAFTAR MODUL -->
<section class="page-section" style="background-image: linear-gradient(#53bce9ee,#fbf6f0); padding-top: 15px; padding-bottom: 15px; color: #0d6287"><br/>
    <center>
        <h1 class="display-4"><b>DAFTAR MODUL</b></h1>
    </center><br/>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="header_iner d-flex justify-content-end align-items-center" style="background: none;">
                    <div class="header_right d-flex justify-content-end align-items-center" style="padding: 10px 10px 10px 10px; background-color: #0d6287; border-radius: 100%;">
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item dropdown no-arrow">
                                <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item dropdown no-arrow">
                                <a class="dropdown-item" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else
                            <div class="profile_info">
                                <img src="{{ asset('storage/img/profile_picture.png') }}" alt="#">
                                <div class="profile_info_iner">
                                    <div class="profile_author_name">
                                        {{-- <p>{{ Auth::user()->role }} </p> --}}
                                        <h5>{{ Auth::user()->nama_pengguna }}</h5>
                                    </div>
                                    <div class="profile_info_details">
                                        <a href="#">
                                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Profile
                                        </a><!--
                                        <a href="#">
                                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Settings
                                        </a>
                                        <a href="#">
                                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Activity Log
                                        </a> -->
                                        <div class="dropdown-divider"></div>
                                        @if (Auth::user()->id_role == 1)
                                        <a href="{{ route('register') }}">
                                            <i class="fas fa-id-card fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Add Account
                                        </a>
                                        @endif
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#logoutModal">
                                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Logout
                                        </a>
                                    </div>
                                </div>
                            </div>
                    @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <a href="{{ url('/perencanaan') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/perencanaan-icon.png') }}" style="padding: 0px;" height="150px">
                    </div>
                    <h4 class="text-white">Perencanaan</h4>
                    <p class="text-white">Database perencaan dan sinkronisasi data dengan SIPD</p>
                </center>
            </a>
            <a href="{{ url('/monev/renja') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/icon_monev_mini.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">RENJA</h4>
                    <p class="text-white">Rencana Kerja</p>
                </center>
            </a>
        </div>
        <div class="row">
            <a href="{{ url('/spm') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/icon_tahunan_mini.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">SPM</h4>
                    <p class="text-white">Standar Pelayanan Minimal</p>
                </center>
            </a>
            <a href="{{ url('/sdgs/codingsdgs') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/SDGS_logo.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">SDGs</h4>
                    <p class="text-white">Sustainable Development Goals / Tujuan Pembangunan Berkelanjutan</p>
                </center>
            </a>
            <a href="#" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/logo_simpedil.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">SI-Pendil</h4>
                    <p class="text-white">Sistem Pengelolaan Data Infrastruktur Lingkungan</p>
                </center>
            </a>
        </div>
        <div class="row">
            <a href="{{ url('musrenbang') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/discussion.png') }}" style="padding: 25px;" height="150px">
                    </div>
                    <h4 class="text-white">Musrenbang</h4>
                    <p class="text-white">Musrenbang atau musyawarah perencanaan dan pembangunan merupakan aplikasi yang menampung aspirasi warga dari tingkat RW sampai Kota.</p>
                </center>
            </a>
            <a href="#" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="" height="150px">
                    </div>
                    <h4>-</h4>
                    <p>-</p>
                </center>
            </a>
            <a href="#" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="" height="150px">
                    </div>
                    <h4>-</h4>
                    <p>-</p>
                </center>
            </a>
        </div>
        <div class="row">
            @if( session()->get('id_role') == 1 )
            <a href="{{ url('/administrator') }}" class="col m12 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0;">
                        <img src="{{ url('storage/img/administrator_logo.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">Administrator</h4>
                    <p class="text-white">Untuk mengelola semua data</p>
                </center>
            </a>
            @endif
        </div>
    </div>
</section>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logoutModalTitle">Anda akan keluar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Klik tombol "Logout" di bawah ini untuk keluar.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.grafik.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item active"><a href="{{ url('monev/grafik') }}">Dashboard</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="white_card mb_30">
                        <div class="white_card_header">
                            
                        </div>                    
                        <div class="white_card_body">        
                            
                            <div class="col-lg-12 col-xl-12">
                                <div class="white_box mb_30">
                                    <div class="box_header ">
                                        <div class="main-title">
                                            <h3 class="mb-0">Capaian Kinerja</h3>
                                        </div>
                                    </div>
                                    <div id="grafikCapaian"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-12">
                                <div class="white_box mb_30">
                                    <div class="box_header ">
                                        <div class="main-title">
                                            <h3 class="mb-0">Realisasi Anggaran</h3>
                                        </div>
                                    </div>
                                    <div id="grafikAnggaran">
                                        <div id="grafikAnggaranRow" class="row"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <script>
    $(document).ready(function(){
        const detilPD = [];
        var item = {}; 
        $.get( "{{ url('api/grafik/capaianPD') }}", function( data ) {
            $.each( data, function( key, value ) {
                var i = 0;
                $.each(value,function(i,item){
                    // console.log(item.nama_skpd);
                    // if(i<=5){
                        detilPD.push({
                            name: item.nama_skpd,
                            data: [50, 60, 70, 80, 90]
                        });
                    // }
                    i++;
                });
            });
        });
           var seriesG = [
                {
                    name: 'DINSOS',
                    data: [44, 55, 57, 56, 61]
                }, 
                {
                    name: 'DINKES',
                    data: [76, 85, 100, 98, 87]
                },
                {
                    name: 'DISKOMINFO',
                    data: [35, 41, 36, 26, 45]
                },
                {
                    name: 'DISPANGTAN',
                    data: [60, 75, 80, 99, 100]
                },
                {
                    name: 'DISBUDPARPORA',
                    data: [87, 22, 50, 89, 98]
                }
            ]
        
        var options = {
            series: detilPD,
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['2018', '2019', '2020', '2021', '2022'],
            },
            yaxis: {
                title: {
                    text: '% (persen)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val + " %"
                    }
                }
            }
        };

        var chart = new ApexCharts(document.querySelector("#grafikCapaian"), options);
        chart.render();
        
        // PIE CHART 1
        var options2 = [];
        var chart2 = [];
        
        $.get( "{{ url('api/grafik/anggaranPD') }}", function( data ) {
            var labels = data.dataTahun
            // console.log(value.);
            var j = 0;
            $.each( data.dataAng, function( key, value ) {
                console.log(value);
                // $.each(value,function(i,item){
                    options2[j] = {
                        series: value,
                        chart: {
                            width: 380,
                            type: 'pie',
                        },
                        labels: labels,
                        responsive: [{
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    position: 'bottom'
                                }
                            }
                        }],
                        tooltip: {
                            y: {
                                formatter: function(value) {
                                    value += '';
                                    x = value.split('.');
                                    x1 = x[0];
                                    x2 = x.length > 1 ? '.' + x[1] : '';
                                    var rgx = /(\d+)(\d{3})/;
                                    while (rgx.test(x1)) {
                                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                                    }
                                    return 'Rp ' + x1 + x2;
                                    // return (value/1000).toFixed(3);
                                }
                            },                            
                            x: {
                                formatter: function(value) {
                                    value += '';
                                    x = value.split('.');
                                    x1 = x[0];
                                    x2 = x.length > 1 ? '.' + x[1] : '';
                                    var rgx = /(\d+)(\d{3})/;
                                    while (rgx.test(x1)) {
                                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                                    }
                                    return 'Rp ' + x1 + x2;
                                    // return (value/1000).toFixed(3);
                                }
                            },                            
                            z: {
                                formatter: function(value) {
                                    value += '';
                                    x = value.split('.');
                                    x1 = x[0];
                                    x2 = x.length > 1 ? '.' + x[1] : '';
                                    var rgx = /(\d+)(\d{3})/;
                                    while (rgx.test(x1)) {
                                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                                    }
                                    return 'Rp ' + x1 + x2;
                                    // return (value/1000).toFixed(3);
                                }
                            }
                        },
                    } 
                    $('#grafikAnggaranRow').append('<div class="col-md-1"><b>'+key+'</b></div><div class="col-md-5" id="grafikAnggaran_'+j+'"></div>');
                    chart2[j] = new ApexCharts(document.querySelector("#grafikAnggaran_"+j), options2[j]);
                    chart2[j].render();
                    j++;
                // });
            });
        });
    });
    </script>
    <!-- Footer -->
    @include('layouts.footer')
@endsection
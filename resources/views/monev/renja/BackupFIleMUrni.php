@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>
        <div class="main_content_iner ">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-12">
                        <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                            <div class="page_title_left">
                                <h3 class="f_s_25 f_w_700 dark_text" >Monev Hasil Renja</h3>
                                <ol class="breadcrumb page_bradcam mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/monev') }}">Monev</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/monev/renja') }}">Renja</a></li>
                                    <li class="breadcrumb-item active">Murni</li>
                                </ol>
                            </div>
                            <div class="page_title_right">
                                <div class="dropdown common_bootstrap_button ">
                                    <span class="dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">Cetak</span>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(1)"> Triwulan 1</a>
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(2)"> Triwulan 2</a>
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(3)"> Triwulan 3</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12">
                        <div class="white_card mb_30 ">
                            <div class="white_card_header">
                                <div class="bulder_tab_wrapper">
                                    <ul class="nav" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="kegiatan-tab" data-bs-toggle="tab" href="#Kegiatan" role="tab" aria-controls="Kegiatan" aria-selected="true">Kegiatan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="subkegiatan-tab" data-bs-toggle="tab" href="#Subkegiatan" role="tab" aria-controls="Subkegiatan" aria-selected="false">Sub Kegiatan</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="white_card_body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="Kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="QA_section">
                                                        <div class="QA_table mb_30">
                                                            <table class="table multiplegroup" id="table_kegiatan">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Subkegiatan" role="tabpanel" aria-labelledby="subkegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row">
                                                    <div class="col-lg-12 ">
                                                        <div class="QA_section">
                                                            <div class="QA_table mb_30">
                                                                <table class="table multiplegroup" id="table_subkegiatan">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col"></th>
                                                                            <th scope="col"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" id="forminputrealisasikegModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Realisasi Indikator Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h5>Kegiatan</h5>
                                                <div class="white_card badge_active2 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_kegiatan">Nama Kegiatan</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <h5>Perangkat Daerah</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_pd">Nama Perangkat Daerah</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <h5>Triwulan</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="triwulan_keg">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="anggaran">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Realisasi Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="r_anggaran">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card_body" style="padding: 10px 30px;">
                                    <div class="white_card_body">
                                        <form id="formrealisasikegiatan">
                                            <input type="hidden" name="id_pd" id="id_pd">
                                            <input type="hidden" name="id_kegiatan" id="id_kegiatan">
                                            <div id="wrap-ind-keg"></div>
                                            <div id="wrap-desk-keg"></div>
                                            <div class="white_card mb_30">
                                                <div class="white_card_body">
                                                    <div class="row mb-3">
                                                        <div class="col-md-12">
                                                            <button type="button" class="btn btn-primary"
                                                                onclick="saverealisasikegiatan(1)">Simpan</button>
                                                            <button class="btn btn-secondary">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" id="forminputrealisasisubkegModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Realisasi Indikator Sub Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h5>Sub Kegiatan</h5>
                                                <div class="white_card badge_active2 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_subkegiatan">Nama Sub Kegiatan</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <h5>Perangkat Daerah</h5>
                                                <div class="white_card badge_active4" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_pd_sub">Nama Perangkat Daerah</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card mb_30">
                                    <div class="white_card_header">
                                        <div class="bulder_tab_wrapper">
                                            <ul class="nav" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="triwulan1-subkeg-tab" data-bs-toggle="tab"
                                                        href="#SubKegTriwulan1" role="tab" aria-controls="Triwulan1"
                                                        aria-selected="true">Triwulan
                                                        1</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="triwulan2-subkeg-tab" data-bs-toggle="tab"
                                                        href="#Triwulan2" role="tab" aria-controls="Triwulan2"
                                                        aria-selected="false">Triwulan 2</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="triwulan3-subkeg-tab" data-bs-toggle="tab"
                                                        href="#Triwulan3" role="tab" aria-controls="Triwulan3"
                                                        aria-selected="false">Triwulan 3</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="white_card_body">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="SubKegTriwulan1" role="tabpanel"
                                                aria-labelledby="triwulan1-subkeg-tab">
                                                <div class="builder_select">
                                                    <form id="formrealisasisubkegiatan">
                                                        <input type="hidden" name="id_pd_subkegiatan" id="id_pd_subkegiatan">
                                                        <input type="hidden" name="id_subkegiatan" id="id_subkegiatan">
                                                        <div class="white_card mb_30">
                                                            <div class="white_card_body">
                                                                <div id="wrap-ind-subkeg-tw1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="wrap-desk-subkeg-tw1"></div>
                                                        <div class="white_card mb_30">
                                                            <div class="white_card_body">
                                                                <div class="row mb-3">
                                                                    <div class="col-md-12">
                                                                        <button type="button" class="btn btn-primary"
                                                                            onclick="saverealisasisubkegiatan(1)">Simpan</button>
                                                                        <button class="btn btn-secondary">Batal</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="SubKegTriwulan2" role="tabpanel"
                                                aria-labelledby="triwulan2-subkeg-tab">
                                                <div class="builder_select">
                                                    <div class="white_card mb_30">
                                                        <div class="white_card_body">
                                                            <div id="wrap-ind-subkeg-tw2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="wrap-desk-subkeg-tw2"></div>
                                                    <div class="white_card mb_30">
                                                        <div class="white_card_body">
                                                            <div class="row mb-3">
                                                                <div class="col-md-12">
                                                                    <button type="button" class="btn btn-primary"
                                                                        onclick="saverealisasisubkegiatan(2)">Simpan</button>
                                                                    <button class="btn btn-secondary">Batal</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="SubKegTriwulan3" role="tabpanel"
                                                aria-labelledby="triwulan3-subkeg-tab">
                                                <div class="builder_select">
                                                    <div class="white_card mb_30">
                                                        <div class="white_card_body">
                                                            <div id="wrap-ind-subkeg-tw3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="wrap-desk-subkeg-tw3"></div>
                                                    <div class="white_card mb_30">
                                                        <div class="white_card_body">
                                                            <div class="row mb-3">
                                                                <div class="col-md-12">
                                                                    <button type="button" class="btn btn-primary"
                                                                        onclick="saverealisasisubkegiatan(3)">Simpan</button>
                                                                    <button class="btn btn-secondary">Batal</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
            aria-hidden="true" id="formcetak">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalTitle">Cetak Monev Hasil Renja</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <input type="hidden" id="tw-cetak">
                                                <label class="form-label" for="#">Perangkat Daerah</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_pd_cetak">
                                                        <option value="">Pilih Perangkat Daerah...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-danger" onclick="execcetak()">Cetak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script>
            var table,table_sub;
            $( document ).ready(function() {
                var collapsedGroups = {},collapsedGroupsSub = {},top,middle,parent;
                table = $('#table_kegiatan').DataTable( {
                    ajax: {
                        url:"{{ url('/api/monev/renja/realisasiindikatorkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('tahun_monev');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}}
                        },
                    },
                    columns: [ 
                        { "data": "kegiatan.nama_kegiatan", 
                            "render": function ( data, type, row, meta ) {
                                return data+row.renja.skpd.nama_skpd;
                            }
                        },
                        { "data": "indikator_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                var t1='',t2='',t3=''
                                if(row.realisasiindikatorkeg){
                                    t1 = row.realisasiindikatorkeg.t_1
                                    t2 = row.realisasiindikatorkeg.t_2
                                    t3 = row.realisasiindikatorkeg.t_3
                                }
                                if(data == null){
                                    return '<div class="card_box box_shadow position-relative mb_10 ml-10">'+
                                        '<div class="white_box_tittle" style="padding: 15px 25px;border-bottom: none;">'+
                                        '<div class="main-title2 ">'+
                                        '<div class="lodo_left d-flex align-items-center">'+
                                        '<div class="bar_line mr_10"></div>'+
                                        '<div class="todo_head">'+
                                        '<h4 class="mb-2 ">Belum ada indikator kegiatan</h4>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>';
                                }else{
                                    var csctw_1 = 'bg-light',csctw_2 = 'bg-light',csctw_3 = 'bg-light'
                                    if(t1 != null && t1 != '' && t1 != 'null'){
                                        csctw_1 = 'bg-primary'
                                    }if(t2 != null && t2 != '' && t2 != 'null'){
                                        csctw_2 = 'bg-primary'
                                    }if(t3 != null && t3 != '' && t3 != 'null'){
                                        csctw_3 = 'bg-primary'
                                    }
                                    return '<div class="card_box box_shadow position-relative mb_10 ml-10">'+
                                        '<div class="white_box_tittle" style="padding: 15px 25px;border-bottom: none;">'+
                                        '<div class="main-title2 ">'+
                                        '<div class="lodo_left d-flex align-items-center">'+
                                        '<div class="bar_line mr_10"></div>'+
                                        '<div class="todo_head">'+
                                        '<h4 class="mb-2">'+data+'</h4>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="box_body" style="padding: 5px 25px">'+
                                        '<div class="row">'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 1</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_1+' mb-2">Realisasi: '+t1+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 2</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_2+' mb-2">Realisasi: '+t2+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 3</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_3+' mb-2">Realisasi: '+t3+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>';
                                }
                            }
                        }
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    rowGroup: {
                        dataSrc: [ 'kegiatan.program.nama_program','kegiatan.nama_kegiatan','renja.skpd.nama_skpd' ],
                        startRender: function (rows, group, level) {
                            
                            var all;

                            if (level === 0) {
                                top = group;
                                all = group;
                                middle = '';
                            }else {
                                // if parent collapsed, nothing to do
                                if (!!collapsedGroups[top]) {
                                    return;
                                }
                                if (level ===1) {
                                    middle = group
                                }
                                all = top + middle + group;
                            }

                            var collapsed = !!collapsedGroups[all];

                            rows.nodes().each(function(r) {
                                r.style.display = collapsed ? 'none' : '';
                            });

                            if(level === 2){
                                var indexOfarray = rows.data()[0].kegiatan.apbd_kota.findIndex((item) => item.id_renja === rows.data()[0].renja.id_renja.toString())
                                var indexOfarrayreal = rows.data()[0].kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === rows.data()[0].renja.id_renja.toString())
                                var real_rp1 = (rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '-'
                                var real_rp2 = (rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '-'
                                var real_rp3 = (rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? rows.data()[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '-'
                                
                                return $('<tr/>')
                                .append('<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<p>'+rows.data()[0].kegiatan.program.nama_program+'</p>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+rows.data()[0].kegiatan.nama_kegiatan+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+rows.data()[0].renja.skpd.nama_skpd+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap" style="display:contents;">'+
                                '<div class="row" style="width: -webkit-fill-available;">'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Anggaran</h6>'+
                                '<p class="text-primary">'+rows.data()[0].kegiatan.apbd_kota[indexOfarray].apbd_kota+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 1</h6>'+
                                '<p class="text-primary">'+real_rp1+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 2</h6>'+
                                '<p class="text-primary">'+real_rp2+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 3</h6>'+
                                '<p class="text-primary">'+real_rp3+'</p>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<button class="btn btn-sm btn-primary" onclick="openforminputrealisasikeg(1,'+rows.data()[0].kegiatan.id_kegiatan+','+rows.data()[0].renja.skpd.id_skpd+')">Input Realisasi</button>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                    '</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }  
                        },
                    },
                    columnDefs: [ 
                        {
                            targets: [ 0 ],
                            visible: false
                        } 
                    ],
                } );

                table_sub = $('#table_subkegiatan').DataTable( {
                    ajax: {
                        url:"{{ url('/api/monev/renja/realisasiindikatorsubkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('tahun_monev');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}}
                        },
                    },
                    columns: [ 
                        { "data": "subkegiatan.nama_sub_kegiatan", 
                            "render": function ( data, type, row, meta ) {
                                return data+row.renja.skpd.nama_skpd;
                            }
                        },
                        { "data": "indikator_subkegiatan",
                            "render": function ( data, type, row, meta ) {
                                var t1='',t2='',t3=''
                                if(row.realisasiindikatorsubkeg){
                                    t1 = row.realisasiindikatorsubkeg.t_1
                                    t2 = row.realisasiindikatorsubkeg.t_2
                                    t3 = row.realisasiindikatorsubkeg.t_3
                                }
                                if(data == null){
                                    return '<div class="card_box box_shadow position-relative mb_10 ml-10">'+
                                        '<div class="white_box_tittle" style="padding: 15px 25px;border-bottom: none;">'+
                                        '<div class="main-title2 ">'+
                                        '<div class="lodo_left d-flex align-items-center">'+
                                        '<div class="bar_line mr_10"></div>'+
                                        '<div class="todo_head">'+
                                        '<h4 class="mb-2 ">Belum ada indikator sub kegiatan</h4>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>';
                                }else{
                                    var csctw_1 = 'bg-light',csctw_2 = 'bg-light',csctw_3 = 'bg-light'
                                    if(t1 != null && t1 != '' && t1 != 'null'){
                                        csctw_1 = 'bg-primary'
                                    }if(t2 != null && t2 != '' && t2 != 'null'){
                                        csctw_2 = 'bg-primary'
                                    }if(t3 != null && t3 != '' && t3 != 'null'){
                                        console.log('masuk')
                                        csctw_3 = 'bg-primary'
                                    }
                                    
                                    return '<div class="card_box box_shadow position-relative mb_10 ml-10">'+
                                        '<div class="white_box_tittle" style="padding: 15px 25px;border-bottom: none;">'+
                                        '<div class="main-title2 ">'+
                                        '<div class="lodo_left d-flex align-items-center">'+
                                        '<div class="bar_line mr_10"></div>'+
                                        '<div class="todo_head">'+
                                        '<h4 class="mb-2 ">'+data+'</h4>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="box_body" style="padding: 5px 25px">'+
                                        '<div class="row">'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 1</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_1+' mb-2">Realisasi: '+t1+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 2</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_2+' mb-2">Realisasi: '+t2+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '<div class="col-s-12 col-lg-4">'+
                                        '<div class="card mb-3" style="border: #a9ff1d solid 1px;border-radius: 18px;">'+
                                        '<div class="card-header" style="border-bottom: none;"><b>Triwulan 3</b></div>'+
                                        '<div class="card-body" style="padding: 5px 20px;">'+
                                        '<div class="row">'+
                                        '<div class="col-lg-12">'+
                                        '<span class="badge '+csctw_3+' mb-2">Realisasi: '+t3+'</span>'+
                                        '</div>'+
                                        '<div class="col-lg-12">'+
                                        '<div class="single_progressbar">'+
                                        '<h6 class="f_s_14 f_w_400" >Tinggi</h6>'+
                                        '<div id="bar4" class="barfiller">'+
                                        '<div class="tipWrap">'+
                                        '<span class="tip"></span>'+
                                        '</div>'+
                                        '<span class="fill" data-percentage="81"></span>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div>';
                                }
                               return data
                            }
                        }
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    rowGroup: {
                        dataSrc: [ 'subkegiatan.kegiatan.program.nama_program','subkegiatan.kegiatan.nama_kegiatan','subkegiatan.nama_sub_kegiatan','renja.skpd.nama_skpd' ],
                        startRender: function (rows, group, level) {
                            
                            var all;

                            if (level === 0) {
                                top = group;
                                all = group;
                                middle = '';
                            }else {
                                // if parent collapsed, nothing to do
                                if (!!collapsedGroupsSub[top]) {
                                    return;
                                }
                                if (level ===1) {
                                    middle = group
                                }
                                all = top + middle + group;
                            }
                            var collapsed = !!collapsedGroupsSub[all];

                            rows.nodes().each(function(r) {
                                r.style.display = collapsed ? 'none' : '';
                            });

                            if(level === 3){
                                
                                var indexOfarray = rows.data()[0].subkegiatan.apbd_kota.findIndex((item) => item.id_renja === rows.data()[0].renja.id_renja.toString())
                                var indexOfarrayreal = rows.data()[0].subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === rows.data()[0].renja.id_renja.toString())
                                var real_rp1 = (rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '-'
                                var real_rp2 = (rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '-'
                                var real_rp3 = (rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? rows.data()[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '-'
                                
                                return $('<tr/>')
                                .append('<td><div class="white_card badge_complete mt-30">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<p>'+rows.data()[0].subkegiatan.kegiatan.program.nama_program+'</p>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<p>'+rows.data()[0].subkegiatan.kegiatan.nama_kegiatan+'</p>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+rows.data()[0].subkegiatan.nama_sub_kegiatan+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+rows.data()[0].renja.skpd.nama_skpd+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap" style="display:contents;">'+
                                '<div class="row" style="width: -webkit-fill-available;">'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Anggaran</h6>'+
                                '<p class="text-primary">'+rows.data()[0].subkegiatan.apbd_kota[indexOfarray].apbd_kota+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 1</h6>'+
                                '<p class="text-primary">'+real_rp1+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 2</h6>'+
                                '<p class="text-primary">'+real_rp2+'</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-lg-3 col-s-12">'+
                                '<div class="white_box p-3">'+
                                '<h6 style="font-size:12px;line-height:16px;">Realisasi Tw. 3</h6>'+
                                '<p class="text-primary">'+real_rp3+'</p>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<button class="btn btn-sm btn-primary" onclick="openforminputrealisasisubkeg('+rows.data()[0].subkegiatan.id_sub_kegiatan+','+rows.data()[0].renja.skpd.id_skpd+')">Input Realisasi</button>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                    '</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }  
                        },
                    },
                    columnDefs: [ 
                        {
                            targets: [ 0 ],
                            visible: false
                        } 
                    ],
                } );
                
                /*$('#table_kegiatan').on('click', 'tr.dtrg-start', function () {
                    var name = $(this).data('name');
                    collapsedGroups[name] = !collapsedGroups[name];
                    table.draw(false);
                });
                */
            
            });
            
            function openforminputrealisasikeg(triwulan,id_kegiatan,id_pd){
                $('#wrap-ind-keg').empty();
                $('#nama_kegiatan').html('');
                $('#nama_pd').html('');
                $('#anggaran').html('');
                $('#r_anggaran').html('');
                $('#triwulan_keg').html(triwulan);

                $('#forminputrealisasikegModal').modal('show')
                loadrealisasiindkeg(triwulan,id_kegiatan,id_pd)
                return false;
            }

            function loadrealisasiindkeg(triwulan,id_kegiatan,id_pd){                
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/monev/renja/realisasiindikatorkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        step : 'murni',
                        id_kegiatan: id_kegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $('#id_pd').val(id_pd);
                        $('#id_kegiatan').val(id_kegiatan);
                        $('#nama_kegiatan').html(response.data[0].kegiatan.nama_kegiatan);
                        $('#nama_pd').html(response.data[0].renja.skpd.nama_skpd);

                        var indexOfarray = response.data[0].kegiatan.apbd_kota.findIndex((item) => item.id_renja === response.data[0].renja.id_renja.toString())
                        var anggaran = (response.data[0].kegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? response.data[0].kegiatan.apbd_kota[indexOfarray].apbd_kota : '0'
                        var indexOfarrayreal = response.data[0].kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === response.data[0].renja.id_renja.toString())
                        var real_rp1 = (response.data[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? response.data[0].kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'
                        $('#anggaran').html(anggaran);
                        $('#r_anggaran').html(real_rp1);

                        renderdataindbytriwulan(triwulan, response, id_kegiatan, id_pd)
                    }
                });
                
            }

            function renderdataindbytriwulan(tw,response,id_kegiatan,id_pd){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/monev/renja/murni/inputrealisasiindkegiatan') }}",
                    data: {
                        tw: tw,
                        data_realisasi: response.data
                    },
                    success: function (response) {
                        $("#wrap-ind-keg").html(response);
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: "{{ url('/monev/renja/murni/inputanalisiskegiatan') }}",
                    data: {
                        tw: tw,
                        id_kegiatan: id_kegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $("#wrap-desk-keg").html(response);
                    }
                });
            }

            function openforminputrealisasisubkeg(id_subkegiatan,id_pd){
                $('#wrap-ind-subkeg-tw1').empty();
                $('#wrap-ind-subkeg-tw2').empty();
                $('#wrap-ind-subkeg-tw3').empty();
                $('#nama_subkegiatan').empty();
                $('#nama_pd_sub').empty();

                $('#forminputrealisasisubkegModal').modal('show')
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/monev/renja/realisasiindikatorsubkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        step: 'murni',
                        id_subkegiatan : id_subkegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        var num = 1,html_tw1 = '',html_tw2 = '',html_tw3 = ''
                        $('#id_pd_subkegiatan').val(id_pd);
                        $('#id_subkegiatan').val(id_subkegiatan);
                        $('#nama_subkegiatan').html(response.data[0].subkegiatan.nama_sub_kegiatan);
                        $('#nama_pd_sub').html(response.data[0].renja.skpd.nama_skpd);
                        $.each(response.data, function (key, value) {
                            var t1= '',t2= '',t3= ''
                            if(value.realisasiindikatorsubkeg){
                            
                                if(value.realisasiindikatorsubkeg.t_1){
                                    t1 = value.realisasiindikatorsubkeg.t_1
                                }
                                if(value.realisasiindikatorsubkeg.t_2){
                                    t2 = value.realisasiindikatorsubkeg.t_2
                                }
                                if(value.realisasiindikatorsubkeg.t_3){
                                    t3 = value.realisasiindikatorsubkeg.t_3
                                }
                            }

                            if(value.indikator_subkegiatan != null){
                            $('#wrap-ind-subkeg-tw1').append('<div class="row mb-3" id="indkeg_'+ num + '">'+
                                    '<input type="hidden" name="id_indikator_subkegiatan[]" id="id_indikator_subkegiatan_tw1'+ num + '" value="'+ value.id_indikator_subkegiatan + '">'+
                                    '<div class="col-md-6">'+
                                    '<label class="form-label" id="label_indsubkeg_'+ num + '">Indikator #'+ num + '</label>'+
                                    '<input type="text" class="form-control" placeholder="" value="'+ value.indikator_subkegiatan + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Target</label>'+
                                    '<input type="text" class="form-control" placeholder="" value="'+ value.target + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Realiasi</label>'+
                                    '<input type="text" class="form-control" name="realisasi[]" onkeypress="return restrictcomma(event);" placeholder="" value="'+ t1 + '">'+
                                    '</div>'+
                                    '<div class="col-md-2 ">'+
                                    '<label class="form-label" for="#">Satuan</label>'+
                                    '<div class="common_select">'+
                                    '<select class="nice_Select wide mb_30" id="satuanindsubkeg_tw1_'+ num + '">'+
                                    '<option value="'+ value.satuan + '">'+ value.satuan + '</option>'+
                                    '</select>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>')

                            $('#wrap-ind-subkeg-tw2').append('<div class="row mb-3" id="indkeg_'+ num + '">'+
                                    '<div class="col-md-6">'+
                                    '<label class="form-label" id="label_indkeg_'+ num + '">Indikator #'+ num + '</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ value.indikator_kegiatan + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Target</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ value.target + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Realiasi</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ t2 + '">'+
                                    '</div>'+
                                    '<div class="col-md-2 ">'+
                                    '<label class="form-label" for="#">Satuan</label>'+
                                    '<div class="common_select">'+
                                    '<select class="nice_Select wide mb_30" id="satuanindsubkeg_tw2_'+ num + '">'+
                                    '<option value="'+ value.satuan + '">'+ value.satuan + '</option>'+
                                    '</select>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>')
                                    
                            $('#wrap-ind-subkeg-tw3').append('<div class="row mb-3" id="indkeg_'+ num + '">'+
                                    '<div class="col-md-6">'+
                                    '<label class="form-label" id="label_indkeg_'+ num + '">Indikator #'+ num + '</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ value.indikator_kegiatan + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Target</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ value.target + '">'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                    '<label class="form-label" for="inputRealisasi">Realiasi</label>'+
                                    '<input type="text" class="form-control" id="" placeholder="" value="'+ t3 + '">'+
                                    '</div>'+
                                    '<div class="col-md-2 ">'+
                                    '<label class="form-label" for="#">Satuan</label>'+
                                    '<div class="common_select">'+
                                    '<select class="nice_Select wide mb_30" id="satuanindsubkeg_tw3_'+ num + '">'+
                                    '<option value="'+ value.satuan + '">'+ value.satuan + '</option>'+
                                    '</select>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>')

                            $('#satuanindsubkeg_tw1_'+ num + '').niceSelect();
                            num++
                            }
                        });
                        
                        $.ajax({
                            type: 'GET',
                            url: "{{ url('/monev/renja/murni/inputanalisissubkegiatan') }}",
                            data: {
                                tw: 1 ,
                                id_subkegiatan: id_subkegiatan,
                                id_pd: id_pd
                            },
                            success: function (response) {
                                $("#wrap-desk-subkeg-tw1").html(response);
                            }
                        });

                        $.ajax({
                            type: 'GET',
                            url: "{{ url('/monev/renja/murni/inputanalisissubkegiatan') }}",
                            data: {
                                tw: 2 ,
                                id_subkegiatan: id_subkegiatan,
                                id_pd: id_pd
                            },
                            success: function (response) {
                                $("#wrap-desk-subkeg-tw2").html(response);
                            }
                        });

                        $.ajax({
                            type: 'GET',
                            url: "{{ url('/monev/renja/murni/inputanalisissubkegiatan') }}",
                            data: {
                                tw: 3 ,
                                id_subkegiatan: id_subkegiatan,
                                id_pd: id_pd
                            },
                            success: function (response) {
                                $("#wrap-desk-subkeg-tw3").html(response);
                            }
                        });
                    }
                });
                return false;
            }

            function saverealisasikegiatan(tw){
                let myForm = document.getElementById('formrealisasikegiatan');
                let formData = new FormData(myForm);
                formData.append('triwulan',tw)
                console.log(formData)
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/monev/renja/murni/saverealisasikeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            alert(response.message)
                            table.ajax.reload( null, false );
                        }
                    }
                });
            }

            function saverealisasisubkegiatan(tw){
                let myForm = document.getElementById('formrealisasisubkegiatan');
                let formData = new FormData(myForm);
                formData.append('triwulan',tw)
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/monev/renja/murni/saverealisasisubkeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            alert(response.message)
                            table_sub.ajax.reload( null, false );
                        }
                    }
                });
            }

            function restrictcomma(e){
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[^,;]+$/;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) {
                        theEvent.preventDefault();
                        alert('Untuk angka desimal silahkan gunakan tanda titik (.)')
                    }
                }
            }

            function renderBarPersentase(target,value,numel){
                var persen = (value/target)*100
                $('#fillbar_ind'+numel).attr('data-percentage',persen);
                $('#bar_ind'+numel).barfiller();

                $('#persenbar_ind'+numel).val(persen);
            }

            function loadskpd(id_pd=0,elm){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:{{$id_pd}}
                    },
                    success: function(response) {
                        $(elm).empty();
                        $(elm).append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $(elm).append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id_pd != 0){
                            $(elm).val(id);
                        }
                        $(elm).niceSelect('update');
                    }
                });
            }

            function openformcetak(tw){
                if({{Session::get('id_role')}} != 4){
                    $('#formcetak').modal('show')
                    $('#tw-cetak').val(tw)
                    loadskpd(0,'#id_pd_cetak')
                }else{
                    $('#tw-cetak').val(tw)
                    execcetak(true)   
                }
                return false;
            }

            function execcetak(bypd=false){
                var twcetak = $('#tw-cetak').val(),periode = {{Session::get('tahun_monev')}}
                if(bypd){
                    var pd = <?php echo json_encode(Session::get('id_skpd'));?>;
                    window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=monev2021/monevhasilrenjamurni-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&periode='+periode+'&skpd='+pd.toString()+'&triwulan='+twcetak);
                }else{
                    var pd = $('#id_pd_cetak').val()
                    window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=monev2021/monevhasilrenjamurni-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&periode='+periode+'&skpd='+pd+'&triwulan='+twcetak);
                }
            }
        </script>

        <!-- Footer -->
        @include('layouts.footer')
@endsection
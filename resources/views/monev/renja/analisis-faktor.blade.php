<div class="col-lg-12">
    <div class="d-flex align-items-start">
        {{-- LIST ANALISIS FAKTOR --}}
        <div class="col-lg-5 overflow-auto d-flex align-items-start" style="height: 500px;">
            <ul class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                @foreach ($anlFkt as $no => $af)
                <li class='nav-item' style="list-style-type: square;">
                    <a  class='nav-link' 
                        id='analisisFaktor{{$af->id_urusan}}' 
                        data-bs-toggle='pill' 
                        href='#detilAnalisisFaktor{{$af->id_urusan}}'  
                        role='tab'  
                        aria-controls='analisisFaktor{{$af->id_urusan}}'  
                        aria-selected='false' 
                    >
                        <span>{{ $af->nama_urusan }}</span>
                    </a>
                </li>
                @endforeach
            </ul>    
        </div>        
        {{-- LIST DETIL ANALISIS FAKTOR--}}
        <div class="col-lg-7 tab-content overflow-auto" style="height: 500px;">
            @foreach ($anlFkt as $no => $af)
            @php
            // dd($anlFkt);
            @endphp
                <div    class='tab-pane fade'    {{-- tab-pane --}}
                        id='detilAnalisisFaktor{{$af->id_urusan}}'
                        role='tabpanel'  
                        aria-labelledby='analisisFaktor{{$af->id_urusan}}-tab' 
                >
                    <hr />
                    <div class='container'> {{-- (mulai) container --}}
                        <h5 class='fw-bold text-center mb-3'>{{$af->nama_urusan}}</h5>{{-- nama urusan --}}
                        <hr/>
                        @foreach($af->viewbidangurusan as $noBu => $bu) {{-- Bidang Urusan --}}
                        <div class="row" style="background-color: rgb(32, 57, 123); color: white;">
                            <span><b>{{($bu->kode_bidang_urusan)}} {{$bu->nama_bidang_urusan}}</b></span>
                        </div>
                            @foreach($bu->viewpdrenja as $noPd => $pd) {{-- Perangkat Daerah --}}
                            {{-- FAKTOR PENDORONG DAN PENGHAMBAT --}}
                            @php
                                $dblTL_tw = '';
                                $dblTL_renja = '';
                                $dblFP = '';
                                $afFP = '';
                                $afFPH = '';
                                $afTL_tw = '';
                                $afTL_renja = '';

                                foreach ($pd->analisisfaktor as $af) {
                                    $afFPH = $af->faktor_pendorong;
                                    $afTL = $af->faktor_penghambat;
                                }
                                
                                foreach ($pd->tindaklanjut as $tl) {
                                    $afTL_tw = $tl->tl_triwulan;
                                    $afTL_renja = $tl->tl_renja;
                                }
                                if(Auth::user()->id_role != 1 && Auth::user()->id_role != 13)
                                {
                                    $dblTL_tw = 'disabled';
                                }
                                if(Auth::user()->id_role == 13)
                                {
                                    $dblFP = 'disabled';
                                    $dblTL_renja = 'disabled';
                                }
                            @endphp
                            <form id="formAF{{ $bu->id_bidang_urusan }}">
                                <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                                <input type='hidden' name='id_bidang_urusan' value='{{ $bu->id_bidang_urusan }}' />
                                <input type='hidden' name='id_renja' value='{{ $pd->id_renja }}' />
                                <input type='hidden' name='triwulan' value='1' />
                                <div class="row">
                                    <b>Faktor Pendorong</b>
                                </div>
                                <div class="row">
                                    <textarea class="form-control" name="faktor_pendorong" {{$dblFP}}></textarea>
                                </div>
                                <div class="row">
                                    <b>Faktor Penghambat</b>
                                </div>
                                <div class="row">
                                    <textarea class="form-control" name="faktor_penghambat" {{$dblFP}}></textarea>
                                </div>
                                <div class="row">
                                    <b>Tindak Lanjut Triwulan Berikutnya</b>
                                </div>   
                                <div class="row">
                                    <textarea class="form-control" name="tl_triwulan" {{$dblTL_tw}}>{{$afTL_tw}}</textarea>
                                </div>
                                <div class="row">
                                    <b>Tindak Lanjut RENJA</b>
                                </div>   
                                <div class="row">
                                    <textarea class="form-control" name="tl_renja" {{$dblTL_renja}}>{{$afTL_renja}}</textarea>
                                </div>
                                <div class="row">
                                    <div class='text-center'>
                                        <br/>                                                                                      
                                        <div id="messageAFSukses{{ $bu->id_bidang_urusan }}" style="
                                            width: 250px;
                                            height: 100px;">
                                            <p class="alert alert-success">
                                                <b>Data berhasil disimpan !</b>
                                            </p>
                                        </div>     
                                        <div id="messageAFGagal{{ $bu->id_bidang_urusan }}" style="
                                            width: 250px;
                                            height: 100px;">
                                            <p class="alert alert-danger">
                                                <b>Data ada yang gagal disimpan !</b>
                                            </p>
                                        </div> 
                                        @php 
                                            date_default_timezone_set("Asia/Jakarta");
                                            $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                            $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                            $sekarang  = date("Y-m-d H:i:s");
                                        @endphp
                                        @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                            <button type='submit' class='btn btn-primary btn-sm'>
                                                Simpan
                                            </button>
                                            <i class="fa fa-spinner" id="loadingAF{{ $bu->id_bidang_urusan }}"></i>
                                        @else
                                            <p class="alert alert-warning">
                                                <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                            </p>                                                
                                        @endif
                                    </div>
                                </div>        
                                <script>
                                    //Submit Form Analisis Faktor
                                    $("#messageAFSukses{{ $bu->id_bidang_urusan }}").hide();
                                    $("#messageAFGagal{{ $bu->id_bidang_urusan }}").hide();
                                    $("#loadingAF{{ $bu->id_bidang_urusan }}").hide();
            
                                    $( "#formAF{{ $bu->id_bidang_urusan }}" ).on( "submit", function(e) {
                                        var dataString = $(this).serialize();
                                        $("#formAF{{ $bu->id_bidang_urusan }} Button").hide();
                                        $("#loadingAF{{ $bu->id_bidang_urusan }}").show();
                                        $.ajax({
                                            type: "POST",
                                            url: "{{ url('/monev/renja/input/realisasi/AnlFaktor') }}",
                                            data: dataString,
                                            success: function (hasil) {
                                                console.log(hasil);
                                                var cek = true;
                                                $.each(hasil, function( index, value ) {
                                                    if(value.status != 200)
                                                    {
                                                        cek = false;
                                                    }
                                                });
                                                if(cek == true)
                                                {
                                                    $("#messageAFSukses{{ $bu->id_bidang_urusan }}").fadeTo(2000, 500).slideUp(500, function() {
                                                        $("#messageAFSukses{{ $bu->id_bidang_urusan }}").slideUp(500);
                                                    });
                                                }else{
                                                    $("#messageAFGagal{{ $bu->id_bidang_urusan }}").fadeTo(2000, 500).slideUp(500, function() {
                                                        $("#messageAFGagal{{ $bu->id_bidang_urusan }}").slideUp(500);
                                                    });
                                                }
                                                $("#formAF{{ $bu->id_bidang_urusan }} Button").show();
                                                $("#loadingAF{{ $bu->id_bidang_urusan }}").hide();
                                            },
                                            error: function () {
                                                $("#messageAFGagal{{ $bu->id_bidang_urusan }}").fadeTo(2000, 500).slideUp(500, function() {
                                                    $("#messageAFGagal{{ $bu->id_bidang_urusan }}").slideUp(500);
                                                });
                                                $("#formAF{{ $bu->id_bidang_urusan }} Button").show();
                                                $("#loadingAF{{ $bu->id_bidang_urusan }}").hide();
                                            }
                                        });
                                        e.preventDefault();
                                    });
                                </script>                                
                            </form> 
                            <hr/>
                            @endforeach
                        <br/>
                        @endforeach
                    </div>  {{-- (akhir) container --}}
                </div>
            @endforeach
        </div>
    </div>
</div>
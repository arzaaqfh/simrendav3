<div class="col-lg-12">
    <div class="d-flex align-items-start">
        {{-- LIST SUB KEGIATAN --}}
        <div class="col-lg-5 overflow-auto d-flex align-items-start" style="height: 500px;">
            <ul class="nav flex-column nav-pills me-3" id="v-pills-afSubKeg-tab" role="tablist" aria-orientation="vertical">
                @foreach ($subKeg as $no => $skg)
                <li class='nav-item' style="list-style-type: square;">
                    <a  class='nav-link' 
                        id='afsubkegiatanitem{{$skg->id_sub_kegiatan}}' 
                        data-bs-toggle='pill' 
                        href='#detilafsubkegiatanitem{{$skg->id_sub_kegiatan}}'  
                        role='tab'  
                        aria-controls='afsubkegiatanitem{{$skg->id_sub_kegiatan}}'  
                        aria-selected='false' 
                    >
                        <span>{{ $skg->nama_sub_kegiatan }}</span>
                    </a>
                </li>
                @endforeach
            </ul>        
        </div>        
        {{-- LIST INDIKATOR SUB KEGIATAN --}}
        <div class="col-lg-7 tab-content overflow-auto" style="height: 500px;">
            @foreach ($subKeg as $no => $skg)
            <div    class='tab-pane fade'    {{-- tab-pane --}}
                    id='detilafsubkegiatanitem{{$skg->id_sub_kegiatan}}'
                    role='tabpanel'  
                    aria-labelledby='afsubkegiatanitem{{$skg->id_sub_kegiatan}}-tab' 
            >
                <form id='formAFSubKeg{{$skg->id_sub_kegiatan}}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                    <div class='table-responsive'>{{-- (awal) div table --}}
                        <table id='formAFSubKegitem{{$skg->id_sub_kegiatan}}' class='table table-borderless'>{{-- (awal) table --}}
                            <thead>
                                <tr>
                                    <th class='text-center fw-bold text-center mb-3'><h5>Daftar Indikator Sub Kegiatan</h5></th>
                                </tr>
                            </thead>
                            @if (!is_null($indSubKeg[$skg->id_sub_kegiatan]))
                                <tbody>                            
                                    @foreach ($indSubKeg[$skg->id_sub_kegiatan] as $noInd => $indikator)
                                    <tr id='AFdetailIndSubKeg_{{$indikator->id_indikator_subkegiatan}}'>
                                        <td>
                                            <h5 class='mb-0'>Indikator #{{($noInd+1)}}</h5>
                                            <div class='border border-primary shadow-sm p-3 mb-3'>
                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class='col-lg-12'> {{-- nama indikator sub kegiatan --}}
                                                        <h6>Nama Indikator</h6>
                                                        <div id='AFnamaIndSubKeg_{{$indikator->id_indikator_subkegiatan}}' class='fw-bold'>
                                                            {{$indikator->indikator_subkegiatan}}
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- target indikator sub kegiatan --}}
                                                        <h6>Target</h6>
                                                        <div id="AFtargetIndSubKeg_{{$indikator->id_indikator_subkegiatan}}" class='fw-bold'>
                                                            <span class="target">{{$indikator->target}}</span>
                                                            <span class="target_perubahan">{{$indikator->target_perubahan}}</span>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- satuan indikator sub kegiatan --}}
                                                        <h6>Satuan</h6>
                                                        <div id="AFsatuanIndSubKeg_{{$indikator->id_indikator_subkegiatan}}" class='fw-bold'>
                                                            {{$indikator->satuan}}
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-5'> {{-- progress --}}
                                                        <h6>Capaian Kinerja Sub Kegiatan</h6>
                                                        @php
                                                            $persen_tw1 = 0;
                                                            $persen_tw2 = 0;
                                                            $persen_tw3 = 0;
                                                            $persen_perubahan = 0;
                                                            $target = floatval($indikator->target);
                                                            $target_perubahan = floatval($indikator->target_perubahan);
                                                            $t_1 = 0;
                                                            $t_2 = 0;
                                                            $t_3 = 0;
                                                            $t_4 = 0;
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_1))
                                                            {
                                                                $t_1 = floatval($indikator->realisasiindikatorsubkeg->t_1);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_2))
                                                            {
                                                                $t_2 = floatval($indikator->realisasiindikatorsubkeg->t_2);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_3))
                                                            {
                                                                $t_3 = floatval($indikator->realisasiindikatorsubkeg->t_3);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_4))
                                                            {
                                                                $t_4 = floatval($indikator->realisasiindikatorsubkeg->t_4);
                                                            }

                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && $target != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_tw1 = (($t_1)/$target*100)/4;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/4;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_tw1 = (($t_1)/$target*100)/2;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/2;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_tw1 = ($t_1)/$target*100;
                                                                    $persen_tw2 = ($t_1+$t_2)/$target*100;
                                                                    $persen_tw3 = ($t_1+$t_2+$t_3)/$target*100;
                                                                }
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && $target_perubahan != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_perubahan = (($t_1+$t_2+$t_3+$t_4)/$target_perubahan*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_perubahan = ($t_4/$target_perubahan*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_perubahan = $t_4/$target_perubahan*100;
                                                                }
                                                            }
                                                        @endphp
                                                        <div class="capaian mt-3">
                                                            {{-- capaian triwulan 1 --}}
                                                            <div class="capaian-bar capaian-tw1">
                                                                <div
                                                                    id="capaianAnlSubKeg-tw1-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw1}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianAnlPersenSubKeg-tw1-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw1,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 2 --}}
                                                            <div class="capaian-bar capaian-tw2">
                                                                <div
                                                                    id="capaianAnlSubKeg-tw2-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw2}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianAnlPersenSubKeg-tw2-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw2,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 3 --}}
                                                            <div class="capaian-bar capaian-tw3">
                                                                <div
                                                                    id="capaianAnlSubKeg-tw3-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw3}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianAnlPersenSubKeg-tw3-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw3,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 4 --}}
                                                            <div class="capaian-bar capaian-tw4">
                                                                <div
                                                                    id="capaianAnlSubKeg-tw4-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_perubahan}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianAnlPersenSubKeg-tw4-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_perubahan,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-3'> {{-- teks tercapai atau belum tercapai --}}
                                                        <h6>Status</h6>
                                                        <div class='fw-bold'>
                                                            @php
                                                                $status_tw1 = '-';
                                                                $status_tw2 = '-';
                                                                $status_tw3 = '-';
                                                                $status_tw4 = '-';
                                                                if($indikator->rumus && ($indikator->rumus == 1 || $indikator->rumus == 6))
                                                                {
                                                                    // jika rumus positif
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }else if($indikator->rumus && ($indikator->rumus == 2 || $indikator->rumus == 7))
                                                                {
                                                                    // jika rumus negatif                                                                    
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }
                                                                if(ctype_alpha($indikator->target))
                                                                {
                                                                    $status_tw1 = '-';
                                                                    $status_tw2 = '-';
                                                                    $status_tw3 = '-';
                                                                    $status_tw4 = '-';
                                                                }
                                                            @endphp
                                                            <span id='badge-sub-kegiatan-progress'
                                                                    class='badge bg-badge'
                                                            >
                                                                <b class="capaian-tw1" style="color:black; font-size: 14px">
                                                                    {{$status_tw1}}
                                                                </b>
                                                                <b class="capaian-tw2" style="color:black; font-size: 14px">
                                                                    {{$status_tw2}}
                                                                </b>
                                                                <b class="capaian-tw3" style="color:black; font-size: 14px">
                                                                    {{$status_tw3}}
                                                                </b>
                                                                <b class="capaian-tw4" style="color:black; font-size: 14px">
                                                                    {{$status_tw4}}
                                                                </b>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            @endif
                        </table>{{-- (akhir) table --}} 
                    </div>{{-- (akhir) div table --}}
                    <br />
                    <div class='container'> {{-- (mulai) container --}}
                        {{-- Analisis Faktor Sub Kegiatan --}}
                        <input type="hidden" name="id_sub_kegiatan" value="{{$skg->id_sub_kegiatan}}"/>
                        <input type="hidden" name="id_renja" value="{{$anlSKG[1][$skg->id_sub_kegiatan]->id_renja}}"/>
                        <h5 class='fw text-center mb-3'>Analisis Faktor</h5>
                        <center>
                            <button class="btn btn-primary" type="button" id="btn-show-card-fp-{{$skg->id_sub_kegiatan}}" onclick="showCard('fp',{{$skg->id_sub_kegiatan}})">Faktor Pendorong</button>
                            <button class="btn btn-primary" type="button" id="btn-show-card-fph-{{$skg->id_sub_kegiatan}}" onclick="showCard('fph',{{$skg->id_sub_kegiatan}})">Faktor Penghambat</button>
                            <button class="btn btn-primary" type="button" id="btn-show-card-tl-{{$skg->id_sub_kegiatan}}" onclick="showCard('tl',{{$skg->id_sub_kegiatan}})">Tindak Lanjut</button>
                        </center>
                        <br/>
                        <div style="height: 500px">
                            <div id="card-fp-{{$skg->id_sub_kegiatan}}" class="card card-fp">
                                <div class="card-body">
                                    {{-- Faktor Pendorong --}}
                                    <h5 class='fw mb-3'>Faktor Pendorong</h5>
                                    <textarea class="form-control" placeholder="faktor pendorong merupakan faktor yang mendorong pencapaian kinerja baik dari segi anggaran, sumber daya, waktu pelaksanaan dsb." rows="15" cols="30" name="inputKeteranganFP"></textarea>
                                </div>
                            </div>
                            <div  id="card-fph-{{$skg->id_sub_kegiatan}}" class="card card-fph">
                                <div class="card-body">
                                    {{-- Faktor Penghambat --}}
                                    <h5 class='fw mb-3'>Faktor Penghambat</h5>
                                    <textarea class="form-control" placeholder="faktor penghambat merupakan faktor yang menghambat pencapaian kinerja baik dari segi anggaran, sumber daya, waktu pelaksanaan, dsb." rows="15" cols="30" name="inputKeteranganFPH"></textarea>
                                </div>
                            </div>
                            <div id="card-tl-{{$skg->id_sub_kegiatan}}" class="card card-tl">
                                <div class="card-body">
                                    {{-- Tindak Lanjut --}}
                                    <h5 class='fw mb-3'>Tindak Lanjut</h5>                                  
                                    <textarea class="form-control" placeholder="" rows="15" cols="30" name="inputKeteranganTL"></textarea>
                                    <p></p>
                                </div>
                            </div>
                        </div>                        
                    </div><br/>   
                    {{-- tombol submit form realisasinya --}}
                    <div class="row">
                         <div id="messageAFSubKegSukses{{$skg->id_sub_kegiatan}}" style="
                            width: 250px;
                            height: 100px;">
                            <p class="alert alert-success">
                                <b>Data berhasil disimpan !</b>
                            </p>
                        </div>     
                        <div id="messageAFSubKegGagal{{$skg->id_sub_kegiatan}}" style="
                            width: 250px;
                            height: 100px;">
                            <p class="alert alert-danger">
                                <b>Data ada yang gagal disimpan !</b>
                            </p>
                        </div> 
                        <div class='text-center'>
                            @php
                                date_default_timezone_set("Asia/Jakarta");
                                $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                $sekarang  = date("Y-m-d H:i:s");
                            @endphp
                            @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                {{-- @if(Auth::user()->id_role != 13) --}}
                                <button type='submit' class='btn btn-primary btn-sm' id='btn_afsubkeg_{{$skg->id_sub_kegiatan}}'>
                                    Simpan
                                </button>
                                <i class="fa fa-spinner" id="loadingAFSubKeg{{$skg->id_sub_kegiatan}}"></i>
                                {{-- @endif --}}
                            @else
                                <p class="alert alert-warning">
                                    <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                </p>                                                
                            @endif
                        </div> 
                    </div>       
                    <script>
                        //Submit Form Realisasi Indikator Sub Kegiatan
                        $("#messageAFSubKegSukses{{$skg->id_sub_kegiatan}}").hide();
                        $("#messageAFSubKegGagal{{$skg->id_sub_kegiatan}}").hide();
                        $("#loadingAFSubKeg{{$skg->id_sub_kegiatan}}").hide();

                        $("#formAFSubKeg{{$skg->id_sub_kegiatan}}" ).on( "submit", function(e) {
                            var dataString = $(this).serialize();
                            $("#formIndSubKeg{{$skg->id_sub_kegiatan}} Button").hide();
                            $("#loadingAFSubKeg{{$skg->id_sub_kegiatan}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('monev/renja/input/analisisFaktor/SubKegiatan') }}",
                                data: dataString,
                                success: function (hasil) {
                                    console.log('hasil: ',hasil);
                                    var cek = true;
                                    $.each(hasil, function( index, value ) {
                                        if(value.status != 200)
                                        {
                                            cek = false;
                                        }
                                    });
                                    if(cek == true)
                                    {
                                        $("#messageAFSubKegSukses{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAFSubKegSukses{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                        reloadAnlSubKeg('{{$skg->id_sub_kegiatan}}');
                                    }else{
                                        $("#messageAFSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAFSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                    }
                                    $("#btn_afsubkeg_{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingAFSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                },
                                error: function () {
                                    $("#messageAFSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageAFSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                    });
                                    $("#btn_afsubkeg_{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingAFSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                }
                            });
                            e.preventDefault();
                        });
                    </script>
                </form>
            </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    $('.card-fp').show();
    $('.card-fph').hide();
    $('.card-tl').hide();
    var id_role = '{{Auth::user()->id_role}}';
    if(id_role != 13 && id_role != 1)
    {
        $('.card-tl').find('textarea').prop('disabled', true);
    }
    if(id_role == 13)
    {
        $('.card-fp').find('textarea').prop('disabled', true);
        $('.card-fph').find('textarea').prop('disabled', true);
    }
    // set status input realisasi per sub kegiatan
    async function setStatInputAnlSubKeg(twl)
    {
        var listItems = $("#v-pills-afSubKeg-tab li");
        var cek = 0; //jumlah yang belum diisi
        var statInput = <?php echo json_encode($statAnlSKG) ?>;
        var indexStat;
        listItems.each(function(idx, li) {
            var link = $(li).find('a');
            var icon = $(li).find('i');
            indexStat = link.attr('id').replace('afsubkegiatanitem','');
            
            if(statInput[twl][indexStat] == 2)
            {
                link.attr('class','nav-link link-success');
                icon.attr('class','fa fa-check-circle');
            }else{
                link.attr('class','nav-link');
                icon.attr('class','');
            }
        });
    }
    // set input analisis sub kegiatan
    async function setInputValAnlSKG(twl)
    {
        var anlSKG =  <?php echo json_encode($anlSKG) ?>;
        var sessionTW = <?php echo Session::get('triwulan') ?>;
        $("textarea").prop('disabled', true);
        // if(twl == sessionTW)
        // {
        //     $("textarea").prop('disabled', false);
        //     if(id_role != 13 && id_role != 1)
        //     {
        //         $('.card-tl').find('textarea').prop('disabled', true);
        //     }
        // }
        if(twl == sessionTW)
        {
            $("textarea").prop('disabled', false);
            if(id_role != 13 && id_role != 1)
            {
                $('.card-tl').find('textarea').prop('disabled', true);
            }
        }
        $.each(anlSKG[twl], function (id_sub_kegiatan, val) {
            // input box
            // faktor pendorong
            $('#card-fp-'+id_sub_kegiatan).find('textarea').val(null);
            // faktor penghambat
            $('#card-fph-'+id_sub_kegiatan).find('textarea').val(null);
            // tindak lanjut
            $('#card-tl-'+id_sub_kegiatan).find('textarea').val(null);
            
            if(val.analisisfp.length > 0)
            {
                $('#card-fp-'+id_sub_kegiatan).find('textarea').val(val.analisisfp[0].keterangan);
            }            
            if(val.analisisfph.length > 0)
            {
                $('#card-fph-'+id_sub_kegiatan).find('textarea').val(val.analisisfph[0].keterangan);
            }            
            if(val.analisistl.length > 0)
            {
                $('#card-tl-'+id_sub_kegiatan).find('textarea').val(val.analisistl[0].keterangan);
            }
        });
    }

    async function showCard(card_name,id_sub_kegiatan)
    {
        if(card_name == 'fp')
        {
            $("#card-fp-"+id_sub_kegiatan).animate({
                height: 'toggle'
            });
            $("#card-fph-"+id_sub_kegiatan).hide();
            $("#card-tl-"+id_sub_kegiatan).hide();
        }else if(card_name == 'fph')
        {
            $("#card-fp-"+id_sub_kegiatan).hide();
            $("#card-fph-"+id_sub_kegiatan).animate({
                height: 'toggle'
            });
            $("#card-tl-"+id_sub_kegiatan).hide();
        }else if(card_name == 'tl')
        {
            $("#card-fp-"+id_sub_kegiatan).hide();
            $("#card-fph-"+id_sub_kegiatan).hide();
            $("#card-tl-"+id_sub_kegiatan).animate({
                height: 'toggle'
            });
        }
    }
    
    //memuat ulang semua data analisis faktor indikator sub kegiatan 
    async function reloadAnlSubKeg(id_sub_kegiatan)
    {
            
        var token = '{{$token}}'; //token
        var urlIndSubkeg = "{{url('/api/renja/get_analisissubkegiatan')}}"; //alamat api analisis faktor sub kegiatan
        var tahun = "{{session()->get('tahun_monev')}}"; //tahun monev
        var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
        var userRole = "{{ Auth::user()->id_role }}";
        
        if(userRole == 1)
        {
            id_skpd = "{{Session::get('pilih_id_skpd')}}";
        }else{
            id_skpd = "{{Auth::user()->viewuser->id_skpd}}";
        }

        $.ajax({
            type        :   "GET",
            url         :   urlIndSubkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'triwulan':twl,
                                'tahun':tahun,
                                'opd':id_skpd,
                                'subkeg':id_sub_kegiatan
                            },
            success     :   function (responses) {
                                $('#afsubkegiatanitem'+id_sub_kegiatan).attr('class','nav-link link-success');
                                $.each(responses, function (i, val)
                                {
                                    // faktor pendorong
                                    if(val[0].analisisfp.length > 0)
                                    {                                        
                                        $.each(val[0].analisisfp, function(ifp, vfp)
                                        {
                                            $('#v-pills-dana-skg-'+id_sub_kegiatan).find('textarea').val(vfp.dana);
                                            $('#v-pills-sdm-skg-'+id_sub_kegiatan).find('textarea').val(vfp.sdm);
                                            $('#v-pills-waktu-pelaksanaan-skg-'+id_sub_kegiatan).find('textarea').val(vfp.waktu_pelaksanaan);
                                            $('#v-pills-peraturan-perundangan-skg-'+id_sub_kegiatan).find('textarea').val(vfp.peraturan_perundangan);
                                            $('#v-pills-spbj-skg-'+id_sub_kegiatan).find('textarea').val(vfp.sistem_pengadaan_barang_jasa);
                                            $('#v-pills-perijinan-skg-'+id_sub_kegiatan).find('textarea').val(vfp.perijinan);
                                            $('#v-pills-ketersediaan-lahan-skg-'+id_sub_kegiatan).find('textarea').val(vfp.ketersediaan_lahan);
                                            $('#v-pills-kdm-skg-'+id_sub_kegiatan).find('textarea').val(vfp.kesiapan_dukungan_masyarakat);
                                            $('#v-pills-faktor-alam-skg-'+id_sub_kegiatan).find('textarea').val(vfp.faktor_alam);
                                            $('#v-pills-keterangan-skg-'+id_sub_kegiatan).find('textarea').val(vfp.keterangan);
                                            if(vfp.dana != null)
                                            {
                                                $('#v-pills-dana-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vfp.sdm != null)
                                            {
                                                $('#v-pills-sdm-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vfp.waktu_pelaksanaan != null)
                                            {
                                                $('#v-pills-waktu-pelaksanaan-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.peraturan_perundangan != null)
                                            {
                                                $('#v-pills-peraturan-perundangan-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.sistem_pengadaan_barang_jasa != null)
                                            {    
                                                $('#v-pills-spbj-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.perijinan != null)
                                            {       
                                                $('#v-pills-perijinan-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.ketersediaan_lahan != null)
                                            {
                                                $('#v-pills-ketersediaan-lahan-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.kesiapan_dukungan_masyarakat != null)
                                            {  
                                                $('#v-pills-kdm-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.faktor_alam != null)
                                            {       
                                                $('#v-pills-faktor-alam-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfp.keterangan != null)
                                            {    
                                                $('#v-pills-keterangan-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                        });

                                    }
                                    // faktor penghambat
                                    if(val[0].analisisfph.length > 0)
                                    {                                        
                                        $.each(val[0].analisisfph, function(ifph, vfph)
                                        {
                                            $('#v-pills-dana-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.dana);
                                            $('#v-pills-sdm-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.sdm);
                                            $('#v-pills-waktu-pelaksanaan-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.waktu_pelaksanaan);
                                            $('#v-pills-peraturan-perundangan-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.peraturan_perundangan);
                                            $('#v-pills-spbj-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.sistem_pengadaan_barang_jasa);
                                            $('#v-pills-perijinan-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.perijinan);
                                            $('#v-pills-ketersediaan-lahan-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.ketersediaan_lahan);
                                            $('#v-pills-kdm-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.kesiapan_dukungan_masyarakat);
                                            $('#v-pills-faktor-alam-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.faktor_alam);
                                            $('#v-pills-keterangan-fph-skg-'+id_sub_kegiatan).find('textarea').val(vfph.keterangan);
                                            
                                            if(vfph.dana != null)
                                            {
                                                $('#v-pills-dana-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vfph.sdm != null)
                                            {
                                                $('#v-pills-sdm-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vfph.waktu_pelaksanaan != null)
                                            {
                                                $('#v-pills-waktu-pelaksanaan-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.peraturan_perundangan != null)
                                            {
                                                $('#v-pills-peraturan-perundangan-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.sistem_pengadaan_barang_jasa != null)
                                            {
                                                $('#v-pills-spbj-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.perijinan != null)
                                            {
                                                $('#v-pills-perijinan-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.ketersediaan_lahan != null)
                                            {
                                                $('#v-pills-ketersediaan-lahan-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.kesiapan_dukungan_masyarakat != null)
                                            {
                                                $('#v-pills-kdm-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.faktor_alam != null)
                                            {
                                                $('#v-pills-faktor-alam-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vfph.keterangan != null)
                                            {
                                                $('#v-pills-keterangan-fph-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                        });

                                    }
                                    // tindak lanjut
                                    if(val[0].analisistl.length > 0)
                                    {                                        
                                        $.each(val[0].analisistl, function(itl, vtl)
                                        {
                                            $('#v-pills-dana-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.dana);
                                            $('#v-pills-sdm-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.sdm);
                                            $('#v-pills-waktu-pelaksanaan-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.waktu_pelaksanaan);
                                            $('#v-pills-peraturan-perundangan-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.peraturan_perundangan);
                                            $('#v-pills-spbj-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.sistem_pengadaan_barang_jasa);
                                            $('#v-pills-perijinan-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.perijinan);
                                            $('#v-pills-ketersediaan-lahan-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.ketersediaan_lahan);
                                            $('#v-pills-kdm-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.kesiapan_dukungan_masyarakat);
                                            $('#v-pills-faktor-alam-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.faktor_alam);
                                            $('#v-pills-keterangan-tl-skg-'+id_sub_kegiatan).find('textarea').val(vtl.keterangan);
                                            
                                            if(vtl.dana != null)
                                            {
                                                $('#v-pills-dana-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vtl.sdm != null)
                                            {
                                                $('#v-pills-sdm-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');
                                            }
                                            if(vtl.waktu_pelaksanaan != null)
                                            {
                                                $('#v-pills-waktu-pelaksanaan-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.peraturan_perundangan != null)
                                            {
                                                $('#v-pills-peraturan-perundangan-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.sistem_pengadaan_barang_jasa != null)
                                            {
                                                $('#v-pills-spbj-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.perijinan != null)
                                            {
                                                $('#v-pills-perijinan-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.ketersediaan_lahan != null)
                                            {
                                                $('#v-pills-ketersediaan-lahan-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.kesiapan_dukungan_masyarakat != null)
                                            {
                                                $('#v-pills-kdm-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.faktor_alam != null)
                                            {
                                                $('#v-pills-faktor-alam-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                            if(vtl.keterangan != null)
                                            {
                                                $('#v-pills-keterangan-tl-tab-skg-'+id_sub_kegiatan).attr('class','nav-link link-success');                    
                                            }
                                        });
                                    }
                                });
                            }    
        });
    }
</script>
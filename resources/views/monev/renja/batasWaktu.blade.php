@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>        
        <div class="main_content_iner ">
            <div class="container-fluid p-0">                
                <div class="white_card mb_30 ">
                    <div class="white_card_header">
                        <h4>Batas Waktu Input Renja</h4>
                    </div>
                    <div class="white_card_body">
                        <form method="POST" action="{{ url('monev/renja/bataswaktu/input') }}">
                            @csrf
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="mulai">Mulai</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <input type="datetime-local" class="form-control" name="mulai" id="mulai"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="akhir">Akhir</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <input type="datetime-local" class="form-control" name="akhir" id="akhir"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <input type="submit" class="btn btn-primary" value="SUBMIT">
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>                             
            </div>
        </div>

        <!-- Footer -->
        @include('layouts.footer')
@endsection
<table>
    <thead>
        <tr>    
            <th rowspan=2>No</th>
            <th rowspan=2>Kode</th>
            <th rowspan=2 colspan="3">Urusan/Bidang Urusan Pemerintahan Daerah dan Program/Kegiatan/Sub Kegiatan</th>
            <th rowspan=2>Indikator Kinerja Program (Outcome)/Kegiatan (Output) Kinerja Program (outcome)/Kegiatan (output)</th>
            <th rowspan=2 colspan=3>Target Kinerja dan Anggaran RKPD Tahun berjalan 2022 yang dievaluasi</th>
            <th colspan=8>Realisasi Kinerja Pada Triwulan</th>
            <th rowspan=2 colspan=2>Realisasi Capaian Kinerja dan Anggaran RKPD yang Dievaluasi 2022</th>
            <th rowspan=2 colspan=2>Tingkat Capaian Kinerja dan Realisasi Anggaran RKPD Tahun 2022</th>
            <th rowspan=2 colspan=2>Realisasi Kinerja dan Anggaran RPJMD s/d Tahun 2022 (Akhir Thn Pelaks RKPD)</th>
            <th rowspan=2 colspan=2>Tingkat Capaian Kinerja dan Realisasi Anggaran RPJMD s/d Tahun 2022 (%)</th>
            <th rowspan=2>Unit SKPD Penanggung Jawab</th>
        </tr>
        <tr>
            <th colspan=2>I</th>
            <th colspan=2>II</th >
            <th colspan=2>III</th >
            <th colspan=2>IV</th>
        </tr>
        <tr>    
            <th rowspan=2>1</th>
            <th rowspan=2>2</th>
            <th rowspan=2 colspan="3">3</th>
            <th rowspan=2>4</th>
            <th colspan=3>7</th>
            <th colspan=2>8</th>
            <th colspan=2>9</th>
            <th colspan=2>10</th>
            <th colspan=2>11</th>
            <th colspan=2>12</th>
            <th colspan=2>13</th>
            <th colspan=2>14=6+2</th>
            <th colspan=2>15=(13/5)x100%</th>
            <th rowspan=2>16</th>
        </tr>
        <tr>
            <th>K</th>
            <th>Satuan</th>    
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>
            <th>K</th>
            <th>Rp</th>
            <th>K</th>
            <th>Rp</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td colspan="26" class="bg-warning"><b>{{ $dat->kode_urusan.' '.$dat->nama_urusan }}</b></td>
        </tr>
        @if(count($dat->bidangurusan) > 0)
            @foreach($dat->bidangurusan as $bid)
                <tr>
                    <td colspan="26"><b>{{ $bid->kode_bidang_urusan.' '.$bid->nama_bidang_urusan }}</b></td>
                </tr>
                @if(count($bid->program) > 0)
                    @foreach($bid->program as $prog)
                        <tr>
                            <td></td>
                            <td></td>
                            <td colspan="24"><b>{{ $prog->nama_program }}</b></td>
                        </tr>
                        @if(count($prog->kegiatan) > 0)
                            @foreach($prog->kegiatan as $keg)
                                {{ $nokeg = 1; }}
                                @if(count($keg->indikatorkegiatan) > 0)
                                    @foreach($keg->indikatorkegiatan as $indkeg)
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td colspan=2><b>{{ $nokeg === 1 ? $keg->nama_kegiatan : "" }}</b></td>
                                            <td><b>{{ $indkeg->indikator_kegiatan }}</b></td>
                                            <td>{{ $indkeg->target }}</td>
                                            <td>{{ $indkeg->satuan }}</td>
                                            <td>{{ $nokeg === 1 ? (count($keg->apbd_kota) > 0 ? $keg->apbd_kota[array_search($indkeg->id_renja, array_column((array) $keg->apbd_kota, 'id_renja'))]->apbd_kota : 0) : "" }}</td>
                                            <td>{{ $indkeg->realisasiindikatorkeg != null ? $indkeg->realisasiindikatorkeg['t_1'] : "" }}</td>
                                            <td>{{ $nokeg === 1 ? (count($keg->realisasi_anggaran) > 0 ? $keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw1 : 0) : "" }}</td>
                                            <td>{{ $indkeg->realisasiindikatorkeg != null ? $indkeg->realisasiindikatorkeg['t_2'] : "" }}</td>
                                            <td>{{ $nokeg === 1 ? (count($keg->realisasi_anggaran) > 0 ? $keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw2 : 0) : "" }}</td>
                                            <td>{{ $indkeg->realisasiindikatorkeg != null ? $indkeg->realisasiindikatorkeg['t_3'] : "" }}</td>
                                            <td>{{ $nokeg === 1 ? (count($keg->realisasi_anggaran) > 0 ? $keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw3 : 0) : "" }}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $indkeg->realisasiindikatorkeg != null ? ($indkeg->realisasiindikatorkeg['persen_1']+$indkeg->realisasiindikatorkeg['persen_2']+$indkeg->realisasiindikatorkeg['persen_3']).'%' : "" }}</td>
                                            <td>{{ $nokeg === 1 ? (count($keg->apbd_kota) > 0 ? round(((($keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw1+$keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw2+$keg->realisasi_anggaran[array_search($indkeg->id_renja, array_column((array) $keg->realisasi_anggaran, 'id_renja'))]->realisasi_tw3)/$keg->apbd_kota[array_search($indkeg->id_renja, array_column((array) $keg->apbd_kota, 'id_renja'))]->apbd_kota)*100),2).'%' : 0) : "" }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $indkeg->renja->skpd->nama_skpd }}</td>
                                        </tr>
                                        {{ $nokeg++; }}
                                    @endforeach
                                @endif
                                @if(count($keg->subkegiatan) > 0)
                                    @foreach($keg->subkegiatan as $sub)
                                        {{ $nosub = 1; }}
                                        @if(count($sub->indikatorsubkegiatan) > 0)
                                            @foreach($sub->indikatorsubkegiatan as $indsub)
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $nosub === 1 ? $sub->nama_sub_kegiatan : "" }}</td>
                                                    <td>{{ $indsub->indikator_subkegiatan }}</td>
                                                    <td>{{ $indsub->target }}</td>
                                                    <td>{{ $indsub->satuan }}</td>
                                                    <td>{{ $nosub === 1 ? (count($sub->apbd_kota) > 0 ? $sub->apbd_kota[array_search($indsub->id_renja, array_column((array) $sub->apbd_kota, 'id_renja'))]->apbd_kota : 0) : "" }}</td>
                                                    <td>{{ $indsub->realisasiindikatorsubkeg != null ? $indsub->realisasiindikatorsubkeg['t_1'] : "" }}</td>
                                                    <td>{{ $nosub === 1 ? (count($sub->realisasi_anggaran) > 0 ? $sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw1 : 0) : "" }}</td>
                                                    <td>{{ $indsub->realisasiindikatorsubkeg != null ? $indsub->realisasiindikatorsubkeg['t_2'] : "" }}</td>
                                                    <td>{{ $nosub === 1 ? (count($sub->realisasi_anggaran) > 0 ? $sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw2 : 0) : "" }}</td>
                                                    <td>{{ $indsub->realisasiindikatorsubkeg != null ? $indsub->realisasiindikatorsubkeg['t_3'] : "" }}</td>
                                                    <td>{{ $nosub === 1 ? (count($sub->realisasi_anggaran) > 0 ? $sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw3 : 0) : "" }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $indsub->realisasiindikatorsubkeg != null ? $indsub->realisasiindikatorsubkeg['persen_1']+$indsub->realisasiindikatorsubkeg['persen_2']+$indsub->realisasiindikatorsubkeg['persen_3'].'%' : "" }}</td>
                                                    <td>{{ $nosub === 1 ? (count($sub->apbd_kota) > 0 ? round(((($sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw1+$sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw2+$sub->realisasi_anggaran[array_search($indsub->id_renja, array_column((array) $sub->realisasi_anggaran, 'id_renja'))]->realisasi_tw3)/$sub->apbd_kota[array_search($indsub->id_renja, array_column((array) $sub->apbd_kota, 'id_renja'))]->apbd_kota)*100),2).'%' : 0) : "" }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $sub->logrenja90->renja->skpd->nama_skpd }}</td>
                                                </tr>
                                                {{ $nosub++; }}
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
    </tbody>
</table>
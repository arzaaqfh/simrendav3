<table>
    <thead>
        <tr>    
            <th rowspan=2>No</th>
            <th rowspan=2>Kode</th>
            <th rowspan=2 colspan="5">Urusan/Bidang Urusan Pemerintahan Daerah dan Program/Kegiatan/Sub Kegiatan</th>
            <th rowspan=2>Indikator Kinerja Program (Outcome)/Kegiatan (Output) Kinerja Program (outcome)/Kegiatan (output)</th>
            <th rowspan=2 colspan="3">Target RPJMD pada Tahun 2017 s/d 2022 (Periode RPJMD)</th>
            <th rowspan=2 colspan="3">Realisasi Capaian Kinerja RPJMD s/d RKPD Tahun 2021</th>
            <th rowspan=2 colspan=3>Target Kinerja dan Anggaran RKPD Tahun berjalan 2022 yang dievaluasi</th>
            <th colspan=8>Realisasi Kinerja Pada Triwulan</th>
            <th rowspan=2 colspan=2>Realisasi Capaian Kinerja dan Anggaran RKPD yang Dievaluasi 2022</th>
            <th rowspan=2 colspan=2>Tingkat Capaian Kinerja dan Realisasi Anggaran RKPD Tahun 2022</th>
            <th rowspan=2 colspan=2>Realisasi Kinerja dan Anggaran RPJMD s/d Tahun 2022 (Akhir Thn Pelaks RKPD)</th>
            <th rowspan=2 colspan=2>Tingkat Capaian Kinerja dan Realisasi Anggaran RPJMD s/d Tahun 2022 (%)</th>
            <th rowspan=2>Unit SKPD Penanggung Jawab</th>
            <th rowspan=2>Ket.</th>
        </tr>
        <tr>
            <th colspan=2>I</th>
            <th colspan=2>II</th >
            <th colspan=2>III</th >
            <th colspan=2>IV</th>
        </tr>
        <tr>    
            <th rowspan=2>1</th>
            <th rowspan=2>2</th>
            <th rowspan=2 colspan="5">3</th>
            <th rowspan=2>4</th>
            <th colspan=3>5</th>
            <th colspan=3>6</th>
            <th colspan=3>7</th>
            <th colspan=2>8</th>
            <th colspan=2>9</th>
            <th colspan=2>10</th>
            <th colspan=2>11</th>
            <th colspan=2>12</th>
            <th colspan=2>13</th>
            <th colspan=2>14=6+2</th>
            <th colspan=2>15=(13/5)x100%</th>
            <th rowspan=2>16</th>
            <th rowspan=2>17</th>
        </tr>
        <tr>
            <th>K</th>
            <th>Satuan</th>    
            <th>Rp</th> 
            <th>K</th>
            <th>Satuan</th>    
            <th>Rp</th> 
            <th>K</th>
            <th>Satuan</th>    
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>    
            <th>K</th>
            <th>Rp</th>
            <th>K</th>
            <th>Rp</th>
            <th>K</th>
            <th>Rp</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $result = array();
        foreach ($data as $element) {
            $result[$element->nama_urusan][$element->nama_bidang_urusan][$element->nama_program][$element->nama_kegiatan][$element->nama_sub_kegiatan][] = $element;
        }
    ?>
    @foreach($result as $key => $bu)
        {{ $urusan = true }}
        @foreach($bu as $kbu => $pro)
        {{ $bidang_urusan = true }}   
            @foreach($pro as $kpro => $keg)
            {{ $program = true }}    
                @foreach($keg as $kkeg => $sub)
                {{ $kegiatan = true }}
                    @foreach($sub as $ksub => $val)
                    {{ $sub_kegiatan = true }}
                        @foreach($val as $kind => $ind)
                            @if($urusan == true)
                            <tr>
                                <td></td>
                                <td>{{ '0'.(string)$ind->kode_urusan }}</td>
                                <td><b>{{ $ind->nama_urusan }}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            {{$urusan=false}}
                            @endif

                            @if($bidang_urusan == true)
                            <tr>
                                <td></td>
                                <td>{{ '0'.$ind->kode_bidang_urusan }}</td>
                                <td></td>
                                <td><b>{{ $ind->nama_bidang_urusan }}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            {{$bidang_urusan=false}}
                            @endif

                            @if($program == true)
                                <?php
                                    $hitindprog = 1;
                                    $arr_temp = [];
                                ?>
                                @if($ind->indi_prog != '' && $ind->indi_prog != null)
                                    <?php
                                        $arindp = array_map(function($v){
                                            if(count(explode("#", $v)) == 6){
                                                return array_combine(["tahun1", "tahun2","indikator","target","satuan","realisasi"], explode("#", $v));
                                            }
                                        }, explode(";", $ind->indi_prog));
                                    ?>
                                    @foreach($arindp as $arv)
                                        @if($arv)
                                            <?php
                                                $target21 = array_values(array_filter($arindp, function ($arindp) use ($arv) {
                                                    if($arindp){
                                                        return ($arindp['tahun1'] == 2021 && $arindp['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                                $target22 = array_values(array_filter($arindp, function ($arindp) use ($arv) {
                                                    if($arindp){
                                                        return ($arindp['tahun2'] == 2022 && $arindp['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                            ?>
                                            @if($hitindprog == 1 && !in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td>{{ '0'.$ind->kode_program }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td colspan="3"><b>{{ $ind->nama_program }}</b></td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['target'] : 'na' }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['realisasi'] : 'na' }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td>{{ count($target22) > 0 ? $target22[0]['target'] : 'na' }}</td>
                                                    <td>{{ count($target22) > 0 ? $target22[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @elseif(!in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td colspan="3"></td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['target'] : 'na' }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['realisasi'] : 'na' }}</td>
                                                    <td>{{ count($target21) > 0 ? $target21[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td>{{ count($target22) > 0 ? $target22[0]['target'] : 'na' }}</td>
                                                    <td>{{ count($target22) > 0 ? $target22[0]['satuan'] : 'na' }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @endif
                                            <?php
                                                $hitindprog++;
                                                array_push($arr_temp,$arv['indikator']);
                                            ?>
                                        @endif    
                                    @endforeach
                                @else
                                    <tr>
                                    <td></td>
                                    <td>{{ '0'.$ind->kode_program }}</td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="3"><b>{{ $ind->nama_program }}</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ $ind->nama_skpd }}</td>
                                    <tr>
                                @endif
                                {{$program=false}}
                            @endif

                            @if($kegiatan == true)
                                <?php
                                    $hitindkeg = 1;
                                    $arr_temp = [];
                                ?>
                                @if($ind->indi_keg != '' && $ind->indi_keg != null)
                                    <?php
                                        $arindk = array_map(function($v){
                                            if(count(explode("#", $v)) == 11){
                                                return array_combine(["m21", "p21","m22","indikator","target","targetp","satuan","r1","r2","r3","r4"], explode("#", $v));
                                            }
                                        }, explode(";", $ind->indi_keg));
                                    ?>
                                    @foreach($arindk as $arv)
                                        @if($arv)
                                            <?php
                                                $target21 = array_values(array_filter($arindk, function ($arindk) use ($arv) {
                                                    if($arindk){
                                                        return ($arindk['m21'] == 121 && $arindk['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                                $targetp21 = array_values(array_filter($arindk, function ($arindk) use ($arv) {
                                                    if($arindk){
                                                        return ($arindk['p21'] == 221 && $arindk['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                                $target22 = array_values(array_filter($arindk, function ($arindk) use ($arv) {
                                                    if($arindk){
                                                        return ($arindk['m22'] == 122 && $arindk['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                            ?>
                                            @if($hitindkeg == 1 && !in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td>{{ '0'.$ind->kode_kegiatan }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td colspan="2"><b>{{ $ind->nama_kegiatan }}</b></td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? (is_numeric($targetp21[0]['target']) && $targetp21[0]['target'] != 0 ? (( ($targetp21[0]['r1'] == null ? 0 : $targetp21[0]['r1'])+($targetp21[0]['r2'] == null ? 0 : $targetp21[0]['r2'])+($targetp21[0]['r3'] == null ? 0 : $targetp21[0]['r3'])+($targetp21[0]['r4'] == null ? 0 : $targetp21[0]['r4']))/$targetp21[0]['target'])*100 : 'NaN') : '') : (count($target21) > 0 ? (is_numeric($target21[0]['target']) && $target21[0]['target'] != 0 ? (( ($target21[0]['r1'] == null ? 0 : $target21[0]['r1'])+($target21[0]['r2'] == null ? 0 : $target21[0]['r2'])+($target21[0]['r3'] == null ? 0 : $target21[0]['r3']))/$target21[0]['target'])*100 : 'NaN') : '') }}</td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? $targetp21[0]['satuan'] : '') : (count($target21) > 0 ? $target21[0]['satuan'] : '') }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['target'] : 0) : 0 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['satuan'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r1'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r2'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r3'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @elseif(!in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? (is_numeric($targetp21[0]['target']) && $targetp21[0]['target'] != 0 ? (( ($targetp21[0]['r1'] == null ? 0 : $targetp21[0]['r1'])+($targetp21[0]['r2'] == null ? 0 : $targetp21[0]['r2'])+($targetp21[0]['r3'] == null ? 0 : $targetp21[0]['r3'])+($targetp21[0]['r4'] == null ? 0 : $targetp21[0]['r4']))/$targetp21[0]['target'])*100 : 'NaN') : '') : (count($target21) > 0 ? (is_numeric($target21[0]['target']) && $target21[0]['target'] != 0 ? (( ($target21[0]['r1'] == null ? 0 : $target21[0]['r1'])+($target21[0]['r2'] == null ? 0 : $target21[0]['r2'])+($target21[0]['r3'] == null ? 0 : $target21[0]['r3']))/$target21[0]['target'])*100 : 'NaN') : '') }}</td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? $targetp21[0]['satuan'] : '') : (count($target21) > 0 ? $target21[0]['satuan'] : '') }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['target'] : 0) : 0 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['satuan'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r1'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r2'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r3'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @endif
                                            <?php
                                                $hitindkeg++;
                                                array_push($arr_temp,$arv['indikator']);
                                            ?>
                                        @endif    
                                    @endforeach
                                @else
                                    <tr>
                                        <td></td>
                                        <td>{{ '0'.$ind->kode_kegiatan }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2"><b>{{ $ind->nama_kegiatan }}</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $ind->nama_skpd }}</td>
                                    <tr>
                                @endif
                                {{$kegiatan=false}}
                            @endif

                            @if($sub_kegiatan == true)
                                <?php
                                    $hitindsub = 1;
                                    $arr_temp = [];
                                ?>
                                @if($ind->indi_sub_keg != '' && $ind->indi_sub_keg != null)
                                    <?php
                                        $arinds = array_map(function($v){
                                            if(count(explode("#", $v)) == 11){
                                                return array_combine(["m21", "p21","m22","indikator","target","targetp","satuan","r1","r2","r3","r4"], explode("#", $v));
                                            }
                                        }, explode(";", $ind->indi_sub_keg));
                                    ?>
                                    @foreach($arinds as $arv)
                                        @if($arv)
                                            <?php
                                                $target21 = array_values(array_filter($arinds, function ($arinds) use ($arv) {
                                                    if($arinds){
                                                        return ($arinds['m21'] == 121 && $arinds['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                                $targetp21 = array_values(array_filter($arinds, function ($arinds) use ($arv) {
                                                    if($arinds){
                                                        return ($arinds['p21'] == 221 && $arinds['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                                $target22 = array_values(array_filter($arinds, function ($arinds) use ($arv) {
                                                    if($arinds){
                                                        return ($arinds['m22'] == 122 && $arinds['indikator'] == $arv['indikator']);
                                                    }    
                                                }));
                                            ?>
                                            @if($hitindsub == 1 && !in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td>{{ '0'.$ind->kode_sub_kegiatan }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_sub_kegiatan }}</td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? (is_numeric($targetp21[0]['target']) && $targetp21[0]['target'] != 0 ? (( ($targetp21[0]['r1'] == null ? 0 : $targetp21[0]['r1'])+($targetp21[0]['r2'] == null ? 0 : $targetp21[0]['r2'])+($targetp21[0]['r3'] == null ? 0 : $targetp21[0]['r3'])+($targetp21[0]['r4'] == null ? 0 : $targetp21[0]['r4']))/$targetp21[0]['target'])*100 : 'NaN') : '') : (count($target21) > 0 ? (is_numeric($target21[0]['target']) && $target21[0]['target'] != 0 ? (( ($target21[0]['r1'] == null ? 0 : $target21[0]['r1'])+($target21[0]['r2'] == null ? 0 : $target21[0]['r2'])+($target21[0]['r3'] == null ? 0 : $target21[0]['r3']))/$target21[0]['target'])*100 : 'NaN') : '') }}</td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? $targetp21[0]['satuan'] : '') : (count($target21) > 0 ? $target21[0]['satuan'] : '') }}</td>
                                                    <td>{{ $ind->anggaran_21 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['target'] : '') : '' }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['satuan'] :'') : '' }}</td>
                                                    <td>{{ $ind->anggaran_22 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r1'] :'') : '' }}</td>
                                                    <td>{{ $ind->r_anggaran_21_tw_1 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r2'] :'') : '' }}</td>
                                                    <td>{{ $ind->r_anggaran_21_tw_2 }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r3'] :'') : '' }}</td>
                                                    <td>{{ $ind->r_anggaran_21_tw_3 }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @elseif(!in_array($arv['indikator'], $arr_temp))
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $arv['indikator'] }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? (is_numeric($targetp21[0]['target']) && $targetp21[0]['target'] != 0 ? (( ($targetp21[0]['r1'] == null ? 0 : $targetp21[0]['r1'])+($targetp21[0]['r2'] == null ? 0 : $targetp21[0]['r2'])+($targetp21[0]['r3'] == null ? 0 : $targetp21[0]['r3'])+($targetp21[0]['r4'] == null ? 0 : $targetp21[0]['r4']))/$targetp21[0]['target'])*100 : 'NaN') : '') : (count($target21) > 0 ? (is_numeric($target21[0]['target']) && $target21[0]['target'] != 0 ? (( ($target21[0]['r1'] == null ? 0 : $target21[0]['r1'])+($target21[0]['r2'] == null ? 0 : $target21[0]['r2'])+($target21[0]['r3'] == null ? 0 : $target21[0]['r3']))/$target21[0]['target'])*100 : 'NaN') : '') }}</td>
                                                    <td>{{ $ind->perubahan == 1 ? (count($targetp21) > 0 ? $targetp21[0]['satuan'] : '') : (count($target21) > 0 ? $target21[0]['satuan'] : '') }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['target'] : '') : '' }}</td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['satuan'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r1'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r2'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td>{{ $ind->tahun_22 == 1 ? (count($target22) > 0 ? $target22[0]['r3'] :'') : '' }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ $ind->nama_skpd }}</td>
                                                </tr>
                                            @endif
                                            <?php
                                                $hitindsub++;
                                                array_push($arr_temp,$arv['indikator']);
                                            ?>
                                        @endif    
                                    @endforeach
                                @else
                                    <tr>
                                        <td></td>
                                        <td>{{ '0'.$ind->kode_sub_kegiatan }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $ind->nama_sub_kegiatan }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $ind->nama_skpd }}</td>
                                    <tr>
                                @endif
                                {{$sub_kegiatan=false}}
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach 
        @endforeach       
    @endforeach
    </tbody>
</table>
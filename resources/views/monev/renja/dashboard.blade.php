@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <div class="row ">
                <div class="col-12">
                    <div class="white_card mb_30 ">
                        <div class="white_card_header">
                            <div class="white_box_tittle list_header">
                                <h4><b>Rekap Input Perangkat Daerah</b></h4>
                            </div>
                            <div class="bulder_tab_wrapper">
                                <ul class="nav" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="kegiatan-tab" data-bs-toggle="tab" href="#Kegiatan" role="tab" aria-controls="Kegiatan" aria-selected="true">Kegiatan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="subkegiatan-tab" data-bs-toggle="tab" href="#Subkegiatan" role="tab" aria-controls="Subkegiatan" aria-selected="false">Sub Kegiatan</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="Kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                    <div class="builder_select">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <div class="QA_section">
                                                    <div class="QA_table mb_30">
                                                        <table class="table multiplegroup" id="table_kegiatan">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col" rowspan="2">Perangkat Daerah</th>
                                                                    <th scope="col" colspan="4">Murni</th>
                                                                    <th scope="col" colspan="2">Perubahan</th>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="col">Jumlah Indikator</th>
                                                                    <th scope="col">Triwulan 1</th>
                                                                    <th scope="col">Triwulan 2</th>
                                                                    <th scope="col">Triwulan 3</th>
                                                                    <th scope="col">Jumlah Indikator</th>
                                                                    <th scope="col">Triwulan 4</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Subkegiatan" role="tabpanel" aria-labelledby="subkegiatan-tab">
                                    <div class="builder_select">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <div class="QA_section">
                                                    <div class="QA_table mb_30">
                                                        <!-- table-responsive -->
                                                        <table class="table multiplegroup" id="table_subkegiatan">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col" rowspan="2">Perangkat Daerah</th>
                                                                    <th scope="col" colspan="4">Murni</th>
                                                                    <th scope="col" colspan="2">Perubahan</th>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="col">Jumlah Indikator</th>
                                                                    <th scope="col">Triwulan 1</th>
                                                                    <th scope="col">Triwulan 2</th>
                                                                    <th scope="col">Triwulan 3</th>
                                                                    <th scope="col">Jumlah Indikator</th>
                                                                    <th scope="col">Triwulan 4</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

    <script type="text/javascript">
        $( document ).ready(function() {
            var tokenUrl = "{{ Session::get('token') }}";
            var table = $('#table_kegiatan').DataTable( {
                ajax: {
                    url:"{{ url('/api/monev/renja/rekapinputkegiatan') }}",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + tokenUrl);
                    },
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        id_pd:{{$id_pd}}
                    },
                },
                columns: [ 
                { "data": "nama_skpd" },
                { "data": "jml_ind_kegiatan", 
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                        }else if(row.jml_kegiatan != row.jml_kegiatan_haveind){
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                        }else{
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_keg1_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_kegiatan || row.jml_kegiatan != row.jml_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_keg2_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_kegiatan || row.jml_kegiatan != row.jml_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_keg3_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_kegiatan || row.jml_kegiatan != row.jml_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { 
                    "data": "jml_ind_kegiatan_perubahan", 
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                        }else if(row.jml_kegiatan_perubahan != row.jml_kegiatan_haveind_perubahan){
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                        }else{
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                        }
                        return html
                    }
                    // "data": "jml_ind_kegiatan", 
                    // "render": function ( data, type, row, meta ) {
                    //     var html = ''
                    //     if(data == 0 || data == null){
                    //         html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                    //     }else if(row.jml_kegiatan != row.jml_kegiatan_haveind){
                    //         html += data+'<br>'
                    //         html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                    //     }else{
                    //         html += data+'<br>'
                    //         html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                    //     }
                    //     return html
                    // }
                },
                { 
                    "data": "jml_ind_keg4_havemonev_perubahan",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        console.log(data)
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_kegiatan_perubahan || row.jml_kegiatan_perubahan != row.jml_kegiatan_haveind_perubahan){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                    // "data": "jml_ind_keg4_havemonev",
                    // "render": function ( data, type, row, meta ) {
                    //     var html = ''
                    //     if(data == 0 || data == null){
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                    //     }else if(data != row.jml_ind_kegiatan || row.jml_kegiatan != row.jml_kegiatan_haveind){
                    //         html += data+'<br>'
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                    //     }else{
                    //         html += data+'<br>'
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                    //     }
                    //     return html
                    // }
                },
            ],
            "lengthChange": true,
            lengthMenu: [5, 10, 20, 50],
            dom: 'Blfrtip',
            buttons: [
                { 
                    extend: 'pdf',
                    title: 'Rekap Input Monev Kegiatan Hasil Renja Tahun '+<?=session()->get('tahun_monev');?>,
                    messageBottom: 'printed by: simrenda. ('+ new Date().toLocaleString() +')'
                },
            ]
        } );

            var tableSub = $('#table_subkegiatan').DataTable( {
                 ajax: {
                    url:"{{ url('/api/monev/renja/rekapinputsubkegiatan') }}",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + tokenUrl);
                    },
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        id_pd:{{$id_pd}}
                    },
                },
                columns: [ 
                { "data": "nama_skpd" },
                { "data": "jml_ind_sub_kegiatan", 
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                        }else if(row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                        }else{
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_subkeg1_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_sub_kegiatan || row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_subkeg2_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_sub_kegiatan || row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { "data": "jml_ind_subkeg3_havemonev",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_sub_kegiatan || row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                },
                { 
                    "data": "jml_ind_sub_kegiatan_perubahan", 
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                        }else if(row.jml_subkegiatan_perubahan != row.jml_subkegiatan_havind_perubahan){
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                        }else{
                            html += data+'<br>'
                            html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                        }
                        return html
                    }
                    // "data": "jml_ind_sub_kegiatan", 
                    // "render": function ( data, type, row, meta ) {
                    //     var html = ''
                    //     if(data == 0 || data == null){
                    //         html += '<span class="f_s_12 f_w_700" style="color:#FF8895;">belum ada indikator</span>'
                    //     }else if(row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                    //         html += data+'<br>'
                    //         html += '<span class="f_s_12 f_w_700" style="color:#FFC881;">indikator belum lengkap</span>'   
                    //     }else{
                    //         html += data+'<br>'
                    //         html += '<span class="f_s_12 f_w_700 text_color_11">indikator sudah lengkap</span>'
                    //     }
                    //     return html
                    // }
                },
                {
                    "data": "jml_ind_subkeg4_havemonev_perubahan",
                    "render": function ( data, type, row, meta ) {
                        var html = ''
                        if(data == 0 || data == null){
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                        }else if(data != row.jml_ind_sub_kegiatan_perubahan || row.jml_subkegiatan_perubahan != row.jml_subkegiatan_havind_perubahan){
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                        }else{
                            html += data+'<br>'
                            link = "#";
                            html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                        }
                        return html
                    }
                    // "data": "jml_ind_subkeg4_havemonev",
                    // "render": function ( data, type, row, meta ) {
                    //     var html = ''
                    //     if(data == 0 || data == null){
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FF8895;">belum input</span></a>'
                    //     }else if(data != row.jml_ind_sub_kegiatan || row.jml_subkegiatan != row.jml_sub_kegiatan_haveind){
                    //         html += data+'<br>'
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700" style="color:#FFC881;">input belum selesai</span></a>'   
                    //     }else{
                    //         html += data+'<br>'
                    //         link = "#";
                    //         html += '<a href="'+link+'" target="_blank"><span class="f_s_12 f_w_700 text_color_11">input sudah selesai</span></a>'
                    //     }
                    //     return html
                    // }
                },
            ],
            "lengthChange": true,
            lengthMenu: [5, 10, 20, 50],
            dom: 'Blfrtip',
            buttons: [
                { 
                    extend: 'pdf',
                    title: 'Rekap Input Monev Sub Kegiatan Hasil Renja Tahun '+<?=session()->get('tahun_monev');?>,
                    messageBottom: 'printed by: simrenda. ('+ new Date().toLocaleString() +')'
                },
            ]
        } );
        });
    </script>
    <!-- Footer -->
    @include('layouts.footer')
@endsection
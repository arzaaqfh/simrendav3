<table style="border: 1px solid black;">
    <thead style="font-size: 10px; text-align: center;">
        <tr>
            <th colspan="6"><b><h3>Analisis Faktor Bidang Urusan</h3></b></th>
        </tr>
        <tr>
            <th colspan="6"><b><h3>Kabupaten/Kota Cimahi</h3></b></th>
        </tr>
        <tr>
            <th colspan="6"><b><h3>Periode : {{Session::get('tahun_monev')}}</h3></b></th>
        </tr>
        <tr>
            <th colspan="6"><b><h3>Triwulan : {{$triwulan}}</h3></b></th>
        </tr>
        <tr>
            <th>Kode</th>
            <th>Urusan</th>
            <th>Bidang Urusan</th>
            <th>Faktor Pendorong</th>
            <th>Faktor Penghambat</th>
            <th>Tindak Lanjut</th>
        </tr>
    </thead>
    <tbody>
        {{-- Urusan --}}
        @foreach ($anlFkt as $urs)
            <tr>
                <td>{{$urs->kode_urusan}}</td>
                <td colspan="5">{{$urs->nama_urusan}}</td>
            </tr>
            {{-- Bidang Urusan --}}
            @foreach ($urs->viewbidangurusan as $bu)
                <tr>
                    <td>{{$bu->kode_bidang_urusan}}</td>
                    <td></td>
                    <td>{{$bu->nama_bidang_urusan}}</td> 
                    <td>
                        {{-- PD Renja --}}
                        @foreach ($bu->viewpdrenja as $pdr)
                            {{-- Analisis Faktor --}}
                            {{-- >> Faktor Pendorong --}}
                            @if(count($pdr->analisisfaktor)>0)
                                @foreach($pdr->analisisfaktor as $afBU)
                                    {{$afBU->faktor_pendorong}}
                                @endforeach
                            @endif
                        @endforeach
                    </td>
                    <td>
                        {{-- PD Renja --}}
                        @foreach ($bu->viewpdrenja as $pdr)
                            {{-- Analisis Faktor --}}
                            {{-- >> Faktor Penghambat --}}
                            @if(count($pdr->analisisfaktor)>0)
                                @foreach($pdr->analisisfaktor as $afBU)
                                    {{$afBU->faktor_penghambat}}
                                @endforeach
                            @endif
                        @endforeach                            
                    </td>
                    <td>
                        {{-- PD Renja --}}
                        @foreach ($bu->viewpdrenja as $pdr)
                            {{-- Analisis Faktor --}}
                            {{-- >> Tindak Lanjut --}}
                            @if(count($pdr->tindaklanjut) > 0)
                            
                            @endif
                        @endforeach
                    </td>
                </tr>                   
            @endforeach
        @endforeach
    </tbody>
</table>
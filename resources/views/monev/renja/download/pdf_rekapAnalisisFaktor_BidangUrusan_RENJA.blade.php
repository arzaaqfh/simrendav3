<!DOCTYPE html>
<html>

<head>
    <title>Simrenda | Kota Cimahi</title>
    <link rel="icon" href="{{ asset('storage/img/cimahi.png') }}" type = "image/x-icon">
    <!-- Inline CSS Boostrap 5.2 -->
    <style>
        @page { size: 25cm 45cm landscape; }
        body{
            font-family: "Open Sans", sans-serif;
        }
        /* bordered table */
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: %;
            border: 1px solid #ddd;
        }

        table tr th,
        table tr td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table tr th{
            font-size: 1rem;
            text-align: center;            
        }
        h3{
            text-align: center;
        }
    </style>
</head>

<body>
    <center>
        <h3>Analisis Faktor Bidang Urusan</h3>
        <h3>Kabupaten/Kota Cimahi</h3>
        <h3>Periode : {{Session::get('tahun_monev')}}</h3>
        <h3>Triwulan : {{$triwulan}}</h3>
    </center>

    <table style="border: 1px solid black;">
        <thead style="font-size: 10px; text-align: center;">
            <tr>
                <th>Kode</th>
                <th>Urusan</th>
                <th>Bidang Urusan</th>
                <th>Faktor Pendorong</th>
                <th>Faktor Penghambat</th>
                <th>Tindak Lanjut</th>
            </tr>
        </thead>
        <tbody>
            {{-- Urusan --}}
            @foreach ($anlFkt as $urs)
                <tr>
                    <td>{{$urs->kode_urusan}}</td>
                    <td colspan="5">{{$urs->nama_urusan}}</td>
                </tr>
                {{-- Bidang Urusan --}}
                @foreach ($urs->viewbidangurusan as $bu)
                    <tr>
                        <td>{{$bu->kode_bidang_urusan}}</td>
                        <td></td>
                        <td>{{$bu->nama_bidang_urusan}}</td> 
                        <td>
                            {{-- PD Renja --}}
                            @foreach ($bu->viewpdrenja as $pdr)
                                {{-- Analisis Faktor --}}
                                {{-- >> Faktor Pendorong --}}
                                @if(count($pdr->analisisfaktor)>0)
                                    @foreach($pdr->analisisfaktor as $afBU)
                                        {{$afBU->faktor_pendorong}}
                                    @endforeach
                                @endif
                            @endforeach
                        </td>
                        <td>
                            {{-- PD Renja --}}
                            @foreach ($bu->viewpdrenja as $pdr)
                                {{-- Analisis Faktor --}}
                                {{-- >> Faktor Penghambat --}}
                                @if(count($pdr->analisisfaktor)>0)
                                    @foreach($pdr->analisisfaktor as $afBU)
                                        {{$afBU->faktor_penghambat}}
                                    @endforeach
                                @endif
                            @endforeach                            
                        </td>
                        <td>
                            {{-- PD Renja --}}
                            @foreach ($bu->viewpdrenja as $pdr)
                                {{-- Analisis Faktor --}}
                                {{-- >> Tindak Lanjut --}}
                                @if(count($pdr->tindaklanjut) > 0)
                                
                                @endif
                            @endforeach
                        </td>
                    </tr>                   
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>

</html>

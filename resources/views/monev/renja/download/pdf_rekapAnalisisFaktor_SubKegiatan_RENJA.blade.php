<!DOCTYPE html>
<html>

<head>
    <title>Simrenda | Kota Cimahi</title>
    <link rel="icon" href="{{ asset('storage/img/cimahi.png') }}" type = "image/x-icon">
    <!-- Inline CSS Boostrap 5.2 -->
    <style>
        @page { size: 25cm 45cm landscape; }
        body{
            font-family: "Open Sans", sans-serif;
        }
        /* bordered table */
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: %;
            border: 1px solid #ddd;
        }

        table tr th,
        table tr td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table tr th{
            font-size: 1rem;
            text-align: center;            
        }
        h3{
            text-align: center;
        }
    </style>
</head>

<body>
    <center>
        <h3>Analisis Faktor Sub Kegiatan</h3>
        <h3>Perangkat Daerah {{$nama_skpd}} Kabupaten/Kota Cimahi</h3>
        <h3>Periode : {{Session::get('tahun_monev')}}</h3>
        <h3>Triwulan : {{$triwulan}}</h3>
    </center>

    <table style="border: 1px solid black;">
        <thead style="font-size: 10px; text-align: center;">
            <tr>
                <th>Kode</th>
                <th>Program</th>
                <th>Kegiatan</th>
                <th>Sub Kegiatan</th>
                <th>Faktor Pendorong</th>
                <th>Faktor Penghambat</th>
                <th>Tindak Lanjut</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataCetak as $urs)
                {{-- Bidang Urusan --}}
                @foreach ($urs->viewbidangurusan as $bu)
                    {{-- Program --}}
                    @foreach ($bu->viewprogram as $prg)
                    <tr>
                        <td>{{$prg->kode_program}}</td>
                        <td colspan="6">{{$prg->nama_program}}</td>
                    </tr>
                        {{-- Kegiatan --}}
                        @foreach ($prg->viewkegiatan as $kg)
                        <tr>
                            <td>{{$kg->kode_kegiatan}}</td>
                            <td></td>
                            <td colspan="5">{{$kg->nama_kegiatan}}</td>
                        </tr>
                            {{-- Sub Kegiatan --}}
                            @foreach ($kg->viewsubkegiatan as $skg)
                            <tr>
                                <td>{{$skg->kode_sub_kegiatan}}</td>
                                <td></td>
                                <td></td>
                                <td>{{$skg->nama_sub_kegiatan}}</td>
                                <td>
                                    {{-- Analisis Faktor Pendorong --}}
                                    @if(array_key_exists($skg->id_sub_kegiatan,$anlSKG))
                                        @if(count($anlSKG[$skg->id_sub_kegiatan]->analisisfp)>0)
                                            @foreach($anlSKG[$skg->id_sub_kegiatan]->analisisfp as $afp)
                                                @php
                                                    if($afp->dana != null){ echo $afp->dana.';';}
                                                    if($afp->sdm != null){echo $afp->sdm.';';}
                                                    if($afp->waktu_pelaksanaan != null){echo $afp->waktu_pelaksanaan.';';}
                                                    if($afp->peraturan_perundangan != null){echo $afp->peraturan_perundangan.';';}
                                                    if($afp->sistem_pengadaan_barang_jasa != null){echo $afp->sistem_pengadaan_barang_jasa.';';}
                                                    if($afp->perijinan != null){echo $afp->perijinan.';';}
                                                    if($afp->ketersediaan_lahan != null){echo $afp->ketersediaan_lahan.';';}
                                                    if($afp->kesiapan_dukungan_masyarakat != null){echo $afp->kesiapan_dukungan_masyarakat.';';}
                                                    if($afp->faktor_alam != null){echo $afp->faktor_alam.';';}
                                                    if($afp->keterangan != null){echo $afp->keterangan.';';}   
                                                @endphp
                                            @endforeach
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    {{-- Analisis Faktor Penghambat --}}
                                    @if(array_key_exists($skg->id_sub_kegiatan,$anlSKG))
                                        @if(count($anlSKG[$skg->id_sub_kegiatan]->analisisfph)>0)
                                            @foreach($anlSKG[$skg->id_sub_kegiatan]->analisisfph as $afph)
                                                @php
                                                    if($afph->dana != null){ echo $afph->dana.';';}
                                                    if($afph->sdm != null){echo $afph->sdm.';';}
                                                    if($afph->waktu_pelaksanaan != null){echo $afph->waktu_pelaksanaan.';';}
                                                    if($afph->peraturan_perundangan != null){echo $afph->peraturan_perundangan.';';}
                                                    if($afph->sistem_pengadaan_barang_jasa != null){echo $afph->sistem_pengadaan_barang_jasa.';';}
                                                    if($afph->perijinan != null){echo $afph->perijinan.';';}
                                                    if($afph->ketersediaan_lahan != null){echo $afph->ketersediaan_lahan.';';}
                                                    if($afph->kesiapan_dukungan_masyarakat != null){echo $afph->kesiapan_dukungan_masyarakat.';';}
                                                    if($afph->faktor_alam != null){echo $afph->faktor_alam.';';}
                                                    if($afph->keterangan != null){echo $afph->keterangan.';';}   
                                                @endphp
                                            @endforeach
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    {{-- Analisis Faktor Tindak Lanjut --}}
                                    @if(array_key_exists($skg->id_sub_kegiatan,$anlSKG))
                                        @if(count($anlSKG[$skg->id_sub_kegiatan]->analisistl)>0)
                                            @foreach($anlSKG[$skg->id_sub_kegiatan]->analisistl as $tl)
                                                @php
                                                    if($tl->dana != null){ echo $tl->dana.';';}
                                                    if($tl->sdm != null){echo $tl->sdm.';';}
                                                    if($tl->waktu_pelaksanaan != null){echo $tl->waktu_pelaksanaan.';';}
                                                    if($tl->peraturan_perundangan != null){echo $tl->peraturan_perundangan.';';}
                                                    if($tl->sistem_pengadaan_barang_jasa != null){echo $tl->sistem_pengadaan_barang_jasa.';';}
                                                    if($tl->perijinan != null){echo $tl->perijinan.';';}
                                                    if($tl->ketersediaan_lahan != null){echo $tl->ketersediaan_lahan.';';}
                                                    if($tl->kesiapan_dukungan_masyarakat != null){echo $tl->kesiapan_dukungan_masyarakat.';';}
                                                    if($tl->faktor_alam != null){echo $tl->faktor_alam.';';}
                                                    if($tl->keterangan != null){echo $tl->keterangan.';';}   
                                                @endphp
                                            @endforeach
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <title>Simrenda | Kota Cimahi</title>
    <link rel="icon" href="{{ asset('storage/img/cimahi.png') }}" type = "image/x-icon">
    <!-- Inline CSS Boostrap 5.2 -->
    <style>
        @page { size: 25cm 45cm landscape; }
        body{
            font-family: "Open Sans", sans-serif;
        }
        /* bordered table */
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: %;
            border: 1px solid #ddd;
        }

        table tr th,
        table tr td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table tr th{
            font-size: 1rem;
            text-align: center;            
        }
        h3{
            text-align: center;
        }
    </style>
</head>

<body>
    <center>
        <h3>Evaluasi Terhadap Hasil Renja Perangkat Daerah Lingkup Kabupaten/Kota</h3>
        <h3>Renja Perangkat Daerah {{$nama_skpd}} Kabupaten/Kota Cimahi</h3>
        <h3>Periode Pelaksanaan: {{Session::get('tahun_monev')}}</h3>
        <h3>Triwulan: {{$triwulan}}</h3>
    </center>

    Indikator dan target kinerja Perangkat Daerah Kabupaten/Kota yang mengacu pada sasaran RKPD:<br/>
    ................<br/>

    <table style="border: 1px solid black;">
        <thead style="font-size: 10px; text-align: center;">
            <tr>
                <td rowspan="2">Kode</td>
                <td rowspan="2">Urusan/Bidang Urusan/Program/Kegiatan/Sub Kegiatan</td>
                <td rowspan="2">Indikator Kinerja Program (<i>outcome</i>)/Kegiatan (<i>output</i>)/Sub Kegiatan</td>
                <td rowspan="2">Satuan</td>
                <td rowspan="2" colspan="2">Target Resntra PD pada Tahun {{Session::get('tahun_monev')}} (Akhir Periode Renstra PD)</td>
                <td rowspan="2" colspan="2">Realisasi Capaian Kinerja Renstra PD s.d. Renja SKPD Tahun Lalu ({{Session::get('tahun_monev')}})</td>
                <td rowspan="2" colspan="2">Target Kinerja dan Anggaran Renja PD Tahun berjalan yang dievaluasi (Tahun {{Session::get('tahun_monev')}})</td>
                <td colspan="8">Realisasi Kinerja Pada Triwulan</td>
                <td rowspan="2" colspan="2">Realisasi Capaian Kinerja dan Anggaran Renja PD yang dievaluasi Tahun {{Session::get('tahun_monev')}}</td>
                <td rowspan="2" colspan="2">Tingkat Capaian Kinerja dan Realisasi Anggaran Renja PD Tahun {{Session::get('tahun_monev')}} (%)</td>
                <td rowspan="2" colspan="2">Realisasi Kinerja dan Anggaran Renstra PD s/d Tahun {{Session::get('tahun_monev')-1}} (%)</td>
                <td rowspan="2" colspan="2">Tingkat Capaian Kinerja dan Realisasi Anggaran Renstra PD s/d Tahun {{Session::get('tahun_monev')-1}} (%)</td>
                <td rowspan="2">Unit PD Penanggung Jawab</td>
                <td rowspan="2">Keterangan</td>
            </tr>
            <tr>
                <td colspan="2">I</td>
                <td colspan="2">II</td>
                <td colspan="2">III</td>
                <td colspan="2">IV</td>
            </tr>
            <tr>
                <td rowspan="2">1</td>
                <td rowspan="2">2</td>
                <td rowspan="2">3</td>
                <td rowspan="2">4</td>
                <td colspan="2">5</td>
                <td colspan="2">6</td>
                <td colspan="2">7</td>
                <td colspan="2">8</td>
                <td colspan="2">9</td>
                <td colspan="2">10</td>
                <td colspan="2">11</td>
                <td colspan="2">12</td>
                <td colspan="2">13</td>
                <td colspan="2">14</td>
                <td colspan="2">15</td>
                <td rowspan="2">16</td>
                <td rowspan="2">17</td>
            </tr>
            <tr>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
                <td>K</td>
                <td>Rp</td>
            </tr>
        </thead>
        <tbody style="font-size: 9px;">
            @php $no = 0; @endphp
            {{-- Urusan --}}
            @foreach ($dataCetak as $af)
            <tr>
                <td>{{$af->kode_urusan}}</td>
                <td colspan="27">{{$af->nama_urusan}}</td>
            </tr>
                {{-- Bidang Urusan --}}
                @foreach ($af->viewbidangurusan as $bu)
                <tr>
                    <td>{{$bu->kode_bidang_urusan}}</td>
                    <td colspan="27">{{$bu->nama_bidang_urusan}}</td>
                </tr>
                    {{-- Program --}}
                    @foreach ($bu->viewprogram as $prg)
                    @php
                        $r_1 = 0;
                        $r_2 = 0;
                        $r_3 = 0;
                        $r_4 = 0;
                    @endphp
                    <tr>
                        <td>{{$prg->kode_program}}</td>
                        <td colspan="7">{{$prg->nama_program}}</td>
                        <td></td>
                        <td>{{number_format($prg->apbd_kota,2,',','.')}}</td>
                        <td></td>
                        <td>
                            @if($triwulan >= 1)
                            @php $r_1 = $prg->r_1; @endphp
                            {{number_format($prg->r_1,2,',','.')}}
                            @endif
                        </td>
                        <td></td>
                        <td>
                            @if($triwulan >= 2)
                            @php $r_2 = $prg->r_2; @endphp
                            {{number_format($prg->r_2,2,',','.')}}
                            @endif
                        </td>
                        <td></td>
                        <td>
                            @if($triwulan >= 3)
                            @php $r_3 = $prg->r_3; @endphp
                            {{number_format($prg->r_3,2,',','.')}}
                            @endif
                        </td>
                        <td></td>
                        <td>
                            @if($triwulan >= 4)
                            @php $r_4 = $prg->r_4; @endphp
                            {{number_format($prg->r_4,2,',','.')}}
                            @endif
                        </td>
                        <td></td>
                        <td>
                            {{
                                number_format($r_1+$r_2+$r_3+$r_4,2,',','.')
                            }}
                        </td>
                        <td></td>
                        <td>
                            @if($prg->apbd_kota != 0)
                            {{
                                number_format(($r_1+$r_2+$r_3+$r_4)/$prg->apbd_kota*100,2,',','.')
                            }}
                            @endif
                        </td>
                        <td colspan="6"></td>             
                    </tr>                            
                        {{-- Indikator Program --}}
                        @foreach ($prg->indikator as $indPrg)
                        <tr>
                            <td></td>
                            <td></td>
                            <td>{{$indPrg->indikator_program}}</td>
                            <td>{{$indPrg->satuan}}</td>
                            <td>
                                {{-- @if(count($indPrg->target_realisasi_i_k_p) > 0)
                                    {{$indPrg->target_realisasi_i_k_p[count($indPrg->target_realisasi_i_k_p)-1]->target}}
                                @endif --}}
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                @php $target_prg = 0; @endphp
                                @if(count($indPrg->target_realisasi_i_k_p) > 0)
                                    @if($triwulan < 4)
                                        {{$indPrg->target_realisasi_i_k_p[0]->target}}
                                        @php $target_prg = $indPrg->target_realisasi_i_k_p[0]->target; @endphp
                                    @elseif($triwulan == 4 && array_key_exists(3,$indPrg->target_realisasi_i_k_p))
                                        {{$indPrg->target_realisasi_i_k_p[3]->target}}
                                        @php $target_prg = $indPrg->target_realisasi_i_k_p[3]->target; @endphp
                                    @endif
                                @endif
                            <td></td>
                            <td>
                                @php $r_ikp = array(0,0,0,0); @endphp
                                @if(array_key_exists(0,$indPrg->target_realisasi_i_k_p) && $triwulan >= 1)
                                    {{ $indPrg->target_realisasi_i_k_p[0]->realisasi }}
                                    @php $r_ikp[0] = (float) $indPrg->target_realisasi_i_k_p[0]->realisasi; @endphp
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if(array_key_exists(1,$indPrg->target_realisasi_i_k_p) && $triwulan >= 2)
                                    {{ $indPrg->target_realisasi_i_k_p[1]->realisasi }}
                                    @php $r_ikp[1] = (float) $indPrg->target_realisasi_i_k_p[1]->realisasi; @endphp
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if(array_key_exists(2,$indPrg->target_realisasi_i_k_p) && $triwulan >= 3)
                                    {{ $indPrg->target_realisasi_i_k_p[2]->realisasi }}
                                    @php $r_ikp[2] = (float) $indPrg->target_realisasi_i_k_p[2]->realisasi; @endphp
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if(array_key_exists(3,$indPrg->target_realisasi_i_k_p) && $triwulan >= 4)
                                    {{ $indPrg->target_realisasi_i_k_p[3]->realisasi }}
                                    @php $r_ikp[3] = (float) $indPrg->target_realisasi_i_k_p[3]->realisasi; @endphp
                                @endif
                            </td>
                            <td></td>
                            <td>                                
                                @php
                                    $total_r_ikp = 0;
                                   
                                    if($triwulan == 1)
                                    {
                                        $total_r_ikp = $r_ikp[0];
                                    }else if($triwulan == 2)
                                    {
                                        $total_r_ikp = ($r_ikp[0]+$r_ikp[1]);
                                    }else if($triwulan == 3)
                                    {
                                        $total_r_ikp = ($r_ikp[0]+$r_ikp[1]+$r_ikp[2]);
                                    }else if($triwulan == 4)
                                    {
                                        $total_r_ikp = ($r_ikp[0]+$r_ikp[1]+$r_ikp[2]+$r_ikp[3]);
                                    }                                    
                                @endphp
                                {{ $total_r_ikp }}
                            </td>
                            <td></td>
                            <td>
                                @if((float)$target_prg != 0)
                                    @php
                                        $persenProg = 0;
                                        if($indPrg->id_rumus == 1)
                                        {
                                            // rumus triwulan
                                            if($triwulan == 1)
                                            {
                                                $persenProg = ($r_ikp[0]/(float)$target_prg*100)/4;
                                            }else if($triwulan == 2)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1])/(float)$target_prg*100)/4;
                                            }else if($triwulan == 3)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1]+$r_ikp[2])/(float)$target_prg*100)/4;
                                            }else if($triwulan == 4)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1]+$r_ikp[2]+$r_ikp[3])/(float)$target_prg*100)/4;
                                            }
                                        }else if($indPrg->id_rumus == 2)
                                        {
                                            // rumus semester
                                            if($triwulan == 1)
                                            {
                                                $persenProg = ($r_ikp[0]/(float)$target_prg*100)/2;
                                            }else if($triwulan == 2)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1])/(float)$target_prg*100)/2;
                                            }else if($triwulan == 3)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1]+$r_ikp[2])/(float)$target_prg*100)/2;
                                            }else if($triwulan == 4)
                                            {
                                                $persenProg = (($r_ikp[0]+$r_ikp[1]+$r_ikp[2]+$r_ikp[3])/(float)$target_prg*100)/2;
                                            }
                                        }else if($indPrg->id_rumus == 3)
                                        {
                                            // rumus semester
                                            if($triwulan == 1)
                                            {
                                                $persenProg = $r_ikp[0]/(float)$target_prg*100;
                                            }else if($triwulan == 2)
                                            {
                                                $persenProg = ($r_ikp[0]+$r_ikp[1])/(float)$target_prg*100;
                                            }else if($triwulan == 3)
                                            {
                                                $persenProg = ($r_ikp[0]+$r_ikp[1]+$r_ikp[2])/(float)$target_prg*100;
                                            }else if($triwulan == 4)
                                            {
                                                $persenProg = ($r_ikp[0]+$r_ikp[1]+$r_ikp[2]+$r_ikp[3])/(float)$target_prg*100;
                                            }
                                        }                                     
                                    @endphp
                                    {{number_format($persenProg,2,',','.')}}
                                @endif
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{$nama_skpd}}</td>
                            <td></td>
                        </tr>
                        @endforeach
                        {{-- Kegiatan --}}
                        @foreach($prg->viewkegiatan as $keg)
                        @php
                            $rkeg_1 = 0;
                            $rkeg_2 = 0;
                            $rkeg_3 = 0;
                            $rkeg_4 = 0;
                        @endphp
                        <tr>
                            <td>{{$keg->kode_kegiatan}}</td>
                            <td colspan="7">{{$keg->nama_kegiatan}}</td>
                            <td></td>
                            <td>
                                {{number_format($keg->apbd_kota,2,',','.')}}
                            </td>
                            <td></td>
                            <td>
                                @if($triwulan >= 1)
                                @php $rkeg_1 = $keg->r_1; @endphp
                                {{number_format($keg->r_1,2,',','.')}}
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if($triwulan >= 2)
                                @php $rkeg_2 = $keg->r_2; @endphp
                                {{number_format($keg->r_2,2,',','.')}}
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if($triwulan >= 3)
                                @php $rkeg_3 = $keg->r_3; @endphp
                                {{number_format($keg->r_3,2,',','.')}}
                                @endif
                            </td>
                            <td></td>
                            <td>
                                @if($triwulan >= 4)
                                @php $rkeg_4 = $keg->r_4; @endphp
                                {{number_format($keg->r_4,2,',','.')}}
                                @endif
                            </td>
                            <td></td>
                            <td>
                                {{
                                    number_format($rkeg_1+$rkeg_2+$rkeg_3+$rkeg_4,2,',','.')
                                }}
                            </td>
                            <td></td>
                            <td>
                                @if($keg->apbd_kota != 0)
                                {{
                                    number_format(($rkeg_1+$rkeg_2+$rkeg_3+$rkeg_4)/$keg->apbd_kota*100,2,',','.')
                                }}
                                @endif
                            </td>
                            <td colspan="6"></td>  
                        </tr>                            
                            {{-- Indikator Kegiatan --}}
                            @foreach ($keg->indikator as $indKeg)
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{$indKeg->indikator_kegiatan}}</td>
                                <td>{{$indKeg->satuan}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    @php $targetIndKeg = 0; @endphp
                                    @if($triwulan < 4)
                                        @php $targetIndKeg = $indKeg->target; @endphp
                                        {{$indKeg->target}}
                                    @elseif($triwulan == 4)
                                        @php $targetIndKeg = $indKeg->target_perubahan; @endphp
                                        {{$indKeg->target_perubahan}}
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @php $r_ikeg = array(0,0,0,0); @endphp
                                    @if(!is_null($indKeg->realisasiindikatorkeg) && $triwulan >= 1)
                                        {{$indKeg->realisasiindikatorkeg->t_1}}
                                        @php $r_ikeg[0] = (float)$indKeg->realisasiindikatorkeg->t_1; @endphp
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!is_null($indKeg->realisasiindikatorkeg) && $triwulan >= 2)
                                        {{$indKeg->realisasiindikatorkeg->t_2}}
                                        @php $r_ikeg[1] = (float)$indKeg->realisasiindikatorkeg->t_2; @endphp
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!is_null($indKeg->realisasiindikatorkeg) && $triwulan >= 3)
                                        {{$indKeg->realisasiindikatorkeg->t_3}}
                                        @php $r_ikeg[2] = (float)$indKeg->realisasiindikatorkeg->t_3; @endphp
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if(!is_null($indKeg->realisasiindikatorkeg) && $triwulan >= 4)
                                        {{$indKeg->realisasiindikatorkeg->t_4}}
                                        @php $r_ikeg[3] = (float)$indKeg->realisasiindikatorkeg->t_4; @endphp
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @php
                                        $total_r_ikeg = 0;
                                        if($triwulan == 1)
                                        {
                                            $total_r_ikeg = $r_ikeg[0];
                                        }else if($triwulan == 2)
                                        {
                                            $total_r_ikeg = ($r_ikeg[0]+$r_ikeg[1]);
                                        }else if($triwulan == 3)
                                        {
                                            $total_r_ikeg = ($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2]);
                                        }else if($triwulan == 4)
                                        {
                                            $total_r_ikeg = ($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2]+$r_ikeg[3]);
                                        }
                                    @endphp
                                    {{$total_r_ikeg}}
                                </td>
                                <td></td>
                                <td>
                                    @if((float) $targetIndKeg != 0)
                                        @php
                                            $persenIndKeg = 0;
                                            
                                                if($indKeg->rumus == 1)
                                                {
                                                    // rumus triwulan
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndKeg = ($r_ikeg[0]/(float)$targetIndKeg*100)/4;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1])/(float)$targetIndKeg*100)/4;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2])/(float)$targetIndKeg*100)/4;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2]+$r_ikeg[3])/(float)$targetIndKeg*100)/4;
                                                    }
                                                }else if($indKeg->rumus == 2)
                                                {
                                                    // rumus semester
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndKeg = ($r_ikeg[0]/(float)$targetIndKeg*100)/2;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1])/(float)$targetIndKeg*100)/2;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2])/(float)$targetIndKeg*100)/2;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndKeg = (($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2]+$r_ikeg[3])/(float)$targetIndKeg*100)/2;
                                                    }
                                                }else if($indKeg->rumus == 3)
                                                {
                                                    // rumus tahun
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndKeg = $r_ikeg[0]/(float)$targetIndKeg*100;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndKeg = ($r_ikeg[0]+$r_ikeg[1])/(float)$targetIndKeg*100;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndKeg = ($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2])/(float)$targetIndKeg*100;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndKeg = ($r_ikeg[0]+$r_ikeg[1]+$r_ikeg[2]+$r_ikeg[3])/(float)$targetIndKeg*100;
                                                    }
                                                }
                                        @endphp
                                        {{number_format($persenIndKeg,2,',','.')}}
                                    @endif
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{$nama_skpd}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                            {{-- Sub Kegiatan --}}
                            @foreach($keg->viewsubkegiatan as $subkeg)                            
                            @php
                                $rsubkeg_1 = 0;
                                $rsubkeg_2 = 0;
                                $rsubkeg_3 = 0;
                                $rsubkeg_4 = 0;
                            @endphp
                            <tr>
                                <td>{{$subkeg->kode_sub_kegiatan}}</td>
                                <td colspan="7">{{$subkeg->nama_sub_kegiatan}}</td>
                                <td></td>
                                <td>{{number_format($subkeg->apbd_kota,2,',','.')}}</td>
                                <td></td>
                                <td>
                                    @if($triwulan >= 1)
                                    @php $rsubkeg_1 = $subkeg->r_1; @endphp
                                        {{number_format($subkeg->r_1,2,',','.')}}
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if($triwulan >= 2)
                                    @php $rsubkeg_2 = $subkeg->r_2; @endphp
                                        {{number_format($subkeg->r_2,2,',','.')}}
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if($triwulan >= 3)
                                    @php $rsubkeg_3 = $subkeg->r_3; @endphp
                                        {{number_format($subkeg->r_3,2,',','.')}}
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    @if($triwulan >= 4)
                                    @php $rsubkeg_4 = $subkeg->r_4; @endphp
                                        {{number_format($subkeg->r_4,2,',','.')}}
                                    @endif
                                </td>
                                <td></td>
                                <td>
                                    {{
                                        number_format($rsubkeg_1+$rsubkeg_2+$rsubkeg_3+$rsubkeg_4,2,',','.')
                                    }}
                                </td>
                                <td></td>
                                <td>
                                    @if($subkeg->apbd_kota != 0)
                                        {{ number_format(($rsubkeg_1+$rsubkeg_2+$rsubkeg_3+$rsubkeg_4)/$subkeg->apbd_kota*100,2,',','.') }}
                                    @endif
                                </td>
                                <td colspan="6"></td>  
                            </tr>
                                {{-- Indikator Sub Kegiatan --}}
                                @foreach($subkeg->indikator as $indsubkeg)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>{{$indsubkeg->indikator_subkegiatan}}</td>
                                    <td>{{$indsubkeg->satuan}}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        @php $targetIndSubKeg = 0; @endphp
                                        @if($triwulan < 4)
                                            @php $targetIndSubKeg = $indsubkeg->target; @endphp
                                            {{$indsubkeg->target}}
                                        @elseif($triwulan == 4)
                                            @php $targetIndSubKeg = $indsubkeg->target_perubahan; @endphp
                                            {{$indsubkeg->target_perubahan}}
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @php $r_isubkeg = array(0,0,0,0); @endphp
                                        @if(!is_null($indsubkeg->realisasiindikatorsubkeg) && $triwulan >= 1)
                                            {{$indsubkeg->realisasiindikatorsubkeg->t_1}}
                                            @php $r_isubkeg[0] = (float) $indsubkeg->realisasiindikatorsubkeg->t_1; @endphp
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @if(!is_null($indsubkeg->realisasiindikatorsubkeg) && $triwulan >= 2)
                                            {{$indsubkeg->realisasiindikatorsubkeg->t_2}}
                                            @php $r_isubkeg[1] = (float) $indsubkeg->realisasiindikatorsubkeg->t_2; @endphp
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @if(!is_null($indsubkeg->realisasiindikatorsubkeg) && $triwulan >= 3)
                                            {{$indsubkeg->realisasiindikatorsubkeg->t_3}}
                                            @php $r_isubkeg[2] = (float) $indsubkeg->realisasiindikatorsubkeg->t_3; @endphp
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @if(!is_null($indsubkeg->realisasiindikatorsubkeg) && $triwulan >= 4)
                                            {{$indsubkeg->realisasiindikatorsubkeg->t_4}}
                                            @php $r_isubkeg[3] = (float) $indsubkeg->realisasiindikatorsubkeg->t_4; @endphp
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @php
                                            $total_r_subkeg = 0;
                                            if($triwulan == 1)
                                            {
                                                $total_r_subkeg = $r_isubkeg[0];
                                            }else if($triwulan == 2)
                                            {
                                                $total_r_subkeg = ($r_isubkeg[0]+$r_isubkeg[1]);
                                            }else if($triwulan == 3)
                                            {
                                                $total_r_subkeg = ($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2]);
                                            }else if($triwulan == 4)
                                            {
                                                $total_r_subkeg = ($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2]+$r_isubkeg[3]);
                                            }                                            
                                        @endphp
                                        {{$total_r_subkeg}}
                                    </td>
                                    <td></td>
                                    <td>
                                        @if((float) $targetIndSubKeg != 0)
                                            @php
                                                $persenIndSubKeg = 0;
                                                if($indsubkeg->rumus == 1)
                                                {
                                                    // rumus triwulan
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndSubKeg = ($r_isubkeg[0]/(float)$targetIndSubKeg*100)/4;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1])/(float)$targetIndSubKeg*100)/4;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2])/(float)$targetIndSubKeg*100)/4;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2]+$r_isubkeg[3])/(float)$targetIndSubKeg*100)/4;
                                                    }
                                                }else if($indsubkeg->rumus == 2)
                                                {
                                                    // rumus semester
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndSubKeg = ($r_isubkeg[0]/(float)$targetIndSubKeg*100)/2;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1])/(float)$targetIndSubKeg*100)/2;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2])/(float)$targetIndSubKeg*100)/2;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndSubKeg = (($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2]+$r_isubkeg[3])/(float)$targetIndSubKeg*100)/2;
                                                    }
                                                }else if($indsubkeg->rumus == 3)
                                                {
                                                    // rumus tahun
                                                    if($triwulan == 1)
                                                    {
                                                        $persenIndSubKeg = $r_isubkeg[0]/(float)$targetIndSubKeg*100;
                                                    }else if($triwulan == 2)
                                                    {
                                                        $persenIndSubKeg = ($r_isubkeg[0]+$r_isubkeg[1])/(float)$targetIndSubKeg*100;
                                                    }else if($triwulan == 3)
                                                    {
                                                        $persenIndSubKeg = ($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2])/(float)$targetIndSubKeg*100;
                                                    }else if($triwulan == 4)
                                                    {
                                                        $persenIndSubKeg = ($r_isubkeg[0]+$r_isubkeg[1]+$r_isubkeg[2]+$r_isubkeg[3])/(float)$targetIndSubKeg*100;
                                                    }
                                                }                                             
                                            @endphp
                                            {{ number_format($persenIndSubKeg,2,',','.') }}
                                        @endif
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$nama_skpd}}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                    @endforeach
                {{-- Rata-rata Capaian Kinerja --}}
                <tr>
                    <td colspan="10" style="text-align: right;">Rata-rata capaian kinerja (%)</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="8"></td>
                </tr>
                {{-- Predikat Kinerja --}}
                <tr>
                    <td colspan="10" style="text-align: right;">Predikat kinerja (%)</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="8"></td>
                </tr>
                {{-- Faktor Pendorong Keberhasilan Kinerja --}}
                <tr>
                    <td colspan="28">Faktor pendorong keberhasilan kinerja:</td>
                </tr>
                {{-- Faktor Penghambat Pencapaian Kinerja --}}
                <tr>
                    <td colspan="28">Faktor penghambat pencapaian kinerja:</td>
                </tr>
                {{-- Tindak Lanjut yang Diperlukan Dalam Triwulan Berikutnya --}}
                <tr>
                    <td colspan="28">Tindak lanjut yang diperlukan dalam triwulan berikutnya:</td>
                </tr>
                {{-- Tindak Lanjut yang Diperlukan Dalam Renja Perangkat Daerah Kabupaten/Kota Berikutnya --}}
                <tr>
                    <td colspan="28">Tindak lanjut yang diperlukan dalam Renja Perangkat Daerah kabupaten/kota berikutnya:</td>
                </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>

    @php 
        date_default_timezone_set("Asia/Jakarta");
        $sekarang = Date("Y");
    @endphp
    <style>
        /* bordered table */
        .table2 {
            border-collapse: none;
            border-spacing: 0;
            width: %;
            border: none;
            float: right;
        }

        .table2 tr th,
        .table2 tr td {
            border: none;
            padding: 8px;
        }

        .table2 tr th{
            font-size: 1rem;
            text-align: center;            
        }
    </style>
    <table class="table2">
        <tr>
            <td style="text-align: center;">Disusun</td>
            <td></td>
            <td style="text-align: center;">Dievaluasi</td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: center;">........, ............. {{Session::get('tahun_monev')}}</td>
            <td></td>
            <td style="text-align: center;">........, ............. {{Session::get('tahun_monev')}}</td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: center;">KEPALA {{$nama_skpd}}</td>
            <td></td>
            <td style="text-align: center;">KEPALA BAPPELITBANGDA</td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: center;">KOTA CIMAHI</td>
            <td></td>
            <td style="text-align: center;">KOTA CIMAHI</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="text-align: center;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
            <td></td>
            <td style="text-align: center;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
            <td></td>
        </tr>
    </table>
</body>

</html>

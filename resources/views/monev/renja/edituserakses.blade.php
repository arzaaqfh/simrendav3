@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner ">
        <div class="container-fluid p-0">                
            <div class="white_card mb_30 ">
                <div class="white_card_header">
                    <div class="row">
                        <div class="col-3">
                            <h4>Setting User</h4>
                        </div>
                        <div class="col-9 text-right">
                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                        </div>
                    </div>
                </div>
                <div class="white_card_body">
                    <form action="{{ url('monev/renja/settinguser/save') }}" method="POST">
                        @csrf
                        <input type="hidden" name="linkpage" value="{{ url()->previous() }}">
                        <input type="hidden" name="id_user" value="{{ $user->id_user }}" />
                        <div class="row">
                            <div class="col-3">
                                <label>
                                    <h5><b>Nama Pengguna</b></h5>
                                </label>                                
                            </div>
                            <div class="col-9">
                                <input class="form-control" type="text" name="nama_pengguna" value="{{ $user->nama_pengguna }}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <label>
                                    <h5><b>NIP</b></h5>
                                </label>                                
                            </div>
                            <div class="col-9">
                                <input class="form-control" type="text" name="nip" value="{{ $user->nip }}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <label>
                                    <h5><b>Username</b></h5>
                                </label>  
                            </div>
                            <div class="col-9">
                                <input class="form-control" type="text" name="username" value="{{ $user->username }}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <label>
                                    <h5><b>Password</b></h5>
                                </label>  
                            </div>
                            <div class="col-9">
                                <input class="form-control" type="password" name="password"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <label>
                                    <h5><b>Role</b></h5>
                                </label>  
                            </div>
                            <div class="col-9">
                                <select class="form-control" name="role">
                                    @foreach ($roles as $role)
                                        <option @php if($role->id_role == $user->id_role){ echo 'selected'; }  @endphp value="{{ $role->id_role }}">{{ $role->nama_roles }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col-12">
                                <input type="checkbox" name="semua_perangkat_daerah" id="semua_perangkat_daerah" onchange="checkAllPD()">
                                <label for="semua_perangkat_daerah">Semua Perangkat Daerah</label><br>
                                <select id="perangkat_daerah" name="perangkat_daerah[]" class="form-select js-example-basic-single" data-placeholder="-" multiple>
                                    @php
                                        $selected = null;
                                        if(count($user->user_skpd)!=0)
                                        {
                                            foreach ($user->user_skpd as $pd)
                                            {
                                                $selected[] = $pd->skpd->first()->id_skpd;
                                            }
                                        }
                                    @endphp
                                    @foreach ($dskpd as $skpd)
                                        <option 
                                            @if (
                                                    !is_null($selected) && 
                                                    array_search($skpd->id_skpd,$selected)
                                                )
                                                selected
                                            @endif 
                                            value="{{$skpd->id_skpd}}"
                                        >
                                            {{$skpd->nama_skpd}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                        
                        <br/>
                        <div class="row">
                            <div class="col-12">
                                <input type="submit" value="Simpan" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2();

        });    
        function checkAllPD()
        {
            if ($('#semua_perangkat_daerah').is(':checked')) {
                $('#perangkat_daerah').prop('disabled', true);
            }else{
                $('#perangkat_daerah').prop('disabled', false);
            }
        }  
    </script>
    @include('layouts.footer')
@endsection
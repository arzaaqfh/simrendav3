@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')
    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <div class="row ">
                <div class="col-12">
                    <div class="white_card mb_30 ">
                        <div class="white_card_body">
                            <br/>
                            <div class="row">
                                <h4><b>Import Monev</b></h4>
                            </div>
                            <div class="row">
                                <div class="col col-md-12">
                                    <p class="alert alert-warning">
                                        File yang dipilih harus berformat <b>.XLS/.XLSX/.CSV</b>
                                    </p>                                    
                                    <form id="formImportFile" method="POST" enctype="multipart/form-data" action="{{url('/monev/renja/importFile')}}">
                                        {{ csrf_field() }}
                                        <input type='hidden' name='_token' value='{{ csrf_token() }}' /> {{-- token --}}
                                        <div class="row">
                                            <div class="col col-md-12">
                                                <label class="label" for="jenisData">
                                                    <b>Jenis Data</b>
                                                </label>
                                                <select class="form-select" name="jenisData" id="jenisData">
                                                    <option value="perubahan">Perubahan</option>
                                                    <option value="murni">Murni</option>
                                                </select>
                                            </div>
                                        </div><br/>
                                        <div class="row">
                                            <div class="col col-md-12">
                                                <label class="label" for="berkasImport">
                                                    <b>Pilih Berkas</b>
                                                </label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input custom-file-input1" id="berkasImport" name="berkasImport">
                                                    <label class="custom-file-label custom-file-label1" for="berkasImport">Choose file</label>
                                                </div>
                                            </div>
                                        </div><br/>
                                        <div class="row">
                                            <div class="col col-md-12">
                                                <center>
                                                    <input class="btn btn-primary" type="submit" value="UPLOAD"/>
                                                </center>
                                            </div>
                                        </div>
                                    </form>
                                    <script>
                                    // Add the following code if you want the name of the file appear on select
                                    $(".custom-file-input1").on("change", function() {
                                        var fileName = $(this).val().split("\\").pop();
                                        $(this).siblings(".custom-file-label1").addClass("selected").html(fileName);
                                    });
                                    </script>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Preview Monev -->
            <div class="row ">
                <div class="col-12">
                    <div class="white_card mb_30 ">
                        <div class="white_card_body">
                            <br/>
                            <div class="row">
                                <h4><b>Preview Monev</b></h4>
                            </div>
                            <div class="row">
                                <div class="col col-md-12">
                                    <p class="alert alert-warning">
                                        File yang dipilih harus berformat <b>.XLS/.XLSX/.CSV</b>
                                    </p>                                    
                                    <form id="formPreviewFile" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type='hidden' name='_token' value='{{ csrf_token() }}' /> {{-- token --}}
                                        <div class="row">
                                            <div class="col col-md-12">
                                                <label class="label" for="berkasPreview">
                                                    <b>Pilih Berkas</b>
                                                </label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input custom-file-input2" id="berkasPreview" name="berkasPreview">
                                                    <label class="custom-file-label custom-file-label2" for="berkasPreview">Choose file</label>
                                                </div>
                                            </div>
                                        </div><br/>
                                        <div class="row">
                                            <div class="col col-md-12">
                                                <center>
                                                    <p id="loadingUploadPreview">mengunggah...</p>
                                                    <input id="uploadPreview" class="btn btn-primary" type="submit" value="UPLOAD"/>
                                                    <a target="_blank" href="{{url('/monev/renja/downloadPreviewFile')}}" class="btn btn-secondary">
                                                        DOWNLOAD
                                                    </a>
                                                </center>
                                            </div>
                                        </div><br/>
                                        <div class="row" id="preview_0">
                                            <div class="col col-md-12">
                                                <p class="alert alert-danger">
                                                    <b>GAGAL unggah berkas!</b>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row" id="preview_1">
                                            <div class="col col-md-12">
                                                <p class="alert alert-success">
                                                    <b>BERHASIL unggah berkas!</b>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                    <script>
                                    // Add the following code if you want the name of the file appear on select
                                    $("#preview_0").hide();
                                    $("#preview_1").hide();
                                    $("#loadingUploadPreview").hide();
                                    $(".custom-file-input2").on("change", function() {
                                        var fileName = $(this).val().split("\\").pop();
                                        $(this).siblings(".custom-file-label2").addClass("selected").html(fileName);
                                    });
                                    $("#formPreviewFile").submit(function(e) {
                                        e.preventDefault();    
                                        var formData = new FormData(this);
                                        $("#uploadPreview").hide();
                                        $("#loadingUploadPreview").show();

                                        $.ajax({
                                            url: "{{url('/monev/renja/previewFile')}}",
                                            type: 'POST',
                                            data: formData,
                                            success: function (data) {
                                                if(data == 1)
                                                {
                                                    $("#preview_1").show();
                                                    $("#preview_0").hide();
                                                    $("#preview_1").delay(5000).fadeOut('slow');
                                                }else{
                                                    $("#preview_1").hide();
                                                    $("#preview_0").show();
                                                    $("#preview_0").delay(5000).fadeOut('slow');
                                                }
                                            },
                                            cache: false,
                                            contentType: false,
                                            processData: false
                                        });  
                                        $('#uploadPreview').show();
                                        $("#loadingUploadPreview").hide();
                                        return 0;
                                    });                                  
                                    </script>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('layouts.footer')
@endsection
<div class="row">
    <div class="col-lg-12">
        <h5>Analisis Kegiatan</h5>
            <div class="white_card mb_30 ">
            <div class="white_card_header p-0">
                <div class="bulder_tab_wrapper">
                    <ul class="nav" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="PD-tab" data-bs-toggle="tab" href="#PD" role="tab"
                                aria-controls="PD" aria-selected="true">Prangkat Daerah</a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="nav-link" id="Bidang-tab" data-bs-toggle="tab" href="#Bidang" role="tab"
                                aria-controls="Bidang" aria-selected="false">Bidang</a>
                        </li>-->
                        <li class="nav-item">
                            <a class="nav-link" id="JF-tab" data-bs-toggle="tab" href="#JF" role="tab"
                                aria-controls="JF" aria-selected="false">Bidang</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="white_card_body p-0">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="PD" role="tabpanel" aria-labelledby="PD-tab">
                        <div class="builder_select">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Faktor Pendorong</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $faktorPendorong_dana = '';
                                                    $faktorPendorong_sdm = '';
                                                    $faktorPendorong_waktu_pelaksanaan = '';
                                                    $faktorPendorong_peraturan_perundangan = '';
                                                    $faktorPendorong_sistem_pengadaan_barang_jasa = '';
                                                    $faktorPendorong_perijinan = '';
                                                    $faktorPendorong_ketersediaan_lahan = '';
                                                    $faktorPendorong_kesiapan_dukungan_masyarakat = '';
                                                    $faktorPendorong_faktor_alam = '';
                                                    $faktorPendorong_keterangan = '';

                                                    if(count($faktorPD)>0)
                                                    {
                                                        foreach ($faktorPD as $dataFPD) {
                                                            $faktorPendorong_dana = $dataFPD->dana;
                                                            $faktorPendorong_sdm = $dataFPD->sdm;
                                                            $faktorPendorong_waktu_pelaksanaan = $dataFPD->waktu_pelaksanaan;
                                                            $faktorPendorong_peraturan_perundangan = $dataFPD->peraturan_perundangan;
                                                            $faktorPendorong_sistem_pengadaan_barang_jasa = $dataFPD->sistem_pengadaan_barang_jasa;
                                                            $faktorPendorong_perijinan = $dataFPD->perijinan;
                                                            $faktorPendorong_ketersediaan_lahan = $dataFPD->ketersediaan_lahan;
                                                            $faktorPendorong_kesiapan_dukungan_masyarakat = $dataFPD->kesiapan_dukungan_masyarakat;
                                                            $faktorPendorong_faktor_alam = $dataFPD->faktor_alam;
                                                            $faktorPendorong_keterangan = $dataFPD->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaFP">Dana</label>
                                                        <input name="inputDanaFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_dana }}" >
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMFP">SDM</label>
                                                        <input name="inputSDMFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanFP">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganFP">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPendorong_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFP">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPendorong_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanFP">Perijinan</label>
                                                        <input name="inputPerijinanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanFP">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFP">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPendorong_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamFP">Faktor Alam</label>
                                                        <input name="inputFaktorAlamFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Faktor Penghambat</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $faktorPenghambat_dana = '';
                                                    $faktorPenghambat_sdm = '';
                                                    $faktorPenghambat_waktu_pelaksanaan = '';
                                                    $faktorPenghambat_peraturan_perundangan = '';
                                                    $faktorPenghambat_sistem_pengadaan_barang_jasa = '';
                                                    $faktorPenghambat_perijinan = '';
                                                    $faktorPenghambat_ketersediaan_lahan = '';
                                                    $faktorPenghambat_kesiapan_dukungan_masyarakat = '';
                                                    $faktorPenghambat_faktor_alam = '';
                                                    $faktorPenghambat_keterangan = '';

                                                    if(count($faktorPH)>0)
                                                    {
                                                        foreach ($faktorPH as $dataFPH) {
                                                            $faktorPenghambat_dana = $dataFPH->dana;
                                                            $faktorPenghambat_sdm = $dataFPH->sdm;
                                                            $faktorPenghambat_waktu_pelaksanaan = $dataFPH->waktu_pelaksanaan;
                                                            $faktorPenghambat_peraturan_perundangan = $dataFPH->peraturan_perundangan;
                                                            $faktorPenghambat_sistem_pengadaan_barang_jasa = $dataFPH->sistem_pengadaan_barang_jasa;
                                                            $faktorPenghambat_perijinan = $dataFPH->perijinan;
                                                            $faktorPenghambat_ketersediaan_lahan = $dataFPH->ketersediaan_lahan;
                                                            $faktorPenghambat_kesiapan_dukungan_masyarakat = $dataFPH->kesiapan_dukungan_masyarakat;
                                                            $faktorPenghambat_faktor_alam = $dataFPH->faktor_alam;
                                                            $faktorPenghambat_keterangan = $dataFPH->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaFPH">Dana</label>
                                                        <input name="inputDanaFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_dana }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMFPH">SDM</label>
                                                        <input name="inputSDMFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanFPH">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganFPH">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPenghambat_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFPH">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPenghambat_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanFPH">Perijinan</label>
                                                        <input name="inputPerijinanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanFPH">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPenghambat_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFPH">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $faktorPenghambat_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamFPH">Faktor Alam</label>
                                                        <input name="inputFaktorAlamFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Tindak Lanjut</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $tindakLanjut_dana = '';
                                                    $tindakLanjut_sdm = '';
                                                    $tindakLanjut_waktu_pelaksanaan = '';
                                                    $tindakLanjut_peraturan_perundangan = '';
                                                    $tindakLanjut_sistem_pengadaan_barang_jasa = '';
                                                    $tindakLanjut_perijinan = '';
                                                    $tindakLanjut_ketersediaan_lahan = '';
                                                    $tindakLanjut_kesiapan_dukungan_masyarakat = '';
                                                    $tindakLanjut_faktor_alam = '';
                                                    $tindakLanjut_keterangan = '';

                                                    if(count($tindakLJ)>0)
                                                    {
                                                        foreach ($tindakLJ as $dataTL) {
                                                            $tindakLanjut_dana = $dataTL->dana;
                                                            $tindakLanjut_sdm = $dataTL->sdm;
                                                            $tindakLanjut_waktu_pelaksanaan = $dataTL->waktu_pelaksanaan;
                                                            $tindakLanjut_peraturan_perundangan = $dataTL->peraturan_perundangan;
                                                            $tindakLanjut_sistem_pengadaan_barang_jasa = $dataTL->sistem_pengadaan_barang_jasa;
                                                            $tindakLanjut_perijinan = $dataTL->perijinan;
                                                            $tindakLanjut_ketersediaan_lahan = $dataTL->ketersediaan_lahan;
                                                            $tindakLanjut_kesiapan_dukungan_masyarakat = $dataTL->kesiapan_dukungan_masyarakat;
                                                            $tindakLanjut_faktor_alam = $dataTL->faktor_alam;
                                                            $tindakLanjut_keterangan = $dataTL->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaTL">Dana</label>
                                                        <input name="inputDanaTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_dana }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMTL">SDM</label>
                                                        <input name="inputSDMTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanTL">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganTL">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $tindakLanjut_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaTL">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $tindakLanjut_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanTL">Perijinan</label>
                                                        <input name="inputPerijinanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanTL">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatTL">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?> value="{{ $tindakLanjut_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamTL">Faktor Alam</label>
                                                        <input name="inputFaktorAlamTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 4)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Bidang" role="tabpanel" aria-labelledby="Bidang-tab">
                        <div class="builder_select">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Faktor Pendorong</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $faktorPendorong_dana = '';
                                                    $faktorPendorong_sdm = '';
                                                    $faktorPendorong_waktu_pelaksanaan = '';
                                                    $faktorPendorong_peraturan_perundangan = '';
                                                    $faktorPendorong_sistem_pengadaan_barang_jasa = '';
                                                    $faktorPendorong_perijinan = '';
                                                    $faktorPendorong_ketersediaan_lahan = '';
                                                    $faktorPendorong_kesiapan_dukungan_masyarakat = '';
                                                    $faktorPendorong_faktor_alam = '';
                                                    $faktorPendorong_keterangan = '';

                                                    if(count($faktorPD_B)>0)
                                                    {
                                                        foreach ($faktorPD_B as $dataFPD) {
                                                            $faktorPendorong_dana = $dataFPD->dana;
                                                            $faktorPendorong_sdm = $dataFPD->sdm;
                                                            $faktorPendorong_waktu_pelaksanaan = $dataFPD->waktu_pelaksanaan;
                                                            $faktorPendorong_peraturan_perundangan = $dataFPD->peraturan_perundangan;
                                                            $faktorPendorong_sistem_pengadaan_barang_jasa = $dataFPD->sistem_pengadaan_barang_jasa;
                                                            $faktorPendorong_perijinan = $dataFPD->perijinan;
                                                            $faktorPendorong_ketersediaan_lahan = $dataFPD->ketersediaan_lahan;
                                                            $faktorPendorong_kesiapan_dukungan_masyarakat = $dataFPD->kesiapan_dukungan_masyarakat;
                                                            $faktorPendorong_faktor_alam = $dataFPD->faktor_alam;
                                                            $faktorPendorong_keterangan = $dataFPD->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaFP">Dana</label>
                                                        <input name="inputDanaFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_dana }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMFP">SDM</label>
                                                        <input name="inputSDMFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanFP">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganFP">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPendorong_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFP">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPendorong_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanFP">Perijinan</label>
                                                        <input name="inputPerijinanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanFP">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFP">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatFP" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPendorong_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamFP">Faktor Alam</label>
                                                        <input name="inputFaktorAlamFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPendorong_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Faktor Penghambat</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $faktorPenghambat_dana = '';
                                                    $faktorPenghambat_sdm = '';
                                                    $faktorPenghambat_waktu_pelaksanaan = '';
                                                    $faktorPenghambat_peraturan_perundangan = '';
                                                    $faktorPenghambat_sistem_pengadaan_barang_jasa = '';
                                                    $faktorPenghambat_perijinan = '';
                                                    $faktorPenghambat_ketersediaan_lahan = '';
                                                    $faktorPenghambat_kesiapan_dukungan_masyarakat = '';
                                                    $faktorPenghambat_faktor_alam = '';
                                                    $faktorPenghambat_keterangan = '';

                                                    if(count($faktorPH_B)>0)
                                                    {
                                                        foreach ($faktorPH_B as $dataFPH) {
                                                            $faktorPenghambat_dana = $dataFPH->dana;
                                                            $faktorPenghambat_sdm = $dataFPH->sdm;
                                                            $faktorPenghambat_waktu_pelaksanaan = $dataFPH->waktu_pelaksanaan;
                                                            $faktorPenghambat_peraturan_perundangan = $dataFPH->peraturan_perundangan;
                                                            $faktorPenghambat_sistem_pengadaan_barang_jasa = $dataFPH->sistem_pengadaan_barang_jasa;
                                                            $faktorPenghambat_perijinan = $dataFPH->perijinan;
                                                            $faktorPenghambat_ketersediaan_lahan = $dataFPH->ketersediaan_lahan;
                                                            $faktorPenghambat_kesiapan_dukungan_masyarakat = $dataFPH->kesiapan_dukungan_masyarakat;
                                                            $faktorPenghambat_faktor_alam = $dataFPH->faktor_alam;
                                                            $faktorPenghambat_keterangan = $dataFPH->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaFPH">Dana</label>
                                                        <input name="inputDanaFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_dana }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMFPH">SDM</label>
                                                        <input name="inputSDMFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanFPH">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganFPH">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPenghambat_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFPH">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPenghambat_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanFPH">Perijinan</label>
                                                        <input name="inputPerijinanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanFPH">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPenghambat_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFPH">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatFPH" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $faktorPenghambat_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamFPH">Faktor Alam</label>
                                                        <input name="inputFaktorAlamFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $faktorPenghambat_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white_card card_height_100">
                                        <div class="white_card_header">
                                            <div class="box_header m-0">
                                                <div class="main-title">
                                                    <h3 class="m-0">Tindak Lanjut</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="white_card_body">
                                            <div class="card-body p-0">
                                                @php
                                                    $tindakLanjut_dana = '';
                                                    $tindakLanjut_sdm = '';
                                                    $tindakLanjut_waktu_pelaksanaan = '';
                                                    $tindakLanjut_peraturan_perundangan = '';
                                                    $tindakLanjut_sistem_pengadaan_barang_jasa = '';
                                                    $tindakLanjut_perijinan = '';
                                                    $tindakLanjut_ketersediaan_lahan = '';
                                                    $tindakLanjut_kesiapan_dukungan_masyarakat = '';
                                                    $tindakLanjut_faktor_alam = '';
                                                    $tindakLanjut_keterangan = '';

                                                    if(count($tindakLJ_B)>0)
                                                    {
                                                        foreach ($tindakLJ_B as $dataTL) {
                                                            $tindakLanjut_dana = $dataTL->dana;
                                                            $tindakLanjut_sdm = $dataTL->sdm;
                                                            $tindakLanjut_waktu_pelaksanaan = $dataTL->waktu_pelaksanaan;
                                                            $tindakLanjut_peraturan_perundangan = $dataTL->peraturan_perundangan;
                                                            $tindakLanjut_sistem_pengadaan_barang_jasa = $dataTL->sistem_pengadaan_barang_jasa;
                                                            $tindakLanjut_perijinan = $dataTL->perijinan;
                                                            $tindakLanjut_ketersediaan_lahan = $dataTL->ketersediaan_lahan;
                                                            $tindakLanjut_kesiapan_dukungan_masyarakat = $dataTL->kesiapan_dukungan_masyarakat;
                                                            $tindakLanjut_faktor_alam = $dataTL->faktor_alam;
                                                            $tindakLanjut_keterangan = $dataTL->keterangan;
                                                        }
                                                    }
                                                @endphp
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputDanaTL">Dana</label>
                                                        <input name="inputDanaTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_dana }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSDMTL">SDM</label>
                                                        <input name="inputSDMTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_sdm }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputWaktuPelaksanaanTL">Waktu Pelaksanaan</label>
                                                        <input name="inputWaktuPelaksanaanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_waktu_pelaksanaan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPeraturanPerundanganTL">Peraturan Perundangan</label>
                                                        <input name="inputPeraturanPerundanganTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $tindakLanjut_peraturan_perundangan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputSistemPengadaanBarangJasaTL">Sistem Pengadaan
                                                            Barang/Jasa</label>
                                                        <input name="inputSistemPengadaanBarangJasaTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $tindakLanjut_sistem_pengadaan_barang_jasa }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputPerijinanTL">Perijinan</label>
                                                        <input name="inputPerijinanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_perijinan }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKetersediaanLahanTL">Ketersediaan Lahan</label>
                                                        <input name="inputKetersediaanLahanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_ketersediaan_lahan }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatTL">Kesiapan/Dukungan
                                                            Masyarakat</label>
                                                        <input name="inputKesiapanDukunganMasyarakatTL" type="text" class="form-control" id=""
                                                            placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?> value="{{ $tindakLanjut_kesiapan_dukungan_masyarakat }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="form-label" for="inputFaktorAlamTL">Faktor Alam</label>
                                                        <input name="inputFaktorAlamTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_faktor_alam }}">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                        <input name="inputKeteranganTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 6)? '' : 'disabled'?>
                                                            value="{{ $tindakLanjut_keterangan }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="JF" role="tabpanel" aria-labelledby="JF-tab">
                        <div class="builder_select">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card_box position-relative">
                                        <div class="box_body p-2">
                                            <div class="default-according arrow_style" id="accordionoc">
                                            @php
                                                if(count($analisisJF)>0)
                                                {
                                                    foreach ($analisisJF as $data) {
                                                        @endphp
                                                        <div class="card mb-4"
                                                            style="box-shadow: 0 12px 30px rgb(48 50 108 / 14%);">
                                                            <div class="card-header">
                                                                <h5 class="mb-0">
                                                                    <button type="button" class="btn collapsed"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseicon{{$data->jafung->id_jafung}}"
                                                                        aria-expanded="false"
                                                                        aria-controls="collapse11"><i
                                                                            class="far fa-user-circle me-2"></i>
                                                                        {{ $data->jafung->nama }}</button>
                                                                </h5>
                                                            </div>
                                                            <div class="collapse" id="collapseicon{{$data->jafung->id_jafung}}"
                                                                aria-labelledby="collapseicon"
                                                                data-parent="#accordionoc">
                                                                <div class="card-body">
                                                                <!--
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="white_card card_height_100">
                                                                            <div class="white_card_header">
                                                                                <div class="box_header m-0">
                                                                                    <div class="main-title">
                                                                                        <h3 class="m-0">Faktor Pendorong</h3>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="white_card_body">
                                                                                <div class="card-body p-0">
                                                                                    @php
                                                                                        $faktorPendorong_dana = '';
                                                                                        $faktorPendorong_sdm = '';
                                                                                        $faktorPendorong_waktu_pelaksanaan = '';
                                                                                        $faktorPendorong_peraturan_perundangan = '';
                                                                                        $faktorPendorong_sistem_pengadaan_barang_jasa = '';
                                                                                        $faktorPendorong_perijinan = '';
                                                                                        $faktorPendorong_ketersediaan_lahan = '';
                                                                                        $faktorPendorong_kesiapan_dukungan_masyarakat = '';
                                                                                        $faktorPendorong_faktor_alam = '';
                                                                                        $faktorPendorong_keterangan = '';

                                                                                        if(isset($data->fpk))
                                                                                        {
                                                                                            foreach ($data->fpk as $dataFPD) {
                                                                                                $faktorPendorong_dana = $dataFPD->dana;
                                                                                                $faktorPendorong_sdm = $dataFPD->sdm;
                                                                                                $faktorPendorong_waktu_pelaksanaan = $dataFPD->waktu_pelaksanaan;
                                                                                                $faktorPendorong_peraturan_perundangan = $dataFPD->peraturan_perundangan;
                                                                                                $faktorPendorong_sistem_pengadaan_barang_jasa = $dataFPD->sistem_pengadaan_barang_jasa;
                                                                                                $faktorPendorong_perijinan = $dataFPD->perijinan;
                                                                                                $faktorPendorong_ketersediaan_lahan = $dataFPD->ketersediaan_lahan;
                                                                                                $faktorPendorong_kesiapan_dukungan_masyarakat = $dataFPD->kesiapan_dukungan_masyarakat;
                                                                                                $faktorPendorong_faktor_alam = $dataFPD->faktor_alam;
                                                                                                $faktorPendorong_keterangan = $dataFPD->keterangan;
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputDanaFP">Dana</label>
                                                                                            <input name="inputDanaFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_dana }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSDMFP">SDM</label>
                                                                                            <input name="inputSDMFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_sdm }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputWaktuPelaksanaanFP">Waktu Pelaksanaan</label>
                                                                                            <input name="inputWaktuPelaksanaanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_waktu_pelaksanaan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPeraturanPerundanganFP">Peraturan Perundangan</label>
                                                                                            <input name="inputPeraturanPerundanganFP" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPendorong_peraturan_perundangan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSistemPengadaanBarangJasaFP">Sistem Pengadaan
                                                                                                Barang/Jasa</label>
                                                                                            <input name="inputSistemPengadaanBarangJasaFP" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPendorong_sistem_pengadaan_barang_jasa }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPerijinanFP">Perijinan</label>
                                                                                            <input name="inputPerijinanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_perijinan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKetersediaanLahanFP">Ketersediaan Lahan</label>
                                                                                            <input name="inputKetersediaanLahanFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_ketersediaan_lahan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKesiapanDukunganMasyarakatFP">Kesiapan/Dukungan
                                                                                                Masyarakat</label>
                                                                                            <input name="inputKesiapanDukunganMasyarakatFP" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPendorong_kesiapan_dukungan_masyarakat }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputFaktorAlamFP">Faktor Alam</label>
                                                                                            <input name="inputFaktorAlamFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_faktor_alam }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-12">
                                                                                            <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                                                            <input name="inputKeteranganFP" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPendorong_keterangan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="white_card card_height_100">
                                                                            <div class="white_card_header">
                                                                                <div class="box_header m-0">
                                                                                    <div class="main-title">
                                                                                        <h3 class="m-0">Faktor Penghambat</h3>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="white_card_body">
                                                                                <div class="card-body p-0">
                                                                                    @php
                                                                                        $faktorPenghambat_dana = '';
                                                                                        $faktorPenghambat_sdm = '';
                                                                                        $faktorPenghambat_waktu_pelaksanaan = '';
                                                                                        $faktorPenghambat_peraturan_perundangan = '';
                                                                                        $faktorPenghambat_sistem_pengadaan_barang_jasa = '';
                                                                                        $faktorPenghambat_perijinan = '';
                                                                                        $faktorPenghambat_ketersediaan_lahan = '';
                                                                                        $faktorPenghambat_kesiapan_dukungan_masyarakat = '';
                                                                                        $faktorPenghambat_faktor_alam = '';
                                                                                        $faktorPenghambat_keterangan = '';

                                                                                        if(isset($data->fphk))
                                                                                        {
                                                                                            foreach ($data->fphk as $dataFPH) {
                                                                                                $faktorPenghambat_dana = $dataFPH->dana;
                                                                                                $faktorPenghambat_sdm = $dataFPH->sdm;
                                                                                                $faktorPenghambat_waktu_pelaksanaan = $dataFPH->waktu_pelaksanaan;
                                                                                                $faktorPenghambat_peraturan_perundangan = $dataFPH->peraturan_perundangan;
                                                                                                $faktorPenghambat_sistem_pengadaan_barang_jasa = $dataFPH->sistem_pengadaan_barang_jasa;
                                                                                                $faktorPenghambat_perijinan = $dataFPH->perijinan;
                                                                                                $faktorPenghambat_ketersediaan_lahan = $dataFPH->ketersediaan_lahan;
                                                                                                $faktorPenghambat_kesiapan_dukungan_masyarakat = $dataFPH->kesiapan_dukungan_masyarakat;
                                                                                                $faktorPenghambat_faktor_alam = $dataFPH->faktor_alam;
                                                                                                $faktorPenghambat_keterangan = $dataFPH->keterangan;
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputDanaFPH">Dana</label>
                                                                                            <input name="inputDanaFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_dana }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSDMFPH">SDM</label>
                                                                                            <input name="inputSDMFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_sdm }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputWaktuPelaksanaanFPH">Waktu Pelaksanaan</label>
                                                                                            <input name="inputWaktuPelaksanaanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_waktu_pelaksanaan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPeraturanPerundanganFPH">Peraturan Perundangan</label>
                                                                                            <input name="inputPeraturanPerundanganFPH" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPenghambat_peraturan_perundangan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSistemPengadaanBarangJasaFPH">Sistem Pengadaan
                                                                                                Barang/Jasa</label>
                                                                                            <input name="inputSistemPengadaanBarangJasaFPH" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPenghambat_sistem_pengadaan_barang_jasa }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPerijinanFPH">Perijinan</label>
                                                                                            <input name="inputPerijinanFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_perijinan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKetersediaanLahanFPH">Ketersediaan Lahan</label>
                                                                                            <input name="inputKetersediaanLahanFPH" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPenghambat_ketersediaan_lahan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKesiapanDukunganMasyarakatFPH">Kesiapan/Dukungan
                                                                                                Masyarakat</label>
                                                                                            <input name="inputKesiapanDukunganMasyarakatFPH" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $faktorPenghambat_kesiapan_dukungan_masyarakat }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputFaktorAlamFPH">Faktor Alam</label>
                                                                                            <input name="inputFaktorAlamFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_faktor_alam }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-12">
                                                                                            <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                                                            <input name="inputKeteranganFPH" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $faktorPenghambat_keterangan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                -->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="white_card card_height_100">
                                                                            <div class="white_card_header">
                                                                                <div class="box_header m-0">
                                                                                    <div class="main-title">
                                                                                        <h3 class="m-0">Tindak Lanjut</h3>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="white_card_body">
                                                                                <div class="card-body p-0">
                                                                                    @php
                                                                                        $tindakLanjut_dana = '';
                                                                                        $tindakLanjut_sdm = '';
                                                                                        $tindakLanjut_waktu_pelaksanaan = '';
                                                                                        $tindakLanjut_peraturan_perundangan = '';
                                                                                        $tindakLanjut_sistem_pengadaan_barang_jasa = '';
                                                                                        $tindakLanjut_perijinan = '';
                                                                                        $tindakLanjut_ketersediaan_lahan = '';
                                                                                        $tindakLanjut_kesiapan_dukungan_masyarakat = '';
                                                                                        $tindakLanjut_faktor_alam = '';
                                                                                        $tindakLanjut_keterangan = '';

                                                                                        if(isset($data->ftlk))
                                                                                        {
                                                                                            foreach ($data->ftlk as $dataTL) {
                                                                                                $tindakLanjut_dana = $dataTL->dana;
                                                                                                $tindakLanjut_sdm = $dataTL->sdm;
                                                                                                $tindakLanjut_waktu_pelaksanaan = $dataTL->waktu_pelaksanaan;
                                                                                                $tindakLanjut_peraturan_perundangan = $dataTL->peraturan_perundangan;
                                                                                                $tindakLanjut_sistem_pengadaan_barang_jasa = $dataTL->sistem_pengadaan_barang_jasa;
                                                                                                $tindakLanjut_perijinan = $dataTL->perijinan;
                                                                                                $tindakLanjut_ketersediaan_lahan = $dataTL->ketersediaan_lahan;
                                                                                                $tindakLanjut_kesiapan_dukungan_masyarakat = $dataTL->kesiapan_dukungan_masyarakat;
                                                                                                $tindakLanjut_faktor_alam = $dataTL->faktor_alam;
                                                                                                $tindakLanjut_keterangan = $dataTL->keterangan;
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputDanaTL">Dana</label>
                                                                                            <input name="inputDanaTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_dana }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSDMTL">SDM</label>
                                                                                            <input name="inputSDMTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_sdm }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputWaktuPelaksanaanTL">Waktu Pelaksanaan</label>
                                                                                            <input name="inputWaktuPelaksanaanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_waktu_pelaksanaan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPeraturanPerundanganTL">Peraturan Perundangan</label>
                                                                                            <input name="inputPeraturanPerundanganTL" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $tindakLanjut_peraturan_perundangan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputSistemPengadaanBarangJasaTL">Sistem Pengadaan
                                                                                                Barang/Jasa</label>
                                                                                            <input name="inputSistemPengadaanBarangJasaTL" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $tindakLanjut_sistem_pengadaan_barang_jasa }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputPerijinanTL">Perijinan</label>
                                                                                            <input name="inputPerijinanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_perijinan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKetersediaanLahanTL">Ketersediaan Lahan</label>
                                                                                            <input name="inputKetersediaanLahanTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_ketersediaan_lahan }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputKesiapanDukunganMasyarakatTL">Kesiapan/Dukungan
                                                                                                Masyarakat</label>
                                                                                            <input name="inputKesiapanDukunganMasyarakatTL" type="text" class="form-control" id=""
                                                                                                placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?> value="{{ $tindakLanjut_kesiapan_dukungan_masyarakat }}">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <label class="form-label" for="inputFaktorAlamTL">Faktor Alam</label>
                                                                                            <input name="inputFaktorAlamTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_faktor_alam }}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row mb-3">
                                                                                        <div class="col-md-12">
                                                                                            <label class="form-label" for="inputKeteranganFP">Keterangan</label>
                                                                                            <input name="inputKeteranganTL" type="text" class="form-control" id="" placeholder="" <?=(session()->get('id_role') == 13)? '' : 'disabled'?>
                                                                                                value="{{ $tindakLanjut_keterangan }}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @php
                                                    }
                                                }else
                                                {
                                            @endphp
                                                <div class="white_card card_height_100">
                                                    <div class="white_card_header">
                                                        <div class="box_header m-0">
                                                            <div class="main-title">
                                                                <h3 class="m-0 text-info">Belum ada catatan dari Jafung</h3>
                                                            </div>
                                                        </div>
                                                    </div>   
                                                </div> 
                                            @php
                                                }
                                            @endphp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
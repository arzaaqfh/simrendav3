@php
$num = 0;
if(array_key_exists('indikator', $data)){
if(count($data['indikator'])>0)
    {
    foreach ($data['indikator'] as $key => $value) {
        if(isset($isnew)){
            $num = $isnew+1;
        }else{
            $num = $key+1;
        }
        
        $is_confirm = null;
        $catatan_verifikasi = null;
        $status_verifikasi = null;
        $verified_by = null;
        
        if($value['verifikasi_indikatorsubkegiatan']){
            $is_confirm = $value['verifikasi_indikatorsubkegiatan']['is_confirm_1'];
            $catatan_verifikasi = $value['verifikasi_indikatorsubkegiatan']['catatan_1'];
            $status_verifikasi = $value['verifikasi_indikatorsubkegiatan']['status_verifikasi_1'];
            $verified_by = $value['verifikasi_indikatorsubkegiatan']['verified_by_1'];
        }

        $realisasi = 0;
        $kategori = 0;
        $persentase = 0;
        $total_persentase = 0;
        $total_persentase_perubahan = 0;
        $persen_1 = 0;
        $persen_2 = 0;
        $persen_3 = 0;
        $persen_4 = 0;
        $t1 = 0;
        $t2 = 0;
        $t3 = 0;
        $t4 = 0;

        if($value['realisasiindikatorsubkeg']){
            if($triwulan == 1){
                $realisasi = $value['realisasiindikatorsubkeg']['t_1'];
                $kategori = $value['realisasiindikatorsubkeg']['kategori_1'];
                $persentase = number_format((float)$value['realisasiindikatorsubkeg']['persen_1'],2,'.','');
            }else if($triwulan == 2){
                $realisasi = $value['realisasiindikatorsubkeg']['t_2'];
                $kategori = $value['realisasiindikatorsubkeg']['kategori_2'];
                $persentase = number_format((float)$value['realisasiindikatorsubkeg']['persen_2'],2,'.','');
            }else if($triwulan == 3){
                $realisasi = $value['realisasiindikatorsubkeg']['t_3'];
                $kategori = $value['realisasiindikatorsubkeg']['kategori_3'];
                $persentase = number_format((float)$value['realisasiindikatorsubkeg']['persen_3'],2,'.','');
            }else if($triwulan == 4){
                $realisasi = $value['realisasiindikatorsubkeg']['t_4'];
                $kategori = $value['realisasiindikatorsubkeg']['kategori_4'];
                $persentase = number_format((float)$value['realisasiindikatorsubkeg']['persen_4'],2,'.','');
                if($value['verifikasi_indikatorsubkegiatan']){
                    $is_confirm = $value['verifikasi_indikatorsubkegiatan']['is_confirm_2'];
                    $catatan_verifikasi = $value['verifikasi_indikatorsubkegiatan']['catatan_2'];
                    $status_verifikasi = $value['verifikasi_indikatorsubkegiatan']['status_verifikasi_2'];
                    $verified_by = $value['verifikasi_indikatorsubkegiatan']['verified_by_2'];
                }
            }

            $total_persentase = number_format((float)$value['realisasiindikatorsubkeg']['total_persen'],2,'.','');
            $total_persentase_perubahan = number_format((float)$value['realisasiindikatorsubkeg']['total_persen_perubahan'],2,'.','');
            

            $persen_1 = $value['realisasiindikatorsubkeg']['persen_1'];
            $persen_2 = $value['realisasiindikatorsubkeg']['persen_2'];
            $persen_3 = $value['realisasiindikatorsubkeg']['persen_3'];
            $persen_4 = $value['realisasiindikatorsubkeg']['persen_4'];

            $t1 = $value['realisasiindikatorsubkeg']['t_1'];
            $t2 = $value['realisasiindikatorsubkeg']['t_2'];
            $t3 = $value['realisasiindikatorsubkeg']['t_3'];
            $t4 = $value['realisasiindikatorsubkeg']['t_4'];
        }
    
        @endphp
        <div class="row">
            <div class="col-lg-12" id="col-wrap-ind{{$num}}">
                <div id="label-ind-{{$num}}"><h5>Indikator <b>#{{$num}}</b> @if($triwulan == 4 && session()->get('id_role') <= 4)  <button type="button" onclick="delind({{$num}})" class="btn btn-danger btn-sm" style="line-height: 0.1;"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button> @endif</h5></div>
                <div class="white_card mb_30" style="border: solid 4px #E2FFE2;">
                    <div class="white_card_body pt-4 pb-0">
                        <div class="row mb-3" id="indsubkeg_{{$num}}">
                            <input type="hidden" name="id_indikator_subkegiatan[]" id="id_indikator_subkegiatan_{{$num}}"
                                value="{{$value['id_indikator_subkegiatan']}}">
                            <div class="col-md-{{ ($triwulan != 4) ? 8 : 5; }} mb-3">
                                <label class="form-label" id="label_indsubkeg_{{$num}}">Nama Indikator</label><br>
                                @if(isset($isnew))
                                    <input type="text" class="form-control" name="nama_indikator[]" placeholder="Input nama indikator">
                                @else
                                    <label class="form-label"><b>{{$value['indikator_subkegiatan']}}</b></label>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" for="inputRealisasi">Target</label><br>
                                @if(isset($isnew))
                                    0
                                @else
                                    <label class="form-label"><b>{{$value['target']}}</b></label>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" for="#">Satuan</label><br>
                                @if(isset($isnew))
                                    <div class="common_select">
                                        <select class="nice_Select select-satuan wide mb_30" name="satuan[]" id="satuan{{$num}}">
                                            <option value="">Pilih Satuan...</option>
                                        </select>
                                    </div>
                                @else
                                    <label class="form-label"><b>{{$value['satuan']}}</b></label>
                                @endif
                            </div>
                            @if($triwulan == 4)
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Total Persentase Tw. 1-3</label><br>
                                <label class="form-label"><b>{{$total_persentase.' %'}}</b></label>
                            </div>
                            @endif
                            @if($triwulan < 4)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.1</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-1-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase('sub',<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal('sub',<?=$value['target']?>,<?=$num;?>,<?=$total_persentase;?>,1,<?=$persen_1;?>);" placeholder="" value="{{$t1}}" <?=$triwulan>1 || (session()->get('id_role') > 4) ? 'disabled' : '';?> >
                                </div>
                                @if($triwulan > 1)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.2</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-2-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase('sub',<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal('sub',<?=$value['target']?>,<?=$num;?>,<?=$total_persentase;?>,2,<?=$persen_2;?>);" placeholder="" value="{{$t2}}" <?=$triwulan>2 || (session()->get('id_role') > 4) ? 'disabled' : '';?> >
                                </div>
                                @endif
                                @if($triwulan > 2)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.3</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-3-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase('sub',<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal('sub',<?=$value['target']?>,<?=$num;?>,<?=$total_persentase;?>,3,<?=$persen_3;?>);" placeholder="" value="{{$t3}}" <?=$triwulan>3 || (session()->get('id_role') > 4) ? 'disabled' : '';?>>
                                </div>
                                @endif
                            @else
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Target Perubahan</label><br>
                                <input type="text" class="form-control" name="target_perubahan[]" id="target_perubahan-<?=$num;?>" 
                                    onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase('sub',value,($('#valuetw-4-<?=$num;?>').val()),<?=$num;?>);renderBarPersentaseTotal('sub',value,<?=$num;?>,<?=$total_persentase;?>,4);"  placeholder="" value="{{$value['target_perubahan']}}" <?=(session()->get('id_role') > 4) ? 'disabled' : '';?>>
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Realisasi Tw.4</label>
                                <input type="text" class="form-control" name="realisasi[]" id="valuetw-4-<?=$num;?>" 
                                    onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase('sub',($('#target_perubahan-<?=$num;?>').val()),value,<?=$num;?>);renderBarPersentaseTotal('sub',($('#target_perubahan-<?=$num;?>').val()),<?=$num;?>,<?=$total_persentase;?>,4);" placeholder="" value="{{$t4}}" <?=(session()->get('id_role') > 4) ? 'disabled' : '';?>>
                            </div>
                            @endif
                            <input type="hidden" name="persentase[]" id="persenbar_ind{{$num}}" value="{{$persentase}}">
                            <div class="col-md-2 d-grid">
                                <label class="form-label" >Persentase Tw.{{$triwulan}} <?= (Session::get('id_role') == 4 || Session::get('id_role') == 1) ? '<button type="button" onclick="editcapaian(&#39;sub&#39;,'.$num.',&#39;forminputrealisasisubkegModal&#39;)" class="btn btn-warning btn-sm" style="padding: 0.1rem 0.2rem;font-size: 0.7rem;line-height: 0;"><i class="fa fa-edit" aria-hidden="true"></i></button>' : '';?></label>
                                <div class="single_progressbar">
                                    <div id="bar_indsub{{$num}}" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" id="fillbar_indsub{{$num}}" data-percentage="{{$persentase}}"></span>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="total_persen[]" id="persenbar_ind_tot{{$num}}" value="<?= ($triwulan < 4) ? $total_persentase : $total_persentase_perubahan;?>">
                            <input type="hidden" name="isConstant[]" id="isConstant{{$num}}" value="<?=$value['is_constant']?>">
                            <div class="col-md-2 d-grid">
                                <label class="form-label" >Total Persentase <?= (Session::get('id_role') == 4 || Session::get('id_role') == 1) ? '<button type="button" onclick="edittotalcapaian('.$num.')" class="btn btn-warning btn-sm" style="padding: 0.1rem 0.2rem;font-size: 0.7rem;line-height: 0;"><i class="fa fa-edit" aria-hidden="true"></i></button>' : '';?></label>
                                <div class="single_progressbar">
                                    <h6 class="f_s_14 f_w_400" ></h6>
                                    <div id="bar_indsub_tot{{$num}}" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" id="fillbar_indsub_tot{{$num}}" data-percentage="<?= ($triwulan <= 4) ? $total_persentase : $total_persentase_perubahan;?>"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" >Status Verifikasi</label><br>
                                <input type="hidden" name="status_verifikasi[]" id="status_verifikasi{{$num}}" value="<?=$status_verifikasi?>">
                                <input type="hidden" name="catatan_verifikasi[]" id="catatan_verifikasi{{$num}}" value="<?=$catatan_verifikasi?>">
                                <input type="hidden" name="is_confirm[]" id="is_confirm{{$num}}" value="<?=$is_confirm?>">
                                <input type="hidden" id="verified_by{{$num}}" value="<?=$verified_by?>">
                                <a href="#" class="{{ $status_verifikasi == 1 ? 'badge_active' : ($status_verifikasi == 2 ? 'badge_active4' : 'badge_active3') }}" id="label_verifikasi{{$num}}">{{ $status_verifikasi == 1 ? 'sudah terverifikasi' : ($status_verifikasi == 2 ? 'menunggu konfirmasi perangkat daerah' : 'belum diverifikasi') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
    }
}
}
@endphp

<div class="row" id="last-wrap-sub" data-num="{{$num}}">
    <div class="col-lg-12">
        @if($triwulan == 4 && session()->get('id_role') <= 4)  
            <button type="button" onclick="addind()" class="btn btn-success btn-sm mb-3"><i class="fa fa-database" aria-hidden="true"></i> Tambah Indikator</button> 
        @endif
    </div>
</div>

<script>
    //$('.select-per-total').niceSelect();
    var totaldata = {{count($data);}};
    for(var i=1;i<=totaldata;i++){
        $('#bar_indsub'+i).barfiller();
        $('#bar_indsub_tot'+i).barfiller();
    }

    function addind(){
        var num = $('#last-wrap-sub').attr('data-num');
        var data = {
            'indikator': [
                {
                    id_indikator_subkegiatan: 0,
                    indikator_subkegiatan: "",
                    is_constant: 0,
                    realisasiindikatorsubkeg:{
                        persen_1: 0,
                        persen_2: 0,
                        persen_3: 0,
                        persen_4: 0,
                        kategori_1: 0,
                        kategori_2: 0,
                        kategori_3: 0,
                        kategori_4: 0,
                        t_1: "",
                        t_2: "",
                        t_3: "",
                        t_4: "",
                        total_persen: 0,
                        total_persen_perubahan: 0
                    },
                    verifikasi_indikatorsubkegiatan:{
                        catatan_1:null,
                        catatan_2:null,
                        is_confirm_1:null,
                        is_confirm_2:null,
                        status_verifikasi_1:null,
                        status_verifikasi_2:null,
                        verified_by_1:null,
                        verified_by_2:null
                    },
                    satuan: "",
                    target: 0,
                    target_perubahan :0
                }
            ]
        };
        
        $.ajax({
            type: 'POST',
            url: "{{ url('/monev/renja/murni/inputrealisasiindsubkegiatan') }}",
            data: {
                tw: 4,
                data_realisasi: data,
                isnew: num,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#last-wrap-sub").remove();
                $("#wrap-ind-subkeg").append(response);
                $("#satuan"+(Number(num)+1)).html(satuanoption);
                $("#satuan"+(Number(num)+1)).niceSelect();
            }
        });
    }

    function delind(el){
        if($('#id_indikator_subkegiatan_'+el).val() != 0){
            var arr = []
            if($('#indsubkeg_emptied').val() != ''){
              arr.push($('#indsubkeg_emptied').val())  
            }
            arr.push($('#id_indikator_subkegiatan_'+el).val())
            $('#indsubkeg_emptied').val(arr)
        }
        
        var lastnum = $("#last-wrap-sub").attr('data-num')
        $("#last-wrap-sub").attr('data-num',lastnum-1);
        $("#col-wrap-ind"+el).remove();

        $("div[id^='label-ind-']").each(function(i, obj) {
            var id = $(this).attr('id')
            var num = id.split('-')
            $(this).html('<h5>Indikator <b>#'+(i+1)+'</b> <button type="button" onclick="delind('+num[2]+')" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button></h5>') 
        });
        
    }
</script>
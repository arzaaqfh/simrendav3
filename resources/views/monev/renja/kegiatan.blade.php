<div class="col-lg-12">
    <div class="d-flex align-items-start">
        {{-- LIST KEGIATAN --}}
        <div class="col-lg-5 overflow-auto d-flex align-items-start" style="height: 500px;">
            <ul class="nav flex-column nav-pills me-3" id="v-pills-tab-keg" role="tablist" aria-orientation="vertical">
                @foreach ($keg as $no => $kg)
                <li class='nav-item' style="list-style-type: square;">
                    <a  class='nav-link' 
                        id='kegiatanitem{{$kg->id_kegiatan}}' 
                        data-bs-toggle='pill' 
                        href='#indkegiatanitem{{$kg->id_kegiatan}}'  
                        role='tab'  
                        aria-controls='kegiatanitem{{$kg->id_kegiatan}}'  
                        aria-selected='false' 
                    >
                    <i class=""></i>
                        <span>{{ $kg->nama_kegiatan }}</span>
                    </a>
                </li>
                @endforeach
            </ul>        
        </div>        
        {{-- LIST INDIKATOR KEGIATAN --}}
        <div class="col-lg-7 tab-content overflow-auto" style="height: 500px;">
            @foreach ($keg as $no => $kg)
            <div    class='tab-pane fade'    {{-- tab-pane --}}
                    id='indkegiatanitem{{$kg->id_kegiatan}}'
                    role='tabpanel'  
                    aria-labelledby='kegiatanitem{{$kg->id_kegiatan}}-tab' 
            >
                <form id='formIndKeg{{$kg->id_kegiatan}}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                    <div class='table-responsive'>{{-- (awal) div table --}}
                        <table id='formIndKegitem{{$kg->id_kegiatan}}' class='table table-borderless'>{{-- (awal) table --}}
                            <thead>
                                <tr>
                                    <th class='text-center fw-bold text-center mb-3'><h5>Daftar Indikator Kegiatan</h5></th>
                                </tr>
                            </thead>
                            @if (!is_null($indKeg[$kg->id_kegiatan]))
                                <tbody>                            
                                    @foreach ($indKeg[$kg->id_kegiatan] as $noInd => $indikator)
                                    <tr id="detailIndKeg_{{$indikator->id_indikator_kegiatan}}">
                                        <td>
                                            <h5 class='mb-0'>Indikator #{{($noInd+1)}}</h5>
                                            <div class='border border-primary shadow-sm p-3 mb-3'>
                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class='col-lg-6'> {{-- nama indikator kegiatan --}}
                                                        <h6>Nama Indikator</h6>
                                                        <div id='namaIndKeg_{{$indikator->id_indikator_kegiatan}}' class='fw-bold'>
                                                            {{$indikator->indikator_kegiatan}}
                                                        </div>
                                                        <input class="form-control editIndKG{{$indikator->id_indikator_kegiatan}}" type="text" name="indikator_kegiatan_{{$indikator->id_indikator_kegiatan}}" value="{{$indikator->indikator_kegiatan}}"/>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                        <h6>Rumus</h6>
                                                        <select class="form-control" name="rumus[]">
                                                            @foreach($rumus as $rms)
                                                                <option 
                                                                    @php
                                                                        if(!is_null($indikator->rumus) && $indikator->rumus == $rms->id_rumus)
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                    @endphp
                                                                    value="{{$rms->id_rumus}}"
                                                                >
                                                                    {{$rms->rumus}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- target indikator kegiatan --}}
                                                        <h6>Target</h6>
                                                        <div id='targetIndKeg_{{$indikator->id_indikator_kegiatan}}' class='fw-bold'>                                                            
                                                            <span class="target">
                                                                {{$indikator->target}}
                                                            </span>
                                                            <span class="target_perubahan">
                                                                {{$indikator->target_perubahan}}
                                                            </span>
                                                        </div>
                                                        <input class="form-control editIndKG{{$indikator->id_indikator_kegiatan}}" type="text" name="target_kegiatan_{{$indikator->id_indikator_kegiatan}}" value="{{$indikator->target}}"/>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- satuan indikator kegiatan --}}
                                                        <h6>Satuan</h6>
                                                        <div id='satuanIndKeg_{{$indikator->id_indikator_kegiatan}}' class='fw-bold'>
                                                            {{$indikator->satuan}}
                                                        </div>
                                                        <input class="form-control editIndKG{{$indikator->id_indikator_kegiatan}}" type="text" name="satuan_kegiatan_{{$indikator->id_indikator_kegiatan}}" value="{{$indikator->satuan}}"/>
                                                    </div>
                                                    <div class='col-lg-5'> {{-- progress --}}
                                                        <h6>Capaian Kinerja Kegiatan</h6>
                                                        @php
                                                            $persen_tw1 = 0;
                                                            $persen_tw2 = 0;
                                                            $persen_tw3 = 0;
                                                            $persen = 0;
                                                            $persen_perubahan = 0;
                                                            $target = floatval($indikator->target);
                                                            $target_perubahan = floatval($indikator->target_perubahan);
                                                            $t_1 = 0;
                                                            $t_2 = 0;
                                                            $t_3 = 0;
                                                            $t_4 = 0;
                                                            if(!is_null($indikator->realisasiindikatorkeg) && !is_null($indikator->realisasiindikatorkeg->t_1))
                                                            {
                                                                $t_1 = floatval($indikator->realisasiindikatorkeg->t_1);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorkeg) && !is_null($indikator->realisasiindikatorkeg->t_2))
                                                            {
                                                                $t_2 = floatval($indikator->realisasiindikatorkeg->t_2);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorkeg) && !is_null($indikator->realisasiindikatorkeg) && !is_null($indikator->realisasiindikatorkeg->t_3))
                                                            {
                                                                $t_3 = floatval($indikator->realisasiindikatorkeg->t_3);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorkeg) && !is_null($indikator->realisasiindikatorkeg->t_4))
                                                            {
                                                                $t_4 = floatval($indikator->realisasiindikatorkeg->t_4);
                                                            }

                                                            if(!is_null($indikator->realisasiindikatorkeg) && $target != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_tw1 = (($t_1)/$target*100)/4;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/4;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_tw1 = (($t_1)/$target*100)/2;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/2;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_tw1 = ($t_1)/$target*100;
                                                                    $persen_tw2 = ($t_1+$t_2)/$target*100;
                                                                    $persen_tw3 = ($t_1+$t_2+$t_3)/$target*100;
                                                                }
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorkeg) && $target_perubahan != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_perubahan = (($t_1+$t_2+$t_3+$t_4)/$target_perubahan*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_perubahan = ($t_4/$target_perubahan*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_perubahan = $t_4/$target_perubahan*100;
                                                                }
                                                            }
                                                        @endphp
                                                        <div class="capaian mt-3">
                                                            {{-- capaian triwulan 1 --}}
                                                            <div class="capaian-bar capaian-tw1">
                                                                <div
                                                                    id="capaianKeg-tw1-{{$indikator->id_indikator_kegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw1}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenKeg-tw1-{{$indikator->id_indikator_kegiatan}}">
                                                                            {{number_format($persen_tw1,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 2 --}}
                                                            <div class="capaian-bar capaian-tw2">
                                                                <div
                                                                    id="capaianKeg-tw2-{{$indikator->id_indikator_kegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw2}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenKeg-tw2-{{$indikator->id_indikator_kegiatan}}">
                                                                            {{number_format($persen_tw2,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 3 --}}
                                                            <div class="capaian-bar capaian-tw3">
                                                                <div
                                                                    id="capaianKeg-tw3-{{$indikator->id_indikator_kegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw3}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenKeg-tw3-{{$indikator->id_indikator_kegiatan}}">
                                                                            {{number_format($persen_tw3,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 4 --}}
                                                            <div class="capaian-bar capaian-tw4">
                                                                <div
                                                                    id="capaianKeg-tw4-{{$indikator->id_indikator_kegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_perubahan}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenKeg-tw4-{{$indikator->id_indikator_kegiatan}}">
                                                                            {{number_format($persen_perubahan,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-3'> {{-- teks tercapai atau belum tercapai --}}
                                                        <h6>Status</h6>
                                                        <div class='fw-bold'>
                                                            @php
                                                                $status_tw1 = '-';
                                                                $status_tw2 = '-';
                                                                $status_tw3 = '-';
                                                                $status_tw4 = '-';
                                                                if($indikator->rumus && ($indikator->rumus == 1 || $indikator->rumus == 6))
                                                                {
                                                                    // jika rumus positif
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }else if($indikator->rumus && ($indikator->rumus == 2 || $indikator->rumus == 7))
                                                                {
                                                                    // jika rumus negatif                                                                    
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }
                                                                if(ctype_alpha($indikator->target))
                                                                {
                                                                    $status_tw1 = '-';
                                                                    $status_tw2 = '-';
                                                                    $status_tw3 = '-';
                                                                    $status_tw4 = '-';
                                                                }
                                                            @endphp
                                                            <span id='badge-kegiatan-progress'
                                                                    class='badge bg-badge'
                                                            >
                                                                <b class="capaian-tw1" style="color:black; font-size: 14px">
                                                                    {{$status_tw1}}
                                                                </b>
                                                                <b class="capaian-tw2" style="color:black; font-size: 14px">
                                                                    {{$status_tw2}}
                                                                </b>
                                                                <b class="capaian-tw3" style="color:black; font-size: 14px">
                                                                    {{$status_tw3}}
                                                                </b>
                                                                <b class="capaian-tw4" style="color:black; font-size: 14px">
                                                                    {{$status_tw4}}
                                                                </b>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}

                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    @if(!is_null($indikator->realisasiindikatorkeg))
                                                        @for($i=1;$i<=4;$i++)
                                                        @php 
                                                            $tw = '';
                                                            $disabled = 'disabled';
                                                            
                                                            if($i==1) // set nilai realisasi sesuai triwulan
                                                            {
                                                                $tw = $indikator->realisasiindikatorkeg->t_1;
                                                            }else if($i==2)
                                                            {
                                                                $tw = $indikator->realisasiindikatorkeg->t_2;
                                                            }else if($i==3)
                                                            {
                                                                $tw = $indikator->realisasiindikatorkeg->t_3;
                                                            }else if($i==4)
                                                            {
                                                                $tw = $indikator->realisasiindikatorkeg->t_4;
                                                            }

                                                            if($i == Session::get('triwulan') || Auth::user()->id_role == 1 || Auth::user()->id_role == 13) // set disabled input realisasi
                                                            {
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                        <div class='col-md-3 realisasiTW{{$i}}'>
                                                            <div class='form-floating'>
                                                                <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                                <br/>
                                                                <label class="labelTriwulan">
                                                                    Triwulan {{$i}}
                                                                </label><br/> 
                                                                <input type='text' class='form-control' 
                                                                    id = 't_{{$i}}_keg_{{$indikator->id_indikator_kegiatan}}'
                                                                    name='t_{{$i}}[]' 
                                                                    placeholder='Triwulan {{$i}}' 
                                                                    value ='{{$tw}}' 
                                                                    {{$disabled}}
                                                                >
                                                            </div>
                                                        </div>
                                                        @endfor
                                                    @else
                                                    @for($i=1;$i<=4;$i++)
                                                    @php 
                                                        $tw = '';
                                                        $disabled = 'disabled';

                                                        if($i == Session::get('triwulan')) // set disabled input realisasi
                                                        {
                                                            $disabled = '';
                                                        }
                                                    @endphp
                                                    <div class='col-md-3 realisasiTW{{$i}}'>
                                                        <div class='form-floating'>
                                                            <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                            <br/>
                                                            <label class="labelTriwulan">
                                                                Triwulan {{$i}}
                                                            </label><br/> 
                                                            <input type='text' class='form-control' 
                                                                id = 't_{{$i}}_keg_{{$indikator->id_indikator_kegiatan}}'
                                                                name='t_{{$i}}[]' 
                                                                placeholder='Triwulan {{$i}}' 
                                                                value ='{{$tw}}' 
                                                                {{$disabled}}
                                                            >
                                                        </div>
                                                    </div>
                                                    @endfor
                                                    @endif
                                                    <input type='hidden' name='id_indikator_kegiatan[]' value='{{$indikator->id_indikator_kegiatan}}'/>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                                
                                                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                <div class='row g-3 mb-3 btnAksiKeg{{$kg->id_kegiatan}}'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class="text-right">                                         
                                                        {{-- tombol untuk edit indikator --}}
                                                        <button type="button" class="btn btn-primary" id="btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}">
                                                            Ubah
                                                        </button>                                           
                                                        <button type="button" class="btn btn-secondary btnAksiUbahIndKG_{{$indikator->id_indikator_kegiatan}}" id="btnBatalUbahIndKG_{{$indikator->id_indikator_kegiatan}}">
                                                            Batal
                                                        </button>                                              
                                                        <button type="button" class="btn btn-primary btnAksiUbahIndKG_{{$indikator->id_indikator_kegiatan}}" id="btnSimpanUbahIndKG_{{$indikator->id_indikator_kegiatan}}" onclick="ubahIndKeg({{$indikator->id_indikator_kegiatan}},{{$kg->id_kegiatan}},{{$kg->id_renja}})">
                                                            Simpan Perubahan
                                                        </button>

                                                        {{-- tombol untuk hapus indikator --}}
                                                        <button type="button" class="btn btn-danger" id="btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}">
                                                            Hapus
                                                        </button>
                                                        <button type="button" class="btn btn-secondary btnAksiHapusIndKG_{{$indikator->id_indikator_kegiatan}}" id="btnBatalHapusIndKG_{{$indikator->id_indikator_kegiatan}}">
                                                            Batal
                                                        </button>
                                                        <button type="button" class="btn btn-danger btnAksiHapusIndKG_{{$indikator->id_indikator_kegiatan}}" id="btnLakukanHapusIndKG_{{$indikator->id_indikator_kegiatan}}" onclick="hapusIndKeg({{$indikator->id_indikator_kegiatan}},{{$kg->id_kegiatan}})">
                                                            Hapus Indikator
                                                        </button>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                                @endif
                                                <script>
                                                    // setting awal
                                                    $('.editIndKG{{$indikator->id_indikator_kegiatan}}').hide();
                                                    $('.btnAksiUbahIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                    $('.btnAksiHapusIndKG_{{$indikator->id_indikator_kegiatan}}').hide();

                                                    // aksi tombol Ubah 
                                                    $('#btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}').on( "click", function() {
                                                        // yang tampil
                                                        $('.editIndKG{{$indikator->id_indikator_kegiatan}}').show();
                                                        $('.btnAksiUbahIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                        // yang ditutup
                                                        $('#btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                        $('#btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                    });
                                                    // aksi tombol Batal Ubah
                                                    $('#btnBatalUbahIndKG_{{$indikator->id_indikator_kegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.editIndKG{{$indikator->id_indikator_kegiatan}}').hide();
                                                        $('.btnAksiUbahIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                        // yang ditutup
                                                        $('#btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                        $('#btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                    });

                                                    // aksi tombol Hapus
                                                    $('#btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.btnAksiHapusIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                        // yang ditutup
                                                        $('#btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                        $('#btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                    });
                                                    // aksi tombol Batal Hapus
                                                    $('#btnBatalHapusIndKG_{{$indikator->id_indikator_kegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.btnAksiHapusIndKG_{{$indikator->id_indikator_kegiatan}}').hide();
                                                        // yang ditutup
                                                        $('#btnUbahIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                        $('#btnHapusIndKG_{{$indikator->id_indikator_kegiatan}}').show();
                                                    });
                                                </script>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>{{-- tombol submit form realisasinya --}}
                                    <tr>
                                        <td>                                              
                                            <div id="messageKegSukses{{$kg->id_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-success">
                                                    <b>Data berhasil disimpan !</b>
                                                </p>
                                            </div>     
                                            <div id="messageKegGagal{{$kg->id_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-danger">
                                                    <b>Data gagal disimpan !</b>
                                                </p>
                                            </div> 
                                            <div class='text-center' id='btnSimpanRIndKeg_{{$kg->id_kegiatan}}'>
                                            @php 
                                                date_default_timezone_set("Asia/Jakarta");
                                                $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                                $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                                $sekarang  = date("Y-m-d H:i:s");
                                            @endphp
                                            @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                                @if(count($indKeg[$kg->id_kegiatan])>0)
                                                    <button id="btnSimpanIndKeg{{$kg->id_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                        Simpan
                                                    </button>
                                                    <i class="fa fa-spinner" id="loadingKeg{{$kg->id_kegiatan}}"></i>                                                        
                                                @else
                                                    <p class="alert alert-info">
                                                        Indikator Tidak Tersedia
                                                    </p>
                                                @endif
                                            @else
                                                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                    @if(count($indKeg[$kg->id_kegiatan])>0)
                                                        <button id="btnSimpanIndKeg{{$kg->id_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                            Simpan
                                                        </button>
                                                        <i class="fa fa-spinner" id="loadingKeg{{$kg->id_kegiatan}}"></i>                                                        
                                                    @else
                                                        <p class="alert alert-info">
                                                            Indikator Tidak Tersedia
                                                        </p>
                                                    @endif
                                                @else
                                                    <p class="alert alert-warning">
                                                        <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                                    </p>
                                                @endif                                              
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            @endif
                        </table>{{-- (akhir) table --}}
                    </div>{{-- (akhir) div table --}}         
                    <script>
                        //Submit Form Realisasi Indikator Kegiatan
                        $("#messageKegSukses{{$kg->id_kegiatan}}").hide();
                        $("#messageKegGagal{{$kg->id_kegiatan}}").hide();
                        $("#loadingKeg{{$kg->id_kegiatan}}").hide();

                        $( "#formIndKeg{{$kg->id_kegiatan}}" ).on( "submit", function(e) {
                            var dataString = $(this).serialize();
                            $("#btnSimpanIndKeg{{$kg->id_kegiatan}}").hide();
                            $("#loadingKeg{{$kg->id_kegiatan}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('monev/renja/input/realisasi/IndKegiatan') }}",
                                data: dataString,
                                success: function (hasil) {
                                    var cek = true;
                                    $.each(hasil, function( index, value ) {
                                        if(value.status != 200)
                                        {
                                            cek = false;
                                        }
                                    });
                                    if(cek == true)
                                    {
                                        $("#messageKegSukses{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageKegSukses{{$kg->id_kegiatan}}").slideUp(500);
                                        });
                                        reloadIndKeg("{{$kg->id_kegiatan}}","simpan");
                                    }else{
                                        $("#messageKegGagal{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageKegGagal{{$kg->id_kegiatan}}").slideUp(500);
                                        });
                                    }
                                    $("#btnSimpanIndKeg{{$kg->id_kegiatan}}").show();
                                    $("#loadingKeg{{$kg->id_kegiatan}}").hide();
                                },
                                error: function () {
                                    $("#messageKegGagal{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageKegGagal{{$kg->id_kegiatan}}").slideUp(500);
                                    });
                                    $("#btnSimpanIndKeg{{$kg->id_kegiatan}}").show();
                                    $("#loadingKeg{{$kg->id_kegiatan}}").hide();

                                }
                            });
                            e.preventDefault();
                            updateStatKeg(); //update status semua input realisasi indikator
                        });
                    </script>
                </form>
                
                {{-- FORM TAMBAH INDIKATOR KEGIATAN --}}
                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                <form id="formAddIndKeg{{$kg->id_kegiatan}}">
                    <div class="container border border-success">
                        <br/>
                        <div class="row">
                            <div class="col">
                                <p class="alert alert-success">
                                    <b>Gunakan form ini untuk menambahkan indikator kegiatan yang tidak ada.</b>
                                </p>
                                <p class="alert alert-warning">
                                    <b>Refresh</b> halaman untuk melihat indikator yang sudah ditambah.
                                </p>
                            </div>
                        </div><br/>
                        <div class="row">
                            <h5>Tambah Indikator Kegiatan</h5>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}' /> {{-- token --}}
                            <input type="hidden" name="id_kegiatan" value="{{$kg->id_kegiatan}}"/> {{-- id kegiatan --}}
                            <input type="hidden" name="id_renja" value="{{$kg->id_renja}}"/> {{-- id renja --}}
                        </div><br/>
                        
                        {{-- Indikator Kegiatan --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Nama Indikator</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="indikator_kegiatan" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Target --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Target</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="target" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Satuan --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Satuan</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="satuan" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Submit Form --}}
                        <div class="row">
                            <div class="text-center">
                                {{-- Message Box --}}
                                <div id="messageAddIndKegSukses{{$kg->id_kegiatan}}" style="
                                    width: 250px;
                                    height: 100px;">
                                    <p class="alert alert-success">
                                        <b>Data berhasil disimpan !</b>
                                    </p>
                                </div>     
                                <div id="messageAddIndKegGagal{{$kg->id_kegiatan}}" style="
                                    width: 250px;
                                    height: 100px;">
                                    <p class="alert alert-danger">
                                        <b>Data gagal disimpan !</b>
                                    </p>
                                </div> 
                                {{-- Tombol Submit --}}
                                <button type="submit" class="btn btn-sm btn-success">
                                    Tambah
                                </button>
                                {{-- Loading --}}
                                <i class="fa fa-spinner" id="loadingAddIndKeg{{$kg->id_kegiatan}}"></i>
                            </div>
                        </div><br/>
                    </div>
                    <script>
                        //Submit Form Realisasi Indikator Sub Kegiatan
                        $("#messageAddIndKegSukses{{$kg->id_kegiatan}}").hide();
                        $("#messageAddIndKegGagal{{$kg->id_kegiatan}}").hide();
                        $("#loadingAddIndKeg{{$kg->id_kegiatan}}").hide();

                        $( "#formAddIndKeg{{$kg->id_kegiatan}}" ).on( "submit", function(e) {
                            var dataString = $(this).serialize();
                            $("#formAddIndKeg{{$kg->id_kegiatan}} Button").hide();
                            $("#loadingAddIndKeg{{$kg->id_kegiatan}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('monev/renja/input/add/IndKeg') }}",
                                data: dataString,
                                success: function (hasil) {
                                    var cek = true;
                                    $.each(hasil, function( index, value ) {
                                        if(value.status != 200)
                                        {
                                            cek = false;
                                        }
                                    });
                                    if(cek == true)
                                    {
                                        $("#messageAddIndKegSukses{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAddIndKegSukses{{$kg->id_kegiatan}}").slideUp(500);
                                        });
                                        reloadIndKeg("{{$kg->id_kegiatan}}","tambah");
                                    }else{
                                        $("#messageAddIndKegGagal{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAddIndKegGagal{{$kg->id_kegiatan}}").slideUp(500);
                                        });
                                    }
                                    $("#formAddIndKeg{{$kg->id_kegiatan}} Button").show();
                                    $("#loadingAddIndKeg{{$kg->id_kegiatan}}").hide();
                                },
                                error: function () {
                                    $("#messageAddIndKegGagal{{$kg->id_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageAddIndKegGagal{{$kg->id_kegiatan}}").slideUp(500);
                                    });
                                    $("#formAddIndKeg{{$kg->id_kegiatan}} Button").show();
                                    $("#loadingAddIndKeg{{$kg->id_kegiatan}}").hide();
                                }
                            });
                            e.preventDefault();
                        });
                    </script>
                </form>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    // set status input realisasi per kegiatan
    async function setStatInputRKeg(twl)
    {
        var listItems = $("#v-pills-tab-keg li");
        var cek = 0; //jumlah yang belum diisi
        var statInput = <?php echo json_encode($statKeg) ?>;
        var indexStat;
        listItems.each(function(idx, li) {
            var link = $(li).find('a');
            var icon = $(li).find('i');
            indexStat = link.attr('id').replace('kegiatanitem','');
            
            if(statInput[twl][indexStat] == 2)
            {
                link.attr('class','nav-link link-success');
                icon.attr('class','fa fa-check-circle');
            }else if(statInput[twl][indexStat] == 1)
            {
                link.attr('class','nav-link link-warning');
                icon.attr('class','fa fa-spinner');
            }else{
                link.attr('class','nav-link');
                icon.attr('class','');
            }
        });
    }
    // update status input realisasi untuk semua indikator kegiatan
    async function updateStatKeg()
    {
        var listItems = $("#v-pills-tab-keg li");
        var cek = 0; //jumlah yang belum diisi
        listItems.each(function(idx, li) {
            var product = $(li).find('a');
            if(product.attr('class')=='nav-link'||product.attr('class')=='nav-link link-warning'){
                cek = cek+1;
            }
        });
        if(cek == 0) //jika semua indikator sudah diisi
        {
            $('#kegiatan-tab').attr('class', 'nav-link text-success border-success');
            $('#kegiatan-tab i').attr('class', 'fa fa-check-circle');
        }else{
            $('#kegiatan-tab').attr('class', 'nav-link');
            $('#kegiatan-tab i').attr('class', '');
        }
    }

    //hapus indikator kegiatan
    async function hapusIndKeg(id_indikator_kegiatan,id_kegiatan)
    {
        var urlIndkeg = "{{url('/api/renja/post_delindikatorkeg')}}"; //alamat api realisasi indikator kegiatan
        var id_indikator_kegiatan = id_indikator_kegiatan;
        var token = '{{$token}}'; //token
        var triwulan = "{{Session::get('triwulan')}}";

        $.ajax({
            type        :   "POST",
            url         :   urlIndkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'id_indikator_kegiatan': id_indikator_kegiatan,
                                'triwulan': triwulan,
                            },
            success     :   function (responses) {
                                alert(responses.message);
                                $('#detailIndKeg_'+id_indikator_kegiatan).remove();
                                reloadIndKeg(id_kegiatan, 'hapus');
                            }
        });

        // yang tampil
        $('.btnAksiHapusIndKG_'+id_indikator_kegiatan).hide();
        // yang ditutup
        $('#btnUbahIndKG_'+id_indikator_kegiatan).show();
        $('#btnHapusIndKG_'+id_indikator_kegiatan).show();
    }
    //ubah indikator kegiatan
    async function ubahIndKeg(id_indikator_kegiatan,id_kegiatan,id_renja)
    {
        var urlIndkeg = "{{url('/api/renja/post_addindikatorkeg')}}"; //alamat api realisasi indikator kegiatan
        var id_indikator_kegiatan = id_indikator_kegiatan;
        var indikator_kegiatan = $("input[name=indikator_kegiatan_"+id_indikator_kegiatan).val();
        var target = $("input[name=target_kegiatan_"+id_indikator_kegiatan).val();
        var satuan = $("input[name=satuan_kegiatan_"+id_indikator_kegiatan).val();       
        var token = '{{$token}}'; //token
        var id_kegiatan = id_kegiatan;
        var id_renja = id_renja;
        var triwulan = "{{Session::get('triwulan')}}";
        $.ajax({
            type        :   "POST",
            url         :   urlIndkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'id_kegiatan': id_kegiatan,
                                'id_renja': id_renja,
                                'id_indikator_kegiatan': id_indikator_kegiatan,
                                'indikator_kegiatan': indikator_kegiatan,
                                'target': target,
                                'satuan': satuan,
                                'triwulan': triwulan
                            },
            success     :   function (responses) {
                                alert(responses.message);
                                reloadIndKeg(id_kegiatan,'ubah');
                            }
        });
        // yang tampil
        $('.editIndKG'+id_indikator_kegiatan).hide();
        $('.btnAksiUbahIndKG_'+id_indikator_kegiatan).hide();
        // yang ditutup
        $('#btnUbahIndKG_'+id_indikator_kegiatan).show();
        $('#btnHapusIndKG_'+id_indikator_kegiatan).show();
    }

    //memuat ulang semua data indikator kegiatan 
    async function reloadIndKeg(id_kegiatan,aksi)
    {
        var formInd = $("#formIndKeg"+id_kegiatan+" tbody");        
        var token = '{{$token}}'; //token
        var urlIndKeg = "{{url('/api/renja/get_inputrealindkeg')}}"; //alamat api realisasi indikator kegiatan
        var tahun = "{{session()->get('tahun_monev')}}"; //tahun monev
        var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
        var opd = '';
        var userRole = "{{ Auth::user()->id_role }}";        
        var listKeg = $("#l_Keg");
        var listIndKeg = $("#l_indKeg");
        var id_indikator_kegiatan;
        var indikator_kegiatan;
        var id_renja;
        var target;
        var target_perubahan;
        var t_1;
        var t_2;
        var t_3;
        var t_4;
        var index;
        var satuan;
        var jmlIndikator;

        if(userRole == 1)
        {
            id_skpd = "{{Session::get('pilih_id_skpd')}}";
        }else{
            id_skpd = "{{Auth::user()->viewuser->id_skpd}}";
        }
        
        $.ajax({
            type        :   "GET",
            url         :   urlIndKeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'triwulan':twl,
                                'tahun':tahun,
                                'opd':id_skpd,
                                'keg':id_kegiatan
                            },
            success     :   function (responses) {
                                var jmlTotalKegDiisi = 0;
                                var jmlKegiatan = responses.data.length;
                                $.each(responses.data, function (index1, item1) {
                                    jmlIndikator = item1.indikator.length; // tampung jumlah indikator
                                    var jmlRIndDiisi = 0;
                                    
                                    if(jmlIndikator == 0)//indikator kosong
                                    {
                                        $("#btnSimpanRIndKeg_"+id_kegiatan).html(
                                            "<p class='alert alert-info'>"+
                                            "    Indikator Tidak Tersedia"+
                                            "</p>"
                                        );
                                        
                                        $("#kegiatanitem"+item1.id_kegiatan).attr('class', 'nav-link');
                                        $("#kegiatanitem"+item1.id_kegiatan).html(
                                            "<i></i> "+
                                            "<span>"+item1.nama_kegiatan+"</span>"
                                        );
                                    }else{ //indikator ada
                                        $.each(item1.indikator, function (index2, item2) {
                                            if(item2.realisasiindikatorkeg != null )
                                            {
                                                if(item2.realisasiindikatorkeg.t_1 != null)
                                                {
                                                    t_1 = item2.realisasiindikatorkeg.t_1;
                                                    jmlRIndDiisi = jmlRIndDiisi + 1; //catat realisasi yang diisi
                                                }else{
                                                    t_1 = '';
                                                }
                                                if(item2.realisasiindikatorkeg.t_2 != null)
                                                {
                                                    t_2 = item2.realisasiindikatorkeg.t_2;
                                                }else{
                                                    t_2 = '';
                                                }
                                                if(item2.realisasiindikatorkeg.t_3 != null)
                                                {
                                                    t_3 = item2.realisasiindikatorkeg.t_3;
                                                }else{
                                                    t_3 = '';
                                                }
                                                if(item2.realisasiindikatorkeg.t_4 != null)
                                                {
                                                    t_4 = item2.realisasiindikatorkeg.t_4;
                                                }else{
                                                    t_4 = '';
                                                }
                                            }else{
                                                t_1 = '';
                                                t_2 = '';
                                                t_3 = '';
                                                t_4 = '';
                                            }
                                            
                                            if(item2.target_perubahan != null)
                                            {
                                                target = item2.target_perubahan;
                                            }else{
                                                target = item2.target;
                                            }

                                            $('#namaIndKeg_'+item2.id_indikator_kegiatan).html(item2.indikator_kegiatan);
                                            $('#targetIndKeg_'+item2.id_indikator_kegiatan+' .target').html(item2.target);
                                            $('#targetIndKeg_'+item2.id_indikator_kegiatan+' .target_perubahan').html(item2.target_perubahan);
                                            $('#satuanIndKeg_'+item2.id_indikator_kegiatan).html(item2.satuan);
                                            $('#t_1_keg_'+item2.id_indikator_kegiatan).val(t_1);
                                            $('#t_2_keg_'+item2.id_indikator_kegiatan).val(t_2);
                                            $('#t_3_keg_'+item2.id_indikator_kegiatan).val(t_3);
                                            $('#t_4_keg_'+item2.id_indikator_kegiatan).val(t_4);
                                            
                                            if(aksi == 'tambah')
                                            {
                                                index = index2;
                                                id_indikator_kegiatan = item2.id_indikator_kegiatan;
                                                indikator_kegiatan = item2.indikator_kegiatan;
                                                id_renja = item1.id_renja;
                                                target = item2.target;
                                                target_perubahan = item2.target_perubahan;
                                                satuan = item2.satuan;
                                            }
                                        });
                                        // setting ceklis status jumlah realisasi yang sudah di input
                                        if(jmlRIndDiisi == jmlIndikator)
                                        {
                                            $("#kegiatanitem"+item1.id_kegiatan).attr('class', 'nav-link link-success');
                                            $("#kegiatanitem"+item1.id_kegiatan).html(
                                                "<i class='fa fa-check-circle'></i> "+
                                                "<span>"+item1.nama_kegiatan+"</span>"
                                            );
                                            jmlTotalKegDiisi = jmlTotalKegDiisi + 1;
                                        }else if(jmlRIndDiisi > 0 && jmlRIndDiisi < jmlIndikator)
                                        {
                                            $("#kegiatanitem"+item1.id_kegiatan).attr('class', 'nav-link link-warning');
                                            $("#kegiatanitem"+item1.id_kegiatan).html(
                                                "<i class='fa fa-spinner'></i> "+
                                                "<span>"+item1.nama_kegiatan+"</span>"
                                            );
                                        }else{
                                            $("#kegiatanitem"+item1.id_kegiatan).attr('class', 'nav-link');
                                            $("#kegiatanitem"+item1.id_kegiatan).html(
                                                "<i></i> "+
                                                "<span>"+item1.nama_kegiatan+"</span>"
                                            );
                                        }
                                        if(aksi == 'tambah'){
                                            var persen_tw1 = 0;
                                            var persen_tw2 = 0;
                                            var persen_tw3 = 0;
                                            var persen_perubahan = 0;
                                            if(parseFloat(target) != 0)
                                            {
                                                persen_tw1 = parseFloat((t_1)/parseFloat(target)*100);
                                                persen_tw2 = parseFloat((t_1+t_2)/parseFloat(target)*100);
                                                persen_tw3 = parseFloat((t_1+t_2+t_3)/parseFloat(target)*100);
                                            }
                                            if(parseFloat(target_perubahan) != 0)
                                            {
                                                persen_perubahan = parseFloat((t_1+t_2+t_3+t_4)/parseFloat(target_perubahan)*100);
                                            }
                                            var disabled_tw1 = 'disabled';
                                            var disabled_tw2 = 'disabled';
                                            var disabled_tw3 = 'disabled';
                                            var disabled_tw4 = 'disabled';
                                            if(userRole == 1)
                                            {
                                                disabled_tw1 = '';
                                                disabled_tw2 = '';
                                                disabled_tw3 = '';
                                                disabled_tw4 = '';
                                            }
                                            if(twl == 1)
                                            {
                                                disabled_tw1 = '';
                                            }else if(twl == 2)
                                            {
                                                disabled_tw2 = '';
                                            }else if(twl == 3)
                                            {
                                                disabled_tw3 = '';
                                            }else if(twl == 4)
                                            {
                                                disabled_tw4 = '';
                                            }
                                            formInd.append(
                                                "<tr id='detailIndKeg_"+id_indikator_kegiatan+"'>"+
                                                    "    <td>"+
                                                    "        <h5 class='mb-0'>Indikator #"+(index+1)+"</h5>"+
                                                    "        <div class='border border-primary shadow-sm p-3 mb-3'>"+
                                                    "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                                    "                <div class='col-lg-12'>"+ // nama indikator kegiatan
                                                    "                    <h6>Nama Indikator</h6>"+
                                                    "                    <div class='fw-bold'>"+indikator_kegiatan+"</div>"+
                                                    "                    <input class='form-control editIndKG"+id_indikator_kegiatan+"' type='text' name='indikator_kegiatan_"+id_indikator_kegiatan+"' value='"+indikator_kegiatan+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // target indikator kegiatan
                                                    "                    <h6>Target</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span class='target'>"+String(target)+"</span>"+
                                                    "                        <span class='target_perubahan'>"+target_perubahan+"</span>"+
                                                    "                    </div>"+
                                                    "                    <input class='form-control editIndKG"+id_indikator_kegiatan+"' type='text' name='target_"+id_indikator_kegiatan+"' value='"+target+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // satuan indikator kegiatan
                                                    "                    <h6>Satuan</h6>"+
                                                    "                    <div class='fw-bold'>"+satuan+"</div>"+
                                                    "                    <input class='form-control editIndKG"+id_indikator_kegiatan+"' type='text' name='satuan_"+id_indikator_kegiatan+"' value='"+satuan+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-5'>"+ // progress
                                                    "                   <h6>Capaian Kinerja Sub Kegiatan</h6>"+                                                        
                                                    "                   <div class='capaian mt-3'>"+
                                                    "                       <div class='capaian-bar capaian-tw1'>"+
                                                    "                           <div"+
                                                    "                               id='capaianKeg-tw1-"+id_indikator_kegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw1+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b  id='capaianPersenKeg-tw1-"+id_indikator_kegiatan+"'>"+
                                                                                            persen_tw1.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw2'>"+
                                                    "                           <div"+
                                                    "                               id='capaianKeg-tw2-"+id_indikator_kegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw2+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b  id='capaianPersenKeg-tw2-"+id_indikator_kegiatan+"'>"+
                                                                                            persen_tw2.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw3'>"+
                                                    "                           <div"+
                                                    "                               id='capaianKeg-tw3-"+id_indikator_kegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw3+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b  id='capaianPersenKeg-tw1-"+id_indikator_kegiatan+"'>"+
                                                                                            persen_tw3.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw4'>"+
                                                    "                           <div"+
                                                    "                               id='capaianKeg-tw4-"+id_indikator_kegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_perubahan+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b  id='capaianPersenKeg-tw1-"+id_indikator_kegiatan+"'>"+
                                                                                            persen_perubahan.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                   </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-3'>"+ // teks tercapai atau belum tercapai
                                                    "                    <h6>Status</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span id='badge-kegiatan-progress'"+
                                                    "                                class='badge bg-badge'"+
                                                    "                        >"+
                                                    "                        -"+
                                                    "                        </span>"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                                    "                    <input type='hidden' name='id_indikator_kegiatan[]' value='"+id_indikator_kegiatan+"'/>"+
                                                    "                <div class='col-md-3 realisasiTW1'>"+ // realisasi triwulan 1
                                                    "                    <div class='form-floating'>"+
                                                    "                       <input type='hidden' name='triwulan1[]' value='1'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 1"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_1"+id_indikator_kegiatan+"' name='t_1[]' placeholder='Triwulan 1' value='"+t_1+"'"+disabled_tw1+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW2' style='display: none;'>"+ // realisasi triwulan 2
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan2[]' value='2'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 2"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_2"+id_indikator_kegiatan+"' name='t_2[]' placeholder='Triwulan 2' value='"+t_2+"'"+disabled_tw2+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW3' style='display: none;'>"+ // realisasi triwulan 3
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan3[]' value='3'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 3"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_3"+id_indikator_kegiatan+"' name='t_3[]' placeholder='Triwulan 3' value='"+t_3+"'"+disabled_tw3+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW4' style='display: none;'>"+ // realisasi triwulan 4
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan4[]' value='4'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 4"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_4"+id_indikator_kegiatan+"' name='t_4[]' placeholder='Triwulan 4' value='"+t_4+"'"+disabled_tw4+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "            <div class='row g-3 mb-3 btnAksiKeg"+id_kegiatan+"'>"+ // (awal) row g-3 mb-3
                                                    "                <div class='text-right'>"+                                       
                                                                        // tombol untuk edit indikator
                                                    "                    <button type='button' class='btn btn-sm btn-primary' id='btnUbahIndKG_"+id_indikator_kegiatan+"'>"+
                                                    "                        Ubah"+
                                                    "                    </button>"+                                         
                                                    "                    <button type='button' class='btn btn-sm btn-secondary btnAksiUbahIndKG_"+id_indikator_kegiatan+"' id='btnBatalUbahIndKG_"+id_indikator_kegiatan+"'>"+
                                                    "                        Batal"+
                                                    "                    </button>"+                                          
                                                    "                    <button type='button' class='btn btn-sm btn-primary btnAksiUbahIndKG_"+id_indikator_kegiatan+"' onclick='ubahIndKeg("+id_indikator_kegiatan+","+id_kegiatan+","+id_renja+")'>"+
                                                    "                        Simpan Perubahan"+
                                                    "                    </button>"+
                                                                        // tombol untuk hapus indikator
                                                    "                    <button type='button' class='btn btn-sm btn-danger' id='btnHapusIndKG_"+id_indikator_kegiatan+"'>"+
                                                    "                        Hapus"+
                                                    "                    </button>"+
                                                    "                    <button type='button' class='btn btn-sm btn-secondary btnAksiHapusIndKG_"+id_indikator_kegiatan+"' id='btnBatalHapusIndKG_"+id_indikator_kegiatan+"'>"+
                                                    "                        Batal"+
                                                    "                    </button>"+
                                                    "                    <button type='button' class='btn btn-sm btn-danger btnAksiHapusIndKG_"+id_indikator_kegiatan+"' onclick='hapusIndKeg("+id_indikator_kegiatan+","+id_kegiatan+")'>"+
                                                    "                        Hapus Indikator"+
                                                    "                    </button>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "       </div>"+
                                                    "    </td>"+
                                                    "</tr>"
                                            );
                                            
                                            var bwi_mulai = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['mulai']))}}";
                                            var bwi_akhir = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['akhir']))}}";
                                            var sekarang = "{{date('Y-m-d H:i:s')}}";
                                            var user_id_role = "{{Auth::user()->id_role}}";
                                            if(sekarang >= bwi_mulai && sekarang <= bwi_akhir){
                                                if(user_id_role != 13){
                                                    $("#btnSimpanRIndKeg_"+id_kegiatan).html(
                                                        "<button id='btnSimpanIndKeg"+id_kegiatan+"' type='submit' class='btn btn-primary btn-sm'>"+
                                                        "    Simpan"+
                                                        "</button>"+
                                                        "<i class='fa fa-spinner' id='loadingKeg"+id_kegiatan+"'></i>"
                                                    );
                                                }
                                            }else{
                                                if(user_id_role == 1 || user_id_role == 13){
                                                    $("#btnSimpanRIndKeg_"+id_kegiatan).html(
                                                        "<button id='btnSimpanIndKeg"+id_kegiatan+"' type='submit' class='btn btn-primary btn-sm'>"+
                                                        "    Simpan"+
                                                        "</button>"+
                                                        "<i class='fa fa-spinner' id='loadingKeg"+id_kegiatan+"'></i>"
                                                    );
                                                }else{
                                                    $("#btnSimpanRIndKeg_"+id_kegiatan).html(
                                                        "<p class='alert alert-warning'>"+
                                                        "    <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>"+
                                                        "</p>"
                                                    );
                                                }
                                            }
                                            
                                            //Untuk ubah indikator kegiatan
                                            $('.editIndKG'+id_indikator_kegiatan).hide();
                                            $('.btnAksiUbahIndKG_'+id_indikator_kegiatan).hide();
                                            $('.btnAksiHapusIndKG_'+id_indikator_kegiatan).hide();
                                            //Submit Form Realisasi Indikator Kegiatan
                                            $("#messageKegSukses"+id_kegiatan).hide();
                                            $("#messageKegGagal"+id_kegiatan).hide();
                                            $("#loadingKeg"+id_kegiatan).hide();
                                            //Submit Form Tambah Indikator Kegiatan
                                            $("#messageAddIndKegSukses"+id_kegiatan).hide();
                                            $("#messageAddIndKegGagal"+id_kegiatan).hide();
                                            $("#loadingAddIndKeg"+id_kegiatan).hide();
                                            
                                            showHide(twl);
                                            // aksi tombol Ubah 
                                            $('#btnUbahIndKG_'+id_indikator_kegiatan).on( "click", function() {
                                                // yang tampil
                                                $('.editIndKG'+id_indikator_kegiatan).show();
                                                $('.btnAksiUbahIndKG_'+id_indikator_kegiatan).show();
                                                // yang ditutup
                                                $('#btnUbahIndKG_'+id_indikator_kegiatan).hide();
                                                $('#btnHapusIndKG_'+id_indikator_kegiatan).hide();
                                            });
                                            // aksi tombol Batal Ubah
                                            $('#btnBatalUbahIndKG_'+id_indikator_kegiatan).on("click", function () {
                                                // yang tampil
                                                $('.editIndKG'+id_indikator_kegiatan).hide();
                                                $('.btnAksiUbahIndKG_'+id_indikator_kegiatan).hide();
                                                // yang ditutup
                                                $('#btnUbahIndKG_'+id_indikator_kegiatan).show();
                                                $('#btnHapusIndKG_'+id_indikator_kegiatan).show();
                                            });
                                            // aksi tombol Hapus
                                            $('#btnHapusIndKG_'+id_indikator_kegiatan).on("click", function () {
                                                // yang tampil
                                                $('.btnAksiHapusIndKG_'+id_indikator_kegiatan).show();
                                                // yang ditutup
                                                $('#btnUbahIndKG_'+id_indikator_kegiatan).hide();
                                                $('#btnHapusIndKG_'+id_indikator_kegiatan).hide();
                                            });
                                            // aksi tombol Batal Hapus
                                            $('#btnBatalHapusIndKG_'+id_indikator_kegiatan).on("click", function () {
                                                // yang tampil
                                                $('.btnAksiHapusIndKG_'+id_indikator_kegiatan).hide();
                                                // yang ditutup
                                                $('#btnUbahIndKG_'+id_indikator_kegiatan).show();
                                                $('#btnHapusIndKG_'+id_indikator_kegiatan).show();
                                            });
                                        }
                                        updateStatKeg();  //update status semua input realisasi indikator
                                    }
                                });
                            }
        });
    }
</script>
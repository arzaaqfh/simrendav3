@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>
        
        <div class="main_content_iner ">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-12">
                        <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                            <div class="page_title_left">
                                <h3 class="f_s_25 f_w_700 dark_text" >Monev Hasil Renja</h3>
                                <ol class="breadcrumb page_bradcam mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/monev') }}">Monev</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/monev/renja') }}">Renja</a></li>
                                    <li class="breadcrumb-item active">Murni</li>
                                </ol>
                            </div>
                            <div class="page_title_right">
                                <div class="dropdown common_bootstrap_button ">
                                    <span class="dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">Cetak</span>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(1)"> Triwulan 1</a>
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(2)"> Triwulan 2</a>
                                        <a class="dropdown-item f_s_16 f_w_600" href="#" onclick="openformcetak(3)"> Triwulan 3</a>
                                    </div>
                                </div>
                                @if(session()->get('id_role') == 1)
                                    <button type="button" class="btn btn-info ml-2 text-white" onclick="openmodaladvcetak()">Cetak Lainnya</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12">
                        <div class="white_card mb_30 ">
                            <div class="white_card_header">
                                <div class="bulder_tab_wrapper">
                                    <ul class="nav" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="kegiatan-tab" data-bs-toggle="tab" href="#Kegiatan" role="tab" aria-controls="Kegiatan" aria-selected="true">Kegiatan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="subkegiatan-tab" data-bs-toggle="tab" href="#Subkegiatan" role="tab" aria-controls="Subkegiatan" aria-selected="false">Sub Kegiatan</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="white_card_body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="Kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row mb-4">
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_program_keg" onchange="related_filter('keg', 'f_program_keg', ['f_kegiatan_keg','f_pd_keg'])">
                                                            <option value="">Filter Program...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_kegiatan_keg" onchange="related_filter('keg', 'f_kegiatan_keg', ['f_program_keg','f_pd_keg'])">
                                                            <option value="">Filter Kegiatan...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @if (Session::get('id_role') != 4)
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_pd_keg" onchange="related_filter('keg', 'f_pd_keg', ['f_program_keg','f_kegiatan_keg'])">
                                                            <option value="">Filter Perangkat Daerah..</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_st_input_keg" onchange="filter_table('keg');">
                                                            <option value="">Semua Status Input</option>
                                                            <option value="si-tw1">Selesai Input - Tw. 1</option>
                                                            <option value="si-tw2">Selesai Input - Tw. 2</option>
                                                            <option value="si-tw3">Selesai Input - Tw. 3</option>
                                                            <option value="bsi-tw1">Belum Selesai Input - Tw. 1</option>
                                                            <option value="bsi-tw2">Belum Selesai Input - Tw. 2</option>
                                                            <option value="bsi-tw3">Belum Selesai Input - Tw. 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="QA_section">
                                                        <div class="QA_table mb_30">
                                                            <table class="table multiplegroup" id="table_kegiatan">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col" class="text-center">Kegiatan</th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Subkegiatan" role="tabpanel" aria-labelledby="subkegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row mb-4">
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_program_subkeg" onchange="filter_table('subkeg');">
                                                            <option value="">Filter Program...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_kegiatan_subkeg" onchange="filter_table('subkeg');">
                                                            <option value="">Filter Kegiatan...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_subkegiatan_subkeg" onchange="filter_table('subkeg');">
                                                            <option value="">Filter Sub Kegiatan...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @php
                                                if(Session::get('id_role') != 4){
                                                @endphp
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_pd_subkeg" onchange="filter_table('subkeg');">
                                                            <option value="">Filter Perangkat Daerah..</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @php
                                                }
                                                @endphp
                                                <div class="col-lg-3">
                                                    <div class="common_select">
                                                        <select class="nice_Select wide" id="f_st_input_subkeg" onchange="filter_table('subkeg');">
                                                            <option value="">Semua Status Input</option>
                                                            <option value="si-tw1">Selesai Input - Tw. 1</option>
                                                            <option value="si-tw2">Selesai Input - Tw. 2</option>
                                                            <option value="si-tw3">Selesai Input - Tw. 3</option>
                                                            <option value="bsi-tw1">Belum Selesai Input - Tw. 1</option>
                                                            <option value="bsi-tw2">Belum Selesai Input - Tw. 2</option>
                                                            <option value="bsi-tw3">Belum Selesai Input - Tw. 3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="QA_section">
                                                        <div class="QA_table mb_30">
                                                            <table class="table multiplegroup" id="table_subkegiatan">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col" class="text-center">Sub Kegiatan</th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" id="forminputrealisasikegModal">
            <form id="formrealisasikegiatan">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle">Input Realisasi Indikator Kegiatan</h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <!--<button type="button" class="btn btn-primary" onclick="saverealisasikegiatan()" id="btn-simpan-keg">Simpan</button>-->
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn-close float-right" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h5>Program</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_program">Nama Program</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <h5>Kegiatan</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_kegiatan">Nama Kegiatan</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <h5>Perangkat Daerah</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_pd">Nama Perangkat Daerah</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <h5>Triwulan</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="triwulan_keg">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="anggaran">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Realisasi Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="r_anggaran">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card_body" style="padding: 10px 30px;">
                                    <div class="white_card_body">
                                            <input type="hidden" name="id_pd" id="id_pd">
                                            <input type="hidden" name="id_kegiatan" id="id_kegiatan">
                                            <div id="wrap-ind-keg"></div>
                                            <div id="wrap-desk-keg"></div>
                                            <div class="white_card mb_30">
                                                <div class="white_card_body">
                                                    <div class="row mb-3">
                                                        <div class="col-md-12">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" id="forminputrealisasisubkegModal">
            <form id="formrealisasisubkegiatan">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle">Input Realisasi Indikator Sub Kegiatan</h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <!--<button type="button" class="btn btn-primary" onclick="saverealisasisubkegiatan()" id="btn-simpan-subkeg">Simpan</button>-->
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn-close float-right" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h5>Program</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_program_sub">Nama Program</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <h5>Kegiatan</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_kegiatan_sub">Nama Kegiatan</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <h5>Sub Kegiatan</h5>
                                                <div class="white_card badge_active4 mb_10" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_subkegiatan">Nama Sub Kegiatan</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <h5>Perangkat Daerah</h5>
                                                <div class="white_card badge_active4" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="nama_pd_sub">Nama Perangkat Daerah</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <h5>Triwulan</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="triwulan_subkeg">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="anggaran_subkeg">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <h5>Realisasi Anggaran</h5>
                                                <div class="white_card badge_active" style="width: -webkit-fill-available;">
                                                    <div class="white_card_header QA_section p-2">
                                                        <div class="box_header m-0">
                                                            <h6 id="r_anggaran_subkeg">0</h6> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card_body" style="padding: 10px 30px;">
                                    <div class="white_card_body">
                                            <input type="hidden" name="id_pd_subkegiatan" id="id_pd_subkegiatan">
                                            <input type="hidden" name="id_subkegiatan" id="id_subkegiatan">
                                            <div id="wrap-ind-subkeg"></div>
                                            <div id="wrap-desk-subkeg"></div>
                                            <div class="white_card mb_30">
                                                <div class="white_card_body">
                                                    <div class="row mb-3">
                                                        <div class="col-md-12">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        
        <!-- Modal -->
        <div class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
            aria-hidden="true" id="formcetak">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalTitle">Cetak Monev Hasil Renja</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <input type="hidden" id="tw-cetak">
                                                <label class="form-label" for="#">Perangkat Daerah</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_pd_cetak">
                                                        <option value="">Pilih Perangkat Daerah...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row mb-3">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-danger" onclick="execcetak()">Cetak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-xl" id="advcetakModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cetak Lainnya</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div  class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" role="button" class="form-control border-0 bg-primary text-white" onclick="renderDtCetak('kegiatan')" value="Data Set Indikator Kegiatan">
                                    </div>                               
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" role="button" class="form-control border-0 bg-success text-white" onclick="renderDtCetak('subkegiatan')" value="Data Set Indikator Sub Kegiatan">
                                    </div>                            
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <input type="text" role="button" class="form-control border-0 bg-danger text-white" onclick="renderDtCetak('rkpd')" value="Data RKPD">
                                    </div>                            
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white_card card_height_100 mb_30">
                                    <div class="white_card_header">
                                        <div class="box_header m-0">
                                            <div class="main-title">
                                                <h3 class="m-0" id="dynamic_label_cetakdata"></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="white_card_body d-none" id="wrap_dynamic_tablecetak">
                                        <div class="QA_section">
                                            <div class="QA_table mb_30">
                                                <!-- table-responsive -->
                                                <table class="table " id="dynamic_tablecetak">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script>
            var table,table_sub,table_cetakdynamic;                                   
            var id_role = <?=Session::get('id_role');?>;
            var vis_maintab = (id_role == 4) ? [0,2,3] : [0,3], vis_maintab_sub = (id_role == 4) ? [0,1,3,4] : [0,1,4];

            $( document ).ready(function() {
                
                var st_input_keg = '<?=$status_input_keg;?>'
                var st_input_subkeg = '<?=$status_input_subkeg;?>'
                
                if(st_input_keg){
                    $('#f_st_input_keg').val(st_input_keg)
                    $('#f_st_input_keg').niceSelect('update');
                }
                if(st_input_subkeg){
                    $('#f_st_input_subkeg').val(st_input_subkeg)
                    $('#f_st_input_subkeg').niceSelect('update');

                    var url = window.location.href;
                    var activeTab = url.substring(url.indexOf("#") + 1);
                    $(".tab-pane").removeClass("active")
                    $("#" + activeTab).addClass("active")
                    $('a[href="#'+activeTab+'"]').tab('show')
                }
                var collapsedGroups = {},collapsedGroupsSub = {},top,middle,parent;
                table = $('#table_kegiatan').DataTable( {
                    ajax: {
                        url:"{{ url('/api/monev/renja/kegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('tahun_monev');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}},
                            nama_program: function() { return $('#f_program_keg').next().find('li.selected').attr('data-value') },
                            nama_kegiatan: function() { return $('#f_kegiatan_keg').next().find('li.selected').attr('data-value') },
                            nama_pd: function() { return $('#f_pd_keg').next().find('li.selected').attr('data-value') },
                            status: function() { return $('#f_st_input_keg').next().find('li.selected').attr('data-value') }
                        },
                    },
                    columns: [ 
                        { "data": "program.nama_program" },
                        { "data": "nama_kegiatan", 
                            "render": function ( data, type, row, meta ) {
                                var show_pdname = '',btn_tw1 = 'btn-primary',btn_tw2 = 'btn-primary',btn_tw3 = 'btn-primary',btn_tw4 = 'btn-primary'
                                if(id_role != 4){
                                    show_pdname = ''
                                }
                                btn_tw1 = row.t_1inputed == 0  ? "btn-danger" : ( row.t_1inputed < row.total_ind && row.t_1inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw2 = row.t_2inputed == 0  ? "btn-danger" : ( row.t_2inputed < row.total_ind && row.t_2inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw3 = row.t_3inputed == 0  ? "btn-danger" : ( row.t_3inputed < row.total_ind && row.t_3inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw4 = row.t_4inputed == 0  ? "btn-danger" : ( row.t_4inputed < row.total_ind_perubahan && row.t_4inputed != 0 ? "btn-warning" : "btn-primary");

                                var kategori = row.total_persen <= 50 ? 'Sangat Rendah' : (row.total_persen <= 65 ? 'Rendah' : (row.total_persen <= 75 ? 'Sedang' : (row.total_persen <= 90 ? 'Tinggi' : 'Sangat Tinggi'))); 
                                var total_persen = row.total_persen != null ? Number(row.total_persen).toFixed(2) : 0;
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;border: solid #E2FFE2;background: #fff !Important;">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+show_pdname+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<div class="row mb-3"><div class="col"><h6 style="font-size:12px;">Jumlah Indikator <span class="badge bg-secondary">'+row.total_ind+'</span></h6></div><div class="col"><h6 style="font-size:12px;">Rata-rata Persentase <span class="badge bg-primary">'+total_persen+'%</span></h6></div><div class="col"><h6 style="font-size:12px;">Kategori <span class="badge bg-primary">'+kategori+'</span></h6></div></div>'+
                                '<div class="row mb-3"><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw1+' " onclick="openforminputrealisasikeg(1,'+row.id_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 1</button></div><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw2+' " onclick="openforminputrealisasikeg(2,'+row.id_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 2 </button></div><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw3+' " onclick="openforminputrealisasikeg(3,'+row.id_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 3</button></div></div>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "nama_skpd",
                            "render": function ( data, type, row, meta ) {
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;">'+
                                '<div class="white_card_header QA_section" style="padding: 10px 0px;">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "id_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                return ''
                            }
                        },
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    initComplete: function () {
                        this.api()
                        .columns(0)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Program</option></select>')
                            .appendTo($('#f_program_keg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'">' + d + '</option>');
                            });
                            $('#f_program_keg').niceSelect('update');
                        });

                        this.api()
                        .columns(1)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Kegiatan</option></select>')
                            .appendTo($('#f_kegiatan_keg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'">' + d + '</option>');
                            });
                            $('#f_kegiatan_keg').niceSelect('update');
                        });
                        
                        this.api()
                        .columns(2)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Perangkat Daerah</option></select>')
                            .appendTo($('#f_pd_keg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                var st_skpd = '<?=$status_input_pd;?>';    
                                if(st_skpd)
                                {
                                    if(d.toLowerCase().indexOf(st_skpd.toLowerCase()) >= 0)
                                    {
                                        select.append('<option value="'+ d +'" selected>' + d + '</option>');
                                    }else{                                        
                                        select.append('<option value="'+ d +'">' + d + '</option>');
                                    }
                                }else{
                                    select.append('<option value="'+ d +'">' + d + '</option>');
                                }
                            });

                            $('#f_pd_keg').niceSelect('update');                                        
                            table.ajax.reload();
                        });
                    },
                    columnDefs: [ 
                        {
                            targets: vis_maintab,
                            visible: false
                        },
                        { "width": "15%", "targets": 2 } 
                    ],
                } );

                table_sub = $('#table_subkegiatan').DataTable( {
                    ajax: {
                        url:"{{ url('/api/monev/renja/subkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('tahun_monev');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}},
                            nama_program: function() { return $('#f_program_subkeg').next().find('li.selected').attr('data-value') },
                            nama_kegiatan: function() { return $('#f_kegiatan_subkeg').next().find('li.selected').attr('data-value') },
                            nama_subkegiatan: function() { return $('#f_subkegiatan_subkeg').next().find('li.selected').attr('data-value') },
                            nama_pd: function() { return $('#f_pd_subkeg').next().find('li.selected').attr('data-value') },
                            status: function() { return $('#f_st_input_subkeg').next().find('li.selected').attr('data-value') }
                        },
                    },
                    columns: [ 
                        { "data": "kegiatan.program.nama_program" },
                        { "data": "kegiatan.nama_kegiatan" },
                        { "data": "nama_sub_kegiatan", 
                            "render": function ( data, type, row, meta ) {
                                var show_pdname = '',btn_tw1 = 'btn-primary',btn_tw2 = 'btn-primary',btn_tw3 = 'btn-primary',btn_tw4 = 'btn-primary'
                                if(id_role != 4){
                                    show_pdname = ''
                                }
                                btn_tw1 = row.t_1inputed == 0  ? "btn-danger" : ( row.t_1inputed < row.total_ind && row.t_1inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw2 = row.t_2inputed == 0  ? "btn-danger" : ( row.t_2inputed < row.total_ind && row.t_2inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw3 = row.t_3inputed == 0  ? "btn-danger" : ( row.t_3inputed < row.total_ind && row.t_3inputed != 0 ? "btn-warning" : "btn-primary");
                                btn_tw4 = row.t_4inputed == 0  ? "btn-danger" : ( row.t_4inputed < row.total_ind_perubahan && row.t_4inputed != 0 ? "btn-warning" : "btn-primary");

                                var kategori = row.total_persen <= 50 ? 'Sangat Rendah' : (row.total_persen <= 65 ? 'Rendah' : (row.total_persen <= 75 ? 'Sedang' : (row.total_persen <= 90 ? 'Tinggi' : 'Sangat Tinggi'))); 
                                var total_persen = row.total_persen != null ? Number(row.total_persen).toFixed(2) : 0;
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;border: solid #E2FFE2;background: #fff !Important;">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+show_pdname+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<div class="row mb-3"><div class="col"><h6 style="font-size:12px;">Jumlah Indikator <span class="badge bg-secondary">'+row.total_ind+'</span></h6></div><div class="col"><h6 style="font-size:12px;">Rata-rata Persentase <span class="badge bg-primary">'+total_persen+'%</span></h6></div><div class="col"><h6 style="font-size:12px;">Kategori <span class="badge bg-primary">'+kategori+'</span></h6></div></div>'+
                                '<div class="row mb-3"><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw1+' " onclick="openforminputrealisasisubkeg(1,'+row.id_sub_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 1</button></div><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw2+' " onclick="openforminputrealisasisubkeg(2,'+row.id_sub_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 2 </button></div><div class="col '+((row.total_ind == 0)?"d-none":"")+'"><button class="btn btn-sm '+btn_tw3+' " onclick="openforminputrealisasisubkeg(3,'+row.id_sub_kegiatan+','+row.id_skpd+')">Input Realisasi Tw. 3</button></div></div>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "nama_skpd",
                            "render": function ( data, type, row, meta ) {
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;">'+
                                '<div class="white_card_header QA_section" style="padding: 10px 0px;">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "id_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                return ''
                            }
                        }
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    initComplete: function () {
                        this.api()
                        .columns(0)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Program</option></select>')
                            .appendTo($('#f_program_subkeg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'">' + d + '</option>');
                            });
                            $('#f_program_subkeg').niceSelect('update');
                        });

                        this.api()
                        .columns(1)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Kegiatan</option></select>')
                            .appendTo($('#f_kegiatan_subkeg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'">' + d + '</option>');
                            });
                            $('#f_kegiatan_subkeg').niceSelect('update');
                        });

                        this.api()
                        .columns(2)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Sub Kegiatan</option></select>')
                            .appendTo($('#f_subkegiatan_subkeg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'">' + d + '</option>');
                            });
                            $('#f_subkegiatan_subkeg').niceSelect('update');
                        });

                        this.api()
                        .columns(3)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Perangkat Daerah</option></select>')
                            .appendTo($('#f_pd_subkeg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                var st_subkeg_skpd = '<?=$status_input_pd_subkeg;?>';    
                                if(st_subkeg_skpd)
                                {
                                    if(d.toLowerCase().indexOf(st_subkeg_skpd.toLowerCase()) >= 0)
                                    {
                                        select.append('<option value="'+ d +'" selected>' + d + '</option>');
                                        filter_table('subkeg');
                                    }else{                                        
                                        select.append('<option value="'+ d +'">' + d + '</option>');
                                    }
                                }else{
                                    select.append('<option value="'+ d +'">' + d + '</option>');
                                }
                            });
                            $('#f_pd_subkeg').niceSelect('update');                                       
                            table_sub.ajax.reload();
                        });
                    },
                    columnDefs: [ 
                        {
                            targets: vis_maintab_sub,
                            visible: false
                        },
                        { "width": "15%", "targets": 3 } 
                    ],
                });
            });
            
            function openforminputrealisasikeg(triwulan,id_kegiatan,id_pd){
                $('#wrap-ind-keg').empty();
                $('#wrap-desk-keg').empty();
                $('#nama_program').html('');
                $('#nama_kegiatan').html('');
                $('#nama_pd').html('');
                $('#anggaran').html('');
                $('#r_anggaran').html('');
                $('#triwulan_keg').html(triwulan);
                $('#btn-simpan-keg').attr('disabled',false)

                $('#forminputrealisasikegModal').modal({backdrop: 'static', keyword: false})
                $('#forminputrealisasikegModal').modal('show')
                loadrealisasiindkeg(triwulan,id_kegiatan,id_pd)
                return false;
            }

            function loadrealisasiindkeg(triwulan,id_kegiatan,id_pd){                
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/monev/renja/realisasiindikatorkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        step : 'murni',
                        id_kegiatan: id_kegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $('#id_pd').val(id_pd);
                        $('#id_kegiatan').val(id_kegiatan);
                        $('#nama_kegiatan').html(response.data.headindikator[0].kegiatan[0].nama_kegiatan);
                        $('#nama_program').html(response.data.headindikator[0].kegiatan[0].program.nama_program);
                        $('#nama_pd').html(response.data.headindikator[0].renja.skpd.nama_skpd);

                        var indexOfarray = response.data.headindikator[0].kegiatan[0].apbd_kota.findIndex((item) => item.id_renja === response.data.headindikator[0].renja.id_renja.toString())
                        var anggaran = (response.data.headindikator[0].kegiatan[0].apbd_kota[indexOfarray].apbd_kota != null) ? response.data.headindikator[0].kegiatan[0].apbd_kota[indexOfarray].apbd_kota : '0'
                        if(response.data.headindikator[0].kegiatan[0].realisasi_anggaran.length > 0){
                            var indexOfarrayreal = response.data.headindikator[0].kegiatan[0].realisasi_anggaran.findIndex((item) => item.id_renja === response.data.headindikator[0].renja.id_renja.toString())
                            var real_rp1 = (response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'
                            var real_rp2 = (response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'
                            var real_rp3 = (response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'
                            var real_rp4 = (response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw4 != null) ? response.data.headindikator[0].kegiatan[0].realisasi_anggaran[indexOfarray].realisasi_tw4 : '0'
                        }else{
                            var real_rp1 = 0
                            var real_rp2 = 0
                            var real_rp3 = 0
                            var real_rp4 = 0
                        }
                        $('#anggaran').html(parseFloat(anggaran).toFixed(2));
                        if(triwulan == 1){
                            $('#r_anggaran').html(parseFloat(real_rp1).toFixed(2));   
                        }else if(triwulan == 2){
                            $('#r_anggaran').html(parseFloat(real_rp2).toFixed(2));
                        }else if(triwulan == 3){
                            $('#r_anggaran').html(parseFloat(real_rp3).toFixed(2));
                        }else{
                            $('#r_anggaran').html(parseFloat(real_rp4).toFixed(2));
                        }
                        $('#anggaran').priceFormat({
                            prefix: 'Rp. ',
                            centsSeparator: '.',
                            thousandsSeparator: ','
                        });
                        $('#r_anggaran').priceFormat({
                            prefix: 'Rp. ',
                            centsSeparator: '.',
                            thousandsSeparator: ','
                        });

                        renderdataindbytriwulan(triwulan, response, id_kegiatan, id_pd)
                    }
                });
                
            }

            function renderdataindbytriwulan(tw,response,id_kegiatan,id_pd){
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/monev/renja/murni/inputrealisasiindkegiatan') }}",
                    data: {
                        tw: tw,
                        data_realisasi: response.data,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        $("#wrap-ind-keg").html(response);
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: "{{ url('/monev/renja/murni/inputanalisiskegiatan') }}",
                    data: {
                        tw: tw,
                        id_kegiatan: id_kegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $("#wrap-desk-keg").html(response);
                    }
                });
            }

            function openforminputrealisasisubkeg(triwulan,id_subkegiatan,id_pd){
                $('#wrap-ind-subkeg').empty();
                $('#wrap-desk-subkeg').empty();
                $('#nama_program_sub').html('');
                $('#nama_kegiatan_sub').html('');
                $('#nama_subkegiatan').empty();
                $('#nama_pd_sub').empty();
                $('#anggaran_subkeg').html('');
                $('#r_anggaran_subkeg').html('');
                $('#triwulan_subkeg').html(triwulan);
                $('#btn-simpan-subkeg').attr('disabled',false)

                $('#forminputrealisasisubkegModal').modal({backdrop: 'static', keyword: false})
                $('#forminputrealisasisubkegModal').modal('show')
                
                loadrealisasiindsubkeg(triwulan,id_subkegiatan,id_pd)
                return false;
            }

            function loadrealisasiindsubkeg(triwulan,id_subkegiatan,id_pd){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/monev/renja/realisasiindikatorsubkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('tahun_monev');?>,
                        step: 'murni',
                        id_subkegiatan : id_subkegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $('#id_pd_subkegiatan').val(id_pd);
                        $('#id_subkegiatan').val(id_subkegiatan);
                        $('#nama_program_sub').html(response.data.headindikator[0].subkegiatan.kegiatan.program.nama_program);
                        $('#nama_kegiatan_sub').html(response.data.headindikator[0].subkegiatan.kegiatan.nama_kegiatan);
                        $('#nama_subkegiatan').html(response.data.headindikator[0].subkegiatan.nama_sub_kegiatan);
                        $('#nama_pd_sub').html(response.data.headindikator[0].renja.skpd.nama_skpd);

                        var indexOfarray = response.data.headindikator[0].subkegiatan.apbd_kota.findIndex((item) => item.id_renja === response.data.headindikator[0].renja.id_renja.toString())
                        var anggaran = (response.data.headindikator[0].subkegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? response.data.headindikator[0].subkegiatan.apbd_kota[indexOfarray].apbd_kota : '0'
                        var indexOfarrayreal = response.data.headindikator[0].subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === response.data.headindikator[0].renja.id_renja.toString())
                        var real_rp1 = (response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'
                        var real_rp2 = (response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'
                        var real_rp3 = (response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'
                        var real_rp4 = (response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw4 != null) ? response.data.headindikator[0].subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw4 : '0'
                        
                        $('#anggaran_subkeg').html(parseFloat(anggaran).toFixed(2));
                        
                        if(triwulan == 1){
                            $('#r_anggaran_subkeg').html(parseFloat(real_rp1).toFixed(2));
                        }else if(triwulan == 2){
                            $('#r_anggaran_subkeg').html(parseFloat(real_rp2).toFixed(2));
                        }else if(triwulan == 3){
                            $('#r_anggaran_subkeg').html(parseFloat(real_rp3).toFixed(2));
                        }else{
                            $('#r_anggaran_subkeg').html(parseFloat(real_rp4).toFixed(2));
                        }

                        $('#anggaran_subkeg').priceFormat({
                            prefix: 'Rp. ',
                            centsSeparator: '.',
                            thousandsSeparator: ','
                        });
                        $('#r_anggaran_subkeg').priceFormat({
                            prefix: 'Rp. ',
                            centsSeparator: '.',
                            thousandsSeparator: ','
                        });

                        renderdataindsubbytriwulan(triwulan, response, id_subkegiatan, id_pd)
                    }
                });
            }

            function renderdataindsubbytriwulan(tw,response,id_subkegiatan,id_pd){
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/monev/renja/murni/inputrealisasiindsubkegiatan') }}",
                    data: {
                        tw: tw,
                        data_realisasi: response.data,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        $("#wrap-ind-subkeg").html(response);
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: "{{ url('/monev/renja/murni/inputanalisissubkegiatan') }}",
                    data: {
                        tw: tw,
                        id_subkegiatan: id_subkegiatan,
                        id_pd: id_pd
                    },
                    success: function (response) {
                        $("#wrap-desk-subkeg").html(response);
                    }
                });
            }
            function saverealisasikegiatan(){
                $('#btn-simpan-keg').attr('disabled','disabled')
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                let myForm = document.getElementById('formrealisasikegiatan');
                let formData = new FormData(myForm);
                formData.append('triwulan',$('#triwulan_keg').text())
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/monev/renja/murni/saverealisasikeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            Swal.fire({
                                title: 'Berhasil!',
                                html: response.message,
                                type: 'success'
                            });
                            table.ajax.reload( null, false );
                            $('#forminputrealisasikegModal').modal('hide')
                        }else{
                            Swal.fire({
                                title: 'Gagal!!',
                                html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                type: 'danger'
                            });
                        }
                    }
                });
            }

            function saverealisasisubkegiatan(){
                $('#btn-simpan-subkeg').attr('disabled','disabled')
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                let myForm = document.getElementById('formrealisasisubkegiatan');
                let formData = new FormData(myForm);
                formData.append('triwulan',$('#triwulan_subkeg').text())
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/monev/renja/murni/saverealisasisubkeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            Swal.fire({
                                title: 'Berhasil!',
                                html: response.message,
                                type: 'success'
                            });
                            table_sub.ajax.reload( null, false );
                            $('#forminputrealisasisubkegModal').modal('hide')
                        }else{
                            Swal.fire({
                                title: 'Gagal!!',
                                html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                type: 'danger'
                            });
                        }
                    }
                });
            }

            function restrictcomma(e){
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[^,;]+$/;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) {
                        theEvent.preventDefault();
                        alert('Untuk angka desimal silahkan gunakan tanda titik (.)')
                    }
                }
            }

            function renderBarPersentase(key,target,value,numel){
                console.log(key)
                var persen = 0.00
                if(target != 0 && target != '0' && !isNaN(value) && !isNaN(target)){
                    persen = (value/target)*100
                    console.log(persen)
                    if(persen%1 != 0){
                        persen = persen.toFixed(2)
                    }
                }

                $('#fillbar_ind'+key+numel).attr('data-percentage',persen);
                $('#bar_ind'+key+numel).barfiller();

                $('#persenbar_ind'+numel).val(persen);
            }

            function renderBarPersentaseTotal(key,target,numel,total_murni=0,tw=0,vb=0){
                var total_v = 0,op = ''
    
                if($("#valuetw-1-" + numel).length != 0) {
                    total_v = total_v+Number($("#valuetw-1-" + numel).val())
                }
                if($("#valuetw-2-" + numel).length != 0) {
                    total_v = total_v+Number($("#valuetw-2-" + numel).val())
                }
                if($("#valuetw-3-" + numel).length != 0) {
                    total_v = total_v+Number($("#valuetw-3-" + numel).val())
                }
                if($("#valuetw-4-" + numel).length != 0) {
                    total_v = total_v+Number($("#valuetw-4-" + numel).val())
                }
                
                var persen = (total_v/target)*100
                persen = cekOp(persen,tw,vb,numel,total_murni,key)
                
                if(target == 0 || target == '0'){
                    persen = 0.00
                }else{
                    if(persen%1 != 0){
                        persen = persen.toFixed(2)
                    }
                }

                $('#fillbar_ind'+key+'_tot'+numel).attr('data-percentage',persen);
                $('#bar_ind'+key+'_tot'+numel).barfiller();

                $('#persenbar_ind_tot'+numel).val(persen);
            }

            function cekOp(x,tw,vb,num,tot,k){
                if(vb<($('#fillbar_ind'+k+num).attr('data-percentage'))){
                    return (tot-vb)+parseFloat($('#fillbar_ind'+k+num).attr('data-percentage'));
                }else if(vb>($('#fillbar_ind'+k+num).attr('data-percentage'))){
                    if((tot - vb) != 0 && ($('#fillbar_ind'+k+num).attr('data-percentage')) != 0){
                        return (tot-vb)+parseFloat($('#fillbar_ind'+k+num).attr('data-percentage'))
                    }else if(($('#fillbar_ind'+k+num).attr('data-percentage')) == 0){
                        return tot-vb
                    }
                    return tot-x;
                }else{
                    return tot;
                }
            }

            function loadskpd(id_pd=0,elm){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:{{$id_pd}}
                    },
                    success: function(response) {
                        $('#id_pd_cetak').empty();
                        $('#id_pd_cetak').append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#id_pd_cetak').append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id_pd != 0){
                            $('#id_pd_cetak').val(id);
                        }
                        $('#id_pd_cetak').niceSelect('update');
                    }
                });
            }

            function openformcetak(tw){
                if({{Session::get('id_role')}} != 4){
                    $('#formcetak').modal('show')
                    $('#tw-cetak').val(tw)
                    loadskpd(0,'#id_pd_cetak')
                }else{
                    $('#tw-cetak').val(tw)
                    execcetak(true)   
                }
                return false;
            }

            function execcetak(bypd=false){
                var twcetak = $('#tw-cetak').val(),periode = {{Session::get('tahun_monev')}}
                if(bypd){
                    var pd = <?php echo json_encode(Session::get('id_skpd'));?>;
                    window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=monev2021/monevhasilrenjamurni-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&periode='+periode+'&skpd='+pd.toString()+'&triwulan='+twcetak);
                }else{
                    var pd = $('#id_pd_cetak').val()
                    window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=monev2021/monevhasilrenjamurni-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&periode='+periode+'&skpd='+pd+'&triwulan='+twcetak);
                }
            }
            
            function related_filter(table_name, filter_name, filter_changed)
            {
                if(table_name == 'keg')
                {
                    if(filter_name == 'f_program_keg')
                    {
                        // Filter Program Kegiatan
                        var program = $('#'+filter_name).next().find('li.selected').attr('data-value');
                        var selectProg = $('#f_program_keg');
                        var selectKeg = $('#f_kegiatan_keg');
                        var selectPd = $('#f_pd_keg');
                        var listProg = new Array();
                        var listKeg = new Array();
                        var listPd = new Array();
                        if(program != '')
                        {
                            $.ajax({
                                type : 'GET',
                                data:{
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    nama_program : program
                                },
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listKeg.indexOf(value.nama_kegiatan) === -1) {
                                            listKeg.push(value.nama_kegiatan);
                                        }
                                        if(listPd.indexOf(value.nama_skpd) === -1) {
                                            listPd.push(value.nama_skpd);
                                        }
                                    });

                                    selectKeg.empty();
                                    selectKeg.append('<option value="">Semua Kegiatan</option>');
                                    $.each(listKeg, function(index, value){
                                        selectKeg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectKeg.niceSelect('update');

                                    selectPd.empty();
                                    selectPd.append('<option value="">Semua Perangkat Daerah</option>');
                                    $.each(listPd, function(index, value){
                                        selectPd.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectPd.niceSelect('update');
                                }
                            });
                        }else{                            
                            $.ajax({
                                type : 'GET',
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                data: {
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    id_pd:{{$id_pd}},
                                    nama_program: '',
                                    nama_kegiatan: '',
                                    nama_pd: '',
                                    status: ''
                                },
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listProg.indexOf(value.program.nama_program) === -1) {
                                            listProg.push(value.program.nama_program);
                                        }
                                        if(listKeg.indexOf(value.nama_kegiatan) === -1) {
                                            listKeg.push(value.nama_kegiatan);
                                        }
                                        if(listPd.indexOf(value.nama_skpd) === -1) {
                                            listPd.push(value.nama_skpd);
                                        }
                                    });

                                    selectProg.empty();
                                    selectProg.append('<option value="">Semua Program</option>');
                                    $.each(listProg, function(index, value){
                                        selectProg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectProg.niceSelect('update');

                                    selectKeg.empty();
                                    selectKeg.append('<option value="">Semua Kegiatan</option>');
                                    $.each(listKeg, function(index, value){
                                        selectKeg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectKeg.niceSelect('update');

                                    selectPd.empty();
                                    selectPd.append('<option value="">Semua Perangkat Daerah</option>');
                                    $.each(listPd, function(index, value){
                                        selectPd.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectPd.niceSelect('update');
                                }
                            })
                        }
                        filter_table('keg');   
                    }else if(filter_name == 'f_kegiatan_keg')
                    {
                        // Filter Kegiatan
                        var kegiatan = $('#'+filter_name).next().find('li.selected').attr('data-value');
                        var selectProg = $('#f_program_keg');
                        var selectKeg = $('#f_kegiatan_keg');
                        var selectPd = $('#f_pd_keg');
                        var listProg = new Array();
                        var listKeg = new Array();
                        var listPd = new Array();
                        if(kegiatan != '')
                        {
                            $.ajax({
                                type : 'GET',
                                data:{
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    nama_kegiatan : kegiatan
                                },
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listProg.indexOf(value.program.nama_program) === -1) {
                                            listProg.push(value.program.nama_program);
                                        }
                                        if(listPd.indexOf(value.nama_skpd) === -1) {
                                            listPd.push(value.nama_skpd);
                                        }
                                    });

                                    selectProg.empty();
                                    selectProg.append('<option value="">Semua Program</option>');
                                    $.each(listProg, function(index, value){
                                        selectProg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectProg.niceSelect('update');

                                    selectPd.empty();
                                    selectPd.append('<option value="">Semua Perangkat Daerah</option>');
                                    $.each(listPd, function(index, value){
                                        selectPd.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectPd.niceSelect('update');
                                }
                            });
                        }else{                            
                            $.ajax({
                                type : 'GET',
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                data: {
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    id_pd:{{$id_pd}},
                                    nama_program: '',
                                    nama_kegiatan: '',
                                    nama_pd: '',
                                    status: ''
                                },
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listProg.indexOf(value.program.nama_program) === -1) {
                                            listProg.push(value.program.nama_program);
                                        }
                                        if(listKeg.indexOf(value.nama_kegiatan) === -1) {
                                            listKeg.push(value.nama_kegiatan);
                                        }
                                        if(listPd.indexOf(value.nama_skpd) === -1) {
                                            listPd.push(value.nama_skpd);
                                        }
                                    });

                                    selectProg.empty();
                                    selectProg.append('<option value="">Semua Program</option>');
                                    $.each(listProg, function(index, value){
                                        selectProg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectProg.niceSelect('update');

                                    selectKeg.empty();
                                    selectKeg.append('<option value="">Semua Kegiatan</option>');
                                    $.each(listKeg, function(index, value){
                                        selectKeg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectKeg.niceSelect('update');

                                    selectPd.empty();
                                    selectPd.append('<option value="">Semua Perangkat Daerah</option>');
                                    $.each(listPd, function(index, value){
                                        selectPd.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectPd.niceSelect('update');
                                }
                            })
                        }
                        filter_table('keg');   
                    }else if(filter_name == 'f_pd_keg')
                    {
                        // Filter Perangkat Daerah
                        var pd = $('#'+filter_name).next().find('li.selected').attr('data-value');
                        var selectProg = $('#f_program_keg');
                        var selectKeg = $('#f_kegiatan_keg');
                        var selectPd = $('#f_pd_keg');
                        var listProg = new Array();
                        var listKeg = new Array();
                        var listPd = new Array();
                        if(pd != '')
                        {
                            $.ajax({
                                type : 'GET',
                                data:{
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    nama_pd : pd
                                },
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listProg.indexOf(value.program.nama_program) === -1) {
                                            listProg.push(value.program.nama_program);
                                        }
                                        if(listKeg.indexOf(value.nama_kegiatan) === -1) {
                                            listKeg.push(value.nama_kegiatan);
                                        }
                                    });

                                    selectProg.empty();
                                    selectProg.append('<option value="">Semua Program</option>');
                                    $.each(listProg, function(index, value){
                                        selectProg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectProg.niceSelect('update');

                                    selectKeg.empty();
                                    selectKeg.append('<option value="">Semua Kegiatan</option>');
                                    $.each(listKeg, function(index, value){
                                        selectKeg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectKeg.niceSelect('update');
                                }
                            });
                        }else{                            
                            $.ajax({
                                type : 'GET',
                                url:"{{ url('/api/monev/renja/kegiatan') }}",
                                data: {
                                    periode_usulan: <?=session()->get('tahun_monev');?>,
                                    step : 'murni',
                                    id_pd:{{$id_pd}},
                                    nama_program: '',
                                    nama_kegiatan: '',
                                    nama_pd: '',
                                    status: ''
                                },
                                success:function(data)
                                {
                                    $.each(data.data, function( index, value ) {
                                        if(listProg.indexOf(value.program.nama_program) === -1) {
                                            listProg.push(value.program.nama_program);
                                        }
                                        if(listKeg.indexOf(value.nama_kegiatan) === -1) {
                                            listKeg.push(value.nama_kegiatan);
                                        }
                                        if(listPd.indexOf(value.nama_skpd) === -1) {
                                            listPd.push(value.nama_skpd);
                                        }
                                    });

                                    selectProg.empty();
                                    selectProg.append('<option value="">Semua Program</option>');
                                    $.each(listProg, function(index, value){
                                        selectProg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectProg.niceSelect('update');

                                    selectKeg.empty();
                                    selectKeg.append('<option value="">Semua Kegiatan</option>');
                                    $.each(listKeg, function(index, value){
                                        selectKeg.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectKeg.niceSelect('update');

                                    selectPd.empty();
                                    selectPd.append('<option value="">Semua Perangkat Daerah</option>');
                                    $.each(listPd, function(index, value){
                                        selectPd.append('<option value="' +value.toUpperCase()+'">' +value.toUpperCase()+ '</option>');
                                    })
                                    selectPd.niceSelect('update');
                                }
                            })
                        }
                        filter_table('keg');   
                    }
                }
            }

            function filter_table(table_name){
                if(table_name == 'keg'){
                    table.ajax.reload();
                }else{
                    table_sub.ajax.reload();
                }
            }

            function openmodaladvcetak(){
                $('#wrap_dynamic_tablecetak').addClass('d-none')
                if ($.fn.DataTable.isDataTable( '#dynamic_tablecetak' ) ) {
                    table_cetakdynamic.clear().destroy();
                }
                $('#advcetakModal').modal('show')
            }

            function renderDtCetak(key){
                var url,lable_cetak,step;
                var columns_render = [],columnsdef_render = [];
                $('#wrap_dynamic_tablecetak').removeClass('d-none')
                if ($.fn.DataTable.isDataTable( '#dynamic_tablecetak' ) ) {
                    table_cetakdynamic.clear().destroy();
                }
                if(key == 'kegiatan'){    
                    url = "{{ url('/api/monev/renja/realisasiindikatorkegiatan') }}";
                    columns_render = [
                        {"title": "Urusan","data": "kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Urusan","data": "kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Bidang Urusan","data": "kegiatan.program.bidang_urusan.nama_bidang_urusan" },{"title": "Program","data": "kegiatan.program.nama_program" },{"title": "Kegiatan","data": "kegiatan.nama_kegiatan" },{"title": "Anggaran","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.apbd_kota.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? row.kegiatan.apbd_kota[indexOfarray].apbd_kota : '0'}},{"title": "Indikator Kegiatan","data": "indikator_kegiatan" },{"title": "Target","data": "target" },{"title": "Satuan","data": "satuan" },{"title": "Realisasi Tw 1","data": "realisasiindikatorkeg.t_1" },{"title": "Realisasi Anggaran TW 1","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'}},{"title": "Realisasi Tw 2","data": "realisasiindikatorkeg.t_2" },{"title": "Realisasi Anggaran TW 2","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'}},{"title": "Realisasi Tw 3","data": "realisasiindikatorkeg.t_3" },{"title": "Realisasi Anggaran TW 3","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'}},{"title": "PD","data": "renja.skpd.nama_skpd"},
                    ];
                    columnsdef_render = [
                        { targets: [0], visible: false }
                    ];
                    lable_cetak = "Data Set Indikator Kegiatan Renja ";
                    step = 'murni'
                }else if(key == 'subkegiatan'){ 
                    url = "{{ url('/api/monev/renja/realisasiindikatorsubkegiatan') }}";   
                    columns_render = [
                        {"title": "Urusan","data": "subkegiatan.kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Bidang Urusan","data": "subkegiatan.kegiatan.program.bidang_urusan.nama_bidang_urusan" },{"title": "Program","data": "subkegiatan.kegiatan.program.nama_program" },{"title": "Kegiatan","data": "subkegiatan.kegiatan.nama_kegiatan" },{"title": "Sub Kegiatan","data": "subkegiatan.nama_sub_kegiatan" },   {"title": "Anggaran","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.apbd_kota.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? row.subkegiatan.apbd_kota[indexOfarray].apbd_kota : '0'}},{"title": "Indikator Sub Kegiatan","data": "indikator_subkegiatan" },{"title": "Target","data": "target" },{"title": "Satuan","data": "satuan" },{"title": "Realisasi Tw 1","data": "realisasiindikatorsubkeg.t_1" },{"title": "Realisasi Anggaran TW 1","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'}},{"title": "Realisasi Tw 2","data": "realisasiindikatorsubkeg.t_2" },{"title": "Realisasi Anggaran TW 2","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'}},{"title": "Realisasi Tw 3","data": "realisasiindikatorsubkeg.t_3" },{"title": "Realisasi Anggaran TW 3","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'}},{"title": "PD","data": "renja.skpd.nama_skpd"},
                    ];
                    columnsdef_render = [
                        { targets: [0], visible: true }
                    ];
                    lable_cetak = "Data Set Indikator Sub Kegiatan Renja ";
                    step = 'murni'
                }else if(key == 'kegiatanperubahan'){    
                    url = "{{ url('/api/monev/renja/realisasiindikatorkegiatan') }}";
                    columns_render = [
                        {"title": "Urusan","data": "kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Urusan","data": "kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Bidang Urusan","data": "kegiatan.program.bidang_urusan.nama_bidang_urusan" },{"title": "Program","data": "kegiatan.program.nama_program" },{"title": "Kegiatan","data": "kegiatan.nama_kegiatan" },{"title": "Anggaran","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.apbd_kota.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? row.kegiatan.apbd_kota[indexOfarray].apbd_kota : '0'}},{"title": "Indikator Kegiatan","data": "indikator_kegiatan" },{"title": "Target","data": "target" },{"title": "Satuan","data": "satuan" },{"title": "Realisasi Tw 1","data": "realisasiindikatorkeg.t_1" },{"title": "Realisasi Anggaran TW 1","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'}},{"title": "Realisasi Tw 2","data": "realisasiindikatorkeg.t_2" },{"title": "Realisasi Anggaran TW 2","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'}},{"title": "Realisasi Tw 3","data": "realisasiindikatorkeg.t_3" },{"title": "Realisasi Anggaran TW 3","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.kegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? row.kegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'}},{"title": "PD","data": "renja.skpd.nama_skpd"},
                    ];
                    columnsdef_render = [
                        { targets: [0], visible: false }
                    ];
                    lable_cetak = "Data Set Indikator Kegiatan Renja Perubahan ";
                    step = 'perubahan'
                }else if(key == 'subkegiatanperubahan'){ 
                    url = "{{ url('/api/monev/renja/realisasiindikatorsubkegiatan') }}";   
                    columns_render = [
                        {"title": "Urusan","data": "subkegiatan.kegiatan.program.bidang_urusan.urusan.nama_urusan" },{"title": "Bidang Urusan","data": "subkegiatan.kegiatan.program.bidang_urusan.nama_bidang_urusan" },{"title": "Program","data": "subkegiatan.kegiatan.program.nama_program" },{"title": "Kegiatan","data": "subkegiatan.kegiatan.nama_kegiatan" },{"title": "Sub Kegiatan","data": "subkegiatan.nama_sub_kegiatan" },   {"title": "Anggaran","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.apbd_kota.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.apbd_kota[indexOfarray].apbd_kota != null) ? row.subkegiatan.apbd_kota[indexOfarray].apbd_kota : '0'}},{"title": "Indikator Sub Kegiatan","data": "indikator_subkegiatan" },{"title": "Target","data": "target" },{"title": "Satuan","data": "satuan" },{"title": "Realisasi Tw 1","data": "realisasiindikatorsubkeg.t_1" },{"title": "Realisasi Anggaran TW 1","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw1 : '0'}},{"title": "Realisasi Tw 2","data": "realisasiindikatorsubkeg.t_2" },{"title": "Realisasi Anggaran TW 2","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw2 : '0'}},{"title": "Realisasi Tw 3","data": "realisasiindikatorsubkeg.t_3" },{"title": "Realisasi Anggaran TW 3","data": "renja.id_renja","render": function ( data, type, row, meta ) { var indexOfarray = row.subkegiatan.realisasi_anggaran.findIndex((item) => item.id_renja === data.toString()) ;return (row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 != null) ? row.subkegiatan.realisasi_anggaran[indexOfarray].realisasi_tw3 : '0'}},{"title": "PD","data": "renja.skpd.nama_skpd"},
                    ];
                    columnsdef_render = [
                        { targets: [0], visible: true }
                    ];
                    lable_cetak = "Data Set Indikator Sub Kegiatan Renja Perubahan ";
                    step = 'perubahan'
                }else if(key == 'rkpd'){ 
                    window.location.href = "{{URL::to('monev/renja/exportrkpd')}}"
                }

                table_cetakdynamic = $('#dynamic_tablecetak').DataTable( {
                    ajax: {
                        url:url,
                        data: {
                            periode_usulan: <?=session()->get('tahun_monev');?>,
                            step : step,
                            id_pd:{{$id_pd}},
                        },
                    },
                    columns: columns_render,
                    columnDefs: columnsdef_render,
                    responsive: true,
                    retrieve: true,
                    "lengthChange": true,
                    lengthMenu: [10, 20, 50, 100, 200],
                    dom: 'Blfrtip',
                    buttons: [
                        { 
                            extend: 'excelHtml5',
                            "text": 'Cetak Excel',
                            title: lable_cetak+<?=session()->get('tahun_monev');?>,
                            messageBottom: 'printed by: simrenda. ('+ new Date().toLocaleString() +')'
                        },
                    ]
                });
            }
        </script>

        <!-- Footer -->
        @include('layouts.footer')
@endsection
<div class="col-lg-12">
    <div class="d-flex align-items-start">
        {{-- LIST PROGRAM --}}
        <div class="col-lg-5 overflow-auto d-flex align-items-start" style="height: 500px;">
            <ul class="nav flex-column nav-pills me-3" id="v-pills-tab-prog" role="tablist" aria-orientation="vertical">
                @foreach ($prog as $no => $prg)
                <li class='nav-item' style="list-style-type: square;">
                    <a  class='nav-link' 
                        id='programitem{{$prg->id_program}}' 
                        data-bs-toggle='pill' 
                        href='#indprogramitem{{$prg->id_program}}'  
                        role='tab'  
                        aria-controls='programitem{{$prg->id_program}}'  
                        aria-selected='false' 
                    >
                        <i class=""></i>
                        <span>{{ $prg->nama_program }}</span>
                    </a>
                </li>
                @endforeach
            </ul>        
        </div>        
        {{-- LIST INDIKATOR PROGRAM --}}
        <div class="col-lg-7 tab-content overflow-auto" style="height: 500px;">
            @foreach ($prog as $no => $prg)
            <div    class='tab-pane fade'    {{-- tab-pane --}}
                    id='indprogramitem{{$prg->id_program}}'
                    role='tabpanel'  
                    aria-labelledby='programitem{{$prg->id_program}}-tab' 
            >
                <form id='formIndProg{{$prg->id_program}}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                    <input type='hidden' name='tahun' value="{{ Session::get('tahun_monev') }}" />
                    <div class='table-responsive'>{{-- (awal) div table --}}
                        <table id='formIndProgitem{{$prg->id_program}}' class='table table-borderless'>{{-- (awal) table --}}
                            <thead>
                                <tr>
                                    <th class='text-center fw-bold text-center mb-3'><h5>Daftar Indikator Program</h5></th>
                                </tr>
                            </thead>
                            @if (!is_null($indProg[$prg->id_program]))
                                <tbody>                            
                                    @foreach ($indProg[$prg->id_program] as $noInd => $indikator)
                                    @php
                                        $indexTRIKP = count($indikator->target_realisasi_i_k_p);
                                        $targetIKP = '';
                                        $realisasiIKP = '';
                                        if($indexTRIKP > 0)
                                        {
                                            $targetIKP = $indikator->target_realisasi_i_k_p[0]->target;
                                            $realisasiIKP = $indikator->target_realisasi_i_k_p[$indexTRIKP-1]->realisasi;
                                        }
                                    @endphp
                                    <tr>
                                        <td>
                                            <h5 class='mb-0'>Indikator #{{($noInd+1)}}</h5>
                                            <div class='border border-primary shadow-sm p-3 mb-3'>
                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class='col-lg-6'> {{-- nama indikator program --}}
                                                        <h6>Nama Indikator</h6>
                                                        <div id='namaIndKeg_{{$indikator->id_indikator_program}}' class='fw-bold'>
                                                            {{$indikator->indikator_program}}
                                                        </div>
                                                        <input class="form-control editIndPRG{{$indikator->id_indikator_program}}" type="text" name="indikator_program_{{$indikator->id_indikator_program}}" value="{{$indikator->indikator_program}}"/>
                                                    </div>
                                                    
                                                    <div class='col-lg-6'>
                                                        <h6>Rumus</h6>
                                                        <select class="form-control" name="rumus[]">
                                                            @foreach($rumus as $rms)
                                                                <option 
                                                                    @php
                                                                        if(!is_null($indikator->id_rumus) && $indikator->id_rumus == $rms->id_rumus)
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                    @endphp
                                                                    value="{{$rms->id_rumus}}"
                                                                >
                                                                    {{$rms->rumus}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- target indikator program --}}
                                                        <h6>Target</h6>
                                                        <div id='targetIndProg_{{$indikator->id_indikator_program}}' class='fw-bold'>
                                                            <span class="target">{{$targetIKP}}</span>
                                                            <span class="target_perubahan">{{$realisasiIKP}}</span>
                                                        </div>
                                                        <input class="form-control editIndPRG{{$indikator->id_indikator_program}}" type="text" name="target_program_{{$indikator->id_indikator_program}}" value="{{$targetIKP}}"/>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- satuan indikator program --}}
                                                        <h6>Satuan</h6>
                                                        <div id='satuanIndProg_{{$indikator->id_indikator_program}}' class='fw-bold'>
                                                            {{$indikator->satuan}}
                                                        </div>
                                                        <input class="form-control editIndPRG{{$indikator->id_indikator_program}}" type="text" name="satuan_program_{{$indikator->id_indikator_program}}" value="{{$indikator->satuan}}"/>
                                                    </div>
                                                    <div class='col-lg-5'> {{-- progress --}}
                                                        <h6>Capaian Kinerja Program</h6>
                                                        @php
                                                            $persen_tw1 = 0;
                                                            $persen_tw2 = 0;
                                                            $persen_tw3 = 0;
                                                            $persen_perubahan = 0;
                                                            $target = 0;
                                                            $target_perubahan = 0;
                                                            if(!is_null($indikator->target_realisasi_i_k_p))
                                                            {
                                                                $r_tw = array(
                                                                    1 => '',
                                                                    2 => '',
                                                                    3 => '',
                                                                    4 => ''
                                                                );
                                                                foreach($indikator->target_realisasi_i_k_p as $tr)
                                                                {
                                                                    if($tr->triwulan == 1)
                                                                    {
                                                                        $r_tw[1] = $tr->realisasi;
                                                                        $target = floatval($tr->target);
                                                                    }else if($tr->triwulan == 2)
                                                                    {
                                                                        $r_tw[2] = $tr->realisasi;
                                                                    }else if($tr->triwulan == 3)
                                                                    {
                                                                        $r_tw[3] = $tr->realisasi;
                                                                    }else if($tr->triwulan == 4)
                                                                    {
                                                                        $r_tw[4] = $tr->realisasi;
                                                                        $target_perubahan = floatval($tr->target);
                                                                    }
                                                                }
                                                                if(floatval($target) != 0)
                                                                {
                                                                    if($indikator->id_rumus && $indikator->id_rumus == 1)
                                                                    {
                                                                        // rumus triwulan
                                                                        $persen_tw1 = (floatval($r_tw[1])/(float)$target*100)/4;
                                                                        $persen_tw2 = (floatval($r_tw[1])+floatval($r_tw[2])/(float)$target*100)/4;
                                                                        $persen_tw3 = (floatval($r_tw[1])+floatval($r_tw[2])+floatval($r_tw[3])/(float)$target*100)/4;
                                                                    }else if($indikator->id_rumus && $indikator->id_rumus == 2)
                                                                    {
                                                                        // rumus semester
                                                                        $persen_tw1 = (floatval($r_tw[1])/(float)$target*100)/2;
                                                                        $persen_tw2 = (floatval($r_tw[1])+floatval($r_tw[2])/(float)$target*100)/2;
                                                                        $persen_tw3 = (floatval($r_tw[1])+floatval($r_tw[2])+floatval($r_tw[3])/(float)$target*100)/2;
                                                                    }else if($indikator->id_rumus && $indikator->id_rumus == 3)
                                                                    {
                                                                        // rumus tahun
                                                                        $persen_tw1 = floatval($r_tw[1])/(float)$target*100;
                                                                        $persen_tw2 = floatval($r_tw[1])+floatval($r_tw[2])/(float)$target*100;
                                                                        $persen_tw3 = floatval($r_tw[1])+floatval($r_tw[2])+floatval($r_tw[3])/(float)$target*100;
                                                                    }
                                                                }
                                                                if(floatval($target_perubahan) != 0)
                                                                {
                                                                    if($indikator->id_rumus && $indikator->id_rumus == 1)
                                                                    {
                                                                        // rumus triwulan
                                                                        $persen_perubahan = (array_sum($r_tw)/(float)$target_perubahan*100)/4;
                                                                    }else if($indikator->id_rumus && $indikator->id_rumus == 2)
                                                                    {
                                                                        // rumus semester
                                                                        $persen_perubahan = (array_sum($r_tw)/(float)$target_perubahan*100)/2;
                                                                    }else if($indikator->id_rumus && $indikator->id_rumus == 3)
                                                                    {
                                                                        // rumus tahun
                                                                        $persen_perubahan = array_sum($r_tw)/(float)$target_perubahan*100;
                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                        <div class="capaian mt-3">
                                                            {{-- capaian triwulan 1 --}}
                                                            <div class="capaian-bar capaian-tw1">
                                                                <div
                                                                    id="capaianProg-tw1-{{$indikator->id_indikator_program}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw1}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenProg-tw1-{{$indikator->id_indikator_program}}">
                                                                            {{number_format($persen_tw1,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 2 --}}
                                                            <div class="capaian-bar capaian-tw2">
                                                                <div
                                                                    id="capaianProg-tw2-{{$indikator->id_indikator_program}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw2}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenProg-tw2-{{$indikator->id_indikator_program}}">
                                                                            {{number_format($persen_tw2,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 3 --}}
                                                            <div class="capaian-bar capaian-tw3">
                                                                <div
                                                                    id="capaianProg-tw3-{{$indikator->id_indikator_program}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw3}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenProg-tw3-{{$indikator->id_indikator_program}}">
                                                                            {{number_format($persen_tw3,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 4 --}}
                                                            <div class="capaian-bar capaian-tw4">
                                                                <div
                                                                    id="capaianProg-tw4-{{$indikator->id_indikator_program}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_perubahan}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenProg-tw4-{{$indikator->id_indikator_program}}">
                                                                            {{number_format($persen_perubahan,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-3'> {{-- teks tercapai atau belum tercapai --}}
                                                        <h6>Status</h6>
                                                        <div class='fw-bold'>
                                                            @php
                                                                $status_tw1 = '-';
                                                                $status_tw2 = '-';
                                                                $status_tw3 = '-';
                                                                $status_tw4 = '-';
                                                                if($indikator->id_rumus && ($indikator->id_rumus == 1 || $indikator->id_rumus == 6))
                                                                {
                                                                    // jika rumus positif
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else
                                                                    {
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else
                                                                    {
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else
                                                                    {
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else
                                                                    {
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }else if($indikator->id_rumus && ($indikator->id_rumus == 2 || $indikator->id_rumus == 7))
                                                                {
                                                                    // jika rumus negatif                                                                    
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else{
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else{
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else{
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else{
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }
                                                                if(
                                                                    array_key_exists(0,$indikator->target_realisasi_i_k_p)
                                                                    && ctype_alpha($indikator->target_realisasi_i_k_p[0]->target)
                                                                )
                                                                {
                                                                    $status_tw1 = '-';
                                                                    $status_tw2 = '-';
                                                                    $status_tw3 = '-';
                                                                    $status_tw4 = '-';
                                                                }
                                                            @endphp
                                                            <span id='badge-sub-kegiatan-progress'
                                                                    class='badge bg-badge'
                                                            >
                                                                <b class="capaian-tw1" style="color:black; font-size: 14px">
                                                                    {{$status_tw1}}
                                                                </b>
                                                                <b class="capaian-tw2" style="color:black; font-size: 14px">
                                                                    {{$status_tw2}}
                                                                </b>
                                                                <b class="capaian-tw3" style="color:black; font-size: 14px">
                                                                    {{$status_tw3}}
                                                                </b>
                                                                <b class="capaian-tw4" style="color:black; font-size: 14px">
                                                                    {{$status_tw4}}
                                                                </b>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}

                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    @if(!is_null($indikator->target_realisasi_i_k_p) && count($indikator->target_realisasi_i_k_p) > 0)
                                                        
                                                        @for($i=1;$i<=4;$i++)
                                                            @php 
                                                                $tw = array(
                                                                    1 => '',
                                                                    2 => '',
                                                                    3 => '',
                                                                    4 => ''
                                                                );
                                                                $disabled = 'disabled';
                                                                foreach($indikator->target_realisasi_i_k_p as $tr)
                                                                {
                                                                    if($tr->triwulan == 1)
                                                                    {
                                                                        $tw[1] = $tr->realisasi;
                                                                    }else if($tr->triwulan == 2)
                                                                    {
                                                                        $tw[2] = $tr->realisasi;
                                                                    }else if($tr->triwulan == 3)
                                                                    {
                                                                        $tw[3] = $tr->realisasi;
                                                                    }else if($tr->triwulan == 4)
                                                                    {
                                                                        $tw[4] = $tr->realisasi;
                                                                    }
                                                                }

                                                                if($i == Session::get('triwulan') || Auth::user()->id_role == 1) // set disabled input realisasi
                                                                {
                                                                    $disabled = '';
                                                                }
                                                            @endphp
                                                        <div class='col-md-3 realisasiTW{{$i}}'>
                                                            <div class='form-floating'>
                                                                <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                                <br/>
                                                                <label class="labelTriwulan">
                                                                    Triwulan {{$i}}
                                                                </label><br/> 
                                                                <input type='text' class='form-control' 
                                                                    name='t_{{$i}}[]' 
                                                                    placeholder='Triwulan {{$i}}' 
                                                                    value ='{{$tw[$i]}}' 
                                                                    {{$disabled}}
                                                                >
                                                            </div>
                                                        </div>
                                                        @endfor
                                                    @else
                                                        @for($i=1;$i<=4;$i++)
                                                            @php 
                                                            $tw = array(
                                                                1 => '',
                                                                2 => '',
                                                                3 => '',
                                                                4 => ''
                                                            );
                                                            $disabled = 'disabled';
                                                            foreach($indikator->target_realisasi_i_k_p as $tr)
                                                            {
                                                                if($tr->triwulan == 1)
                                                                {
                                                                    $tw[1] = $tr->realisasi;
                                                                }else if($tr->triwulan == 2)
                                                                {
                                                                    $tw[2] = $tr->realisasi;
                                                                }else if($tr->triwulan == 3)
                                                                {
                                                                    $tw[3] = $tr->realisasi;
                                                                }else if($tr->triwulan == 4)
                                                                {
                                                                    $tw[4] = $tr->realisasi;
                                                                }
                                                            }

                                                            if($i == Session::get('triwulan')) // set disabled input realisasi
                                                            {
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                            <div class='col-md-3 realisasiTW{{$i}}'>
                                                                <div class='form-floating'>
                                                                    <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                                    <br/>
                                                                    <label class="labelTriwulan">
                                                                        Triwulan {{$i}}             
                                                                    </label><br/> 
                                                                    <input type='text' class='form-control' 
                                                                        id = 't_{{$i}}_prog_{{$indikator->id_indikator_program}}'
                                                                        name='t_{{$i}}[]' 
                                                                        placeholder='Triwulan {{$i}}' 
                                                                        value ='{{$tw[$i]}}' 
                                                                        {{$disabled}}
                                                                    >
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @endif
                                                    <input type='hidden' name='id_indikator_program[]' value='{{$indikator->id_indikator_program}}'/>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                                <script>
                                                    // setting awal
                                                    $('.editIndPRG{{$indikator->id_indikator_program}}').hide();
                                                    $('.btnAksiUbahIndPRG_{{$indikator->id_indikator_program}}').hide();
                                                    $('.btnAksiHapusIndPRG_{{$indikator->id_indikator_program}}').hide();
                                                </script>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>{{-- tombol submit form realisasinya --}}
                                    <tr>
                                        <td>                                              
                                            <div id="messageProgSukses{{$prg->id_program}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-success">
                                                    <b>Data berhasil disimpan !</b>
                                                </p>
                                            </div>     
                                            <div id="messageProgGagal{{$prg->id_program}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-danger">
                                                    <b>Data ada yang gagal disimpan !</b>
                                                </p>
                                            </div> 
                                            <div class='text-center'>
                                            @php 
                                                $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                                $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                                $sekarang  = date("Y-m-d H:i:s");
                                                date_default_timezone_set("Asia/Jakarta");
                                            @endphp
                                            @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                                <button id="btnSimpanIndProg{{$prg->id_program}}" type='submit' class='btn btn-primary btn-sm'>
                                                    Simpan
                                                </button>
                                                <i class="fa fa-spinner" id="loadingProg{{$prg->id_program}}"></i>
                                            @else
                                                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                    <button id="btnSimpanIndProg{{$prg->id_program}}" type='submit' class='btn btn-primary btn-sm'>
                                                        Simpan
                                                    </button>
                                                    <i class="fa fa-spinner" id="loadingProg{{$prg->id_program}}"></i>                                                
                                                @else
                                                    <p class="alert alert-warning">
                                                        <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                                    </p>                                                
                                                @endif
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            @endif
                        </table>{{-- (akhir) table --}}
                    </div>{{-- (akhir) div table --}}         
                    <script>
                        //Submit Form Realisasi Indikator Program
                        $("#messageProgSukses{{$prg->id_program}}").hide();
                        $("#messageProgGagal{{$prg->id_program}}").hide();
                        $("#loadingProg{{$prg->id_program}}").hide();

                        $( "#formIndProg{{$prg->id_program}}" ).on( "submit", function(e) {
                            var dataString = $(this).serialize();
                            $("#btnSimpanIndProg{{$prg->id_program}}").hide();
                            $("#loadingProg{{$prg->id_program}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('monev/renja/input/realisasi/IndProgram') }}",
                                data: dataString,
                                success: function (hasil) {
                                    console.log(hasil);
                                    var cek = true;
                                    $.each(hasil, function( index, value ) {
                                        if(value.status != 200)
                                        {
                                            cek = false;
                                        }
                                    });
                                    if(cek == true)
                                    {
                                        $("#messageProgSukses{{$prg->id_program}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageProgSukses{{$prg->id_program}}").slideUp(500);
                                        });
                                        reloadIndProg("{{$prg->id_program}}","simpan");
                                    }else{
                                        $("#messageProgGagal{{$prg->id_program}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageProgGagal{{$prg->id_program}}").slideUp(500);
                                        });
                                    }
                                    $("#btnSimpanIndProg{{$prg->id_program}}").show();
                                    $("#loadingProg{{$prg->id_program}}").hide();
                                },
                                error: function () {
                                    $("#messageProgGagal{{$prg->id_program}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageProgGagal{{$prg->id_program}}").slideUp(500);
                                    });
                                    $("#btnSimpanIndProg{{$prg->id_program}}").show();
                                    $("#loadingProg{{$prg->id_program}}").hide();

                                }
                            });
                            e.preventDefault();
                            updateStatProg();
                        });
                    </script>
                </form>
            </div>
            @endforeach
        </div>
    </div>
</div>
<script>    
    // set status input realisasi per program
    async function setStatInputRProg(twl)
    {
        var listItems = $("#v-pills-tab-prog li");
        var cek = 0; //jumlah yang belum diisi
        var statInput = <?php echo json_encode($statProg) ?>;
        var indexStat;
        listItems.each(function(idx, li) {
            var link = $(li).find('a');
            var icon = $(li).find('i');
            indexStat = link.attr('id').replace('programitem','');
            
            if(statInput[twl][indexStat] == 2)
            {
                link.attr('class','nav-link link-success');
                icon.attr('class','fa fa-check-circle');
            }else if(statInput[twl][indexStat] == 1)
            {
                link.attr('class','nav-link link-warning');
                icon.attr('class','fa fa-spinner');
            }else{
                link.attr('class','nav-link');
                icon.attr('class','');
            }
        });
    }
    // update status input realisasi untuk semua indikator program
    async function updateStatProg()
    {
        var listItems = $("#v-pills-tab-prog li");
        var cek = 0; //jumlah yang belum diisi
        listItems.each(function(idx, li) {
            var product = $(li).find('a');
            if(product.attr('class')=='nav-link'||product.attr('class')=='nav-link link-warning'){
                cek = cek+1;
            }
        });
        if(cek == 0) //jika semua indikator sudah diisi
        {
            $('#program-tab').attr('class', 'nav-link text-success border-success');
            $('#program-tab i').attr('class', 'fa fa-check-circle');
        }else if(cek > 0 && cek < listItems.length) //jika sebagian indikator sudah diisi
        {
            $('#program-tab').attr('class', 'nav-link text-warning border-warning');
            $('#program-tab i').attr('class', 'fa fa-check-spinner');
        }else{
            $('#program-tab').attr('class', 'nav-link');
            $('#program-tab i').attr('class', '');
        }
    }

    //memuat ulang semua data indikator program 
    async function reloadIndProg(id_program, aksi)
    {
        /* 
        aksi : 
            >> tambah (tambah indikator baru)
            >> simpan (simpan realisasi indikator)
            >> ubah (ubah indikator)
            >> hapus (hapus indikator) 
        */
        var formInd = $("#formIndProg"+id_program+" tbody");        
        var token = '{{$token}}'; //token
        var urlIndProg = "{{url('/api/renja/get_inputrealindprog')}}"; //alamat api realisasi indikator program
        var listProg = $("#l_prog");
        var listIndProg = $("#l_indProg");
        var tahun = "{{session()->get('tahun_monev')}}"; //tahun monev
        var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
        var opd = '';
        var userRole = "{{ Auth::user()->id_role }}";
        var id_indikator_program;
        var indikator_program;
        var id_renja;
        var target;
        var target_perubahan;
        var t_1;
        var t_2;
        var t_3;
        var t_4;
        var index;
        var satuan;
        var jmlIndikator;
        
        if(userRole == 1)
        {
            id_skpd = "{{Session::get('pilih_id_skpd')}}";
        }else{
            id_skpd = "{{Auth::user()->viewuser->id_skpd}}";
        }

        $.ajax({
            type        :   "GET",
            url         :   urlIndProg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'triwulan':twl,
                                'tahun':tahun,
                                'opd':id_skpd,
                                'prog':id_program
                            },
            success     :   function (responses) {
                                var jmlTotalProgDiisi = 0;
                                var jmlProg = responses.data.length;
                                $.each(responses.data, function (index1, item1) {
                                    jmlIndikator = item1.indikator.length; // tampung jumlah indikator
                                    var jmlRIndDiisi = 0;
                                    if(jmlIndikator == 0)//indikator kosong
                                    {
                                        $("#btnSimpanRIndProg_"+id_program).html(
                                            "<p class='alert alert-info'>"+
                                            "    Indikator Tidak Tersedia"+
                                            "</p>"
                                        );
                                        
                                        $("#programitem"+item1.id_program).attr('class', 'nav-link');
                                        $("#programitem"+item1.id_program).html(
                                            "<i class=''></i> "+
                                            "<span>"+item1.nama_program+"</span>"
                                        );
                                    }else{ //indikator ada
                                        $.each(item1.indikator, function (index2, item2) {
                                            if(item2.target_realisasi_i_k_p != null )
                                            {
                                                if(item2.target_realisasi_i_k_p[0] != null)
                                                {
                                                    t_1 = item2.target_realisasi_i_k_p[0].realisasi;
                                                    jmlRIndDiisi = jmlRIndDiisi + 1; //catat realisasi yang diisi
                                                }else{
                                                    t_1 = '';
                                                }
                                                if(item2.target_realisasi_i_k_p[1] != null)
                                                {
                                                    t_2 = item2.target_realisasi_i_k_p[1].realisasi;
                                                }else{
                                                    t_2 = '';
                                                }
                                                if(item2.target_realisasi_i_k_p[2] != null)
                                                {
                                                    t_3 = item2.target_realisasi_i_k_p[2].realisasi;
                                                }else{
                                                    t_3 = '';
                                                }
                                                if(item2.target_realisasi_i_k_p[3] != null)
                                                {
                                                    t_4 = item2.target_realisasi_i_k_p[3].realisasi;
                                                }else{
                                                    t_4 = '';
                                                }
                                            }else{
                                                t_1 = '';
                                                t_2 = '';
                                                t_3 = '';
                                                t_4 = '';
                                            }
                                            $('#namaIndProg_'+item2.id_indikator_program).html(item2.indikator_program);
                                            $('#targetIndProg_'+item2.id_indikator_program+' .target').html(item2.target);
                                            $('#targetIndProg_'+item2.id_indikator_program+' .target_perubahan').html(item2.target);
                                            $('#satuanIndProg_'+item2.id_indikator_program).html(item2.satuan);
                                            $('#t_1_'+item2.id_indikator_program).val(t_1);
                                            $('#t_2_'+item2.id_indikator_program).val(t_2);
                                            $('#t_3_'+item2.id_indikator_program).val(t_3);
                                            $('#t_4_'+item2.id_indikator_program).val(t_4);
                                            
                                            // if(aksi == 'tambah')
                                            // {
                                            //     index = index2;
                                            //     id_indikator_program = item2.id_indikator_program;
                                            //     indikator_program = item2.indikator_program;
                                            //     id_renja = item1.id_renja;
                                            //     target = item2.target;
                                            //     target_perubahan = item2.target;
                                            //     satuan = item2.satuan;
                                            // }
                                        });
                                        // setting ceklis status jumlah realisasi yang sudah di input
                                        if(jmlRIndDiisi == jmlIndikator)
                                        {
                                            $("#programitem"+item1.id_program).attr('class', 'nav-link link-success');
                                            $("#programitem"+item1.id_program).html(
                                                "<i class='fa fa-check-circle'></i> "+
                                                "<span>"+item1.nama_program+"</span>"
                                            );
                                            jmlTotalProgDiisi = jmlTotalProgDiisi + 1;
                                        }else if(jmlRIndDiisi > 0 && jmlRIndDiisi < jmlIndikator)
                                        {
                                            $("#programitem"+item1.id_program).attr('class', 'nav-link link-warning');
                                            $("#programitem"+item1.id_program).html(
                                                "<i class='fa fa-spinner'></i> "+
                                                "<span>"+item1.nama_program+"</span>"
                                            );
                                        }else{
                                            $("#programitem"+item1.id_program).attr('class', 'nav-link');
                                            $("#programitem"+item1.id_program).html(
                                                "<i></i> "+
                                                "<span>"+item1.nama_program+"</span>"
                                            );
                                        }
                                        // if(aksi == 'tambah'){
                                        //     formInd.append(
                                        //             "<tr id='detailIndProg_"+id_indikator_program+"'>"+
                                        //             "    <td>"+
                                        //             "        <h5 class='mb-0'>Indikator #"+(index+1)+"</h5>"+
                                        //             "        <div class='border border-primary shadow-sm p-3 mb-3'>"+
                                        //             "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                        //             "                <div class='col-lg-12'>"+ // nama indikator sub kegiatan
                                        //             "                    <h6>Nama Indikator</h6>"+
                                        //             "                    <div class='fw-bold'>"+indikator_subkegiatan+"</div>"+
                                        //             "                    <input class='form-control editIndSKG"+id_indikator_program+"' type='text' name='indikator_subkegiatan_"+id_indikator_program+"' value='"+indikator_subkegiatan+"'/>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-lg-2'>"+ // target indikator sub kegiatan
                                        //             "                    <h6>Target</h6>"+
                                        //             "                    <div class='fw-bold'>"+
                                        //             "                        <span class='target'>"+String(target)+"</span>"+
                                        //             "                        <span class='target_perubahan'>"+target_perubahan+"</span>"+
                                        //             "                    </div>"+
                                        //             "                    <input class='form-control editIndSKG"+id_indikator_program+"' type='text' name='target_"+id_indikator_program+"' value='"+target+"'/>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-lg-2'>"+ // satuan indikator sub kegiatan
                                        //             "                    <h6>Satuan</h6>"+
                                        //             "                    <div class='fw-bold'>"+satuan+"</div>"+
                                        //             "                    <input class='form-control editIndSKG"+id_indikator_program+"' type='text' name='satuan_"+id_indikator_program+"' value='"+satuan+"'/>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-lg-5'>"+ // progress
                                        //             "                    <h6>Capaian Kinerja Sub Kegiatan</h6>"+
                                        //             "                    <div class='progress mt-3'>"+
                                        //             "                        <div id='progressprogress'"+
                                        //             "                            class='progress-bar' role='progressbar'"+
                                        //             "                            style='width: 0%'"+
                                        //             "                            aria-valuenow='progress' aria-valuemin='0'"+
                                        //             "                            aria-valuemax='100'>"+
                                        //             "                            - %"+
                                        //             "                        </div>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-lg-3'>"+ // teks tercapai atau belum tercapai
                                        //             "                    <h6>Status</h6>"+
                                        //             "                    <div class='fw-bold'>"+
                                        //             "                        <span id='badge-sub-kegiatan-progress'"+
                                        //             "                                class='badge bg-badge'"+
                                        //             "                        >"+
                                        //             "                        -"+
                                        //             "                        </span>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "            </div>"+ // (akhir) row g-3 mb-3
                                        //             "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                        //             "                    <input type='hidden' name='id_indikator_program[]' value='"+id_indikator_program+"'/>"+
                                        //             "                <div class='col-md-3 realisasiTW1'>"+ // realisasi triwulan 1
                                        //             "                    <div class='form-floating'>"+
                                        //             "                       <input type='hidden' name='triwulan1[]' value='1'>"+
                                        //             "                        <br>"+
                                        //             "                        <label>"+
                                        //             "                            Triwulan 1"+
                                        //             "                        </label><br> "+
                                        //             "                        <input type='text' class='form-control' id='t_1"+id_indikator_program+"' name='t_1[]' placeholder='Triwulan 1' value='"+t_1+"'>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-md-3 realisasiTW2' style='display: none;'>"+ // realisasi triwulan 2
                                        //             "                    <div class='form-floating'>"+
                                        //             "                        <input type='hidden' name='triwulan2[]' value='2'>"+
                                        //             "                        <br>"+
                                        //             "                        <label>"+
                                        //             "                            Triwulan 2"+
                                        //             "                        </label><br> "+
                                        //             "                        <input type='text' class='form-control' id='t_2"+id_indikator_program+"' name='t_2[]' placeholder='Triwulan 2' value='"+t_2+"' disabled=''>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-md-3 realisasiTW3' style='display: none;'>"+ // realisasi triwulan 3
                                        //             "                    <div class='form-floating'>"+
                                        //             "                        <input type='hidden' name='triwulan3[]' value='3'>"+
                                        //             "                        <br>"+
                                        //             "                        <label>"+
                                        //             "                            Triwulan 3"+
                                        //             "                        </label><br> "+
                                        //             "                        <input type='text' class='form-control' id='t_3"+id_indikator_program+"' name='t_3[]' placeholder='Triwulan 3' value='"+t_3+"' disabled=''>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "                <div class='col-md-3 realisasiTW4' style='display: none;'>"+ // realisasi triwulan 4
                                        //             "                    <div class='form-floating'>"+
                                        //             "                        <input type='hidden' name='triwulan4[]' value='4'>"+
                                        //             "                        <br>"+
                                        //             "                        <label>"+
                                        //             "                            Triwulan 4"+
                                        //             "                        </label><br> "+
                                        //             "                        <input type='text' class='form-control' id='t_4"+id_indikator_program+"' name='t_4[]' placeholder='Triwulan 4' value='"+t_4+"' disabled=''>"+
                                        //             "                    </div>"+
                                        //             "                </div>"+
                                        //             "            </div>"+ // (akhir) row g-3 mb-3
                                        //             "            <div class='row g-3 mb-3 btnAksiProg"+id_program+"'>"+ // (awal) row g-3 mb-3
                                        //             "                <div class='text-right'>"+                                       
                                        //                                 // tombol untuk edit indikator
                                        //             "                    <button type='button' class='btn btn-sm btn-primary' id='btnUbahIndPRG_"+id_indikator_program+"'>"+
                                        //             "                        Ubah"+
                                        //             "                    </button>"+                                         
                                        //             "                    <button type='button' class='btn btn-sm btn-secondary btnAksiUbahIndPRG_"+id_indikator_program+"' id='btnBatalUbahIndPRG_"+id_indikator_program+"'>"+
                                        //             "                        Batal"+
                                        //             "                    </button>"+                                          
                                        //             "                    <button type='button' class='btn btn-sm btn-primary btnAksiUbahIndPRG_"+id_indikator_program+"' onclick='ubahIndProg("+id_indikator_program+","+id_program+","+id_renja+")'>"+
                                        //             "                        Simpan Perubahan"+
                                        //             "                    </button>"+
                                        //                                 // tombol untuk hapus indikator
                                        //             "                    <button type='button' class='btn btn-sm btn-danger' id='btnHapusIndPRG_"+id_indikator_program+"'>"+
                                        //             "                        Hapus"+
                                        //             "                    </button>"+
                                        //             "                    <button type='button' class='btn btn-sm btn-secondary btnAksiHapusIndPRG_"+id_indikator_program+"' id='btnBatalHapusIndPRG_"+id_indikator_program+"'>"+
                                        //             "                        Batal"+
                                        //             "                    </button>"+
                                        //             "                    <button type='button' class='btn btn-sm btn-danger btnAksiHapusIndPRG_"+id_indikator_program+"' onclick='hapusIndProg("+id_indikator_program+","+id_program+")'>"+
                                        //             "                        Hapus Indikator"+
                                        //             "                    </button>"+
                                        //             "                </div>"+
                                        //             "            </div>"+ // (akhir) row g-3 mb-3
                                        //             "       </div>"+
                                        //             "    </td>"+
                                        //             "</tr>"
                                        //     );
                                            
                                        
                                        //     var bwi_mulai = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['mulai']))}}";
                                        //     var bwi_akhir = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['akhir']))}}";
                                        //     var sekarang = "{{date('Y-m-d H:i:s')}}";
                                        //     var user_id_role = "{{Auth::user()->id_role}}";
                                        //     if(sekarang >= bwi_mulai && sekarang <= bwi_akhir){
                                        //         if(user_id_role != 13){
                                        //             $("#btnSimpanRIndProg_"+id_program).html(
                                        //                 "<button id='btnSimpanIndProg"+id_program+"' type='submit' class='btn btn-primary btn-sm'>"+
                                        //                 "    Simpan"+
                                        //                 "</button>"+
                                        //                 "<i class='fa fa-spinner' id='loadingProg"+id_program+"'></i>"
                                        //             );
                                        //         }
                                        //     }else{
                                        //         $("#btnSimpanRIndProg_"+id_program).html(
                                        //             "<p class='alert alert-warning'>"+
                                        //             "    <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>"+
                                        //             "</p>"
                                        //         );
                                        //     }
                                            
                                        //     //Untuk ubah indikator sub kegiatan
                                        //     $('.editIndPRG'+id_indikator_program).hide();
                                        //     $('.btnAksiUbahIndPRG_'+id_indikator_program).hide();
                                        //     $('.btnAksiHapusIndPRG_'+id_indikator_program).hide();
                                        //     //Submit Form Realisasi Indikator Sub Kegiatan
                                        //     $("#messageProgSukses"+id_program).hide();
                                        //     $("#messageProgGagal"+id_program).hide();
                                        //     $("#loadingProg"+id_program).hide();
                                        //     //Submit Form Tambah Indikator Sub Kegiatan
                                        //     $("#messageAddIndProgSukses"+id_program).hide();
                                        //     $("#messageAddIndProgGagal"+id_program).hide();
                                        //     $("#loadingAddIndProg"+id_program).hide();
                                            
                                        //     showHide(twl);
                                        //     // aksi tombol Ubah 
                                        //     $('#btnUbahIndPRG_'+id_indikator_program).on( "click", function() {
                                        //         // yang tampil
                                        //         $('.editIndPRG'+id_indikator_program).show();
                                        //         $('.btnAksiUbahIndPRG_'+id_indikator_program).show();
                                        //         // yang ditutup
                                        //         $('#btnUbahIndPRG_'+id_indikator_program).hide();
                                        //         $('#btnHapusIndPRG_'+id_indikator_program).hide();
                                        //     });
                                        //     // aksi tombol Batal Ubah
                                        //     $('#btnBatalUbahIndPRG_'+id_indikator_program).on("click", function () {
                                        //         // yang tampil
                                        //         $('.editIndPRG'+id_indikator_program).hide();
                                        //         $('.btnAksiUbahIndPRG_'+id_indikator_program).hide();
                                        //         // yang ditutup
                                        //         $('#btnUbahIndPRG_'+id_indikator_program).show();
                                        //         $('#btnHapusIndPRG_'+id_indikator_program).show();
                                        //     });
                                        //     // aksi tombol Hapus
                                        //     $('#btnHapusIndPRG_'+id_indikator_program).on("click", function () {
                                        //         // yang tampil
                                        //         $('.btnAksiHapusIndPRG_'+id_indikator_program).show();
                                        //         // yang ditutup
                                        //         $('#btnUbahIndPRG_'+id_indikator_program).hide();
                                        //         $('#btnHapusIndPRG_'+id_indikator_program).hide();
                                        //     });
                                        //     // aksi tombol Batal Hapus
                                        //     $('#btnBatalHapusIndPRG_'+id_indikator_program).on("click", function () {
                                        //         // yang tampil
                                        //         $('.btnAksiHapusIndPRG_'+id_indikator_program).hide();
                                        //         // yang ditutup
                                        //         $('#btnUbahIndPRG_'+id_indikator_program).show();
                                        //         $('#btnHapusIndPRG_'+id_indikator_program).show();
                                        //     });
                                        // }
                                        updateStatProg();  //update status semua input realisasi indikator
                                    }                                    
                                });
                            }    
        });
    }
</script>
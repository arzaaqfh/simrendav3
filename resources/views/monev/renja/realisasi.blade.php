@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            ::-webkit-input-placeholder {
                font-style: italic;
            }
            :-moz-placeholder {
                font-style: italic;  
            }
            ::-moz-placeholder {
                font-style: italic;  
            }
            :-ms-input-placeholder {  
                font-style: italic; 
            }
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }    
            .capaian {
                width: 100%;
                max-width: 600px;
                padding: 0 20px;
            }
            .capaian-bar {
                height: 14px;
                background: #e6e3e3;
                border-radius: 3px;
            }
            .capaian-per {
                height: 14px;
                color: #ffffff;
                background: rgb(10, 206, 255);
                border-radius: 3px;
                position: relative;
                padding-bottom: 1px;
                animation: fillBars 2.5s 1;
            }
            .capaian-per::before {
                content: attr(per);
                position: absolute;
                /* background: #f1f1f1; */
                border-radius: 4px;
                font-size: 12px;
                /* top: 25px; */
                right: 0;
                /* transform: translateX(50%); */
            }
            .capaian-per::after {
                content: "";
                position: absolute;
                /* width: 10px; */
                /* height: 10px; */
                /* background: #f1f1f1; */
                /* top: 20px; */
                /* right: 0; */
                /* transform: translateX(50%) rotate(45deg); */
                /* border-radius: 2px; */
            }
            @keyframes fillBars {
                from{
                    width: 0;
                }
                to{
                    width: 100%;
                }
            }
            @media screen and (max-width: 700px) {
                .labelTriwulan {font-size:4px}
            }

            @media screen and (min-device-width: 701px) and (max-device-width: 1400px) { 
                .labelTriwulan {font-size:10px}
            }

            @media only screen and (min-device-width: 1400px) {
                .labelTriwulan {font-size:12px}
            }
        </style>
        <div class="main_content_iner ">
            <div class="container-fluid p-0">
                <div class="row ">
                    <div class="col-12">
                        <div class="white_card mb_30 ">
                            <div class="white_card_header">
                                <div class="white_box_tittle list_header">
                                    <h4><b>Realisasi RENJA</b></h4>
                                </div>
                                <div class="bulder_tab_wrapper">
                                    <ul class="nav" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="subkegiatan-tab" data-bs-toggle="tab" href="#Subkegiatan" role="tab" aria-controls="Subkegiatan" aria-selected="true">
                                                <i class = ""></i> Sub Kegiatan
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="kegiatan-tab" data-bs-toggle="tab" href="#Kegiatan" role="tab" aria-controls="Kegiatan" aria-selected="false">
                                                <i class = ""></i> Kegiatan
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="program-tab" data-bs-toggle="tab" href="#Program" role="tab" aria-controls="Program" aria-selected="false">
                                                <i class = ""></i> Program
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="analisis-faktor-subKegiatan-tab" data-bs-toggle="tab" href="#AFSubKeg" role="tab" aria-controls="AFSubKeg" aria-selected="false">
                                                Analisis Faktor Sub Kegiatan
                                            </a>
                                        </li>
                                        {{-- @if(Auth::user()->id_role == 13 || Auth::user()->id_role == 1)
                                        <li class="nav-item">
                                            <a class="nav-link" id="analisisfaktor-tab" data-bs-toggle="tab" href="#AnalisisFaktor" role="tab" aria-controls="AnalisisFaktor" aria-selected="false">Analisis Faktor</a>
                                        </li>
                                        @endif --}}
                                    </ul>
                                </div>
                            </div>
                            <div class="white_card_body">
                                @php
                                    $tabActive = session()->get('tab_renja');
                                    $tabSubKeg = '';
                                    $tabKeg = '';
                                    $tabProg = '';
                                    $tabAnlFtr = '';
                                    if($tabActive == 'subkeg')
                                    {
                                        $tabSubKeg = 'show active';
                                    }else if($tabActive == 'keg')
                                    {
                                        $tabKeg = 'show active';
                                    }else if($tabActive == 'prog')
                                    {
                                        $tabProg = 'show active';
                                    }else if($tabActive == 'af')
                                    {
                                        $tabAnlFtr = 'show active';
                                    }
                                @endphp                                
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade {{$tabSubKeg}}" id="Subkegiatan" role="tabpanel" aria-labelledby="subkegiatan-tab">
                                        @include('monev.renja.sub-kegiatan')
                                    </div>
                                    <div class="tab-pane fade {{$tabKeg}}" id="Kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                        @include('monev.renja.kegiatan')                                        
                                    </div>
                                    <div class="tab-pane fade {{$tabProg}}" id="Program" role="tabpanel" aria-labelledby="program-tab">
                                        @include('monev.renja.program')                                        
                                    </div>
                                    <div class="tab-pane fade" id="AFSubKeg" role="tabpanel" aria-labelledby="analisis-faktor-subKegiatan-tab">
                                        @include('monev.renja.analisisSubKegiatan')                                        
                                    </div>
                                    {{-- @if(Auth::user()->id_role == 13 || Auth::user()->id_role == 1)
                                    <div class="tab-pane fade {{$tabAnlFtr}}" id="AnalisisFaktor" role="tabpanel" aria-labelledby="analisisfaktor-tab">
                                        @include('monev.renja.analisis-faktor')                                        
                                    </div>
                                    @endif --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
            async function showHide(twl) // set kondisi sesuai triwulan yang dipilih
            {
                if(twl == 4)
                {
                    $(".target_perubahan").show();
                    $(".target").hide();
                    $(".realisasiTW1").show();
                    $(".realisasiTW2").show();
                    $(".realisasiTW3").show();
                    $(".realisasiTW4").show();
                    $(".capaian-tw1").hide();
                    $(".capaian-tw2").hide();
                    $(".capaian-tw3").hide();
                    $(".capaian-tw4").show();
                    $(".link-excel-tw1").hide();
                    $(".link-excel-tw2").hide();
                    $(".link-excel-tw3").hide();
                    $(".link-excel-tw4").show();
                    $(".link-pdf-tw1").hide();
                    $(".link-pdf-tw2").hide();
                    $(".link-pdf-tw3").hide();
                    $(".link-pdf-tw4").show();
                }else if(twl == 3)
                {
                    $(".target_perubahan").hide();
                    $(".target").show();
                    $(".realisasiTW1").show();
                    $(".realisasiTW2").show();
                    $(".realisasiTW3").show();
                    $(".realisasiTW4").hide();
                    $(".capaian-tw1").hide();
                    $(".capaian-tw2").hide();
                    $(".capaian-tw3").show();
                    $(".capaian-tw4").hide();
                    $(".link-excel-tw1").hide();
                    $(".link-excel-tw2").hide();
                    $(".link-excel-tw3").show();
                    $(".link-excel-tw4").hide();
                    $(".link-pdf-tw1").hide();
                    $(".link-pdf-tw2").hide();
                    $(".link-pdf-tw3").show();
                    $(".link-pdf-tw4").hide();

                }else if(twl == 2)
                {
                    $(".target_perubahan").hide();
                    $(".target").show();
                    $(".realisasiTW1").show();
                    $(".realisasiTW2").show();
                    $(".realisasiTW3").hide();
                    $(".realisasiTW4").hide();
                    $(".capaian-tw1").hide();
                    $(".capaian-tw2").show();
                    $(".capaian-tw3").hide();
                    $(".capaian-tw4").hide();
                    $(".link-excel-tw1").hide();
                    $(".link-excel-tw2").show();
                    $(".link-excel-tw3").hide();
                    $(".link-excel-tw4").hide();
                    $(".link-pdf-tw1").hide();
                    $(".link-pdf-tw2").show();
                    $(".link-pdf-tw3").hide();
                    $(".link-pdf-tw4").hide();
                }else if(twl == 1)
                {
                    $(".target_perubahan").hide();
                    $(".target").show();
                    $(".realisasiTW1").show();
                    $(".realisasiTW2").hide();
                    $(".realisasiTW3").hide();
                    $(".realisasiTW4").hide();
                    $(".capaian-tw1").show();
                    $(".capaian-tw2").hide();
                    $(".capaian-tw3").hide();
                    $(".capaian-tw4").hide();
                    $(".link-excel-tw1").show();
                    $(".link-excel-tw2").hide();
                    $(".link-excel-tw3").hide();
                    $(".link-excel-tw4").hide();
                    $(".link-pdf-tw1").show();
                    $(".link-pdf-tw2").hide();
                    $(".link-pdf-tw3").hide();
                    $(".link-pdf-tw4").hide();
                }else{
                    $(".target_perubahan").hide();
                    $(".target").show();
                }
            }

            $("#twl_monev").on('change', function() {
                twl = this.value;
                showHide(twl);
                setStatInputRSubKeg(twl); //set status input realisasi per sub kegiatan sesuai triwulan
                updateStatSubKeg();  //update status semua input realisasi indikator
                setStatInputRKeg(twl); //set status input realisasi per kegiatan sesuai triwulan
                updateStatKeg();  //update status semua input realisasi indikator
                setStatInputRProg(twl); //set status input realisasi per program sesuai triwulan
                updateStatProg();  //update status semua input realisasi indikator
                setStatInputAnlSubKeg(twl); //set status input analisis faktor per sub kegiatan sesuai triwulan
                setInputValAnlSKG(twl); //set nilai input analisisi faktor per sub kegiatan sesuai triwulan
            }); //pilih triwulan
            showHide(twl);
            setStatInputRSubKeg(twl); //set status input realisasi per sub kegiatan
            setStatInputRKeg(twl); //set status input realisasi per kegiatan sesuai triwulan
            setStatInputRProg(twl); //set status input realisasi per program sesuai triwulan
            setStatInputAnlSubKeg(twl); //set status input analisis faktor per sub kegiatan sesuai triwulan
            setInputValAnlSKG(twl); //set nilai input analisisi faktor per sub kegiatan sesuai triwulan
        </script>
        <!-- Footer -->
        @include('layouts.footer')
@endsection
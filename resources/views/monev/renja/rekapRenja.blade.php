@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')
    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <div class="row ">
                <div class="col-12">
                    <div class="white_card mb_30 ">
                        <div class="white_card_body">
                            <br/>
                            <div class="row">
                                <h4><b>Rekap RENJA</b></h4>
                            </div>
                            <div class="row">
                                <div class="col col-md-12">
                                    <style>
                                        .menu_cetak, .menu_cetak-content a {
                                            position: relative;
                                            display: inline-block;
                                            text-decoration: none;
                                        }
                                        
                                        .menu_cetak-content {
                                            display: none;
                                            position: absolute;
                                            background-color: #f9f9f9;
                                            min-width: 270px;
                                            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                                            padding: 12px 16px;
                                            z-index: 1;
                                            left: -150%;
                                        }
                                        
                                        .menu_cetak:hover .menu_cetak-content {
                                            display: block;
                                        }
                                        .link-excel, .link-pdf {
                                            width: 100%;
                                            padding: 3px;
                                        }
                                        .link-excel:hover {
                                            background-color: green;
                                            color:white;
                                        }
                                        .link-pdf:hover {
                                            background-color: rgb(167, 0, 0);
                                            color:white;
                                        }
                                    </style>                                        
                                    <div class="float-right">
                                        <div class="menu_cetak">
                                            <button id="menu_cetak_excel" class="btn btn-success">
                                                <i class="fa fa-download" aria-hidden="true"></i> EXCEL
                                            </button>                                            
                                            <div class="menu_cetak-content">
                                                @for($tw=1;$tw<=4;$tw++)
                                                    <div class="link-excel-tw{{$tw}}">                                                
                                                        <a class="link-excel" href="{{ url('monev/renja/cetak/renja/excel').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Realisasi
                                                        </a>
                                                        @if(Auth::user()->id_role == 1)
                                                        <a class="link-excel" href="{{ url('monev/renja/cetak/renja/excel').'/'.$id_skpd.'/true/'.$tw }}" target="_blank">                                        
                                                            Realisasi (Semua PD)
                                                        </a>
                                                        @endif
                                                        <a class="link-excel" href="{{ url('monev/renja/cetak/renja_af_skg/excel').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Analisis Faktor (per Sub Kegiatan)
                                                        </a>
                                                        <a class="link-excel" href="{{ url('monev/renja/cetak/renja_af_bu/excel').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Analisis Faktor (per Bidang Urusan)
                                                        </a>
                                                    </div>
                                                @endfor                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <div class="menu_cetak">
                                            <button id="menu_cetak_pdf" class="btn btn-danger">
                                                <i class="fa fa-download" aria-hidden="true"></i> PDF
                                            </button>                                            
                                            <div class="menu_cetak-content">
                                                @for($tw=1;$tw<=4;$tw++)
                                                    <div class="link-pdf-tw{{$tw}}">    
                                                        <a class="link-pdf" href="{{ url('monev/renja/cetak/renja/pdf').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Realisasi
                                                        </a>
                                                        {{-- @if(Auth::user()->id_role == 1)
                                                        <a class="link-pdf" href="{{ url('monev/renja/cetak/renja/pdf').'/'.$id_skpd.'/true' }}" target="_blank">                                        
                                                            Realisasi (Semua PD)
                                                        </a>
                                                        @endif --}}
                                                        <a class="link-pdf" href="{{ url('monev/renja/cetak/renja_af_skg/pdf').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Analisis Faktor (per Sub Kegiatan)
                                                        </a>
                                                        <a class="link-pdf" href="{{ url('monev/renja/cetak/renja_af_bu/pdf').'/'.$id_skpd.'/false/'.$tw }}" target="_blank">                                        
                                                            Analisis Faktor (per Bidang Urusan)
                                                        </a>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">                                
                                <table class="table table-bordered">
                                    <thead class="text-center align-middle">
                                        <tr>
                                            <th rowspan="2">Kode</th>
                                            <th colspan="3" rowspan="2">Program / Kegiatan / Sub Kegiatan</th>
                                            <th rowspan="2">Indikator</th>
                                            <th rowspan="2">Target</th>
                                            <th rowspan="2">Satuan</th>
                                            <th colspan="4">Capaian</th>
                                            <th rowspan="2">Perangkat Daerah</th>
                                        </tr>
                                        <tr>
                                            <th>TW1</th>
                                            <th>TW2</th>
                                            <th>TW3</th>
                                            <th>TW4</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 12px;">
                                        {{-- Program --}}
                                        @foreach($program as $prg)
                                        <tr>
                                            <td rowspan="{{ count($prg->indikator)+1 }}"><b>{{ $prg->kode_program }}</b></td>
                                            <td rowspan="{{ count($prg->indikator)+1 }}" colspan="3"><b>{{ $prg->nama_program }}</b></td>
                                            @if(count($prg->indikator) == 0)
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            @endif
                                        </tr>
                                        {{-- Indikator Program --}}
                                        @foreach($prg->indikator as $ind)
                                        <tr>
                                            <td>{{ $ind->indikator_program }}</td>
                                            <td>{{ $ind->target_realisasi_i_k_p[count($ind->target_realisasi_i_k_p)-1]->target }}</td>
                                            <td>{{ $ind->satuan }}</td>
                                            <td>
                                                @if(array_key_exists(0,$ind->target_realisasi_i_k_p))
                                                {{ $ind->target_realisasi_i_k_p[0]->realisasi }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists(1,$ind->target_realisasi_i_k_p))
                                                {{ $ind->target_realisasi_i_k_p[1]->realisasi }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists(2,$ind->target_realisasi_i_k_p))
                                                {{ $ind->target_realisasi_i_k_p[2]->realisasi }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists(3,$ind->target_realisasi_i_k_p))
                                                {{ $ind->target_realisasi_i_k_p[3]->realisasi }}
                                                @endif
                                            </td>
                                            <td>{{ $prg->nama_skpd }}</td>
                                        </tr>
                                        @endforeach
                                            {{-- Kegiatan --}}
                                            @foreach($kegiatan[$prg->id_program] as $keg)
                                            <tr>
                                                <td rowspan="{{count($indKeg[$keg['id_kegiatan']])+1}}">{{ $keg['kode_kegiatan'] }}</td>
                                                <td rowspan="{{count($indKeg[$keg['id_kegiatan']])+1}}"></td>
                                                <td rowspan="{{count($indKeg[$keg['id_kegiatan']])+1}}" colspan="2">{{ $keg['nama_kegiatan'] }}</td>
                                                @if(count($indKeg[$keg['id_kegiatan']]) == 0)
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                @endif
                                            </tr>
                                            {{-- Indikator Kegiatan --}}
                                            @foreach($indKeg[$keg['id_kegiatan']] as $indKeg_)
                                            @php
                                                if(is_null($indKeg_->realisasiindikatorkeg) == 'kosong')
                                                {
                                                    $t_1_keg = '';
                                                    $t_2_keg = '';
                                                    $t_3_keg = '';
                                                    $t_4_keg = '';
                                                }else{
                                                    $t_1_keg = $indKeg_->realisasiindikatorkeg->t_1;
                                                    $t_2_keg = $indKeg_->realisasiindikatorkeg->t_2;
                                                    $t_3_keg = $indKeg_->realisasiindikatorkeg->t_3;
                                                    $t_4_keg = $indKeg_->realisasiindikatorkeg->t_4;                                                            
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{ $indKeg_->indikator_kegiatan }}</td>
                                                <td>
                                                    <label class="rekap_target">
                                                        {{ $indKeg_->target }}
                                                    </label>
                                                    <label class="rekap_target_perubahan">
                                                        {{ $indKeg_->target_perubahan }}
                                                    </label>
                                                </td>
                                                <td>{{ $indKeg_->satuan }}</td>
                                                <td>{{ $t_1_keg }}</td>
                                                <td>{{ $t_2_keg }}</td>
                                                <td>{{ $t_3_keg }}</td>
                                                <td>{{ $t_4_keg }}</td>
                                                <td>{{ $keg['nama_skpd'] }}</td>
                                            </tr>
                                            @endforeach
                                                {{-- Sub Kegiatan --}}
                                                @foreach($subKegiatan[$keg['id_kegiatan']] as $subKeg)
                                                <tr>
                                                    <td rowspan="{{ count($subKeg['indikator'])+1 }}">{{ $subKeg['kode_sub_kegiatan'] }}</td>
                                                    <td rowspan="{{ count($subKeg['indikator'])+1 }}"></td>
                                                    <td rowspan="{{ count($subKeg['indikator'])+1 }}"></td>
                                                    <td rowspan="{{ count($subKeg['indikator'])+1 }}">{{ $subKeg['nama_sub_kegiatan'] }}</td>
                                                    @if(count($subKeg['indikator']) == 0)                                                    
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    @endif
                                                </tr>
                                                    {{-- Indikator Sub Kegiatan --}}
                                                    @foreach($subKeg['indikator'] as $indSubKeg)
                                                    @php
                                                        if(is_null($indSubKeg->realisasiindikatorsubkeg) == 'kosong')
                                                        {
                                                            $t_1_SubKeg = '';
                                                            $t_2_SubKeg = '';
                                                            $t_3_SubKeg = '';
                                                            $t_4_SubKeg = '';
                                                        }else{
                                                            $t_1_SubKeg = $indSubKeg->realisasiindikatorsubkeg->t_1;
                                                            $t_2_SubKeg = $indSubKeg->realisasiindikatorsubkeg->t_2;
                                                            $t_3_SubKeg = $indSubKeg->realisasiindikatorsubkeg->t_3;
                                                            $t_4_SubKeg = $indSubKeg->realisasiindikatorsubkeg->t_4;                                                            
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $indSubKeg->indikator_subkegiatan }}</td>
                                                        <td>
                                                            <label class="rekap_target">
                                                                {{ $indSubKeg->target }}
                                                            </label>
                                                            <label class="rekap_target_perubahan">
                                                                {{ $indSubKeg->target_perubahan }}
                                                            </label>
                                                        </td>
                                                        <td>{{ $indSubKeg->satuan }}</td>
                                                        <td>{{$t_1_SubKeg}}</td>
                                                        <td>{{$t_2_SubKeg}}</td>
                                                        <td>{{$t_3_SubKeg}}</td>
                                                        <td>{{$t_4_SubKeg}}</td>
                                                        <td>{{$subKeg['nama_skpd']}}</td>
                                                    </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <script>
        
        var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
        
        async function showHide(twl) // set kondisi sesuai triwulan yang dipilih
        {
            if(twl == 4)
            {
                $(".link-excel-tw1").hide();
                $(".link-excel-tw2").hide();
                $(".link-excel-tw3").hide();
                $(".link-excel-tw4").show();
                $(".link-pdf-tw1").hide();
                $(".link-pdf-tw2").hide();
                $(".link-pdf-tw3").hide();
                $(".link-pdf-tw4").show();
                $(".rekap_target").hide();
                $(".rekap_target_perubahan").show();
            }else if(twl == 3)
            {
                $(".link-excel-tw1").hide();
                $(".link-excel-tw2").hide();
                $(".link-excel-tw3").show();
                $(".link-excel-tw4").hide();
                $(".link-pdf-tw1").hide();
                $(".link-pdf-tw2").hide();
                $(".link-pdf-tw3").show();
                $(".link-pdf-tw4").hide();
                $(".rekap_target").show();
                $(".rekap_target_perubahan").hide();

            }else if(twl == 2)
            {
                $(".link-excel-tw1").hide();
                $(".link-excel-tw2").show();
                $(".link-excel-tw3").hide();
                $(".link-excel-tw4").hide();
                $(".link-pdf-tw1").hide();
                $(".link-pdf-tw2").show();
                $(".link-pdf-tw3").hide();
                $(".link-pdf-tw4").hide();
                $(".rekap_target").show();
                $(".rekap_target_perubahan").hide();
            }else if(twl == 1)
            {
                $(".link-excel-tw1").show();
                $(".link-excel-tw2").hide();
                $(".link-excel-tw3").hide();
                $(".link-excel-tw4").hide();
                $(".link-pdf-tw1").show();
                $(".link-pdf-tw2").hide();
                $(".link-pdf-tw3").hide();
                $(".link-pdf-tw4").hide();
                $(".rekap_target").show();
                $(".rekap_target_perubahan").hide();
            }
        }
        $("#twl_monev").on('change', function() {
            twl = this.value;
            showHide(twl);
        }); //pilih triwulan
        showHide(twl);
    </script>
    @include('layouts.footer')
@endsection
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>        
        <div class="main_content_iner ">
            <div class="container-fluid p-0">                
                <div class="white_card mb_30 ">
                    <div class="white_card_header">
                        <h4>Setting Input</h4>
                    </div>
                    <div class="white_card_body">
                        <form method="POST" action="{{ url('monev/renja/settinginput/save') }}">
                            @csrf
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="jenisInput">Jenis Input</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <select class="form-select" aria-label="Default select example" name="jenisInput" id="jenisInput">
                                        <option value="renja" @if($settingInput->jenisInput == 'renja') selected @endif>RENJA</option>
                                        <!-- <option value="rkpd">RKPD</option>
                                        <option value="spm">SPM</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="tahun">Tahun</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <select class="form-select" aria-label="Default select example" name="tahun" id="tahun">
                                        <option value="2024" @if($settingInput->tahun == 2024) selected @endif >2024</option>
                                        <option value="2023" @if($settingInput->tahun == 2023) selected @endif >2023</option>
                                        <option value="2022" @if($settingInput->tahun == 2022) selected @endif >2022</option>
                                        <option value="2021" @if($settingInput->tahun == 2021) selected @endif >2021</option>
                                        <option value="2020" @if($settingInput->tahun == 2020) selected @endif >2020</option>
                                        <option value="2019" @if($settingInput->tahun == 2019) selected @endif >2019</option>
                                        <option value="2018" @if($settingInput->tahun == 2018) selected @endif >2018</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="triwulan">Triwulan</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <select class="form-select" aria-label="Default select example" name="triwulan" id="triwulan">
                                        <option value="1" @if($settingInput->triwulan == 1) selected @endif>1</option>
                                        <option value="2" @if($settingInput->triwulan == 2) selected @endif>2</option>
                                        <option value="3" @if($settingInput->triwulan == 3) selected @endif>3</option>
                                        <option value="4" @if($settingInput->triwulan == 4) selected @endif>4</option>
                                    </select>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col col-md-12">
                                    <h4>Batas Waktu Input</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="mulai">Mulai</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <input type="datetime-local" class="form-control" name="mulai" id="mulai" value="{{$settingInput->mulai}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <b><label for="akhir">Akhir</label></b>
                                </div>
                                <div class="col col-sm-2">
                                    <input type="datetime-local" class="form-control" name="akhir" id="akhir" value="{{$settingInput->akhir}}"/>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col col-sm-2">
                                    <input type="submit" class="btn btn-primary" value="SUBMIT">
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>                             
            </div>
        </div>

        <!-- Footer -->
        @include('layouts.footer')
@endsection
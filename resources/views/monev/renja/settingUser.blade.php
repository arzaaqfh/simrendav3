@extends('layouts.app')
@section('content')

    <!-- Side Bar -->
    @include('monev.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <style type="text/css">
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>
        <div class="main_content_iner ">
            <div class="container-fluid p-0">                
                <div class="white_card mb_30 ">
                    <div class="white_card_header">
                        <h4>Setting User</h4>
                    </div>
                    <div class="white_card_body">
                        <p class="alert alert-danger">
                            Klik <b>HAPUS</b> data user saat ini <b>BELUM DENGAN QUESTION BOX</b> !<br/>
                            Klik <b>HAPUS</b> akan <b>MENGHAPUS LANGSUNG DATA USER</b> !
                        </p>
                        <div class="row text-right">
                            <div class="col-12">
                                <a href="{{ url('monev/renja/settinguser/create/') }}" class="btn btn-primary">
                                    Tambah
                                </a>
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Pengguna</th>
                                    <th>Username</th>
                                    <th>Perangkat Daerah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $index => $usr)
                                <tr>
                                    <td>
                                        {{ $index+1 }}
                                    </td>
                                    <td>
                                        {{$usr->nama_pengguna}}
                                    </td>
                                    <td>
                                        {{$usr->username}}
                                    </td>
                                    <td>
                                        <ul>
                                            @foreach ($usr->user_skpd as $pd)                                                    
                                                <li>{{ $pd->skpd->first()->nama_skpd }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        @if (Auth::user()->id_user != $usr->id_user)
                                            <a href="{{ url('monev/renja/settinguser/edit/'.$usr->id_user) }}" class="btn btn-secondary">
                                                Ubah
                                            </a><br/>
                                            <a href="{{ url('/monev/renja/settinguser/delete/'.$usr->id_user)}}" class="btn btn-danger">
                                                Hapus
                                            </a>                                               
                                        @endif                          
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        {{-- {{ $user->links('vendor.pagination.bootstrap-4') }} --}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>   
                </div>                             
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.js-example-basic-single').select2();
            });      
        </script>
        <!-- Footer -->
        @include('layouts.footer')
@endsection
<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-primary ">
        {{-- Logo SIMRENDA --}}
        <img class="large_logo" src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="50px" width="250px">
        <img class="small_logo" src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="50px" width="50px">
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <h4 class="menu-text"><span>MODUl LAINNYA</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li>
            <a href="{{ url('sdgs/dashboard/rekap') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    
                </div>
                <div class="nav_title">
                    <span>SDGs</span>
                </div>
            </a>
        </li>
        @if(Auth::user()->id_role != 15)
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li>
            <a href="{{ url('monev/renja') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard RENJA</span>
                </div>
            </a>
        </li>

        <h4 class="menu-text"><span>MENU CAPAIAN</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li>
            <a href="{{ url('monev/renja/realisasi') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-book"></i>
                </div>
                <div class="nav_title">
                    <span>Realisasi RENJA</span>
                </div>
            </a>
        </li>
        <h4 class="menu-text"><span>MENU LAINNYA</span> <i class="fas fa-ellipsis-h"></i> </h4>
        @if (Auth::user()->id_role == 1)
        <!-- <li>
            <a href="{{ url('monev/renja/bataswaktu') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-stopwatch"></i>
                </div>
                <div class="nav_title">
                    <span>Batas Waktu Input</span>
                </div>
            </a>
        </li> -->
        <li>
            <a href="{{ url('monev/renja/settinginput') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-stopwatch"></i>
                </div>
                <div class="nav_title">
                    <span>Setting Input</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('monev/renja/settinguser') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-tasks"></i>
                </div>
                <div class="nav_title">
                    <span>Setting User</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('monev/renja/importMonev') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-database"></i>
                </div>
                <div class="nav_title">
                    <span>Import Data Monev</span>
                </div>
            </a>
        </li>
        @endif
        <li>
            <a href="{{ url('monev/renja/rekap/renja') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-table"></i>
                </div>
                <div class="nav_title">
                    <span>Rekap RENJA</span>
                </div>
            </a>
        </li>
        @endif
    </ul>
</nav>
<!--/ sidebar  -->
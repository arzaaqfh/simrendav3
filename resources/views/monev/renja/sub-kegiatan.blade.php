<div class="col-lg-12">
    <div class="d-flex align-items-start">
        {{-- LIST SUB KEGIATAN --}}
        <div class="col-lg-5 overflow-auto d-flex align-items-start" style="height: 500px;">
            <ul class="nav flex-column nav-pills me-3" id="v-pills-tab-subkeg" role="tablist" aria-orientation="vertical">
                @foreach ($subKeg as $no => $skg)
                <li class='nav-item' style="list-style-type: square;">
                    <a  class='nav-link' 
                        id='subkegiatanitem{{$skg->id_sub_kegiatan}}' 
                        data-bs-toggle='pill' 
                        href='#indsubkegiatanitem{{$skg->id_sub_kegiatan}}'  
                        role='tab'  
                        aria-controls='subkegiatanitem{{$skg->id_sub_kegiatan}}'  
                        aria-selected='false' 
                    >
                        <i class=""></i>
                        <span>{{ $skg->nama_sub_kegiatan }}</span>
                    </a>
                </li>
                @endforeach
            </ul>        
        </div>        
        {{-- LIST INDIKATOR SUB KEGIATAN --}}
        <div class="col-lg-7 tab-content overflow-auto" style="height: 500px;">
            @foreach ($subKeg as $no => $skg)
            <div    class='tab-pane fade'    {{-- tab-pane --}}
                    id='indsubkegiatanitem{{$skg->id_sub_kegiatan}}'
                    role='tabpanel'  
                    aria-labelledby='subkegiatanitem{{$skg->id_sub_kegiatan}}-tab' 
            >
                <form id='formAngSubKeg{{$skg->id_sub_kegiatan}}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                    <div class='table-responsive'>{{-- (awal) div table --}}
                        <table id='formAngSKGitem{{$skg->id_sub_kegiatan}}' class='table table-borderless'>{{-- (awal) table --}}
                            <thead>
                                <tr>
                                    <th class='text-center fw-bold text-center mb-3'><h5>Capaian Anggaran</h5></th>
                                </tr>
                            </thead>
                            @if (!is_null($skg->apbd_kota))
                                <tbody>
                                    <tr id="detailAngSubKeg_{{$skg->id_sub_kegiatan}}">
                                        <td>
                                            <div class='border border-primary shadow-sm p-3 mb-3'>
                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class='col-lg-4'> {{-- target indikator sub kegiatan --}}
                                                        <h6>Anggaran</h6>
                                                        <div id='targetAngSubKeg_{{$skg->id_sub_kegiatan}}' class='fw-bold'>
                                                            <span class="target">
                                                                @if($skg->apbd_kota != null)                                                                
                                                                    {{number_format($skg->apbd_kota,2,',','.')}}
                                                                @else
                                                                    -
                                                                @endif                                                            
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-5'> {{-- progress --}}
                                                        <h6>Capaian Anggaran Sub Kegiatan</h6>
                                                        
                                                        <div class="capaian mt-3">
                                                            {{-- capaian triwulan 1 --}}
                                                            <div class="capaian-angsubkeg-bar capaian-angsubkeg-tw1">
                                                                <div
                                                                    id="capaianAngSubKeg-tw1-{{$skg->r_1}}"
                                                                    class="capaian-per"
                                                                    style="max-width: 0%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenAngSubKeg-tw1-{{$skg->id_sub_kegiatan}}">
                                                                            {{number_format(0,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-3'> {{-- teks tercapai atau belum tercapai --}}
                                                        <h6>Status</h6>
                                                        <div class='fw-bold'>
                                                            <span id='badge-sub-kegiatan-progress'
                                                                    class='badge bg-badge'
                                                            >
                                                            -
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}

                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    @for($i=1;$i<=4;$i++)
                                                        @php 
                                                            $tw = '';
                                                            $disabled = 'disabled';
                                                            
                                                            if($i==1) // set nilai realisasi sesuai triwulan
                                                            {
                                                                $tw = $skg->r_1;
                                                            }else if($i==2)
                                                            {
                                                                $tw = $skg->r_2;
                                                            }else if($i==3)
                                                            {
                                                                $tw = $skg->r_3;
                                                            }else if($i==4)
                                                            {
                                                                $tw = $skg->r_4;
                                                            }

                                                            if($i == Session::get('triwulan') || Auth::user()->id_role == 1 || Auth::user()->id_role == 13) // set disabled input realisasi
                                                            {
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                        <div class='col-md-3 realisasiTW{{$i}}'>
                                                            <div class='form-floating'>
                                                                <input type='hidden' name='ang_triwulan{{$i}}[]' value='{{$i}}'>
                                                                <br/>
                                                                <label class="labelAngTriwulan">
                                                                    Triwulan {{$i}}
                                                                </label><br/> 
                                                                <input type='text' 
                                                                    data-type = 'currency'
                                                                    class='form-control' 
                                                                    name='r_{{$i}}' 
                                                                    placeholder='Triwulan {{$i}}' 
                                                                    value ='{{$tw}}' 
                                                                    {{$disabled}}
                                                                >
                                                            </div>
                                                        </div>
                                                    @endfor                                                                                                     
                                                    <input type='hidden' name='id_sub_kegiatan' value='{{$skg->id_sub_kegiatan}}'/>
                                                    <input type='hidden' name='id_log_renja' value='{{$skg->id_log_renja}}'>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>{{-- tombol submit form anggaran --}}
                                    <tr>
                                        <td>
                                            <div id="messageAngSubKegSukses{{$skg->id_sub_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-success">
                                                    <b>Anggaran berhasil disimpan !</b>
                                                </p>
                                            </div>     
                                            <div id="messageAngSubKegGagal{{$skg->id_sub_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-danger">
                                                    <b>Anggaran ada yang gagal disimpan !</b>
                                                </p>
                                            </div> 
                                            <div class='text-center' id='btnSimpanAngSubKeg_{{$skg->id_sub_kegiatan}}'>
                                                @php
                                                    date_default_timezone_set("Asia/Jakarta");
                                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                                    $sekarang  = date("Y-m-d H:i:s");
                                                @endphp
                                                @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                                    <button id="btnSimpanAngSubKeg{{$skg->id_sub_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                        Simpan
                                                    </button>
                                                    <i class="fa fa-spinner" id="loadingAngSubKeg{{$skg->id_sub_kegiatan}}"></i>
                                                @else
                                                    @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                        <button id="btnSimpanAngSubKeg{{$skg->id_sub_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                            Simpan
                                                        </button>
                                                        <i class="fa fa-spinner" id="loadingAngSubKeg{{$skg->id_sub_kegiatan}}"></i>   
                                                    @else
                                                        <p class="alert alert-warning">
                                                            <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                                        </p>                                                            
                                                    @endif                                               
                                                @endif
                                            </div> 
                                        </td>
                                    </tr>
                                </tfoot>
                            @endif
                        </table>{{-- (akhir) table --}} 
                    </div>{{-- (akhir) div table --}} 
                    <script>                        
                        // Submit Form Anggaran Sub Kegiatan
                        $("#messageAngSubKegSukses{{$skg->id_sub_kegiatan}}").hide();
                        $("#messageAngSubKegGagal{{$skg->id_sub_kegiatan}}").hide();
                        $("#loadingAngSubKeg{{$skg->id_sub_kegiatan}}").hide();
                        var cek = null;
                        $("#formAngSubKeg{{$skg->id_sub_kegiatan}}" ).on( "submit", function(e) {
                            var realisasi = '';
                            var triwulan = "{{Session::get('triwulan')}}";
                            var token = "{{$token}}";
                            if(triwulan == 1)
                            {
                                realisasi = $(this).find("input[name='r_1']").val();
                            }else if(triwulan == 2)
                            {
                                realisasi = $(this).find("input[name='r_2']").val();
                            }else if(triwulan == 3)
                            {
                                realisasi = $(this).find("input[name='r_3']").val();
                            }else if(triwulan == 4)
                            {
                                realisasi = $(this).find("input[name='r_4']").val();
                            }
                            var dataString = {
                                id_log_renja : $(this).find("input[name='id_log_renja']").val(),
                                triwulan : triwulan,
                                realisasi : Number(realisasi.replace(/[^0-9\.-]+/g,"")) 
                            };
                            console.log(dataString);
                            cek = $(dataString);
                            // console.log($(this).get('id_log_renja'));
                            $("#btnSimpanAngSubKeg{{$skg->id_sub_kegiatan}}").hide();
                            $("#loadingAngSubKeg{{$skg->id_sub_kegiatan}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('api/renja/save_realisasi_anggaran') }}",
                                beforeSend  :   function(xhr){
                                                    xhr.setRequestHeader('Authorization', 'Bearer '+token);
                                                },
                                data: dataString,
                                success: function (hasil) {
                                    console.log(hasil);
                                    if(hasil.original.status == 200)
                                    {
                                        $("#messageAngSubKegSukses{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAngSubKegSukses{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                        reloadIndSubKeg("{{$skg->id_sub_kegiatan}}",'simpan');
                                    }else{
                                        $("#messageAngSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAngSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                    }
                                    $("#btnSimpanAngSubKeg{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingAngSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                },
                                error: function () {
                                    $("#messageAngSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageAngSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                    });
                                    $("#btnSimpanAngSubKeg{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingAngSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                }
                            });
                            e.preventDefault();
                            updateStatSubKeg(); //update status semua input realisasi indikator
                        }); 
                    </script>
                </form><hr/>
                <form id='formIndSubKeg{{$skg->id_sub_kegiatan}}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}' />
                    <div class='table-responsive'>{{-- (awal) div table --}}
                        <table id='formIndSubKegitem{{$skg->id_sub_kegiatan}}' class='table table-borderless'>{{-- (awal) table --}}
                            <thead>
                                <tr>
                                    <th class='text-center fw-bold text-center mb-3'><h5>Daftar Indikator Sub Kegiatan</h5></th>
                                </tr>
                            </thead>
                            @if (!is_null($indSubKeg[$skg->id_sub_kegiatan]))
                                <tbody>                            
                                    @foreach ($indSubKeg[$skg->id_sub_kegiatan] as $noInd => $indikator)
                                    <tr id="detailIndSubKeg_{{$indikator->id_indikator_subkegiatan}}">
                                        <td>
                                            <h5 class='mb-0'>Indikator #{{($noInd+1)}}</h5>
                                            <div class='border border-primary shadow-sm p-3 mb-3'>
                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class='col-lg-6'> {{-- nama indikator sub kegiatan --}}
                                                        <h6>Nama Indikator</h6>
                                                        <div id='namaIndSubKeg_{{$indikator->id_indikator_subkegiatan}}' class='fw-bold'>
                                                            {{$indikator->indikator_subkegiatan}}
                                                        </div>
                                                        <input class="form-control editIndSKG{{$indikator->id_indikator_subkegiatan}}" type="text" name="indikator_subkegiatan_{{$indikator->id_indikator_subkegiatan}}" value="{{$indikator->indikator_subkegiatan}}"/>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                        <h6>Rumus Perhitungan</h6>
                                                        <select id="rumussubkeg_{{$indikator->id_indikator_subkegiatan}}" class="form-control" name="rumus[]">
                                                            <option value="">Pilih Rumus</option>
                                                            @foreach($rumus as $rms)
                                                                <option 
                                                                    @php
                                                                        if(!is_null($indikator->rumus) && $indikator->rumus == $rms->id_rumus)
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                    @endphp
                                                                    value="{{$rms->id_rumus}}"
                                                                >
                                                                    {{$rms->rumus}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <script>
                                                            // jquery untuk select rumus
                                                            $("#rumussubkeg_"+'{{$indikator->id_indikator_subkegiatan}}').on('change',function()
                                                            {
                                                                let indikatorsubkegiatan = JSON.parse('"{{ json_encode($indikator) }}"');
                                                                console.log(indikatorsubkegiatan);
                                                            });
                                                        </script>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- target indikator sub kegiatan --}}
                                                        <h6>Target</h6>
                                                        <div id='targetIndSubKeg_{{$indikator->id_indikator_subkegiatan}}' class='fw-bold'>
                                                            <span class="target">
                                                                {{$indikator->target}}                                                         
                                                            </span>
                                                            <span class="target_perubahan">
                                                                {{$indikator->target_perubahan}}
                                                            </span>
                                                        </div>
                                                        <input class="form-control editIndSKG{{$indikator->id_indikator_subkegiatan}}" type="text" name="target_{{$indikator->id_indikator_subkegiatan}}" value="@if($indikator->target_perubahan != null) {{$indikator->target_perubahan}} @else {{$indikator->target}} @endif"/>
                                                    </div>
                                                    <div class='col-lg-2'> {{-- satuan indikator sub kegiatan --}}
                                                        <h6>Satuan</h6>
                                                        <div id='satuanIndSubKeg_{{$indikator->id_indikator_subkegiatan}}' class='fw-bold'>
                                                            {{$indikator->satuan}}
                                                        </div>
                                                        <input class="form-control editIndSKG{{$indikator->id_indikator_subkegiatan}}" type="text" name="satuan_{{$indikator->id_indikator_subkegiatan}}" value="{{$indikator->satuan}}"/>
                                                    </div>
                                                    <div class='col-lg-5'> {{-- progress --}}
                                                        <h6>Capaian Kinerja Sub Kegiatan</h6>
                                                        @php
                                                            $persen_tw1 = 0;
                                                            $persen_tw2 = 0;
                                                            $persen_tw3 = 0;
                                                            $persen_perubahan = 0;
                                                            $target = floatval($indikator->target);
                                                            $target_perubahan = floatval($indikator->target_perubahan);
                                                            $t_1 = 0;
                                                            $t_2 = 0;
                                                            $t_3 = 0;
                                                            $t_4 = 0;
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_1))
                                                            {
                                                                $t_1 = floatval($indikator->realisasiindikatorsubkeg->t_1);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_2))
                                                            {
                                                                $t_2 = floatval($indikator->realisasiindikatorsubkeg->t_2);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_3))
                                                            {
                                                                $t_3 = floatval($indikator->realisasiindikatorsubkeg->t_3);
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && !is_null($indikator->realisasiindikatorsubkeg->t_4))
                                                            {
                                                                $t_4 = floatval($indikator->realisasiindikatorsubkeg->t_4);
                                                            }

                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && $target != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_tw1 = (($t_1)/$target*100)/4;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/4;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_tw1 = (($t_1)/$target*100)/2;
                                                                    $persen_tw2 = (($t_1+$t_2)/$target*100)/2;
                                                                    $persen_tw3 = (($t_1+$t_2+$t_3)/$target*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_tw1 = ($t_1)/$target*100;
                                                                    $persen_tw2 = ($t_1+$t_2)/$target*100;
                                                                    $persen_tw3 = ($t_1+$t_2+$t_3)/$target*100;
                                                                }
                                                            }
                                                            if(!is_null($indikator->realisasiindikatorsubkeg) && $target_perubahan != 0)
                                                            {
                                                                if($indikator->rumus && $indikator->rumus == 1)
                                                                {
                                                                    // rumus triwulan
                                                                    $persen_perubahan = (($t_1+$t_2+$t_3+$t_4)/$target_perubahan*100)/4;
                                                                }else if($indikator->rumus && $indikator->rumus == 2)
                                                                {
                                                                    // rumus semester
                                                                    $persen_perubahan = ($t_4/$target_perubahan*100)/2;
                                                                }else if($indikator->rumus && $indikator->rumus == 3)
                                                                {
                                                                    // rumus tahun
                                                                    $persen_perubahan = $t_4/$target_perubahan*100;
                                                                }
                                                            }
                                                        @endphp
                                                        <div class="capaian mt-3">
                                                            {{-- capaian triwulan 1 --}}
                                                            <div class="capaian-bar capaian-tw1">
                                                                <div
                                                                    id="capaianSubKeg-tw1-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw1}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenSubKeg-tw1-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw1,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 2 --}}
                                                            <div class="capaian-bar capaian-tw2">
                                                                <div
                                                                    id="capaianSubKeg-tw2-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw2}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenSubKeg-tw2-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw2,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 3 --}}
                                                            <div class="capaian-bar capaian-tw3">
                                                                <div
                                                                    id="capaianSubKeg-tw3-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_tw3}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenSubKeg-tw3-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_tw3,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                            {{-- capaian triwulan 4 --}}
                                                            <div class="capaian-bar capaian-tw4">
                                                                <div
                                                                    id="capaianSubKeg-tw4-{{$indikator->id_indikator_subkegiatan}}"
                                                                    class="capaian-per"
                                                                    style="max-width: {{$persen_perubahan}}%"
                                                                >
                                                                </div>
                                                                <center>
                                                                    <div style="position: relative; z-index: 1; font-size: 12px;">
                                                                        <b id="capaianPersenSubKeg-tw4-{{$indikator->id_indikator_subkegiatan}}">
                                                                            {{number_format($persen_perubahan,2,',','.')}} %
                                                                        </b>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-3'> {{-- teks tercapai atau belum tercapai --}}
                                                        <h6>Status</h6>
                                                        <div class='fw-bold'>
                                                            @php
                                                                $status_tw1 = '-';
                                                                $status_tw2 = '-';
                                                                $status_tw3 = '-';
                                                                $status_tw4 = '-';
                                                                if($indikator->rumus && ($indikator->rumus == 1 || $indikator->rumus == 6))
                                                                {
                                                                    // jika rumus positif
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else{
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }else if($indikator->rumus && ($indikator->rumus == 2 || $indikator->rumus == 7))
                                                                {
                                                                    // jika rumus negatif                                                                    
                                                                    // triwulan 1
                                                                    if($persen_tw1>90 && $persen_tw1<=100 || $persen_tw1 > 100)
                                                                    {
                                                                        $status_tw1 = 'Sangat Rendah';
                                                                    }else if($persen_tw1>75 && $persen_tw1<=90)
                                                                    {
                                                                        $status_tw1 = 'Rendah';
                                                                    }else if($persen_tw1>65 && $persen_tw1<=75)
                                                                    {
                                                                        $status_tw1 = 'Sedang';
                                                                    }else if($persen_tw1>50 && $persen_tw1<=65)
                                                                    {
                                                                        $status_tw1 = 'Tinggi';
                                                                    }else if($persen_tw1<=50)
                                                                    {
                                                                        $status_tw1 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw1 = '-';
                                                                    }
                                                                    // triwulan 2
                                                                    if($persen_tw2>90 && $persen_tw2<=100 || $persen_tw2 > 100)
                                                                    {
                                                                        $status_tw2 = 'Sangat Rendah';
                                                                    }else if($persen_tw2>75 && $persen_tw2<=90)
                                                                    {
                                                                        $status_tw2 = 'Rendah';
                                                                    }else if($persen_tw2>65 && $persen_tw2<=75)
                                                                    {
                                                                        $status_tw2 = 'Sedang';
                                                                    }else if($persen_tw2>50 && $persen_tw2<=65)
                                                                    {
                                                                        $status_tw2 = 'Tinggi';
                                                                    }else if($persen_tw2<=50)
                                                                    {
                                                                        $status_tw2 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw2 = '-';
                                                                    }
                                                                    // triwulan 3
                                                                    if($persen_tw3>90 && $persen_tw3<=100 || $persen_tw3 > 100)
                                                                    {
                                                                        $status_tw3 = 'Sangat Rendah';
                                                                    }else if($persen_tw3>75 && $persen_tw3<=90)
                                                                    {
                                                                        $status_tw3 = 'Rendah';
                                                                    }else if($persen_tw3>65 && $persen_tw3<=75)
                                                                    {
                                                                        $status_tw3 = 'Sedang';
                                                                    }else if($persen_tw3>50 && $persen_tw3<=65)
                                                                    {
                                                                        $status_tw3 = 'Tinggi';
                                                                    }else if($persen_tw3<=50)
                                                                    {
                                                                        $status_tw3 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw3 = '-';
                                                                    }
                                                                    // triwulan 4
                                                                    if($persen_perubahan>90 && $persen_perubahan<=100 || $persen_perubahan > 100)
                                                                    {
                                                                        $status_tw4 = 'Sangat Rendah';
                                                                    }else if($persen_perubahan>75 && $persen_perubahan<=90)
                                                                    {
                                                                        $status_tw4 = 'Rendah';
                                                                    }else if($persen_perubahan>65 && $persen_perubahan<=75)
                                                                    {
                                                                        $status_tw4 = 'Sedang';
                                                                    }else if($persen_perubahan>50 && $persen_perubahan<=65)
                                                                    {
                                                                        $status_tw4 = 'Tinggi';
                                                                    }else if($persen_perubahan<=50)
                                                                    {
                                                                        $status_tw4 = 'Sangat Tinggi';
                                                                    }else
                                                                    {
                                                                        $status_tw4 = '-';
                                                                    }
                                                                }
                                                                if(ctype_alpha($indikator->target))
                                                                {
                                                                    $status_tw1 = '-';
                                                                    $status_tw2 = '-';
                                                                    $status_tw3 = '-';
                                                                    $status_tw4 = '-';
                                                                }
                                                            @endphp
                                                            <span id='badge-sub-kegiatan-progress'
                                                                    class='badge bg-badge'
                                                            >
                                                                <b class="capaian-tw1" style="color:black; font-size: 14px">
                                                                    {{$status_tw1}}
                                                                </b>
                                                                <b class="capaian-tw2" style="color:black; font-size: 14px">
                                                                    {{$status_tw2}}
                                                                </b>
                                                                <b class="capaian-tw3" style="color:black; font-size: 14px">
                                                                    {{$status_tw3}}
                                                                </b>
                                                                <b class="capaian-tw4" style="color:black; font-size: 14px">
                                                                    {{$status_tw4}}
                                                                </b>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}

                                                <div class='row g-3 mb-3'> {{-- (awal) row g-3 mb-3 --}}
                                                    @if(!is_null($indikator->realisasiindikatorsubkeg))
                                                        @for($i=1;$i<=4;$i++)
                                                        @php 
                                                            $tw = '';
                                                            $disabled = 'disabled';
                                                            
                                                            if($i==1) // set nilai realisasi sesuai triwulan
                                                            {
                                                                $tw = $indikator->realisasiindikatorsubkeg->t_1;
                                                            }else if($i==2)
                                                            {
                                                                $tw = $indikator->realisasiindikatorsubkeg->t_2;
                                                            }else if($i==3)
                                                            {
                                                                $tw = $indikator->realisasiindikatorsubkeg->t_3;
                                                            }else if($i==4)
                                                            {
                                                                $tw = $indikator->realisasiindikatorsubkeg->t_4;
                                                            }

                                                            if($i == Session::get('triwulan') || Auth::user()->id_role == 1 || Auth::user()->id_role == 13) // set disabled input realisasi
                                                            {
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                        <div class='col-md-3 realisasiTW{{$i}}'>
                                                            <div class='form-floating'>
                                                                <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                                <br/>
                                                                <label class="labelTriwulan">
                                                                    Triwulan {{$i}}
                                                                </label><br/> 
                                                                <input type='text' class='form-control' 
                                                                    id = 't_{{$i}}_{{$indikator->id_indikator_subkegiatan}}'
                                                                    name='t_{{$i}}[]' 
                                                                    placeholder='Triwulan {{$i}}' 
                                                                    value ='{{$tw}}' 
                                                                    {{$disabled}}
                                                                >
                                                            </div>
                                                        </div>
                                                        @endfor
                                                    @else
                                                    @for($i=1;$i<=4;$i++)
                                                        @php 
                                                            $tw = '';
                                                            $disabled = 'disabled';
                                                            
                                                            if($i == Session::get('triwulan')) // set disabled input realisasi
                                                            {
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                        <div class='col-md-3 realisasiTW{{$i}}'>
                                                            <div class='form-floating'>
                                                                <input type='hidden' name='triwulan{{$i}}[]' value='{{$i}}'>
                                                                <br/>
                                                                <label class="labelTriwulan">
                                                                    Triwulan {{$i}}
                                                                </label><br/> 
                                                                <input type='text' class='form-control' 
                                                                    id = 't_{{$i}}_{{$indikator->id_indikator_subkegiatan}}'
                                                                    name='t_{{$i}}[]' 
                                                                    placeholder='Triwulan {{$i}}' 
                                                                    value ='{{$tw}}' 
                                                                    {{$disabled}}
                                                                >
                                                            </div>
                                                        </div>
                                                        @endfor
                                                    @endif                                                    
                                                    <input type='hidden' name='id_indikator_subkegiatan[]' value='{{$indikator->id_indikator_subkegiatan}}'/>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                <div class='row g-3 mb-3 btnAksiSubKeg{{$skg->id_sub_kegiatan}}'> {{-- (awal) row g-3 mb-3 --}}
                                                    <div class="text-right">                                        
                                                        {{-- tombol untuk edit indikator --}}
                                                        <button type="button" class="btn btn-sm btn-primary" id="btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}">
                                                            Ubah
                                                        </button>                                           
                                                        <button type="button" class="btn btn-sm btn-secondary btnAksiUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}" id="btnBatalUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}">
                                                            Batal
                                                        </button>                                              
                                                        <button type="button" class="btn btn-sm btn-primary btnAksiUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}" onclick="ubahIndSubKeg({{$indikator->id_indikator_subkegiatan}},{{$skg->id_sub_kegiatan}},{{$skg->id_renja}})">
                                                            Simpan Perubahan
                                                        </button>

                                                        {{-- tombol untuk hapus indikator --}}
                                                        <button type="button" class="btn btn-sm btn-danger" id="btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}">
                                                            Hapus
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-secondary btnAksiHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}" id="btnBatalHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}">
                                                            Batal
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-danger btnAksiHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}" onclick="hapusIndSubKeg({{$indikator->id_indikator_subkegiatan}},{{$skg->id_sub_kegiatan}})">
                                                            Hapus Indikator
                                                        </button>
                                                    </div>
                                                </div> {{-- (akhir) row g-3 mb-3 --}}
                                                @endif
                                                <script>
                                                    // setting awal
                                                    $('.editIndSKG{{$indikator->id_indikator_subkegiatan}}').hide();
                                                    $('.btnAksiUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                    $('.btnAksiHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();

                                                    // aksi tombol Ubah 
                                                    $('#btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').on( "click", function() {
                                                        // yang tampil
                                                        $('.editIndSKG{{$indikator->id_indikator_subkegiatan}}').show();
                                                        $('.btnAksiUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                        // yang ditutup
                                                        $('#btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                        $('#btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                    });
                                                    // aksi tombol Batal Ubah
                                                    $('#btnBatalUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.editIndSKG{{$indikator->id_indikator_subkegiatan}}').hide();
                                                        $('.btnAksiUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                        // yang ditutup
                                                        $('#btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                        $('#btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                    });

                                                    // aksi tombol Hapus
                                                    $('#btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.btnAksiHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                        // yang ditutup
                                                        $('#btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                        $('#btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                    });
                                                    // aksi tombol Batal Hapus
                                                    $('#btnBatalHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').on("click", function () {
                                                        // yang tampil
                                                        $('.btnAksiHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').hide();
                                                        // yang ditutup
                                                        $('#btnUbahIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                        $('#btnHapusIndSKG_{{$indikator->id_indikator_subkegiatan}}').show();
                                                    });
                                                </script>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>{{-- tombol submit form realisasinya --}}
                                    <tr>
                                        <td>
                                            <div id="messageSubKegSukses{{$skg->id_sub_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-success">
                                                    <b>Data berhasil disimpan !</b>
                                                </p>
                                            </div>     
                                            <div id="messageSubKegGagal{{$skg->id_sub_kegiatan}}" style="
                                                width: 250px;
                                                height: 100px;">
                                                <p class="alert alert-danger">
                                                    <b>Data ada yang gagal disimpan !</b>
                                                </p>
                                            </div> 
                                            <div class='text-center' id='btnSimpanRIndSubKeg_{{$skg->id_sub_kegiatan}}'>
                                                @php
                                                    date_default_timezone_set("Asia/Jakarta");
                                                    $bwi_mulai = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['mulai']));
                                                    $bwi_akhir = date("Y-m-d H:i:s",strtotime(Session::get('bwi')['akhir']));
                                                    $sekarang  = date("Y-m-d H:i:s");
                                                @endphp
                                                @if($sekarang >= $bwi_mulai && $sekarang <= $bwi_akhir)
                                                    @if(count($indSubKeg[$skg->id_sub_kegiatan])>0)
                                                        <button id="btnSimpanIndSubKeg{{$skg->id_sub_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                            Simpan
                                                        </button>
                                                        <i class="fa fa-spinner" id="loadingSubKeg{{$skg->id_sub_kegiatan}}"></i>                                                        
                                                    @else
                                                        <p class="alert alert-info">
                                                            Indikator Tidak Tersedia
                                                        </p>
                                                    @endif
                                                @else
                                                    @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                                                        @if(count($indSubKeg[$skg->id_sub_kegiatan])>0)
                                                            <button id="btnSimpanIndSubKeg{{$skg->id_sub_kegiatan}}" type='submit' class='btn btn-primary btn-sm'>
                                                                Simpan
                                                            </button>
                                                            <i class="fa fa-spinner" id="loadingSubKeg{{$skg->id_sub_kegiatan}}"></i>                                                        
                                                        @else
                                                            <p class="alert alert-info">
                                                                Indikator Tidak Tersedia
                                                            </p>
                                                        @endif
                                                    @else
                                                        <p class="alert alert-warning">
                                                            <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>
                                                        </p>                                                            
                                                    @endif                                               
                                                @endif
                                            </div> 
                                        </td>
                                    </tr>
                                </tfoot>
                            @endif
                        </table>{{-- (akhir) table --}} 
                    </div>{{-- (akhir) div table --}} 
                    <script>
                        //Submit Form Realisasi Indikator Sub Kegiatan
                        $("#messageSubKegSukses{{$skg->id_sub_kegiatan}}").hide();
                        $("#messageSubKegGagal{{$skg->id_sub_kegiatan}}").hide();
                        $("#loadingSubKeg{{$skg->id_sub_kegiatan}}").hide();

                        $( "#formIndSubKeg{{$skg->id_sub_kegiatan}}" ).on( "submit", function(e) {
                            var dataString = $(this).serialize();
                            $("#btnSimpanIndSubKeg{{$skg->id_sub_kegiatan}}").hide();
                            $("#loadingSubKeg{{$skg->id_sub_kegiatan}}").show();
                            $.ajax({
                                type: "POST",
                                url: "{{ url('monev/renja/input/realisasi/IndSubKegiatan') }}",
                                data: dataString,
                                success: function (hasil) {
                                    console.log(hasil);
                                    var cek = true;
                                    $.each(hasil, function( index, value ) {
                                        if(value.status != 200)
                                        {
                                            cek = false;
                                        }
                                    });
                                    if(cek == true)
                                    {
                                        $("#messageSubKegSukses{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageSubKegSukses{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                        reloadIndSubKeg("{{$skg->id_sub_kegiatan}}",'simpan');
                                    }else{
                                        $("#messageSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                    }
                                    $("#btnSimpanIndSubKeg{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                },
                                error: function () {
                                    $("#messageSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                        $("#messageSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                    });
                                    $("#btnSimpanIndSubKeg{{$skg->id_sub_kegiatan}}").show();
                                    $("#loadingSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                }
                            });
                            e.preventDefault();
                            updateStatSubKeg(); //update status semua input realisasi indikator
                        });               
                    </script>
                </form>
                {{-- FORM TAMBAH INDIKATOR SUB KEGIATAN --}}
                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 13)
                <form id="formAddIndSubKeg{{$skg->id_sub_kegiatan}}">
                    <div class="container border border-success">
                        <br/>
                        <div class="row">
                            <div class="col">
                                <p class="alert alert-success">
                                    <b>Gunakan form ini untuk menambahkan indikator sub kegiatan yang tidak ada.</b>
                                </p>
                                <p class="alert alert-warning">
                                    <b>Refresh</b> halaman untuk melihat indikator yang sudah ditambah.
                                </p>
                            </div>
                        </div><br/>
                        <div class="row">
                            <h5>Tambah Indikator Sub Kegiatan</h5>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}' /> {{-- token --}}
                            <input type="hidden" name="id_sub_kegiatan" value="{{$skg->id_sub_kegiatan}}"/> {{-- id sub kegiatan --}}
                            <input type="hidden" name="id_renja" value="{{$skg->id_renja}}"/> {{-- id renja --}}
                        </div><br/>
                        
                        {{-- Indikator Sub Kegiatan --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Nama Indikator</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="indikator_subkegiatan" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Target --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Target</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="target" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Satuan --}}
                        <div class="row">
                            <div class="col col-md-4">
                                <b>Satuan</b>
                            </div>
                            <div class="col col-md-8">
                                <input style="50px" class="form-control" type="text" name="satuan" value="" required/>
                            </div>
                        </div><br/>
                        {{-- Submit Form --}}
                        <div class="row">
                            <div class="text-center">
                                {{-- Message Box --}}
                                <div id="messageAddIndSubKegSukses{{$skg->id_sub_kegiatan}}" style="
                                    width: 250px;
                                    height: 100px;">
                                    <p class="alert alert-success">
                                        <b>Data berhasil disimpan !</b>
                                    </p>
                                </div>     
                                <div id="messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}" style="
                                    width: 250px;
                                    height: 100px;">
                                    <p class="alert alert-danger">
                                        <b>Data gagal disimpan !</b>
                                    </p>
                                </div> 
                                {{-- Tombol Submit --}}
                                <button type="submit" class="btn btn-sm btn-success">
                                    Tambah
                                </button>
                                {{-- Loading --}}
                                <i class="fa fa-spinner" id="loadingAddIndSubKeg{{$skg->id_sub_kegiatan}}"></i>
                            </div>
                        </div><br/>
                    </div>
                    <script>
                            //Submit Form Tambah Indikator Sub Kegiatan
                            $("#messageAddIndSubKegSukses{{$skg->id_sub_kegiatan}}").hide();
                            $("#messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}").hide();
                            $("#loadingAddIndSubKeg{{$skg->id_sub_kegiatan}}").hide();

                            $( "#formAddIndSubKeg{{$skg->id_sub_kegiatan}}" ).on( "submit", function(e) {
                                var dataString = $(this).serialize();
                                $("#formAddIndSubKeg{{$skg->id_sub_kegiatan}} Button").hide();
                                $("#loadingAddIndSubKeg{{$skg->id_sub_kegiatan}}").show();
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('monev/renja/input/add/IndSubKeg') }}",
                                    data: dataString,
                                    success: function (hasil) {
                                        var cek = true;
                                        $.each(hasil, function( index, value ) {
                                            if(value.status != 200)
                                            {
                                                cek = false;
                                            }
                                        });
                                        if(cek == true)
                                        {
                                            $("#messageAddIndSubKegSukses{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                                $("#messageAddIndSubKegSukses{{$skg->id_sub_kegiatan}}").slideUp(500);
                                            });
                                            reloadIndSubKeg("{{$skg->id_sub_kegiatan}}","tambah");
                                        }else{
                                            $("#messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                                $("#messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                            });
                                        }
                                        $("#formAddIndSubKeg{{$skg->id_sub_kegiatan}} Button").show();
                                        $("#loadingAddIndSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                    },
                                    error: function () {
                                        $("#messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}").fadeTo(2000, 500).slideUp(500, function() {
                                            $("#messageAddIndSubKegGagal{{$skg->id_sub_kegiatan}}").slideUp(500);
                                        });
                                        $("#formAddIndSubKeg{{$skg->id_sub_kegiatan}} Button").show();
                                        $("#loadingAddIndSubKeg{{$skg->id_sub_kegiatan}}").hide();
                                    }
                                });
                                e.preventDefault();
                            });                  
                    </script>
                </form>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    // set status input realisasi per sub kegiatan
    async function setStatInputRSubKeg(twl)
    {
        var listItems = $("#v-pills-tab-subkeg li");
        var cek = 0; //jumlah yang belum diisi
        var statInput = <?php echo json_encode($statSubKeg) ?>;
        var indexStat;
        listItems.each(function(idx, li) {
            var link = $(li).find('a');
            var icon = $(li).find('i');
            indexStat = link.attr('id').replace('subkegiatanitem','');
            
            if(statInput[twl][indexStat] == 2)
            {
                link.attr('class','nav-link link-success');
                icon.attr('class','fa fa-check-circle');
            }else if(statInput[twl][indexStat] == 1)
            {
                link.attr('class','nav-link link-warning');
                icon.attr('class','fa fa-spinner');
            }else{
                link.attr('class','nav-link');
                icon.attr('class','');
            }
        });
    }

    // update status input realisasi untuk semua indikator sub kegiatan
    async function updateStatSubKeg()
    {
        var listItems = $("#v-pills-tab-subkeg li");
        var cek = 0; //jumlah yang belum diisi
        listItems.each(function(idx, li) {
            var product = $(li).find('a');
            if(product.attr('class')=='nav-link'||product.attr('class')=='nav-link link-warning'){
                cek = cek+1;
            }
        });
        if(cek == 0) //jika semua indikator sudah diisi
        {
            $('#subkegiatan-tab').attr('class', 'nav-link text-success border-success');
            $('#subkegiatan-tab i').attr('class', 'fa fa-check-circle');
        }else{
            $('#subkegiatan-tab').attr('class', 'nav-link');
            $('#subkegiatan-tab i').attr('class', '');
        }
    }
    //hapus indikator sub kegiatan
    async function hapusIndSubKeg(id_indikator_subkegiatan,id_sub_kegiatan)
    {
        var urlIndSubkeg = "{{url('/api/renja/post_delindikatorsubkeg')}}"; //alamat api realisasi indikator sub kegiatan
        var id_indikator_subkegiatan = id_indikator_subkegiatan;
        var token = '{{$token}}'; //token
        var triwulan = "{{Session::get('triwulan')}}";

        $.ajax({
            type        :   "POST",
            url         :   urlIndSubkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'id_indikator_subkegiatan': id_indikator_subkegiatan,
                                'triwulan': triwulan,
                            },
            success     :   function (responses) {
                                alert(responses.message);
                                $('#detailIndSubKeg_'+id_indikator_subkegiatan).remove();
                                $('#AFdetailIndSubKeg_'+id_indikator_subkegiatan).remove();
                                reloadIndSubKeg(id_sub_kegiatan, 'hapus');
                            }
        });

        // yang tampil
        $('.btnAksiHapusIndSKG_'+id_indikator_subkegiatan).hide();
        // yang ditutup
        $('#btnUbahIndSKG_'+id_indikator_subkegiatan).show();
        $('#btnHapusIndSKG_'+id_indikator_subkegiatan).show();
    }
    //ubah indikator sub kegiatan
    async function ubahIndSubKeg(id_indikator_subkegiatan,id_sub_kegiatan,id_renja)
    {
        var urlIndSubkeg = "{{url('/api/renja/post_addindikatorsubkeg')}}"; //alamat api realisasi indikator sub kegiatan
        var id_indikator_subkegiatan = id_indikator_subkegiatan;
        var indikator_subkegiatan = $("input[name=indikator_subkegiatan_"+id_indikator_subkegiatan).val();
        var target = $("input[name=target_"+id_indikator_subkegiatan).val();
        var satuan = $("input[name=satuan_"+id_indikator_subkegiatan).val();       
        var token = '{{$token}}'; //token
        var id_sub_kegiatan = id_sub_kegiatan;
        var id_renja = id_renja;
        var triwulan = "{{Session::get('triwulan')}}";
        $.ajax({
            type        :   "POST",
            url         :   urlIndSubkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'id_sub_kegiatan': id_sub_kegiatan,
                                'id_renja': id_renja,
                                'id_indikator_subkegiatan': id_indikator_subkegiatan,
                                'indikator_subkegiatan': indikator_subkegiatan,
                                'target': target,
                                'satuan': satuan,
                                'triwulan': triwulan
                            },
            success     :   function (responses) {
                                console.log(responses.message);
                                reloadIndSubKeg(id_sub_kegiatan,'ubah');
                            }
        });
        // yang tampil
        $('.editIndSKG'+id_indikator_subkegiatan).hide();
        $('.btnAksiUbahIndSKG_'+id_indikator_subkegiatan).hide();
        // yang ditutup
        $('#btnUbahIndSKG_'+id_indikator_subkegiatan).show();
        $('#btnHapusIndSKG_'+id_indikator_subkegiatan).show();
    }

    //memuat ulang semua data indikator sub kegiatan 
    async function reloadIndSubKeg(id_sub_kegiatan, aksi)
    {
        /* 
        aksi : 
            >> tambah (tambah indikator baru)
            >> simpan (simpan realisasi indikator)
            >> ubah (ubah indikator)
            >> hapus (hapus indikator) 
        */
        var formInd = $("#formIndSubKeg"+id_sub_kegiatan+" tbody");
        var formIndAnl = $("#formAFSubKegitem"+id_sub_kegiatan+" tbody");             
        var token = '{{$token}}'; //token
        var urlIndSubkeg = "{{url('/api/renja/get_inputrealindsubkeg')}}"; //alamat api realisasi indikator sub kegiatan
        var listSubKeg = $("#l_subKeg");
        var listIndSubKeg = $("#l_indSubKeg");
        var tahun = "{{session()->get('tahun_monev')}}"; //tahun monev
        var twl = $("#twl_monev").find(":selected").val(); //triwulan yang dipilih
        var opd = '';
        var userRole = "{{ Auth::user()->id_role }}";
        var id_indikator_subkegiatan;
        var indikator_subkegiatan;
        var id_renja;
        var target;
        var target_perubahan;
        var t_1;
        var t_2;
        var t_3;
        var t_4;
        var index;
        var satuan;
        var jmlIndikator;
        
        if(userRole == 1)
        {
            id_skpd = "{{Session::get('pilih_id_skpd')}}";
        }else{
            id_skpd = "{{Auth::user()->viewuser->id_skpd}}";
        }

        $.ajax({
            type        :   "GET",
            url         :   urlIndSubkeg,
            beforeSend  :   function(xhr){
                                xhr.setRequestHeader('Authorization', 'Bearer '+token);
                            },
            data        :   {
                                'triwulan':twl,
                                'tahun':tahun,
                                'opd':id_skpd,
                                'subkeg':id_sub_kegiatan
                            },
            success     :   function (responses) {
                                var jmlTotalSubKegDiisi = 0;
                                var jmlSubKegiatan = responses.data.length; 
                                $.each(responses.data, function (index1, item1) {
                                    jmlIndikator = item1.indikator.length; // tampung jumlah indikator
                                    var jmlRIndDiisi = 0;                               
                                    var persen_tw1 = 0;
                                    var persen_tw2 = 0;
                                    var persen_tw3 = 0;
                                    var persen_perubahan = 0;
                                    if(jmlIndikator == 0)//indikator kosong
                                    {
                                        $("#btnSimpanRIndSubKeg_"+id_sub_kegiatan).html(
                                            "<p class='alert alert-info'>"+
                                            "    Indikator Tidak Tersedia"+
                                            "</p>"
                                        );
                                        
                                        $("#subkegiatanitem"+item1.id_sub_kegiatan).attr('class', 'nav-link');
                                        $("#subkegiatanitem"+item1.id_sub_kegiatan).html(
                                            "<i class=''></i> "+
                                            "<span>"+item1.nama_sub_kegiatan+"</span>"
                                        );
                                    }else{ //indikator ada
                                        $.each(item1.indikator, function (index2, item2) {
                                            if(item2.realisasiindikatorsubkeg != null )
                                            {
                                                if(item2.realisasiindikatorsubkeg.t_1 != null)
                                                {
                                                    t_1 = item2.realisasiindikatorsubkeg.t_1;
                                                    jmlRIndDiisi = jmlRIndDiisi + 1; //catat realisasi yang diisi
                                                }else{
                                                    t_1 = '';
                                                }
                                                if(item2.realisasiindikatorsubkeg.t_2 != null)
                                                {
                                                    t_2 = item2.realisasiindikatorsubkeg.t_2;
                                                }else{
                                                    t_2 = '';
                                                }
                                                if(item2.realisasiindikatorsubkeg.t_3 != null)
                                                {
                                                    t_3 = item2.realisasiindikatorsubkeg.t_3;
                                                }else{
                                                    t_3 = '';
                                                }
                                                if(item2.realisasiindikatorsubkeg.t_4 != null)
                                                {
                                                    t_4 = item2.realisasiindikatorsubkeg.t_4;
                                                }else{
                                                    t_4 = '';
                                                }
                                            }else{
                                                t_1 = '';
                                                t_2 = '';
                                                t_3 = '';
                                                t_4 = '';
                                            }
                                            // tab sub kegiatan
                                            if(item2.target_perubahan != null)
                                            {
                                                target = item2.target_perubahan;
                                            }else{
                                                target = item2.target;
                                            }
                                            $('#namaIndSubKeg_'+item2.id_indikator_subkegiatan).html(item2.indikator_subkegiatan);
                                            $('#targetIndSubKeg_'+item2.id_indikator_subkegiatan+' .target').html(target);
                                            $('#targetIndSubKeg_'+item2.id_indikator_subkegiatan+' .target_perubahan').html(item2.target_perubahan);
                                            $('#satuanIndSubKeg_'+item2.id_indikator_subkegiatan).html(item2.satuan);
                                            $('#t_1_'+item2.id_indikator_subkegiatan).val(t_1);
                                            $('#t_2_'+item2.id_indikator_subkegiatan).val(t_2);
                                            $('#t_3_'+item2.id_indikator_subkegiatan).val(t_3);
                                            $('#t_4_'+item2.id_indikator_subkegiatan).val(t_4);
                                            // tab analisis faktor sub kegiatan
                                            $('#AFnamaIndSubKeg_'+item2.id_indikator_subkegiatan).html(item2.indikator_subkegiatan);
                                            $('#AFtargetIndSubKeg_'+item2.id_indikator_subkegiatan+' .target').html(target);
                                            $('#AFtargetIndSubKeg_'+item2.id_indikator_subkegiatan+' .target_perubahan').html(item2.target_perubahan);
                                            $('#AFsatuanIndSubKeg_'+item2.id_indikator_subkegiatan).html(item2.satuan);
                                            $('#AFt_1_'+item2.id_indikator_subkegiatan).val(t_1);
                                            $('#AFt_2_'+item2.id_indikator_subkegiatan).val(t_2);
                                            $('#AFt_3_'+item2.id_indikator_subkegiatan).val(t_3);
                                            $('#AFt_4_'+item2.id_indikator_subkegiatan).val(t_4);
                                            if(aksi == 'tambah')
                                            {
                                                index = index2;
                                                id_indikator_subkegiatan = item2.id_indikator_subkegiatan;
                                                indikator_subkegiatan = item2.indikator_subkegiatan;
                                                id_renja = item1.id_renja;
                                                target = item2.target;
                                                target_perubahan = item2.target_perubahan;
                                                satuan = item2.satuan;
                                            }   
                                            // bagian untuk reload capaian
                                            if(parseFloat(target) != 0)
                                            {
                                                persen_tw1 = parseFloat((t_1)/parseFloat(target)*100);
                                                persen_tw2 = parseFloat((t_1+t_2)/parseFloat(target)*100);
                                                persen_tw3 = parseFloat((t_1+t_2+t_3)/parseFloat(target)*100);
                                            }
                                            if(parseFloat(target_perubahan) != 0)
                                            {
                                                persen_perubahan = parseFloat((t_1+t_2+t_3+t_4)/parseFloat(target_perubahan)*100);
                                            }
                                            $('#capaianSubKeg-tw1-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw1+"%'");
                                            $('#capaianSubKeg-tw2-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw2+"%'");
                                            $('#capaianSubKeg-tw3-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw3+"%'");
                                            $('#capaianSubKeg-tw4-'+id_indikator_subkegiatan).css("max-width","'"+persen_perubahan+"%'");
                                            $('#capaianPersenSubKeg-tw1-'+id_indikator_subkegiatan).html(persen_tw1.toFixed(2)+" %");
                                            $('#capaianPersenSubKeg-tw2-'+id_indikator_subkegiatan).html(persen_tw2.toFixed(2)+" %");
                                            $('#capaianPersenSubKeg-tw3-'+id_indikator_subkegiatan).html(persen_tw3.toFixed(2)+" %");
                                            $('#capaianPersenSubKeg-tw4-'+id_indikator_subkegiatan).html(persen_perubahan.toFixed(2)+" %");
                                            $('#capaianAnlSubKeg-tw1-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw1+"%'");
                                            $('#capaianAnlSubKeg-tw2-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw2+"%'");
                                            $('#capaianAnlSubKeg-tw3-'+id_indikator_subkegiatan).css("max-width","'"+persen_tw3+"%'");
                                            $('#capaianAnlSubKeg-tw4-'+id_indikator_subkegiatan).css("max-width","'"+persen_perubahan+"%'");
                                            $('#capaianPersenAnlSubKeg-tw1-'+id_indikator_subkegiatan).html(persen_tw1.toFixed(2)+" %");
                                            $('#capaianPersenAnlSubKeg-tw2-'+id_indikator_subkegiatan).html(persen_tw2.toFixed(2)+" %");
                                            $('#capaianPersenAnlSubKeg-tw3-'+id_indikator_subkegiatan).html(persen_tw3.toFixed(2)+" %");
                                            $('#capaianPersenAnlSubKeg-tw4-'+id_indikator_subkegiatan).html(persen_perubahan.toFixed(2)+" %");
                                        });
                                        // setting ceklis status jumlah realisasi yang sudah di input
                                        if(jmlRIndDiisi == jmlIndikator)
                                        {
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).attr('class', 'nav-link link-success');
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).html(
                                                "<i class='fa fa-check-circle'></i> "+
                                                "<span>"+item1.nama_sub_kegiatan+"</span>"
                                            );
                                            jmlTotalSubKegDiisi = jmlTotalSubKegDiisi + 1;
                                        }else if(jmlRIndDiisi > 0 && jmlRIndDiisi < jmlIndikator)
                                        {
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).attr('class', 'nav-link link-warning');
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).html(
                                                "<i class='fa fa-spinner'></i> "+
                                                "<span>"+item1.nama_sub_kegiatan+"</span>"
                                            );
                                        }else{
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).attr('class', 'nav-link');
                                            $("#subkegiatanitem"+item1.id_sub_kegiatan).html(
                                                "<i></i> "+
                                                "<span>"+item1.nama_sub_kegiatan+"</span>"
                                            );
                                        }
                                        if(aksi == 'tambah'){
                                            var disabled_tw1 = 'disabled';
                                            var disabled_tw2 = 'disabled';
                                            var disabled_tw3 = 'disabled';
                                            var disabled_tw4 = 'disabled';
                                            if(userRole == 1)
                                            {
                                                disabled_tw1 = '';
                                                disabled_tw2 = '';
                                                disabled_tw3 = '';
                                                disabled_tw4 = '';
                                            }
                                            if(twl == 1)
                                            {
                                                disabled_tw1 = '';
                                            }else if(twl == 2)
                                            {
                                                disabled_tw2 = '';
                                            }else if(twl == 3)
                                            {
                                                disabled_tw3 = '';
                                            }else if(twl == 4)
                                            {
                                                disabled_tw4 = '';
                                            }
                                            // tab sub kegiatan
                                            formInd.append(
                                                    "<tr id='detailIndSubKeg_"+id_indikator_subkegiatan+"'>"+
                                                    "    <td>"+
                                                    "        <h5 class='mb-0'>Indikator #"+(index+1)+"</h5>"+
                                                    "        <div class='border border-primary shadow-sm p-3 mb-3'>"+
                                                    "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                                    "                <div class='col-lg-12'>"+ // nama indikator sub kegiatan
                                                    "                    <h6>Nama Indikator</h6>"+
                                                    "                    <div class='fw-bold'>"+indikator_subkegiatan+"</div>"+
                                                    "                    <input class='form-control editIndSKG"+id_indikator_subkegiatan+"' type='text' name='indikator_subkegiatan_"+id_indikator_subkegiatan+"' value='"+indikator_subkegiatan+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // target indikator sub kegiatan
                                                    "                    <h6>Target</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span class='target'>"+String(target)+"</span>"+
                                                    "                        <span class='target_perubahan'>"+target_perubahan+"</span>"+
                                                    "                    </div>"+
                                                    "                    <input class='form-control editIndSKG"+id_indikator_subkegiatan+"' type='text' name='target_"+id_indikator_subkegiatan+"' value='"+target+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // satuan indikator sub kegiatan
                                                    "                    <h6>Satuan</h6>"+
                                                    "                    <div class='fw-bold'>"+satuan+"</div>"+
                                                    "                    <input class='form-control editIndSKG"+id_indikator_subkegiatan+"' type='text' name='satuan_"+id_indikator_subkegiatan+"' value='"+satuan+"'/>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-5'>"+ // progress
                                                    "                   <h6>Capaian Kinerja Sub Kegiatan</h6>"+                                                        
                                                    "                   <div class='capaian mt-3'>"+
                                                    "                       <div class='capaian-bar capaian-tw1'>"+
                                                    "                           <div"+
                                                    "                               id='capaianSubKeg-tw1-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw1+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenSubKeg-tw1-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw1.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw2'>"+
                                                    "                           <div"+
                                                    "                               id='capaianSubKeg-tw2-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw2+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenSubKeg-tw2-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw2.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw3'>"+
                                                    "                           <div"+
                                                    "                               id='capaianSubKeg-tw3-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw3+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenSubKeg-tw3-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw3.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw4'>"+
                                                    "                           <div"+
                                                    "                               id='capaianSubKeg-tw4-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_perubahan+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenSubKeg-tw4-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_perubahan.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                   </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-3'>"+ // teks tercapai atau belum tercapai
                                                    "                    <h6>Status</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span id='badge-sub-kegiatan-progress'"+
                                                    "                                class='badge bg-badge'"+
                                                    "                        >"+
                                                    "                        -"+
                                                    "                        </span>"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                                    "                    <input type='hidden' name='id_indikator_subkegiatan[]' value='"+id_indikator_subkegiatan+"'/>"+
                                                    "                <div class='col-md-3 realisasiTW1'>"+ // realisasi triwulan 1
                                                    "                    <div class='form-floating'>"+
                                                    "                       <input type='hidden' name='triwulan1[]' value='1'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 1"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_1"+id_indikator_subkegiatan+"' name='t_1[]' placeholder='Triwulan 1' value='"+t_1+"'"+disabled_tw1+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW2' style='display: none;'>"+ // realisasi triwulan 2
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan2[]' value='2'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 2"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_2"+id_indikator_subkegiatan+"' name='t_2[]' placeholder='Triwulan 2' value='"+t_2+"'"+disabled_tw2+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW3' style='display: none;'>"+ // realisasi triwulan 3
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan3[]' value='3'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 3"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_3"+id_indikator_subkegiatan+"' name='t_3[]' placeholder='Triwulan 3' value='"+t_3+"'"+disabled_tw3+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-md-3 realisasiTW4' style='display: none;'>"+ // realisasi triwulan 4
                                                    "                    <div class='form-floating'>"+
                                                    "                        <input type='hidden' name='triwulan4[]' value='4'>"+
                                                    "                        <br>"+
                                                    "                        <label>"+
                                                    "                            Triwulan 4"+
                                                    "                        </label><br> "+
                                                    "                        <input type='text' class='form-control' id='t_4"+id_indikator_subkegiatan+"' name='t_4[]' placeholder='Triwulan 4' value='"+t_4+"'"+disabled_tw4+">"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "            <div class='row g-3 mb-3 btnAksiSubKeg"+id_sub_kegiatan+"'>"+ // (awal) row g-3 mb-3
                                                    "                <div class='text-right'>"+                                       
                                                                        // tombol untuk edit indikator
                                                    "                    <button type='button' class='btn btn-sm btn-primary' id='btnUbahIndSKG_"+id_indikator_subkegiatan+"'>"+
                                                    "                        Ubah"+
                                                    "                    </button>"+                                         
                                                    "                    <button type='button' class='btn btn-sm btn-secondary btnAksiUbahIndSKG_"+id_indikator_subkegiatan+"' id='btnBatalUbahIndSKG_"+id_indikator_subkegiatan+"'>"+
                                                    "                        Batal"+
                                                    "                    </button>"+                                          
                                                    "                    <button type='button' class='btn btn-sm btn-primary btnAksiUbahIndSKG_"+id_indikator_subkegiatan+"' onclick='ubahIndSubKeg("+id_indikator_subkegiatan+","+id_sub_kegiatan+","+id_renja+")'>"+
                                                    "                        Simpan Perubahan"+
                                                    "                    </button>"+
                                                                        // tombol untuk hapus indikator
                                                    "                    <button type='button' class='btn btn-sm btn-danger' id='btnHapusIndSKG_"+id_indikator_subkegiatan+"'>"+
                                                    "                        Hapus"+
                                                    "                    </button>"+
                                                    "                    <button type='button' class='btn btn-sm btn-secondary btnAksiHapusIndSKG_"+id_indikator_subkegiatan+"' id='btnBatalHapusIndSKG_"+id_indikator_subkegiatan+"'>"+
                                                    "                        Batal"+
                                                    "                    </button>"+
                                                    "                    <button type='button' class='btn btn-sm btn-danger btnAksiHapusIndSKG_"+id_indikator_subkegiatan+"' onclick='hapusIndSubKeg("+id_indikator_subkegiatan+","+id_sub_kegiatan+")'>"+
                                                    "                        Hapus Indikator"+
                                                    "                    </button>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "       </div>"+
                                                    "    </td>"+
                                                    "</tr>"
                                            );
                                            // tab analisis faktor sub kegiatan
                                            formIndAnl.append(
                                                    "<tr id='AFdetailIndSubKeg_"+id_indikator_subkegiatan+"'>"+
                                                    "    <td>"+
                                                    "        <h5 class='mb-0'>Indikator #"+(index+1)+"</h5>"+
                                                    "        <div class='border border-primary shadow-sm p-3 mb-3'>"+
                                                    "            <div class='row g-3 mb-3'>"+ // (awal) row g-3 mb-3
                                                    "                <div class='col-lg-12'>"+ // nama indikator sub kegiatan
                                                    "                    <h6>Nama Indikator</h6>"+
                                                    "                    <div class='fw-bold'>"+indikator_subkegiatan+"</div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // target indikator sub kegiatan
                                                    "                    <h6>Target</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span class='target'>"+String(target)+"</span>"+
                                                    "                        <span class='target_perubahan'>"+target_perubahan+"</span>"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-2'>"+ // satuan indikator sub kegiatan
                                                    "                    <h6>Satuan</h6>"+
                                                    "                    <div class='fw-bold'>"+satuan+"</div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-5'>"+ // progress
                                                    "                   <h6>Capaian Kinerja Sub Kegiatan</h6>"+                                                        
                                                    "                   <div class='capaian mt-3'>"+
                                                    "                       <div class='capaian-bar capaian-tw1'>"+
                                                    "                           <div"+
                                                    "                               id='capaianAnlSubKeg-tw1-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw1+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenAnlSubKeg-tw1-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw1.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw2'>"+
                                                    "                           <div"+
                                                    "                               id='capaianAnlSubKeg-tw2-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw2+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenAnlSubKeg-tw2-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw2.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw3'>"+
                                                    "                           <div"+
                                                    "                               id='capaianAnlSubKeg-tw3-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_tw3+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenAnlSubKeg-tw3-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_tw3.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                       <div class='capaian-bar capaian-tw4'>"+
                                                    "                           <div"+
                                                    "                               id='capaianAnlSubKeg-tw4-"+id_indikator_subkegiatan+"'"+
                                                    "                               class='capaian-per'"+
                                                    "                               style='max-width: "+persen_perubahan+"%'"+
                                                    "                           >"+
                                                    "                           </div>"+
                                                    "                           <center>"+
                                                    "                               <div style='position: relative; z-index: 1; font-size: 12px;'>"+
                                                    "                                   <b id='capaianPersenAnlSubKeg-tw4-"+id_indikator_subkegiatan+"'>"+
                                                                                            persen_perubahan.toFixed(2)+" %"+
                                                    "                                   </b>"+
                                                    "                               </div>"+
                                                    "                           </center>"+
                                                    "                       </div>"+
                                                    "                   </div>"+
                                                    "                </div>"+
                                                    "                <div class='col-lg-3'>"+ // teks tercapai atau belum tercapai
                                                    "                    <h6>Status</h6>"+
                                                    "                    <div class='fw-bold'>"+
                                                    "                        <span id='badge-sub-kegiatan-progress'"+
                                                    "                                class='badge bg-badge'"+
                                                    "                        >"+
                                                    "                        -"+
                                                    "                        </span>"+
                                                    "                    </div>"+
                                                    "                </div>"+
                                                    "            </div>"+ // (akhir) row g-3 mb-3
                                                    "       </div>"+
                                                    "    </td>"+
                                                    "</tr>"
                                            );
                                            var bwi_mulai = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['mulai']))}}";
                                            var bwi_akhir = "{{date('Y-m-d H:i:s',strtotime(Session::get('bwi')['akhir']))}}";
                                            var sekarang = "{{date('Y-m-d H:i:s')}}";
                                            var user_id_role = "{{Auth::user()->id_role}}";
                                            if(sekarang >= bwi_mulai && sekarang <= bwi_akhir){
                                                if(user_id_role != 13){
                                                    $("#btnSimpanRIndSubKeg_"+id_sub_kegiatan).html(
                                                        "<button id='btnSimpanIndSubKeg"+id_sub_kegiatan+"' type='submit' class='btn btn-primary btn-sm'>"+
                                                        "    Simpan"+
                                                        "</button>"+
                                                        "<i class='fa fa-spinner' id='loadingSubKeg"+id_sub_kegiatan+"'></i>"
                                                    );
                                                }
                                            }else{
                                                if(user_id_role == 1 || user_id_role == 13){
                                                    $("#btnSimpanRIndSubKeg_"+id_sub_kegiatan).html(
                                                        "<button id='btnSimpanIndSubKeg"+id_sub_kegiatan+"' type='submit' class='btn btn-primary btn-sm'>"+
                                                        "    Simpan"+
                                                        "</button>"+
                                                        "<i class='fa fa-spinner' id='loadingSubKeg"+id_sub_kegiatan+"'></i>"
                                                    );
                                                }else{
                                                    $("#btnSimpanRIndSubKeg_"+id_sub_kegiatan).html(
                                                        "<p class='alert alert-warning'>"+
                                                        "    <b>Batas waktu input berakhir atau Kegiatan input belum dimulai</b>"+
                                                        "</p>"
                                                    );
                                                }
                                            }
                                            
                                            //Untuk ubah indikator sub kegiatan
                                            $('.editIndSKG'+id_indikator_subkegiatan).hide();
                                            $('.btnAksiUbahIndSKG_'+id_indikator_subkegiatan).hide();
                                            $('.btnAksiHapusIndSKG_'+id_indikator_subkegiatan).hide();
                                            //Submit Form Realisasi Indikator Sub Kegiatan
                                            $("#messageSubKegSukses"+id_sub_kegiatan).hide();
                                            $("#messageSubKegGagal"+id_sub_kegiatan).hide();
                                            $("#loadingSubKeg"+id_sub_kegiatan).hide();
                                            //Submit Form Tambah Indikator Sub Kegiatan
                                            $("#messageAddIndSubKegSukses"+id_sub_kegiatan).hide();
                                            $("#messageAddIndSubKegGagal"+id_sub_kegiatan).hide();
                                            $("#loadingAddIndSubKeg"+id_sub_kegiatan).hide();
                                            
                                            showHide(twl);
                                            // aksi tombol Ubah 
                                            $('#btnUbahIndSKG_'+id_indikator_subkegiatan).on( "click", function() {
                                                // yang tampil
                                                $('.editIndSKG'+id_indikator_subkegiatan).show();
                                                $('.btnAksiUbahIndSKG_'+id_indikator_subkegiatan).show();
                                                // yang ditutup
                                                $('#btnUbahIndSKG_'+id_indikator_subkegiatan).hide();
                                                $('#btnHapusIndSKG_'+id_indikator_subkegiatan).hide();
                                            });
                                            // aksi tombol Batal Ubah
                                            $('#btnBatalUbahIndSKG_'+id_indikator_subkegiatan).on("click", function () {
                                                // yang tampil
                                                $('.editIndSKG'+id_indikator_subkegiatan).hide();
                                                $('.btnAksiUbahIndSKG_'+id_indikator_subkegiatan).hide();
                                                // yang ditutup
                                                $('#btnUbahIndSKG_'+id_indikator_subkegiatan).show();
                                                $('#btnHapusIndSKG_'+id_indikator_subkegiatan).show();
                                            });
                                            // aksi tombol Hapus
                                            $('#btnHapusIndSKG_'+id_indikator_subkegiatan).on("click", function () {
                                                // yang tampil
                                                $('.btnAksiHapusIndSKG_'+id_indikator_subkegiatan).show();
                                                // yang ditutup
                                                $('#btnUbahIndSKG_'+id_indikator_subkegiatan).hide();
                                                $('#btnHapusIndSKG_'+id_indikator_subkegiatan).hide();
                                            });
                                            // aksi tombol Batal Hapus
                                            $('#btnBatalHapusIndSKG_'+id_indikator_subkegiatan).on("click", function () {
                                                // yang tampil
                                                $('.btnAksiHapusIndSKG_'+id_indikator_subkegiatan).hide();
                                                // yang ditutup
                                                $('#btnUbahIndSKG_'+id_indikator_subkegiatan).show();
                                                $('#btnHapusIndSKG_'+id_indikator_subkegiatan).show();
                                            });
                                        }
                                        updateStatSubKeg();  //update status semua input realisasi indikator
                                    }                                    
                                });
                            }    
        });
    }
    $("input[data-type='currency']").on({
        keyup: function() {
        formatCurrency($(this));
        },
        blur: function() { 
        formatCurrency($(this), "blur");
        }
    });


    function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.
        
        // get input value
        var input_val = input.val();
        
        // don't validate empty input
        if (input_val === "") { return; }
        
        // original length
        var original_len = input_val.length;

        // initial caret position 
        var caret_pos = input.prop("selectionStart");
            
        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);
            
            // On blur make sure 2 numbers after decimal
            if (blur === "blur") {
                right_side;
            }
            
            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;
            
            // final formatting
            if (blur === "blur") {
                input_val;
            }
        }                        
        // send updated string to input
        input.val(input_val);
        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>
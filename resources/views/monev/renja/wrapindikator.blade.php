            <div class="col-lg-12">
                <h5>Indikator <b>#{{$num}}</b> @if($triwulan == 4)  <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button> @endif</h5>
                <div class="white_card mb_30" style="border: solid 4px #E2FFE2;">
                    <div class="white_card_body pt-4 pb-0">
                        <div class="row mb-3" id="indkeg_{{$num}}">
                            <input type="hidden" name="id_indikator_kegiatan[]" id="id_indikator_kegiatan_{{$num}}"
                                value="{{$value['id_indikator_kegiatan']}}">
                            <div class="col-md-{{ ($triwulan != 4) ? 8 : 5; }}">
                                <label class="form-label" id="label_indkeg_{{$num}}">Nama Indikator</label><br>
                                <label class="form-label"><b>{{$value['indikator_kegiatan']}}</b></label>
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" for="inputRealisasi">Target</label><br>
                                <label class="form-label"><b>{{$value['target']}}</b></label>
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" for="#">Satuan</label><br>
                                <label class="form-label"><b>{{$value['satuan']}}</b></label>
                            </div>
                            @if($triwulan == 4)
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Total Persentase Tw. 1-3</label><br>
                                <label class="form-label"><b>{{$total_persentase_murni.' %'}}</b></label>
                            </div>
                            @endif
                            @if($triwulan < 4)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.1</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-1-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase(<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal(<?=$value['target']?>,<?=$num;?>,<?=$total_persentase_murni;?>,1,<?=$value['realisasiindikatorkeg']['persen_1'];?>);" placeholder="" value="{{$value['realisasiindikatorkeg']['t_1']}}" <?=$triwulan>1 ? 'disabled' : '';?> >
                                </div>
                                @if($triwulan > 1)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.2</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-2-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase(<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal(<?=$value['target']?>,<?=$num;?>,<?=$total_persentase_murni;?>,2,<?=$value['realisasiindikatorkeg']['persen_2'];?>);" placeholder="" value="{{$value['realisasiindikatorkeg']['t_2']}}" <?=$triwulan>2 ? 'disabled' : '';?> >
                                </div>
                                @endif
                                @if($triwulan > 2)
                                <div class="col">
                                    <label class="form-label" for="inputRealisasi">Realisasi Tw.3</label>
                                    <input type="text" class="form-control" name="realisasi[]" id="valuetw-3-<?=$num;?>"
                                        onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase(<?=$value['target']?>,value,<?=$num;?>);renderBarPersentaseTotal(<?=$value['target']?>,<?=$num;?>,<?=$total_persentase_murni;?>,3,<?=$value['realisasiindikatorkeg']['persen_2'];?>);" placeholder="" value="{{$value['realisasiindikatorkeg']['t_3']}}">
                                </div>
                                @endif
                            @else
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Target Perubahan</label><br>
                                <input type="text" class="form-control" name="target_perubahan" id="target_perubahan-<?=$num;?>" 
                                    onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase(value,($('#valuetw-4-<?=$num;?>').val()),<?=$num;?>);renderBarPersentaseTotal(value,<?=$num;?>,<?=$total_persentase_murni;?>,4);"  placeholder="" value="{{$value['target_perubahan']}}">
                            </div>
                            <div class="col-md-3">
                                <label class="form-label" for="inputRealisasi">Realisasi Tw.4</label>
                                <input type="text" class="form-control" name="realisasi[]" id="valuetw-4-<?=$num;?>" 
                                    onkeypress="return restrictcomma(event);" onkeyup="renderBarPersentase(($('#target_perubahan-<?=$num;?>').val()),value,<?=$num;?>);renderBarPersentaseTotal(($('#target_perubahan-<?=$num;?>').val()),<?=$num;?>,<?=$total_persentase_murni;?>,4);" placeholder="" value="{{$value['realisasiindikatorkeg']['t_4']}}">
                            </div>
                            @endif
                            <input type="hidden" name="persentase[]" id="persenbar_ind{{$num}}" value="{{$persentase}}">
                            <div class="col-md-2 d-grid">
                                <label class="form-label" >Persentase Tw.{{$triwulan}}</label>
                                <div class="single_progressbar">
                                    <h6 class="f_s_14 f_w_400" >{{$kategori}}</h6>
                                    <div id="bar_ind{{$num}}" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" id="fillbar_ind{{$num}}" data-percentage="{{$persentase}}"></span>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="total_persen[]" id="persenbar_ind_tot{{$num}}" value="{{$total_persentase}}">
                            <div class="col-md-2 d-grid">
                                <label class="form-label" >Total Persentase</label>
                                <div class="single_progressbar">
                                    <h6 class="f_s_14 f_w_400" ></h6>
                                    <div id="bar_ind_tot{{$num}}" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" id="fillbar_ind_tot{{$num}}" data-percentage="{{$total_persentase}}"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="form-label" >Status Verifikasi</label><br>
                                <a href="#" class="badge_active3">belum terverifikasi</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
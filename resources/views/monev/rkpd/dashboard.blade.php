@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <div class="main_content_iner overly_inner ">
        <div class="container-fluid p-0 ">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('monev') }}">Monev</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('monev/rkpd') }}">RKPD</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="white_card mb_30">
                        <div class="white_card_header">
                            <div class="bulder_tab_wrapper">
                                <ul  class="nav" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="iku-tab" data-bs-toggle="tab" href="#iku" role="tab" aria-controls="iku" aria-selected="true">Dashboard IKU</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ikk-tab" data-bs-toggle="tab" href="#ikk" role="tab" aria-controls="ikk" aria-selected="false">Dashboard IKK</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ikp-tab" data-bs-toggle="tab" href="#ikp" role="tab" aria-controls="ikp" aria-selected="false">Dashboard IKP</a>
                                    </li>
                                </ul>
                            </div>
                        </div>                    
                        <div class="white_card_body">            
                            <div class="tab-content" id="myTabContent" style="padding-top: 25px;">
                                <div class="tab-pane fade show active" id="iku" role="tabpanel" aria-labelledby="iku-tab">
                                    @include('monev.rkpd.dashboardIKU')
                                </div>
                                <div class="tab-pane fade" id="ikk" role="tabpanel" aria-labelledby="ikk-tab">
                                    @include('monev.rkpd.dashboardIKK')
                                </div>
                                <div class="tab-pane fade" id="ikp" role="tabpanel" aria-labelledby="ikp-tab">
                                    @include('monev.rkpd.dashboardIKP')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

    <!-- Footer -->
    @include('layouts.footer')
@endsection
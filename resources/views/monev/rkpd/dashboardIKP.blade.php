<div class="row ">
    
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle blue_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tercapai
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    {{ $jmlKtpIKP['tercapai'] }}
                </h4>
            </div>
            <a href="#">
                <div class="card_footer blue_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle green_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tidak Tercapai
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    {{ $jmlKtpIKP['tidak_tercapai'] }}
                </h4>
            </div>
            <a href="#">
                <div class="card_footer green_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle orange_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tidak Ada Data
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    {{ $jmlKtpIKP['tidak_ada_data'] }}
                </h4>
            </div>
            <a href="#">
                <div class="card_footer orange_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle parpel_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Melebihi Capaian
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    {{ $jmlKtpIKP['melebihi_capaian'] }}
                </h4>
            </div>
            <a href="#">
                <div class="card_footer parpel_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    
    <div class="col-xl-12 ">
        <div class="card_box position-relative mb_30 white_bg card_height_100">
            <div class="white_box_tittle  bg-green-custom-1">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white">
                       Indikator Kinerja Program
                    </h4>
                </div>
            </div>
            <div class="box_body">
                <table class="table lms_table_active">
                    <thead>
                        <tr>
                            <th>Perangkat Daerah</th>
                            <th>Program</th>
                            <th>Indikator Program</th>
                            <th>Target</th>
                            <th>Realisasi</th>
                            <th>Capaian</th>
                            <th>Kategori</th> <!-- Persentase Target&Realisasi Beserta Warna Backgroundnya -->
                            <th>Keterangan</th> <!-- Tercapai, Tidak Tercapai, Melebihi Capaian, Tidak Ada Data -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ikp as $dataIKP)
                        @php
                            $bg_ketercapaian = "bg-light";
                            $bg_kategori = "bg-light";
                            
                            // ketercapaian
                            if($dataIKP->KetercapaianRPJMD->id_ketercapaian == 2)
                            {
                                $bg_ketercapaian = "bg-success"; // tercapai
                            }else if($dataIKP->KetercapaianRPJMD->id_ketercapaian == 3)
                            {
                                $bg_ketercapaian = "bg-danger"; // tidak tercapai
                            }else if($dataIKP->KetercapaianRPJMD->id_ketercapaian == 4)
                            {
                                $bg_ketercapaian = "bg-info"; // melebihi capaian
                            }else if($dataIKP->KetercapaianRPJMD->id_ketercapaian == 5)
                            {
                                $bg_ketercapaian = "bg-warning"; // tidak ada data
                            }else
                            {
                                $bg_ketercapaian = "bg-light"; // -
                            }

                            // kategori
                            if($dataIKP->KategoriRPJMD->id_kategori == 2)
                            {
                                $bg_kategori = "bg-green-custom-1"; // sangat tinggi
                            }else if($dataIKP->KategoriRPJMD->id_kategori == 3)
                            {
                                $bg_kategori = "bg-success"; // tinggi
                            }else if($dataIKP->KategoriRPJMD->id_kategori == 4)
                            {
                                $bg_kategori = "bg-orange"; // sedang
                            }else if($dataIKP->KategoriRPJMD->id_kategori == 5)
                            {
                                $bg_kategori = "bg-warning"; // rendah
                            }else if($dataIKP->KategoriRPJMD->id_kategori == 6)
                            {
                                $bg_kategori = "bg-danger"; // sangat rendah
                            }else
                            {
                                $bg_kategori = "bg-light"; // -
                            }
                        @endphp
                        <tr>
                            <td>{{ $dataIKP->IndikatorProgramRPJMD->SKPD->nama_skpd }}</td>
                            <td>{{ $dataIKP->IndikatorProgramRPJMD->ProgramRPJMD->program }}</td>
                            <td>{{ $dataIKP->IndikatorProgramRPJMD->indikator_program }}</td>
                            <td>{{ $dataIKP->target }}</td>
                            <td>{{ $dataIKP->realisasi }}</td>
                            <td class="{{ $bg_ketercapaian }}">
                                {{ $dataIKP->KetercapaianRPJMD->ketercapaian }}
                            </td>
                            <td class="{{ $bg_kategori }}">
                                {{ $dataIKP->KategoriRPJMD->kategori }}
                            </td>
                            <td>{{ $dataIKP->keterangan }}</td>
                        </tr>                            
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xl-12 ">
        <div class="card_box position-relative mb_30 white_bg card_height_100">
            <div class="white_box_tittle  bg-green-custom-1">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white">
                       Rekapitulasi Input Data Indikator Kinerja Program
                    </h4>
                </div>
            </div>
            <div class="box_body">
                <div class="row">
                    <div class="d-flex flex-row-reverse">
                        <a class="btn btn-primary" href="#">
                            <i class="fa fa-print"></i> | Cetak
                        </a>
                    </div>
                </div>
                <div class="row">
                    <table class="table lms_table_active">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Perangkat Daerah</th>
                                <th>Jumlah Indikator Program</th>
                                <th>Jumlah Indikator Program yang Belum Diinput</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($skpd as $key => $dataSKPD)           
                            <tr>
                                <td>{{ ($key+1) }}</td>
                                <td>{{ $dataSKPD->nama_skpd }}</td>
                                <td>{{ $jmlIKP[$dataSKPD->id_skpd] }}</td>
                                <td>{{ $ikpBelum[$dataSKPD->id_skpd] }}</td>
                            </tr>                                
                            @endforeach                 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
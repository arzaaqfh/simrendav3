<div class="row ">
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle blue_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tercapai
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    0
                </h4>
            </div>
            <a href="#">
                <div class="card_footer blue_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle green_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tidak Tercapai
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    0
                </h4>
            </div>
            <a href="#">
                <div class="card_footer green_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle orange_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Tidak Ada Data
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    0
                </h4>
            </div>
            <a href="#">
                <div class="card_footer orange_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    <div class="col-xl-3">
        <div class="card_box position-relative  mb_30 white_bg  ">
            <div class="white_box_tittle parpel_bg">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white text-center">
                        Melebihi Capaian
                    </h4>
                </div>
            </div>
            <div class="box_body">                
                <h4 class="mb-2 nowrap text-center">
                    0
                </h4>
            </div>
            <a href="#">
                <div class="card_footer parpel_bg">
                    <center>
                        <h4 class="text_white">DETAIL</h4>
                    </center>
                </div>
            </a>
        </div>        
    </div>
    <div class="col-xl-12">   
        <div class="white_box mb_30">
            <div id="grafCapaian"></div>
            <script>
                $(document).ready(function(){
                    var options = {
                        series: [{
                            name: 'Melebihi Capaian',
                            data: [50, 50, 50, 50, 50, 50, 50]
                        }, {
                            name: 'Tercapai',
                            data: [70, 70, 70, 70, 70, 70, 70]
                        }, {
                            name: 'Tidak Tercapai',
                            data: [30, 30, 30, 30, 30, 30, 30]
                        }, {
                            name: 'Tidak Ada Data',
                            data: [10, 10, 10, 10, 10, 10, 10]
                        }],
                        chart: {
                            height: 350,
                            type: 'area'
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth'
                        },
                        xaxis: {
                            type: 'datetime',
                            categories: [
                                "2018-09-19T00:00:00.000Z", 
                                "2018-09-19T01:30:00.000Z", 
                                "2018-09-19T02:30:00.000Z", 
                                "2018-09-19T03:30:00.000Z", 
                                "2018-09-19T04:30:00.000Z", 
                                "2018-09-19T05:30:00.000Z", 
                                "2018-09-19T06:30:00.000Z"
                            ]
                        },
                        tooltip: {
                            x: {
                                format: 'dd/MM/yy HH:mm'
                            },
                        },
                    };

                    var chart = new ApexCharts(document.querySelector("#grafCapaian"), options);
                    chart.render();
                });
            </script>
        </div>
    </div>
    <div class="col-xl-12 ">
        <div class="card_box position-relative mb_30 white_bg card_height_100">
            <div class="white_box_tittle  bg-green-custom-1">
                <div class="main-title2">
                    <h4 class="mb-2 nowrap text_white">
                       Indikator Kinerja Utama
                    </h4>
                </div>
            </div>
            <div class="box_body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Misi Ke-</th>
                            <th>Misi</th>
                            <th>Indikator Kinerja Utama</th>
                            <th>Target</th>
                            <th>Realisasi</th>
                            <th>Kategori</th> <!-- Persentase Target&Realisasi Beserta Warna Backgroundnya -->
                            <th>Keterangan</th> <!-- Tercapai, Tidak Tercapai, Melebihi Capaian, Tidak Ada Data -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($misi as $data)
                            @if (!empty($iku[$data->id_misi_rpjmd]))
                                @php
                                    $count = count($iku[$data->id_misi_rpjmd])+1;        
                                @endphp
                            <tr>
                                <td rowspan="{{ $count }}">{{ $data->id_misi_rpjmd }}</td>
                                <td rowspan="{{ $count }}">{{ $data->misi_rpjmd }}</td>
                                @foreach ($iku[$data->id_misi_rpjmd] as $dataIku)
                                <tr>
                                    <td>{{ $dataIku[0]->indikator_sasaran }}<br/></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    @foreach ($ikk as $data)
                    <div class="white_card card_height_auto mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-12">                                        
                                        <h3 class="m-0">Aspek</h3><br/>
                                        <h5 class="m-0 alert alert-success"> 
                                            {{ $data->IndikatorKinerjaRPJMD->Aspek->aspek }}
                                        </h5>
                                    </div>                                   
                                </div><hr/>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <h3 class="m-0">Indikator Kinerja Kunci</h3><br/>
                                        <h5 class="m-0 alert alert-warning">
                                            {{ $data->IndikatorKinerjaRPJMD->indikator_kinerja }}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{ url('monev/rkpd/ikk/update') }}"> 
                    {{ csrf_field() }}
                    <input type="text" name="id_target_realisasi" value="{{ $data->id_target_realisasi }}" hidden>
                    <input type="text" name="rumus" id="rumus" value="{{ $data->IndikatorKinerjaRPJMD->Rumus->id_rumus }}" hidden> 
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Realisasi</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-2">
                                        <label class="form-label" for="target">Target</label>
                                        <input name="target" type="text" class="form-control" id="target" placeholder="" value="{{ $data->target }}" readonly>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="form-label" for="realisasi">Realisasi</label>
                                        <input name="realisasi" type="text" class="form-control" id="realisasi" placeholder="" value="{{ $data->realisasi }}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="form-label" for="satuan">Satuan</label>
                                        <input name="satuan" type="text" class="form-control" id="satuan" placeholder="" value="{{ $data->IndikatorKinerjaRPJMD->SatuanRPJMD->satuan }}" readonly>
                                    </div>                                  
                                    <div class="col-md-2">
                                        <label class="form-label" for="sumberData">Sumber Data</label>
                                        <select name="sumberData" id="sumberData" class="form-select">
                                         <option selected="">Pilih...</option>
                                             @foreach ($sumberData as $dataSumberData)
                                                 <option 
                                                    @if ($data->id_sumber_data == $dataSumberData->id_sumber_data)
                                                        selected="true"
                                                    @endif 
                                                    value="{{ $dataSumberData->id_sumber_data }}"
                                                 >
                                                    {{ $dataSumberData->sumber_data }}
                                                 </option>
                                             @endforeach
                                            <option value="lainnya">Lainnya</option>
                                        </select>
                                        <input class="form-control" type="text" id="sumberDataLainnya" name="sumberDataLainnya" placeholder="sumber lain"/>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="form-label" for="ketercapaian">Ketercapaian</label>
                                        <select name="ketercapaian" id="ketercapaian" class="form-select">
                                            <option>Pilih...</option>
                                            @foreach ($ketercapaian as $dataKetercapaian)
                                                <option
                                                    @if ($data->KetercapaianRPJMD->id_ketercapaian == $dataKetercapaian->id_ketercapaian)
                                                        selected="true"
                                                    @endif 
                                                    value="{{ $dataKetercapaian->id_ketercapaian }}"
                                                >
                                                    {{ $dataKetercapaian->ketercapaian }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                       <label class="form-label" for="kategori">Kategori</label>
                                       <select name="kategori" id="kategori" class="form-select">
                                            <option selected="">Pilih...</option>
                                            @foreach ($kategori as $dataKategori)
                                                <option 
                                                    @if ($data->KategoriRPJMD->id_kategori == $dataKategori->id_kategori)
                                                        selected="true"
                                                    @endif 
                                                    value="{{ $dataKategori->id_kategori }}"
                                                >
                                                    {{ $dataKategori->kategori }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <label class="form-label" for="keterangan">Keterangan</label>
                                        <textarea class="form-control" name="keterangan" id="keterangan" cols="30" rows="10">{{ $data->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ url('monev/rkpd/ikk') }}" class="btn btn-secondary">Batal</a>
                            </div>
                        </div>
                    </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>   

    <!-- Footer -->
    @include('layouts.footer')
@endsection
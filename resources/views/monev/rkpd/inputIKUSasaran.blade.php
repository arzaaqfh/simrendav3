@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    @foreach ($ikuSasaran as $data)
                    <div class="white_card card_height_auto mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-12">                                        
                                        <h3 class="m-0">Misi</h3><br/>
                                        <h5 class="m-0 alert alert-success">
                                            {{ $data->IndikatorSasaranRPJMD->SasaranRPJMD->TujuanRPJMD->MisiRPJMD->nomor.'. '.$data->IndikatorSasaranRPJMD->SasaranRPJMD->TujuanRPJMD->MisiRPJMD->misi }}
                                        </h5>
                                    </div>                                   
                                </div><hr/>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <h3 class="m-0">Indikator Kinerja Utama Sasaran</h3><br/>
                                        <h5 class="m-0 alert alert-warning">
                                            {{ $data->IndikatorSasaranRPJMD->indikator_sasaran }}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{ url('monev/rkpd/ikuSasaran/update') }}">
                    {{ csrf_field() }}
                    
                    <input type="text" name="id_target_realisasi" value="{{ $data->id_target_realisasi }}" hidden>
                    <input type="text" name="rumus" id="rumus" value="{{ $data->IndikatorSasaranRPJMD->Rumus->id_rumus }}" hidden>

                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Realisasi</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-3">
                                        <label class="form-label" for="target_ssp">Target Sesudah Perubahan</label>
                                        <input name="target" type="text" class="form-control" id="target_ssp" placeholder="" value="{{ $data->target_ssp }}" readonly>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="target_sbp">Target Sebelum Perubahan</label>
                                        <input name="target" type="text" class="form-control" id="target_sbp" placeholder="" value="{{ $data->target_sbp }}" readonly>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="realisasi">Realisasi</label>
                                        <input name="realisasi" type="text" class="form-control" id="realisasi" placeholder="" value="{{ $data->realisasi }}">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="satuan">Satuan</label>
                                        <input name="satuan" type="text" class="form-control" id="satuan" placeholder="" value="{{ $data->IndikatorSasaranRPJMD->SatuanRPJMD->satuan }}" readonly>
                                    </div>
                                </div>
                                <div class="row mb-3">                                   
                                    <div class="col-md-4">
                                        <label class="form-label" for="sumberData">Sumber Data</label>
                                        <select name="sumberData" id="sumberData" class="form-select">
                                            <option selected="">Pilih...</option>
                                            @foreach ($sumberData as $dataSumberData)
                                                <option 
                                                @if ($data->id_sumber_data == $dataSumberData->id_sumber_data)
                                                    selected="true"
                                                @endif 
                                                value="{{ $dataSumberData->id_sumber_data }}"
                                                >
                                                {{ $dataSumberData->sumber_data }}
                                                </option>
                                            @endforeach
                                            <option value="lainnya">Lainnya</option>
                                         </select>
                                         <input class="form-control" type="text" id="sumberDataLainnya" name="sumberDataLainnya" placeholder="sumber lain"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="ketercapaian">Ketercapaian</label>
                                        <select name="ketercapaian" id="ketercapaian" class="form-select">
                                            <option>Pilih...</option>
                                            @foreach ($ketercapaian as $dataKetercapaian)
                                                <option
                                                    @if ($data->KetercapaianRPJMD->id_ketercapaian == $dataKetercapaian->id_ketercapaian)
                                                        selected="true"
                                                    @endif 
                                                    value="{{ $dataKetercapaian->id_ketercapaian }}"
                                                >
                                                    {{ $dataKetercapaian->ketercapaian }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                       <label class="form-label" for="kategori">Kategori</label>
                                       <select name="kategori" id="kategori" class="form-select">
                                        <option selected="">Pilih...</option>
                                            @foreach ($kategori as $dataKategori)
                                                <option 
                                                    @if ($data->KategoriRPJMD->id_kategori == $dataKategori->id_kategori)
                                                        selected="true"
                                                    @endif 
                                                    value="{{ $dataKategori->id_kategori }}"
                                                >
                                                    {{ $dataKategori->kategori }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <label class="form-label" for="keterangan">Keterangan</label>
                                        <textarea class="form-control" name="keterangan" id="keterangan" cols="30" rows="10">{{ $data->keterangan }}</textarea>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Faktor Pendorong</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="card-body">
                                @php
                                    $faktorPendorong_dana = '';
                                    $faktorPendorong_sdm = '';
                                    $faktorPendorong_waktu_pelaksanaan = '';
                                    $faktorPendorong_peraturan_perundangan = '';
                                    $faktorPendorong_sistem_pengadaan_barang_jasa = '';
                                    $faktorPendorong_perijinan = '';
                                    $faktorPendorong_ketersediaan_lahan = '';
                                    $faktorPendorong_kesiapan_dukungan_masyarakat = '';
                                    $faktorPendorong_faktor_alam = '';
                                    
                                    if(count($faktorPD)>0)
                                    {
                                        foreach ($faktorPD as $dataFPD) {                                            
                                            $faktorPendorong_dana = $dataFPD->dana;
                                            $faktorPendorong_sdm = $dataFPD->sdm;
                                            $faktorPendorong_waktu_pelaksanaan = $dataFPD->waktu_pelaksanaan;
                                            $faktorPendorong_peraturan_perundangan = $dataFPD->peraturan_perundangan;
                                            $faktorPendorong_sistem_pengadaan_barang_jasa = $dataFPD->sistem_pengadaan_barang_jasa;
                                            $faktorPendorong_perijinan = $dataFPD->perijinan;
                                            $faktorPendorong_ketersediaan_lahan = $dataFPD->ketersediaan_lahan;
                                            $faktorPendorong_kesiapan_dukungan_masyarakat = $dataFPD->kesiapan_dukungan_masyarakat;
                                            $faktorPendorong_faktor_alam = $dataFPD->faktor_alam;
                                        }
                                    }
                                @endphp
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputDanaFP">Dana</label>
                                        <input name="inputDanaFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_dana }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSDMFP">SDM</label>
                                        <input name="inputSDMFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_sdm }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputWaktuPelaksanaanFP">Waktu Pelaksanaan</label>
                                        <input name="inputWaktuPelaksanaanFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_waktu_pelaksanaan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPeraturanPerundanganFP">Peraturan Perundangan</label>
                                        <input name="inputPeraturanPerundanganFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_peraturan_perundangan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFP">Sistem Pengadaan Barang/Jasa</label>
                                        <input name="inputSistemPengadaanBarangJasaFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_sistem_pengadaan_barang_jasa }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPerijinanFP">Perijinan</label>
                                        <input name="inputPerijinanFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_perijinan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKetersediaanLahanFP">Ketersediaan Lahan</label>
                                        <input name="inputKetersediaanLahanFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_ketersediaan_lahan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFP">Kesiapan/Dukungan Masyarakat</label>
                                        <input name="inputKesiapanDukunganMasyarakatFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_kesiapan_dukungan_masyarakat }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputFaktorAlamFP">Faktor Alam</label>
                                        <input name="inputFaktorAlamFP" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPendorong_faktor_alam }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Faktor Penghambat</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="card-body">
                                @php
                                    $faktorPenghambat_dana = '';
                                    $faktorPenghambat_sdm = '';
                                    $faktorPenghambat_waktu_pelaksanaan = '';
                                    $faktorPenghambat_peraturan_perundangan = '';
                                    $faktorPenghambat_sistem_pengadaan_barang_jasa = '';
                                    $faktorPenghambat_perijinan = '';
                                    $faktorPenghambat_ketersediaan_lahan = '';
                                    $faktorPenghambat_kesiapan_dukungan_masyarakat = '';
                                    $faktorPenghambat_faktor_alam = '';
                                    
                                    if(count($faktorPH)>0)
                                    {
                                        foreach ($faktorPH as $dataFPH) {                                            
                                            $faktorPenghambat_dana = $dataFPH->dana;
                                            $faktorPenghambat_sdm = $dataFPH->sdm;
                                            $faktorPenghambat_waktu_pelaksanaan = $dataFPH->waktu_pelaksanaan;
                                            $faktorPenghambat_peraturan_perundangan = $dataFPH->peraturan_perundangan;
                                            $faktorPenghambat_sistem_pengadaan_barang_jasa = $dataFPH->sistem_pengadaan_barang_jasa;
                                            $faktorPenghambat_perijinan = $dataFPH->perijinan;
                                            $faktorPenghambat_ketersediaan_lahan = $dataFPH->ketersediaan_lahan;
                                            $faktorPenghambat_kesiapan_dukungan_masyarakat = $dataFPH->kesiapan_dukungan_masyarakat;
                                            $faktorPenghambat_faktor_alam = $dataFPH->faktor_alam;
                                        }
                                    }
                                @endphp
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputDanaFPH">Dana</label>
                                        <input name="inputDanaFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_dana }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSDMFPH">SDM</label>
                                        <input name="inputSDMFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_sdm }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputWaktuPelaksanaanFPH">Waktu Pelaksanaan</label>
                                        <input name="inputWaktuPelaksanaanFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_waktu_pelaksanaan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPeraturanPerundanganFPH">Peraturan Perundangan</label>
                                        <input name="inputPeraturanPerundanganFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_peraturan_perundangan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSistemPengadaanBarangJasaFPH">Sistem Pengadaan Barang/Jasa</label>
                                        <input name="inputSistemPengadaanBarangJasaFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_sistem_pengadaan_barang_jasa }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPerijinanFPH">Perijinan</label>
                                        <input name="inputPerijinanFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_perijinan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKetersediaanLahanFPH">Ketersediaan Lahan</label>
                                        <input name="inputKetersediaanLahanFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_ketersediaan_lahan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatFPH">Kesiapan/Dukungan Masyarakat</label>
                                        <input name="inputKesiapanDukunganMasyarakatFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_kesiapan_dukungan_masyarakat }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputFaktorAlamFPH">Faktor Alam</label>
                                        <input name="inputFaktorAlamFPH" type="text" class="form-control" id="" placeholder="" value="{{ $faktorPenghambat_faktor_alam }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Tindak Lanjut</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="card-body">
                                @php
                                    $tindakLanjut_dana = '';
                                    $tindakLanjut_sdm = '';
                                    $tindakLanjut_waktu_pelaksanaan = '';
                                    $tindakLanjut_peraturan_perundangan = '';
                                    $tindakLanjut_sistem_pengadaan_barang_jasa = '';
                                    $tindakLanjut_perijinan = '';
                                    $tindakLanjut_ketersediaan_lahan = '';
                                    $tindakLanjut_kesiapan_dukungan_masyarakat = '';
                                    $tindakLanjut_faktor_alam = '';
                                    
                                    if(count($tindakLJ)>0)
                                    {
                                        foreach ($tindakLJ as $dataTL) {                                            
                                            $tindakLanjut_dana = $dataTL->dana;
                                            $tindakLanjut_sdm = $dataTL->sdm;
                                            $tindakLanjut_waktu_pelaksanaan = $dataTL->waktu_pelaksanaan;
                                            $tindakLanjut_peraturan_perundangan = $dataTL->peraturan_perundangan;
                                            $tindakLanjut_sistem_pengadaan_barang_jasa = $dataTL->sistem_pengadaan_barang_jasa;
                                            $tindakLanjut_perijinan = $dataTL->perijinan;
                                            $tindakLanjut_ketersediaan_lahan = $dataTL->ketersediaan_lahan;
                                            $tindakLanjut_kesiapan_dukungan_masyarakat = $dataTL->kesiapan_dukungan_masyarakat;
                                            $tindakLanjut_faktor_alam = $dataTL->faktor_alam;
                                        }
                                    }
                                @endphp
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputDanaTL">Dana</label>
                                        <input name="inputDanaTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_dana }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSDMTL">SDM</label>
                                        <input name="inputSDMTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_sdm }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputWaktuPelaksanaanTL">Waktu Pelaksanaan</label>
                                        <input name="inputWaktuPelaksanaanTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_waktu_pelaksanaan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPeraturanPerundanganTL">Peraturan Perundangan</label>
                                        <input name="inputPeraturanPerundanganTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_peraturan_perundangan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputSistemPengadaanBarangJasaTL">Sistem Pengadaan Barang/Jasa</label>
                                        <input name="inputSistemPengadaanBarangJasaTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_sistem_pengadaan_barang_jasa }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputPerijinanTL">Perijinan</label>
                                        <input name="inputPerijinanTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_perijinan }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKetersediaanLahanTL">Ketersediaan Lahan</label>
                                        <input name="inputKetersediaanLahanTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_ketersediaan_lahan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputKesiapanDukunganMasyarakatTL">Kesiapan/Dukungan Masyarakat</label>
                                        <input name="inputKesiapanDukunganMasyarakatTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_kesiapan_dukungan_masyarakat }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputFaktorAlamTL">Faktor Alam</label>
                                        <input name="inputFaktorAlamTL" type="text" class="form-control" id="" placeholder="" value="{{ $tindakLanjut_faktor_alam }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                      
                    @endforeach
                    
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ url('monev/rkpd/ikuSasaran') }}" class="btn btn-secondary">Batal</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>   

    <!-- Footer -->
    @include('layouts.footer')
@endsection
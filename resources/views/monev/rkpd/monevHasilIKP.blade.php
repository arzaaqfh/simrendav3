@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('monev.rkpd.sidebar')
                
    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('monev') }}">Monev</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('monev/rkpd') }}">RKPD</a></li>
                                <li class="breadcrumb-item active">IKP</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Monev Hasil</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Indikator Kinerja Program</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" placeholder="Search content here...">
                                                    </div>
                                                    <button type="submit"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table  class="table table_monevhasilikp">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Indikator Kinerja Program</th>
                                                <th>Target</th>
                                                <th>Realisasi</th>
                                                <th>Satuan</th>
                                                <th>Capaian</th>
                                                <th>Tahun</th>
                                                <th>Perangkat Daerah</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_monevhasilikp').DataTable({
                dom: 'lrtip',                
                lengthMenu: [10, 20, 50, 100],
                ajax: {
                    url:"{{ url('/api/monev/rkpd/hasil/IKP') }}"+"/2022"+"/4"
                },
                columns: [
                    {data: 'id_target_realisasi',orderable: false,searchable: false},
                    {data: 'indikator_program', name: 'indikator_program'},
                    {data: 'target', name: 'target'},
                    {data: 'realisasi', name: 'realisasi'},
                    {data: 'satuan', name: 'satuan'},
                    {
                        data: 'target',
                        render: function( data, type, full, meta) {
                            var trfTarget = "";
                            var trfRealisasi = "";
                            var bg_ketercapaian = "bg-light";
                            var bg_kategori = "bg-light";
                            var persen = 0;
                            var returnText = "";
                            var target = "";  
                            var realisasi = "";

                            if(full.target != null && full.realisasi != null)
                            {
                                trfTarget = full.target.replace(',','.');
                                trfRealisasi = full.realisasi.replace(',','.');
                            }else
                            {
                                trfTarget = 0;
                                trfRealisasi = 0;
                            }

                            if($.isNumeric(trfTarget) && $.isNumeric(trfRealisasi))
                            {
                                target = parseFloat(trfTarget);  
                                realisasi = parseFloat(trfRealisasi);
                            }else
                            {
                                target = 0;  
                                realisasi = 0;
                            }

                            if(target != NaN && realisasi != NaN && target > 0)
                            {
                                if(full.id_rumus == 1) //rumus positif
                                {
                                    persen = Math.round((realisasi/target)*100);
                                }else{
                                    persen = 100-Math.round((realisasi/target)*100);
                                }
                            }
                            // ketercapaian
                            if(full.id_ketercapaian == 2)
                            {
                                bg_ketercapaian = "bg-success"; // tercapai
                            }else if(full.id_ketercapaian == 3)
                            {
                                bg_ketercapaian = "bg-danger"; // tidak tercapai
                            }else if(full.id_ketercapaian == 4)
                            {
                                bg_ketercapaian = "bg-info"; // melebihi capaian
                            }else if(full.id_ketercapaian == 5)
                            {
                                bg_ketercapaian = "bg-warning"; // tidak ada data
                            }else
                            {
                                bg_ketercapaian = "bg-light"; // -
                            }

                            // kategori
                            if(full.id_kategori == 2)
                            {
                                bg_kategori = "bg-green-custom-1"; // sangat tinggi
                            }else if(full.id_kategori == 3)
                            {
                                bg_kategori = "bg-success"; // tinggi
                            }else if(full.id_kategori == 4)
                            {
                                bg_kategori = "bg-orange"; // sedang
                            }else if(full.id_kategori == 5)
                            {
                                bg_kategori = "bg-warning"; // rendah
                            }else if(full.id_kategori == 6)
                            {
                                bg_kategori = "bg-danger"; // sangat rendah
                            }else
                            {
                                bg_kategori = "bg-light"; // -
                            }

                            returnText =    "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <span class='badge "+bg_ketercapaian+" mb-2'>"+full.ketercapaian+"</span>"+
                                            "    </div>"+
                                            "</div>"+
                                            "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <span class='badge "+bg_kategori+" mb-2'>"+full.kategori+"</span>"+
                                            "    </div>"+
                                            "</div>"+
                                            "<div class='row'>"+
                                            "    <div class='col-lg-12'>"+
                                            "        <br/>"+
                                            "        <div class='single_progressbar'>"+
                                            "            <div id='bar"+full.id_target_realisasi+"' class='barfiller'>"+
                                            "                <div class='tipWrap' style='display: inline;'>"+
                                            "                    <span class='tip' style='left: 116.095px; transition: left 2.2s ease-in-out 0s;'>"+persen+"</span>"+
                                            "                </div>"+
                                            "                <span class='fill' data-percentage='"+persen+"' style='background: rgb(253, 60, 151); width: 139.611px; transition: width 2.2s ease-in-out 0s;'></span>"+
                                            "            </div>"+
                                            "        </div>"+
                                            "    </div>"+
                                            "</div>";
                            return returnText;
                        } 
                    },
                    {data: 'tahun', name: 'tahun'},
                    {data: 'nama_skpd', name: 'perangkat_daerah'},
                    {
                        data: 'id_target_realisasi',
                        render: function(data, type, full, meta){
                            var url = "{{ url('/monev/rkpd/ikp/input/') }}"; 
                            var returnText = ""; 
                            
                            if(full.tahun > 2020)
                            {                                           
                                returnText =    "<a class='btn btn-success' href='"+url+"/"+data+"'>"+
                                                "    <i class='fa fa-plus'></i>"+
                                                "</a>";
                            }else
                            {
                                returnText =    "-";
                            }
                            return returnText;
                        }
                    },                                
                ]
            });
            
            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        })
    </script> 

    <!-- Footer -->
    @include('layouts.footer')
@endsection
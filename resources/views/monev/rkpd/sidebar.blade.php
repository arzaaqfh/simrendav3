<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-green-custom-1">
        <a class="large_logo" href="{{ url('home')}}">
            <img src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>Rencana Kerja Pemerintah Daerah</b></h5>
        </a>
             
        <a class="small_logo" href="{{ url('home')}}">
            <img src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>RKPD</b></h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4> 
        <li>
            <a href="{{ url('home') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-home" ></i>
                </div>
                <div class="nav_title">
                    <span>Home</span>
                </div>
            </a>
        </li> 
        <li>
            <a href="{{ url('monev') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-warehouse" ></i>
                </div>
                <div class="nav_title">
                    <span>Monev</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('monev/rkpd') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard</span>
                </div>
            </a>
        </li>

        <h4 class="menu-text"><span>MENU UTAMA</span> <i class="fas fa-ellipsis-h"></i> </h4>     
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Monev Hasil </span>
                </div>
            </a>
            <ul>
                @if( session()->get('id_role') == 1 )
                <li>                    
                    <a href="{{ url('monev/rkpd/ikuTujuan') }}">IKU Tujuan</a></li>
                </li>
                <li>                    
                    <a href="{{ url('monev/rkpd/ikuSasaran') }}">IKU Sasaran</a></li>
                </li>
                @endif
                <li>
                    <a href="{{ url('monev/rkpd/ikk') }}">Indikator Kinerja Kunci</a>
                </li>
                <li>
                    <a href="{{ url('monev/rkpd/ikp') }}">Indikator Kinerja Program</a>
                </li>
            </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-download"></i>
                </div>
                <div class="nav_title">
                    <span>Unduh </span>
                </div>
            </a>
            <ul>
              <li>
                <a href="{{ url('#') }}">Bahan Rakor Monev</a>
              </li>
            </ul>
        </li>

        <h4 class="menu-text"><span>MENU LAINNYA</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li class="">
            <a href="{{ url('#') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/2.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Belum Tersedia </span>
                </div>
            </a>
        </li>

      </ul>
</nav>
 <!--/ sidebar  -->
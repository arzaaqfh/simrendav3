@extends('layouts.app') 

@section('content')
<!-- DAFTAR MODUL -->
<section class="page-section" style="background-image: linear-gradient(#53bce9ee,#fbf6f0); padding-top: 15px; padding-bottom: 15px; color: #0d6287"><br/>
    <center>
        <h1 class="display-4"><b>DAFTAR SUB MODUL</b></h1>
        <h2 class="display-6"><b>MUSRENBANG</b></h2>
    </center><br/>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 p-0">
                <a href="{{ url('/home') }}" class="header_iner d-flex justify-content-start align-items-center" style="background: none;">
                    <div class="header_right d-flex justify-content-start align-items-center" style="padding: 10px 10px 10px 10px; background-color: #0d6287; border-radius: 100%;">
                        <div class="btn">
                            <i class="fa fa-lg fa-home text-white" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 p-0">
                <div class="header_iner d-flex justify-content-end align-items-center" style="background: none;">
                    <div class="header_right d-flex justify-content-end align-items-center" style="padding: 10px 10px 10px 10px; background-color: #0d6287; border-radius: 100%;">
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item dropdown no-arrow">
                                <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item dropdown no-arrow">
                                <a class="dropdown-item" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else
                            <div class="profile_info">
                                <img src="{{ asset('storage/img/profile_picture.png') }}" alt="#">
                                <div class="profile_info_iner">
                                    <div class="profile_author_name">
                                        {{-- <p>{{ Auth::user()->role }} </p> --}}
                                        <h5>{{ Auth::user()->nama_pengguna }}</h5>
                                    </div>
                                    <div class="profile_info_details">
                                        <a href="#">
                                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Profile
                                        </a><!-- 
                                        <a href="#">
                                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Settings
                                        </a>
                                        <a href="#">
                                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Activity Log
                                        </a> -->
                                        <div class="dropdown-divider"></div>
                                        @if (Auth::user()->role == 'Admin')
                                        <a href="{{ route('register') }}">
                                            <i class="fas fa-id-card fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Add Account
                                        </a>
                                        @endif
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#logoutModal">
                                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Logout
                                        </a>   
                                    </div>
                                </div>
                            </div>
                    @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <a href="{{ url('musrenbang/kota') }}" class="col m4 moduls">
                <center>
                    <div class="btn" style="margin: 15px 0 15px 0; background-color: #ff6464;">
                        <img src="{{ url('storage/img/planning.png') }}" height="150px">
                    </div>
                    <h4 class="text-white">KOTA</h4>                    
                    <p class="text-white">Musrenbang - Kota</p>
                </center>
            </a>
        </div>
    </div>
</section>



<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logoutModalTitle">Anda akan keluar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Klik tombol "Logout" di bawah ini untuk keluar.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('musrenbang.kota.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <div class="main_content_iner overly_inner ">
            <div class="container-fluid p-0 ">
                <!-- page title  -->
                <div class="row">
                    <div class="col-12">
                        <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                            <div class="page_title_left">
                                <h3 class="f_s_25 f_w_700 dark_text" >Dashboard</h3>
                                <ol class="breadcrumb page_bradcam mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/musrenbang') }}">Musrenbang</a></li>
                                    <li class="breadcrumb-item active">Kota</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="white_card card_height_100 mb_30 social_media_card">
                        <div class="media_thumb ml_25">
                            <img src="img/media.svg" alt="">
                        </div>
                        <div class="media_card_body">
                            <div class="media_card_list">
                                <div class="single_media_card">
                                    <span>Followers</span>
                                    <h3>35.6 K</h3>
                                </div>
                                <div class="single_media_card">
                                    <span>Followers</span>
                                    <h3>35.6 K</h3>
                                </div>
                                <div class="single_media_card">
                                    <span>Followers</span>
                                    <h3>35.6 K</h3>
                                </div>
                                <div class="single_media_card">
                                    <span>Followers</span>
                                    <h3>35.6 K</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        @include('layouts.footer')
        
@endsection
<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-danger">
        <a class="large_logo" href="{{ url('home') }}">
            <img src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>Musyawarah Rencana Pembangunan</b></h5>
        </a>

        <a class="small_logo" href="{{ url('home') }}">
            <img src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>Musrenbang</b></h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu" class="metismenu">
        
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4> 
        <li>
            <a href="{{ url('home') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-home" ></i>
                </div>
                <div class="nav_title">
                    <span>Home</span>
                </div>
            </a>
        </li> 
        <li>
            <a href="{{ url('musrenbang') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-warehouse" ></i>
                </div>
                <div class="nav_title">
                    <span>Musrenbang</span>
                </div>
            </a>
        </li>
        <li>
            <a href="{{ url('musrenbang/kota') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard Kota</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/dashboard.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard Kecamatan</span>
                </div>
            </a>
        </li>

        <h4 class="menu-text"><span>MENU UTAMA</span> <i class="fas fa-ellipsis-h"></i> </h4> 
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>RW</span>
                </div>
            </a>
            <ul>
               
            </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Kelurahan</span>
                </div>
            </a>
            <ul>
               
            </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Kecamatan</span>
                </div>
            </a>
            <ul>
               
            </ul>
        </li>
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-clipboard"></i>
                </div>
                <div class="nav_title">
                    <span>Kota</span>
                </div>
            </a>
            <ul>
               
            </ul>
        </li>
    </ul>
</nav>
<!--/ sidebar  -->

@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('perencanaan.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        <script src="https://unpkg.com/gojs@2.2.2/release/go.js"></script>

      	<div class="main_content_iner overly_inner ">
      		<div class="container-fluid p-0 ">
      			<!-- page title  -->
      			<div class="row">
      				<div class="col-12">
      					<div id="allSampleContent" class="p-4 w-full">
        <script src="https://unpkg.com/gojs@2.2.2/extensions/Figures.js"></script>
    <script id="code">
    function init() {

      // Since 2.2 you can also author concise templates with method chaining instead of GraphObject.make
      // For details, see https://gojs.net/latest/intro/buildingObjects.html
      const $ = go.GraphObject.make;  // for conciseness in defining templates
      myDiagram =
        $(go.Diagram, "myDiagramDiv",
          {
            allowCopy: false,
            "draggingTool.dragsTree": true,
            "commandHandler.deletesTree": true,
            layout:
              $(go.TreeLayout,
                { angle: 90, arrangement: go.TreeLayout.ArrangementFixedRoots }),
            "undoManager.isEnabled": true
          });

      // when the document is modified, add a "*" to the title and enable the "Save" button
      myDiagram.addDiagramListener("Modified", e => {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
          if (idx < 0) document.title += "*";
        } else {
          if (idx >= 0) document.title = document.title.substr(0, idx);
        }
      });

      var bluegrad = $(go.Brush, "Linear", { 0: "#C4ECFF", 1: "#70D4FF" });
      var greengrad = $(go.Brush, "Linear", { 0: "#B1E2A5", 1: "#7AE060" });

      // each action is represented by a shape and some text
      var actionTemplate =
        $(go.Panel, "Horizontal",
          $(go.Shape,
            { width: 12, height: 12 },
            new go.Binding("figure"),
            new go.Binding("fill")
          ),
          $(go.TextBlock,
            { font: "10pt Verdana, sans-serif" },
            new go.Binding("text")
          )
        );

      // each regular Node has body consisting of a title followed by a collapsible list of actions,
      // controlled by a PanelExpanderButton, with a TreeExpanderButton underneath the body
      myDiagram.nodeTemplate =  // the default node template
        $(go.Node, "Vertical",
          new go.Binding("isTreeExpanded").makeTwoWay(),  // remember the expansion state for
          new go.Binding("wasTreeExpanded").makeTwoWay(), //   when the model is re-loaded
          { selectionObjectName: "BODY" },
          // the main "BODY" consists of a RoundedRectangle surrounding nested Panels
          $(go.Panel, "Auto",
            { name: "BODY" },
            $(go.Shape, "Rectangle",
              { fill: bluegrad, stroke: null }
            ),
            $(go.Panel, "Vertical",
              { margin: 3 },
              // the title
              $(go.TextBlock,
                {
                  stretch: go.GraphObject.Horizontal,
                  font: "bold 12pt Verdana, sans-serif"
                },
                new go.Binding("text", "question")
              ),
              // the optional list of actions
              $(go.Panel, "Vertical",
                { stretch: go.GraphObject.Horizontal, visible: false },  // not visible unless there is more than one action
                new go.Binding("visible", "actions", acts => (Array.isArray(acts) && acts.length > 0)),
                // headered by a label and a PanelExpanderButton inside a Table
                $(go.Panel, "Table",
                  { stretch: go.GraphObject.Horizontal },
                  $(go.TextBlock, "Choices",
                    {
                      alignment: go.Spot.Left,
                      font: "10pt Verdana, sans-serif"
                    }
                  ),
                  $("PanelExpanderButton", "COLLAPSIBLE",  // name of the object to make visible or invisible
                    { column: 1, alignment: go.Spot.Right }
                  )
                ), // end Table panel
                // with the list data bound in the Vertical Panel
                $(go.Panel, "Vertical",
                  {
                    name: "COLLAPSIBLE",  // identify to the PanelExpanderButton
                    padding: 2,
                    stretch: go.GraphObject.Horizontal,  // take up whole available width
                    background: "white",  // to distinguish from the node's body
                    defaultAlignment: go.Spot.Left,  // thus no need to specify alignment on each element
                    itemTemplate: actionTemplate  // the Panel created for each item in Panel.itemArray
                  },
                  new go.Binding("itemArray", "actions")  // bind Panel.itemArray to nodedata.actions
                )  // end action list Vertical Panel
              )  // end optional Vertical Panel
            )  // end outer Vertical Panel
          ),  // end "BODY"  Auto Panel
          $(go.Panel,  // this is underneath the "BODY"
            { height: 17 },  // always this height, even if the TreeExpanderButton is not visible
            $("TreeExpanderButton")
          )
        );

      // define a second kind of Node:
      myDiagram.nodeTemplateMap.add("Terminal",
        $(go.Node, "Spot",
          $(go.Shape, "Circle",
            { width: 55, height: 55, fill: greengrad, stroke: null }
          ),
          $(go.TextBlock,
            { font: "10pt Verdana, sans-serif" },
            new go.Binding("text")
          )
        )
      );

      myDiagram.linkTemplate =
        $(go.Link, go.Link.Orthogonal,
          { deletable: false, corner: 10 },
          $(go.Shape,
            { strokeWidth: 2 }
          ),
          $(go.TextBlock, go.Link.OrientUpright,
            {
              background: "white",
              visible: false,  // unless the binding sets it to true for a non-empty string
              segmentIndex: -2,
              segmentOrientation: go.Link.None
            },
            new go.Binding("text", "answer"),
            // hide empty string;
            // if the "answer" property is undefined, visible is false due to above default setting
            new go.Binding("visible", "answer", a => a ? true : false)
          )
        );

      var nodeDataArray = [
        {
          key: 1, question: "Greeting",
          actions: [
            { text: "Sales", figure: "ElectricalHazard", fill: "blue" },
            { text: "Parts and Services", figure: "FireHazard", fill: "red" },
            { text: "Representative", figure: "IrritationHazard", fill: "yellow" }
          ]
        },
        {
          key: 2, question: "Sales",
          actions: [
            { text: "Compact", figure: "ElectricalHazard", fill: "blue" },
            { text: "Mid-Size", figure: "FireHazard", fill: "red" },
            { text: "Large", figure: "IrritationHazard", fill: "yellow" }
          ]
        },
        {
          key: 3, question: "Parts and Services",
          actions: [
            { text: "Maintenance", figure: "ElectricalHazard", fill: "blue" },
            { text: "Repairs", figure: "FireHazard", fill: "red" },
            { text: "State Inspection", figure: "IrritationHazard", fill: "yellow" }
          ]
        },
        { key: 4, question: "Representative" },
        { key: 5, question: "Compact" },
        { key: 6, question: "Mid-Size" },
        {
          key: 7, question: "Large",
          actions: [
            { text: "SUV", figure: "ElectricalHazard", fill: "blue" },
            { text: "Van", figure: "FireHazard", fill: "red" }
          ]
        },
        { key: 8, question: "Maintenance" },
        { key: 9, question: "Repairs" },
        { key: 10, question: "State Inspection" },
        { key: 11, question: "SUV" },
        { key: 12, question: "Van" },
        { key: 13, category: "Terminal", text: "Susan" },
        { key: 14, category: "Terminal", text: "Eric" },
        { key: 15, category: "Terminal", text: "Steven" },
        { key: 16, category: "Terminal", text: "Tom" },
        { key: 17, category: "Terminal", text: "Emily" },
        { key: 18, category: "Terminal", text: "Tony" },
        { key: 19, category: "Terminal", text: "Ken" },
        { key: 20, category: "Terminal", text: "Rachel" }
      ];
      var linkDataArray = [
        { from: 1, to: 2, answer: 1 },
        { from: 1, to: 3, answer: 2 },
        { from: 1, to: 4, answer: 3 },
        { from: 2, to: 5, answer: 1 },
        { from: 2, to: 6, answer: 2 },
        { from: 2, to: 7, answer: 3 },
        { from: 3, to: 8, answer: 1 },
        { from: 3, to: 9, answer: 2 },
        { from: 3, to: 10, answer: 3 },
        { from: 7, to: 11, answer: 1 },
        { from: 7, to: 12, answer: 2 },
        { from: 5, to: 13 },
        { from: 6, to: 14 },
        { from: 11, to: 15 },
        { from: 12, to: 16 },
        { from: 8, to: 17 },
        { from: 9, to: 18 },
        { from: 10, to: 19 },
        { from: 4, to: 20 }
      ];

      // create the Model with the above data, and assign to the Diagram
      myDiagram.model = new go.GraphLinksModel(
        {
          copiesArrays: true,
          copiesArrayObjects: true,
          nodeDataArray: nodeDataArray,
          linkDataArray: linkDataArray
        });

    }
    window.addEventListener('DOMContentLoaded', init);
  </script>

<div id="sample">
  <div id="myDiagramDiv" style="border: 1px solid black; width: 100%; height: 500px; position: relative; -webkit-tap-highlight-color: rgba(255, 255, 255, 0); cursor: auto;"><canvas tabindex="0" width="1037" height="498" style="position: absolute; top: 0px; left: 0px; z-index: 2; user-select: none; touch-action: none; width: 1037px; height: 498px; cursor: auto;">This text is displayed if your browser does not support the Canvas HTML element.</canvas><div style="position: absolute; overflow: auto; width: 1054px; height: 498px; z-index: 1;"><div style="position: absolute; width: 1px; height: 619.257px;"></div></div></div>
  <p>
    An <em>IVR tree</em>, or Interactive Voice Response Tree, is typically used by
    automated answering systems to direct calls to the correct party. This particular example
    is for a car dealership to route calls.
  </p>
  <p>
    This Interactive Voice Response Tree (IVR tree) has nodes that contain a collapsible list of actions, controlled by a <b>PanelExpanderButton</b>,
    with a <b>TreeExpanderButton</b> underneath the body. See the <a href="../intro/buttons.html">Intro page on Buttons</a> for more GoJS button information.
  </p>
<p class="text-xs">GoJS version 2.2.2. Copyright 1998-2022 by Northwoods Software.</p></div>
    <p><a href="https://github.com/NorthwoodsSoftware/GoJS/blob/master/samples/IVRtree.html" target="_blank">View this sample page's source on GitHub</a></p><pre class=" language-js">
    <span class="token keyword">function</span> <span class="token function">init</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>

      <span class="token comment">// Since 2.2 you can also author concise templates with method chaining instead of GraphObject.make</span>
      <span class="token comment">// For details, see https://gojs.net/latest/intro/buildingObjects.html</span>
      <span class="token keyword">const</span> $ <span class="token operator">=</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>make<span class="token punctuation">;</span>  <span class="token comment">// for conciseness in defining templates</span>
      myDiagram <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Diagram<span class="token punctuation">,</span> <span class="token string">"myDiagramDiv"</span><span class="token punctuation">,</span>
          <span class="token punctuation">{</span>
            allowCopy<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
            <span class="token string">"draggingTool.dragsTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            <span class="token string">"commandHandler.deletesTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            layout<span class="token operator">:</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">,</span>
                <span class="token punctuation">{</span> angle<span class="token operator">:</span> <span class="token number">90</span><span class="token punctuation">,</span> arrangement<span class="token operator">:</span> go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">.</span>ArrangementFixedRoots <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token string">"undoManager.isEnabled"</span><span class="token operator">:</span> <span class="token boolean">true</span>
          <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// when the document is modified, add a "*" to the title and enable the "Save" button</span>
      myDiagram<span class="token punctuation">.</span><span class="token function">addDiagramListener</span><span class="token punctuation">(</span><span class="token string">"Modified"</span><span class="token punctuation">,</span> <span class="token parameter">e</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
        <span class="token keyword">var</span> button <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">getElementById</span><span class="token punctuation">(</span><span class="token string">"SaveButton"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>button<span class="token punctuation">)</span> button<span class="token punctuation">.</span>disabled <span class="token operator">=</span> <span class="token operator">!</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">;</span>
        <span class="token keyword">var</span> idx <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">indexOf</span><span class="token punctuation">(</span><span class="token string">"*"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">)</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&lt;</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">+=</span> <span class="token string">"*"</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&gt;=</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">substr</span><span class="token punctuation">(</span><span class="token number">0</span><span class="token punctuation">,</span> idx<span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> bluegrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#C4ECFF"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#70D4FF"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> greengrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#B1E2A5"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#7AE060"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each action is represented by a shape and some text</span>
      <span class="token keyword">var</span> actionTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Horizontal"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">12</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"figure"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"fill"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each regular Node has body consisting of a title followed by a collapsible list of actions,</span>
      <span class="token comment">// controlled by a PanelExpanderButton, with a TreeExpanderButton underneath the body</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplate <span class="token operator">=</span>  <span class="token comment">// the default node template</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"isTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// remember the expansion state for</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"wasTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">//   when the model is re-loaded</span>
          <span class="token punctuation">{</span> selectionObjectName<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token comment">// the main "BODY" consists of a RoundedRectangle surrounding nested Panels</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Auto"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Rectangle"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> fill<span class="token operator">:</span> bluegrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
            <span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> margin<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
              <span class="token comment">// the title</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
                <span class="token punctuation">{</span>
                  stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>
                  font<span class="token operator">:</span> <span class="token string">"bold 12pt Verdana, sans-serif"</span>
                <span class="token punctuation">}</span><span class="token punctuation">,</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"question"</span><span class="token punctuation">)</span>
              <span class="token punctuation">)</span><span class="token punctuation">,</span>
              <span class="token comment">// the optional list of actions</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span> visible<span class="token operator">:</span> <span class="token boolean">false</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// not visible unless there is more than one action</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">,</span> <span class="token parameter">acts</span> <span class="token operator">=&gt;</span> <span class="token punctuation">(</span>Array<span class="token punctuation">.</span><span class="token function">isArray</span><span class="token punctuation">(</span>acts<span class="token punctuation">)</span> <span class="token operator">&amp;&amp;</span> acts<span class="token punctuation">.</span>length <span class="token operator">&gt;</span> <span class="token number">0</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
                <span class="token comment">// headered by a label and a PanelExpanderButton inside a Table</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Table"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> <span class="token string">"Choices"</span><span class="token punctuation">,</span>
                    <span class="token punctuation">{</span>
                      alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>
                      font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span>
                    <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"PanelExpanderButton"</span><span class="token punctuation">,</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// name of the object to make visible or invisible</span>
                    <span class="token punctuation">{</span> column<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Right <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span>
                <span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">// end Table panel</span>
                <span class="token comment">// with the list data bound in the Vertical Panel</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span>
                    name<span class="token operator">:</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// identify to the PanelExpanderButton</span>
                    padding<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span>
                    stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>  <span class="token comment">// take up whole available width</span>
                    background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>  <span class="token comment">// to distinguish from the node's body</span>
                    defaultAlignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>  <span class="token comment">// thus no need to specify alignment on each element</span>
                    itemTemplate<span class="token operator">:</span> actionTemplate  <span class="token comment">// the Panel created for each item in Panel.itemArray</span>
                  <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"itemArray"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">)</span>  <span class="token comment">// bind Panel.itemArray to nodedata.actions</span>
                <span class="token punctuation">)</span>  <span class="token comment">// end action list Vertical Panel</span>
              <span class="token punctuation">)</span>  <span class="token comment">// end optional Vertical Panel</span>
            <span class="token punctuation">)</span>  <span class="token comment">// end outer Vertical Panel</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// end "BODY"  Auto Panel</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span>  <span class="token comment">// this is underneath the "BODY"</span>
            <span class="token punctuation">{</span> height<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// always this height, even if the TreeExpanderButton is not visible</span>
            <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"TreeExpanderButton"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// define a second kind of Node:</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplateMap<span class="token punctuation">.</span><span class="token function">add</span><span class="token punctuation">(</span><span class="token string">"Terminal"</span><span class="token punctuation">,</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Spot"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Circle"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> greengrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span>
      <span class="token punctuation">)</span><span class="token punctuation">;</span>

      myDiagram<span class="token punctuation">.</span>linkTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Link<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>Orthogonal<span class="token punctuation">,</span>
          <span class="token punctuation">{</span> deletable<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span> corner<span class="token operator">:</span> <span class="token number">10</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> strokeWidth<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>OrientUpright<span class="token punctuation">,</span>
            <span class="token punctuation">{</span>
              background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>
              visible<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>  <span class="token comment">// unless the binding sets it to true for a non-empty string</span>
              segmentIndex<span class="token operator">:</span> <span class="token operator">-</span><span class="token number">2</span><span class="token punctuation">,</span>
              segmentOrientation<span class="token operator">:</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>None
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token comment">// hide empty string;</span>
            <span class="token comment">// if the "answer" property is undefined, visible is false due to above default setting</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">,</span> <span class="token parameter">a</span> <span class="token operator">=&gt;</span> a <span class="token operator">?</span> <span class="token boolean">true</span> <span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> nodeDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Greeting"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Representative"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Compact"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Mid-Size"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Maintenance"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Repairs"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"State Inspection"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Representative"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Compact"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Mid-Size"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"SUV"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Van"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Maintenance"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Repairs"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"State Inspection"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"SUV"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Van"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">13</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Susan"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">14</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Eric"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">15</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Steven"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">16</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tom"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">17</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Emily"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">18</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tony"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">19</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Ken"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">20</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Rachel"</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> linkDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">13</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">14</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">15</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">16</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">18</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">19</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">20</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>

      <span class="token comment">// create the Model with the above data, and assign to the Diagram</span>
      myDiagram<span class="token punctuation">.</span>model <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>GraphLinksModel</span><span class="token punctuation">(</span>
        <span class="token punctuation">{</span>
          copiesArrays<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          copiesArrayObjects<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          nodeDataArray<span class="token operator">:</span> nodeDataArray<span class="token punctuation">,</span>
          linkDataArray<span class="token operator">:</span> linkDataArray
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token punctuation">}</span>
    window<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'DOMContentLoaded'</span><span class="token punctuation">,</span> init<span class="token punctuation">)</span><span class="token punctuation">;</span>
  </pre><pre class=" language-js">
    <span class="token keyword">function</span> <span class="token function">init</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>

      <span class="token comment">// Since 2.2 you can also author concise templates with method chaining instead of GraphObject.make</span>
      <span class="token comment">// For details, see https://gojs.net/latest/intro/buildingObjects.html</span>
      <span class="token keyword">const</span> $ <span class="token operator">=</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>make<span class="token punctuation">;</span>  <span class="token comment">// for conciseness in defining templates</span>
      myDiagram <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Diagram<span class="token punctuation">,</span> <span class="token string">"myDiagramDiv"</span><span class="token punctuation">,</span>
          <span class="token punctuation">{</span>
            allowCopy<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
            <span class="token string">"draggingTool.dragsTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            <span class="token string">"commandHandler.deletesTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            layout<span class="token operator">:</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">,</span>
                <span class="token punctuation">{</span> angle<span class="token operator">:</span> <span class="token number">90</span><span class="token punctuation">,</span> arrangement<span class="token operator">:</span> go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">.</span>ArrangementFixedRoots <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token string">"undoManager.isEnabled"</span><span class="token operator">:</span> <span class="token boolean">true</span>
          <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// when the document is modified, add a "*" to the title and enable the "Save" button</span>
      myDiagram<span class="token punctuation">.</span><span class="token function">addDiagramListener</span><span class="token punctuation">(</span><span class="token string">"Modified"</span><span class="token punctuation">,</span> <span class="token parameter">e</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
        <span class="token keyword">var</span> button <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">getElementById</span><span class="token punctuation">(</span><span class="token string">"SaveButton"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>button<span class="token punctuation">)</span> button<span class="token punctuation">.</span>disabled <span class="token operator">=</span> <span class="token operator">!</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">;</span>
        <span class="token keyword">var</span> idx <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">indexOf</span><span class="token punctuation">(</span><span class="token string">"*"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">)</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&lt;</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">+=</span> <span class="token string">"*"</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&gt;=</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">substr</span><span class="token punctuation">(</span><span class="token number">0</span><span class="token punctuation">,</span> idx<span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> bluegrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#C4ECFF"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#70D4FF"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> greengrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#B1E2A5"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#7AE060"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each action is represented by a shape and some text</span>
      <span class="token keyword">var</span> actionTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Horizontal"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">12</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"figure"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"fill"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each regular Node has body consisting of a title followed by a collapsible list of actions,</span>
      <span class="token comment">// controlled by a PanelExpanderButton, with a TreeExpanderButton underneath the body</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplate <span class="token operator">=</span>  <span class="token comment">// the default node template</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"isTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// remember the expansion state for</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"wasTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">//   when the model is re-loaded</span>
          <span class="token punctuation">{</span> selectionObjectName<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token comment">// the main "BODY" consists of a RoundedRectangle surrounding nested Panels</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Auto"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Rectangle"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> fill<span class="token operator">:</span> bluegrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
            <span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> margin<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
              <span class="token comment">// the title</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
                <span class="token punctuation">{</span>
                  stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>
                  font<span class="token operator">:</span> <span class="token string">"bold 12pt Verdana, sans-serif"</span>
                <span class="token punctuation">}</span><span class="token punctuation">,</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"question"</span><span class="token punctuation">)</span>
              <span class="token punctuation">)</span><span class="token punctuation">,</span>
              <span class="token comment">// the optional list of actions</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span> visible<span class="token operator">:</span> <span class="token boolean">false</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// not visible unless there is more than one action</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">,</span> <span class="token parameter">acts</span> <span class="token operator">=&gt;</span> <span class="token punctuation">(</span>Array<span class="token punctuation">.</span><span class="token function">isArray</span><span class="token punctuation">(</span>acts<span class="token punctuation">)</span> <span class="token operator">&amp;&amp;</span> acts<span class="token punctuation">.</span>length <span class="token operator">&gt;</span> <span class="token number">0</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
                <span class="token comment">// headered by a label and a PanelExpanderButton inside a Table</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Table"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> <span class="token string">"Choices"</span><span class="token punctuation">,</span>
                    <span class="token punctuation">{</span>
                      alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>
                      font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span>
                    <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"PanelExpanderButton"</span><span class="token punctuation">,</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// name of the object to make visible or invisible</span>
                    <span class="token punctuation">{</span> column<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Right <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span>
                <span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">// end Table panel</span>
                <span class="token comment">// with the list data bound in the Vertical Panel</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span>
                    name<span class="token operator">:</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// identify to the PanelExpanderButton</span>
                    padding<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span>
                    stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>  <span class="token comment">// take up whole available width</span>
                    background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>  <span class="token comment">// to distinguish from the node's body</span>
                    defaultAlignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>  <span class="token comment">// thus no need to specify alignment on each element</span>
                    itemTemplate<span class="token operator">:</span> actionTemplate  <span class="token comment">// the Panel created for each item in Panel.itemArray</span>
                  <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"itemArray"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">)</span>  <span class="token comment">// bind Panel.itemArray to nodedata.actions</span>
                <span class="token punctuation">)</span>  <span class="token comment">// end action list Vertical Panel</span>
              <span class="token punctuation">)</span>  <span class="token comment">// end optional Vertical Panel</span>
            <span class="token punctuation">)</span>  <span class="token comment">// end outer Vertical Panel</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// end "BODY"  Auto Panel</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span>  <span class="token comment">// this is underneath the "BODY"</span>
            <span class="token punctuation">{</span> height<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// always this height, even if the TreeExpanderButton is not visible</span>
            <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"TreeExpanderButton"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// define a second kind of Node:</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplateMap<span class="token punctuation">.</span><span class="token function">add</span><span class="token punctuation">(</span><span class="token string">"Terminal"</span><span class="token punctuation">,</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Spot"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Circle"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> greengrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span>
      <span class="token punctuation">)</span><span class="token punctuation">;</span>

      myDiagram<span class="token punctuation">.</span>linkTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Link<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>Orthogonal<span class="token punctuation">,</span>
          <span class="token punctuation">{</span> deletable<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span> corner<span class="token operator">:</span> <span class="token number">10</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> strokeWidth<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>OrientUpright<span class="token punctuation">,</span>
            <span class="token punctuation">{</span>
              background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>
              visible<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>  <span class="token comment">// unless the binding sets it to true for a non-empty string</span>
              segmentIndex<span class="token operator">:</span> <span class="token operator">-</span><span class="token number">2</span><span class="token punctuation">,</span>
              segmentOrientation<span class="token operator">:</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>None
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token comment">// hide empty string;</span>
            <span class="token comment">// if the "answer" property is undefined, visible is false due to above default setting</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">,</span> <span class="token parameter">a</span> <span class="token operator">=&gt;</span> a <span class="token operator">?</span> <span class="token boolean">true</span> <span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> nodeDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Greeting"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Representative"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Compact"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Mid-Size"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Maintenance"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Repairs"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"State Inspection"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Representative"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Compact"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Mid-Size"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"SUV"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Van"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Maintenance"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Repairs"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"State Inspection"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"SUV"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Van"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">13</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Susan"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">14</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Eric"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">15</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Steven"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">16</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tom"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">17</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Emily"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">18</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tony"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">19</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Ken"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">20</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Rachel"</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> linkDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">13</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">14</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">15</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">16</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">18</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">19</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">20</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>

      <span class="token comment">// create the Model with the above data, and assign to the Diagram</span>
      myDiagram<span class="token punctuation">.</span>model <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>GraphLinksModel</span><span class="token punctuation">(</span>
        <span class="token punctuation">{</span>
          copiesArrays<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          copiesArrayObjects<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          nodeDataArray<span class="token operator">:</span> nodeDataArray<span class="token punctuation">,</span>
          linkDataArray<span class="token operator">:</span> linkDataArray
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token punctuation">}</span>
    window<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'DOMContentLoaded'</span><span class="token punctuation">,</span> init<span class="token punctuation">)</span><span class="token punctuation">;</span>
  </pre><pre class=" language-js">
    <span class="token keyword">function</span> <span class="token function">init</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>

      <span class="token comment">// Since 2.2 you can also author concise templates with method chaining instead of GraphObject.make</span>
      <span class="token comment">// For details, see https://gojs.net/latest/intro/buildingObjects.html</span>
      <span class="token keyword">const</span> $ <span class="token operator">=</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>make<span class="token punctuation">;</span>  <span class="token comment">// for conciseness in defining templates</span>
      myDiagram <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Diagram<span class="token punctuation">,</span> <span class="token string">"myDiagramDiv"</span><span class="token punctuation">,</span>
          <span class="token punctuation">{</span>
            allowCopy<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
            <span class="token string">"draggingTool.dragsTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            <span class="token string">"commandHandler.deletesTree"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
            layout<span class="token operator">:</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">,</span>
                <span class="token punctuation">{</span> angle<span class="token operator">:</span> <span class="token number">90</span><span class="token punctuation">,</span> arrangement<span class="token operator">:</span> go<span class="token punctuation">.</span>TreeLayout<span class="token punctuation">.</span>ArrangementFixedRoots <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token string">"undoManager.isEnabled"</span><span class="token operator">:</span> <span class="token boolean">true</span>
          <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// when the document is modified, add a "*" to the title and enable the "Save" button</span>
      myDiagram<span class="token punctuation">.</span><span class="token function">addDiagramListener</span><span class="token punctuation">(</span><span class="token string">"Modified"</span><span class="token punctuation">,</span> <span class="token parameter">e</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
        <span class="token keyword">var</span> button <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">getElementById</span><span class="token punctuation">(</span><span class="token string">"SaveButton"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>button<span class="token punctuation">)</span> button<span class="token punctuation">.</span>disabled <span class="token operator">=</span> <span class="token operator">!</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">;</span>
        <span class="token keyword">var</span> idx <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">indexOf</span><span class="token punctuation">(</span><span class="token string">"*"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>myDiagram<span class="token punctuation">.</span>isModified<span class="token punctuation">)</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&lt;</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">+=</span> <span class="token string">"*"</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
          <span class="token keyword">if</span> <span class="token punctuation">(</span>idx <span class="token operator">&gt;=</span> <span class="token number">0</span><span class="token punctuation">)</span> document<span class="token punctuation">.</span>title <span class="token operator">=</span> document<span class="token punctuation">.</span>title<span class="token punctuation">.</span><span class="token function">substr</span><span class="token punctuation">(</span><span class="token number">0</span><span class="token punctuation">,</span> idx<span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> bluegrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#C4ECFF"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#70D4FF"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> greengrad <span class="token operator">=</span> <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Brush<span class="token punctuation">,</span> <span class="token string">"Linear"</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token number">0</span><span class="token operator">:</span> <span class="token string">"#B1E2A5"</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token operator">:</span> <span class="token string">"#7AE060"</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each action is represented by a shape and some text</span>
      <span class="token keyword">var</span> actionTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Horizontal"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">12</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"figure"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"fill"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// each regular Node has body consisting of a title followed by a collapsible list of actions,</span>
      <span class="token comment">// controlled by a PanelExpanderButton, with a TreeExpanderButton underneath the body</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplate <span class="token operator">=</span>  <span class="token comment">// the default node template</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"isTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// remember the expansion state for</span>
          <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"wasTreeExpanded"</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">makeTwoWay</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">//   when the model is re-loaded</span>
          <span class="token punctuation">{</span> selectionObjectName<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token comment">// the main "BODY" consists of a RoundedRectangle surrounding nested Panels</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Auto"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">"BODY"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Rectangle"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> fill<span class="token operator">:</span> bluegrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
            <span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
              <span class="token punctuation">{</span> margin<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
              <span class="token comment">// the title</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
                <span class="token punctuation">{</span>
                  stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>
                  font<span class="token operator">:</span> <span class="token string">"bold 12pt Verdana, sans-serif"</span>
                <span class="token punctuation">}</span><span class="token punctuation">,</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"question"</span><span class="token punctuation">)</span>
              <span class="token punctuation">)</span><span class="token punctuation">,</span>
              <span class="token comment">// the optional list of actions</span>
              <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span> visible<span class="token operator">:</span> <span class="token boolean">false</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// not visible unless there is more than one action</span>
                <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">,</span> <span class="token parameter">acts</span> <span class="token operator">=&gt;</span> <span class="token punctuation">(</span>Array<span class="token punctuation">.</span><span class="token function">isArray</span><span class="token punctuation">(</span>acts<span class="token punctuation">)</span> <span class="token operator">&amp;&amp;</span> acts<span class="token punctuation">.</span>length <span class="token operator">&gt;</span> <span class="token number">0</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
                <span class="token comment">// headered by a label and a PanelExpanderButton inside a Table</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Table"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span> stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> <span class="token string">"Choices"</span><span class="token punctuation">,</span>
                    <span class="token punctuation">{</span>
                      alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>
                      font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span>
                    <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span><span class="token punctuation">,</span>
                  <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"PanelExpanderButton"</span><span class="token punctuation">,</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// name of the object to make visible or invisible</span>
                    <span class="token punctuation">{</span> column<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> alignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Right <span class="token punctuation">}</span>
                  <span class="token punctuation">)</span>
                <span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">// end Table panel</span>
                <span class="token comment">// with the list data bound in the Vertical Panel</span>
                <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span> <span class="token string">"Vertical"</span><span class="token punctuation">,</span>
                  <span class="token punctuation">{</span>
                    name<span class="token operator">:</span> <span class="token string">"COLLAPSIBLE"</span><span class="token punctuation">,</span>  <span class="token comment">// identify to the PanelExpanderButton</span>
                    padding<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span>
                    stretch<span class="token operator">:</span> go<span class="token punctuation">.</span>GraphObject<span class="token punctuation">.</span>Horizontal<span class="token punctuation">,</span>  <span class="token comment">// take up whole available width</span>
                    background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>  <span class="token comment">// to distinguish from the node's body</span>
                    defaultAlignment<span class="token operator">:</span> go<span class="token punctuation">.</span>Spot<span class="token punctuation">.</span>Left<span class="token punctuation">,</span>  <span class="token comment">// thus no need to specify alignment on each element</span>
                    itemTemplate<span class="token operator">:</span> actionTemplate  <span class="token comment">// the Panel created for each item in Panel.itemArray</span>
                  <span class="token punctuation">}</span><span class="token punctuation">,</span>
                  <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"itemArray"</span><span class="token punctuation">,</span> <span class="token string">"actions"</span><span class="token punctuation">)</span>  <span class="token comment">// bind Panel.itemArray to nodedata.actions</span>
                <span class="token punctuation">)</span>  <span class="token comment">// end action list Vertical Panel</span>
              <span class="token punctuation">)</span>  <span class="token comment">// end optional Vertical Panel</span>
            <span class="token punctuation">)</span>  <span class="token comment">// end outer Vertical Panel</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>  <span class="token comment">// end "BODY"  Auto Panel</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Panel<span class="token punctuation">,</span>  <span class="token comment">// this is underneath the "BODY"</span>
            <span class="token punctuation">{</span> height<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>  <span class="token comment">// always this height, even if the TreeExpanderButton is not visible</span>
            <span class="token function">$</span><span class="token punctuation">(</span><span class="token string">"TreeExpanderButton"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// define a second kind of Node:</span>
      myDiagram<span class="token punctuation">.</span>nodeTemplateMap<span class="token punctuation">.</span><span class="token function">add</span><span class="token punctuation">(</span><span class="token string">"Terminal"</span><span class="token punctuation">,</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Node<span class="token punctuation">,</span> <span class="token string">"Spot"</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span> <span class="token string">"Circle"</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> width<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> height<span class="token operator">:</span> <span class="token number">55</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> greengrad<span class="token punctuation">,</span> stroke<span class="token operator">:</span> <span class="token keyword">null</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> font<span class="token operator">:</span> <span class="token string">"10pt Verdana, sans-serif"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span>
      <span class="token punctuation">)</span><span class="token punctuation">;</span>

      myDiagram<span class="token punctuation">.</span>linkTemplate <span class="token operator">=</span>
        <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Link<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>Orthogonal<span class="token punctuation">,</span>
          <span class="token punctuation">{</span> deletable<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span> corner<span class="token operator">:</span> <span class="token number">10</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>Shape<span class="token punctuation">,</span>
            <span class="token punctuation">{</span> strokeWidth<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span>
          <span class="token punctuation">)</span><span class="token punctuation">,</span>
          <span class="token function">$</span><span class="token punctuation">(</span>go<span class="token punctuation">.</span>TextBlock<span class="token punctuation">,</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>OrientUpright<span class="token punctuation">,</span>
            <span class="token punctuation">{</span>
              background<span class="token operator">:</span> <span class="token string">"white"</span><span class="token punctuation">,</span>
              visible<span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>  <span class="token comment">// unless the binding sets it to true for a non-empty string</span>
              segmentIndex<span class="token operator">:</span> <span class="token operator">-</span><span class="token number">2</span><span class="token punctuation">,</span>
              segmentOrientation<span class="token operator">:</span> go<span class="token punctuation">.</span>Link<span class="token punctuation">.</span>None
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"text"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
            <span class="token comment">// hide empty string;</span>
            <span class="token comment">// if the "answer" property is undefined, visible is false due to above default setting</span>
            <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>Binding</span><span class="token punctuation">(</span><span class="token string">"visible"</span><span class="token punctuation">,</span> <span class="token string">"answer"</span><span class="token punctuation">,</span> <span class="token parameter">a</span> <span class="token operator">=&gt;</span> a <span class="token operator">?</span> <span class="token boolean">true</span> <span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">)</span>
          <span class="token punctuation">)</span>
        <span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token keyword">var</span> nodeDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Greeting"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Representative"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Sales"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Compact"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Mid-Size"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Parts and Services"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Maintenance"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Repairs"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"State Inspection"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"IrritationHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"yellow"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Representative"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Compact"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Mid-Size"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          key<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Large"</span><span class="token punctuation">,</span>
          actions<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"SUV"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"ElectricalHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"blue"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> text<span class="token operator">:</span> <span class="token string">"Van"</span><span class="token punctuation">,</span> figure<span class="token operator">:</span> <span class="token string">"FireHazard"</span><span class="token punctuation">,</span> fill<span class="token operator">:</span> <span class="token string">"red"</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Maintenance"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Repairs"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"State Inspection"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"SUV"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> question<span class="token operator">:</span> <span class="token string">"Van"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">13</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Susan"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">14</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Eric"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">15</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Steven"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">16</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tom"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">17</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Emily"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">18</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Tony"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">19</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Ken"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> key<span class="token operator">:</span> <span class="token number">20</span><span class="token punctuation">,</span> category<span class="token operator">:</span> <span class="token string">"Terminal"</span><span class="token punctuation">,</span> text<span class="token operator">:</span> <span class="token string">"Rachel"</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>
      <span class="token keyword">var</span> linkDataArray <span class="token operator">=</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">3</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">3</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">1</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">7</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> answer<span class="token operator">:</span> <span class="token number">2</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">13</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">14</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">11</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">15</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">12</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">16</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">17</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">9</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">18</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">10</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">19</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span> from<span class="token operator">:</span> <span class="token number">4</span><span class="token punctuation">,</span> to<span class="token operator">:</span> <span class="token number">20</span> <span class="token punctuation">}</span>
      <span class="token punctuation">]</span><span class="token punctuation">;</span>

      <span class="token comment">// create the Model with the above data, and assign to the Diagram</span>
      myDiagram<span class="token punctuation">.</span>model <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">go<span class="token punctuation">.</span>GraphLinksModel</span><span class="token punctuation">(</span>
        <span class="token punctuation">{</span>
          copiesArrays<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          copiesArrayObjects<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          nodeDataArray<span class="token operator">:</span> nodeDataArray<span class="token punctuation">,</span>
          linkDataArray<span class="token operator">:</span> linkDataArray
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token punctuation">}</span>
    window<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'DOMContentLoaded'</span><span class="token punctuation">,</span> init<span class="token punctuation">)</span><span class="token punctuation">;</span>
  </pre></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	@include('layouts.footer')

</section>  
@endsection

@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('perencanaan.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')

        <style type="text/css">
            tr.collapsed td:first-child>i.fas.fa-angle-down{
                transform: rotate(-90deg);
            }
            .nice-select.has-multiple {
                min-height: 40px;
                line-height: 42px;
            }
            span.current {
                display: block;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            .nice_Select .list li {
                white-space: break-spaces;
            }
            .nice_Select .list { max-height: 300px; overflow: scroll !important; }
        </style>
        
        <div class="main_content_iner ">
            <div class="container-fluid p-0 ">
                <!-- page title  -->
                <div class="row">
                    <div class="col-12">
                        <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                            <div class="page_title_left">
                                <h3 class="f_s_25 f_w_700 dark_text" >Renja</h3>
                                <ol class="breadcrumb page_bradcam mb-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/perencanaan') }}">Perencanaan</a></li>
                                    <li class="breadcrumb-item active">Renja</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12">
                        <div class="white_card mb_30 ">
                            <div class="white_card_header">
                                <div class="bulder_tab_wrapper">
                                    <ul class="nav" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="kegiatan-tab" data-bs-toggle="tab" href="#Kegiatan" role="tab" aria-controls="Kegiatan" aria-selected="true">Kegiatan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="subkegiatan-tab" data-bs-toggle="tab" href="#Subkegiatan" role="tab" aria-controls="Subkegiatan" aria-selected="false">Sub Kegiatan</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="white_card_body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="Kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="QA_section">
                                                        <div class="white_box_tittle list_header">
                                                            <h4>Data Kegiatan</h4>
                                                            @php if(Session::get('id_role') == 4 || Session::get('id_role') == 1){
                                                            @endphp
                                                                <div class="box_right d-flex lms_block">
                                                                    <div class="add_button ms-2">
                                                                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalAddIndKeg" class="btn_1" onclick="openmodalAddKeg()"><i class="ti-plus"></i> Indikator</a>
                                                                    </div>
                                                                </div>
                                                            @php
                                                            }
                                                            @endphp
                                                        </div>
                                                        <div class="QA_table mb_30">
                                                            <table class="table multiplegroup" id="table_kegiatan">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col" class="text-center">Kegiatan</th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Subkegiatan" role="tabpanel" aria-labelledby="subkegiatan-tab">
                                        <div class="builder_select">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="QA_section">
                                                        <div class="white_box_tittle list_header">
                                                            <h4>Data Sub Kegiatan</h4>
                                                            @php if(Session::get('id_role') == 4 || Session::get('id_role') == 1){
                                                            @endphp
                                                            <div class="box_right d-flex lms_block">
                                                                <div class="add_button ms-2">
                                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalAddIndSubKeg" class="btn_1" onclick="openmodalAddSubKeg()"><i class="ti-plus"></i> Indikator</a>
                                                                </div>
                                                            </div>
                                                            @php
                                                            }
                                                            @endphp
                                                        </div>

                                                        <div class="QA_table mb_30">
                                                            <!-- table-responsive -->
                                                            <table class="table multiplegroup" id="table_subkegiatan">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col"></th>
                                                                        <th scope="col" class="text-center">Sub Kegiatan</th>
                                                                        <th scope="col"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalAddIndKeg">
            <div class="modal-dialog modal-xl" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Indikator Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <form id="inputindikatorkegiatanForm">
                                    <div class="white_card card_height_100 mb_30">
                                        <div class="white_card_body">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-lg-12 ">
                                                        <label class="form-label" for="#">Perangkat Daerah</label>
                                                        <div class="common_select">
                                                            <select class="nice_Select wide mb_30" id="id_pd_keg" name="pd_keg">
                                                                <option value="">Pilih Perangkat Daerah...</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 ">
                                                        <label class="form-label" for="#">Kegiatan</label>
                                                        <div class="common_select">
                                                            <select class="nice_Select wide mb_30" onchange="loadindikatorkegiatan(this.value)" id="id_kegiatan" name="id_kegiatan">
                                                                <option value="">Pilih Kegiatan...</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 ">
                                                        <div class="white_card card_height_100 mb_30 bg-secondary">
                                                            <div class="white_card_body">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <label class="form-label" for="#" id="lable-namaprogam"></label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <label class="form-label" for="#">Indikator Program RPJMD</label>
                                                                        <div class="common_select">
                                                                            <select class="nice_Select wide mb_30" multiple="multiple"
                                                                                name="id_ind_program_rpjmd[]" id="id_ind_program_rpjmd">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="deleted_indkeg[]" id="deleted_indkeg">
                                                <div id="wrap-ind-keg">
                                                </div>
                                                <div class="row mb-3" style="display:none;" id="btn_addfieldformindkeg">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-success" type="button"
                                                            onclick="addRowIndikatorKegiatan(null,null,true)"><i class="ti-plus"></i> Tambah Indikator</button>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn btn-primary" onclick="saveKegiatan()" id=btn-saveindkeg><i class="ti-save"></i> Simpan Data</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalAddIndSubKeg">
            <div class="modal-dialog modal-xl" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Indikator Sub Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <form id="inputindikatorsubkegiatanForm">
                                <div class="white_card card_height_100 mb_30">
                                    <div class="white_card_body">
                                        <div class="card-body">
                                            <div class="row mb-3">
                                                <div class="col-lg-12 ">
                                                    <label class="form-label" for="#">Perangkat Daerah</label>
                                                    <div class="common_select">
                                                        <select class="nice_Select wide mb_30" name="pd_subkeg" id="id_pd_subkeg">
                                                            <option value="">Pilih Perangkat Daerah...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <label class="form-label" for="#">Sub Kegiatan</label>
                                                    <div class="common_select">
                                                        <select class="nice_Select wide mb_30" onchange="loadindikatorsubkegiatan(this.value)" name="id_subkegiatan" id="id_subkegiatan">
                                                            <option value="">Pilih Sub Kegiatan...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="white_card card_height_100 mb_30 bg-secondary">
                                                        <div class="white_card_body">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <label class="form-label" for="#" id="lable-namakegiatan"></label>
                                                                </div>
                                                                <div class="row">
                                                                    <label class="form-label" for="#">Indikator Kegiatan</label>
                                                                    <div class="common_select">
                                                                        <select class="nice_Select wide mb_30" multiple="multiple" name="id_indikator_kegiatan[]" id="id_indikator_kegiatan">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" row mb-3">
                                                <div class="col-sm-12">Relasi Sub Kegiatan</div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="relasisubkeg[]" type="checkbox" id="relasisubkeg1" value="SDGS">
                                                        <label class="form-label form-check-label" for="gridCheck1">
                                                        SDGS
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="relasisubkeg[]" type="checkbox" id="relasisubkeg2" value="SPM">
                                                        <label class="form-label form-check-label" for="gridCheck1">
                                                        SPM
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="deleted_indsubkeg[]" id="deleted_indsubkeg">
                                            <div id="wrap-ind-subkeg">
                                            </div>
                                            <div class="row mb-3" style="display:none;" id="btn_addfieldformindsubkeg">
                                                <div class="col-md-12">
                                                    <button class="btn btn-success" type="button" onclick="addRowIndikatorSubKegiatan(null,null,true)"><i class="ti-plus"></i> Tambah Indikator</button>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-primary" onclick="saveSubKegiatan()" id="btn-saveindsubkeg"><i class="ti-save"></i> Simpan Data</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalVerIndKeg">
            <div class="modal-dialog modal-xl" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Verifikasi Indikator Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="closeFormVerIndKeg()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <form id="inputverindikatorkegiatanForm" method="POST">
                                    {{ csrf_field() }}
                                    <div class="white_card card_height_100 mb_30">
                                        <div class="white_card_body">
                                            <div class="card-body">
                                                <div class="row mb-3">                                                    
                                                    <div class="col-lg-12 ">
                                                        <label class="form-label" for="#"><h5>Kegiatan</h5></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="hidden" name="ver_id_kegiatan" id="ver_id_kegiatan" readonly>
                                                            <input class="form-control" type="text" name="ver_nama_kegiatan" id="ver_nama_kegiatan" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="form-label" for="#"><h5>Indikator Kegiatan</h5></label>
                                                        <table class="table multiplegroup" id="tableVerIndKeg">
                                                        </table>
                                                        <select name="multiselectVerIndKeg[]" id="multiselectVerIndKeg" multiple hidden>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row mb-3" id="verifikatorIndKeg">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="#"><h5>Verifikator</h5></label>
                                                        <input class="form-control" type="text" name="verifikator_ind_keg" id="verifikator_ind_keg" value="{{Auth::user()->nama_pengguna}}" readonly>
                                                    </div>
                                                </div>
                                                
                                                <input type="hidden" name="statusVerIndKeg" id="statusVerIndKeg">

                                                <div class="row mb-3" id="button_ver_ind_keg">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn btn-primary" onclick="doVerIndKeg(1)">
                                                            <i class="ti-check"></i> Verifikasi
                                                        </button>
                                                        <button type="button" class="btn btn-warning" onclick="openFormCatatanVerKeg()">
                                                            <i class="ti-hand-stop"></i> Tunda Verifikasi
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="row mb-3" id="form_cat_ver_ind_keg">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="#"><h5>Catatan</h5></label>
                                                        <textarea class="form-control" name="catatan_ver_ind_keg" id="catatan_ver_ind_keg" cols="30" rows="10"></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <br/>
                                                        <button type="button" class="btn btn-primary" onclick="doVerIndKeg(0)">
                                                            <i class="ti-save"></i> Simpan
                                                        </button>
                                                        <button type="button" class="btn btn-danger" onclick="closeFormCatatanVerKeg()">
                                                            <i class="ti-close"></i> Tutup
                                                        </button>
                                                    </div>
                                                </div>
                                                <script>
                                                    $(document).ready(function()
                                                    {
                                                        $('#form_cat_ver_ind_keg').hide();
                                                        $('#button_ver_ind_keg').show();
                                                    });

                                                    async function openFormCatatanVerKeg()
                                                    {
                                                        $('#form_cat_ver_ind_keg').show();
                                                        $('#button_ver_ind_keg').hide();
                                                    }

                                                    async function closeFormCatatanVerKeg()
                                                    {
                                                        $('#catatan_ver_ind_keg').val('');
                                                        $('#form_cat_ver_ind_keg').hide();
                                                        $('#button_ver_ind_keg').show();
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalVerIndSubKeg">
            <div class="modal-dialog modal-xl" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Verifikasi Indikator Sub Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="closeFormVerIndSubKeg()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <form id="inputverindikatorsubkegiatanForm" method="POST">
                                    {{ csrf_field() }}
                                    <div class="white_card card_height_100 mb_30">
                                        <div class="white_card_body">
                                            <div class="card-body">
                                                <div class="row mb-3">                                                    
                                                    <div class="col-lg-12 ">
                                                        <label class="form-label" for="#"><h5>Sub Kegiatan</h5></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="hidden" name="ver_id_subkegiatan" id="ver_id_subkegiatan" readonly>
                                                            <input class="form-control" type="text" name="ver_nama_subkegiatan" id="ver_nama_subkegiatan" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="form-label" for="#"><h5>Indikator Sub Kegiatan</h5></label>
                                                        <table class="table multiplegroup" id="tableVerIndSubKeg">
                                                        </table>
                                                        <select name="multiselectVerIndSubKeg[]" id="multiselectVerIndSubKeg" multiple hidden>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row mb-3" id="verifikatorIndSubKeg">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="#"><h5>Verifikator</h5></label>
                                                        <input class="form-control" type="text" name="verifikator_ind_subkeg" id="verifikator_ind_subkeg" value="{{Auth::user()->nama_pengguna}}" readonly>
                                                    </div>
                                                </div>
                                                
                                                <input type="hidden" name="statusVerIndSubKeg" id="statusVerIndSubKeg">

                                                <div class="row mb-3" id="button_ver_ind_subkeg">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn btn-primary" onclick="doVerIndSubKeg(1)">
                                                            <i class="ti-check"></i> Verifikasi
                                                        </button>
                                                        <button type="button" class="btn btn-warning" onclick="openFormCatatanVerSubKeg()">
                                                            <i class="ti-hand-stop"></i> Tunda Verifikasi
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="row mb-3" id="form_cat_ver_ind_subkeg">
                                                    <div class="col-md-12">
                                                        <label class="form-label" for="#"><h5>Catatan</h5></label>
                                                        <textarea class="form-control" name="catatan_ver_ind_subkeg" id="catatan_ver_ind_subkeg" cols="30" rows="10"></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <br/>
                                                        <button type="button" class="btn btn-primary" onclick="doVerIndSubKeg(0)">
                                                            <i class="ti-save"></i> Simpan
                                                        </button>
                                                        <button type="button" class="btn btn-danger" onclick="closeFormCatatanVerSubKeg()">
                                                            <i class="ti-close"></i> Tutup
                                                        </button>
                                                    </div>
                                                </div>
                                                <script>
                                                    $(document).ready(function()
                                                    {
                                                        $('#form_cat_ver_ind_subkeg').hide();
                                                        $('#button_ver_ind_subkeg').show();
                                                    });

                                                    async function openFormCatatanVerSubKeg()
                                                    {
                                                        $('#form_cat_ver_ind_subkeg').show();
                                                        $('#button_ver_ind_subkeg').hide();
                                                    }

                                                    async function closeFormCatatanVerSubKeg()
                                                    {
                                                        $('#catatan_ver_ind_subkeg').val('');
                                                        $('#form_cat_ver_ind_subkeg').hide();
                                                        $('#button_ver_ind_subkeg').show();
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            let satuanoption = ''
            var table,table_sub;
            var id_role = <?=Session::get('id_role');?>;
            
            $( document ).ready(function() {
                var collapsedGroups = {},collapsedGroupsSub = {},top,middle,parent,keyrowkeg1,keyrowkeg2,keyrowkeg3,keyrowskeg1,keyrowskeg2,keyrowskeg3;
                var table = $('#table_kegiatan').DataTable( {
                    ajax: {
                        url:"{{ url('/api/renja/kegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}}
                        },
                    },
                    columns: [ 
                        { "data": "program.nama_program" },
                        { "data": "nama_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                var show_pdname = '', show_btn_ind = ''
                                
                                if(id_role != 4){
                                    show_pdname = '<li class="mb-1">'+
                                        '<div class="activity_bell"></div>'+
                                        '<div class="timeLine_inner d-flex align-items-center">'+
                                        '<div class="activity_wrap">'+
                                        '<h6>'+row.nama_skpd+'</h6>'+
                                        '</div>'+
                                        '</div>'+
                                        '</li>'
                                }
                                if(id_role == 4 || id_role == 1){
                                    show_btn_ind = '<button class="btn btn-sm me-3 btn-primary" onclick="openmodalAddKeg('+row.id_skpd+','+row.id_kegiatan+')">Edit Indikator</button>'
                                }
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+show_pdname+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<button class="btn btn-sm me-3 btn-warning" onclick="openModalVerKeg('+row.id_skpd+','+row.id_kegiatan+')">Verifikasi Indikator </button>'+show_btn_ind+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "nama_skpd", 
                            "render": function ( data, type, row, meta ) {
                                return data
                            } 
                        }
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    columnDefs: [ 
                        {
                            targets: [ 0,2 ],
                            visible: false
                        } 
                    ],
                } );

                var tableSub = $('#table_subkegiatan').DataTable( {
                    ajax: {
                        url: "{{ url('/api/renja/subkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            step : 'murni',
                            id_pd:{{$id_pd}}
                        },
                    },
                    columns: [ 
                        { "data": "kegiatan.program.nama_program" },
                        { "data": "kegiatan.nama_kegiatan" },
                        { "data": "nama_sub_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                var show_pdname = '',show_btn_ind = ''
                                if(id_role != 4){
                                    show_pdname = '<li class="mb-1">'+
                                        '<div class="activity_bell"></div>'+
                                        '<div class="timeLine_inner d-flex align-items-center">'+
                                        '<div class="activity_wrap">'+
                                        '<h6>'+row.nama_skpd+'</h6>'+
                                        '</div>'+
                                        '</div>'+
                                        '</li>'
                                }
                                if(id_role == 4 || id_role == 1){
                                    show_btn_ind = '<button class="btn btn-sm me-3 btn-primary" onclick="openmodalAddSubKeg('+row.id_skpd+','+row.id_sub_kegiatan+')">Edit Indikator</button>'
                                }
                                var html = '<td><div class="white_card badge_complete mt-30" style="width: -webkit-fill-available;">'+
                                '<div class="white_card_header QA_section">'+
                                '<div class="box_header m-0">'+
                                '<div class="Activity_timeline">'+
                                '<ul>'+
                                '<li class="mb-1">'+
                                '<div class="activity_bell"></div>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<h6>'+data+'</h6>'+
                                '</div>'+
                                '</div>'+
                                '</li>'+show_pdname+
                                '<li>'+
                                '<div class="timeLine_inner d-flex align-items-center">'+
                                '<div class="activity_wrap">'+
                                '<button class="btn btn-sm me-3 btn-warning" onclick="openModalVerSubKeg('+row.id_skpd+','+row.id_sub_kegiatan+')">Verifikasi Indikator</button>'+show_btn_ind+
                                '</div>'+
                                '</div>'+
                                '</li>'+
                                '</ul>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</td>'
                                return html
                            }
                        },
                        { "data": "nama_skpd", 
                            "render": function ( data, type, row, meta ) {
                                return data
                            }
                        }
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    columnDefs: [ 
                        {
                            targets: [ 0,1,3 ],
                            visible: false
                        } 
                    ],
                    /*"serverSide": true,*/
                } );

                $("#id_pd_keg").change(function(){
                    loadaddkegiatan(0,this.value)
                });

                $("#id_pd_subkeg").change(function(){
                    loadaddsubkegiatan(0,this.value)
                });
            });

            function loadindikatorkegiatan(val){
                $('#lable-namaprogam').empty();
                if(val){
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorprogramrpjmd') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            id_kegiatan : val,
                            id_pd : $('#id_pd_keg').val()
                        },
                        success: function(response) {
                            $('#id_ind_program_rpjmd').empty();
                            $.each(response.data[0].data, function(key, value) {
                                $('#lable-namaprogam').html('Nama Program: '+value.program_r_p_j_m_d.program)
                                $('#id_ind_program_rpjmd').append(
                                    $('<option></option>').val(value['id_indikator_program']).html(value['indikator_program'])
                                );
                            });

                            var val = []
                            $.each(response.data[0].indcascading, function(key, value) {
                                val.push(value['id_indikator_program'])
                            });
                            $('#id_ind_program_rpjmd').val(val);
                            $('#id_ind_program_rpjmd').niceSelect('update');
                        }
                    });

                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            id_kegiatan : val,
                            id_pd : $('#id_pd_keg').val()
                        },
                        success: function(response) {
                            $('#wrap-ind-keg').empty();
                            $('#btn_addfieldformindkeg').css('display','block');
                            $.each(response.data, function(key, value) {
                                if(value.indikator_kegiatan){
                                    addRowIndikatorKegiatan(key,value,false)
                                }
                            });
                        }
                    });
                }else{
                    $('#lable-namaprogam').empty();
                    $('#id_ind_program_rpjmd').empty();
                    $('#wrap-ind-keg').empty();
                    $('#deleted_indkeg').val();
                }
            }

            function loadsatuan(elm,val=""){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/satuan') }}",
                    success: function(response) {
                        $('#'+elm).empty();
                        $('#'+elm).append(
                            $('<option></option>').val('').html('Pilih Satuan...')
                        );
                        $.each(response.data, function(key, value) {
                            $('#'+elm).append(
                                $('<option></option>').val(value['satuan']).html(value['satuan'])
                            );
                        });
                        if(val){
                            var d = val.replace(/"/g, '')
                            $('#'+elm).val(String(d))
                        }
                        $('#'+elm).niceSelect()
                    }
                });
            }

            function openmodalAddKeg(id_pd=0,id_kegiatan=0){
                loadskpd(id_pd,'#id_pd_keg',id_kegiatan);
                $('#btn-saveindkeg').css('display','none')
                $('#inputindikatorkegiatanForm')[0].reset();
                $('#wrap-ind-keg').empty();
                $('#modalAddIndKeg').modal({backdrop: 'static', keyword: false})
                $('#modalAddIndKeg').modal('show');
            }

            function openmodalAddSubKeg(id_pd=0,id_subkegiatan=0){
                loadskpd(id_pd,'#id_pd_subkeg',0,id_subkegiatan);
                $('#btn-saveindsubkeg').css('display','none')
                $('#relasisubkeg1').prop('checked', false);
                $('#relasisubkeg2').prop('checked', false);
                $('#inputindikatorsubkegiatanForm')[0].reset();
                $('#wrap-ind-subkeg').empty();
                $('#modalAddIndSubKeg').modal({backdrop: 'static', keyword: false})
                $('#modalAddIndSubKeg').modal('show');
            }

            function loadskpd(id_pd=0,elm,id_keg=0,id_subkeg=0){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:{{$id_pd}}
                    },
                    success: function(response) {
                        $(elm).empty();
                        $(elm).append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $(elm).append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id_pd != 0){
                            $(elm).val(id_pd);
                        }
                        $(elm).niceSelect('update');

                        if(id_keg != 0){
                            loadaddkegiatan(id_keg,id_pd)
                        }

                        if(id_subkeg != 0){
                            loadaddsubkegiatan(id_subkeg,id_pd)
                        }
                    }
                });
            }

            function loadaddkegiatan(id_kegiatan=0,id_skpd=0){
                var url = "{{ url('/api/renja/kegiatan') }}"
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        periode_usulan: <?=session()->get('periode_usulan');?>,
                        step : 'murni',
                        id_pd : id_skpd
                    },
                    success: function(response) {
                        $('#id_kegiatan').empty();
                        $('#id_kegiatan').append(
                            $('<option></option>').val('').html('Pilih Kegiatan...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#id_kegiatan').append(
                                $('<option></option>').val(value['id_kegiatan']).html(value['nama_kegiatan'])
                                );
                        });
                        if(id_kegiatan != 0){
                            $('#id_kegiatan').val(id_kegiatan);
                            loadindikatorkegiatan(id_kegiatan)
                        }
                        $('#id_kegiatan').niceSelect('update');
                    }
                });
                $('#wrap-ind-keg').empty();
            }

            function addRowIndikatorKegiatan(key,value,created){
                if(created){
                    var countselector = $('div[id^="indkeg_"]').filter(function(){return this.id.match(/\d+$/);});
                    var num = countselector.length+1
                    var array = {
                        id_indikator_kegiatan:0,
                        indikator_kegiatan:"",
                        target:"",
                        satuan:""
                    }
                }else{
                    var num = key+1
                    var array = value
                }
                var html = '<div class="row mb-3" id="indkeg_'+num+'">'+
                            '<input type="hidden" name="id_indikator_kegiatan[]" id="id_indikator_kegiatan_'+num+'" value="'+array.id_indikator_kegiatan+'">'+
                            '<div class="col-md-5">'+
                            '<label class="form-label" id="label_indkeg_'+num+'">Indikator #'+num+'</label>'+
                            '<input type="text" class="form-control" name="nama_indikator_kegiatan[]" placeholder="" value="'+array.indikator_kegiatan+'">'+
                            '</div>'+
                            '<div class="col-md-3">'+
                            '<label class="form-label" for="inputRealisasi">Target</label>'+
                            '<input type="text" class="form-control" onkeypress="return restrictcomma(event);" name="target_indikator_kegiatan[]" placeholder="" value="'+array.target+'">'+
                            '</div>'+
                            '<div class="col-md-3 ">'+
                            '<label class="form-label" for="#">Satuan</label>'+
                            '<div class="common_select">'+
                            '<select class="nice_Select wide mb_30" name="satuan_indikator_kegiatan[]" id="satuanindkeg_'+num+'">'+
                            '<option value="'+array.satuan+'">'+array.satuan+'</option>'+
                            '</select>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-lg-1">'+
                            '<label class="form-label" for="inputRealisasi">Aksi</label>'+
                            '<div class="row">'+
                            '<div class="col-md-12" id="btn_delindkeg_'+num+'">'+
                            '<button class="btn btn-sm btn-danger" type="button" onclick="delRowIndikatorKegiatan('+num+')"><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>';

                $('#wrap-ind-keg').append(html)
                var key = 'satuanindkeg_'+num
                loadsatuan(key,'\"'+array.satuan+'\"')
                $('#btn-saveindkeg').css('display','block')
            }

            function delRowIndikatorKegiatan(num){
                
                if($('#id_indikator_kegiatan_'+num).val() != 0){
                    var iddel = []
                    if($('#deleted_indkeg').val().length > 0){
                        iddel = JSON.parse($('#deleted_indkeg').val())
                    }
                    iddel.push($('#id_indikator_kegiatan_'+num).val())
                    $('[name="deleted_indkeg[]"]').val(JSON.stringify( iddel ))
                }
                $('#indkeg_'+num).remove()
                while($('#indkeg_'+(num+1)).length){
                    $('#indkeg_'+(num+1)).attr("id", "indkeg_"+((num+1)-1));
                    $('#label_indkeg_'+(num+1)).html('Indikator #'+num);
                    $('#label_indkeg_'+(num+1)).attr("id", "label_indkeg_"+((num+1)-1));
                    $('#btn_delindkeg_'+(num+1)).html('<button class="btn btn-sm btn-danger" type="button" onclick="delRowIndikatorKegiatan('+num+')"><i class="fa fa-trash"></i></button>');
                    $('#btn_delindkeg_'+(num+1)).attr("id", "btn_delindkeg_"+((num+1)-1));
                    $('#satuanindkeg_'+(num+1)).attr("id", "satuanindkeg_"+((num+1)-1));
                    $('#id_indikator_kegiatan_'+(num+1)).attr("id", "id_indikator_kegiatan_"+((num+1)-1));
                    num++;
                }
            }

            function saveKegiatan(){
                let myForm = document.getElementById('inputindikatorkegiatanForm');
                let formData = new FormData(myForm);
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/perencanaan/renja/saveindikatorkegiatan') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            var id_kegiatan = $('#id_kegiatan').val()
                            var id_skpd = $('#id_pd_keg').val()
                            loadindikatorkegiatan(id_kegiatan)   
                            alert(response.message)
                            table.ajax.reload( null, false );
                        }
                    }
                });
            }

            function loadaddsubkegiatan(id_subkegiatan=0,id_skpd){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/subkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('periode_usulan');?>,
                        step : 'murni',
                        id_pd : id_skpd
                    },
                    success: function(response) {
                        $('#id_subkegiatan').empty();
                        $('#id_subkegiatan').append(
                            $('<option></option>').val('').html('Pilih SubKegiatan...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#id_subkegiatan').append(
                                $('<option></option>').val(value['id_sub_kegiatan']).html(value['nama_sub_kegiatan'])
                                );
                        });
                        if(id_subkegiatan != 0){
                            $('#id_subkegiatan').val(id_subkegiatan);
                            loadindikatorsubkegiatan(id_subkegiatan)
                        }
                        $('#id_subkegiatan').niceSelect('update');
                    }
                });
                $('#wrap-ind-subkeg').empty();
            }

            function loadindikatorsubkegiatan(val) {
                $('#lable-namakegiatan').empty();
                if (val) {
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?> ,
                            id_subkegiatan : val,
                            step: 'murni',
                            id_pd: $('#id_pd_subkeg').val(),
                            cascading: true
                        },
                        success: function (response) {
                            $('#id_indikator_kegiatan').empty();
                            $.each(response.data[0].data, function (key, value) {
                                if (value.indikator_kegiatan) {
                                    $('#id_indikator_kegiatan').append(
                                        $('<option></option>').val(value['id_indikator_kegiatan']).html(value['indikator_kegiatan'])
                                    );
                                }
                            });
                            var val = []
                            $.each(response.data[0].indcascading, function(key, value) {
                                val.push(value['id_indikator_kegiatan'])
                            });
                            $('#id_indikator_kegiatan').val(val);
                            $('#id_indikator_kegiatan').niceSelect('update');
                        }
                    });

                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/relasisubkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?> ,
                            id_subkegiatan : val,
                            id_pd: $('#id_pd_subkeg').val()
                        },
                        success: function (response) {
                            $.each(response.data, function (key, value) {
                                if (value['isSDGS'] == 1) {
                                    $('#relasisubkeg1').prop('checked', true);
                                }
                                if (value['isSPM'] == 1) {
                                    $('#relasisubkeg2').prop('checked', true);
                                }
                            });
                        }
                    });

                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorsubkegiatan') }}",
                        data: {
                            id_subkegiatan: val,
                            step: 'murni',
                            periode_usulan: <?=session()->get('periode_usulan');?> ,
                            id_pd : $('#id_pd_subkeg').val()
                        },
                        success: function (response) {
                            $('#wrap-ind-subkeg').empty();
                            $('#btn_addfieldformindsubkeg').css('display', 'block');
                            $.each(response.data, function (key, value) {
                                $('#lable-namakegiatan').html('Nama Kegiatan: '+value.subkegiatan.kegiatan.nama_kegiatan)
                                if (value.indikator_subkegiatan) {
                                    addRowIndikatorSubKegiatan(key, value, false)
                                }
                            });
                        }
                    });
                } else {
                    $('#lable-namakegiatan').empty();
                    $('#id_indikator_kegiatan').empty();
                    $('#wrap-ind-subkeg').empty();
                }
            }
            
            function addRowIndikatorSubKegiatan(key,value,created){
                if(created){
                    var countselector = $('div[id^="indsubkeg_"]').filter(function(){return this.id.match(/\d+$/);});
                    var num = countselector.length+1
                    var array = {
                        id_indikator_subkegiatan:0,
                        indikator_subkegiatan:"",
                        target:"",
                        satuan:""
                    }
                }else{
                    var num = key+1
                    var array = value
                }
                var html = '<div class="row mb-3" id="indsubkeg_'+num+'">'+
                            '<input type="hidden" name="id_indikator_subkegiatan[]" id="id_indikator_subkegiatan_'+num+'" value="'+array.id_indikator_subkegiatan+'">'+
                            '<div class="col-md-5">'+
                            '<label class="form-label" id="label_indsubkeg_'+num+'">Indikator #'+num+'</label>'+
                            '<input type="text" class="form-control" name="nama_indikator_subkegiatan[]" placeholder="" value="'+array.indikator_subkegiatan+'">'+
                            '</div>'+
                            '<div class="col-md-3">'+
                            '<label class="form-label" for="inputRealisasi">Target</label>'+
                            '<input type="text" class="form-control" onkeypress="return restrictcomma(event);" name="target_indikator_subkegiatan[]" placeholder="" value="'+array.target+'">'+
                            '</div>'+
                            '<div class="col-md-3 ">'+
                            '<label class="form-label" for="#">Satuan</label>'+
                            '<div class="common_select">'+
                            '<select class="nice_Select wide mb_30" name="satuan_indikator_subkegiatan[]" id="satuanindsubkeg_'+num+'">'+
                            '<option value="'+array.satuan+'">'+array.satuan+'</option>'+
                            '</select>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-lg-1">'+
                            '<label class="form-label" for="inputRealisasi">Aksi</label>'+
                            '<div class="row">'+
                            '<div class="col-md-12" id="btn_delindsubkeg_'+num+'">'+
                            '<button class="btn btn-sm btn-danger" type="button" onclick="delRowIndikatorSubKegiatan('+num+')"><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>';

                $('#wrap-ind-subkeg').append(html)
                var key = 'satuanindsubkeg_'+num
                loadsatuan(key,'\"'+array.satuan+'\"')
                $('#btn-saveindsubkeg').css('display','block')
            }

            function delRowIndikatorSubKegiatan(num){
                if($('#id_indikator_subkegiatan_'+num).val() != 0){
                    var iddel = []
                    if($('#deleted_indsubkeg').val().length > 0){
                        iddel = JSON.parse($('#deleted_indsubkeg').val())
                    }
                    iddel.push($('#id_indikator_subkegiatan_'+num).val())
                    $('[name="deleted_indsubkeg[]"]').val(JSON.stringify( iddel ))
                }

                $('#indsubkeg_'+num).remove();
                
                while($('#indsubkeg_'+(num+1)).length){
                    $('#indsubkeg_'+(num+1)).attr("id", "indsubkeg_"+((num+1)-1));
                    $('#label_indsubkeg_'+(num+1)).html('Indikator #'+num);
                    $('#label_indsubkeg_'+(num+1)).attr("id", "label_indsubkeg_"+((num+1)-1));
                    $('#btn_delindsubkeg_'+(num+1)).html('<button class="btn btn-sm btn-danger" type="button" onclick="delRowIndikatorSubKegiatan('+num+')"><i class="fa fa-trash"></i></button>');
                    $('#btn_delindsubkeg_'+(num+1)).attr("id", "btn_delindsubkeg_"+((num+1)-1));
                    $('#satuanindsubkeg_'+(num+1)).attr("id", "satuanindsubkeg_"+((num+1)-1));
                    $('#id_indikator_subkegiatan_'+(num+1)).attr("id", "id_indikator_subkegiatan_"+((num+1)-1));
                    num++;
                }
            }

            function saveSubKegiatan(){
                let myForm = document.getElementById('inputindikatorsubkegiatanForm');
                let formData = new FormData(myForm);
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/perencanaan/renja/saveindikatorsubkegiatan') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            var id_subkegiatan = $('#id_subkegiatan').val()
                            var id_skpd = $('#id_pd_subkeg').val()
                            loadindikatorsubkegiatan(id_subkegiatan)
                            alert(response.message)
                            table_sub.ajax.reload( null, false );
                        }
                    }
                });
            }

            function restrictcomma(e){
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[^,;]+$/;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) {
                        theEvent.preventDefault();
                        alert('Untuk angka desimal silahkan gunakan tanda titik (.)')
                    }
                }
            }

            /**************************************
            | Verifikasi Indikator Kegiatan (awal)
            ***************************************/
            // Modal (Form) Verifikasi Indikator Kegiatan   
            function openModalVerKeg(id_pd=0,id_kegiatan=0){
                loadVerKegiatan(id_pd,id_kegiatan);
                $('#modalVerIndKeg').modal('show');
            }            
            // Load Data Kegiatan
            function loadVerKegiatan(id_skpd=0,id_keg=0){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/kegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('periode_usulan');?>,
                        step : 'murni',
                        id_pd : id_skpd
                    },
                    success: function(response) {
                        if(id_keg != 0){ 
                            $('#ver_id_kegiatan').val(id_keg);
                            $.each(response.data, function(key, value) {
                                if(value.id_kegiatan == id_keg)
                                {
                                    $('#ver_nama_kegiatan').val(value.nama_kegiatan);
                                }
                            });
                            loadVerIndikatorKegiatan(id_skpd,id_keg);
                        }
                    }
                });
            }
            // Load Data Indikator Kegiatan
            function loadVerIndikatorKegiatan(id_skpd=0,id_keg=0){
                if(id_keg){
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            id_kegiatan : id_keg,
                            id_pd : id_skpd
                        },
                        success: function(data){
                            $("#tableVerIndKeg").append(
                                    "<thead>"+
                                    "   <tr>"+
                                    "       <td>No.</td>"+
                                    "       <td>Nama Indikator</td>"+
                                    "       <td>Target</td>"+
                                    "       <td>Satuan</td>"+ 
                                    "   </tr>"+
                                    "</thead>"+
                                    "<tbody>");
                            $.each(data.data, function(key, value){
                                $('#multiselectVerIndKeg').append(
                                    $('<option selected></option>').val(value.id_indikator_kegiatan).html(value.indikator_kegiatan)
                                );
                                $("#tableVerIndKeg").append(
                                    "<tr>"+
                                    "   <td>"+(key+1)+"</td>"+
                                    "   <td>"+value.indikator_kegiatan+"</td>"+
                                    "   <td>"+value.target+"</td>"+
                                    "   <td>"+value.satuan+"</td>"+
                                    "<tr>");
                            });
                            $("#tableVerIndKeg").append("</tbody>");
                        }
                    });
                }
            }
            //Proses Melakukan Verifikasi Ke Database 
            function doVerIndKeg(status){
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                $('#statusVerIndKeg').val(status);
                let myForm = document.getElementById('inputverindikatorkegiatanForm');
                let formData = new FormData(myForm);
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/perencanaan/renja/verifikasiIndKeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {                        
                        if(response.status == 'sukses'){
                            Swal.fire({
                                title: 'Berhasil!',
                                html: response.message,
                                type: 'success'
                            });
                            table.ajax.reload( null, false );
                        }else{
                            Swal.fire({
                                title: 'Gagal!!',
                                html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                type: 'danger'
                            });
                        }
                    }
                });                
                $('#modalVerIndKeg').modal('toggle');
                $('#tableVerIndKeg').empty();
                $('#multiselectVerIndKeg').empty();
                $('#ver_id_kegiatan').val();
                $('#ver_nama_kegiatan').val();
            }

            $('#modalVerIndKeg').on('hidden.bs.modal', function () {
                $('#tableVerIndKeg').empty();
                $('#multiselectVerIndKeg').empty();
                $('#ver_id_kegiatan').val();
                $('#ver_nama_kegiatan').val();
            })
            /**************************************
            | Verifikasi Indikator Kegiatan (akhir)
            ***************************************/

           /**************************************
            | Verifikasi Indikator Sub Kegiatan (awal)
            ***************************************/
            // Modal (Form) Verifikasi Indikator Sub Kegiatan   
            function openModalVerSubKeg(id_pd=0,id_subkegiatan=0){
                loadVerSubKegiatan(id_pd,id_subkegiatan);
                $('#modalVerIndSubKeg').modal('show');
            }            
            // Load Data Sub Kegiatan
            function loadVerSubKegiatan(id_skpd=0,id_subkeg=0){
                $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/subkegiatan') }}",
                    data: {
                        periode_usulan: <?=session()->get('periode_usulan');?>,
                        step : 'murni',
                        id_pd : id_skpd
                    },
                    success: function(response) {
                        if(id_subkeg != 0){ 
                            $('#ver_id_subkegiatan').val(id_subkeg);
                            $.each(response.data, function(key, value) {
                                if(value.id_sub_kegiatan == id_subkeg)
                                {
                                    $('#ver_nama_subkegiatan').val(value.nama_sub_kegiatan);
                                }
                            });
                            loadVerIndikatorSubKegiatan(id_skpd,id_subkeg);
                        }
                    }
                });
            }
            // Load Data Indikator Kegiatan
            function loadVerIndikatorSubKegiatan(id_skpd_subkeg=0,id_subkeg=0){
                if(id_subkeg){
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/renja/indikatorsubkegiatan') }}",
                        data: {
                            periode_usulan: <?=session()->get('periode_usulan');?>,
                            step: 'murni',
                            id_subkegiatan : id_subkeg,
                            id_pd : id_skpd_subkeg
                        },
                        success: function(data){
                            $("#tableVerIndSubKeg").append(
                                    "<thead>"+
                                    "   <tr>"+
                                    "       <td>No.</td>"+
                                    "       <td>Nama Indikator</td>"+
                                    "       <td>Target</td>"+
                                    "       <td>Satuan</td>"+ 
                                    "   </tr>"+
                                    "</thead>"+
                                    "<tbody>");
                            $.each(data.data, function(key, value){
                                $('#multiselectVerIndSubKeg').append(
                                    $('<option selected></option>').val(value.id_indikator_subkegiatan).html(value.indikator_subkegiatan)
                                );
                                $("#tableVerIndSubKeg").append(
                                    "<tr>"+
                                    "   <td>"+(key+1)+"</td>"+
                                    "   <td>"+value.indikator_subkegiatan+"</td>"+
                                    "   <td>"+value.target+"</td>"+
                                    "   <td>"+value.satuan+"</td>"+
                                    "<tr>");
                            });
                            $("#tableVerIndSubKeg").append("</tbody>");
                        }
                    });
                }
            }
            //Proses Melakukan Verifikasi Ke Database 
            function doVerIndSubKeg(status){
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })                
                $('#statusVerIndSubKeg').val(status);
                let myForm = document.getElementById('inputverindikatorsubkegiatanForm');
                let formData = new FormData(myForm);
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('/perencanaan/renja/verifikasiIndSubKeg') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.status == 'sukses'){
                            Swal.fire({
                                title: 'Berhasil!',
                                html: response.message,
                                type: 'success'
                            });
                            table.ajax.reload( null, false );
                        }else{
                            Swal.fire({
                                title: 'Gagal!!',
                                html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                type: 'danger'
                            });
                        }
                    }
                });
                $('#modalVerIndSubKeg').modal('toggle');
                $('#tableVerIndSubKeg').empty();
                $('#multiselectVerIndSubKeg').empty();
                $('#ver_id_subkegiatan').val();
                $('#ver_nama_subkegiatan').val();
            }

            $('#modalVerIndSubKeg').on('hidden.bs.modal', function () {
                $('#tableVerIndSubKeg').empty();
                $('#multiselectVerIndSubKeg').empty();
                $('#ver_id_subkegiatan').val();
                $('#ver_nama_subkegiatan').val();
            })
            /**************************************
            | Verifikasi Indikator Sub Kegiatan (akhir)
            ***************************************/
        </script>

        <!-- Footer -->
        @include('layouts.footer')
        
@endsection
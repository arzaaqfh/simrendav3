@extends('layouts.app')

@section('content')

    <!-- Side Bar -->
    @include('perencanaan.renja.sidebar')

        <!-- Top Bar -->
        @include('layouts.topbar')
        
        <div class="main_content_iner ">
            <div class="container-fluid p-0 ">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="white_card card_height_100 mb_30 ">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="white_card_header">
                                        <div class="box_header m-0">
                                            <div class="main-title">
                                                <h3 class="m-0">Perangkat Daerah</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="white_card_body QA_section">
                                        <div class="QA_table ">
                                            <!-- table-responsive -->
                                            <table class="table lms_table_active2 p-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Kode</th>
                                                        <th scope="col">Perangkat Daerah</th>
                                                        <th scope="col">Kegiatan</th>
                                                        <th scope="col">Sub Kegiatan</th>
                                                        <th scope="col">Status Input</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="customer d-flex align-items-center">
                                                                <div class="social_media">
                                                                    <i class="fab fa-facebook-f"></i>
                                                                </div>
                                                                <div class="ml_18">
                                                                    <h3 class="f_s_18 f_w_900 mb-0" >Facebook Promotion</h3>
                                                                    <span class="f_s_12 f_w_700 text_color_8" >Unique Watch</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_900 mb-0" >08:32</h3>
                                                                <span class="f_s_12 f_w_700 color_text_3" >12.12.2022</span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_800 mb-0" >H&G Fashion</h3>
                                                                <span class="f_s_12 f_w_500 color_text_3" >Fashion and design</span>
                                                            </div>
                                                        </td>
                                                        <td class="f_s_14 f_w_400 color_text_3">
                                                            <a href="#" class="badge_active">Active</a>
                                                        </td>
                                                        <td>
                                                            <div class="action_btns d-flex">
                                                                <a href="#" class="action_btn mr_10"> <i class="far fa-edit"></i> </a>
                                                                <a href="#" class="action_btn"> <i class="fas fa-trash"></i> </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="customer d-flex align-items-center">
                                                                <div class="social_media insta_bg">
                                                                    <i class="fab fa-instagram"></i>
                                                                </div>
                                                                <div class="ml_18">
                                                                    <h3 class="f_s_18 f_w_900 mb-0" >Instagram</h3>
                                                                    <span class="f_s_12 f_w_700 text_color_9" >Unique Watch</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_900 mb-0" >08:32</h3>
                                                                <span class="f_s_12 f_w_700 color_text_3" >12.12.2022</span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_800 mb-0" >H&G Fashion</h3>
                                                                <span class="f_s_12 f_w_500 color_text_3" >Fashion and design</span>
                                                            </div>
                                                        </td>
                                                        <td class="f_s_14 f_w_400 color_text_3">
                                                            <a href="#" class="badge_active2">Posed</a>
                                                        </td>
                                                        <td>
                                                            <div class="action_btns d-flex">
                                                                <a href="#" class="action_btn mr_10"> <i class="far fa-edit"></i> </a>
                                                                <a href="#" class="action_btn"> <i class="fas fa-trash"></i> </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="customer d-flex align-items-center">
                                                                <div class="social_media twitter_bg">
                                                                    <i class="fab fa-twitter"></i>
                                                                </div>
                                                                <div class="ml_18">
                                                                    <h3 class="f_s_18 f_w_900 mb-0" >Twitter</h3>
                                                                    <span class="f_s_12 f_w_700 text_color_10" >Unique Watch</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_900 mb-0" >08:32</h3>
                                                                <span class="f_s_12 f_w_700 color_text_3" >12.12.2022</span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_800 mb-0" >H&G Fashion</h3>
                                                                <span class="f_s_12 f_w_500 color_text_3" >Fashion and design</span>
                                                            </div>
                                                        </td>
                                                        <td class="f_s_14 f_w_400 color_text_3">
                                                            <a href="#" class="badge_active3">Closed</a>
                                                        </td>
                                                        <td>
                                                            <div class="action_btns d-flex">
                                                                <a href="#" class="action_btn mr_10"> <i class="far fa-edit"></i> </a>
                                                                <a href="#" class="action_btn"> <i class="fas fa-trash"></i> </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="customer d-flex align-items-center">
                                                                <div class="social_media youtube_bg">
                                                                    <i class="fab fa-youtube"></i>
                                                                </div>
                                                                <div class="ml_18">
                                                                    <h3 class="f_s_18 f_w_900 mb-0" >Youtube</h3>
                                                                    <span class="f_s_12 f_w_700 text_color_11" >Summer Campain</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_900 mb-0" >08:32</h3>
                                                                <span class="f_s_12 f_w_700 color_text_3" >12.12.2022</span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div >
                                                                <h3 class="f_s_18 f_w_800 mb-0" >H&G Fashion</h3>
                                                                <span class="f_s_12 f_w_500 color_text_3" >Fashion and design</span>
                                                            </div>
                                                        </td>
                                                        <td class="f_s_14 f_w_400 color_text_3">
                                                            <a href="#" class="badge_active4">End soon</a>
                                                        </td>
                                                        <td>
                                                            <div class="action_btns d-flex">
                                                                <a href="#" class="action_btn mr_10"> <i class="far fa-edit"></i> </a>
                                                                <a href="#" class="action_btn"> <i class="fas fa-trash"></i> </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 white_card_body pt_25">
                                    <div class="project_complete">
                                        <div class="single_pro d-flex">
                                            <div class="probox"></div>
                                            <div class="box_content">
                                                <h4>5685</h4>
                                                <span>Project completed</span>
                                            </div>
                                        </div>
                                        <div class="single_pro d-flex">
                                            <div class="probox blue_box"></div>
                                            <div class="box_content">
                                                <h4 class="bluish_text" >5685</h4>
                                                <span class="grayis_text" >Project completed</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModalLong">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Indikator Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <div class="row justify-content-center">
                <div class="col-lg-12">
                    <form>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-lg-6 ">
                                        <label class="form-label" for="#">Kegiatan</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" id="id_kegiatan">
                                                <option value="">Pilih Kegiatan...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <label class="form-label" for="#">Indikator Program RPJMD</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" multiple="multiple" name="id_program_rpjmd[]" id="id_program_rpjmd">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="wrap-ind-keg">
                                </div>
                                <div class="row mb-3" style="display:none;" id="btn_addfieldformindikator">
                                    <div class="col-md-12">
                                        <button class="btn btn-success" type="button" onclick="addRowIndikatorKegiatan(null,null,true)"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button class="btn btn-secondary">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModalLong2">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Indikator Sub Kegiatan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <div class="row justify-content-center">
                <div class="col-lg-12">
                    <form>
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_body">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-lg-6 ">
                                        <label class="form-label" for="#">Sub Kegiatan</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" >
                                                <option value="">Pilih Sub Kegiatan...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <label class="form-label" for="#">Indikator Kegiatan</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" >
                                                <option value="">Pilih Indikator...</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class=" row mb-3">
                                    <div class="col-sm-4">Kategori</div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck1">
                                            <label class="form-label form-check-label" for="gridCheck1">
                                            SDGS
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck1">
                                            <label class="form-label form-check-label" for="gridCheck1">
                                            SPM
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <label class="form-label" for="inputRealisasi">Indikator</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="inputRealisasi">Target</label>
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                    <div class="col-md-3 ">
                                        <label class="form-label" for="#">Satuan</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" >
                                                <option value="">Pilih Satuan...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <label class="form-label" for="inputRealisasi">Aksi</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button class="btn btn-secondary">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="https://cdn.datatables.net/rowgroup/1.1.4/js/dataTables.rowGroup.min.js"></script>
        <script type="text/javascript">
            $( document ).ready(function() {
                var collapsedGroups = {},collapsedGroupsSub = {},top,middle,parent;

                var table = $('#table_kegiatan').DataTable( {
                    ajax: "{{ url('/api/renja/indikatorkegiatan') }}",
                    columns: [ 
                        { "data": "kegiatan.program.nama_program" },
                        { "data": "kegiatan.nama_kegiatan" },
                        { "data": "indikator_kegiatan" },
                        { "data": "target" },
                        { "data": "satuan" },
                        { "data": "id_indikator_kegiatan", 
                            "render": function ( data, type, row, meta ) {
                                return '<button type="button" class="btn btn-sm btn-warning"><i class="ti-pencil-alt"></i></button>';
                            } 
                        },
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    rowGroup: {
                        dataSrc: [ 'kegiatan.program.nama_program','kegiatan.nama_kegiatan'],
                        startRender: function (rows, group, level) {
                            
                            var all;

                            if (level === 0) {
                                top = group;
                                all = group;
                                middle = '';
                            }else {
                                // if parent collapsed, nothing to do
                                if (!!collapsedGroups[top]) {
                                    return;
                                }
                                if (level ===1) {
                                    middle = group
                                }
                                all = top + middle + group;
                            }

                            var collapsed = !!collapsedGroups[all];

                            rows.nodes().each(function(r) {
                                r.style.display = collapsed ? 'none' : '';
                            });

                            if(level === 0){
                                return $('<tr/>')
                                .append('<td colspan="4"><i class="fas fa-angle-down"></i> PROGRAM : ' + group + ' (' + rows.count() + ')</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }else if(level === 1){
                                return $('<tr/>')
                                .append('<td colspan="4"><i class="fas fa-angle-down"></i> KEGIATAN: ' + group + ' (' + rows.count() + ')</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }  
                        },
                    },
                    columnDefs: [ 
                        {
                            targets: [ 0,1 ],
                            visible: false
                        } 
                    ],
                    /*"serverSide": true,*/
                } );
                $('#table_kegiatan').on('click', 'tr.dtrg-start', function () {
                    var name = $(this).data('name');
                    collapsedGroups[name] = !collapsedGroups[name];
                    table.draw(false);
                });

                var tableSub = $('#table_subkegiatan').DataTable( {
                    ajax: "{{ url('/api/renja/indikatorsubkegiatan') }}",
                    columns: [ 
                        { "data": "subkegiatan.kegiatan.nama_kegiatan" },
                        { "data": "subkegiatan.nama_sub_kegiatan" },
                        { "data": "indikator_subkegiatan" },
                        { "data": "target" },
                        { "data": "satuan" },
                        { "data": "id_indikator_subkegiatan", 
                            "render": function ( data, type, row, meta ) {
                                return '<button type="button" class="btn btn-sm btn-warning"><i class="ti-pencil-alt"></i></button> <button type="button" class="btn btn-sm btn-danger"><i class="ti-trash"></i></button>';
                            } 
                        },
                    ],
                    lengthMenu: [5, 10, 20, 50, 100],
                    rowGroup: {
                        dataSrc: [ 'subkegiatan.kegiatan.nama_kegiatan','subkegiatan.nama_sub_kegiatan' ],
                        startRender: function (rows, group, level) {
                            
                            var all;

                            if (level === 0) {
                                top = group;
                                all = group;
                                middle = '';
                            }else {
                                // if parent collapsed, nothing to do
                                if (!!collapsedGroupsSub[top]) {
                                    return;
                                }
                                if (level ===1) {
                                    middle = group
                                }
                                all = top + middle + group;
                            }

                            var collapsed = !!collapsedGroupsSub[all];

                            rows.nodes().each(function(r) {
                                r.style.display = collapsed ? 'none' : '';
                            });

                            if(level === 0){
                                return $('<tr/>')
                                .append('<td colspan="4"><i class="fas fa-angle-down"></i> KEGIATAN : ' + group + '</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }else if(level === 1){
                                return $('<tr/>')
                                .append('<td colspan="4"><i class="fas fa-angle-down"></i> SUB KEGIATAN: ' + group + ' (' + rows.count() + ')</td>')
                                .attr('data-name', all)
                                .toggleClass('collapsed', collapsed); 
                            }  
                        },
                    },
                    columnDefs: [ 
                        {
                            targets: [ 1,0 ],
                            visible: false
                        } 
                    ],
                    /*"serverSide": true,*/
                } );
                $('#table_subkegiatan').on('click', 'tr.dtrg-start', function () {
                    var name = $(this).data('name');
                    console.log(name)
                    collapsedGroupsSub[name] = !collapsedGroupsSub[name];
                    tableSub.draw(false);
                }); 

                $("#id_kegiatan").change(function(){
                    var url = "{{ url('/api/renja/kegiatan') }}"
                    if(this.value){
                        $.ajax({
                            type: 'GET',
                            url: url,
                        //data: "tahun=" + tahun,
                            success: function(response) {
                                $('#id_program_rpjmd').empty();
                                $.each(response.data, function(key, value) {
                                    $('#id_program_rpjmd').append(
                                        $('<option></option>').val(value['id_kegiatan']).html(value['nama_kegiatan'])
                                        );
                                });
                                $('#id_program_rpjmd').niceSelect('update');
                            }
                        });

                        $.ajax({
                            type: 'GET',
                            url: "{{ url('/api/renja/indikatorkegiatan') }}",
                            data: {
                               id_kegiatan: this.value
                            },
                            success: function(response) {
                                $('#wrap-ind-keg').empty();
                                $('#btn_addfieldformindikator').css('display','block');
                                $.each(response.data, function(key, value) {
                                    addRowIndikatorKegiatan(key,value,false)
                                });
                            }
                        });
                    }else{
                        $('#id_program_rpjmd').empty();
                        $('#wrap-ind-keg').empty();
                    }
                });
            });

            function loadaddkegiatan(id_kegiatan=0){
                var url = "{{ url('/api/renja/kegiatan') }}"
                $.ajax({
                    type: 'GET',
                    url: url,
                    //data: "tahun=" + tahun,
                    success: function(response) {
                        $('#id_kegiatan').empty();
                        $('#id_kegiatan').append(
                            $('<option></option>').val('').html('Pilih Kegiatan...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#id_kegiatan').append(
                                $('<option></option>').val(value['id_kegiatan']).html(value['nama_kegiatan'])
                                );
                        });
                        if(id_kegiatan != 0){
                            $('#id_kegiatan').val(id);
                        }
                        $('#id_kegiatan').niceSelect('update');
                    }
                });
            }

            function addRowIndikatorKegiatan(key,value,created){
                if(created){
                    var countselector = $('div[id^="indkeg_"]').filter(function(){return this.id.match(/\d+$/);});
                    var num = countselector.length+1
                    var array = {
                        id_indikator_kegiatan:0,
                        indikator_kegiatan:"",
                        target:"",
                        satuan:""
                    }
                }else{
                    var num = key+1
                    var array = value
                }
                var html = '<div class="row mb-3" id="indkeg_'+num+'">'+
                            '<div class="col-md-5">'+
                            '<label class="form-label" for="inputRealisasi">Indikator #'+num+'</label>'+
                            '<input type="text" class="form-control" id="" placeholder="" value="'+array.indikator_kegiatan+'">'+
                            '</div>'+
                            '<div class="col-md-3">'+
                            '<label class="form-label" for="inputRealisasi">Target</label>'+
                            '<input type="text" class="form-control" id="" placeholder="" value="'+array.target+'">'+
                            '</div>'+
                            '<div class="col-md-3 ">'+
                            '<label class="form-label" for="#">Satuan</label>'+
                            '<div class="common_select">'+
                            '<select class="nice_Select wide mb_30" id="satuan_'+num+'">'+
                            '<option value="'+array.satuan+'">'+array.satuan+'</option>'+
                            '</select>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-lg-1">'+
                            '<label class="form-label" for="inputRealisasi">Aksi</label>'+
                            '<div class="row">'+
                            '<div class="col-md-12">'+
                            '<button class="btn btn-danger" type="button" onclick="delRowIndikatorKegiatan('+array.id_indikator_kegiatan+','+num+')"><i class="fa fa-trash"></i></button>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>';

                $('#wrap-ind-keg').append(html)
                $('#satuan_'+num+'').niceSelect();
            }

            function delRowIndikatorKegiatan(id_indikator_kegiatan,num){
                $('#indkeg_'+num).remove();
            }
        </script>

        <!-- Footer -->
        @include('layouts.footer')
        
@endsection
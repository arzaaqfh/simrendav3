@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Pemetaan SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SDGS</a></li>
                                <li class="breadcrumb-item active">Pemetaan Sdgs</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Pemetaan SDGS</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Pemetaan SDGS terhadap Sub Kegiatan Kota Cimahi</h4>
                                    <div class="box_right d-flex lms_block">
                                    <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" onkeydown="return (event.keyCode!=13);" placeholder="Search content here...">
                                                    </div>
                                                    <button type="button"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30 table-responsive">
                                    <table class="table table_coding table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Indikator Nasional</th>
                                                <th>Indikator Kota</th>
                                                <th>Program</th>
                                                <th>Kegiatan</th>
                                                <th>Sub Kegiatan</th>
                                                <th>Perangkat Daerah</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" id="addsdgsCoding">
        <form id="sdgscodingForm">
            <input type="hidden" name="id_log_indikatorkota" id="id_log_indikatorkota">
            <input type="hidden" name="id_pd" id="id_pd">
            <input type="hidden" name="tahun" value="<?= session()->get('tahun_sdgs')?>">
            <input type="hidden" name="input_ck_listcoding" id="input_ck_listcoding">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Nasional</label>
                                                <input class="form-control border-0 bg-primary pt-4 pb-4 m-b-10 text-white" id="target_nasional" readonly>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Kota</label>
                                                <input class="form-control border-0 bg-info pt-4 pb-4 m-b-10 text-white" id="indikator_kota" readonly>
                                            </div>
                                            @if(session()->get('id_role') != 4)
                                            <div class="col-lg-12">
                                                <label class="form-label">Perangkat Daerah</label>
                                                <input class="form-control border-0 bg-success pt-4 pb-4 m-b-10 text-white" id="perangkat_daerah" readonly>
                                            </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="white_card card_height_100 mb_30" style="background-color: rgb(231,237,97,0.8)">
                                                    <div class="white_card_header">
                                                        <div class="main-title">
                                                            <h3 class="m-0">Pilih Sub Kegiatan</h3>
                                                            <span>- Sub Kegiatan terpilih</span>
                                                        </div>
                                                    </div>
                                                    <div class="white_card_body ">
                                                        <div class="card_container">
                                                            <div class="row mb-4">
                                                                <div class="col-lg-3">
                                                                    <div class="common_select">
                                                                        <select class="nice_Select wide" id="f_program" onchange="loaddtrenja('f_program')">
                                                                            <option value="">Filter Program...</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="common_select">
                                                                        <select class="nice_Select wide" id="f_kegiatan" onchange="loaddtrenja('f_kegiatan')">
                                                                            <option value="">Filter Kegiatan...</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="common_select">
                                                                        <select class="nice_Select wide" id="f_subkeg" onchange="loaddtrenja('f_subkeg')">
                                                                            <option value="">Filter Sub Kegiatan...</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="QA_section">
                                                                <div class="QA_table mb_30 table-responsive">
                                                                    <table class="table table-striped" id="table_renja">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Program</th>
                                                                                <th>Kegiatan</th>
                                                                                <th>SubKegiatan</th>
                                                                                <th>Anggaran (Rp)</th>
                                                                                <th>Pilih &nbsp;<input type="checkbox" id="chk_renja" onchange="checkAll(this)" name="ck_list[]"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        var tahun = "<?= session()->get('tahun_sdgs'); ?>"
        var table_renja,pd_key,kota_id,global_tempval,f_program,f_kegiatan,f_subkeg,array_input
        $( document ).ready(function() {
            $('.nice_Select').niceSelect()
            loadpd()
            pd_key = -1
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_coding').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                "lengthChange": true,
                ajax: {
                    url:"{{ url('/api/get/sdgscoding') }}",
                    data: function(d){
                        d.id_skpd = {{$id_pd}},
                        d.tahun = tahun
                    },
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {data: 'target_nasional', name: 'target_nasional'},
                    {data: 'nama_indikator_kota', name: 'nama_indikator_kota'},
                    {data: 'nama_program', name: 'nama_program'},
                    {data: 'nama_kegiatan', name: 'nama_kegiatan'},
                    {data: 'nama_sub_kegiatan', name: 'nama_sub_kegiatan'},
                    {data: 'nama_skpd', name: 'nama_skpd'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width:"20%", className: "text-center"},

                ],
                rowsGroup: [
                    1,2,6,7
                ],
                columnDefs: [ 
                    <?php if(session()->get('id_role') == 4){ ?>
                        { targets: [6], visible: false },
                    <?php } ?>
                ]
            });

            table_renja = $('#table_renja').DataTable( {
                ajax: {
                    url:"{{ url('/api/renja/subkegiatansdgs') }}",
                    data: function(d){
                        d.periode_usulan = tahun,
                        d.step  = 'perubahan',
                        d.id_pd = pd_key,
                        d.nama_program = $('#f_program').next().find('li.selected').attr('data-value')
                        d.nama_kegiatan = $('#f_kegiatan').next().find('li.selected').attr('data-value')
                        d.nama_subkegiatan = $('#f_subkeg').next().find('li.selected').attr('data-value')
                    },
                },
                columns: [ 
                    { "data": "kegiatan.program.nama_program" },
                    { "data": "kegiatan.nama_kegiatan" },
                    { "data": "nama_sub_kegiatan",
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    { "data": "apbd_kota",
                        "render": function ( data, type, row, meta ) {
                            return $.fn.dataTable.render.number( ',', '.', 0).display(data);
                        }
                    },
                    { "data": "id_log_renja",
                        "render": function ( data, type, row, meta ) {
                            var html = '<div class="form-check"><input class="form-check-input" type="checkbox" value="'+data+'" id="id_log'+data+'" name="ck_listcoding[]" onclick="tr_cklist('+data+')"><label class="form-check-label"></label></div>'
                            return html
                        }
                    },
                ],
                drawCallback: function(settings) {
                    this.api()
                        .columns(0)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Program</option></select>')
                            .appendTo($('#f_program').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="'+ d +'"'+ (f_program != '' ? "selected" : "")+'>' + d + '</option>');
                            });
                            $('#f_program').niceSelect('update');
                        });

                    this.api()
                        .columns(1)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Kegiatan</option></select>')
                            .appendTo($('#f_kegiatan').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'"'+ (f_kegiatan != '' ? "selected" : "")+'>' + d + '</option>');
                            });
                            $('#f_kegiatan').niceSelect('update');
                        });

                    this.api()
                        .columns(2)
                        .every(function () {
                            var column = this;
                            var select = $('<select><option value="">Semua Sub Kegiatan</option></select>')
                            .appendTo($('#f_subkeg').empty())
                            column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d +'"'+ (f_subkeg != '' ? "selected" : "")+'>' + d + '</option>');
                            });
                            $('#f_subkeg').niceSelect('update');
                        });
                },
                paging: false,
                "lengthChange": false,
                "searching": false,
                order: [[4, 'desc']]
            });

            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                
                var url = "{{ route('sdgscoding.store') }}"

                let myForm = document.getElementById('sdgscodingForm');
                let formData = new FormData(myForm);
                
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processData: false,
                    contentType: false,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $('#addsdgsCoding').modal('hide');
                        table.draw();
                        Swal.fire({
                            title: 'Berhasil!',
                            html: data.success,
                            type: 'success'
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        //$('#saveBtn').html('Save Changes');
                        Swal.fire({
                            title: 'Gagal!!',
                            html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                            type: 'danger'
                        });
                    }
                });
            });

            $('body').on('click', '.CodingSdgs', function () {
                kota_id = $(this).data("id");
                openModal(kota_id)
            });
        });

        function loaddtrenja(kf=''){
            if(kf == 'f_program'){
                f_program = $('#f_program').next().find('li.selected').attr('data-value')
                f_kegiatan = ''
                f_subkeg = ''
                $('#f_kegiatan').next().find('li.selected').removeClass('selected')
                $('#f_subkeg').next().find('li.selected').removeClass('selected')
            }
            if(kf == 'f_kegiatan'){
                f_kegiatan = $('#f_kegiatan').next().find('li.selected').attr('data-value')
                f_subkeg = ''
                $('#f_subkeg').next().find('li.selected').removeClass('selected')
            }
            if(kf == 'f_subkeg'){
                f_subkeg = $('#f_subkeg').next().find('li.selected').attr('data-value')
            }

            $.ajax({
                type: "get",
                url: "/api/showbypd/sdgscoding/"+pd_key,
                data: {
                    _token: token,
                    id_log_indikatorkota: kota_id,
                },
                success: function (response) {
                    if(response.data.length > 0){
                        global_tempval = response.data
                        if(global_tempval.length > 0){
                            var array = []
                            for(var i=0;i<global_tempval.length;i++){
                                array.push(Number(global_tempval[i].id_log_renja))
                            }
                            $('#input_ck_listcoding').val(array)
                        }
                    }else{
                        global_tempval = []
                    }
                },
                error: function (data) {
                    console.log('Error:', data);           
                }
            });

            table_renja.ajax.reload(setValRenja,false);
        }

        function setValRenja(){
            $('#chk_renja').prop("checked", false);
            var cs = 0;
            if(global_tempval.length > 0){
                for(var i=0;i<global_tempval.length;i++){
                    $('#id_log'+global_tempval[i].id_log_renja).prop('checked', true);
                    array_input.push(Number(global_tempval[i].id_log_renja))
                    var myEle = document.getElementById('id_log'+global_tempval[i].id_log_renja);
                    if(myEle){
                        cs++;   
                    }
                }
                if(table_renja.data().count() == cs){
                    $('#chk_renja').prop("checked", true);
                }
            }
        }

        function openModal(a){
            global_tempval = null
            array_input = []
            f_program = ''
            f_kegiatan = ''
            f_subkeg = ''
            $('#f_program').next().find('li.selected').removeClass('selected')
            $('#f_kegiatan').next().find('li.selected').removeClass('selected')
            $('#f_subkeg').next().find('li.selected').removeClass('selected')

            $('#input_ck_listcoding').val('')
            $('#sdgscodingForm').trigger("reset");
                $('#exampleModalLongTitle').text('Pemetaan Data Indikator Kota')
                $.ajax({
                    type: "get",
                    url: "/api/showbylogind/sdgscoding/"+a,
                    data: {
                        _token: token,
                    },
                    success: function (response) {
                        $('#id_log_indikatorkota').val(response.data[0].id_log_indikatorkota)
                        $('#id_pd').val(response.data[0].id_skpd)
                        $('#target_nasional').val(response.data[0].target_nasional)
                        $('#indikator_kota').val(response.data[0].nama_indikator_kota)
                        $('#perangkat_daerah').val(response.data[0].nama_skpd)
                        pd_key = response.data[0].id_skpd
                        kota_id = response.data[0].id_log_indikatorkota
                        loaddtrenja()
                    },
                    error: function (data) {
                        console.log('Error:', data);           
                    }
                });
            $('#addsdgsCoding').modal('show')
        }

        function renderpd(res){
            $('#perangkat_daerah').empty();
            $('#perangkat_daerah').append(
                $('<option></option>').val('').html('Pilih Perangkat Daerah...')
            );
            for (let index = 0; index < res.length; index++) {
                if(res[index].nama_skpd != null){
                    $('#perangkat_daerah').append(
                        $('<option></option>').val(res[index].id_skpd).html(res[index].nama_skpd)
                    );
                }
            }
            if(res[0].id_skpd!=0){
                $('#perangkat_daerah').val(res[0].id_skpd);
            }
            $('#perangkat_daerah').niceSelect('update');
        }

        function loadnasional(id=0,id_kota=0){
            $.ajax({
                type: 'GET',
                url: "/api/show/sdgsindnasional/"+id,
                success: function(response) {
                    $('#id_target_nasional').empty();
                    $('#id_target_nasional').append(
                        $('<option></option>').val('').html('Pilih Indikator Nasional...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_target_nasional').append(
                            $('<option></option>').val(value['id_target_nasional']).html(value['target_global'])
                        );
                    });
                    if(id!=0){
                        $('#id_target_nasional').val(id);
                    }
                    $('#id_target_nasional').niceSelect('update');
                    loadkota(id_kota)
                }
            });
        }

        function loadkota(id=0){
            $.ajax({
                type: 'GET',
                url: "/api/get/master/sdgsindkota",
                data: {
                    id: id,
                },
                success: function(response) {
                    $('#id_indikator_kota').empty();
                    var arr = []
                    arr.push(response.data[0])
                    $.each(arr, function(key, value) {
                        $('#id_indikator_kota').append(
                            $('<option></option>').val(value['id_indikator_kota']).html(value['nama_indikator_kota'])
                        );
                    });
                    if(id!=0){
                        $('#id_indikator_kota').val(id);
                    }
                    $('#id_indikator_kota').niceSelect('update');
                }
            });
        }

        function loadpd(id=0){
            $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:id
                    },
                    success: function(response) {
                        $('#perangkat_daerah').empty();
                        $('#perangkat_daerah').append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#perangkat_daerah').append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id != 0){
                            $('#perangkat_daerah').val(id);
                        }
                        $('#perangkat_daerah').niceSelect('update');
                    }
                });
        }

        function checkAll(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox'  && !(checkboxes[i].disabled) ) {
                        checkboxes[i].checked = true;
                        tr_cklist(checkboxes[i].value)
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                        tr_cklist(checkboxes[i].value)
                    }
                }
            }
        }

        function tr_cklist(d){
            if($("#id_log"+d).is(':checked')){
                array_input.push(d);
            }else{
                var filtered = array_input.filter(function(value, index, arr){ 
                    return value != d;
                });
                array_input = filtered
            }
            $('#input_ck_listcoding').val(array_input)
        }

        
    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Dashboard SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/sdgs') }}">SDGS</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/sdgs/dashboard/rekap') }}">Rekap SDGs</a></li>
                                <li class="breadcrumb-item active">Detail Rekap SDGs</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Detail Rekapan SDGs (Pemetaaan)</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xl-12">
                    <div class="white_card card_height_100 mb_30 ">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white_card_header">
                                    <div class="box_header m-0">
                                        <div class="main-title">
                                            <h3 class="m-0">Pemetaan SDGs</h3>
                                            <span>Data SDGs yang terakomodir di Renja</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="white_card_body QA_section">
                                    <div class="QA_table ">
                                        <!-- table-responsive -->
                                        <table class="table p-0" id="table_detail_pemetaan">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Tujuan Global</th>
                                                    <th scope="col">Tujuan Nasional</th>
                                                    <th scope="col">Indikator Nasional</th>
                                                    <th scope="col">Indikator Kota</th>
                                                    <th scope="col">Program</th>
                                                    <th scope="col">Kegiatan</th>
                                                    <th scope="col">Sub Kegiatan</th>
                                                    <th scope="col">Perangkat Daerah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" id="infoDetailPd">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-6">
                        <h5 class="modal-title" id="titleModal"></h5>
                    </div>
                    <div class="col-md-5">
                        <div class="float-right">
                            <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Tutup</button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col mb-3">
                            <div class="white_card" style="background:#E2FFE2;">
                                <div class="white_card_header">
                                    <div class="box_header m-0">
                                        <div class="main-title">
                                            <h3 class="m-0" id="titleSubModal1"></h3>
                                            <span id="jml_selesai">0</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="white_card_body QA_section">
                                    <div class="todo_wrapper" id="wrap_sudah">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="white_card" style="background:#FFEFF1;">
                                <div class="white_card_header">
                                    <div class="box_header m-0">
                                        <div class="main-title">
                                            <h3 class="m-0" id="titleSubModal2"></h3>
                                            <span id="jml_belum">0</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="white_card_body QA_section">
                                    <div class="todo_wrapper" id="wrap_belum">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#table_detail_pemetaan').DataTable({
                ajax: {
                    url:"{{ url('/api/detailpemetaansdgs/sdgsdashboard') }}"
                },
                columns: [
                    {data: 'nama_tujuan'},
                    {data: 'target_global',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'target_nasional',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'nama_indikator_kota',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'nama_program',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'nama_kegiatan',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'nama_sub_kegiatan',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                    {data: 'nama_skpd',
                        "render": function ( data, type, row, meta ) {
                            return data
                        }
                    },
                ],
                "scrollX": true
            });
        });

        function openModal(id,k){
            $('#infoDetailPd').modal('show')

            $.ajax({
                type: "get",
                url: "/api/infodetailpdsdgs/sdgsdashboard",
                data: {
                    id_skpd: id
                },
                success: function (response) {
                    console.log(response)
                    var html = '',html2 = '',jml_selesai = 0, jml_belum = 0
                    $.each(response.data, function(key, value) {
                        //pemetaan
                        if(k == 1){
                            $('#titleModal').text('Info Detail Pemetaan Perangkat Daerah')
                            $('#titleSubModal1').text('Indikator Kota Yang Sudah Selesai di Petakan')
                            $('#titleSubModal2').text('Indikator Kota Yang Belum Selesai di Petakan')
                            if(value.id_log_indikatorkota != null){
                                html = html+'<div class="single_todo d-flex justify-content-between align-items-center">'+
                                    '<div class="lodo_left d-flex align-items-center">'+
                                    '<div class="bar_line mr_10"></div>'+
                                    '<div class="todo_head">'+
                                    '<h5 class="f_s_18 f_w_600 mb-0">'+value.nama_indikator_kota+'</h5>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>';
                                jml_selesai++
                            }else if(value.id_log_indikatorkota == null){
                                html2 = html2+'<div class="single_todo d-flex justify-content-between align-items-center">'+
                                    '<div class="lodo_left d-flex align-items-center">'+
                                    '<div class="bar_line red_line mr_10"></div>'+
                                    '<div class="todo_head">'+
                                    '<h5 class="f_s_18 f_w_600 mb-0">'+value.nama_indikator_kota+'</h5>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>';
                                jml_belum++    
                            }
                        }else if(k == 2){
                            $('#titleModal').text('Info Detail Input Monev Perangkat Daerah')
                            $('#titleSubModal1').text('Indikator Kota Yang Sudah Selesai di Monev')
                            $('#titleSubModal2').text('Indikator Kota Yang Belum Selesai di Monev')
                            if(value.realisasi_target_1 != null && value.realisasi_target_2 != null){
                                html = html+'<div class="single_todo d-flex justify-content-between align-items-center">'+
                                    '<div class="lodo_left d-flex align-items-center">'+
                                    '<div class="bar_line mr_10"></div>'+
                                    '<div class="todo_head">'+
                                    '<h5 class="f_s_18 f_w_600 mb-0">'+value.nama_indikator_kota+'</h5>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>';
                                jml_selesai++
                            }else{
                                html2 = html2+'<div class="single_todo d-flex justify-content-between align-items-center">'+
                                    '<div class="lodo_left d-flex align-items-center">'+
                                    '<div class="bar_line red_line mr_10"></div>'+
                                    '<div class="todo_head">'+
                                    '<h5 class="f_s_18 f_w_600 mb-0">'+value.nama_indikator_kota+'</h5>'+
                                    '</div>'+
                                    '</div>'+
                                    '</div>';
                                jml_belum++    
                            }
                        }
                    });
                    $('#wrap_sudah').html(html);
                    $('#jml_selesai').text(jml_selesai);
                    $('#wrap_belum').html(html2);
                    $('#jml_belum').text(jml_belum);
                },
                error: function (data) {
                    console.log('Error:', data);           
                }
            });
            return false;
        }
    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
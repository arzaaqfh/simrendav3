@extends('layouts.sdgs')

@section('content')

<!-- Side Bar -->
@include('sdgs.sidebar')

<!-- Top Bar -->
@include('layouts.topbar')

<!-- Begin Page Content -->
<div class="main_content_iner ">
    <div class="container-fluid p-0">
        <!-- page title  -->
        <div class="row">
            <div class="col-12">
                <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                    <div class="page_title_left">
                        <h3 class="f_s_25 f_w_700 dark_text">Dashboard SDGS</h3>
                        <ol class="breadcrumb page_bradcam mb-0">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SDGS</a></li>
                            <li class="breadcrumb-item active">Rekap SDGs</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="white_card card_height_100 mb_30">
                    <div class="white_card_header">
                        <div class="box_header m-0">
                            <div class="main-title">
                                <h3 class="m-0">Rekapan SDGs</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            @if(session()->get('id_role') == 1 || session()->get('id_role') == 13 || session()->get('id_role') == 15)
            <div class="col">
                <div class="white_card mb_30" style="background-color:#EEE5FF;">
                    <div class="white_card_header">
                        <div class="box_header m-0">
                            <div class="main-title">
                                <h3 class="m-0">Perangkat Daerah</h3>
                                <span>Jumlah Perangkat Daerah Terlibat SDGs</span>
                            </div>
                        </div>
                    </div>
                    <div class="white_card_body">
                        <div class="monthly_plan_wraper">
                            <div class="single_plan d-flex align-items-center justify-content-between">
                                <div class="plan_left d-flex align-items-center">
                                    <div class="thumb">
                                        <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                    </div>
                                    <div>
                                        <h5 id="jml_pd_pemetaan">0</h5>
                                        <span>Sudah Melakukan Pemetaan</span>
                                    </div>
                                </div>
                                <span class="brouser_btn" id="jml_pd_pemetaan_minus">0</span>
                            </div>
                            <div class="single_plan d-flex align-items-center justify-content-between">
                                <div class="plan_left d-flex align-items-center">
                                    <div class="thumb">
                                        <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                    </div>
                                    <div>
                                        <h5 id="jml_pd_monev">0</h5>
                                        <span>Sudah Mengisi Monev</span>
                                    </div>
                                </div>
                                <span class="brouser_btn" id="jml_pd_monev_minus">0</span>
                            </div>
                            <div class="single_plan d-flex align-items-center justify-content-between">
                                <div class="plan_left d-flex align-items-center">
                                    <div class="thumb">
                                        <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                    </div>
                                    <div>
                                        <h5 id="jml_total_pd">0</h5>
                                        <span>Total Perangkat Daerah</span>
                                    </div>
                                </div>
                            </div>
                            <div class="single_plan d-flex align-items-center justify-content-between">
                                <div class="plan_left d-flex align-items-center">
                                    <div class="thumb"></div>
                                    <div>
                                        <span><a target="_blank" href="{{ url('/sdgs/dashboard/detailrekap/pd') }}">Lihat Detail</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="white_card mb_30">
                    <div class="white_card_header">
                        <div class="box_header m-0">
                            <div class="main-title">
                                <h3 class="m-0">Pemetaan SDGs</h3>
                                <span>Jumlah Data SDGs terakomodir di Renja</span>
                            </div>
                        </div>
                    </div>
                    <div class="white_card_body">
                        <div class="row">
                            <div class="col">
                                <div class="monthly_plan_wraper">
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_tujuan_global">0</h5>
                                                <span>Tujuan Global</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_target_nasional">0</h5>
                                                <span>Tujuan Nasional</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_indikator_nasional">0</h5>
                                                <span>Indikator Nasional</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_indikator_kota">0</h5>
                                                <span>Indikator Kota</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="monthly_plan_wraper">
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_program">0</h5>
                                                <span>Program</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_kegiatan">0</h5>
                                                <span>Kegiatan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb">
                                                <i class="fa fa-info-circle" aria-hidden="true" style="font-size: 40px;"></i>
                                            </div>
                                            <div>
                                                <h5 id="jml_subkegiatan">0</h5>
                                                <span>Sub Kegiatan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_plan d-flex align-items-center justify-content-between">
                                        <div class="plan_left d-flex align-items-center">
                                            <div class="thumb"></div>
                                            <div>
                                                <span>
                                                    <!--<a target="_blank" href="{{ url('/sdgs/dashboard/detailrekap/pemetaan') }}">Lihat Detail</a>-->
                                                    <!--<a href="#">Lihat Detail</a>-->
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-lg-12">
                <div class="white_card card_height_100 mb_30">
                    <div class="white_card_header">
                        <div class="box_header m-0">
                            <div class="main-title">
                                <h3 class="m-0">Cetak Data</h3>
                            </div>
                        </div>
                    </div>
                    <div class="white_card_body">
                    </div>
                </div>
            </div>
            <div class="col-xl-12">
                <div class="white_card card_height_100 mb_30 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white_card_header">
                                <div class="box_header m-0">
                                    <div class="main-title">
                                        <h3 class="m-0">Ketercapaian Pagu Indikator Kota </h3>
                                        <span>Data Ketercapaian Pagu Indikator Kota </span>
                                    </div>
                                </div>
                            </div>
                            <div class="white_card_body QA_section">
                                <div class="QA_table ">
                                    <!-- table-responsive -->
                                    <table class="table p-0" id="table_capaianpagu">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" scope="col">Indikator Nasional</th>
                                                <th rowspan="2" scope="col">Indikator Kota</th>
                                                <th rowspan="2" scope="col">Target Anggaran</th>
                                                <th colspan="2" scope="col">Realisasi Anggaran Semester I</th>
                                                <th colspan="2" scope="col">Realisasi Anggaran Semester II</th>
                                                <th rowspan="2" scope="col">Perangkat Daerah</th>
                                            </tr>
                                            <tr>
                                                <th>Realisasi (Rp.)</th>
                                                <th>Capaian (%)</th>
                                                <th>Realisasi (Rp.)</th>
                                                <th>Capaian (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12">
                <div class="white_card card_height_100 mb_30 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white_card_header">
                                <div class="box_header m-0">
                                    <div class="main-title">
                                        <h3 class="m-0">Ketercapaian Kinerja Indikator Kota </h3>
                                        <span>Data Ketercapaian Kinerja Indikator Kota </span>
                                    </div>
                                </div>
                            </div>
                            <div class="white_card_body QA_section">
                                <div class="QA_table ">
                                    <!-- table-responsive -->
                                    <table class="table p-0" id="table_capaiankinerja">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" scope="col">Indikator Nasional</th>
                                                <th rowspan="2" scope="col">Indikator Kota</th>
                                                <th rowspan="2" scope="col">Target Kinerja</th>
                                                <th colspan="2" scope="col">Realisasi Kinerja Semester I</th>
                                                <th colspan="2" scope="col">Realisasi Kinerja Semester II</th>
                                                <th rowspan="2" scope="col">Perangkat Daerah</th>
                                            </tr>
                                            <tr>
                                                <th>Realisasi (Rp.)</th>
                                                <th>Capaian (%)</th>
                                                <th>Realisasi (Rp.)</th>
                                                <th>Capaian (%)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="formcetak">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalTitle">Cetak Realisasi Indikator SDGs</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="white_card">
                            <div class="white_card_body">
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <label class="form-label" for="#">Perangkat Daerah</label>
                                        <div class="common_select">
                                            <select class="nice_Select wide mb_30" id="id_pd_cetak">
                                                <option value="">Pilih Perangkat Daerah...</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="white_card">
                            <div class="white_card_body">
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-danger" onclick="execcetak()">Cetak</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.nice_Select').niceSelect()
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        loaddatapdsdgs()
        loaddatapemetaansdgs()

        var table = $('#table_capaianpagu').DataTable({
            ajax: {
                url: "{{ url('/api/capaianpaguindsdgs/sdgsdashboard') }}",
                data: {
                    tahun: <?= session()->get('tahun_sdgs') ?>
                },
            },
            columns: [{
                    data: 'target_nasional'
                },
                {
                    data: 'nama_indikator_kota'
                },
                {
                    data: 'total_anggaran',
                    "render": function(data, type, row, meta) {
                        return $.fn.dataTable.render.number(',', '.', 0).display(data);
                    }
                },
                {
                    data: 'realisasi_anggaran_sem_1',
                    "render": function(data, type, row, meta) {
                        return $.fn.dataTable.render.number(',', '.', 0).display(data);
                    }
                },
                {
                    data: 'total_anggaran',
                    "render": function(data, type, row, meta) {
                        var persentase = 0
                        if (data != 0) {
                            persentase = ((row.realisasi_anggaran_sem_1 / data) * 100).toFixed(2)
                        }
                        return '<span class="' + (persentase == 0 ? "badge bg-light text-dark" : (persentase == 100) ? "badge bg-success" : (persentase > 100) ? "badge bg-danger" : "badge bg-info") + '">' + persentase + '</span>';
                    }
                },
                {
                    data: 'realisasi_anggaran_sem_2',
                    "render": function(data, type, row, meta) {
                        return $.fn.dataTable.render.number(',', '.', 0).display(data);
                    }
                },
                {
                    data: 'total_anggaran',
                    "render": function(data, type, row, meta) {
                        var persentase = 0
                        if (data != 0) {
                            persentase = ((row.realisasi_anggaran_sem_2 / data) * 100).toFixed(2)
                        }
                        return '<span class="' + (persentase == 0 ? "badge bg-light text-dark" : (persentase == 100) ? "badge bg-success" : (persentase > 100) ? "badge bg-danger" : "badge bg-info") + '">' + persentase + '</span>';
                    }
                },
                {
                    data: 'nama_skpd'
                },
            ],
            "scrollX": true
        });

        var table2 = $('#table_capaiankinerja').DataTable({
            ajax: {
                url: "{{ url('/api/capaiankinerjaindsdgs/sdgsdashboard') }}",
                data: {
                    tahun: <?= session()->get('tahun_sdgs') ?>
                },
            },
            columns: [{
                    data: 'target_nasional'
                },
                {
                    data: 'nama_indikator_kota'
                },
                {
                    data: 'target',
                    "render": function(data, type, row, meta) {
                        return data + ' ' + row.satuan;
                    }
                },
                {
                    data: 'realisasi_target_1',
                    "render": function(data, type, row, meta) {
                        if (data != '' && data != null) {
                            return data + ' ' + row.satuan;
                        } else {
                            return ''
                        }
                    }
                },
                {
                    data: 'capaian_target_1',
                    "render": function(data, type, row, meta) {
                        if (!isNaN(data) && data != null) {
                            var persentase = (parseFloat(data)).toFixed(2)
                            return '<span class="' + (persentase == 0 ? "badge bg-light text-dark" : (persentase == 100) ? "badge bg-success" : (persentase > 100) ? "badge bg-danger" : "badge bg-info") + '">' + persentase + '</span>';
                        } else {
                            return ''
                        }
                    }
                },
                {
                    data: 'realisasi_target_2',
                    "render": function(data, type, row, meta) {
                        if (data != '' && data != null) {
                            return data + ' ' + row.satuan;
                        } else {
                            return ''
                        }
                    }
                },
                {
                    data: 'capaian_target_2',
                    "render": function(data, type, row, meta) {
                        if (!isNaN(data) && data != null) {
                            var persentase = (parseFloat(data)).toFixed(2)
                            return '<span class="' + (persentase == 0 ? "badge bg-light text-dark" : (persentase == 100) ? "badge bg-success" : (persentase > 100) ? "badge bg-danger" : "badge bg-info") + '">' + persentase + '</span>';
                        } else {
                            return ''
                        }
                    }
                },
                {
                    data: 'nama_skpd'
                },
            ],
            "scrollX": true
        });
    });

    function loaddatapdsdgs() {
        $.ajax({
            type: 'GET',
            url: "/api/pdsdgs/sdgsdashboard",
            data: {
                tahun: <?= session()->get('tahun_sdgs') ?>
            },
            success: function(response) {
                $.each(response.data, function(key, value) {
                    $('#jml_pd_pemetaan').text(value.jumlah_pemetaan)
                    $('#jml_pd_pemetaan_minus').text('-' + (value.total_skpd - value.jumlah_pemetaan))
                    $('#jml_pd_monev').text(value.sudah_monev)
                    $('#jml_pd_monev_minus').text('-' + (value.total_skpd - value.sudah_monev))
                    $('#jml_total_pd').text(value.total_skpd)
                });
            }
        });
    }

    function loaddatapemetaansdgs() {
        $.ajax({
            type: 'GET',
            url: "/api/pemetaansdgs/sdgsdashboard",
            data: {
                tahun: <?= session()->get('tahun_sdgs') ?>
            },
            success: function(response) {
                $.each(response.data, function(key, value) {
                    $('#jml_tujuan_global').text(value.jumlah_tujuan_global)
                    $('#jml_target_nasional').text(value.jumlah_tujuan_nasional)
                    $('#jml_indikator_nasional').text(value.jumlah_indikator_nasional)
                    $('#jml_indikator_kota').text(value.jumlah_indikator_kota)
                    $('#jml_program').text(value.jumlah_program)
                    $('#jml_kegiatan').text(value.jumlah_kegiatan)
                    $('#jml_subkegiatan').text(value.jumlah_subkegiatan)
                });
            }
        });
    }

    function openformcetak() {
        if ({{ Session::get('id_role') }} != 4) {
            $('#formcetak').modal('show')
            loadskpd(0, '#id_pd_cetak')
        } else {
            execcetak(true)
        }
    }

    function execcetak(bypd = false) {
        if (bypd) {
            var pd = <?php echo json_encode(Session::get('id_skpd')); ?>;
            window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=sdgs/2021/cetak_sdgs2021-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&id_pd=' + pd);
        } else {
            var pd = $('#id_pd_cetak').val()
            if (pd) {
                window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=sdgs/2021/cetak_sdgs2021-pd.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10&id_pd=' + pd);
            } else {
                window.open('https://simrenda-rpt.cimahikota.go.id/birt-viewer/frameset?__report=sdgs/2021/cetak_sdgs2021.rptdesign&__format=xls&__svg=true&__locale=en_US&__timezone=VST&__masterpage=true&__rtl=false&__cubememsize=10');
            }
        }
    }

    function loadskpd(id_pd = 0, elm) {
        $.ajax({
            type: 'GET',
            url: "{{ url('/api/renja/perangkatdaerah') }}",
            data: {
                id_pd: {{ $id_pd }}
            },
            success: function(response) {
                $('#id_pd_cetak').empty();
                $('#id_pd_cetak').append(
                    $('<option></option>').val('').html('Semua Perangkat Daerah')
                );
                $.each(response.data, function(key, value) {
                    $('#id_pd_cetak').append(
                        $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                    );
                });
                if (id_pd != 0) {
                    $('#id_pd_cetak').val(id);
                }
                $('#id_pd_cetak').niceSelect('update');
            }
        });
    }
</script>

<!-- Footer -->
@include('layouts.footer')
@endsection
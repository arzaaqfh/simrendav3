<table style="border: 5px solid black">
    <thead style="font-size: 10px; text-align: center;">
        <tr>
            <th colspan="18">
                REALISASI INDIKATOR SDGS TAHUN {{ $tahun }}
            </th>
        </tr>
        <tr>
            <th colspan="18"></th>
        </tr>
        <tr>
            <th rowspan="3">Target Nasional</th>
            <th rowspan="3">Indikator Nasional</th>
            <th rowspan="3">Indikator Kota</th>
            <th rowspan="3">Program</th>
            <th rowspan="3">Kegiatan</th>
            <th rowspan="3">Sub Kegiatan</th>
            <th rowspan="3">Target</th>
            <th rowspan="3">Satuan</th>
            <th colspan="4">Realisasi Target Tahun {{ $tahun-1 }}</th>
            <th rowspan="3">Pagu Anggaran</th>
            <th colspan="4">Realisasi Anggaran Tahun {{ $tahun-1 }}</th>
            <th rowspan="3">Nama Perangkat Daerah</th>
        </tr>
        <tr>
            <th colspan="2">Semester 1</th>
            <th colspan="2">Semester 2</th>
            <th colspan="2">Semester 1</th>
            <th colspan="2">Semester 2</th>
        </tr>
        <tr>
            <th>Realisasi</th>
            <th>Capaian (%)</th>
            <th>Realisasi</th>
            <th>Capaian (%)</th>
            <th>Realisasi</th>
            <th>Capaian (%)</th>
            <th>Realisasi</th>
            <th>Capaian (%)</th>
        </tr>
    </thead>
    <tbody style="font-size: 9px;">
        @foreach ($dataCetak as $dat)
        <tr>
            <td colspan="18">{{ $dat->nama_tujuan }}</td>
        </tr>
        <tr>
            <td>{{ $dat->target_global }}</td>
            <td>{{ $dat->target_nasional }}</td>
            <td>{{ $dat->nama_indikator_kota }}</td>
            <td>{{ $dat->nama_program }}</td>
            <td>{{ $dat->nama_kegiatan }}</td>
            <td>{{ $dat->nama_sub_kegiatan }}</td>
            <td>{{ $dat->target }}</td>
            <td>{{ $dat->satuan }}</td>
            <td>{{ $dat->realisasi_target_1 }}</td>
            <td>{{ $dat->capaian_target_1 }}</td>
            <td>{{ $dat->realisasi_target_2 }}</td>
            <td>{{ $dat->capaian_target_2 }}</td>
            <td>{{ number_format($dat->anggaran,2,',','.') }}</td>
            <td>{{ number_format($dat->realisasi_anggaran_1,2,',','.') }}</td>
            <td>{{ number_format($dat->capaian_anggaran_1,2,',','.') }}</td>
            <td>{{ number_format($dat->realisasi_anggaran_2,2,',','.') }}</td>
            <td>{{ number_format($dat->capaian_anggaran_2,2,',','.') }}</td>
            <td>{{ $dat->nama_skpd }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

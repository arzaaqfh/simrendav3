@extends('layouts.sdgs')

@section('content')

<!-- Side Bar -->
@include('sdgs.sidebar')

<!-- Top Bar -->
@include('layouts.topbar')

<!-- Begin Page Content -->
<div class="main_content_iner ">
    <div class="container-fluid p-0">
        <!-- page title  -->
        <div class="row">
            <div class="col-12">
                <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                    <div class="page_title_left">
                        <h3 class="f_s_25 f_w_700 dark_text">Indikator SDGS</h3>
                        <ol class="breadcrumb page_bradcam mb-0">
                            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SDGS</a></li>
                            <li class="breadcrumb-item active">Indikator Kota</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="white_card card_height_100 mb_30">
                    <div class="white_card_header">
                        <div class="box_header m-0">
                            <div class="main-title">
                                <h3 class="m-0">Indikator SDGS</h3>
                            </div>
                        </div>
                    </div>
                    <div class="white_card_body">
                        <div class="QA_section">
                            <div class="white_box_tittle list_header">
                                <h4>Data Indikator Kota Cimahi</h4>
                                <div class="box_right d-flex lms_block">
                                    <div class="serach_field_2">
                                        <div class="search_inner">
                                            <form Active="#">
                                                <div class="search_field">
                                                    <input id="dtsearch" type="text" onkeydown="return (event.keyCode!=13);" placeholder="Search content here...">
                                                </div>
                                                <button type="button"> <i class="ti-search"></i> </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="add_button ms-2">
                                        <button class="btn_1" onclick="openModal('add')">Tambah</a>
                                    </div>
                                </div>
                            </div>
                            <div class="QA_table mb_30">
                                <table class="table table-striped table_indkota">
                                    <thead>
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th>Indikator Nasional</th>
                                            <th>Indikator Kota</th>
                                            <th>Target</th>
                                            <th>Perangkat Daerah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="addsdgsindKota">
    <form id="sdgsindkotaForm">
        <input type="hidden" name="id_target_realisasi_kota" id="id_target_realisasi_kota">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-6">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                    </div>
                    <div class="col-md-5">
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                            <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="white_card">
                                <div class="white_card_body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label class="form-label">Tujuan Global</label>
                                            <div class="common_select">
                                                <select class="nice_Select wide mb_30" id="id_sdgs" name="id_sdgs" onchange="loadglobal(this.value,0,0)">
                                                    <option value="">Pilih Tujuan Global...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <label class="form-label">Target Nasional</label>
                                            <div class="common_select">
                                                <select class="nice_Select wide mb_30" id="id_target_global" name="id_target_global" onchange="loadnasional(this.value,0)">
                                                    <option value="">Pilih Target Nasional...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <label class="form-label">Indikator Nasional</label>
                                            <div class="common_select">
                                                <select class="nice_Select wide mb_30" id="id_target_nasional" name="id_target_nasional" onchange="loadmasterindkota()">
                                                    <option value="">Pilih Indikator Nasional...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" id="select_indikator_kota">
                                            <label class="form-label">Indikator Kota</label>
                                            <div class="input-group">
                                                <select class="selectpicker form-control" data-live-search="true" id="id_indikator_kota" name="id_indikator_kota">
                                                    <option value="">Pilih Indikator Kota...</option>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-outline-success" type="button" onclick="addIndikator()"><i class="fa fa-plus" aria-hidden="true"></i> Indikator</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 d-none" id="text_indikator_kota">
                                            <label class="form-label">Indikator Kota</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="val_text_indikator_kota">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-outline-warning" type="button" onclick="pushIndikator()"><i class="fa fa-save" aria-hidden="true"></i> Tambahkan</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 mt-4">
                                            <label class="form-label">Target</label>
                                            <input class="form-control" name="target" id="target" placeholder="" required>
                                        </div>
                                        <div class="col-lg-4 mt-4">
                                            <label class="form-label">Satuan</label>
                                            <div class="common_select">
                                                <select class="nice_Select wide mb_30" id="satuan" name="satuan">
                                                    <option value="">Pilih Satuan...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 mt-4">
                                            <label class="form-label">Perangkat Daerah</label>
                                            <div class="common_select">
                                                <select class="selectpicker wide mb_30" multiple aria-label="Default select example" data-live-search="true" id="perangkat_daerah" name="perangkat_daerah[]">
                                                    <option value="">Pilih Perangkat Daerah...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="tahun" value="<?= session()->get('tahun_sdgs'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    var token = $("meta[name='csrf-token']").attr("content");
    var tahun = "<?= session()->get('tahun_sdgs'); ?>"
    
    $(document).ready(function() {
        loadpd()
        loadsatuan()
        $('.nice_Select').niceSelect()
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.table_indkota').DataTable({
            processing: true,
            serverSide: true,
            dom: 'lrtip',
            "lengthChange": true,
            "ajax": {
                "url": "{{ route('sdgsindkota.index') }}",
                "data": {
                    tahun: tahun
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'target_nasional',
                    name: 'target_nasional'
                },
                {
                    data: 'nama_indikator_kota',
                    name: 'nama_indikator_kota'
                },
                {
                    data: 'target',
                    name: 'target'
                },
                {
                    data: 'nama_skpd',
                    name: 'nama_skpd'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    width: "20%",
                    className: "text-center"
                },

            ],
            rowsGroup: [
                5, 3, 2, 1, 0
            ]
        });

        $('#dtsearch').keyup(function() {
            table.search($(this).val()).draw();
        })

        $('#saveBtn').click(function(e) {
            e.preventDefault();
            //$(this).html('Sending..');
            Swal.fire({
                html: 'menyimpan data ...',
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            })

            if ($('#id_target_realisasi_kota').val() == '') {
                var url = "{{ route('sdgsindkota.store') }}"
            } else {
                var url = "/api/update/sdgsindkota/" + $('#id_target_realisasi_kota').val()
            }

            $.ajax({
                data: $('#sdgsindkotaForm').serialize(),
                url: url,
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    $('#addsdgsindKota').modal('hide');
                    table.draw();
                    Swal.fire({
                        title: 'Berhasil!',
                        html: data.success,
                        type: 'success'
                    });
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#saveBtn').html('Save Changes');
                    Swal.fire({
                        title: 'Gagal!!',
                        html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                        type: 'danger'
                    });
                }
            });
        });

        $('body').on('click', '.deleteIndkotaSdgs', function() {
            var kota_id = $(this).data("id");
            //confirm("Apakah yakin akan menghapus data '"+$(this).data("id")+"'?");
            Swal.fire({
                title: 'Apakah yakin akan menghapus data ' + kota_id + '?',
                icon: 'question',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Ya',
                denyButtonText: 'Tidak',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    Swal.fire({
                        html: 'menghapus data ...',
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        }
                    })
                    $.ajax({
                        type: "post",
                        url: "/api/delete/sdgsindkota",
                        data: {
                            id: kota_id,
                            _token: token,
                        },
                        success: function(data) {
                            table.draw();
                            Swal.fire({
                                title: 'Berhasil!',
                                html: data.success,
                                type: 'success'
                            });
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            Swal.fire({
                                title: 'Gagal!!',
                                html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                type: 'danger'
                            });
                        }
                    });
                } else if (result.isDenied) {
                    //Swal.fire('', '', 'info')
                }
            })
        });

        $('body').on('click', '.editIndkotaSdgs', function() {
            var kota_id = $(this).data("id");
            openModal(kota_id)
        });
    });

    function addIndikator() {
        $('#select_indikator_kota').addClass("d-none");
        $('#text_indikator_kota').removeClass("d-none")
    }

    function pushIndikator() {
        if ($('#val_text_indikator_kota').val()) {
            $('#id_indikator_kota').append(
                $('<option></option>').val($('#val_text_indikator_kota').val()).html($('#val_text_indikator_kota').val())
            );
            $("#id_indikator_kota").val($('#val_text_indikator_kota').val());
            $("#id_indikator_kota").selectpicker("refresh");
        }

        $('#val_text_indikator_kota').val("");
        $('#select_indikator_kota').removeClass("d-none");
        $('#text_indikator_kota').addClass("d-none")
    }

    function openModal(a) {
        $('#id_target_realisasi_kota').val('')
        $('#sdgsindkotaForm').trigger("reset");
        $('#satuan').val('');
        $('#satuan').niceSelect('update');
        $('#perangkat_daerah').val('');
        $("#perangkat_daerah").selectpicker("refresh");
        if (a == 'add') {
            $('#exampleModalLongTitle').text('Tambah Data Indikator Kota')
            loadtujuan()
            loadmasterindkota()
        } else {
            $('#exampleModalLongTitle').text('Edit Data Indikator Kota')
            $.ajax({
                type: "get",
                url: "/api/show/sdgsindkota/" + a,
                data: {
                    _token: token,
                    tahun: tahun
                },
                success: function(response) {
                    loadtujuan(response.data[0].id_sdgs, response.data[0].id_target_global, response.data[0].id_target_nasional)
                    $('#id_target_realisasi_kota').val(response.data[0].id_target_realisasi_kota)
                    loadmasterindkota(response.data[0].id_indikator_kota)
                    $('#target').val(response.data[0].target)
                    $('#satuan').val(response.data[0].satuan)
                    $('#satuan').niceSelect('update');
                    if (response.data.length > 0) {
                        var pd_arr = []
                        for (let index = 0; index < response.data.length; index++) {
                            if (response.data[index].id_skpd != null) {
                                pd_arr.push(response.data[index].id_skpd)
                            }
                        }
                    }
                    $("#perangkat_daerah").val(pd_arr);
                    $("#perangkat_daerah").selectpicker("refresh");
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }
        $('#addsdgsindKota').modal('show')
    }

    function loadmasterindkota(id = 0) {
        $.ajax({
            type: 'GET',
            url: "/api/get/master/sdgsindkota",
            data:{
                id_target_nasional : $('#id_target_nasional').val()
            },
            success: function(response) {
                $('#id_indikator_kota').empty();
                $('#id_indikator_kota').append(
                    $('<option></option>').val('').html('Pilih Indikator Kota')
                );
                $.each(response.data, function(key, value) {
                    $('#id_indikator_kota').append(
                        $('<option></option>').val(value['id_indikator_kota']).html(value['nama_indikator_kota'])
                    );
                });
                if (id != 0) {
                    $('#id_indikator_kota').val(id);
                }
                $("#id_indikator_kota").selectpicker("refresh");
            }
        });
    }

    function loadtujuan(id = 0, id_g = 0, id_n = 0) {
        $.ajax({
            type: 'GET',
            url: "/api/showall/sdgstujuan",
            success: function(response) {
                $('#id_sdgs').empty();
                $('#id_sdgs').append(
                    $('<option></option>').val('').html('Pilih Tujuan Global...')
                );
                $.each(response.data, function(key, value) {
                    $('#id_sdgs').append(
                        $('<option></option>').val(value['id_sdgs']).html(value['nama_tujuan'])
                    );
                });
                if (id != 0) {
                    $('#id_sdgs').val(id);
                }
                $('#id_sdgs').niceSelect('update');
                loadglobal(id, id_g, id_n)
            }
        });
    }

    function loadglobal(id_t, id = 0, id_n = 0) {
        $.ajax({
            type: 'GET',
            url: "/api/showbytujuan/sdgsindglobal/" + id_t,
            success: function(response) {
                $('#id_target_global').empty();
                $('#id_target_global').append(
                    $('<option></option>').val('').html('Pilih Target Nasional...')
                );
                $.each(response.data, function(key, value) {
                    $('#id_target_global').append(
                        $('<option></option>').val(value['id_target_global']).html(value['target_global'])
                    );
                });
                if (id != 0) {
                    $('#id_target_global').val(id);
                }
                $('#id_target_global').niceSelect('update');
                loadnasional(id, id_n)
            }
        });
    }

    function loadnasional(id_g, id = 0) {
        $.ajax({
            type: 'GET',
            url: "/api/showbyindglobal/sdgsindnasional/" + id_g,
            success: function(response) {
                $('#id_target_nasional').empty();
                $('#id_target_nasional').append(
                    $('<option></option>').val('').html('Pilih Indikator Nasional...')
                );
                $.each(response.data, function(key, value) {
                    $('#id_target_nasional').append(
                        $('<option></option>').val(value['id_target_nasional']).html(value['target_nasional'])
                    );
                });
                if (id != 0) {
                    $('#id_target_nasional').val(id);
                }
                $('#id_target_nasional').niceSelect('update');
            }
        });
    }

    function loadsatuan(val = "") {
        $.ajax({
            type: 'GET',
            url: "{{ url('/api/renja/satuan') }}",
            success: function(response) {
                $('#satuan').empty();
                $('#satuan').append(
                    $('<option></option>').val('').html('Pilih Satuan...')
                );
                $.each(response.data, function(key, value) {
                    $('#satuan').append(
                        $('<option></option>').val(value['satuan']).html(value['satuan'])
                    );
                });
                if (val) {
                    var d = val.replace(/"/g, '')
                    $('#satuan').val(String(d))
                }
                $('#satuan').niceSelect('update');
            }
        });
    }

    function loadpd(id = 0) {
        $.ajax({
            type: 'GET',
            url: "{{ url('/api/renja/perangkatdaerah') }}",
            data: {
                id_pd: id
            },
            success: function(response) {
                $('#perangkat_daerah').empty();
                $.each(response.data, function(key, value) {
                    $('#perangkat_daerah').append(
                        $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                    );
                });
                if (id != 0) {
                    $('#perangkat_daerah').val(id);
                }
                $("#perangkat_daerah").selectpicker("refresh");
                //$('#perangkat_daerah').niceSelect('update');
            }
        });
    }
</script>

<!-- Footer -->
@include('layouts.footer')
@endsection
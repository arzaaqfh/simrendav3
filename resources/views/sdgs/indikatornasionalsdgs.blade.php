@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Indikator SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SDGS</a></li>
                                <li class="breadcrumb-item active">Indikator Nasional</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Indikator SDGS</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Indikator Nasional</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" onkeydown="return (event.keyCode!=13);" placeholder="Search content here...">
                                                    </div>
                                                    <button type="button"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="add_button ms-2">
                                            <button class="btn_1" onclick="openModal('add')">Tambah</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table class="table lms_table_active table_indnasional">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Nama Tujuan</th>
                                                <th>Target Nasional</th>
                                                <th>Indikator Nasional</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" id="addsdgsNasional">
        <form id="sdgsnasionalForm">
            <input type="hidden" name="id_target_nasional" id="id_target_nasional">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label">Tujuan Global</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_sdgs" name="id_sdgs" onchange="loadglobal(this.value,0)">
                                                        <option value="">Pilih Tujuan GLobal...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Target Nasional</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_target_global" name="id_target_global">>
                                                        <option value="">Pilih Target Nasional...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Nasional</label>
                                                <textarea class="form-control" rows="3" name="target_nasional" id="target_nasional" placeholder="" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        $( document ).ready(function() {
            $('.nice_Select').niceSelect()
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_indnasional').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                "lengthChange": false,
                ajax: "{{ route('sdgsindnasional.index') }}",
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {data: 'nama_tujuan', name: 'nama_tujuan'},
                    {title: 'Target Nasional', data: 'target_global', name: 'target_global'},
                    {title: 'Indikator Nasional', data: 'target_nasional', name: 'target_nasional'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width:"20%", className: "text-center"},

                ]
            });

            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                //$(this).html('Sending..');
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                
                if($('#id_target_nasional').val() == ''){
                    var url = "{{ route('sdgsindnasional.store') }}"
                }else{
                    var url = "/api/update/sdgsindnasional/"+$('#id_target_nasional').val()
                }

                $.ajax({
                    data: $('#sdgsnasionalForm').serialize(),
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#addsdgsNasional').modal('hide');
                        table.draw();
                        Swal.fire({
                            title: 'Berhasil!',
                            html: data.success,
                            type: 'success'
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        //$('#saveBtn').html('Save Changes');
                        Swal.fire({
                            title: 'Gagal!!',
                            html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                            type: 'danger'
                        });
                    }
                });
            });

            $('body').on('click', '.deleteNasionalSdgs', function () {
                var nasional_id = $(this).data("id");
                //confirm("Apakah yakin akan menghapus data '"+$(this).data("id")+"'?");
                Swal.fire({
                    title: 'Apakah yakin akan menghapus data '+nasional_id+'?',
                    icon: 'question',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    denyButtonText: 'Tidak',
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.value) {
                        Swal.fire({
                            html: 'menghapus data ...',
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            }
                        })
                        $.ajax({
                            type: "post",
                            url: "/api/delete/sdgsindnasional",
                            data: {
                                id: nasional_id,
                                _token: token,
                            },
                            success: function (data) {
                                table.draw();
                                Swal.fire({
                                    title: 'Berhasil!',
                                    html: data.success,
                                    type: 'success'
                                });
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                Swal.fire({
                                    title: 'Gagal!!',
                                    html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                    type: 'danger'
                                });
                            }
                        });
                    } else if (result.isDenied) {
                        //Swal.fire('', '', 'info')
                    }
                })
            });

            $('body').on('click', '.editNasionalSdgs', function () {
                var nasional_id = $(this).data("id");
                openModal(nasional_id)
            });
        });

        function openModal(a){
            $('#id_target_nasional').val('')
            $('#sdgsnasionalForm').trigger("reset");
            if(a == 'add'){
                $('#exampleModalLongTitle').text('Tambah Data Indikator Nasional')
                loadtujuan()
            }else{
                $('#exampleModalLongTitle').text('Edit Data Indikator Nasional')
                $.ajax({
                    type: "get",
                    url: "/api/show/sdgsindnasional/"+a,
                    data: {
                        _token: token,
                    },
                    success: function (response) {
                        console.log(response)
                        loadtujuan(response.data[0].id_sdgs,response.data[0].id_target_global)
                        $('#id_target_nasional').val(response.data[0].id_target_nasional)
                        $('#target_nasional').val(response.data[0].target_nasional)
                    },
                    error: function (data) {
                        console.log('Error:', data);           
                    }
                });
            }
            $('#addsdgsNasional').modal('show')
        }

        function loadtujuan(id=0,id_g=0){
            $.ajax({
                type: 'GET',
                url: "/api/showall/sdgstujuan",
                success: function(response) {
                    $('#id_sdgs').empty();
                    $('#id_sdgs').append(
                        $('<option></option>').val('').html('Pilih Tujuan Global...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_sdgs').append(
                            $('<option></option>').val(value['id_sdgs']).html(value['nama_tujuan'])
                        );
                    });
                    if(id!=0){
                        $('#id_sdgs').val(id);
                    }
                    $('#id_sdgs').niceSelect('update');
                    loadglobal(id,id_g)
                }
            });
        }

        function loadglobal(id_t,id=0){
            $.ajax({
                type: 'GET',
                url: "/api/showbytujuan/sdgsindglobal/"+id_t,
                success: function(response) {
                    $('#id_target_global').empty();
                    $('#id_target_global').append(
                        $('<option></option>').val('').html('Pilih Target Nasional...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_target_global').append(
                            $('<option></option>').val(value['id_target_global']).html(value['target_global'])
                        );
                    });
                    if(id!=0){
                        $('#id_target_global').val(id);
                    }
                    $('#id_target_global').niceSelect('update');
                }
            });
        }
    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
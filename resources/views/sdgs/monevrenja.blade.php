@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Monev SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/sdgs') }}">SDGS</a></li>
                                <li class="breadcrumb-item active">Monev</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Indikator Sub Kegiatan Renja</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Monev SDGS terhadap SubKegiatan Kota Cimahi</h4>
                                    <div class="box_right d-flex lms_block">
                                    <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" placeholder="Search content here...">
                                                    </div>
                                                    <button type="submit"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table class="table lms_table_active table_coding table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Indikator Nasional</th>
                                                <th>Indikator Kota</th>
                                                <th>Program</th>
                                                <th>Kegiatan</th>
                                                <th>Sub Kegiatan</th>
                                                <th>Pagu</th>
                                                <th>Perangkat Daerah</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" id="addsdgsCoding">
        <form id="sdgscodingForm">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Nasional</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_target_nasional" disabled>
                                                        <option value="">Pilih Indikator Nasional...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Kota</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="id_indikator_kota" name="id_indikator_kota" readonly>
                                                        <option value="">Pilih Indikator Kota...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Perangkat Daerah</label>
                                                <div class="common_select">
                                                    <select class="nice_Select wide mb_30" id="perangkat_daerah" disabled>
                                                        <option value="">Pilih Perangkat Daerah...</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="white_card_body">
                                                    <div class="QA_section">
                                                        <div class="QA_table mb_30">
                                                            <table class="table lms_table_active" id="table_renja">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Program</th>
                                                                        <th>Kegiatan</th>
                                                                        <th>SubKegiatan</th>
                                                                        <th>Pagu</th>
                                                                        <th>Pilih</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        var table_renja,pd_key,global_tempval
        $( document ).ready(function() {
            $('.nice_Select').niceSelect()
            loadpd()
            pd_key = -1
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_coding').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                "lengthChange": false,
                ajax: "{{ route('sdgscoding.index') }}",
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {text:'Indikator Nasional', data: 'target_nasional', name: 'target_nasional'},
                    {data: 'nama_indikator_kota', name: 'nama_indikator_kota'},
                    {data: 'nama_program', name: 'nama_program'},
                    {data: 'nama_kegiatan', name: 'nama_kegiatan'},
                    {data: 'nama_sub_kegiatan', name: 'nama_sub_kegiatan'},
                    {data: 'apbd_kota', name: 'apbd_kota',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                    {data: 'nama_skpd', name: 'nama_skpd'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width:"20%", className: "text-center"},

                ],
                scrollX: true
            });

            table_renja = $('#table_renja').DataTable( {
                    ajax: {
                        url:"{{ url('/api/renja/subkegiatan') }}",
                        data: function(d){
                            d.periode_usulan = 2021,
                            d.step  = 'perubahan',
                            d.id_pd = pd_key
                        },
                    },
                    columns: [ 
                        { "data": "kegiatan.program.nama_program" },
                        { "data": "kegiatan.nama_kegiatan" },
                        { "data": "nama_sub_kegiatan",
                            "render": function ( data, type, row, meta ) {
                                return data
                            }
                        },
                        { "data": "anggaran",
                            "render": function ( data, type, row, meta ) {
                                return data
                            }
                        },
                        { "data": "id_log_renja",
                            "render": function ( data, type, row, meta ) {
                                var html = '<div class="form-check"><input class="form-check-input" type="checkbox" value="'+data+'" id="id_log'+data+'" name="ck_listcoding[]"><label class="form-check-label"></label></div>'
                                return html
                            }
                        },
                    ],
                } );

            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                
                var url = "{{ route('sdgscoding.store') }}"

                let myForm = document.getElementById('sdgscodingForm');
                let formData = new FormData(myForm);
                
                $.ajax({
                    type: "POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processData: false,
                    contentType: false,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $('#addsdgsCoding').modal('hide');
                        table.draw();
                        Swal.fire({
                            title: 'Berhasil!',
                            html: data.success,
                            type: 'success'
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        //$('#saveBtn').html('Save Changes');
                        Swal.fire({
                            title: 'Gagal!!',
                            html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                            type: 'danger'
                        });
                    }
                });
            });

            $('body').on('click', '.CodingSdgs', function () {
                var kota_id = $(this).data("id");
                openModal(kota_id)
            });
        });

        function loaddtrenja(k){
            pd_key = k
            table_renja.ajax.reload(setValRenja,false);
        }

        function setValRenja(){
            if(global_tempval.length > 0){
                for(var i=0;i<=global_tempval.length;i++){
                    $('#id_log'+global_tempval[i]['id_log_renja']).prop('checked', true);
                }
            }
        }

        function openModal(a){
            global_tempval = null
            $('#sdgscodingForm').trigger("reset");
                $('#exampleModalLongTitle').text('Coding Data Indikator Kota')
                $.ajax({
                    type: "get",
                    url: "/api/showbyindkota/sdgscoding/"+a,
                    data: {
                        _token: token,
                    },
                    success: function (response) {
                        loadnasional(response.data[0].id_target_nasional,response.data[0].id_indikator_kota)
                        $('#id_indikator_kota').val(response.data[0].id_indikator_kota)
                        $('#nama_indikator_kota').val(response.data[0].nama_indikator_kota)
                        $('#t4').val(response.data[0].t4)
                        $('#satuan').val(response.data[0].satuan)
                        $('#satuan').niceSelect('update');
                        $('#perangkat_daerah').val(response.data[0].perangkat_daerah)
                        $('#perangkat_daerah').niceSelect('update');

                        var key = response.data[0].perangkat_daerah
                        if(key.length > 0 && key != null){
                            loaddtrenja(response.data[0].perangkat_daerah)
                        }else{
                            loaddtrenja('-1')
                        }
                        global_tempval = response.data
                    },
                    error: function (data) {
                        console.log('Error:', data);           
                    }
                });
            $('#addsdgsCoding').modal('show')
        }

        function loadnasional(id=0,id_kota=0){
            $.ajax({
                type: 'GET',
                url: "/api/show/sdgsindnasional/"+id,
                success: function(response) {
                    $('#id_target_nasional').empty();
                    $('#id_target_nasional').append(
                        $('<option></option>').val('').html('Pilih Indikator Nasional...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_target_nasional').append(
                            $('<option></option>').val(value['id_target_nasional']).html(value['target_global'])
                        );
                    });
                    if(id!=0){
                        $('#id_target_nasional').val(id);
                    }
                    $('#id_target_nasional').niceSelect('update');
                    loadkota(id_kota)
                }
            });
        }

        function loadkota(id=0){
            $.ajax({
                type: 'GET',
                url: "/api/show/sdgsindkota/"+id,
                success: function(response) {
                    $('#id_indikator_kota').empty();
                    $.each(response.data, function(key, value) {
                        $('#id_indikator_kota').append(
                            $('<option></option>').val(value['id_indikator_kota']).html(value['nama_indikator_kota'])
                        );
                    });
                    if(id!=0){
                        $('#id_indikator_kota').val(id);
                    }
                    $('#id_indikator_kota').niceSelect('update');
                }
            });
        }

        function loadpd(id=0){
            $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:id
                    },
                    success: function(response) {
                        $('#perangkat_daerah').empty();
                        $('#perangkat_daerah').append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#perangkat_daerah').append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id != 0){
                            $('#perangkat_daerah').val(id);
                        }
                        $('#perangkat_daerah').niceSelect('update');
                    }
                });
        }
    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
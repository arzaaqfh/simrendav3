@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Monev SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/sdgs') }}">SDGS</a></li>
                                <li class="breadcrumb-item active">Monev</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Indikator Kota</h3>
                                </div>
                            </div>
                        </div>
                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Monev Indikator Kota Cimahi</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" onkeydown="return (event.keyCode!=13);" placeholder="Search content here...">
                                                    </div>
                                                    <button type="button"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30 table-responsive">
                                    <table class="table table-striped table_monevsdgs">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" width="5%">No.</th>
                                                <th rowspan="2">Indikator Nasional</th>
                                                <th rowspan="2">Indikator Kota</th>
                                                <th rowspan="2">Target</th>
                                                <th colspan="2">Realisasi</th>
                                                <th rowspan="2">Sumber Data</th>
                                                <th rowspan="2">Status Verifikasi</th>
                                                <th rowspan="2">Aksi</th>
                                            </tr>
                                            <tr>
                                                <th>Sem I</th>
                                                <th>Sem II</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" id="addsdgsindKota">
        <form id="monevSdgsindkotaForm">
            <input type="hidden" name="id_target_realisasi_kota" id="id_target_realisasi_kota">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-6">
                            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        </div>
                        <div class="col-md-5">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                                <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <div class="white_card">
                                    <div class="white_card_body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label class="form-label">Tujuan Global</label>
                                                <input class="form-control border-0 bg-primary pt-4 pb-4 m-b-10 text-white" id="id_sdgs" readonly>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Target Nasional</label>
                                                <input class="form-control border-0 bg-info pt-4 pb-4 m-b-10 text-white" id="id_target_global" readonly>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Nasional</label>
                                                <input class="form-control border-0 bg-success pt-4 pb-4 m-b-10 text-white" id="id_target_nasional" readonly>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="form-label">Indikator Kota</label>
                                                <div class="white_card" style="border: solid 4px #E2FFE2;">
                                                    <div class="white_card_body pt-4 pb-0">
                                                        <div class="row mb-3">
                                                            <div class="col-md-6">
                                                                <label class="form-label" >Nama Indikator</label><br>
                                                                <label class="form-label font-weight-bold" id="nama_indikator_kota"></label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="form-label" >Target</label><br>
                                                                <label class="form-label font-weight-bold" id="target"></label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="form-label">Satuan</label><br>
                                                                <label class="form-label font-weight-bold" id="satuan"></label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="form-label">Total Anggaran</label><br>
                                                                <label class="form-label font-weight-bold" id="anggaran"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Realisasi Target Semester I</label>
                                                <input class="form-control" name="realisasi_target_1" id="realisasi_target_1" onkeyup=renderPersentarget(1) placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Capaian Target Semester I (%)</label>
                                                <input class="form-control" name="capaian_target_1" id="capaian_target_1" placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Realisasi Anggaran Semester I</label>
                                                <input class="form-control" name="realisasi_anggaran_1" id="realisasi_anggaran_1" onkeyup=renderPersenanggaran(1) placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Capaian Anggaran Semester I (%)</label>
                                                <input class="form-control" name="capaian_anggaran_1" id="capaian_anggaran_1" placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Realisasi Target Semester II</label>
                                                <input class="form-control" name="realisasi_target_2" id="realisasi_target_2" onkeyup=renderPersentarget(2) placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Capaian Target Semester II (%)</label>
                                                <input class="form-control" name="capaian_target_2" id="capaian_target_2" placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Realisasi Anggaran Semester II</label>
                                                <input class="form-control" name="realisasi_anggaran_2" id="realisasi_anggaran_2" onkeyup=renderPersenanggaran(2) placeholder="" required>
                                            </div>
                                            <div class="col-lg-3 mt-4">
                                                <label class="form-label">Capaian Anggaran Semester II (%)</label>
                                                <input class="form-control" name="capaian_anggaran_2" id="capaian_anggaran_2" placeholder="" required>
                                            </div>
                                            <div class="col-lg-12 mt-4">
                                                <label class="form-label">Sumber Data</label>
                                                <textarea class="form-control" rows="3" name="sumber_data" id="sumber_data" placeholder=""></textarea>
                                            </div>
                                            <div class="col-lg-12 mt-4">
                                                <label class="form-label">Catatan</label>
                                                <textarea class="form-control" rows="3"  name="catatan" id="catatan" placeholder=""></textarea>
                                            </div>
                                            <div class="col-lg-12 mt-4">
                                                <label class="form-label">Lampiran</label>
                                                <div class="input-group mb-3 d-none" id="lampiran_file">
                                                    <input type="text" role="button" class="form-control border-0 text-decoration-underline text-primary" id="lampiran_name">
                                                    <div class="input-group-append">
                                                        <span class="btn btn-outline-danger" id="drop_lampiran"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;Hapus</span>
                                                    </div>
                                                </div>
                                                <input class="form-control" type="file" id="lampiran" name="lampiran">
                                                <input type="hidden" id="lampiran_emptied" name="lampiran_emptied">
                                            </div>
                                            @if(session()->get('id_role') == 13)
                                                <div class="col-lg-12 mt-4">
                                                    <label class="form-label">Status Veifikasi Sektor</label><br>
                                                    <div class="form-check mb-1">
                                                        <input class="form-check-input" type="checkbox" name="verified" value="<?=session()->get('id_jafung')?>" id="verified">
                                                        <label class="form-label form-check-label" for="gridCheck">
                                                        Verifikasi
                                                        </label>
                                                    </div>
                                                    <a href="#" class="badge_active2"><div id="verified_by"></div></a>
                                                </div>
                                            @else
                                                <div class="col-lg-12 mt-4">
                                                    <label class="form-label">Status Veifikasi Sektor</label><br>
                                                    <a href="#" class="badge_active2"><div id="verified_by"></div></a>
                                                </div>
                                            @endif
                                            <div class="col-lg-12 mt-3">
                                                <div class="white_card card_height_100 mb_30" style="background-color: rgb(120 225 145 / 80%)">
                                                    <div class="white_card_header">
                                                        <div class="main-title">
                                                            <h3 class="m-0">Sub Kegiatan Hasil Pemetaan SDGS</h3>
                                                        </div>
                                                    </div>
                                                    <div class="white_card_body ">
                                                        <div class="card_container p-0">
                                                            <div class="QA_section">
                                                                <div class="QA_table mb_30">
                                                                    <table class="table" id="table_renja">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Perangkat Daerah</th>
                                                                                <th>Program</th>
                                                                                <th>Kegiatan</th>
                                                                                <th>SubKegiatan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        var table_renja;
        var tahun = "<?= session()->get('tahun_sdgs'); ?>"
        
        $( document ).ready(function() {
            loadpd()
            //loadsatuan()
            $('.nice_Select').niceSelect()
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.table_monevsdgs').DataTable({            
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                "lengthChange": true,
                ajax: {
                    url:"{{ url('/api/get/monevsdgsindkota') }}",
                    data: function(d){
                        d.id_skpd = {{$id_pd}},
                        d.tahun = tahun
                    },
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {data: 'target_nasional', name: 'target_nasional'},
                    {data: 'nama_indikator_kota', name: 'nama_indikator_kota'},
                    {data: 'target', name: 'target', render : function (data, type, row, meta){
                        return data+' '+row.satuan
                    }},
                    {data: 'realisasi_target_1', name: 'realisasi_target_1'},
                    {data: 'realisasi_target_2', name: 'realisasi_target_2'},
                    {data: 'sumber_data', name: 'sumber_data'},
                    {data: 'status_verifikasi', name: 'status_verifikasi',width:"20%", className: "text-center"},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width:"20%", className: "text-center"},

                ]
            });

            table_renja = $('#table_renja').DataTable( {
                processing: true,
                ajax: {
                    url:"{{ url('/api/get/sdgscoding') }}",
                    data: function(d){
                        d.id_target_realisasi = ($('#id_target_realisasi_kota').val()) ? $('#id_target_realisasi_kota').val() : -1,
                        d.tahun = tahun 
                    },
                },
                columns: [ 
                    { "data": "nama_skpd" },
                    { "data": "nama_program" },
                    { "data": "nama_kegiatan" },
                    { "data": "nama_sub_kegiatan" },
                ],
                scrollX: true,
                rowsGroup: [
                    0
                ],
            });

            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                //$(this).html('Sending..');
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })
                
                var url = "{{ route('monevsdgsindkota.store') }}"
                
                $.ajax({
                    processData: false,
                    contentType: false,
                    data: new FormData(document.getElementById('monevSdgsindkotaForm')),
                    url: url,
                    type: "POST",
                    success: function (data) {
                        $('#addsdgsindKota').modal('hide');
                        table.draw();
                        Swal.fire({
                            title: 'Berhasil!',
                            html: data.success,
                            type: 'success'
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        //$('#saveBtn').html('Save Changes');
                        Swal.fire({
                            title: 'Gagal!!',
                            html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                            type: 'danger'
                        });
                    }
                });
            });

            $('body').on('click', '.monevIndkotaSdgs', function () {
                var kota_id = $(this).data("id");
                openModal(kota_id)
            });

            $('#lampiran_name').click(function (e) {
                e.preventDefault();
                var id = this.value
                var src_dok = "{{ url('/sdgs/download/lampiranmonev?file=') }}";
                window.open(src_dok+this.value);
            });

            $('#drop_lampiran').click(function (e) {
                e.preventDefault();
                $('#lampiran_name').val('')
                $('#lampiran_emptied').val(1)
                $('#lampiran_file').addClass('d-none')
            });
        });

        function openModal(a){
            $('#id_target_realisasi_kota').val('')
            $('#monevSdgsindkotaForm').trigger("reset");
            if(a == 'add'){
            }else{
                $('#exampleModalLongTitle').text('Input Monev Indikator Kota')
                $.ajax({
                    type: "get",
                    url: "/api/show/sdgsindkota/"+a,
                    data: {
                        _token: token,
                        tahun: tahun
                    },
                    success: function (response) {
                        //loadtujuan(response.data[0].id_sdgs,response.data[0].id_target_global,response.data[0].id_target_nasional)
                        $('#id_sdgs').val(response.data[0].nama_tujuan)
                        $('#id_target_global').val(response.data[0].target_global)
                        $('#id_target_nasional').val(response.data[0].target_nasional)
                        $('#id_target_realisasi_kota').val(response.data[0].id_target_realisasi_kota)
                        $('#nama_indikator_kota').text(response.data[0].nama_indikator_kota)
                        $('#sumber_data').val(response.data[0].sumber_data)
                        $('#target').text(response.data[0].target)
                        $('#realisasi_target_1').val(response.data[0].realisasi_target_1)
                        $('#capaian_target_1').val(!isNaN(response.data[0].capaian_target_1) ? (response.data[0].capaian_target_1 ? parseFloat(response.data[0].capaian_target_1).toFixed(2) : 0) : 0)
                        $('#realisasi_anggaran_1').val((response.data[0].realisasi_anggaran_1 ? parseFloat(response.data[0].realisasi_anggaran_1).toFixed(0) : 0))
                        $('#capaian_anggaran_1').val(!isNaN(response.data[0].capaian_anggaran_1) ? (response.data[0].capaian_anggaran_1 ? parseFloat(response.data[0].capaian_anggaran_1).toFixed(2) : 0) : 0)
                        $('#realisasi_target_2').val(response.data[0].realisasi_target_2)
                        $('#capaian_target_2').val(!isNaN(response.data[0].capaian_target_2) ? (response.data[0].capaian_target_2 ? parseFloat(response.data[0].capaian_target_2).toFixed(2) : 0) : 0)
                        $('#realisasi_anggaran_2').val((response.data[0].realisasi_anggaran_2 ? parseFloat(response.data[0].realisasi_anggaran_2).toFixed(0) : 0))
                        $('#capaian_anggaran_2').val(!isNaN(response.data[0].capaian_anggaran_2) ? (response.data[0].capaian_anggaran_2 ? parseFloat(response.data[0].capaian_anggaran_2).toFixed(2) : 0) : 0)
                        $('#catatan').val(response.data[0].catatan)
                        $('#satuan').text(response.data[0].satuan)
                        $('#anggaran').text(((response.data[0].anggaran ? parseFloat(response.data[0].anggaran).toFixed(0) : 0)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                        
                        if(response.data[0].lampiran){
                            $('#lampiran_name').val(response.data[0].lampiran)
                            $('#lampiran_file').removeClass('d-none')
                        }
                        
                        $('#verified').prop('checked', false);
                        var id_role = <?=session()->get('id_role')?>;
                        if(response.data[0].verified_by != null && id_role != 13){
                            $('#verified_by').html('telah diverifikasi oleh '+response.data[0].nama_jafung);
                            $('#realisasi').prop( "disabled", true );
                            $('#sumber_data').prop( "disabled", true );
                            $('#lampiran').prop( "disabled", true );
                            $('#saveBtn').addClass( "d-none");
                        }else{
                            $('#verified_by').html('belum di verifikasi');
                            $('#realisasi').prop( "disabled", false );
                            $('#sumber_data').prop( "disabled", false );
                            $('#lampiran').prop( "disabled", false );
                            $('#saveBtn').removeClass( "d-none");
                            $('#verified_by').html('belum di verifikasi');
                        }

                        if(response.data[0].verified_by != null && id_role == 13){
                            $('#verified_by').html('telah diverifikasi oleh '+response.data[0].nama_jafung);
                            $('#verified').prop('checked', true);
                        }
                        table_renja.ajax.reload(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);           
                    }
                });
            }
            $('#addsdgsindKota').modal('show')
        }

        function loadtujuan(id=0,id_g=0,id_n=0){
            $.ajax({
                type: 'GET',
                url: "/api/showall/sdgstujuan",
                success: function(response) {
                    $('#id_sdgs').empty();
                    $('#id_sdgs').append(
                        $('<option></option>').val('').html('Pilih Tujuan Global...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_sdgs').append(
                            $('<option></option>').val(value['id_sdgs']).html(value['nama_tujuan'])
                        );
                    });
                    if(id!=0){
                        $('#id_sdgs').val(id);
                    }
                    $('#id_sdgs').niceSelect('update');
                    loadglobal(id,id_g,id_n)
                }
            });
        }

        function loadglobal(id_t,id=0,id_n=0){
            $.ajax({
                type: 'GET',
                url: "/api/showbytujuan/sdgsindglobal/"+id_t,
                success: function(response) {
                    $('#id_target_global').empty();
                    $('#id_target_global').append(
                        $('<option></option>').val('').html('Pilih Indikator Global...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_target_global').append(
                            $('<option></option>').val(value['id_target_global']).html(value['target_global'])
                        );
                    });
                    if(id!=0){
                        $('#id_target_global').val(id);
                    }
                    $('#id_target_global').niceSelect('update');
                    loadnasional(id,id_n)
                }
            });
        }

        function loadnasional(id_g,id=0){
            $.ajax({
                type: 'GET',
                url: "/api/showbyindglobal/sdgsindnasional/"+id_g,
                success: function(response) {
                    $('#id_target_nasional').empty();
                    $('#id_target_nasional').append(
                        $('<option></option>').val('').html('Pilih Indikator Nasional...')
                    );
                    $.each(response.data, function(key, value) {
                        $('#id_target_nasional').append(
                            $('<option></option>').val(value['id_target_nasional']).html(value['target_nasional'])
                        );
                    });
                    if(id!=0){
                        $('#id_target_nasional').val(id);
                    }
                    $('#id_target_nasional').niceSelect('update');
                }
            });
        }

        function loadsatuan(val=""){
            $.ajax({
                type: 'GET',
                url: "{{ url('/api/renja/satuan') }}",
                success: function(response) {
                    $('#satuan').empty();
                    $('#satuan').append(
                        $('<option></option>').val('').html('Pilih Satuan...')
                        );
                    $.each(response.data, function(key, value) {
                        $('#satuan').append(
                            $('<option></option>').val(value['satuan']).html(value['satuan'])
                            );
                    });
                    if(val){
                        var d = val.replace(/"/g, '')
                        $('#satuan').val(String(d))
                    }
                    $('#satuan').niceSelect('update');
                }
            });
        }

        function loadpd(id=0){
            $.ajax({
                    type: 'GET',
                    url: "{{ url('/api/renja/perangkatdaerah') }}",
                    data:{
                        id_pd:id
                    },
                    success: function(response) {
                        $('#perangkat_daerah').empty();
                        $('#perangkat_daerah').append(
                            $('<option></option>').val('').html('Pilih Perangkat Daerah...')
                            );
                        $.each(response.data, function(key, value) {
                            $('#perangkat_daerah').append(
                                $('<option></option>').val(value['id_skpd']).html(value['nama_skpd'])
                                );
                        });
                        if(id != 0){
                            $('#perangkat_daerah').val(id);
                        }
                        $('#perangkat_daerah').niceSelect('update');
                    }
                });
        }

        function renderPersentarget(sem){
            var val = 0
            if(!isNaN($('#target').text())){
                val = (($('#realisasi_target_'+sem).val()/$('#target').text())*100).toFixed(2)
            }else{
                val = 'NaN'
            }
            $('#capaian_target_'+sem).val(val)
        }

        function renderPersenanggaran(sem){
            var val = 0
            if($('#anggaran').text() != '' && $('#anggaran').text() != 0){
                val = (($('#realisasi_anggaran_'+sem).val()/($('#anggaran').text()).replace(/\,/g,''))*100).toFixed(2)
            }else{
                val = 'NaN'
            }
            $('#capaian_anggaran_'+sem).val(val)
        }
    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
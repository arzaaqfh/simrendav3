<!-- sidebar  -->
<nav class="sidebar">
    <div class="logo d-flex justify-content-between bg-primary">
            <a class="large_logo" href="{{ url('home') }}">
                <img src="{{ asset('storage/img/simrenda-full.png') }}" alt="" height="34px">
                <h5 class="text-light"><b>Sustainable Development Goals</b></h5>
            </a>

        <a class="small_logo" href="{{ url('home') }}">
            <img src="{{ asset('storage/img/simrenda-logo.png') }}" alt="" height="34px">
            <h5 class="text-light"><b>SDGS</b></h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <h4 class="menu-text"><span>MENU DASHBOARD</span> <i class="fas fa-ellipsis-h"></i> </h4>
        <li>
            <a href="{{ url('home') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <i class="fa fa-home" ></i>
                </div>
                <div class="nav_title">
                    <span>Home</span>
                </div>
            </a>
        </li>
        
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/5.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Dashboard </span>
                </div>
            </a>
            <ul>
                {{-- <li>
                    <a href="{{ url('/sdgs/tujuansdgs') }}">Analisis</a>
                </li> --}}
                <li>
                    <a href="{{ url('/sdgs/dashboard/rekap') }}">Rekapan</a>
                </li>
                <li>
                    @if (Auth::user()->id_role == 1)
                        <form method="POST" action="{{ url('sdgs/cetak') }}">
                            @csrf
                            <input type="hidden" name="tahun" value="2023"/>
                            <input type="hidden" name="id_skpd" value=""/>
                            <input type="hidden" name="all" value="1">
                            <input type="submit" value="Cetak Realisasi Semua PD SDGS">
                        </form>
                    @elseif (Auth::user()->id_role == 13)
                    
                    @else  
                        <form method="POST" action="{{ url('sdgs/cetak') }}">
                            @csrf
                            <input type="hidden" name="tahun" value="2023"/>
                            <input type="hidden" name="id_skpd" value="{{ Session::get('id_skpd')[0] }}"/>
                            <input type="hidden" name="all" value="0">
                            <input type="submit" value="Cetak Realisasi SDGS">
                        </form>
                    @endif
                </li>
            </ul>
        </li>
        
        @if (session()->get('id_role') != 4)
        <li class="">
            <a class="has-arrow" href="#" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/5.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Indikator SDGS </span>
                </div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('/sdgs/tujuansdgs') }}">Tujuan Global</a>
                </li>
                <li>
                    <a href="{{ url('/sdgs/indikatorglobalsdgs') }}">Target Nasional</a>
                </li>
                <li>
                    <a href="{{ url('/sdgs/indikatornasionalsdgs') }}">Indikator Nasional</a>
                </li>
                <li>
                    <a href="{{ url('/sdgs/indikatorkotasdgs') }}">Indikator Kota</a>
                </li>
            </ul>
        </li>
        @endif
        <li class="">
            <a href="{{ url('/sdgs/codingsdgs') }}" aria-expanded="false">
                <div class="nav_icon_small">
                    <img src="{{ asset('storage/img/menu-icon/2.svg') }}" alt="">
                </div>
                <div class="nav_title">
                    <span>Pemetaan SDGS </span>
                </div>
            </a>
        </li>
        <li class="">
            <a  class="has-arrow" href="#" aria-expanded="false">
              <div class="nav_icon_small">
                  <img src="{{ asset('storage/img/menu-icon/4.svg') }}" alt="">
              </div>
              <div class="nav_title">
                  <span>Monev SDGS</span>
              </div>
            </a>
            <ul>
              <li>
                  <a href="{{ url('/sdgs/monevsdgs') }}">Indikator Kota</a>
              </li>
            </ul>
        </li>

      </ul>
</nav>
 <!--/ sidebar  -->
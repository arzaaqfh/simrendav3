@extends('layouts.sdgs')

@section('content')

    <!-- Side Bar -->
    @include('sdgs.sidebar')

    <!-- Top Bar -->
    @include('layouts.topbar')

    <!-- Begin Page Content -->
    <div class="main_content_iner ">
        <div class="container-fluid p-0">
            <!-- page title  -->
            <div class="row">
                <div class="col-12">
                    <div class="page_title_box d-flex flex-wrap align-items-center justify-content-between">
                        <div class="page_title_left">
                            <h3 class="f_s_25 f_w_700 dark_text" >Indikator SDGS</h3>
                            <ol class="breadcrumb page_bradcam mb-0">
                                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/spm') }}">SDGS</a></li>
                                <li class="breadcrumb-item active">Tujuan Global</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="white_card card_height_100 mb_30 px-2">
                        <div class="white_card_header">
                            <div class="box_header m-0">
                                <div class="main-title">
                                    <h3 class="m-0">Indikator SDGS</h3>
                                </div>
                            </div>
                        </div>
                        <!-- <div id="message" style="display: none">
                            <div class="alert text-white bg-success d-flex align-items-center justify-content-between" role="alert">
                                <div class="alert-text" id="textmessage"></div>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div> -->

                        <div class="white_card_body">
                            <div class="QA_section">
                                <div class="white_box_tittle list_header">
                                    <h4>Data Tujuan Global</h4>
                                    <div class="box_right d-flex lms_block">
                                        <div class="serach_field_2">
                                            <div class="search_inner">
                                                <form Active="#">
                                                    <div class="search_field">
                                                        <input id="dtsearch" type="text" onkeydown="return (event.keyCode!=13);" placeholder="Search content here...">
                                                    </div>
                                                    <button type="button"> <i class="ti-search"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="add_button ms-2">
                                            <button class="btn_1" onclick="openModal('add')">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="QA_table mb_30">
                                    <table class="table lms_table_active data-table">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th>Nama Tujuan</th>
                                                <th>Urutan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-xl" id="addsdgsTujuan" tabindex="-1" role="dialog" aria-labelledby="addsdgsTujuanTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-6">
                        <h5class="modal-title" id="exampleModalLongTitle"></h5>
                    </div>
                    <div class="col-md-5">
                        <div class="float-right">
                            <button type="button" class="btn btn-primary" id="saveBtn">Simpan</button>
                            <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Batal</button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                        <form id="sdgstujuanForm" action="">
                            <input type="hidden" name="id_sdgs" id="id_sdgs">
                            <div class="mb-3">
                                <label class="form-label">Nama Tujuan</label>
                                <textarea class="form-control" rows="3" name="nama_tujuan" id="nama_tujuan" placeholder="" required></textarea>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Urutan</label>
                                <input type="number" min="1" class="form-control" id="urutan" name="urutan" placeholder="" required>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                "lengthChange": false,
                ajax: "{{ route('sdgstujuan.index') }}",
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {data: 'nama_tujuan', name: 'nama_tujuan'},
                    {data: 'urutan', name: 'urutan'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, width:"20%", className: "text-center"},

                ]
            });
            $('#dtsearch').keyup(function(){
                table.search($(this).val()).draw() ;
            })

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                //$(this).html('Sending..');
                Swal.fire({
                    html: 'menyimpan data ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                })

                console.log($('#id_sdgs').val())
                if($('#id_sdgs').val() == ''){
                    var url = "{{ route('sdgstujuan.store') }}"
                }else{
                    var url = "/api/update/sdgstujuan/"+$('#id_sdgs').val()
                }

                $.ajax({
                    data: $('#sdgstujuanForm').serialize(),
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#addsdgsTujuan').modal('hide');
                        //$('#textmessage').html(data.success);
                        //$('#message').show();
                        table.draw();
                        Swal.fire({
                            title: 'Berhasil!',
                            html: data.success,
                            type: 'success'
                        });
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        //$('#saveBtn').html('Save Changes');
                        Swal.fire({
                            title: 'Gagal!!',
                            html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                            type: 'danger'
                        });
                    }
                });
            });

            $('body').on('click', '.deleteTujuanSdgs', function () {
                var sdgs_id = $(this).data("id");
                //confirm("Apakah yakin akan menghapus data '"+$(this).data("id")+"'?");
                Swal.fire({
                    title: 'Apakah yakin akan menghapus data '+sdgs_id+'?',
                    icon: 'question',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    denyButtonText: 'Tidak',
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.value) {
                        Swal.fire({
                            html: 'menghapus data ...',
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            }
                        })
                        $.ajax({
                            type: "post",
                            url: "/api/delete/sdgstujuan",
                            data: {
                                id: sdgs_id,
                                _token: token,
                            },
                            success: function (data) {
                                table.draw();
                                Swal.fire({
                                    title: 'Berhasil!',
                                    html: data.success,
                                    type: 'success'
                                });
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                Swal.fire({
                                    title: 'Gagal!!',
                                    html: 'Oops! Terjadi kesalahan ketika menyimpan data [kode: 500]',
                                    type: 'danger'
                                });
                            }
                        });
                    } else if (result.isDenied) {
                        //Swal.fire('', '', 'info')
                    }
                })
            });

            $('body').on('click', '.editTujuanSdgs', function () {
                var sdgs_id = $(this).data("id");
                openModal(sdgs_id)
            });
        });

        function openModal(a){
            $('#id_sdgs').val('')
            $('#sdgstujuanForm').trigger("reset");
            if(a == 'add'){
                $('#exampleModalLongTitle').text('Tambah Data Tujuan Global')
            }else{
                $('#exampleModalLongTitle').text('Edit Data Tujuan Global')
                $.ajax({
                    type: "get",
                    url: "/api/show/sdgstujuan/"+a,
                    data: {
                        _token: token,
                    },
                    success: function (response) {
                        console.log(response)
                        $('#id_sdgs').val(response.data[0].id_sdgs)
                        $('#nama_tujuan').val(response.data[0].nama_tujuan)
                        $('#urutan').val(response.data[0].urutan)
                    },
                    error: function (data) {
                        console.log('Error:', data);           
                    }
                });
            }
            $('#addsdgsTujuan').modal('show')
        }

    </script>

    <!-- Footer -->
    @include('layouts.footer')
@endsection
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
</head>

<body>

    <table id="rpd">
        <thead>
            <tr>
                <th>Uraian</th>
                <th>Pagu</th>
                <th>Indikator</th>
                <th>Target</th>
                <th>Program</th>
                <th>Kegiatan</th>
                <th>Pagu</th>
                <th>Indikator</th>
                <th>Target</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" ></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js" ></script>
    
 

    <script>
        var arraykeg = []
        var arraysub = []
        var arrayprog = []

        $(document).ready(function() {
            console.log("ready!");
            $.ajax({
                type: 'GET',
                url: "{{ url('/api/getrender/renjaprog') }}",
                success: function(response) {
                    arrayprog = response.data;
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/api/getrender/renjakeg') }}",
                        success: function(response) {
                            arraykeg = response.data;
                            $.ajax({
                                type: 'GET',
                                url: "{{ url('/api/getrender/renjasub') }}",
                                success: function(response) {
                                    arraysub = response.data;
                                    render()
                                }
                            });
                        }
                    });
                }
            });

            $('#rpd').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        });

        function render() {
            $.ajax({
                type: 'GET',
                url: "{{ url('/api/getrender/rpd') }}",
                success: function(response) {
                    var stateurusan = '',
                        statebidang = '',
                        stateprogram = '',
                        statekegiatan = '',
                        statesub = '',
                        stateind = '',
                        data = ''
                    iterati = 0
                    $.each(response.data, function(index, value) {
                        iterati++
                        if (stateurusan == value.urusan) {
                            if (statebidang == value.nama_urusan) {
                                if (stateprogram == value.nama_program) {
                                    data = ''

                                    insert_row(data, value.indikator_program, value.target, value.satuan)
                                } else {
                                    var map = arraykeg.map(function(x, i) {
                                        if (x.nama_program == stateprogram) {
                                            if (statekegiatan != x.nama_kegiatan) {
                                                statekegiatan = x.nama_kegiatan
                                                insert_row2(x.nama_kegiatan, null, 'keg', null, null, x.uang)
                                                var mapsub = arraysub.map(function(x, i) {
                                                    if (x.nama_kegiatan == statekegiatan) {
                                                        if (statesub != x.nama_sub_kegiatan) {
                                                            statesub = x.nama_sub_kegiatan
                                                            insert_row2(x.nama_sub_kegiatan, x.indikator_subkegiatan, 'sub', x.target, x.satuan, x.uang)
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                    data = value.nama_program
                                    stateprogram = value.nama_program
                                    var nameprog = ''
                                    var uangprog = 0
                                    var mapprog = arrayprog.map(function(x, i) {
                                        if (x.nama_program == stateprogram) {
                                            nameprog = x.nama_program
                                            uangprog = x.uang
                                        }
                                    })
                                    insert_row(data, value.indikator_program, value.target, value.satuan, nameprog, value.uang, uangprog)
                                    if (iterati == response.data.length) {
                                        var map = arraykeg.map(function(x, i) {
                                            if (x.nama_program == stateprogram) {
                                                if (statekegiatan != x.nama_kegiatan) {
                                                    statekegiatan = x.nama_kegiatan
                                                    insert_row2(x.nama_kegiatan, null, 'keg', null, null, x.uang)
                                                    var mapsub = arraysub.map(function(x, i) {
                                                        if (x.nama_kegiatan == statekegiatan) {
                                                            if (statesub != x.nama_sub_kegiatan) {
                                                                statesub = x.nama_sub_kegiatan
                                                                insert_row2(x.nama_sub_kegiatan, x.indikator_subkegiatan, 'sub', x.target, x.satuan, x.uang)
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            } else {
                                data = value.nama_urusan
                                statebidang = value.nama_urusan
                                insert_row(data)
                                if (stateprogram == value.nama_program) {
                                    console.log('ab')
                                } else {
                                    data = value.nama_program
                                    stateprogram = value.nama_program
                                    var nameprog = ''
                                    var uangprog = 0
                                    var mapprog = arrayprog.map(function(x, i) {
                                        if (x.nama_program == stateprogram) {
                                            nameprog = x.nama_program
                                            uangprog = x.uang
                                        }
                                    })
                                    insert_row(data, value.indikator_program, value.target, value.satuan, nameprog, value.uang, uangprog)
                                }
                            }
                        } else {
                            data = value.urusan
                            stateurusan = value.urusan
                            insert_row(data)
                            if (statebidang == value.nama_urusan) {
                                console.log('au')
                            } else {
                                data = value.nama_urusan
                                statebidang = value.nama_urusan
                                insert_row(data)
                                if (stateprogram == value.nama_program) {
                                    console.log('aa')
                                } else {
                                    data = value.nama_program
                                    stateprogram = value.nama_program
                                    var nameprog = ''
                                    var uangprog = 0
                                    var mapprog = arrayprog.map(function(x, i) {
                                        if (x.nama_program == stateprogram) {
                                            nameprog = x.nama_program
                                            uangprog = x.uang
                                        }
                                    })
                                    insert_row(data, value.indikator_program, value.target, value.satuan, nameprog, value.uang, uangprog)
                                }
                            }
                        }
                    });
                }
            });
        }

        function insert_row(data, ind = '', target = '', satuan = '', nameprog = '', uangp = '', uangrkp = '') {
            var table = document.getElementById("rpd");
            var row = table.insertRow();
            var rowlength = table.rows.length;

            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            var cell7 = row.insertCell(6);
            var cell8 = row.insertCell(7);
            var cell9 = row.insertCell(8);
            var cell10 = row.insertCell(9);

            cell1.innerHTML = '<b>' + data + '</b>';
            cell2.innerHTML = uangp;
            cell3.innerHTML = ind;
            cell4.innerHTML = target + ' ' + satuan;
            cell5.innerHTML = nameprog;
            cell6.innerHTML = '';
            cell7.innerHTML = uangrkp;
            cell8.innerHTML = '';
            cell9.innerHTML = '';
        }

        function insert_row2(data, ind = '', el = '', target = '', satuan = '', uang = '') {
            var table = document.getElementById("rpd");
            var row = table.insertRow();
            var rowlength = table.rows.length;

            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            var cell7 = row.insertCell(6);
            var cell8 = row.insertCell(7);
            var cell9 = row.insertCell(8);
            var cell10 = row.insertCell(9);

            cell1.innerHTML = '';
            cell2.innerHTML = '';
            cell3.innerHTML = '';
            cell4.innerHTML = '';
            cell5.innerHTML = '';
            if (el == 'keg') {
                cell6.innerHTML = '<b>' + data + '</b>';
            } else {
                cell6.innerHTML = data;
            }
            cell7.innerHTML = uang;
            cell8.innerHTML = ind;
            cell9.innerHTML = target + ' ' + satuan;
            cell10.innerHTML = '';

        }
    </script>
</body>


</html>
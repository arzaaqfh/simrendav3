<?php

use App\Http\Controllers\Tahun_Controller;

use App\Http\Controllers\Api\API_MonevRENJAController;
use App\Http\Controllers\Api\API_RENSTRAController;
use App\Http\Controllers\Api\API_RENJAController;
use App\Http\Controllers\Api\API_RKPDController;
use App\Http\Controllers\Api\API_SPMController;
use App\Http\Controllers\Api\AspekController;
use App\Http\Controllers\Api\FaktorPendorongIKPController;
use App\Http\Controllers\Api\FaktorPendorongSasaranIKUController;
use App\Http\Controllers\Api\FaktorPendorongTujuanIKUController;
use App\Http\Controllers\Api\FaktorPenghambatIKPController;
use App\Http\Controllers\Api\FaktorPenghambatSasaranIKUController;
use App\Http\Controllers\Api\FaktorPenghambatTujuanIKUController;
use App\Http\Controllers\Api\IndikatorKinerjaRPJMDController;
use App\Http\Controllers\Api\IndikatorProgramRPJMDController;
use App\Http\Controllers\Api\IndikatorSasaranRPJMDController;
use App\Http\Controllers\Api\IndikatorTujuanRPJMDController;
use App\Http\Controllers\Api\KategoriRPJMDController;
use App\Http\Controllers\Api\KetercapaianRPJMDController;
use App\Http\Controllers\Api\MisiRPJMDController;
use App\Http\Controllers\Api\PeriodeController;
use App\Http\Controllers\Api\ProgramRPJMDController;
use App\Http\Controllers\Api\RPJMDController;
use App\Http\Controllers\Api\RumusController;
use App\Http\Controllers\Api\SasaranRPJMDController;
use App\Http\Controllers\Api\SatuanRPJMDController;
use App\Http\Controllers\Api\SKPD_90Controller;
use App\Http\Controllers\Api\StrategiRPJMDController;
use App\Http\Controllers\Api\SumberDataController;
use App\Http\Controllers\Api\TahunController;
use App\Http\Controllers\Api\TargetRealisasiIKKController;
use App\Http\Controllers\Api\TargetRealisasiIKPController;
use App\Http\Controllers\Api\TargetRealisasiProgramV2Controller;
use App\Http\Controllers\Api\TargetRealisasiSasaranIKUController;
use App\Http\Controllers\Api\TargetRealisasiTujuanIKUController;
use App\Http\Controllers\Api\TindakLanjutIKPController;
use App\Http\Controllers\Api\TindakLanjutSasaranIKUController;
use App\Http\Controllers\Api\TindakLanjutTujuanIKUController;
use App\Http\Controllers\Api\TujuanRPJMDController;
use App\Http\Controllers\Api\UrusanRPJMDController;
use App\Http\Controllers\Api\BidangUrusan_90Controller;
use App\Http\Controllers\Api\Kegiatan_90Controller;
use App\Http\Controllers\Api\SdgsTujuanController;
use App\Http\Controllers\Api\SdgsIndikatorGlobalController;
use App\Http\Controllers\Api\SdgsIndikatorNasionalController;
use App\Http\Controllers\Api\SdgsIndikatorKotaController;
use App\Http\Controllers\Api\MonevSdgsIndikatorKotaController;
use App\Http\Controllers\Api\SdgsCodingController;
use App\Http\Controllers\Api\SdgsDashboardController;
use App\Http\Controllers\Api\Program_90Controller;
use App\Http\Controllers\Api\SPM_AnggaranController;
use App\Http\Controllers\Api\SPM_CapaianController;
use App\Http\Controllers\Api\SPM_Dasar_HukumController;
use App\Http\Controllers\Api\SPM_Indikator_PencapaianController;
use App\Http\Controllers\Api\SPM_Jenis_LayananController;
use App\Http\Controllers\Api\SPM_Landasan_HukumController;
use App\Http\Controllers\Api\SPM_PelayananController;
use App\Http\Controllers\Api\SPM_R_jenisRincianController;
use App\Http\Controllers\Api\SPM_Rincian_Jenis_LayananController;
use App\Http\Controllers\Api\SPM_UrusanController;
use App\Http\Controllers\Api\Urusan_90Controller;
use App\Http\Controllers\Api\VerifikasiIndikatorKegiatan_90Controller;
use App\Http\Controllers\Api\VerifikasiIndikatorSubKegiatan_90Controller;
use App\Http\Controllers\Api\MonevRKPDHasilController;
use App\Http\Controllers\Api\Api_GrafikMonevController;
use App\Http\Controllers\Api\SettingInputController;
use App\Http\Controllers\Api\Jafung_90Controller;
use App\Http\Controllers\Api\RenderController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\RolesController;
use App\Http\Controllers\Api\AnalisisFaktorProgramController;
use App\Http\Controllers\Api\AnalisisFaktorKegiatanController;
use App\Http\Controllers\Api\AnalisisFaktorSubKegiatanController;
use App\Http\Controllers\Api\AnalisatorController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RenjaLamaController;
use App\Http\Controllers\Api\MisiController;
use App\Http\Controllers\Api\TujuanController;
use App\Http\Controllers\Api\IndikatorTujuanController;
use App\Http\Controllers\Api\TargetRealisasiTujuanController;
use App\Http\Controllers\Api\SasaranController;
use App\Http\Controllers\Api\IndikatorSasaranController;
use App\Http\Controllers\Api\TargetRealisasiSasaranController;
use App\Http\Controllers\Api\QueryController;
use App\Http\Controllers\Api\PerencanaanController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Otentikasi Api
Route::get('/auth/login',[AuthController::class,'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/auth/logout', [AuthController::class, 'logout']);

    /**
     * Perencanaan
     */
    Route::post('/perencanaan/get_sasaran',[PerencanaanController::class,'getSasaran']);
    Route::post('/perencanaan/get_tujuan',[PerencanaanController::class,'getTujuan']);
    Route::post('/perencanaan/get_program',[PerencanaanController::class,'getProgram']);
    Route::post('/perencanaan/get_kegiatan',[PerencanaanController::class,'getKegiatan']);
    Route::post('/perencanaan/get_subkegiatan',[PerencanaanController::class,'getSubKegiatanPohonKinerja']);
    Route::post('/perencanaan/get_ind_tujuan',[PerencanaanController::class,'getIndikatorTujuan']);
    Route::post('/perencanaan/get_ind_sasaran',[PerencanaanController::class,'getIndikatorSasaran']);
    Route::post('/perencanaan/get_ind_program',[PerencanaanController::class,'getIndikatorProgram']);
    Route::post('/perencanaan/get_sub_kegiatan',[PerencanaanController::class,'getSubKegiatan']);
    Route::post('/perencanaan/save_tagging_data',[PerencanaanController::class,'saveTaggingData']);
    Route::post('/perencanaan/save_pohon_kinerja',[PerencanaanController::class,'savePohonKinerja']);
    Route::get('/perencanaan/get_tagging_list',[PerencanaanController::class,'taggingList']);

    /**
     * Query
     */
    Route::get('/query/anggaran/program/all',[QueryController::class, 'getAnggaranProgram']);
    Route::get('/query/anggaran/kegiatan/all',[QueryController::class, 'getAnggaranKegiatan']);
    Route::get('/query/anggaran/subkegiatan/all',[QueryController::class, 'getAnggaranSubKegiatan']);
    Route::post('/query/anggaran/program/showBy',[QueryController::class, 'showByAnggaranProgram']);
    Route::post('/query/anggaran/kegiatan/showBy',[QueryController::class, 'showByAnggaranKegiatan']);
    Route::post('/query/anggaran/subkegiatan/showBy',[QueryController::class, 'showByAnggaranSubKegiatan']);
    Route::post('/query/fisik/program/showBy',[QueryController::class, 'showByFisikProgram']);

    /*NEW API*/
    Route::post('/renstra/get_cetak',[API_RENSTRAController::class,'getCetak']);
    Route::post('/renstra/get_from_python',[API_RENSTRAController::class,'get_from_python']);

    Route::get('/master/perangkatdaerah/get_data', [API_RENJAController::class, 'getdata']);

    Route::get('/renja/get_subkeg',[API_RENJAController::class,'getSubKegiatan']);
    Route::get('/renja/get_keg',[API_RENJAController::class,'getKegiatan']);
    Route::get('/renja/get_prog',[API_RENJAController::class,'getProgram']);

    Route::get('/renja/get_inputrealindsubkeg',[API_RENJAController::class,'getIndikatorSubKegiatan']);
    Route::get('/renja/get_inputrealindkeg',[API_RENJAController::class,'getIndikatorKegiatan']);
    Route::get('/renja/get_inputrealindprog',[API_RENJAController::class,'getIndikatorProgram']);
    Route::get('/renja/get_analisissubkegiatan',[API_RENJAController::class,'getAnalisisSubKegiatan']);
    Route::get('/renja/get_analisisfaktor',[API_RENJAController::class,'getAnalisisFaktor']);
    Route::get('/renja/get_editindsubkeg',[API_RENJAController::class,'getEditIndikatorSubKegiatan']);
    Route::get('/renja/get_editindkeg',[API_RENJAController::class,'getEditIndikatorKegiatan']);
    Route::get('/renja/get_cetakrkpd',[API_RENJAController::class,'getCetakRKPD']);
    
    Route::post('/renja/post_realindsubkeg', [API_RENJAController::class, 'saveinputrealindsubkeg']);
    Route::post('/renja/post_realindkeg', [API_RENJAController::class, 'saveinputrealindkeg']);
    Route::post('/renja/post_realindprog', [API_RENJAController::class, 'saveinputrealindprog']);
    Route::post('/renja/post_analisissubkeg', [API_RENJAController::class, 'saveinputanalisissubkeg']);
    Route::post('/renja/post_analisisfaktor', [API_RENJAController::class, 'saveinputakbidangurusan']);
    Route::post('/renja/post_addindikatorsubkeg', [API_RENJAController::class, 'saveaddindsubkegiatan']);
    Route::post('/renja/post_addindikatorkeg', [API_RENJAController::class, 'saveaddindkegiatan']);
    Route::post('/renja/post_delindikatorkeg', [API_RENJAController::class, 'deleteindkegiatan']);
    Route::post('/renja/post_delindikatorsubkeg', [API_RENJAController::class, 'deleteindsubkegiatan']);
    
    Route::post('/renja/exec_cnv_indikatorprog',[API_RENJAController::class, 'execute_conversi_program']);

    /*
        render
    */
    Route::get('/getrender/rpd',[RenderController::class,'rpd']);
    Route::get('/getrender/renjakeg',[RenderController::class,'renjakeg']);
    Route::get('/getrender/renjasub',[RenderController::class,'renjasub']);
    Route::get('/getrender/renjaprog',[RenderController::class,'renjaprog']);
    /*
        API Renja
    */
    Route::post('/renja/get_all',[API_RENJAController::class,'getMonevAll']);
    Route::post('/renja/get_realisasi_anggaran',[API_RENJAController::class,'getRealisasiAnggaran']);
    Route::post('/renja/save_realisasi_anggaran',[API_RENJAController::class,'saveRealisasiAnggaran']);
    /*
        Setting Input
    */
    Route::apiResource('/settinginput',SettingInputController::class);
    /*
        Jafung
    */
    Route::get('/get/all/users',[Jafung_90Controller::class,'index']);
    Route::get('/get/all/userjafung',[Jafung_90Controller::class,'getUserJafung']);
    Route::post('/get/show/user',[Jafung_90Controller::class,'show']);
    Route::post('/update/akses/userjafung',[Jafung_90Controller::class,'updateJafung']);
    /*
        Users
    */
    Route::get('/get/all/usersonly',[UsersController::class,'index']);
    Route::post('/save/new/user',[UsersController::class,'create']);
    Route::apiResource('setting-user-simrenda',UsersController::class);

    /*
        Roles
    */
    Route::get('/get/all/roles',[RolesController::class,'index']);
    Route::apiResource('/roles',RolesController::class);

    /*
        Analisis Faktor
    */
    Route::apiResource('/afprogram', AnalisisFaktorProgramController::class);
    Route::apiResource('/afkegiatan', AnalisisFaktorKegiatanController::class);
    Route::apiResource('/afsubkegiatan', AnalisisFaktorSubKegiatanController::class);

    /**
     *  Renja Lama
     */
    Route::apiResource('/renjalama', RenjaLamaController::class);
    
    /**
     *  Target Realisasi Program V2
     */
    Route::apiResource('/TargetRealisasiProgramV2', TargetRealisasiProgramV2Controller::class);

    /**
     *  Analisator
     */
    Route::apiResource('/analisator',AnalisatorController::class);
    Route::get('/all/viewUser',[API_SPMController::class,'viewUser']);

    Route::get('/all/spmAnggaran',[API_SPMController::class,'anggaran']);
    Route::get('/all/spmCapaian',[API_SPMController::class,'capaian']);
    Route::get('/all/spmDasarHukum',[API_SPMController::class,'dasarHukum']);
    Route::get('/all/spmIndikator',[API_SPMController::class,'indikator']);
    Route::get('/all/spmJenisLayanan',[API_SPMController::class,'jenisLayanan']);
    Route::get('/all/spmLandasanHukum',[API_SPMController::class,'landasanHukum']);
    Route::get('/all/spmPelayanan',[API_SPMController::class,'pelayanan']);
    Route::get('/all/spmRincian',[API_SPMController::class,'rincian']);
    Route::get('/all/spmUrusan',[API_SPMController::class,'urusan']);
    Route::get('/all/spmStatus/{id}',[API_SPMController::class,'statusBy']);

    /* Changed To New Api On Bottom 

    */

    Route::get('/renja/kegiatan',[API_RENJAController::class,'getKegiatan']);
    Route::get('/renja/subkegiatan',[API_RENJAController::class,'getSubKegiatan']);
    Route::get('/renja/subkegiatansdgs',[API_RENJAController::class,'getSubKegiatansdgs']);
    Route::get('/renja/indikatorkegiatan',[API_RENJAController::class,'getIndikatorKegiatan']);
    Route::get('/renja/indikatorprogramrpjmd',[API_RENJAController::class,'getIndikatorProgramRPJMD']);

    Route::get('/renja/perangkatdaerah',[API_RENJAController::class,'getPD']);
    Route::get('/renja/satuan',[API_RENJAController::class,'getSatuan']);
    Route::get('/renja/indikatorsubkegiatan',[API_RENJAController::class,'getIndikatorSubKegiatan']);
    Route::get('/renja/relasisubkegiatan',[API_RENJAController::class,'getLogRenjaBySubKegiatan']);


    Route::get('/monev/renja/kegiatan',[API_MonevRENJAController::class,'getMonevKegiatan']);
    Route::get('/monev/renja/subkegiatan',[API_MonevRENJAController::class,'getMonevSubKegiatan']);
    Route::get('/monev/renja/realisasiindikatorkegiatan',[API_MonevRENJAController::class,'getRealisasiIndikatorKegiatan']);
    Route::get('/monev/renja/realisasiindikatorsubkegiatan',[API_MonevRENJAController::class,'getRealisasiIndikatorSubKegiatan']);


    Route::get('/monev/renja/rekapinputkegiatan',[API_MonevRENJAController::class,'getRekapInputKegiatan']);
    Route::get('/monev/renja/rekapinputsubkegiatan',[API_MonevRENJAController::class,'getRekapInputSubKegiatan']);
    Route::get('/monev/renja/rekapinputprogram',[API_MonevRENJAController::class,'getRekapInputProgram']);
    Route::get('/monev/renja/rekapinputall',[API_MonevRENJAController::class,'getRekapAll']);
    Route::get('/monev/renja/rekapinputallv2',[API_MonevRENJAController::class,'getRekapInputAll']);

    Route::get('/all/tahun',[Tahun_Controller::class,'index']);

    /*-------------------------------------
    | GRAFIK MONEV
    ---------------------------------------*/
    Route::get('/grafik/anggaranPD',[Api_GrafikMonevController::class, 'anggaranPerPD']);
    Route::get('/grafik/capaianPD',[Api_GrafikMonevController::class, 'capaianPerPD']);

    /*-------------------------------------
    | RKPD
    ---------------------------------------*/
    Route::get('/all/rkpd/IKU',[API_RKPDController::class,'getIKU']);
    Route::get('/all/rkpd/IKK',[API_RKPDController::class,'getIKK']);
    Route::get('/all/rkpd/IKP',[API_RKPDController::class,'getIKP']);
    Route::get('/all/rkpd/SKPD',[API_RKPDController::class,'getSKPD']);
    Route::get('/monev/rkpd/hasil/IKUTujuan/{tahun}/{pd}',[API_RKPDController::class,'monevHasilIKUTujuan']);
    Route::get('/monev/rkpd/hasil/IKUSasaran/{tahun}/{pd}',[API_RKPDController::class,'monevHasilIKUSasaran']);
    Route::get('/monev/rkpd/hasil/IKK/{tahun}/{pd}',[API_RKPDController::class,'monevHasilIKK']);
    Route::get('/monev/rkpd/hasil/IKP/{tahun}/{pd}',[API_RKPDController::class,'monevHasilIKP']);

    /*-------------------------------------
    | Aspek
    ---------------------------------------*/
    Route::get('/get/Aspek',[AspekController::class,'index']); //ambil semua data
    Route::post('/store/Aspek',[AspekController::class,'store']); //menambahkan data
    Route::get('/show/Aspek/{id}',[AspekController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Aspek/{id}',[AspekController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Aspek/{id}',[AspekController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Pendorong IKP
    ---------------------------------------*/
    Route::get('/get/FaktorPendorongIKP',[FaktorPendorongIKPController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPendorongIKP',[FaktorPendorongIKPController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPendorongIKP/{id}',[FaktorPendorongIKPController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPendorongIKP/{id}',[FaktorPendorongIKPController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPendorongIKP/{id}',[FaktorPendorongIKPController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Pendorong Sasaran IKU
    ---------------------------------------*/
    Route::get('/get/FaktorPendorongSasaranIKU',[FaktorPendorongSasaranIKUController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPendorongSasaranIKU',[FaktorPendorongSasaranIKUController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPendorongSasaranIKU/{id}',[FaktorPendorongSasaranIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPendorongSasaranIKU/{id}',[FaktorPendorongSasaranIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPendorongSasaranIKU/{id}',[FaktorPendorongSasaranIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Pendorong Tujuan IKU
    ---------------------------------------*/
    Route::get('/get/FaktorPendorongTujuanIKU',[FaktorPendorongTujuanIKUController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPendorongTujuanIKU',[FaktorPendorongTujuanIKUController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPendorongTujuanIKU/{id}',[FaktorPendorongTujuanIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPendorongTujuanIKU/{id}',[FaktorPendorongTujuanIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPendorongTujuanIKU/{id}',[FaktorPendorongTujuanIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Penghambat IKP
    ---------------------------------------*/
    Route::get('/get/FaktorPenghambatIKP',[FaktorPenghambatIKPController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPenghambatIKP',[FaktorPenghambatIKPController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPenghambatIKP/{id}',[FaktorPenghambatIKPController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPenghambatIKP/{id}',[FaktorPenghambatIKPController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPenghambatIKP/{id}',[FaktorPenghambatIKPController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Penghambat Sasaran IKU
    ---------------------------------------*/
    Route::get('/get/FaktorPenghambatSasaranIKU',[FaktorPenghambatSasaranIKUController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPenghambatSasaranIKU',[FaktorPenghambatSasaranIKUController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPenghambatSasaranIKU/{id}',[FaktorPenghambatSasaranIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPenghambatSasaranIKU/{id}',[FaktorPenghambatSasaranIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPenghambatSasaranIKU/{id}',[FaktorPenghambatSasaranIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Faktor Penghambat Tujuan IKU
    ---------------------------------------*/
    Route::get('/get/FaktorPenghambatTujuanIKU',[FaktorPenghambatTujuanIKUController::class,'index']); //ambil semua data
    Route::post('/store/FaktorPenghambatTujuanIKU',[FaktorPenghambatTujuanIKUController::class,'store']); //menambahkan data
    Route::get('/show/FaktorPenghambatTujuanIKU/{id}',[FaktorPenghambatTujuanIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/FaktorPenghambatTujuanIKU/{id}',[FaktorPenghambatTujuanIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/FaktorPenghambatTujuanIKU/{id}',[FaktorPenghambatTujuanIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Indikator Kinerja
    ---------------------------------------*/
    Route::get('/get/IndikatorKinerjaRPJMD',[IndikatorKinerjaRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/IndikatorKinerjaRPJMD',[IndikatorKinerjaRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/IndikatorKinerjaRPJMD/{id}',[IndikatorKinerjaRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/IndikatorKinerjaRPJMD/{id}',[IndikatorKinerjaRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/IndikatorKinerjaRPJMD/{id}',[IndikatorKinerjaRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Indikator Program
    ---------------------------------------*/
    Route::get('/get/IndikatorProgramRPJMD',[IndikatorProgramRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/IndikatorProgramRPJMD',[IndikatorProgramRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/IndikatorProgramRPJMD/{id}',[IndikatorProgramRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/IndikatorProgramRPJMD/{id}',[IndikatorProgramRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/IndikatorProgramRPJMD/{id}',[IndikatorProgramRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    Route::apiResource('/IndikatorProgramRPJMD',IndikatorProgramRPJMDController::class);
    /*-------------------------------------
    | Indikator Sasaran IKU
    ---------------------------------------*/
    Route::get('/get/IndikatorSasaranRPJMD',[IndikatorSasaranRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/IndikatorSasaranRPJMD',[IndikatorSasaranRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/IndikatorSasaranRPJMD/{id}',[IndikatorSasaranRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/IndikatorSasaranRPJMD/{id}',[IndikatorSasaranRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/IndikatorSasaranRPJMD/{id}',[IndikatorSasaranRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Indikator Tujuan IKU
    ---------------------------------------*/
    Route::get('/get/IndikatorTujuanRPJMD',[IndikatorTujuanRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/IndikatorTujuanRPJMD',[IndikatorTujuanRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/IndikatorTujuanRPJMD/{id}',[IndikatorTujuanRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/IndikatorTujuanRPJMD/{id}',[IndikatorTujuanRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/IndikatorTujuanRPJMD/{id}',[IndikatorTujuanRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Kategori RPJMD
    ---------------------------------------*/
    Route::get('/get/KategoriRPJMD',[KategoriRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/KategoriRPJMD',[KategoriRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/KategoriRPJMD/{id}',[KategoriRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/KategoriRPJMD/{id}',[KategoriRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/KategoriRPJMD/{id}',[KategoriRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Ketercapaian RPJMD
    ---------------------------------------*/
    Route::get('/get/KetercapaianRPJMD',[KetercapaianRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/KetercapaianRPJMD',[KetercapaianRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/KetercapaianRPJMD/{id}',[KetercapaianRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/KetercapaianRPJMD/{id}',[KetercapaianRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/KetercapaianRPJMD/{id}',[KetercapaianRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Misi RPJMD
    ---------------------------------------*/
    Route::get('/get/MisiRPJMD',[MisiRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/MisiRPJMD',[MisiRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/MisiRPJMD/{id}',[MisiRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/MisiRPJMD/{id}',[MisiRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/MisiRPJMD/{id}',[MisiRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Periode
    ---------------------------------------*/
    Route::get('/get/Periode',[PeriodeController::class,'index']); //ambil semua data
    Route::post('/store/Periode',[PeriodeController::class,'store']); //menambahkan data
    Route::get('/show/Periode/{id}',[PeriodeController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Periode/{id}',[PeriodeController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Periode/{id}',[PeriodeController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Program RPJMD
    ---------------------------------------*/
    Route::get('/get/ProgramRPJMD',[ProgramRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/ProgramRPJMD',[ProgramRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/ProgramRPJMD/{id}',[ProgramRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/ProgramRPJMD/{id}',[ProgramRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/ProgramRPJMD/{id}',[ProgramRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | RPJMD
    ---------------------------------------*/
    Route::get('/get/RPJMD',[RPJMDController::class,'index']); //ambil semua data
    Route::post('/store/RPJMD',[RPJMDController::class,'store']); //menambahkan data
    Route::get('/show/RPJMD/{id}',[RPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/RPJMD/{id}',[RPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/RPJMD/{id}',[RPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Rumus
    ---------------------------------------*/
    Route::get('/get/Rumus',[RumusController::class,'index']); //ambil semua data
    Route::get('/get/RumusPositif',[RumusController::class,'indexPositif']); //ambil semua rumus positif
    Route::post('/store/Rumus',[RumusController::class,'store']); //menambahkan data
    Route::get('/show/Rumus/{id}',[RumusController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Rumus/{id}',[RumusController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Rumus/{id}',[RumusController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Sasaran RPJMD
    ---------------------------------------*/
    Route::get('/get/SasaranRPJMD',[SasaranRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/SasaranRPJMD',[SasaranRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/SasaranRPJMD/{id}',[SasaranRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SasaranRPJMD/{id}',[SasaranRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SasaranRPJMD/{id}',[SasaranRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Satuan RPJMD
    ---------------------------------------*/
    Route::get('/get/SatuanRPJMD',[SatuanRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/SatuanRPJMD',[SatuanRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/SatuanRPJMD/{id}',[SatuanRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SatuanRPJMD/{id}',[SatuanRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SatuanRPJMD/{id}',[SatuanRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    Route::apiResource('/SatuanRPJMD',SatuanRPJMDController::class);
    /*-------------------------------------
    | SKPD 90
    ---------------------------------------*/
    Route::get('/get/SKPD_90',[SKPD_90Controller::class,'index']); //ambil semua data
    Route::post('/store/SKPD_90',[SKPD_90Controller::class,'store']); //menambahkan data
    Route::get('/show/SKPD_90/{id}',[SKPD_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SKPD_90/{id}',[SKPD_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SKPD_90/{id}',[SKPD_90Controller::class,'destroy']); //menghapus data sesuai {id}

    Route::apiResource('skpd-90',SKPD_90Controller::class);
    /*-------------------------------------
    | Strategi RPJMD
    ---------------------------------------*/
    Route::get('/get/StrategiRPJMD',[StrategiRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/StrategiRPJMD',[StrategiRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/StrategiRPJMD/{id}',[StrategiRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/StrategiRPJMD/{id}',[StrategiRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/StrategiRPJMD/{id}',[StrategiRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Sumber Data
    ---------------------------------------*/
    Route::get('/get/SumberData',[SumberDataController::class,'index']); //ambil semua data
    Route::post('/store/SumberData',[SumberDataController::class,'store']); //menambahkan data
    Route::get('/show/SumberData/{id}',[SumberDataController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SumberData/{id}',[SumberDataController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SumberData/{id}',[SumberDataController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Tahun
    ---------------------------------------*/
    Route::get('/get/Tahun',[TahunController::class,'index']); //ambil semua data
    Route::post('/store/Tahun',[TahunController::class,'store']); //menambahkan data
    Route::get('/show/Tahun/{id}',[TahunController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Tahun/{id}',[TahunController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Tahun/{id}',[TahunController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Target Realisasi IKK
    ---------------------------------------*/
    Route::get('/get/TargetRealisasiIKK',[TargetRealisasiIKKController::class,'index']); //ambil semua data
    Route::post('/store/TargetRealisasiIKK',[TargetRealisasiIKKController::class,'store']); //menambahkan data
    Route::get('/show/TargetRealisasiIKK/{id}',[TargetRealisasiIKKController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TargetRealisasiIKK/{id}',[TargetRealisasiIKKController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TargetRealisasiIKK/{id}',[TargetRealisasiIKKController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Target Realisasi IKP
    ---------------------------------------*/
    Route::get('/get/TargetRealisasiIKP',[TargetRealisasiIKPController::class,'index']); //ambil semua data
    Route::post('/store/TargetRealisasiIKP',[TargetRealisasiIKPController::class,'store']); //menambahkan data
    Route::get('/show/TargetRealisasiIKP/{id}',[TargetRealisasiIKPController::class,'show']); //menampilkan data sesuai {id}
    Route::post('/update/TargetRealisasiIKP',[TargetRealisasiIKPController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TargetRealisasiIKP/{id}',[TargetRealisasiIKPController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Target Realisasi Sasaran IKU
    ---------------------------------------*/
    Route::get('/get/TargetRealisasiSasaranIKU',[TargetRealisasiSasaranIKUController::class,'index']); //ambil semua data
    Route::post('/store/TargetRealisasiSasaranIKU',[TargetRealisasiSasaranIKUController::class,'store']); //menambahkan data
    Route::get('/show/TargetRealisasiSasaranIKU/{id}',[TargetRealisasiSasaranIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TargetRealisasiSasaranIKU/{id}',[TargetRealisasiSasaranIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TargetRealisasiSasaranIKU/{id}',[TargetRealisasiSasaranIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Target Realisasi Tujuan IKU
    ---------------------------------------*/
    Route::get('/get/TargetRealisasiTujuanIKU',[TargetRealisasiTujuanIKUController::class,'index']); //ambil semua data
    Route::post('/store/TargetRealisasiTujuanIKU',[TargetRealisasiTujuanIKUController::class,'store']); //menambahkan data
    Route::get('/show/TargetRealisasiTujuanIKU/{id}',[TargetRealisasiTujuanIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TargetRealisasiTujuanIKU/{id}',[TargetRealisasiTujuanIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TargetRealisasiTujuanIKU/{id}',[TargetRealisasiTujuanIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Tindak Lanjut IKP
    ---------------------------------------*/
    Route::get('/get/TindakLanjutIKP',[TindakLanjutIKPController::class,'index']); //ambil semua data
    Route::post('/store/TindakLanjutIKP',[TindakLanjutIKPController::class,'store']); //menambahkan data
    Route::get('/show/TindakLanjutIKP/{id}',[TindakLanjutIKPController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TindakLanjutIKP/{id}',[TindakLanjutIKPController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TindakLanjutIKP/{id}',[TindakLanjutIKPController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Tindak Lanjut Sasaran IKU
    ---------------------------------------*/
    Route::get('/get/TindakLanjutSasaranIKU',[TindakLanjutSasaranIKUController::class,'index']); //ambil semua data
    Route::post('/store/TindakLanjutSasaranIKU',[TindakLanjutSasaranIKUController::class,'store']); //menambahkan data
    Route::get('/show/TindakLanjutSasaranIKU/{id}',[TindakLanjutSasaranIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TindakLanjutSasaranIKU/{id}',[TindakLanjutSasaranIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TindakLanjutSasaranIKU/{id}',[TindakLanjutSasaranIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Tindak Lanjut Tujuan IKU
    ---------------------------------------*/
    Route::get('/get/TindakLanjutTujuanIKU',[TindakLanjutTujuanIKUController::class,'index']); //ambil semua data
    Route::post('/store/TindakLanjutTujuanIKU',[TindakLanjutTujuanIKUController::class,'store']); //menambahkan data
    Route::get('/show/TindakLanjutTujuanIKU/{id}',[TindakLanjutTujuanIKUController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TindakLanjutTujuanIKU/{id}',[TindakLanjutTujuanIKUController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TindakLanjutTujuanIKU/{id}',[TindakLanjutTujuanIKUController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Tujuan RPJMD
    ---------------------------------------*/
    Route::get('/get/TujuanRPJMD',[TujuanRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/TujuanRPJMD',[TujuanRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/TujuanRPJMD/{id}',[TujuanRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/TujuanRPJMD/{id}',[TujuanRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/TujuanRPJMD/{id}',[TujuanRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Urusan RPJMD
    ---------------------------------------*/
    Route::get('/get/UrusanRPJMD',[UrusanRPJMDController::class,'index']); //ambil semua data
    Route::post('/store/UrusanRPJMD',[UrusanRPJMDController::class,'store']); //menambahkan data
    Route::get('/show/UrusanRPJMD/{id}',[UrusanRPJMDController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/UrusanRPJMD/{id}',[UrusanRPJMDController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/UrusanRPJMD/{id}',[UrusanRPJMDController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Bidang Urusan 90
    ---------------------------------------*/
    Route::get('/get/BidangUrusan_90',[BidangUrusan_90Controller::class,'index']); //ambil semua data
    Route::post('/store/BidangUrusan_90',[BidangUrusan_90Controller::class,'store']); //menambahkan data
    Route::get('/show/BidangUrusan_90/{id}',[BidangUrusan_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/BidangUrusan_90/{id}',[BidangUrusan_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/BidangUrusan_90/{id}',[BidangUrusan_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Kegiatan 90
    ---------------------------------------*/
    Route::get('/get/Kegiatan_90',[Kegiatan_90Controller::class,'index']); //ambil semua data
    Route::post('/store/Kegiatan_90',[Kegiatan_90Controller::class,'store']); //menambahkan data
    Route::get('/show/Kegiatan_90/{id}',[Kegiatan_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Kegiatan_90/{id}',[Kegiatan_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Kegiatan_90/{id}',[Kegiatan_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Program 90
    ---------------------------------------*/
    Route::get('/get/Program_90',[Program_90Controller::class,'index']); //ambil semua data
    Route::post('/store/Program_90',[Program_90Controller::class,'store']); //menambahkan data
    Route::get('/show/Program_90/{id}',[Program_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Program_90/{id}',[Program_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Program_90/{id}',[Program_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Anggaran
    ---------------------------------------*/
    Route::get('/get/SPM_Anggaran',[SPM_AnggaranController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Anggaran',[SPM_AnggaranController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Anggaran/{id}',[SPM_AnggaranController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Anggaran/{id}',[SPM_AnggaranController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Anggaran/{id}',[SPM_AnggaranController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Capaian
    ---------------------------------------*/
    Route::get('/get/SPM_Capaian',[SPM_CapaianController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Capaian',[SPM_CapaianController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Capaian/{id}',[SPM_CapaianController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Capaian/{id}',[SPM_CapaianController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Capaian/{id}',[SPM_CapaianController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Dasar Hukum
    ---------------------------------------*/
    Route::get('/get/SPM_Dasar_Hukum',[SPM_Dasar_HukumController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Dasar_Hukum',[SPM_Dasar_HukumController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Dasar_Hukum/{id}',[SPM_Dasar_HukumController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Dasar_Hukum/{id}',[SPM_Dasar_HukumController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Dasar_Hukum/{id}',[SPM_Dasar_HukumController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Indikator Pencapaian
    ---------------------------------------*/
    Route::get('/get/SPM_Indikator_Pencapaian',[SPM_Indikator_PencapaianController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Indikator_Pencapaian',[SPM_Indikator_PencapaianController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Indikator_Pencapaian/{id}',[SPM_Indikator_PencapaianController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Indikator_Pencapaian/{id}',[SPM_Indikator_PencapaianController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Indikator_Pencapaian/{id}',[SPM_Indikator_PencapaianController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Jenis Layanan
    ---------------------------------------*/
    Route::get('/get/SPM_Jenis_Layanan',[SPM_Jenis_LayananController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Jenis_Layanan',[SPM_Jenis_LayananController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Jenis_Layanan/{id}',[SPM_Jenis_LayananController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Jenis_Layanan/{id}',[SPM_Jenis_LayananController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Jenis_Layanan/{id}',[SPM_Jenis_LayananController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Landasan Hukum
    ---------------------------------------*/
    Route::get('/get/SPM_Landasan_Hukum',[SPM_Landasan_HukumController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Landasan_Hukum',[SPM_Landasan_HukumController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Landasan_Hukum/{id}',[SPM_Landasan_HukumController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Landasan_Hukum/{id}',[SPM_Landasan_HukumController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Landasan_Hukum/{id}',[SPM_Landasan_HukumController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Pelayanan
    ---------------------------------------*/
    Route::get('/get/SPM_Pelayanan',[SPM_PelayananController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Pelayanan',[SPM_PelayananController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Pelayanan/{id}',[SPM_PelayananController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Pelayanan/{id}',[SPM_PelayananController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Pelayanan/{id}',[SPM_PelayananController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM R Jenis Rincian
    ---------------------------------------*/
    Route::get('/get/SPM_R_jenisRincian',[SPM_R_jenisRincianController::class,'index']); //ambil semua data
    Route::post('/store/SPM_R_jenisRincian',[SPM_R_jenisRincianController::class,'store']); //menambahkan data
    Route::get('/show/SPM_R_jenisRincian/{id}',[SPM_R_jenisRincianController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_R_jenisRincian/{id}',[SPM_R_jenisRincianController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_R_jenisRincian/{id}',[SPM_R_jenisRincianController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Rincian Jenis Layanan
    ---------------------------------------*/
    Route::get('/get/SPM_Rincian_Jenis_Layanan',[SPM_Rincian_Jenis_LayananController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Rincian_Jenis_Layanan',[SPM_Rincian_Jenis_LayananController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Rincian_Jenis_Layanan/{id}',[SPM_Rincian_Jenis_LayananController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Rincian_Jenis_Layanan/{id}',[SPM_Rincian_Jenis_LayananController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Rincian_Jenis_Layanan/{id}',[SPM_Rincian_Jenis_LayananController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | SPM Urusan
    ---------------------------------------*/
    Route::get('/get/SPM_Urusan',[SPM_UrusanController::class,'index']); //ambil semua data
    Route::post('/store/SPM_Urusan',[SPM_UrusanController::class,'store']); //menambahkan data
    Route::get('/show/SPM_Urusan/{id}',[SPM_UrusanController::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/SPM_Urusan/{id}',[SPM_UrusanController::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/SPM_Urusan/{id}',[SPM_UrusanController::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Urusan 90
    ---------------------------------------*/
    Route::get('/get/Urusan_90',[Urusan_90Controller::class,'index']); //ambil semua data
    Route::post('/store/Urusan_90',[Urusan_90Controller::class,'store']); //menambahkan data
    Route::get('/show/Urusan_90/{id}',[Urusan_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/Urusan_90/{id}',[Urusan_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/Urusan_90/{id}',[Urusan_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Verifikasi Indikator Kegiatan 90
    ---------------------------------------*/
    Route::get('/get/VerifikasiIndikatorKegiatan_90',[VerifikasiIndikatorKegiatan_90Controller::class,'index']); //ambil semua data
    Route::post('/store/VerifikasiIndikatorKegiatan_90',[VerifikasiIndikatorKegiatan_90Controller::class,'store']); //menambahkan data
    Route::get('/show/VerifikasiIndikatorKegiatan_90/{id}',[VerifikasiIndikatorKegiatan_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/VerifikasiIndikatorKegiatan_90/{id}',[VerifikasiIndikatorKegiatan_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/VerifikasiIndikatorKegiatan_90/{id}',[VerifikasiIndikatorKegiatan_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | Verifikasi Indikator Sub Kegiatan 90
    ---------------------------------------*/
    Route::get('/get/VerifikasiIndikatorSubKegiatan_90',[VerifikasiIndikatorSubKegiatan_90Controller::class,'index']); //ambil semua data
    Route::post('/store/VerifikasiIndikatorSubKegiatan_90',[VerifikasiIndikatorSubKegiatan_90Controller::class,'store']); //menambahkan data
    Route::get('/show/VerifikasiIndikatorSubKegiatan_90/{id}',[VerifikasiIndikatorSubKegiatan_90Controller::class,'show']); //menampilkan data sesuai {id}
    Route::put('/update/VerifikasiIndikatorSubKegiatan_90/{id}',[VerifikasiIndikatorSubKegiatan_90Controller::class,'update']); //mengubah data sesuai {id}
    Route::delete('/delete/VerifikasiIndikatorSubKegiatan_90/{id}',[VerifikasiIndikatorSubKegiatan_90Controller::class,'destroy']); //menghapus data sesuai {id}

    /*-------------------------------------
    | MonevRKPDHasilController
    ---------------------------------------*/
    Route::get('/get/dataIKP',[MonevRKPDHasilController::class,'dataIKP']);
    Route::get('/get/dataIKK',[MonevRKPDHasilController::class,'dataIKK']);

    /**
     * Misi
     * Tujuan (Sasaran Kota)
     * Indikator Tujuan
     * Target Realisasi Indikator Tujuan
     */
    Route::apiResource('/misi',MisiController::class);
    Route::apiResource('/tujuan',TujuanController::class);
    Route::apiResource('/indikatortujuan',IndikatorTujuanController::class);
    Route::apiResource('/targetrealisasitujuan',TargetRealisasiTujuanController::class);
    Route::post('/targetrealisasitujuan/customshow',[TargetRealisasiTujuanController::class,'customshow']);
    Route::apiResource('/sasaran',SasaranController::class);
    Route::apiResource('/indikatorsasaran',IndikatorSasaranController::class);
    Route::apiResource('/targetrealisasisasaran',TargetRealisasiSasaranController::class);
    Route::post('/targetrealisasisasaran/customshow',[TargetRealisasiSasaranController::class,'customshow']);

    /**
     * Rekap Anggaran
     */
    Route::post('rekap/anggaran',[API_RENJAController::class,'getRekapAnggaran']);
    
});
/*-------------------------------------
    | SDGS Tujuan
    ---------------------------------------*/
    Route::get('/get/sdgstujuan',[SdgsTujuanController::class,'index'])->name('sdgstujuan.index'); //ambil semua data
    Route::post('/store/sdgstujuan',[SdgsTujuanController::class,'store'])->name('sdgstujuan.store'); //menambahkan data
    Route::get('/show/sdgstujuan/{id}',[SdgsTujuanController::class,'show']); //menampilkan data sesuai {id}
    Route::get('/showall/sdgstujuan',[SdgsTujuanController::class,'showAll']);
    Route::post('/update/sdgstujuan/{id}',[SdgsTujuanController::class,'update']); //mengubah data sesuai {id}
    Route::post('/delete/sdgstujuan',[SdgsTujuanController::class,'destroy'])->name('sdgstujuan.delete'); //menghapus data sesuai {id}

    /*-------------------------------------
    | SDGS Indikator Global
    ---------------------------------------*/
    Route::get('/get/sdgsindglobal',[SdgsIndikatorGlobalController::class,'index'])->name('sdgsindglobal.index'); //ambil semua data
    Route::post('/store/sdgsindglobal',[SdgsIndikatorGlobalController::class,'store'])->name('sdgsindglobal.store'); //menambahkan data
    Route::get('/show/sdgsindglobal/{id}',[SdgsIndikatorGlobalController::class,'show']); //menampilkan data sesuai {id}
    Route::get('/showbytujuan/sdgsindglobal/{id}',[SdgsIndikatorGlobalController::class,'showByTujuan']); //menampilkan data sesuai {id} tujuan
    Route::post('/update/sdgsindglobal/{id}',[SdgsIndikatorGlobalController::class,'update']); //mengubah data sesuai {id}
    Route::post('/delete/sdgsindglobal',[SdgsIndikatorGlobalController::class,'destroy'])->name('sdgsindglobal.delete'); //menghapus data sesuai {id}

    /*-------------------------------------
    | SDGS Indikator Nasional
    ---------------------------------------*/
    Route::get('/get/sdgsindnasional',[SdgsIndikatorNasionalController::class,'index'])->name('sdgsindnasional.index'); //ambil semua data
    Route::post('/store/sdgsindnasional',[SdgsIndikatorNasionalController::class,'store'])->name('sdgsindnasional.store'); //menambahkan data
    Route::get('/show/sdgsindnasional/{id}',[SdgsIndikatorNasionalController::class,'show']); //menampilkan data sesuai {id}
    Route::get('/showbyindglobal/sdgsindnasional/{id}',[SdgsIndikatorNasionalController::class,'showByIndglobal']); //menampilkan data sesuai {id} indikator global
    Route::post('/update/sdgsindnasional/{id}',[SdgsIndikatorNasionalController::class,'update']); //mengubah data sesuai {id}
    Route::post('/delete/sdgsindnasional',[SdgsIndikatorNasionalController::class,'destroy'])->name('sdgsindnasional.delete'); //menghapus data sesuai {id}

    /*-------------------------------------
    | SDGS Indikator Kota
    ---------------------------------------*/
    Route::get('/get/sdgsindkota',[SdgsIndikatorKotaController::class,'index'])->name('sdgsindkota.index');
    Route::get('/get/master/sdgsindkota',[SdgsIndikatorKotaController::class,'master'])->name('sdgsindkota.master'); //ambil semua data
    Route::post('/store/sdgsindkota',[SdgsIndikatorKotaController::class,'store'])->name('sdgsindkota.store'); //menambahkan data
    Route::get('/show/sdgsindkota/{id}',[SdgsIndikatorKotaController::class,'show']); //menampilkan data sesuai {id}
    Route::post('/update/sdgsindkota/{id}',[SdgsIndikatorKotaController::class,'update']); //mengubah data sesuai {id}
    Route::post('/delete/sdgsindkota',[SdgsIndikatorKotaController::class,'destroy'])->name('sdgsindkota.delete'); //menghapus data sesuai {id}

    /*-------------------------------------
    | SDGS Monev Indikator Kota
    ---------------------------------------*/
    Route::get('/get/monevsdgsindkota',[MonevSdgsIndikatorKotaController::class,'index'])->name('monevsdgsindkota.index'); //ambil semua data
    Route::post('/store/monevsdgsindkota',[MonevSdgsIndikatorKotaController::class,'store'])->name('monevsdgsindkota.store'); //menambahkan data

    /*-------------------------------------
    | SDGS Monev Indikator Renja
    ---------------------------------------*/
    Route::get('/get/monevsdgsindrenja',[MonevSdgsIndikatorRenjaController::class,'index'])->name('monevsdgsindrenja.index'); //ambil semua data

    /*-------------------------------------
    | SDGS Coding
    ---------------------------------------*/
    Route::get('/get/sdgscoding',[SdgsCodingController::class,'index'])->name('sdgscoding.index'); //ambil semua data
    Route::post('/store/sdgscoding',[SdgsCodingController::class,'store'])->name('sdgscoding.store'); //menambahkan data
    Route::get('/showbylogind/sdgscoding/{id}',[SdgsCodingController::class,'showbyLogIndikatorKota']); //menampilkan data sesuai {id}
    Route::get('/showbypd/sdgscoding/{id}',[SdgsCodingController::class,'showbyPd']); //menampilkan data sesuai {id}

    /*-------------------------------------
    | SDGS Dashboard
    ---------------------------------------*/
    Route::get('/pdsdgs/sdgsdashboard',[SdgsDashboardController::class,'getPerangkatDaerahSdgs']);
    Route::get('/detailpdsdgs/sdgsdashboard',[SdgsDashboardController::class,'getDetailPerangkatDaerahSdgs']);
    Route::get('/infodetailpdsdgs/sdgsdashboard',[SdgsDashboardController::class,'getInfoDetailPerangkatDaerahSdgs']);
    Route::get('/pemetaansdgs/sdgsdashboard',[SdgsDashboardController::class,'getPemetaanSdgs']);
    Route::get('/detailpemetaansdgs/sdgsdashboard',[SdgsDashboardController::class,'getDetailPemetaanSdgs']);
    Route::get('/capaianpaguindsdgs/sdgsdashboard',[SdgsDashboardController::class,'capaianPaguIndKota']);
    Route::get('/capaiankinerjaindsdgs/sdgsdashboard',[SdgsDashboardController::class,'capaianKinerjaIndKota']);
    Route::get('/cetakrealisasi/sdgsdashboard',[SdgsDashboardController::class,'cetakRealisasiSdgs']);
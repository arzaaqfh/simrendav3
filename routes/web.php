<?php

use App\Http\Controllers\SPM_Controller;
use App\Http\Controllers\Monev_Controller;
use App\Http\Controllers\RKPD_Controller;
use App\Http\Controllers\RENJA_Controller;
use App\Http\Controllers\Sdgs_Controller;
use App\Http\Controllers\MonevRENJA_Controller;
use App\Http\Controllers\Perencanaan_Controller;
use App\Http\Controllers\Musrenbang_Controller;
use App\Http\Controllers\MusrenbangKOTA_Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Administrator_Controller;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('main/dashboard');
});

Route::get('/main', function () {
    return view('main/mainmenu');
});

Route::get('/render', function () {
    return view('tes/render');
});
// Route Laman API
Route::get('/simrendata', function () {
	return view('home_simrendata');
});

// SPM
Route::get('/spm',[SPM_Controller::class,'index']);
Route::get('/spm/dasarHukum',[SPM_Controller::class,'dasarHukum']);
Route::get('/spm/landasanHukum',[SPM_Controller::class,'landasanHukum']);
Route::get('/spm/penerapan',[SPM_Controller::class,'penerapan']);
Route::get('/spm/jenisLayanan',[SPM_Controller::class,'jenisLayanan']);
Route::get('/spm/jenisLayanan/destroy/{id}',[SPM_Controller::class,'destroyJenisLayanan']);
Route::get('/spm/rincianJenisLayanan',[SPM_Controller::class,'rincianJenisLayanan']);
Route::get('/spm/rincianJenisLayanan/destroy/{id}',[SPM_Controller::class,'destroyRincianJenisLayanan']);
Route::get('/spm/relasiJenisRincian',[SPM_Controller::class,'relasiJenisRincian']);
Route::get('/spm/relasiJenisRincian/destroy/{id}',[SPM_Controller::class,'destroyRelasiJenisRincian']);
Route::get('/spm/indikatorPencapaian',[SPM_Controller::class,'indikatorPencapaian']);
Route::get('/spm/indikatorPencapaian/destroy/{id}',[SPM_Controller::class,'destroyIndikatorPencapaian']);
Route::get('/spm/detailSPM/{id_bidang_urusan}/{id_tahun}/{tw}',[SPM_Controller::class,'detailSPM']);
Route::get('/spm/addFormCapaian/{id_bidang_urusan}/{tw}/{jmlRincian}',[SPM_Controller::class,'addFormCapaian']);
Route::post('/spm/doAddFormCapaian', [SPM_Controller::class, 'doAddFormCapaian']);
Route::post('/spm/capaian/update', [SPM_Controller::class, 'updateCapaian']);

//SDGS
Route::get('/sdgs',[Sdgs_Controller::class,'index']);
Route::get('/sdgs/tujuansdgs',[Sdgs_Controller::class,'tujuansdgs']);
Route::get('/sdgs/indikatorglobalsdgs',[Sdgs_Controller::class,'indikatorglobalsdgs']);
Route::get('/sdgs/indikatornasionalsdgs',[Sdgs_Controller::class,'indikatornasionalsdgs']);
Route::get('/sdgs/indikatorprovinsisdgs',[Sdgs_Controller::class,'indikatorprovinsisdgs']);
Route::get('/sdgs/indikatorkotasdgs',[Sdgs_Controller::class,'indikatorkotasdgs']);
Route::get('/sdgs/codingsdgs',[Sdgs_Controller::class,'codingsdgs']);
Route::get('/sdgs/monevsdgs',[Sdgs_Controller::class,'monevsdgs']);
Route::get('/sdgs/monevrenja',[Sdgs_Controller::class,'monevrenja']);
Route::get('/sdgs/dashboard/rekap',[Sdgs_Controller::class,'dashboardrekap']);
Route::get('/sdgs/dashboard/detailrekap/{param}',[Sdgs_Controller::class,'dashboardrekapdetail']);
Route::get('/sdgs/download/lampiranmonev', function()
{
	$filename =  Request::get("file");
    // Check if file exists in app/storage/file folder
    $file_path = public_path() .'/lampiran_sdgs/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9\-\_\.]+');

/*MONEV*/
Route::get('/monev', [Monev_Controller::class, 'index']);
Route::get('/monev/grafik', [Monev_Controller::class, 'grafikMonev']);
	/*RKPD*/
	Route::get('/monev/rkpd', [RKPD_Controller::class, 'index']);
	Route::get('/monev/rkpd/ikuTujuan', [RKPD_Controller::class, 'monevHasilIKUTujuan']);
	Route::get('/monev/rkpd/ikuTujuan/input/{id}', [RKPD_Controller::class, 'inputMonevHasilIKUTujuan']);
	Route::post('/monev/rkpd/ikuTujuan/update', [RKPD_Controller::class, 'updateMonevHasilIKUTujuan']);
	Route::get('/monev/rkpd/ikuSasaran', [RKPD_Controller::class, 'monevHasilIKUSasaran']);
	Route::get('/monev/rkpd/ikuSasaran/input/{id}', [RKPD_Controller::class, 'inputMonevHasilIKUSasaran']);
	Route::post('/monev/rkpd/ikuSasaran/update', [RKPD_Controller::class, 'updateMonevHasilIKUSasaran']);
	Route::get('/monev/rkpd/ikk', [RKPD_Controller::class, 'monevHasilIKK']);
	Route::get('/monev/rkpd/ikk/input/{id}', [RKPD_Controller::class, 'inputMonevHasilIKK']);
	Route::post('/monev/rkpd/ikk/update', [RKPD_Controller::class, 'updateMonevHasilIKK']);
	Route::get('/monev/rkpd/ikp', [RKPD_Controller::class, 'monevHasilIKP']);
	Route::get('/monev/rkpd/ikp/input/{id}', [RKPD_Controller::class, 'inputmonevHasilIKP']);
	Route::post('/monev/rkpd/ikp/update', [RKPD_Controller::class, 'updateMonevHasilIKP']);
	/*RENJA*/
	Route::get('/monev/renja', [MonevRENJA_Controller::class, 'index']);
	Route::get('/monev/renja/murni', [MonevRENJA_Controller::class, 'monevHasilMurni']);
	Route::post('/monev/renja/murni/inputrealisasiindkegiatan', [MonevRENJA_Controller::class, 'inputRealisasiIndKegiatan']);
	Route::get('/monev/renja/murni/inputanalisiskegiatan', [MonevRENJA_Controller::class, 'inputAnalisisKegiatan']);
	Route::post('/monev/renja/murni/saverealisasikeg',[MonevRENJA_Controller::class,'saveRealisasiKegiatan']);
	Route::post('/monev/renja/murni/inputrealisasiindsubkegiatan', [MonevRENJA_Controller::class, 'inputRealisasiIndSubKegiatan']);
	Route::get('/monev/renja/murni/inputanalisissubkegiatan', [MonevRENJA_Controller::class, 'inputAnalisisSubKegiatan']);
	Route::post('/monev/renja/murni/saverealisasisubkeg',[MonevRENJA_Controller::class,'saveRealisasiSubKegiatan']);
	Route::get('/monev/renja/exportrkpd', [MonevRENJA_Controller::class,'exportRkpd']);
	Route::get('/monev/renja/perubahan', [MonevRENJA_Controller::class, 'monevHasilPerubahan']);
	Route::get('/monev/renja/bataswaktu', [MonevRENJA_Controller::class, 'batasWaktu']);
	Route::post('/monev/renja/bataswaktu/input', [MonevRENJA_Controller::class, 'batasWaktuInput']);
	Route::get('/monev/renja/settinginput', [MonevRENJA_Controller::class, 'settingInput']);
	Route::post('/monev/renja/settinginput/save', [MonevRENJA_Controller::class, 'settingInputSave']);
	Route::get('/monev/renja/settinguser', [MonevRENJA_Controller::class, 'settingUser']);
	Route::get('/monev/renja/settinguser/edit/{id}', [MonevRENJA_Controller::class, 'settingUserEdit']);
	Route::get('/monev/renja/settinguser/create', [MonevRENJA_Controller::class, 'settingUserCreate']);
	Route::post('/monev/renja/settinguser/add', [MonevRENJA_Controller::class, 'settingUserAdd']);
	Route::post('/monev/renja/settinguser/save', [MonevRENJA_Controller::class, 'settingUserSave']);
	Route::get('/monev/renja/settinguser/delete/{id}', [MonevRENJA_Controller::class, 'settingUserDelete']);
	Route::get('/monev/renja/realisasi', [MonevRENJA_Controller::class, 'realisasiRenja']);
	Route::post('/monev/renja/input/realisasi/IndSubKegiatan', [MonevRENJA_Controller::class, 'inputRIndSubKeg']);
	Route::post('/monev/renja/input/realisasi/IndKegiatan', [MonevRENJA_Controller::class, 'inputRIndKeg']);
	Route::post('/monev/renja/input/realisasi/IndProgram', [MonevRENJA_Controller::class, 'inputRIndProg']);
	Route::post('/monev/renja/input/realisasi/AnlFaktor', [MonevRENJA_Controller::class, 'inputAnlFaktor']);
	Route::post('/monev/renja/input/analisisFaktor/SubKegiatan', [MonevRENJA_Controller::class, 'inputAFSubKeg']);
	Route::post('/monev/renja/input/add/IndSubKeg', [MonevRENJA_Controller::class, 'addindsubkegiatan']);
	Route::post('/monev/renja/input/add/IndKeg', [MonevRENJA_Controller::class, 'addindkegiatan']);
	Route::get('monev/renja/rekap/renja', [MonevRENJA_Controller::class, 'rekapRenja']);
	Route::get('monev/renja/cetak/{jenis_data}/{file_type}/{id_skpd}/{all}/{tw}', [MonevRENJA_Controller::class, 'cetakRekap']);
	Route::get('monev/renja/importMonev', [MonevRENJA_Controller::class, 'importMonev']);
	Route::post('/monev/renja/importFile', [MonevRENJA_Controller::class, 'importFile']);
	Route::post('/monev/renja/previewFile', [MonevRENJA_Controller::class, 'previewFile']);
	Route::get('/monev/renja/downloadPreviewFile', [MonevRENJA_Controller::class, 'downloadPreviewFile']);
	//Route::get('/monev/renja/perubahan', [MonevRENJA_Controller::class, 'monevHasilPerubahan']);
	//Route::get('/monev/renja/analisis', [MonevRENJA_Controller::class, 'analisisMonevRenja']);

/*PERENCANAAN*/
Route::get('/perencanaan', [Perencanaan_Controller::class, 'index'])->name('perencanaan');
	/*RENJA*/
	Route::get('/perencanaan/renja', [RENJA_Controller::class, 'index']);
	Route::get('/perencanaan/renja/final', [RENJA_Controller::class, 'renjaFinal']);
	Route::post('/perencanaan/renja/saveindikatorkegiatan',[RENJA_Controller::class,'saveIndikatorKegiatan']);
	Route::post('/perencanaan/renja/saveindikatorsubkegiatan',[RENJA_Controller::class,'saveIndikatorSubKegiatan']);
	Route::post('/perencanaan/renja/verifikasiIndKeg',[RENJA_Controller::class,'doVerIndKeg']);
	Route::post('/perencanaan/renja/verifikasiIndSubKeg',[RENJA_Controller::class,'doVerIndSubKeg']);

	//Route::get('/perencanaan/renja/cascading', [RENJA_Controller::class, 'renjaCascading']);

/*MUSRENBANG*/
Route::get('/musrenbang', [Musrenbang_Controller::class, 'index'])->name('musrenbang');
	/*KOTA*/
	Route::get('/musrenbang/kota', [MusrenbangKOTA_Controller::class, 'index']);
	/*Route::get('/perencanaan/renja/murni', [RENJA_Controller::class, 'renjaMurni']);
	Route::get('/perencanaan/renja/cascading', [RENJA_Controller::class, 'renjaCascading']);*/

/*ADMINISTRATOR*/
Route::get('/administrator', [Administrator_Controller::class, 'index']);
Route::get('/administrator/monev/rkpd', [Administrator_Controller::class, 'indexRKPD']);
Route::get('/administrator/monev/rkpd/targetRealisasiTujuanIKU', [Administrator_Controller::class, 'targetRealisasiTujuanIKU']);
Route::get('/administrator/monev/rkpd/targetRealisasiSasaranIKU', [Administrator_Controller::class, 'targetRealisasiSasaranIKU']);
Route::get('/administrator/monev/rkpd/targetRealisasiIKK', [Administrator_Controller::class, 'targetRealisasiIKK']);
Route::get('/administrator/monev/rkpd/targetRealisasiIKP', [Administrator_Controller::class, 'targetRealisasiIKP']);
Route::get('/administrator/monev/rkpd/indikatorTujuanIKU', [Administrator_Controller::class, 'indikatorTujuanIKU']);
Route::get('/administrator/monev/rkpd/indikatorSasaranIKU', [Administrator_Controller::class, 'indikatorSasaranIKU']);
Route::get('/administrator/monev/rkpd/indikatorKinerja', [Administrator_Controller::class, 'indikatorKinerja']);
Route::get('/administrator/monev/rkpd/indikatorProgram', [Administrator_Controller::class, 'indikatorProgram']);
Route::get('/administrator/monev/rkpd/input/targetRealisasiIKP', [Administrator_Controller::class, 'inputTargetRealisasiIKP']);
Route::get('/administrator/monev/rkpd/input/indikatorProgram', [Administrator_Controller::class, 'inputIndikatorProgram']);
Route::post('/administrator/monev/rkpd/create/targetRealisasiIKP', [Administrator_Controller::class, 'createTargetRealisasiIKP']);
Route::post('/administrator/monev/rkpd/create/indikatorProgram', [Administrator_Controller::class, 'createIndikatorProgram']);

Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/set/session', [HomeController::class, 'session']);
//Route::get('/home', [HomeController::class, 'index'])->name('home');
/**
 * Cetak SDGS
 */
Route::post('/sdgs/cetak',[Sdgs_Controller::class,'cetaksdgs']);
Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});